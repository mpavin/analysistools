Analysis Tools for p + T2K replica target at 31 GeV/c
=====================================================
This set of tools was developed for the T2K replica target analysis in NA61/SHINE experiment at CETN SPS. 
In particular it was kept in mind that high-statistics dataset will be analyzed. The dataset contains around 9 million events recorded with normal magnetic field and 1.2 million events with high magnetic field. To get more details please see NA61/SHINE bookkeeping page (https://na61bq.web.cern.ch/na61bq/xc/bq.html). Tools have been divided on two parts. First part contains general classes and definitions. They are located at PathToTools/include and  PathToTools/src. Here is the list of them:
- List of the classes:
-# AnaTrack
-# Bin
-# Cut
-# EventSelection
-# PhaseSpace
-# PlotGraph
-# PlotHisto
-# Selection
-# TargetAna
-# TrackExtrapolation
-# TrackSelection
- List of the files in which various variables, functions, structs and enums have been defined:
-# CConfig.h
-# Constants.h
-# Functions.h
-# TreeDataTypes.h\n
Second part contains so-called modules. Modules preform various tasks and extract information from the SHOE files. Also modules use classes and definitions from the first part described above, but they can have their own classes and definitions. In other words modules are standalone C++ applications which make use of the classes defined in PathToTools/include and  PathToTools/src. Usually modules consist of one *.cpp file with main function located at PatToTools/modules/ModuleName/name.cpp and other classes and definitions located in the PathToTools/Modules/ModuleName/include and PathToTools/Modules/ModuleName/src. It is important to note that modules come in pairs. For example module DataAna will analyze the data and obtain dE/dx and TOF information, but module SpectrumPlotter will read the output of DataAna and make histograms. In this way data does not need to be reanalyzed just because one decides to change binning of the histograms or size of the phase space. First module which analyzes the data is called with:\n
__FirstModuleName -b bootstrap.xml -n NumberOfEvents__\n
and second is called:\n
__SecondModuleName -b bootstrap.xml -i InputFromFirstModule__\n
Modules use bootstrap file which contains paths to the config files. 
Let's take one module and see how it works. 

DataAna
-------
DataAna module is used for obtaining dE/dx and TOF information from the SHOE files. It loops over events and tracks, applies cuts on them and extrapolates tracks towards the target position. Like most of the modules, this module is called like \n
__dataAna -b bootstrap.xml -n Number_of_Events__\n

The heart of DataAna is in function Run in class Ana. Let's examine how this function works. \n

\includelineno Ana.cc

In the line 38 and 39 two string variables are defined. First one is name of the file containing information about dE/dx and TOF spectrum and other is name containing information about events and tracks which passed cuts. Next step is to read SHOE data files. This is done with EventFileChain class from SHINE and vector of file names is obtained from config file. Then in line 43 instance of Event class has been initialized. In it, information from the SHOE files will be stored. \n
In lines 47 and 48 track and event selection have been initialized. When this classes are created they automatically read configuration files containing cut parameters from bootstrap.xml . In the next step track extrapolation has been defined. It takes target as an argument because extrapolation is done to the target surface. \n
Output file has been opened and SpectrumdEdxToF struct has been assigned as branch of Root tree. Now, everything is ready for analyzing the data. Function loops over events until the end or until number events is reached number defined when module is called. \n
It is possible to skip some runs if this information is defined in the configuration file. This is done in lines 69-78. \n
In the line 85 event is checked if passes all event cuts. If this is not the case then remainder of the code is skipped and loop runs from the beginning. At line 91 for loops has been introduced. It runs over all global TPC tracks in the event. First obstacle is obtaining TOF information, because it is not stored in Track structure. So, one needs to match TOF hit with VertexTrack structure and Track with VertexTrack in order to obtain TOF information about TPC track. This is done in lines 95-133. In this part of the code, AnaTrack has been defined. It contains all information from Track structure including TOF information. Also, this class has built-in function which calls for track extrapolation in magnetic field. \n
Now, cuts on AnaTrack structure are applied in line 134. If track fails to pass the cuts, loops start from the beginning (switches to the next event).  During the track selection procedure, extrapolation in magnetic field is done.\n
From lines 138 to 156 track information is stored in the Root tree. Root tree is then written to the Root file and this is also the case for the information from the EventSelection and TrackSelection classes. 
