#include <TFile.h>
#include <TH1D.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <sstream>
#include <iostream>    
#include <iomanip> 
#include <stdlib.h>

std::string ConvertToString(double x, int prec){
	std::stringstream ss;
	std::string val;

	ss << std::fixed << std::setprecision(prec);
	ss << x;
	ss >> val;

	return val;
}
void CountEvents(){

	string inputFile = "listCutsMC.txt";
	vector<string> job;
	
	ifstream ifs;
	string line;
	ifs.open(inputFile.c_str());
	double nEvents = 0;	
	double nAll = 0;
	if (!ifs.is_open()){
		cerr << "[ReadInputListFile] Could not open infile. Exiting..." << endl;			
		exit (101);
	}
    
	while (getline(ifs, line)){
		if(line.at(0)=='#')
			continue;
		job.push_back(line.c_str());
	}
	ifs.close();		

	
	for(int i = 0; i < job.size(); i++){
		TFile input(job.at(i).c_str(), "READ");
		
		TH1D* hist = (TH1D*) input.Get("event_cuts");
		//cout << hist->GetBinContent(3) << endl;
		nEvents += hist->GetBinContent(4);
		nAll += hist->GetBinContent(1);
	}
	string n1 = ConvertToString(nEvents+0.0,0);
	string n2 = ConvertToString(nAll+0.0,0);
	
	cout << n1 << " " << n2 << " " << nEvents/nAll << endl;
}
