#!/bin/sh                                                                                                                                                                

MC=1
START=1
STEP=10
#for (( i = 0 ; i < 1290 ; i++ )); do
#for (( i = 0 ; i < 838 ; i++ )); do
for (( i = 0 ; i < 400 ; i++ )); do
    bsub -q 8nh -J "DATAANA_${i}" -e /afs/cern.ch/work/m/mpavin/logs/dataana.err -o /afs/cern.ch/work/m/mpavin/logs/dataana.log run.sh $((MC)) $((START)) $((STEP));
    START=$((START+STEP))

    #sleep 0.2
done;

