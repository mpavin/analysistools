#include <vector>
#include <string>
#include <iostream>
#include "Functions.h"
#include <TH1D.h>
#include <TH2D.h>
#include <TFile.h>
#include <TChain.h>
#include <TTree.h>
#include <TMath.h>
#include <math.h>

using namespace std;


int main(){

	int data = 1;
	double scale;
	string inputFile;
	string add;
	
	if(data==1){
		scale = 1;
		inputFile = "listData.txt";
		add = "data_";	
	}
	else{

		//scale = 1;
		inputFile = "listMC.txt";
		add = "mc_";			
	}
	TChain chain("Data");
	
	string outName = add + "clusters.root";
	TFile outFile(outName.c_str(), "RECREATE");
	ifstream ifs;
	string line;
	ifs.open(inputFile.c_str());
		
	if (!ifs.is_open()){
		cerr << "[ReadInputListFile] Could not open infile. Exiting..." << endl;			
		exit (101);
	}
    
	while (getline(ifs, line)){
		if(line.at(0)=='#')
			continue;
		chain.Add(line.c_str());
	}
	ifs.close();		



	BeamA beamD;
	double x;
	double y;
	double z;
	double xE;
	double yE; 
	double px;
	double py;
	double pz;
	double distTarg;
	int q;
	int topology;
	int nTOF;
	int tofScId;
	vector<double> *xCluster = NULL;
	vector<double> *yCluster = NULL;
	vector<double> *zCluster = NULL;
	vector<double> *xExtrap = NULL;
	vector<double> *yExtrap = NULL;
	vector<double> *zExtrap = NULL;
	vector<int> *tpc = NULL;

	int runNumber;
	
	//TBranch *b_aData = chain.GetBranch("Beam");
 	//b_aData->SetAddress(&beamD);
	chain.SetBranchAddress("x", &x);
	chain.SetBranchAddress("xE", &xE);
	chain.SetBranchAddress("y", &y);
	//chain.SetBranchAddress("yE", &yE);
	chain.SetBranchAddress("z", &z);
	chain.SetBranchAddress("px", &px);
	chain.SetBranchAddress("py", &py);
	chain.SetBranchAddress("pz", &pz);
	chain.SetBranchAddress("distTarg", &distTarg);
	chain.SetBranchAddress("q", &q);
	chain.SetBranchAddress("topology", &topology);
	chain.SetBranchAddress("nTOF", &nTOF);
	chain.SetBranchAddress("tofScId", &tofScId);
	chain.SetBranchAddress("runNumber", &runNumber);
	chain.SetBranchAddress("xCluster", &xCluster);
	chain.SetBranchAddress("yCluster", &yCluster);
	chain.SetBranchAddress("zCluster", &zCluster);
	chain.SetBranchAddress("xExtrap", &xExtrap);
	chain.SetBranchAddress("yExtrap", &yExtrap);
	chain.SetBranchAddress("zExtrap", &zExtrap);
	chain.SetBranchAddress("tpc", &tpc);
	
	int nEntries = chain.GetEntries();
	
	double zbor[7] = {0, 18, 36, 54, 72, 89.99, 90.01};

	/*vector<TH2D> clusterxzRST;
	vector<TH2D> clusteryzRST;
	vector<TH2D> clusterxzWST;
	vector<TH2D> clusteryzWST;	
	
	vector<TH1D> clusterxRSTl;
	vector<TH1D> clusteryRSTl;
	vector<TH1D> clusterxWSTl;
	vector<TH1D> clusteryWSTl;
	
	vector<TH1D> clusterxRSTr;
	vector<TH1D> clusteryRSTr;
	vector<TH1D> clusterxWSTr;
	vector<TH1D> clusteryWSTr;	
	
	for(int i = 0; i < 7; i++){
		string namexzRST = "clusterxzRST" + ConvertToString(i+9.,0);
		string nameyzRST = "clusteryzRST" + ConvertToString(i+9.,0);
		string namexzWST = "clusterxzWST" + ConvertToString(i+9.,0);
		string nameyzWST = "clusteryzWST" + ConvertToString(i+9.,0);
		
		TH2D temp1(namexzRST.c_str(), "", 1100, -500, 600, 820, -410., 410.);
		TH2D temp2(nameyzRST.c_str(), "", 1100, -500, 600, 200, -100., 100.);
		TH2D temp3(namexzWST.c_str(), "", 1100, -500, 600, 820, -410., 410.);
		TH2D temp4(nameyzWST.c_str(), "", 1100, -500, 600, 200, -100., 100.);			
		
		clusterxzRST.push_back(temp1);	
		clusteryzRST.push_back(temp2);	
		clusterxzWST.push_back(temp3);	
		clusteryzWST.push_back(temp4);	
		
		
		string namexRSTl = "clusterxRSTl" + ConvertToString(i+9.,0);
		string nameyRSTl = "clusteryRSTl" + ConvertToString(i+9.,0);
		string namexWSTl = "clusterxWSTl" + ConvertToString(i+9.,0);
		string nameyWSTl = "clusteryWSTl" + ConvertToString(i+9.,0);
		
		TH1D temp5(namexRSTl.c_str(), "",  200, -200., 200.);
		TH1D temp6(nameyRSTl.c_str(), "", 200, -100., 100.);
		TH1D temp7(namexWSTl.c_str(), "", 200, -200., 200.);
		TH1D temp8(nameyWSTl.c_str(), "", 200, -100., 100.);			
		
		clusterxRSTl.push_back(temp5);	
		clusteryRSTl.push_back(temp6);	
		clusterxWSTl.push_back(temp7);	
		clusteryWSTl.push_back(temp8);	
		
		string namexRSTr = "clusterxRSTr" + ConvertToString(i+9.,0);
		string nameyRSTr = "clusteryRSTr" + ConvertToString(i+9.,0);
		string namexWSTr = "clusterxWSTr" + ConvertToString(i+9.,0);
		string nameyWSTr = "clusteryWSTr" + ConvertToString(i+9.,0);
		
		TH1D temp9(namexRSTr.c_str(), "",  200, -200., 200.);
		TH1D temp10(nameyRSTr.c_str(), "", 200, -100., 100.);
		TH1D temp11(namexWSTr.c_str(), "", 200, -200., 200.);
		TH1D temp12(nameyWSTr.c_str(), "", 200, -100., 100.);			
		
		clusterxRSTr.push_back(temp9);	
		clusteryRSTr.push_back(temp10);	
		clusterxWSTr.push_back(temp11);	
		clusteryWSTr.push_back(temp12);	
	}*/

	
	TH2D clustersxz("clustersxz", "", 225, -500, 600, 205, -410., 410.);
	
	cout << nEntries << endl;
	

	
	for(int i = 0; i < nEntries; ++i){
		
		chain.GetEntry(i);
		
		if(!((i+1)%10000))
			cout << "Processing event # "  << i+1 << endl;
		
		
		double zRec = z+657.51;
		double pRec = sqrt(px*px+py*py+pz*pz);
		double thetaRec = 1000*acos(pz/pRec);
				
		if(zRec < 54) continue;
		if(zRec > 90) continue;
		if(nTOF < 1) continue;
		
		if(thetaRec < 260) continue;
		if(thetaRec > 400) continue; 
		//if(pRec > 6) continue;
		//if(xCluster->at(0)*xCluster->at(xCluster->size()-1)>0) continue;
		
		/*for(int j = 0; j < tpc->size(); j++){
			clustersxz.Fill(zCluster->at(j), xCluster->at(j));
		}*/
		for(int j = 0; j < xExtrap->size(); j++){
			clustersxz.Fill(zExtrap->at(j), xExtrap->at(j));
		}
		/*bool d[2] = {false, false};
		for(int j = 0; j < tpc->size(); j++){
		

			if(px*q > 0){
				clusterxzRST.at(topology-9).Fill(zCluster->at(j), xCluster->at(j));
				clusteryzRST.at(topology-9).Fill(zCluster->at(j), yCluster->at(j));		
				
				if(tpc->at(j) == 3 && !d[0]){
					clusterxRSTl.at(topology-9).Fill(xCluster->at(j));
					clusteryRSTl.at(topology-9).Fill(yCluster->at(j));
					d[0] = true;
				}
				if(tpc->at(j) == 4 && !d[1]){
					clusterxRSTr.at(topology-9).Fill(xCluster->at(j));
					clusteryRSTr.at(topology-9).Fill(yCluster->at(j));			
					d[1] = true;
				}
			}
			else{
				clusterxzWST.at(topology-9).Fill(zCluster->at(j), xCluster->at(j));
				clusteryzWST.at(topology-9).Fill(zCluster->at(j), yCluster->at(j));				
				
				if(tpc->at(j) == 3 && !d[0]){
					clusterxWSTl.at(topology-9).Fill(xCluster->at(j));
					clusteryWSTl.at(topology-9).Fill(yCluster->at(j));			
					d[0] = true;
				}
				if(tpc->at(j) == 4 && !d[1]){
					clusterxWSTr.at(topology-9).Fill(xCluster->at(j));
					clusteryWSTr.at(topology-9).Fill(yCluster->at(j));			
					d[1] = true;
				}
			}

		}*/

	}

	outFile.cd();

	/*for(int i = 0; i < 7; i++){

		clusterxzRST.at(i).Write();	
		clusteryzRST.at(i).Write();	
		clusterxzWST.at(i).Write();	
		clusteryzWST.at(i).Write();	

		clusterxRSTl.at(i).Write();
		clusteryRSTl.at(i).Write();
		clusterxWSTl.at(i).Write();
		clusteryWSTl.at(i).Write(); 
				
		clusterxRSTr.at(i).Write();
		clusteryRSTr.at(i).Write();
		clusterxWSTr.at(i).Write();
		clusteryWSTr.at(i).Write();
		
	}*/
	clustersxz.Write();
	outFile.Close();
		
}





