/*!
* \file
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <cstdlib>
#include <stdexcept>
#include <TTree.h>
#include <TFile.h>
#include <cstdlib>
#include <evt/Event.h>
#include <evt/EventHeader.h>
#include <io/EventFileChain.h>
#include <det/TriggerConst.h>
#include <det/BPDConst.h>
#include <evt/RawEvent.h>
#include <evt/SimEvent.h>
#include "StrTools.h"
#include <boost/format.hpp>
#include "Ana.h"
#include "TrackExtrapolation.h"
#include "evt/RawEvent.h"
#include "evt/raw/TOF.h"
#include <evt/rec/Cluster.h>
using namespace std;
using namespace io;
using namespace evt;
using namespace evt::rec;
using namespace evt::sim;
using namespace evt::raw;
using namespace det;
using namespace boost;
using namespace StrTools;
/*!
 *	\fn 
 *	Function Run reads SHOE files. For this class EventFileChain is used. It needs vector containing paths to the SHOE files.
 *	This vector is obtained from the DataAnaConfig class.
*/
void Ana::Run(){
	int k = 0;
	//string spectrumFileName = fDataAnaConfig.GetOutputDir() + "/spectrum_" + GetTimeString() + ".root";
	//string cutFile = fDataAnaConfig.GetOutputDir() + "/cuts_" + GetTimeString() + ".root";
	string spectrumFileName = fDataAnaConfig.GetOutputDir() + "/spectrum.root";
	string cutFile = fDataAnaConfig.GetOutputDir() + "/cuts.root";
	
	EventFileChain eventFileChain(fDataAnaConfig.GetJob());
	Event event;

	unsigned int nEvents = 0;

	EventSelection evSel;
	TrackSelection trSel;

	TrackExtrapolation trackExtrapolation(true, 0.1, 30400.);
	TrackExtrapolation trackExtrapolation1(true, 0.1, 30400.);
	trackExtrapolation.SetTarget(fDataAnaConfig.GetTarget());
	
	//TH2D tpos("tpos", "", 200, -400, 400, 200, -90, 90);
	
	BeamA beamD;
	double x;
	double xE;
	double y;
	double yE;
	double z;
	double px;
	double py;
	double pz;
	double distTarg;
	int q;
	int nTOF;
	int tofScId;
	int topology;
	vector<double> xCluster;
	vector<double> yCluster;
	vector<double> zCluster;
	vector<double> xExtrap;
	vector<double> yExtrap;
	vector<double> zExtrap;
	vector<int> tpc;

	
	
	
	unsigned int runNumber = 0;

	TFile *spectrumFile = new TFile(spectrumFileName.c_str(), "RECREATE");
	spectrumFile->cd();
	
	fSpectrum = new TTree("Data", "Data");
	fSpectrum->SetAutoSave(-300000000);

	//fSpectrum->Branch("Beam", &(beamD.aX), "aX/D:bX:aY:bY");
	
	fSpectrum->Branch("x", &x, "x/D");
	fSpectrum->Branch("xE", &xE, "xE/D");
	fSpectrum->Branch("y", &y, "y/D");
	fSpectrum->Branch("yE", &yE, "yE/D");
	fSpectrum->Branch("z", &z, "z/D");
	fSpectrum->Branch("px", &px, "px/D");
	fSpectrum->Branch("py", &py, "py/D");
	fSpectrum->Branch("pz", &pz, "pz/D");
	fSpectrum->Branch("q", &q, "q/I");
	fSpectrum->Branch("topology", &topology, "topology/I");
	fSpectrum->Branch("distTarg", &distTarg, "distTarg/D");
	fSpectrum->Branch("nTOF", &nTOF, "nTOF/I");
	fSpectrum->Branch("tofScId", &tofScId, "tofScId/I");
	fSpectrum->Branch("xCluster", "vector<double>", &xCluster);
	fSpectrum->Branch("yCluster", "vector<double>", &yCluster);
	fSpectrum->Branch("zCluster", "vector<double>", &zCluster);
	fSpectrum->Branch("xExtrap", "vector<double>", &xExtrap);
	fSpectrum->Branch("yExtrap", "vector<double>", &yExtrap);
	fSpectrum->Branch("zExtrap", "vector<double>", &zExtrap);
	fSpectrum->Branch("tpc", "vector<int>", &tpc);
	fSpectrum->Branch("runNumber", &runNumber, "runNumber/I");
	//fSpectrum->Branch("fSpectrumStruct", &(fSpectrumStruct.x), "x/D:xE:y:yE:z:px:py:pz:dEdx:m2ToF:dEdxE:m2ToFE:scintillatorId/I:topology:q:runNumber");
	
	
	
	bool mc = false;
	if(eventFileChain.Read(event) == eSuccess){
		const SimEvent& simEvent = event.GetSimEvent();
		mc = simEvent.HasMainVertex();
	}
	else{
		cerr << __FUNCTION__ << ": Error in input files! Exiting..." << endl;
		exit(EXIT_FAILURE);
	}
	
	/*if(mc){
		fSpectrumMC = new TTree("MC", "MC");
		fSpectrumMC->SetAutoSave(-300000000);

		fSpectrumMC->Branch("Position", "vector<Position>", &posM);
		fSpectrumMC->Branch("Momentum", "vector<MomentumWE>", &momM);
		fSpectrum->Branch("q", "vector<int>", &qM);
		fSpectrumMC->Branch("TPC", "vector<TPCA>", &tpcM);	
	}*/
	
	int ntracks = 0;
	
	
	while (eventFileChain.Read(event) == eSuccess && nEvents < Ana::fMaxEv) {

	  //	  RawEvent& rawEvent = event.GetRawEvent();
	  //const evt::RawEvent::TOFList& tof = rawEvent.GetTOFs();


		bool skipRun = false;
		EventHeader &evHeader = event.GetEventHeader();

		if(evHeader.GetRunNumber() != runNumber){
			runNumber = evHeader.GetRunNumber();
			skipRun = fDataAnaConfig.SkipRun(runNumber);
		}

		if(skipRun) continue;

   		++nEvents;
    
    	if (!(nEvents%1000))
      		cout << " --->Processing event # " << nEvents << endl;

		const RecEvent& recEvent = event.GetRecEvent();

		
		if(evSel.IsEventGood(event) == false)
			continue;	

		RawEvent& rawEvent = event.GetRawEvent();
		const evt::RawEvent::TOFList& tofR = rawEvent.GetTOFs();

		if(mc){
			const SimEvent& simEvent = event.GetSimEvent();
			const evt::sim::Beam& beam = simEvent.GetBeam();
		
			beamD.aX = beam.GetMomentum().GetX()/beam.GetMomentum().GetZ();
			beamD.aY = beam.GetMomentum().GetY()/beam.GetMomentum().GetZ();
			beamD.bX = beam.GetOrigin().GetX() - beamD.aX*beam.GetOrigin().GetZ();
			beamD.bY = beam.GetOrigin().GetY() - beamD.aY*beam.GetOrigin().GetZ();		
		}
		else{
			const Fitted2DLine lineX = event.GetRecEvent().GetBeam().Get(BPDConst::eX);
    		const Fitted2DLine lineY = event.GetRecEvent().GetBeam().Get(BPDConst::eY);
    		
			beamD.aX = lineX.GetSlope();
			beamD.bX = lineX.GetIntercept();
			beamD.aY = lineY.GetSlope();
			beamD.bY = lineY.GetIntercept();   		
		}

		for (std::list<rec::Track>::const_iterator iter = recEvent.Begin<rec::Track>();
           iter != recEvent.End<rec::Track>(); ++iter)
      	{
			
			xCluster.clear();
			yCluster.clear();
			zCluster.clear();
			xExtrap.clear();
			yExtrap.clear();
			zExtrap.clear();
			tpc.clear();
			const Track& track = *iter;
			//if(track.GetNumberOfSimVertexTracks() !=1) continue;
			//if(track.GetMomentum().GetMag() < 1.5) continue;
			//if(track.GetMomentum().GetMag() > 3.5) continue;
			
			ntracks++;
			int nToF = 0;
			double tofMass = 0;
			bool hasToF = false;
			vector<int> tofSId;
			double tofE = 0;
			double dx = 100;
			AnaTrack anaTrack(trackExtrapolation);

			//if(track.GetNumberOfVertexTracks()< 1) 
			//	continue;
			
			
			
			anaTrack.SetTrack(track);
			//if(anaTrack.GetTopology() == 10) continue;
			for(evt::rec::VertexTrackIndexIterator vtxIter = track.VertexTracksBegin(); vtxIter != track.VertexTracksEnd(); vtxIter++){		
			//evt::rec::VertexTrackIndexIterator vtxIter = track.VertexTracksBegin();
				const evt::rec::VertexTrack vtxRecTr = recEvent.Get(*vtxIter);
					
				

				for (rec::TOFMassIndexIterator tofIter = vtxRecTr.TOFMassBegin();
				   		tofIter != vtxRecTr.TOFMassEnd(); ++tofIter)
			  	{
					const rec::TOFMass& tof = recEvent.Get(*tofIter);
					if(tof.GetTOFId() != TOFConst::eTOFF)
						continue;
					
					nToF++;
					hasToF = true;		
					tofSId.push_back(tof.GetScintillatorId());
					//if(tof.GetScintillatorId() == 47) cout << "radi" << endl;
					if(TMath::Abs(tof.GetPosition().GetX()-fDataAnaConfig.GetTOFFxCenter(tof.GetScintillatorId())) < dx){
						dx = TMath::Abs(tof.GetPosition().GetX()-fDataAnaConfig.GetTOFFxCenter(tof.GetScintillatorId()));
						tofMass = tof.GetSquaredMass();
					}

				}


			
			}
			
			if(hasToF){

				  //if(tofR.size() == 0) cout << "radi" << endl;
				std::sort (tofSId.begin(), tofSId.end());
				double nt = nToF;

				anaTrack.SetToFMassSquared(tofMass);
				anaTrack.SetToFScintillatorId(tofSId.at(0));
			}

			

			if(!trSel.IsTrackGood(anaTrack))
				continue;
			
			
			double theta = 1000*acos(anaTrack.GetMomentum().GetZ()/anaTrack.GetMomentum().GetMag());
			if(theta < 260) continue;
			if(theta > 400) continue;
			
			
			//if (anaTrack.GetExtrapolatedPosition().GetZ() > -657.51+54) continue;
			if(track.GetEnergyDeposit(TrackConst::eAll) > 1.30) continue;
			//if(anaTrack.GetExtrapolatedPosition().GetZ()>-657.51+18) continue;
			k++;

			
			
			
			x = anaTrack.GetExtrapolatedPosition().GetX();
			y = anaTrack.GetExtrapolatedPosition().GetY();
			z = anaTrack.GetExtrapolatedPosition().GetZ();
			
			xE = anaTrack.GetPositionErrors().at(0);
			yE = anaTrack.GetPositionErrors().at(1);
			
			distTarg = anaTrack.GetDistanceFromTarget();
			px = anaTrack.GetMomentum().GetX();
			py = anaTrack.GetMomentum().GetY();
			pz = anaTrack.GetMomentum().GetZ();
			
			nTOF = nToF;
			if(nToF > 0) {
				tofScId = tofSId.at(0);
			}
			else{
				tofScId = 0;			
			}

			q = track.GetCharge();
			topology = anaTrack.GetTopology();

			double zp = -99;
			vector<TPCCluster> clusters;

			for(evt::rec::ClusterIndexIterator clIter = track.ClustersBegin(); clIter != track.ClustersEnd(); clIter++){	
				const evt::rec::Cluster& cluster = recEvent.Get(*clIter);
				const Point& pos = cluster.GetPosition();
				TPCCluster temp(pos.GetX(), pos.GetY(), pos.GetZ(), cluster.GetPositionUncertainty( evt::rec::ClusterConst::eX), 
						cluster.GetPositionUncertainty( evt::rec::ClusterConst::eY), cluster.GetTPCId());				
				int tpcid = 0;
				switch(cluster.GetTPCId()){
					case  det::TPCConst::eVTPC1:
					{
						tpcid = 1;
						clusters.push_back(temp);
						break;
					}
					case  det::TPCConst::eVTPC2:
					{
						tpcid = 2;
						clusters.push_back(temp);
						break;
					}
					case  det::TPCConst::eMTPCL:
					{
						clusters.push_back(temp);
						tpcid = 3;
						break;
					}
					case  det::TPCConst::eMTPCR:
					{
						clusters.push_back(temp);
						tpcid = 4;
						break;
					}
					case  det::TPCConst::eGTPC:
						tpcid = 5;
						break;
					default:
						tpcid = 0;
						break;
						
				}
				
				
				if(!tpcid) continue;
				

										
				/*xCluster.push_back(cluster.GetPosition().GetX());
				yCluster.push_back(cluster.GetPosition().GetY());
				zCluster.push_back(cluster.GetPosition().GetZ());*/
				tpc.push_back(tpcid);
			}
			
			
			
			trackExtrapolation1.SetTrackParams(track);
			TrackPar &trackPar = trackExtrapolation1.GetStopTrackParam();	

			for(int i = 1; i < clusters.size(); i++){
				trackExtrapolation1.DoKalmanStep(clusters.at(i));

				xExtrap.push_back(trackPar.GetPar(0));
				yExtrap.push_back(trackPar.GetPar(1));
				zExtrap.push_back(trackPar.GetZ());
				
				if(i < clusters.size()-1){
					if(clusters.at(i+1).GetPosition(eZ) - trackPar.GetZ() > 8){
						while(trackPar.GetZ() < clusters.at(i).GetPosition(eZ)){
							trackExtrapolation1.Extrapolate(trackPar.GetZ()+4.0);
							xExtrap.push_back(trackPar.GetPar(0));
							yExtrap.push_back(trackPar.GetPar(1));
							zExtrap.push_back(trackPar.GetZ());
						}					
					}
				}
			}
				
			while(trackPar.GetZ() < 600){
				trackExtrapolation1.Extrapolate(trackPar.GetZ()+4.0);
				xExtrap.push_back(trackPar.GetPar(0));
				yExtrap.push_back(trackPar.GetPar(1));
				zExtrap.push_back(trackPar.GetZ());
			}				


			if(tpc.size()>0){
				spectrumFile->cd();
				fSpectrum->Fill();
			}
		}
			
	}

	
	cout << ntracks << endl;
	cout << nEvents << endl;
	spectrumFile->cd();
	fSpectrum->Write();
	//tpos.Write();
	spectrumFile->Close();

}


