#include <stdlib.h>
#include "PhaseSpace.h"
#include "MCCorr.h"
#include "MCCorrConfig.h"
#include <sstream>

#include <TFile.h>
#include <TChain.h>
#include <TTree.h>
#include <sstream> 
#include "StrTools.h" 
#include "TreeDataTypes.h"
#include "AnaTrack.h"
#include "PhaseSpace.h"
#include "TrackSelection.h"

using namespace StrTools;


void MCCorr::Run(){

	bool data = false;
	bool mc = false;
	

	MCCorrConfig cfg;
	PhaseSpace phaseSpace;
	TrackSelection trkSel;
	
	string name = cfg.GetOutputDir() + "MCCorr_" + ConvertToString(cfg.GetParticleId()) + "_" + GetTimeString() + ".root";
	TFile output(name.c_str(), "RECREATE");



	TTree *outTree = new TTree("Multiplicity", "Multiplicity");
	
	vector<double> binLow;
	vector<double> binHigh;
	vector<double> p;
	vector<double> pWidth;
	vector<double> nMC;
	vector<double> nMC1;
	vector<double> nMCE;
	vector<double> nMCSimTOF;
	vector<double> nMCSimTOFGenZ;
	vector<double> nSim;
	vector<double> nSimPhi;
	vector<double> nSimPhiTOF;
	vector<double> nSimPhiTOFRecZ;
	int indexZ;
	int indexTh;


	 

	TFile *mcFile = NULL;
	TTree *mcTree = NULL;
	vector<double> binLowm;
	vector<double> binHighm;
	int indexZm, indexThm, indexPm;
	double npim[2];
	double npm[2];
	double nKm[2];
	double nem[2];	

	for(int i = 0; i < 3; i++){
		binLow.push_back(0);
		binHigh.push_back(0);
		binLowm.push_back(0);
		binHighm.push_back(0);
	}

	outTree->Branch("zMin", &(binLow.at(0)), "zMin/D");
	outTree->Branch("zMax", &(binHigh.at(0)), "zMax/D");
	outTree->Branch("thetaMin", &(binLow.at(1)), "thetaMin/D");
	outTree->Branch("thetaMax", &(binHigh.at(1)), "thetaMax/D");
	outTree->Branch("indexZ", &indexZ, "indexZ/I");
	outTree->Branch("indexTh", &indexTh, "indexTh/I");
	outTree->Branch("p", "vector<double>", &p);
	outTree->Branch("pWidth", "vector<double>", &pWidth);
	outTree->Branch("nMC", "vector<double>", &nMC);
	outTree->Branch("nMC1", "vector<double>", &nMC1);
	outTree->Branch("nMCE", "vector<double>", &nMCE);
	outTree->Branch("nMCSimTOF", "vector<double>", &nMCSimTOF);
	outTree->Branch("nMCSimTOFGenZ", "vector<double>", &nMCSimTOFGenZ);
	outTree->Branch("nSim", "vector<double>", &nSim);
	outTree->Branch("nSimPhi", "vector<double>", &nSimPhi);
	outTree->Branch("nSimPhiTOF", "vector<double>", &nSimPhiTOF);
	outTree->Branch("nSimPhiTOFRecZ", "vector<double>", &nSimPhiTOFRecZ);
	
	
		

	int nEntriesm = 0;
	int nEntries = 0;

	//cout << "radi" << endl;

	mcFile = new TFile(fInputMCFile.c_str(), "READ");	
	mcTree = (TTree*) mcFile->Get("Multiplicity");

	mcTree->SetBranchAddress("zMin", &(binLowm.at(0)));

	mcTree->SetBranchAddress("zMax", &(binHighm.at(0)));
	mcTree->SetBranchAddress("thetaMin", &(binLowm.at(1)));
	mcTree->SetBranchAddress("thetaMax", &(binHighm.at(1)));
	mcTree->SetBranchAddress("pMin", &(binLowm.at(2)));
	mcTree->SetBranchAddress("pMax", &(binHighm.at(2)));
	mcTree->SetBranchAddress("indexZ", &indexZm);
	mcTree->SetBranchAddress("indexTh", &indexThm);
	mcTree->SetBranchAddress("indexP", &indexPm);
	mcTree->SetBranchAddress("npi", &(npim[0]));
	mcTree->SetBranchAddress("npiE", &(npim[1]));
	mcTree->SetBranchAddress("np", &(npm[0]));
	mcTree->SetBranchAddress("npE", &(npm[1]));
	mcTree->SetBranchAddress("nK", &(nKm[0]));
	mcTree->SetBranchAddress("nKE", &(nKm[1]));
	mcTree->SetBranchAddress("ne", &(nem[0]));
	mcTree->SetBranchAddress("neE", &(nem[1]));
	nEntriesm = mcTree->GetEntries();
	nEntries = nEntriesm;

	

	
	TChain mcChain("SimData");
	TChain dChain("Data");
	for(int i = 0; i < cfg.GetMCJob().size(); i++){
		mcChain.Add(cfg.GetMCJob().at(i).c_str());

	}
	for(int i = 0; i < cfg.GetDataJob().size(); i++){
		
		dChain.Add(cfg.GetDataJob().at(i).c_str());
	}
		
	BeamA beam;
	vector<double> *z = NULL;
	vector<double> *theta = NULL;
	vector<double> *phi = NULL;
	vector<double> *pSim = NULL;
	vector<double> *pxSim = NULL;
	vector<int> *q = NULL;
	vector<int> *pdgId = NULL;
	vector<int> *iSimZ = NULL;
	vector<int> *iSimZRec = NULL;
	vector<int> *iSimTh = NULL;
	vector<int> *iSimThRec = NULL;
	vector<int> *iSimP = NULL;
	vector<int> *process = NULL;
	vector<int> *hasTOF = NULL;
	vector<int> *nVTPC1 = NULL;
	vector<int> *nVTPC2 = NULL;
	vector<int> *nGTPC = NULL;
	vector<int> *nMTPC = NULL;
	TBranch *b_aData = mcChain.GetBranch("Beam");
 	b_aData->SetAddress(&beam);
	mcChain.SetBranchAddress("z", &z);
	mcChain.SetBranchAddress("theta" , &theta);
	mcChain.SetBranchAddress("phi" , &phi);
	mcChain.SetBranchAddress("p", &pSim);
	mcChain.SetBranchAddress("px", &pxSim);
	mcChain.SetBranchAddress("q", &q);
	mcChain.SetBranchAddress("pdgId", &pdgId);
	mcChain.SetBranchAddress("indexZ", &iSimZ);
	mcChain.SetBranchAddress("indexZRec", &iSimZRec);
	mcChain.SetBranchAddress("hasTOF", &hasTOF);
	mcChain.SetBranchAddress("indexTh", &iSimTh);
	mcChain.SetBranchAddress("indexThRec", &iSimThRec);
	mcChain.SetBranchAddress("indexP", &iSimP);
	mcChain.SetBranchAddress("process", &process);
	mcChain.SetBranchAddress("nVTPC1", &nVTPC1);
	mcChain.SetBranchAddress("nVTPC2", &nVTPC2);
	mcChain.SetBranchAddress("nGTPC", &nGTPC);
	mcChain.SetBranchAddress("nMTPC", &nMTPC);
	int nEntriesSim = mcChain.GetEntries();
	int nEntriesData = dChain.GetEntries();


	vector<double> *xd = NULL;
	vector<double> *xEd = NULL;
	vector<double> *yd = NULL;
	vector<double> *yEd = NULL;
	vector<double> *zd = NULL;
	vector<double> *pxd = NULL;
	vector<double> *pyd = NULL;
	vector<double> *pzd = NULL;
	vector<double> *distTargd = NULL;
	vector<double> *zStartd = NULL;
	vector<double> *zLastd = NULL;
	vector<double> *dEdxd = NULL;
	vector<int> *qDd = NULL;
	vector<int> *nVTPC1d = NULL;
	vector<int> *nVTPC2d = NULL;
	vector<int> *nGTPCd = NULL;
	vector<int> *nMTPCd = NULL;
	vector<int> *topologyd = NULL;
	vector<int> *nTOFd = NULL;
	vector<double> *m2TOFd = NULL;	
	vector<int> *tofScIdd = NULL;
	int runNumberd;
	

	vector<int> *pdgIdd = NULL;
	vector<int> *qSimd = NULL;
	vector<int> *processd = NULL;
	vector<double> *pxSimd = NULL;
	vector<double> *pySimd = NULL;
	vector<double> *pzSimd = NULL;
	vector<double> *xSimd = NULL;
	vector<double> *ySimd = NULL;
	vector<double> *zSimd = NULL;
	
	dChain.SetBranchAddress("x", &xd);
	dChain.SetBranchAddress("xE", &xEd);
	dChain.SetBranchAddress("y", &yd);
	dChain.SetBranchAddress("yE", &yEd);
	dChain.SetBranchAddress("z", &zd);
	dChain.SetBranchAddress("px", &pxd);
	dChain.SetBranchAddress("py", &pyd);
	dChain.SetBranchAddress("pz", &pzd);
	dChain.SetBranchAddress("distTarg", &distTargd);
	dChain.SetBranchAddress("zStart", &zStartd);
	dChain.SetBranchAddress("zLast", &zLastd);
	dChain.SetBranchAddress("dEdx", &dEdxd);
	dChain.SetBranchAddress("q", &qDd);
	dChain.SetBranchAddress("nVTPC1", &nVTPC1d);
	dChain.SetBranchAddress("nVTPC2", &nVTPC2d);
	dChain.SetBranchAddress("nGTPC", &nGTPCd);
	dChain.SetBranchAddress("nMTPC", &nMTPCd);
	dChain.SetBranchAddress("topology", &topologyd);
	dChain.SetBranchAddress("nTOF", &nTOFd);
	dChain.SetBranchAddress("m2TOF", &m2TOFd);
	dChain.SetBranchAddress("tofScId", &tofScIdd);
	dChain.SetBranchAddress("runNumber", &runNumberd);
	dChain.SetBranchAddress("pdgId", &pdgIdd);
	dChain.SetBranchAddress("pxSim", &pxSimd);
	dChain.SetBranchAddress("pySim", &pySimd);
	dChain.SetBranchAddress("pzSim", &pzSimd);
	dChain.SetBranchAddress("xSim", &xSimd);
	dChain.SetBranchAddress("ySim", &ySimd);
	dChain.SetBranchAddress("zSim", &zSimd);
	dChain.SetBranchAddress("qSim", &qSimd);
	
	
	dChain.SetBranchAddress("process", &processd);


	vector<double> simMult;
	vector<double> simMult1;
	vector<double> simMult2;
	vector<double> simMult3;
	vector<double> recMult;
	vector<double> nMC1Mult;
	vector<double> recMult1;
	for(int i = 0; i < nEntries; i++){
		simMult.push_back(0);
		simMult1.push_back(0);
		simMult2.push_back(0);
		simMult3.push_back(0);
		recMult.push_back(0);
		recMult1.push_back(0);
		nMC1Mult.push_back(0);
	}
	

	vector<vector<int> > binIds;

	for(int k = 0; k < nEntries; k++){

		mcTree->GetEntry(k);
		vector<int> temp;
		temp.push_back(k);
		temp.push_back(indexZm);
		temp.push_back(indexThm);
		temp.push_back(indexPm);	
		binIds.push_back(temp);		
				
	}
	
	//nEntriesSim = 1;
	for(int i = 0; i < nEntriesSim; i++){
	//for(int i = 0; i < 10000; i++){
		mcChain.GetEntry(i);
		if(!((i+1)%100000))
			cout << "Processing MC event # " << i+1 << endl;
			
		for(int j = 0; j < z->size(); j++){

			if(pdgId->at(j) != cfg.GetParticleId()) continue;
			if(process->at(j) != 13) continue;
			if(iSimZ->at(j) < 1) continue;
			if(iSimTh->at(j) < 1) continue;
			if(iSimP->at(j) < 1) continue;
			/*if(cfg.IsRST()){
					if(q->at(j)*pxSim->at(j) < 0) continue;
			}
			else{
				if(q->at(j)*pxSim->at(j) >0 ) continue;
			}*/
			
			for(int k = 0; k < binIds.size(); k++){
				if(binIds.at(k).at(1)!=iSimZ->at(j)) continue;
				if(binIds.at(k).at(2)!=iSimTh->at(j)) continue;
				if(binIds.at(k).at(3)!=iSimP->at(j)) continue;

				simMult.at(binIds.at(k).at(0)) = simMult.at(binIds.at(k).at(0)) + 1;
				
				if(theta->at(j) >= 0 && theta->at(j) < 20){
					if( (phi->at(j) < -70 || phi->at(j) > 70) && (phi->at(j) < 110 || phi->at(j) > 250)) break;
				}
				else if (theta->at(j) >= 20 && theta->at(j) < 40){
					if( (phi->at(j) < -60 || phi->at(j) > 60) && (phi->at(j) < 140 || phi->at(j) > 220)) break;
				}
				else if (theta->at(j) >= 40 && theta->at(j) < 60){
					if( (phi->at(j) < -45 || phi->at(j) > 35) && (phi->at(j) < 145 || phi->at(j) > 225)) break;
				}
				else if (theta->at(j) >= 60 && theta->at(j) < 80){
					if( (phi->at(j) < -30 || phi->at(j) > 30) && (phi->at(j) < 150 || phi->at(j) > 210)) break;
				}
				else if (theta->at(j) >= 80 && theta->at(j) < 100){
					if( (phi->at(j) < -20 || phi->at(j) > 20) && (phi->at(j) < 160 || phi->at(j) > 200)) break;
				}	
				else if (theta->at(j) >= 100 && theta->at(j) < 120){
					if( (phi->at(j) < -17 || phi->at(j) > 17) && (phi->at(j) < 163 || phi->at(j) > 197)) break;
				}
				else if (theta->at(j) >= 120 && theta->at(j) < 140){
					if( (phi->at(j) < -15 || phi->at(j) > 15) && (phi->at(j) < 165 || phi->at(j) > 195)) break;
				}			
				else if (theta->at(j) >= 140 && theta->at(j) < 160){
					if( (phi->at(j) < -13 || phi->at(j) > 13) && (phi->at(j) < 167 || phi->at(j) > 193)) break;
				}	
				else if (theta->at(j) >= 160 && theta->at(j) < 200){
					if( (phi->at(j) < -12 || phi->at(j) > 12) && (phi->at(j) < 168 || phi->at(j) > 192)) break;
				}	
				else if (theta->at(j) >= 200 && theta->at(j) < 240){
					if( (phi->at(j) < -11 || phi->at(j) > 11) && (phi->at(j) < 169 || phi->at(j) > 191)) break;
				}		
				else if (theta->at(j) >= 240 && theta->at(j) < 600){
					if( (phi->at(j) < -10 || phi->at(j) > 10) && (phi->at(j) < 170 || phi->at(j) > 190)) break;
				}	
				
				simMult1.at(binIds.at(k).at(0)) = simMult1.at(binIds.at(k).at(0)) + 1;
				if(hasTOF->at(j) == 0) break;
				int top = 0;
				if(nVTPC1->at(j) > 0) top+=1;
				if(nVTPC2->at(j) > 0) top+=2;
				if(nGTPC->at(j) > 0) top+=4;
				if(nMTPC->at(j) > 0) top+=8;
				
				if(top == 10 || top == 13) break;
				if(top == 12){
				  //if(pxSim->at(j)*q->at(j) < 0) break;
					if(nGTPC->at(j) < 6) break;
					if(nMTPC->at(j) < 60) break;
				}
				else if (top == 9){
					if(nVTPC1->at(j) + nVTPC2->at(j) < 30) break;
					if(nMTPC->at(j) < 30) break;				
				}
				else if (top == 11){
					if(nVTPC1->at(j) + nVTPC2->at(j) < 30) break;
					if(nMTPC->at(j) < 30) break;				
				}
				else if (top == 14){
				  //if(pxSim->at(j)*q->at(j) < 0 && theta->at(j) > 40) break;
					if(nGTPC->at(j) < 4) break;
					if(nVTPC1->at(j) + nVTPC2->at(j) < 30) break;
					if(nMTPC->at(j) < 30) break;				
				}
				else if(top == 15){
					if(pxSim->at(j)*q->at(j) < 0 && theta->at(j) > 40) break;
					if(nGTPC->at(j) < 4) break;
					if(nVTPC1->at(j) + nVTPC2->at(j) < 30) break;
					if(nMTPC->at(j) < 30) break;						
					
				}

				simMult2.at(binIds.at(k).at(0)) = simMult2.at(binIds.at(k).at(0)) + 1;
				
				if(iSimZ->at(j) != iSimZRec->at(j)) break;
				if(iSimTh->at(j) != iSimThRec->at(j)) break;
				simMult3.at(binIds.at(k).at(0)) = simMult3.at(binIds.at(k).at(0)) + 1;
				
				break;
			}
			
		}
	}
	
	cout << "************************************************" << endl;
	
	for(int i = 0; i < nEntriesData; i++){
	//for(int i = 0; i < 1000000; i++){
		if(!((i+1)%100000))
			cout << "Processing Data event # " << i+1 << endl;
		dChain.GetEntry(i);
		
		for(int j = 0; j < xd->size(); j++){
		
			
			switch(cfg.GetParticleId()){
				case 211:
					if(pdgIdd->at(j) != 211 && pdgIdd->at(j) != -13) continue;
					break;
				case -211:
					if(pdgIdd->at(j) != -211 && pdgIdd->at(j) != 13) continue;
					break;
				default:
					continue;
					break;
			}

			
			
			//if(process->at(j)!=13 )cout << process->at(j) << endl;
			NumberOfClusters nClusters;
			nClusters.VTPC1 = nVTPC1d->at(j);
			nClusters.VTPC2 = nVTPC2d->at(j);
			nClusters.GTPC = nGTPCd->at(j);
			nClusters.MTPC = nMTPCd->at(j);
			
			Point pos(xd->at(j), yd->at(j), zd->at(j));
			Vector mom(pxd->at(j), pyd->at(j), pzd->at(j));
			
			AnaTrack anaTrack;
			anaTrack.SetExtrapolatedPosition(pos);
			anaTrack.SetMomentum(mom);
			anaTrack.SetNumberOfClusters(nClusters);
			anaTrack.SetDistanceFromTarget(distTargd->at(j));
			anaTrack.SetToFMassSquared(m2TOFd->at(j));
			anaTrack.SetToFScintillatorId(tofScIdd->at(j));
			anaTrack.SetTopology(topologyd->at(j));
			anaTrack.SetCharge(qDd->at(j));
			anaTrack.SetPositionErrors(xEd->at(j), yEd->at(j));
			anaTrack.SetLastZ(zLastd->at(j));
			if(!trkSel.IsTrackGood(anaTrack))
				continue;	
				
			if(topologyd->at(j) == 12){
			  //	if(pxd->at(j)*qDd->at(j) < 0) continue;
			}
			double pVal = sqrt(pxd->at(j)*pxd->at(j) + pyd->at(j)*pyd->at(j) + pzd->at(j)*pzd->at(j));
			double thetaVal = 1000*acos(pzd->at(j)/pVal);
			double pValSim = sqrt(pxSimd->at(j)*pxSimd->at(j)+pySimd->at(j)*pySimd->at(j)+pzSimd->at(j)*pzSimd->at(j));
			double thetaValSim = 1000*acos(pzSimd->at(j)/pValSim);
			/*if(topologyd->at(j) == 14){
				if(thetaVal > 40){
					if(pxd->at(j)*qDd->at(j) < 0) continue;
				}
			}*/
			//cout << "radi" << endl;
			double zVal = zd->at(j) - cfg.GetTargetZ();
			double zValSim = zSimd->at(j) - cfg.GetTargetZ();
			
			
			

			int nBinsz = phaseSpace.GetDimBins(1);	
			int iZ = -99;
			int iZSim = -99;
			for(int k=0; k < nBinsz; k++){
				Bin& bin = phaseSpace.GetBin(k+1, 1, 1);
				if(bin.GetLowValue(1) > zVal || zVal >= bin.GetHighValue(1)) continue;
				iZ = k+1;		
			}
			if(iZ < 1) continue;
			for(int k=0; k < nBinsz; k++){
				Bin& bin = phaseSpace.GetBin(k+1, 1, 1);
				if(bin.GetLowValue(1) > zValSim || zValSim >= bin.GetHighValue(1)) continue;
				iZSim = k+1;		
			}			
			int nBinsth = phaseSpace.GetDimBins(2, iZ);	
			int iTh = -99;
			for(int k=0; k < nBinsth; k++){
				Bin& bin = phaseSpace.GetBin(iZ, k+1, 1);
				if(bin.GetLowValue(2) > thetaVal || thetaVal >= bin.GetHighValue(2)) continue;
				iTh = k+1;					
			}
			int iThSim = -99;
			if(iZSim > 0){
			  int nBinsthSim = phaseSpace.GetDimBins(2, iZSim);
			  for(int k=0; k < nBinsthSim; k++){
			    Bin& bin = phaseSpace.GetBin(iZSim, k+1, 1);
			    if(bin.GetLowValue(2) > thetaValSim || thetaValSim >= bin.GetHighValue(2)) continue;
			    iThSim = k+1;
			  }
			}
			if(iTh < 1) continue;	
			
			int nBinsp = phaseSpace.GetDimBins(3, iZ, iTh);	
			int iP = -99;
			for(int k=0; k < nBinsp; k++){
				Bin& bin = phaseSpace.GetBin(iZ, iTh, k+1);
				if(bin.GetLowValue(3) > pVal || pVal >= bin.GetHighValue(3)) continue;
				iP = k+1;		
			}	
			
                        int iPSim = -99;
			if(iZSim > 0 && iThSim > 0){
			  int nBinspSim = phaseSpace.GetDimBins(3, iZSim, iThSim);
			  for(int k=0; k < nBinspSim; k++){
			    Bin& bin = phaseSpace.GetBin(iZSim, iThSim, k+1);
			    if(bin.GetLowValue(3) > pValSim || pValSim >= bin.GetHighValue(3)) continue;
			    iPSim = k+1;
			  }
			}
			
			if(iP < 1) continue;
			
			
			for(int k = 0; k < binIds.size(); k++){
				if(binIds.at(k).at(1)!=iZ) continue;
				if(binIds.at(k).at(2)!=iTh) continue;
				if(binIds.at(k).at(3)!=iP) continue;	
				
				nMC1Mult.at(binIds.at(k).at(0)) = nMC1Mult.at(binIds.at(k).at(0)) + 1;
				//cout << pdgIdd->at(j) << endl;
				
				if(process->at(j) != 13 || iZSim<0) break;
				if(pdgIdd->at(j) != cfg.GetParticleId()) break;
				recMult.at(binIds.at(k).at(0)) = recMult.at(binIds.at(k).at(0)) + 1;
				
				/*double d = sqrt((xSim->at(j)-0.2391)*(xSim->at(j)-0.2391) + (ySim->at(j)-0.1258)*(ySim->at(j)-0.1258));
				
				if((d < 1.31) && (zSim->at(j) > -657.51) && (zSim->at(j) < -567.50)){
					
				}*/
				if(iZ != iZSim)	break;
				if(iTh != iThSim) break;
				//if(iPSim < 0) break;
				recMult1.at(binIds.at(k).at(0)) = recMult1.at(binIds.at(k).at(0)) + 1;
				
				break;
			}
			
								
		}
	}
	int pdg = abs(cfg.GetParticleId());

	int thetaOld = 0; 
	


	mcTree->GetEntry(0);
	thetaOld = indexThm;	
	for(int j = 0; j < 3; j++){
		binLow.at(j) = binLowm.at(j);
		binHigh.at(j) = binHighm.at(j);
	}
	indexZ = indexZm;
	indexTh = indexThm;

	
	
	
	for(int i = 0; i < nEntries; i++){
	
		cout << "Processing bin # " << i+1 << endl;

		mcTree->GetEntry(i);	
	


		if(thetaOld !=indexThm){
			thetaOld = indexThm;
			//output.cd();
			outTree->Fill();
			nMC.clear();
			nMC1.clear();
			nMCE.clear();
			nSim.clear();
			nSimPhi.clear();
			nSimPhiTOF.clear();
			nMCSimTOF.clear();
			nMCSimTOFGenZ.clear();
			nSimPhiTOFRecZ.clear();
			p.clear();
			pWidth.clear();

			for(int j = 0; j < 3; j++){
				binLow.at(j) = binLowm.at(j);
				binHigh.at(j) = binHighm.at(j);
			}
			indexZ = indexZm;
			indexTh = indexThm;
		}

		p.push_back((binLowm.at(2)+binHighm.at(2))/2.);
		pWidth.push_back((binHighm.at(2)-binLowm.at(2))/2.);		


		
		
		switch(pdg){
			case 11:
			{

				nMC.push_back(nem[0]);
				nMCE.push_back(nem[1]);
				break;

			}
			case 211:
			{

				nMC.push_back(npim[0]);
				nMCE.push_back(npim[1]);
				break;
			}
			case 321:
			{

				nMC.push_back(nKm[0]);
				nMCE.push_back(nKm[1]);	
				break;
			}
			case 2212:
			{

				nMC.push_back(npm[0]);
				nMCE.push_back(npm[1]);
				break;
			}
		}
		
		cout << indexZ << " "  << indexTh << " "  << (binLowm.at(2)+binHighm.at(2))/2. << " " << recMult.at(i)/nMC1Mult.at(i) << endl;
		nSim.push_back(simMult.at(i));
		nSimPhi.push_back(simMult1.at(i));
		nSimPhiTOF.push_back(simMult2.at(i));
		nSimPhiTOFRecZ.push_back(simMult3.at(i));
		nMC1.push_back(nMC1Mult.at(i));
		nMCSimTOF.push_back(recMult.at(i));
		nMCSimTOFGenZ.push_back(recMult1.at(i));

	}
	output.cd();
	outTree->Write();
	output.Close();
}
