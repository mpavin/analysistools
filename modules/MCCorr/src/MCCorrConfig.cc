#include <stdlib.h>
#include "MCCorrConfig.h"
#include <fstream>
#include <fwk/CentralConfig.h>
#include <utl/Branch.h>
#include <utl/BranchIterator.h>
#include "CConfig.h"
#include <math.h>  

using namespace std;
using namespace fwk;
using namespace utl; 

void MCCorrConfig::Read(){
	Branch spectrumPlot = cConfig.GetTopBranch("MCCorr");

	if(spectrumPlot.GetName() != "MCCorr"){
		cout << __FUNCTION__ << ": Wrong MCCorr config file! Top branch name doesn't match! Please check your configuration files! Exiting..." << endl;
		exit (EXIT_FAILURE);
	}

	fIsRST = true;
	fMCList = "";
	fPDGId = 211;
	for(BranchIterator it = spectrumPlot.ChildrenBegin(); it != spectrumPlot.ChildrenEnd(); ++it){
		Branch child = *it;
		
		if(child.GetName() == "output"){
			child.GetData(fOutput);
		}
		
		else if(child.GetName() == "mcList"){
			child.GetData(fMCList);
		}
		else if(child.GetName() == "dataList"){
			child.GetData(fDataList);
		}
		else if(child.GetName() == "isRST"){
			child.GetData(fIsRST);
		}
		else if(child.GetName() == "pdgId"){
			child.GetData(fPDGId);
		}	
		else if(child.GetName() == "targetZ"){
			child.GetData(fTargetZ);
		}		
		
	}

}

vector<string>& MCCorrConfig::GetMCJob(){

	if(fMCJob.size() == 0)
		ReadInputList(fMCList, fMCJob);

	return fMCJob;
}

vector<string>& MCCorrConfig::GetDataJob(){

	if(fDataJob.size() == 0)
		ReadInputList(fDataList, fDataJob);

	return fDataJob;
}

void MCCorrConfig::ReadInputList(string list, vector<string> &job){

	if (list != " " ){
		ifstream ifs;
		string line;
		ifs.open(list.c_str());
		
		if (!ifs.is_open()){
			cerr << "[ReadInputListFile] Could not open infile. Exiting..." << endl;			
			exit (101);
		}
    
		while (getline(ifs, line)){
			if(line.at(0)=='#')
				continue;
			job.push_back(line);
		}
		ifs.close();	
	
	}
	else cout << "WARNING: Empty file list! Jobs not created!" << endl;
}
