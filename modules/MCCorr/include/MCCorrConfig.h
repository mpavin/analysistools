#ifndef MCCORRCONFIG_H
#define MCCORRCONFIG_H
#include <iostream>
#include <vector>
#include <string>

#include <vector>



using namespace std;

class MCCorrConfig{

	public:
		MCCorrConfig(){Read();}

		string GetOutputDir(){return fOutput;}
		int GetParticleId(){return fPDGId;}
		bool IsRST(){return fIsRST;}
		vector<string>& GetMCJob();
		vector<string>& GetDataJob();
		string GetMCList(){return fMCList;}
		double GetTargetZ(){return fTargetZ;}
		
		void SetOutputDir(string value){fOutput = value;}
		void SetParticleId(int id){fPDGId = id;}
		void SetIsRST(bool val){fIsRST = val;}
		void SetMCList(string list){fMCList = list;}
		void SetTargetZ(double z) {fTargetZ = z;}
	private:

		void Read();
		void ReadInputList(string list, vector<string> &job);
		string fOutput;
		int fPDGId;
		string fMCList;
		string fDataList;
		bool fIsRST;
		vector<string> fMCJob;
		vector<string> fDataJob;
		double fTargetZ;
};

#endif
