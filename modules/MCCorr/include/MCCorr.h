#ifndef MCCORR_H
#define MCCORR_H


#include <iostream>
#include <vector>
#include <string>


using namespace std;

class MCCorr{

	public:
		MCCorr(string inputMC): fInputMCFile(inputMC) {}
		
		//void SetInputDataFile(string inputFile){fInputDataFile = inputFile;}
		void SetInputMCFile(string inputFile){fInputMCFile = inputFile;}
		
		//string GetInputDataFile(){return fInputDataFile;}
		string GetInputMCFile(){return fInputMCFile;}
		void Run();
		
	private:
		

		string fInputMCFile;

		
};
#endif
