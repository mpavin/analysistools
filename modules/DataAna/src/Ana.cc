/*!
* \file
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <cstdlib>
#include <stdexcept>
#include <TTree.h>
#include <TFile.h>
#include <cstdlib>
#include <evt/Event.h>
#include <evt/EventHeader.h>
#include <io/EventFileChain.h>
#include <det/TriggerConst.h>
#include <det/BPDConst.h>
#include <evt/RawEvent.h>
#include <evt/SimEvent.h>
#include "StrTools.h"
#include <boost/format.hpp>
#include "Ana.h"
#include "TrackExtrapolation.h"
#include "evt/RawEvent.h"
#include "evt/raw/TOF.h"
#include "TrackTools.h"

using namespace std;
using namespace io;
using namespace evt;
using namespace evt::rec;
using namespace evt::sim;
using namespace evt::raw;
using namespace det;
using namespace boost;
using namespace StrTools;
using namespace TrackTools;
/*!
 *	\fn 
 *	Function Run reads SHOE files. For this class EventFileChain is used. It needs vector containing paths to the SHOE files.
 *	This vector is obtained from the DataAnaConfig class.
*/
void Ana::Run(){
	int k = 0;
	//string spectrumFileName = fDataAnaConfig.GetOutputDir() + "/spectrum_" + GetTimeString() + ".root";
	//string cutFile = fDataAnaConfig.GetOutputDir() + "/cuts_" + GetTimeString() + ".root";
	string spectrumFileName = fDataAnaConfig.GetOutputDir() + "/spectrum.root";
	string cutFile = fDataAnaConfig.GetOutputDir() + "/cuts.root";
	
	EventFileChain eventFileChain(fDataAnaConfig.GetJob());
	Event event;

	unsigned int nEvents = 0;

	EventSelection evSel;
	TrackSelection trSel;

	TrackExtrapolation trackExtrapolation(true, 0.05, 30400.);
	trackExtrapolation.SetTarget(fDataAnaConfig.GetTarget());
	cout << fDataAnaConfig.GetTarget().GetUpstreamPosition().GetX() << endl;
	//TH2D tpos("tpos", "", 200, -400, 400, 200, -90, 90);
	TH1D tops("tops", "", 16, 0, 16);
	BeamA beamD;
	vector<double> x;
	vector<double> xE;
	vector<double> y;
	vector<double> yE;
	vector<double> z;
	vector<double> px;
	vector<double> py;
	vector<double> pz;
	vector<double> distTarg;
	vector<double> zStart;
	vector<double> zLast;
	vector<double> xLast;
	vector<double> dEdx;
	vector<int> qD;
	vector<int> nVTPC1;
	vector<int> nVTPC2;
	vector<int> nGTPC;
	vector<int> nMTPC;
	vector<int> topology;
	vector<int> nTOF;
	vector<double> m2TOF;	
	vector<int> tofScId;
	
	vector<int> pdgId;
	vector<double> pxSim;
	vector<double> pzSim;
	vector<double> pySim;
	vector<double> xSim;
	vector<double> ySim;
	vector<double> zSim;
	vector<int> qSim;
	vector<int> process;
	unsigned int runNumber = 0;

	TFile *spectrumFile = new TFile(spectrumFileName.c_str(), "RECREATE");
	spectrumFile->cd();
	
	fSpectrum = new TTree("Data", "Data");
	fSpectrum->SetAutoSave(-300000000);

	fSpectrum->Branch("Beam", &(beamD.aX), "aX/D:aY:bX:bY");
	fSpectrum->Branch("x", "vector<double>", &x);
	fSpectrum->Branch("xE", "vector<double>", &xE);
	fSpectrum->Branch("y", "vector<double>", &y);
	fSpectrum->Branch("yE", "vector<double>", &yE);
	fSpectrum->Branch("z", "vector<double>", &z);
	fSpectrum->Branch("px", "vector<double>", &px);
	fSpectrum->Branch("py", "vector<double>", &py);
	fSpectrum->Branch("pz", "vector<double>", &pz);
	fSpectrum->Branch("distTarg", "vector<double>", &distTarg);
	fSpectrum->Branch("q", "vector<int>", &qD);
	fSpectrum->Branch("zStart", "vector<double>", &zStart);
	fSpectrum->Branch("xLast", "vector<double>", &xLast);
	fSpectrum->Branch("zLast", "vector<double>", &zLast);
	fSpectrum->Branch("nVTPC1", "vector<int>", &nVTPC1);
	fSpectrum->Branch("nVTPC2", "vector<int>", &nVTPC2);
	fSpectrum->Branch("nGTPC", "vector<int>", &nGTPC);
	fSpectrum->Branch("nMTPC", "vector<int>", &nMTPC);
	fSpectrum->Branch("topology", "vector<int>", &topology);
	fSpectrum->Branch("dEdx", "vector<double>", &dEdx);
	fSpectrum->Branch("nTOF", "vector<int>", &nTOF);
	fSpectrum->Branch("m2TOF", "vector<double>", &m2TOF);
	fSpectrum->Branch("tofScId", "vector<int>", &tofScId);
	fSpectrum->Branch("runNumber", &runNumber, "runNumber/I");
	//fSpectrum->Branch("fSpectrumStruct", &(fSpectrumStruct.x), "x/D:xE:y:yE:z:px:py:pz:dEdx:m2ToF:dEdxE:m2ToFE:scintillatorId/I:topology:q:runNumber");
	
	
	
	bool mc = false;
	if(eventFileChain.Read(event) == eSuccess){
		const SimEvent& simEvent = event.GetSimEvent();
		mc = simEvent.HasMainVertex();
	}
	else{
		cerr << __FUNCTION__ << ": Error in input files! Exiting..." << endl;
		exit(EXIT_FAILURE);
	}
	
	if(mc){
		fSpectrum->Branch("pdgId", "vector<int>", &pdgId);
		fSpectrum->Branch("pxSim", "vector<double>", &pxSim);
		fSpectrum->Branch("pxSim", "vector<double>", &pxSim);
		fSpectrum->Branch("pySim", "vector<double>", &pySim);
		fSpectrum->Branch("pzSim", "vector<double>", &pzSim);
		fSpectrum->Branch("xSim", "vector<double>", &xSim);
		fSpectrum->Branch("ySim", "vector<double>", &ySim);
		fSpectrum->Branch("zSim", "vector<double>", &zSim);
		fSpectrum->Branch("qSim", "vector<int>", &qSim);
		fSpectrum->Branch("process", "vector<int>", &process);
	}
	/*if(mc){
		fSpectrumMC = new TTree("MC", "MC");
		fSpectrumMC->SetAutoSave(-300000000);

		fSpectrumMC->Branch("Position", "vector<Position>", &posM);
		fSpectrumMC->Branch("Momentum", "vector<MomentumWE>", &momM);
		fSpectrum->Branch("q", "vector<int>", &qM);
		fSpectrumMC->Branch("TPC", "vector<TPCA>", &tpcM);	
	}*/
	
	int ntracks = 0;
	
	/*double xte[7] = {0, 0, 0, 0, 0, 0, 0};
	double yte[7] = {0, 0, 0, 0, 0, 0, 0};
	double ne[7] = {0, 0, 0, 0, 0, 0, 0};*/
	while (eventFileChain.Read(event) == eSuccess && nEvents < Ana::fMaxEv) {

	  //	  RawEvent& rawEvent = event.GetRawEvent();
	  //const evt::RawEvent::TOFList& tof = rawEvent.GetTOFs();

		x.clear();
		xE.clear();
		y.clear();
		yE.clear();
		z.clear();
		
		px.clear();
		py.clear();
		pz.clear();
		
		zStart.clear();
		zLast.clear();
		xLast.clear();
		qD.clear();
		distTarg.clear();
		
		nTOF.clear();
		m2TOF.clear();
		tofScId.clear();
		dEdx.clear();
		nVTPC1.clear();
		nVTPC2.clear();
		nGTPC.clear();
		nMTPC.clear();
		topology.clear();
		if(mc){
			pdgId.clear();
			pxSim.clear();
			pySim.clear();
			pzSim.clear();
			xSim.clear();
			ySim.clear();
			zSim.clear();
			qSim.clear();
			process.clear();
		}
		bool skipRun = false;
		EventHeader &evHeader = event.GetEventHeader();

		if(evHeader.GetRunNumber() != runNumber){
			runNumber = evHeader.GetRunNumber();
			skipRun = fDataAnaConfig.SkipRun(runNumber);
		}

		if(skipRun) continue;

   		++nEvents;
    
    	if (!(nEvents%1000))
      		cout << " --->Processing event # " << nEvents << endl;

		const RecEvent& recEvent = event.GetRecEvent();

		
		if(evSel.IsEventGood(event) == false)
			continue;	

		RawEvent& rawEvent = event.GetRawEvent();
		const evt::RawEvent::TOFList& tofR = rawEvent.GetTOFs();

		if(mc){
			const SimEvent& simEvent = event.GetSimEvent();
			const evt::sim::Beam& beam = simEvent.GetBeam();
		
			beamD.aX = beam.GetMomentum().GetX()/beam.GetMomentum().GetZ();
			beamD.aY = beam.GetMomentum().GetY()/beam.GetMomentum().GetZ();
			beamD.bX = beam.GetOrigin().GetX() - beamD.aX*beam.GetOrigin().GetZ();
			beamD.bY = beam.GetOrigin().GetY() - beamD.aY*beam.GetOrigin().GetZ();		
		}
		else{
			const Fitted2DLine lineX = event.GetRecEvent().GetBeam().Get(BPDConst::eX);
    		const Fitted2DLine lineY = event.GetRecEvent().GetBeam().Get(BPDConst::eY);
    		
			beamD.aX = lineX.GetSlope();
			beamD.bX = lineX.GetIntercept();
			beamD.aY = lineY.GetSlope();
			beamD.bY = lineY.GetIntercept();   		
		}

		for (std::list<rec::Track>::const_iterator iter = recEvent.Begin<rec::Track>();
           iter != recEvent.End<rec::Track>(); ++iter)
      	{
			

			const Track& track = *iter;
			//if(track.GetNumberOfSimVertexTracks() !=1) continue;
			ntracks++;
			int nToF = 0;
			double tofMass = 0;
			bool hasToF = false;
			vector<int> tofSId;
			double tofE = 0;
			double dx = 100;
			AnaTrack anaTrack(trackExtrapolation);

			if(track.GetNumberOfVertexTracks()< 1) 
				continue;
			
			
			
			anaTrack.SetTrack(track);
			tops.Fill(anaTrack.GetTopology());
			//bool bad=false;
			TrackPar &trkPar = trackExtrapolation.GetStopTrackParam();
			/*if(track.GetStatus() == 0 && trkPar.GetCov(0) > 1e-8 && anaTrack.GetTopology() > 8) {
				xte[anaTrack.GetTopology()-9] += trkPar.GetCov(0);
				yte[anaTrack.GetTopology()-9] += trkPar.GetCov(5);
				ne[anaTrack.GetTopology()-9] += 1.;
			}	*/
			//if(track.GetStatus() == 0 && track.GetMomentum().GetMag()>0.1) trkPar.Print();
			if(trkPar.GetCov(0) < 1e-5 && track.GetStatus()==0 && track.GetMomentum().GetMag()>0.1) {
				if(anaTrack.GetTopology() == 9){
					trkPar.SetCov(0, 0.000281604);
					trkPar.SetCov(5, 4.82535e-05);				
				}
				else if(anaTrack.GetTopology() == 11){
					trkPar.SetCov(0, 0.000155937);
					trkPar.SetCov(5, 3.75757e-05);				
				}
				else if(anaTrack.GetTopology() == 12){
					trkPar.SetCov(0, 0.747718);
					trkPar.SetCov(5, 0.00482059);				
				}
				else if(anaTrack.GetTopology() == 13){
					trkPar.SetCov(0, 0.000217568);
					trkPar.SetCov(5, 4.65574e-05);				
				}
				else if(anaTrack.GetTopology() == 14){
					trkPar.SetCov(0, 0.000770518);
					trkPar.SetCov(5, 0.000299262);				
				}
				else if(anaTrack.GetTopology() == 15){
					trkPar.SetCov(0, 0.000191875);
					trkPar.SetCov(5, 4.59441e-05);				
				}
				trkPar.SetCov(9, 0.0001*trkPar.GetPar(2)*trkPar.GetPar(2));
				trkPar.SetCov(12, 0.0001*trkPar.GetPar(3)*trkPar.GetPar(3));
				trkPar.SetCov(14, 0.0001*trkPar.GetPar(4)*trkPar.GetPar(4));
				//bad = true;
			//trkPar.Print();
			
			}
			/*if(mc){
				if(anaTrack.GetTopology() == 9){
					trkPar.SetCov(0, 0.795181539*trkPar.GetCov(0));
					trkPar.SetCov(5, 0.975774243*trkPar.GetCov(5));				
				}
				else if(anaTrack.GetTopology() == 11){
					trkPar.SetCov(0, 0.880100011*trkPar.GetCov(0));
					trkPar.SetCov(5, 1.01331647*trkPar.GetCov(5));				
				}
				else if(anaTrack.GetTopology() == 14){
					trkPar.SetCov(0, 2.878428911*trkPar.GetCov(0));
					trkPar.SetCov(5, 4.862743696*trkPar.GetCov(5));				
				}
				else if(anaTrack.GetTopology() == 15){
					trkPar.SetCov(0, 0.942305142*trkPar.GetCov(0));
					trkPar.SetCov(5, 1.072342839*trkPar.GetCov(5));				
				}			
			}*/
			//if(anaTrack.GetTopology() == 10) continue;
			for(evt::rec::VertexTrackIndexIterator vtxIter = track.VertexTracksBegin(); vtxIter != track.VertexTracksEnd(); vtxIter++){		
			//evt::rec::VertexTrackIndexIterator vtxIter = track.VertexTracksBegin();
				const evt::rec::VertexTrack vtxRecTr = recEvent.Get(*vtxIter);
					
				

				for (rec::TOFMassIndexIterator tofIter = vtxRecTr.TOFMassBegin();
				   		tofIter != vtxRecTr.TOFMassEnd(); ++tofIter)
			  	{
					const rec::TOFMass& tof = recEvent.Get(*tofIter);
					if(tof.GetTOFId() != TOFConst::eTOFF)
						continue;
					
					nToF++;
					hasToF = true;		
					tofSId.push_back(tof.GetScintillatorId());
					//if(tof.GetScintillatorId() == 47) cout << "radi" << endl;
					if(TMath::Abs(tof.GetPosition().GetX()-fDataAnaConfig.GetTOFFxCenter(tof.GetScintillatorId())) < dx){
						dx = TMath::Abs(tof.GetPosition().GetX()-fDataAnaConfig.GetTOFFxCenter(tof.GetScintillatorId()));
						tofMass = tof.GetSquaredMass();
					}

				}


			
			}
			
			 //if (anaTrack.GetTopology() == 9) trackExtrapolation.GetStopTrackParam().Print();
			if(hasToF){

				  //if(tofR.size() == 0) cout << "radi" << endl;
				std::sort (tofSId.begin(), tofSId.end());
				double nt = nToF;

				anaTrack.SetToFMassSquared(tofMass);
				anaTrack.SetToFScintillatorId(tofSId.at(0));
			}

			

			if(!trSel.IsTrackGood(anaTrack))
				continue;
			/*if (anaTrack.GetTopology() == 9) {
				trackExtrapolation.GetStopTrackParam().Print();
				cout << "000000000000000000000000000000000000000000000000000000000000000" << endl;
			}*/
			k++;
			//trkPar.Print();
			/*if(bad){
				trkPar.Print();
			}*/
			if(mc){
				//cout << "radi" << endl;
				if(track.GetNumberOfSimVertexTracks() == 0){
					 pdgId.push_back(-999);
					pxSim.push_back(-999);
					pySim.push_back(-999);
					pzSim.push_back(-999);
					xSim.push_back(-999);
					ySim.push_back(-999);
					zSim.push_back(-999);					
					qSim.push_back(-999);
					process.push_back(-999);	 
				}
				else{
				  //cout << track.GetNumberOfSimVertexTracks()  << endl; 
					bool radi = true;
					//cout << "****************************************" << endl;
					for(sim::VertexTrackIndexIterator it = track.SimVertexTracksBegin (); it != track.SimVertexTracksEnd (); ++it){
					  //cout << "********************" << endl;
						//if(track.GetNumberOfSimVertexTracks() > 1) cout << endl;
						sim::VertexTrack &simtr = event.GetSimEvent().Get(*it);

						if(simtr.GetRecTrackWithMaxCommonPoints () != track.GetIndex() && track.GetNumberOfSimVertexTracks() != 1){
							continue;
						}
						//cout << simtr.GetParticleId() << endl;
						pdgId.push_back(simtr.GetParticleId());
						pxSim.push_back(simtr.GetMomentum().GetX());
						pySim.push_back(simtr.GetMomentum().GetY());
						pzSim.push_back(simtr.GetMomentum().GetZ());
						qSim.push_back(simtr.GetCharge());
						
						if(simtr.HasStartVertex()){
						  
							const sim::Vertex &vtx = event.GetSimEvent().Get(simtr.GetStartVertexIndex());
							xSim.push_back(vtx.GetPosition().GetX());
							ySim.push_back(vtx.GetPosition().GetY());
							zSim.push_back(vtx.GetPosition().GetZ());
							process.push_back(vtx.GetType());
							//cout << simtr.GetParticleId() << " " << vtx.GetPosition().GetZ() << endl;	
							if(vtx.GetNumberOfParentTracks() > 0){
								sim::VertexTrackIndexIterator pit = vtx.ParentTracksBegin();
								sim::VertexTrack &psimtr = event.GetSimEvent().Get(*pit);
							}
						}
						else{
							xSim.push_back(-999);
							ySim.push_back(-999);
							zSim.push_back(-999);	
							process.push_back(-999);	 					
						}
						radi = true;
						break;
					}
					if(!radi){
						pdgId.push_back(-999);
						pxSim.push_back(-999);
						pySim.push_back(-999);
						pzSim.push_back(-999);
						xSim.push_back(-999);
						ySim.push_back(-999);
						zSim.push_back(-999);					
						qSim.push_back(-999);
						process.push_back(-999);							
					}
					
				} 
			}

			x.push_back(anaTrack.GetExtrapolatedPosition().GetX());
			y.push_back(anaTrack.GetExtrapolatedPosition().GetY());
			z.push_back(anaTrack.GetExtrapolatedPosition().GetZ());
			
			xE.push_back(anaTrack.GetPositionErrors().at(0));
			yE.push_back(anaTrack.GetPositionErrors().at(1));
			
			distTarg.push_back(anaTrack.GetDistanceFromTarget());
			px.push_back(anaTrack.GetMomentum().GetX());
			py.push_back(anaTrack.GetMomentum().GetY());
			pz.push_back(anaTrack.GetMomentum().GetZ());
			
			if(nToF > 0) {
				nTOF.push_back(nToF);
				m2TOF.push_back(tofMass);
				tofScId.push_back(tofSId.at(0));
			}
			else{
				nTOF.push_back(0);
				m2TOF.push_back(-9999.);
				tofScId.push_back(0);			
			}
			zStart.push_back(track.GetFirstPointOnTrack().GetZ());
			zLast.push_back(track.GetLastPointOnTrack().GetZ());
			xLast.push_back(track.GetLastPointOnTrack().GetX());
			qD.push_back(track.GetCharge());
			
			nVTPC1.push_back(anaTrack.GetNumberOfClusters().VTPC1); 
			nVTPC2.push_back(anaTrack.GetNumberOfClusters().VTPC2);
			nGTPC.push_back(anaTrack.GetNumberOfClusters().GTPC);
			nMTPC.push_back(anaTrack.GetNumberOfClusters().MTPC);
			topology.push_back(anaTrack.GetTopology());
			
			dEdx.push_back(track.GetEnergyDeposit(TrackConst::eAll));

			if(anaTrack.GetExtrapolatedPosition().GetZ() < -657.51+36) {
				/*if(1000*acos(anaTrack.GetMomentum().GetZ()/anaTrack.GetMomentum().GetMag()) > 300 && 1000*acos(anaTrack.GetMomentum().GetZ()/anaTrack.GetMomentum().GetMag()) < 400){
					if(nToF<1) cout << runNumber << " " << evHeader.GetId() << " " << track.GetCharge() << " " << anaTrack.GetMomentum().GetMag() << endl;
				}*/
			}
		}

		if(zStart.size()>0){
			spectrumFile->cd();
			fSpectrum->Fill();
			//if (z.size() != zSim.size()) cout << z.size() << " " << zSim.size() << endl;
		}
	}
	
	cout << ntracks << endl;
	cout << nEvents << endl;
	spectrumFile->cd();
	fSpectrum->Write();
	/*for(int i = 0; i < 7; i++){
		cout << xte[i]/ne[i] << " " << yte[i]/ne[i] << endl;
	}*/
	//cout << xte/ne << " " << yte/ne << endl;
	//tpos.Write();
	spectrumFile->Close();
	cout << k << endl;
	evSel.WriteToRoot("event_cuts", cutFile);
	trSel.WriteToRoot("track_cuts", cutFile);
	evSel.StoreCutHisto("event_cuts", cutFile);
	trSel.StoreCutHisto("track_cuts", cutFile);
	TFile out(cutFile.c_str(), "UPDATE");
	out.cd();
	tops.Write();
	out.Close();
}


