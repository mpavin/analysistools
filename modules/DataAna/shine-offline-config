#!/bin/sh

# Shine Offline configuration script
# Author: Lukas Nellen
# $Id: shine-offline-config.in 18333 2011-01-15 15:21:17Z paul $

prefix=/afs/cern.ch/na61/Releases/SHINE/v1r4p0
exec_prefix=

CASTOR_LIB_DIR=`dirname "/afs/cern.ch/sw/lcg/external/castor/2.1.13-6/i686-slc6-gcc47-opt/usr/lib/libshift.so"`
ROOT_LIB_DIR=/afs/cern.ch/sw/lcg/app/releases/ROOT/5.34.19/i686-slc6-gcc47-opt/root/lib
ROOT_BIN_DIR=/afs/cern.ch/sw/lcg/app/releases/ROOT/5.34.19/i686-slc6-gcc47-opt/root/bin
#G4_LIB_DIR=
XERCESC_LIB_DIR=/afs/cern.ch/sw/lcg/external/XercesC/3.1.1p1/i686-slc6-gcc47-opt/lib
#CLHEP_LIB_DIR=/lib
BOOST_LIB_DIR=/afs/cern.ch/sw/lcg/external/Boost/1.53.0_python2.7/i686-slc6-gcc47-opt/lib
MYSQL_LIB_DIR=
XZ_LIB_DIR=/afs/cern.ch/na61/Programs/xz/i686-slc5-gcc43-opt/lib
if [ -e /afs/cern.ch/na61/Releases/SHINE/v1r4p0/lib/na49 ] ; then
  NA61_LD_LIBRARY_PATH=$ROOT_LIB_DIR:$XERCESC_LIB_DIR:$BOOST_LIB_DIR:$MYSQL_LIB_DIR:$XZ_LIB_DIR:/afs/cern.ch/na61/Releases/SHINE/v1r4p0/lib:/afs/cern.ch/na61/Releases/SHINE/v1r4p0/lib/na49
  LEGACY_LD_FLAGS="-L/afs/cern.ch/na61/Releases/SHINE/v1r4p0/lib/na49 -lNA49LegacyCore"
else
  NA61_LD_LIBRARY_PATH=$ROOT_LIB_DIR:$XERCESC_LIB_DIR:$BOOST_LIB_DIR:$MYSQL_LIB_DIR:$XZ_LIB_DIR:/afs/cern.ch/na61/Releases/SHINE/v1r4p0/lib
fi
if [ -e $CASTOR_LIB_DIR ] ; then
  NA61_LD_LIBRARY_PATH=$NA61_LD_LIBRARY_PATH:$CASTOR_LIB_DIR
fi
#if [ x$G4_LIB_DIR != x ]; then
#  NA61_LD_LIBRARY_PATH=$NA61_LD_LIBRARY_PATH:$G4_LIB_DIR
#fi
if [ -e /afs/cern.ch/na61/Releases/SHINE/v1r4p0/bin/na49 ]; then
  NA61_PATH=$ROOT_BIN_DIR:/afs/cern.ch/na61/Releases/SHINE/v1r4p0/bin:/afs/cern.ch/na61/Releases/SHINE/v1r4p0/bin/na49
else
  NA61_PATH=$ROOT_BIN_DIR:/afs/cern.ch/na61/Releases/SHINE/v1r4p0/bin
fi
################### no argument given #################
if [ x$1 = x ]; then
  sh $0 --help
  exit 1
fi

for opt; do
  case $opt in
################### option handling  #################

    --version) echo "v1r4p0-svn_tag" ;;

    --bugs) echo Report bugs at "https://its.cern.ch/jira/browse/SHINEJIRA" ;;

    --cppflags) echo -I/afs/cern.ch/sw/lcg/external/XercesC/3.1.1p1/i686-slc6-gcc47-opt/include  -pthread -m32 -I/afs/cern.ch/sw/lcg/app/releases/ROOT/5.34.19/i686-slc6-gcc47-opt/root/include  -I/afs/cern.ch/sw/lcg/external/Boost/1.53.0_python2.7/i686-slc6-gcc47-opt/include/boost-1_53 -DBOOST_MULTI_INDEX_DISABLE_SERIALIZATION  -I/afs/cern.ch/na61/Releases/SHINE/v1r4p0/include ;;

    --ldflags) echo -Wl,--no-as-needed  $LEGACY_LD_FLAGS -L/afs/cern.ch/na61/Releases/SHINE/v1r4p0/lib -lShineModules -lShineFramework -lShineEventIO -lShineUtilities /afs/cern.ch/sw/lcg/external/XercesC/3.1.1p1/i686-slc6-gcc47-opt/lib/libxerces-c.so  -L/afs/cern.ch/sw/lcg/app/releases/ROOT/5.34.19/i686-slc6-gcc47-opt/root/lib -lCore -lCint -lRIO -lNet -lHist -lGraf -lGraf3d -lGpad -lTree -lRint -lPostscript -lMatrix -lPhysics -lMathCore -lThread -pthread -lm -ldl -rdynamic  -lMinuit  /afs/cern.ch/sw/lcg/external/Boost/1.53.0_python2.7/i686-slc6-gcc47-opt/lib/libboost_filesystem-gcc47-mt-1_53.so /afs/cern.ch/sw/lcg/external/Boost/1.53.0_python2.7/i686-slc6-gcc47-opt/lib/libboost_program_options-gcc47-mt-1_53.so /afs/cern.ch/sw/lcg/external/Boost/1.53.0_python2.7/i686-slc6-gcc47-opt/lib/libboost_iostreams-gcc47-mt-1_53.so /afs/cern.ch/sw/lcg/external/Boost/1.53.0_python2.7/i686-slc6-gcc47-opt/lib/libboost_regex-gcc47-mt-1_53.so /afs/cern.ch/sw/lcg/external/Boost/1.53.0_python2.7/i686-slc6-gcc47-opt/lib/libboost_system-gcc47-mt-1_53.so  -lrt;;

    --cxxflags) echo -fPIC -pipe -pthread -Wall -Wextra  -m32 -march=pentium4 -mfpmath=sse ;;

    --main) echo -L/afs/cern.ch/na61/Releases/SHINE/v1r4p0/lib -lShineOfflineMain ;;

    --config) echo /afs/cern.ch/na61/Releases/SHINE/v1r4p0/config ;;

    --doc) echo /afs/cern.ch/na61/Releases/SHINE/v1r4p0/doc ;;

    --db-path) echo /afs/cern.ch/na61/ReleasesDB/v2/Shine ;;

    --db-path-na49) echo /afs/cern.ch/na61/ReleasesDB/v2/NA49/DB ;;

    --path) echo /afs/cern.ch/na61/Releases/SHINE/v1r4p0/bin ;;

    --ld_library_path) echo $NA61_LD_LIBRARY_PATH ;;

    --rootsys) echo /afs/cern.ch/sw/lcg/app/releases/ROOT/5.34.19/i686-slc6-gcc47-opt/root ;;

    --dshack_home) echo /afs/cern.ch/na61/Releases/SHINE/v1r4p0 ;;

    --dshack_odf_dir) echo /afs/cern.ch/na61/Releases/SHINE/v1r4p0/config/odf ;;

    --env-sh)
      if [ -e $LD_LIBRARY_PATH ]; then
        echo export LD_LIBRARY_PATH=$NA61_LD_LIBRARY_PATH\;
      else
        echo export LD_LIBRARY_PATH=$NA61_LD_LIBRARY_PATH:\$LD_LIBRARY_PATH\;
      fi
      echo export PATH=$NA61_PATH:\$PATH\;
      if [ -e /afs/cern.ch/na61/Releases/SHINE/v1r4p0/config/odf ]; then
        echo export DSHACK_REP_PATH=/afs/cern.ch/na61/Releases/SHINE/v1r4p0/config/odf;
      fi
      echo export ROOTSYS=/afs/cern.ch/sw/lcg/app/releases/ROOT/5.34.19/i686-slc6-gcc47-opt/root\;
      if [ -e /afs/cern.ch/na61/Releases/SHINE/v1r4p0 ]; then
        echo export DSHACK_HOME=/afs/cern.ch/na61/Releases/SHINE/v1r4p0\;
      fi
      ;;

    --env-csh)
      if [ -e $LD_LIBRARY_PATH ]; then
        echo setenv LD_LIBRARY_PATH $NA61_LD_LIBRARY_PATH\;
      else
        echo setenv LD_LIBRARY_PATH $NA61_LD_LIBRARY_PATH:\$LD_LIBRARY_PATH\;
      fi
      echo setenv PATH $NA61_PATH:\$PATH\;
      if [ -e /afs/cern.ch/na61/Releases/SHINE/v1r4p0/config/odf ]; then
        echo setenv DSHACK_REP_PATH /afs/cern.ch/na61/Releases/SHINE/v1r4p0/config/odf\;
      fi
      echo setenv ROOTSYS /afs/cern.ch/sw/lcg/app/releases/ROOT/5.34.19/i686-slc6-gcc47-opt/root\;
      if [ -e /afs/cern.ch/na61/Releases/SHINE/v1r4p0 ]; then
        echo setenv DSHACK_HOME /afs/cern.ch/na61/Releases/SHINE/v1r4p0\;
      fi
      ;;

    --schema-location) echo /afs/cern.ch/na61/Releases/SHINE/v1r4p0/config ;;

################### help text #################
    --help) cat <<EOF 1>&2

Shine offline software configuration tool version "v1r4p0-svn_tag".

Retrieve configuration options of the currently installed version of
the Shine offline software
    --version            Package version
    --bugs               Bug reporting information
    --cppflags           Flags for the C++ pre-processor (to locate header files)
    --cxxflags           Flags for the C++ compiler and linker
                         (optimization, warning, debugging, ...)
    --ldflags            Flags for the linker
    --libtool            Path to copy of the libtool script used to compile the
                         shine offline software
    --main               Path and name of the object file with the main
    --config             Path to default configuration files
    --doc                Path to documentation
    --schema-location    Path to schema files
    --db-path            Path to shine db
    --db-path-na49       Path to na49db
    --path               Path to executables
    --ld_library_path    Path's to extra shared libraries
    --rootsys            The ROOTSYS environment variable as set during compilation
    --dshack_home        The location of DSHACK (necessary for plugin seatch)
    --dshack_odf_dir     The location of DSHACK ODF repository
    --env-sh             Provide statements for configuring the enviroment for
                         the Bourne-shell family of shells
    --env-csh            Provide statements for configuring the enviroment for
                         the c-shell family of shells
    --help               This information

For setting up the user's environment, use something like
  eval \`$0 --env-sh\`
or
  eval \`$0 --env-csh\`
in the .bashrc or .cshrc
EOF
;;
################### unknown option #################
    *)
      echo "Unknown option $opt " 2>&1
      sh $0 --help
      exit 1 ;;
  esac
done
