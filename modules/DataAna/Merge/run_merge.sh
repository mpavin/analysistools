#!/bin/sh

readonly shine_version='v1r4p0'
readonly shine_root="/afs/cern.ch/na61/Releases/SHINE/${shine_version}"
. "${shine_root}/scripts/env/lxplus_32bit_slc6.sh"
eval $("${shine_root}/bin/shine-offline-config" --env-sh)

#RESDIR="/afs/cern.ch/work/m/mpavin/Results/DATA"
ADD=$1

if [ "$ADD" == "mc" ]
then
    RESDIR="/afs/cern.ch/work/m/mpavin/Results/MC2"
else
    RESDIR="/afs/cern.ch/work/m/mpavin/Results/DATA2"
fi

#PATH="${RESDIR}/${ADD}_spectrum_*.root"
PATH="${RESDIR}/${ADD}_spectrum_*.root"
root="${ROOTSYS}/bin/root"
#echo $ROOTSYS
/bin/rm "list.txt"
for line in $PATH
do
	echo $line >>list.txt 
	#echo $line
done

if [ "$ADD" == "mc" ]
then
	$root -l 'merge.C("list.txt", "mc")'
else
	$root -l 'merge.C("list.txt", "data")'
fi
