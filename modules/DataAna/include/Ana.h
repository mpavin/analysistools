#ifndef ANA_H
#define ANA_H

#include <vector>
#include <string>

#include "EventSelection.h"
#include "TrackSelection.h"

#include "DataAnaConfig.h"
#include "TreeDataTypes.h"
#include <TTree.h>

using namespace std;

class Ana {

	public:

		Ana (unsigned int max): fDataAnaConfig(DataAnaConfig()), fSpectrum(NULL)  {

			SetMaxNumEvents(max);
			//fDataAnaConfig = DataAnaConfig(mConfig);
			//fSpectrum = TTree("Spectrum", "Spectrum");
		}

		void SetMainConfigFile(string file){fMainConfigFile = file;}
		void SetMaxNumEvents(unsigned int num){fMaxEv = num;}

		void Run();
		void WriteData();
		

		

	private:


		string fMainConfigFile;
		string fInputFile;
		
		unsigned int fMaxEv;
		DataAnaConfig fDataAnaConfig;
		TTree *fSpectrum;
		TTree *fSpectrumMC;
		SpectrumdEdxToF fSpectrumStruct;

};

#endif
