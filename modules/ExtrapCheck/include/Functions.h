#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <fstream>
#include <vector>
#include <string>
#include <sstream>
#include <iostream>    
#include <iomanip> 
#include <stdlib.h>
#include <TH1.h>

using namespace std;

struct Hit{
	int tr;
	double tof[2];
	
};

struct BeamA{
	double aX;
	double bX;
	double aY;
	double bY;
};

struct ImpactPar{
	int scint;
	double x;
	double y;
	double dx;
};

void ReadInputList(string list, vector<string> &job){

	ifstream ifs;
	string line;
	ifs.open(list.c_str());
		
	if (!ifs.is_open()){
		cerr << "Could not open infile. Exiting..." << endl;			
		exit (101);
	}
    
	while (getline(ifs, line)){
		if(line.at(0)=='#')
			continue;
		job.push_back(line);
	}
	ifs.close();	

}


std::string ConvertToString(double x, int prec){
	std::stringstream ss;
	std::string val;

	ss << std::fixed << std::setprecision(prec);
	ss << x;
	ss >> val;

	return val;
}


int FindZBin(double z){
	double zT = z + 657.41;
	
	//cout << zT << endl;
	if(zT >= 0 && zT < 18) return 0;
	else if(zT >= 18 && zT < 36) return 1;
	else if(zT >= 36 && zT < 54) return 2;
	else if(zT >= 54 && zT < 72) return 3;
	else if(zT >= 72 && zT < 89.99) return 4;
	else if(zT >= 89.99 && zT < 90.01) return 5;
	else{
		cerr << "Bad z bin!" << endl;
		return 5;
	}
}


void SetHistoStyle(TH1& hist, string title, double axlbsize, double axtsize, string xtitle, string ytitle){
	hist.SetTitle(title.c_str());
	hist.GetXaxis()->SetTitle(xtitle.c_str());
	hist.GetXaxis()->SetTitleSize(axtsize);
	hist.GetXaxis()->SetLabelSize(axlbsize);
	
	hist.GetYaxis()->SetTitle(ytitle.c_str());
	hist.GetYaxis()->SetTitleSize(axtsize);
	hist.GetYaxis()->SetLabelSize(axlbsize);
	
}
#endif
