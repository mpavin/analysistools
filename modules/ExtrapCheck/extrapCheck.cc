#include <vector>
#include <string>
#include <iostream>
#include "Functions.h"
#include <TH1D.h>
#include <TH2D.h>
#include <TFile.h>
#include <TChain.h>
#include <TTree.h>
#include <TMath.h>
#include <math.h>

using namespace std;


int main(){


	

	string inputFile = "list1.txt";
	const int n = 6;
	double zTarg = 657.51;
	TChain chain("Data");
	
	string outName =  "extraplmf.root";
	TFile outFile(outName.c_str(), "RECREATE");
	ifstream ifs;
	string line;
	ifs.open(inputFile.c_str());
		
	if (!ifs.is_open()){
		cerr << "[ReadInputListFile] Could not open infile. Exiting..." << endl;			
		exit (101);
	}
    
	while (getline(ifs, line)){
		if(line.at(0)=='#')
			continue;
		chain.Add(line.c_str());
	}
	ifs.close();		



	BeamA beamD;
	vector<double> *x = NULL;
	vector<double> *xE = NULL;
	vector<double> *y = NULL;
	vector<double> *yE = NULL;
	vector<double> *z = NULL;
	vector<double> *px = NULL;
	vector<double> *py = NULL;
	vector<double> *pz = NULL;
	vector<double> *distTarg = NULL;
	vector<double> *zStart = NULL;
	vector<double> *dEdx = NULL;
	vector<int> *qD = NULL;
	vector<int> *nVTPC1 = NULL;
	vector<int> *nVTPC2 = NULL;
	vector<int> *nGTPC = NULL;
	vector<int> *nMTPC = NULL;
	vector<int> *topology = NULL;
	vector<int> *nTOF = NULL;
	vector<double> *m2TOF = NULL;	
	vector<int> *tofScId = NULL;
	int runNumber;
	
	vector<int> *pdgId = NULL;
	vector<double> *xSim = NULL;
	vector<double> *ySim = NULL;
	vector<double> *zSim = NULL;
	vector<double> *pxSim = NULL;
	vector<double> *pySim = NULL;
	vector<double> *pzSim = NULL;
	vector<int> *qSim = NULL;
	
	TBranch *b_aData = chain.GetBranch("Beam");
 	b_aData->SetAddress(&beamD);
	chain.SetBranchAddress("x", &x);
	chain.SetBranchAddress("xE", &xE);
	chain.SetBranchAddress("y", &y);
	chain.SetBranchAddress("yE", &yE);
	chain.SetBranchAddress("z", &z);
	chain.SetBranchAddress("px", &px);
	chain.SetBranchAddress("py", &py);
	chain.SetBranchAddress("pz", &pz);
	chain.SetBranchAddress("distTarg", &distTarg);
	chain.SetBranchAddress("zStart", &zStart);
	chain.SetBranchAddress("dEdx", &dEdx);
	chain.SetBranchAddress("q", &qD);
	chain.SetBranchAddress("nVTPC1", &nVTPC1);
	chain.SetBranchAddress("nVTPC2", &nVTPC2);
	chain.SetBranchAddress("nGTPC", &nGTPC);
	chain.SetBranchAddress("nMTPC", &nMTPC);
	chain.SetBranchAddress("topology", &topology);
	chain.SetBranchAddress("nTOF", &nTOF);
	chain.SetBranchAddress("m2TOF", &m2TOF);
	chain.SetBranchAddress("tofScId", &tofScId);
	chain.SetBranchAddress("runNumber", &runNumber);
	
	chain.SetBranchAddress("pdgId", &pdgId);
	chain.SetBranchAddress("xSim", &xSim);
	chain.SetBranchAddress("ySim", &ySim);
	chain.SetBranchAddress("zSim", &zSim);
	chain.SetBranchAddress("pxSim", &pxSim);
	chain.SetBranchAddress("pySim", &pySim);
	chain.SetBranchAddress("pzSim", &pzSim);
	
	int nEntries = chain.GetEntries();
	
	double zbor[7] = {0, 18, 36, 54, 72, 89.99, 90.02};
	double bdx = 0.8;
	double bdy = 0.8;
	double bp = 0.08;

	//**************** check ***********************
	TH1D dp("dp", "", 100, -0.5, 0.5);
	TH1D ph("ph", "", 100, 29, 32);
	TH1D phSim("phSim", "", 100, 29, 32);
	TH1D dx("dx", "", 100, -bdx, bdx);
	TH1D dy("dy", "", 100, -bdy, bdy);
	TH1D dz("dz", "", 100, -10, 10);
	TH2D dz2D("dz2D", "", 100, -5, 95, 100, -5, 95);

	TH1D dtx("dtx", "", 100, -0.01, 0.01);
	TH1D dty("dty", "", 100, -0.01, 0.01);
	
	TH1D xe("xe", "", 100, 0.0, 1);
	TH1D ye("ye", "", 100, 0.0, 1);
	

	TH1D dist("dist", "", 10, 0, 10);

	
	cout << nEntries << endl;
	//nEntries = 1300000;
	for(int i = 0; i < nEntries; ++i){
		//cout << "******************************" << endl;
		chain.GetEntry(i,1);
		
		if(!((i+1)%100000))
			cout << "Processing event # "  << i+1 << endl;
		
			//*************************************CUTS***************************************************************
		if(z->size() != zSim->size()) continue;
		for(int j = 0; j < px->size(); j++){
			
			if(pdgId->at(j) == -999) continue;

			//if(zSim->at(j) > -567.50) continue;
			//if(zSim->at(j) < -567.52) continue;
			//if(z->at(j) < -567.52) continue;
			//if(topology->at(j) != 12) continue;
			//if(topology->at(j) == 12) continue;
			if(topology->at(j) == 10) continue;
			//if(nVTPC1->at(j) + nVTPC2->at(j) + nGTPC->at(j) + nMTPC->at(j) < 50) continue;
			//if(nVTPC1->at(j) + nVTPC2->at(j) < 30) continue;
			//if(xE->at(j) > 0.1) continue;
			//if(yE->at(j) > 0.1) continue;
			double p = sqrt(px->at(j)*px->at(j) + py->at(j)*py->at(j) + pz->at(j)*pz->at(j));
			if(p<30.0) continue;
			if(p>31.72) continue;
			double pSim = sqrt(pxSim->at(j)*pxSim->at(j) + pySim->at(j)*pySim->at(j) + pzSim->at(j)*pzSim->at(j));
			//if(pSim < 30.89) continue;
			dp.Fill(pSim-p);
			ph.Fill(p);
			phSim.Fill(pSim);
			//********************************************FILL HISTOS**************************************************
			/*double txSim = pxSim->at(j)/pzSim->at(j);
			double tySim = pySim->at(j)/pzSim->at(j);
			double tx = px->at(j)/pz->at(j);
			double ty = py->at(j)/pz->at(j);
			dtx.Fill(tx - txSim);
			dty.Fill(ty - tySim);*/
			double xVal = -567.41*beamD.aX + beamD.bX;
			double yVal = -567.41*beamD.aY + beamD.bY;
			dx.Fill(xSim->at(j) - x->at(j));
			dy.Fill(ySim->at(j) - y->at(j));
			xe.Fill(xE->at(j));
			ye.Fill(yE->at(j));
			
			dz2D.Fill(zSim->at(j)+657.51, z->at(j)+657.51);
			if(zSim->at(j) < -567.511){
				dz.Fill(z->at(j) - zSim->at(j));
				
			}
			double sigmaR = sqrt(TMath::Power(x->at(j)*xE->at(j),2)+TMath::Power(y->at(j)*yE->at(j),2))/sqrt(x->at(j)*x->at(j)+y->at(j)*y->at(j));
			dist.Fill(distTarg->at(j)/sigmaR);
		}

	}

	outFile.cd();
	dx.Write();
	dy.Write();
	dz.Write();
	dtx.Write();
	dty.Write();
	xe.Write();
	ye.Write();
	dz2D.Write();
	dp.Write();
	ph.Write();
	phSim.Write();
	outFile.Close();
		
}





