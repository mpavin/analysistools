#ifndef DEDXCALIBCALC_H
#define DEDXCALIBCALC_H


#include <iostream>
#include <vector>
#include <string>
#include <TH2D.h>
#include <TGraphErrors.h>
#include <TFile.h>

using namespace std;

void dEdxCalibCalc(string inputFile, bool isList);
void SliceFit(TH2D &histo, TFile *output, double *pars);
#endif
