#ifndef DEDXCALIBCONFIG_H
#define DEDXCALIBCONFIG_H
#include <iostream>
#include <vector>
#include <string>

#include <vector>



using namespace std;

class dEdxCalibConfig{

	public:
		dEdxCalibConfig(){Read();}

		string GetOutputDir(){return fOutput;}
		double GetPionMass(){return fPionMass;}
		double GetKaonMass(){return fKaonMass;}
		double GetProtonMass(){return fProtonMass;}
		double GetElectronMass(){return fElectronMass;}
		bool IsPion(double p, double m2ToF, double dEdx);
		bool IsKaon(double p, double m2ToF, double dEdx);
		bool IsProton(double p, double m2ToF, double dEdx);
		bool IsElectron(double p, double m2ToF, double dEdx);
		
		void SetOutputDir(string value){fOutput = value;}
		void SetPionMass(double mass){fPionMass = mass;}
		void SetKaonMass(double mass){fKaonMass = mass;}
		void SetProtonMass(double mass){fKaonMass = mass;}
		void SetElectronMass(double mass){fKaonMass = mass;}
		void AddPionTOFPid(vector<double> &pid);
		void AddKaonTOFPid(vector<double> &pid);
		void AddProtonTOFPid(vector<double> &pid);
		void AddElectronTOFPid(vector<double> &pid);
		
		
	private:
		bool IsParticle(vector<vector<double> >& pid, double p,  double m2ToF, double dEdx);
		void AddPid(vector<vector<double> >& pids, vector<double> &pid);
		void Read();

		string fOutput;
		double fPionMass;
		double fProtonMass;
		double fKaonMass;
		double fElectronMass;
		vector<vector<double> > fPionPid;
		vector<vector<double> > fProtonPid;
		vector<vector<double> > fKaonPid;
		vector<vector<double> > fElectronPid;

};



#endif
