#include <stdlib.h>
#include "dEdxCalibCalc.h"
#include "TreeDataTypes.h"
#include "dEdxCalibConfig.h"
#include "dEdxResolution.h"
#include "Functions.h"
#include <sstream>
#include <TChain.h>
#include <TFile.h>
#include <TF1.h>
#include <TH1D.h>
#include <TCanvas.h>
#include <TTree.h>
#include <TVectorD.h>
#include <TBranch.h>
#include <utl/Vector.h>
#include "StrTools.h"
#include <TMath.h>
#include <TFitResult.h>
#include <TFitResultPtr.h>

using namespace utl; 
using namespace StrTools;
void dEdxCalibCalc(string inputFile, bool isList){

	dEdxCalibConfig cfg;
	
	
	TChain chain("Data");
	
	if(isList){
		ifstream ifs;
		string line;
		ifs.open(inputFile.c_str());
		
		if (!ifs.is_open()){
			cerr << "[ReadInputListFile] Could not open infile. Exiting..." << endl;			
			exit (101);
		}
    
		while (getline(ifs, line)){
			if(line.at(0)=='#')
				continue;
			chain.Add(line.c_str());
		}
		ifs.close();		
	}
	else {
		chain.Add(inputFile.c_str());
	}
	//TFile input(fInputFile.c_str(), "READ");
	//TTree *tree = (TTree*) input.Get("Data");


	BeamA beamD;
	vector<double> *x = NULL;
	vector<double> *xE = NULL;
	vector<double> *y = NULL;
	vector<double> *yE = NULL;
	vector<double> *z = NULL;
	vector<double> *px = NULL;
	vector<double> *py = NULL;
	vector<double> *pz = NULL;
	vector<double> *distTarg = NULL;
	vector<double> *zStart = NULL;
	vector<double> *dEdx = NULL;
	vector<int> *qD = NULL;
	vector<int> *nVTPC1 = NULL;
	vector<int> *nVTPC2 = NULL;
	vector<int> *nGTPC = NULL;
	vector<int> *nMTPC = NULL;
	vector<int> *topology = NULL;
	vector<int> *nTOF = NULL;
	vector<double> *m2TOF = NULL;	
	vector<int> *tofScId = NULL;
	int runNumber;
	
	TBranch *b_aData = chain.GetBranch("Beam");
 	b_aData->SetAddress(&beamD);
	chain.SetBranchAddress("x", &x);
	chain.SetBranchAddress("xE", &xE);
	chain.SetBranchAddress("y", &y);
	chain.SetBranchAddress("yE", &yE);
	chain.SetBranchAddress("z", &z);
	chain.SetBranchAddress("px", &px);
	chain.SetBranchAddress("py", &py);
	chain.SetBranchAddress("pz", &pz);
	chain.SetBranchAddress("distTarg", &distTarg);
	chain.SetBranchAddress("zStart", &zStart);
	chain.SetBranchAddress("dEdx", &dEdx);
	chain.SetBranchAddress("q", &qD);
	chain.SetBranchAddress("nVTPC1", &nVTPC1);
	chain.SetBranchAddress("nVTPC2", &nVTPC2);
	chain.SetBranchAddress("nGTPC", &nGTPC);
	chain.SetBranchAddress("nMTPC", &nMTPC);
	chain.SetBranchAddress("topology", &topology);
	chain.SetBranchAddress("nTOF", &nTOF);
	chain.SetBranchAddress("m2TOF", &m2TOF);
	chain.SetBranchAddress("tofScId", &tofScId);
	chain.SetBranchAddress("runNumber", &runNumber);

	
	

	int nEntries = chain.GetEntries();
	//cout << cfg.GetElectronMass() << endl;

	TH2D dEdx_bg("dEdx_bg", "", 210, 0, 4.2, 200, 0.8, 1.9);
	TH2D dEdx_logpp("dEdx_logpp", "", 150, -0.8, 1.5, 200, 0.8, 3.0);
	TH2D dEdx_logpm("dEdx_logpm", "", 150, -1.5, 0.8, 200, 0.8, 3.0);
	for(int i = 0; i < nEntries; ++i){
		chain.GetEntry(i);

		if(!((i+1)%100000))
			cout << "Processing entry # " << i+1 << " out of " << nEntries << "." << endl;
			
		for(int j = 0; j < x->size(); j++){	
			double p = sqrt(px->at(j)*px->at(j) + py->at(j)*py->at(j) + pz->at(j)*pz->at(j));
			if(qD->at(j)>0){
				dEdx_logpp.Fill(TMath::Log10(p), dEdx->at(j));
			}
			else{
				dEdx_logpm.Fill(-TMath::Log10(p), dEdx->at(j));
			}
			short int selectedpi = 0;
			short int selectedp = 0;
			short int selectedK = 0;
			short int selectede = 0;

			//if(qD->at(j)<0)continue;
			if(cfg.IsProton(p, m2TOF->at(j), dEdx->at(j))){
				selectedp = 1;
			}
			
			if(cfg.IsPion(p, m2TOF->at(j), dEdx->at(j))){
				selectedpi = 1;
			}

			if(cfg.IsElectron(p, m2TOF->at(j), dEdx->at(j))){
				selectede = 1;
			}
			
			if(cfg.IsKaon(p, m2TOF->at(j), dEdx->at(j))){
				selectedK = 1;
			}	
				
			short int selected = selectedpi + selectedp + selectedK + selectede;
			if(selected == 1){
				if(selectedpi == 1){
					dEdx_bg.Fill(log10(p/cfg.GetPionMass()), dEdx->at(j));
				}
				else if(selectedp == 1){
					dEdx_bg.Fill(log10(p/cfg.GetProtonMass()), dEdx->at(j));
				}
				else if(selectedK == 1){
					dEdx_bg.Fill(log10(p/cfg.GetKaonMass()), dEdx->at(j));
				}
				else if(selectede == 1){
					dEdx_bg.Fill(log10(p/cfg.GetElectronMass()), dEdx->at(j));
				}
		
			}
		}
	
	}
		
		
	string name = cfg.GetOutputDir() + "/dEdxMap.root";
	TFile *output = new TFile(name.c_str(), "RECREATE");
	output->cd();
	dEdx_bg.Write();
	dEdx_logpp.Write();
	dEdx_logpm.Write();
	
	//TCanvas cp("cp", "", 800, 800);
	//TCanvas cn("cn", "", 800, 800);
	
	/*TF1* dEdxFitpi = new TF1("dEdxFitpi", "[0]*(0.5*[1]*(1+[8]*[8]*TMath::Power(10,-2*x))*TMath::Log([2]*TMath::Power(10,2*x)/[8]/[8])-1-[3]*TMath::Power(1+[8]*[8]*TMath::Power(10,-2*x),[7])-[1]*TMath::Log(10)*(1+[8]*[8]*TMath::Power(10,-2*x))*((x-TMath::Log10([8])>=[4]&&x-TMath::Log10([8])<[5] ? 1.0 : 0.0)*(x-TMath::Log10([8])-[4]-([5]-[4])/[6]+([5]-[4])*TMath::Power(abs([5]-x+TMath::Log10([8]))/([5]-[4]),[6])/[6])+(x-TMath::Log10([8])>=[5] ? 1.0 : 0.0)*(x-TMath::Log10([8])-[4]-([5]-[4])/[6])))", -0.5,1.5);
	TF1* dEdxFitp = new TF1("dEdxFitp", "[0]*(0.5*[1]*(1+[8]*[8]*TMath::Power(10,-2*x))*TMath::Log([2]*TMath::Power(10,2*x)/[8]/[8])-1-[3]*TMath::Power(1+[8]*[8]*TMath::Power(10,-2*x),[7])-[1]*TMath::Log(10)*(1+[8]*[8]*TMath::Power(10,-2*x))*((x-TMath::Log10([8])>=[4]&&x-TMath::Log10([8])<[5] ? 1.0 : 0.0)*(x-TMath::Log10([8])-[4]-([5]-[4])/[6]+([5]-[4])*TMath::Power(abs([5]-x+TMath::Log10([8]))/([5]-[4]),[6])/[6])+(x-TMath::Log10([8])>=[5] ? 1.0 : 0.0)*(x-TMath::Log10([8])-[4]-([5]-[4])/[6])))", -0.01,1.5);
	TF1* dEdxFitK = new TF1("dEdxFitK", "[0]*(0.5*[1]*(1+[8]*[8]*TMath::Power(10,-2*x))*TMath::Log([2]*TMath::Power(10,2*x)/[8]/[8])-1-[3]*TMath::Power(1+[8]*[8]*TMath::Power(10,-2*x),[7])-[1]*TMath::Log(10)*(1+[8]*[8]*TMath::Power(10,-2*x))*((x-TMath::Log10([8])>=[4]&&x-TMath::Log10([8])<[5] ? 1.0 : 0.0)*(x-TMath::Log10([8])-[4]-([5]-[4])/[6]+([5]-[4])*TMath::Power(abs([5]-x+TMath::Log10([8]))/([5]-[4]),[6])/[6])+(x-TMath::Log10([8])>=[5] ? 1.0 : 0.0)*(x-TMath::Log10([8])-[4]-([5]-[4])/[6])))", -0.30,1.5);
	TF1* dEdxFite = new TF1("dEdxFite", "[0]*(0.5*[1]*(1+[8]*[8]*TMath::Power(10,-2*x))*TMath::Log([2]*TMath::Power(10,2*x)/[8]/[8])-1-[3]*TMath::Power(1+[8]*[8]*TMath::Power(10,-2*x),[7])-[1]*TMath::Log(10)*(1+[8]*[8]*TMath::Power(10,-2*x))*((x-TMath::Log10([8])>=[4]&&x-TMath::Log10([8])<[5] ? 1.0 : 0.0)*(x-TMath::Log10([8])-[4]-([5]-[4])/[6]+([5]-[4])*TMath::Power(abs([5]-x+TMath::Log10([8]))/([5]-[4]),[6])/[6])+(x-TMath::Log10([8])>=[5] ? 1.0 : 0.0)*(x-TMath::Log10([8])-[4]-([5]-[4])/[6])))", -0.5,1.5);
	TF1* dEdxFitmu = new TF1("dEdxFitmu", "[0]*(0.5*[1]*(1+[8]*[8]*TMath::Power(10,-2*x))*TMath::Log([2]*TMath::Power(10,2*x)/[8]/[8])-1-[3]*TMath::Power(1+[8]*[8]*TMath::Power(10,-2*x),[7])-[1]*TMath::Log(10)*(1+[8]*[8]*TMath::Power(10,-2*x))*((x-TMath::Log10([8])>=[4]&&x-TMath::Log10([8])<[5] ? 1.0 : 0.0)*(x-TMath::Log10([8])-[4]-([5]-[4])/[6]+([5]-[4])*TMath::Power(abs([5]-x+TMath::Log10([8]))/([5]-[4]),[6])/[6])+(x-TMath::Log10([8])>=[5] ? 1.0 : 0.0)*(x-TMath::Log10([8])-[4]-([5]-[4])/[6])))", -0.3,1.5);
	TF1* dEdxFitd = new TF1("dEdxFitd", "[0]*(0.5*[1]*(1+[8]*[8]*TMath::Power(10,-2*x))*TMath::Log([2]*TMath::Power(10,2*x)/[8]/[8])-1-[3]*TMath::Power(1+[8]*[8]*TMath::Power(10,-2*x),[7])-[1]*TMath::Log(10)*(1+[8]*[8]*TMath::Power(10,-2*x))*((x-TMath::Log10([8])>=[4]&&x-TMath::Log10([8])<[5] ? 1.0 : 0.0)*(x-TMath::Log10([8])-[4]-([5]-[4])/[6]+([5]-[4])*TMath::Power(abs([5]-x+TMath::Log10([8]))/([5]-[4]),[6])/[6])+(x-TMath::Log10([8])>=[5] ? 1.0 : 0.0)*(x-TMath::Log10([8])-[4]-([5]-[4])/[6])))", 0.3,1.5);
	dEdxFitpi->SetLineWidth(2);
	dEdxFitp->SetLineWidth(2);
	dEdxFitK->SetLineWidth(2);
	dEdxFite->SetLineWidth(2);
	dEdxFitmu->SetLineWidth(2);
	dEdxFitd->SetLineWidth(2);*/
	//dEdxFit->SetLineColor(2);
	double pars[8];
	SliceFit(dEdx_bg, output, pars);
	
	/*for(int i = 0; i < 8; i++){
		cout << pars[i] << endl;
		dEdxFitpi->SetParameter(i, pars[i]);
		dEdxFitp->SetParameter(i, pars[i]);
		dEdxFitK->SetParameter(i, pars[i]);
		dEdxFite->SetParameter(i, pars[i]);
	}*/
	//cp.cd();
	//dEdx_logpp.Draw("colz");
	
	/*dEdxFitpi->SetParameter(8, cfg.GetPionMass());
	dEdxFitpi->Draw("Csame");
	dEdxFitpi->Write();
	dEdxFitp->SetParameter(8, cfg.GetProtonMass());
	dEdxFitp->Draw("Csame");
	dEdxFitp->Write();
	dEdxFitK->SetParameter(8, cfg.GetKaonMass());
	dEdxFitK->Draw("Csame");
	dEdxFitK->Write();
	dEdxFite->SetParameter(8, cfg.GetElectronMass());
	dEdxFite->Draw("Csame");
	dEdxFite->Write();
	dEdxFitmu->SetParameter(8, 0.1056);
	dEdxFitmu->Draw("Csame");
	dEdxFitmu->Write();
	dEdxFitd->SetParameter(8, 1.876);
	dEdxFitd->Draw("Csame");
	dEdxFitd->Write();	*/
	//cn.cd();
	//dEdx_logpm.Draw("colz");
	

	/*dEdxFitpi->Draw("Csame");
	dEdxFitp->Draw("Csame");
	dEdxFitK->Draw("Csame");
	dEdxFite->Draw("Csame");
	dEdxFitmu->Draw("Csame");
	dEdxFitd->Draw("Csame");	*/
	//cp.Write();
	//cn.Write();
	
	output->Close();

	
}

void SliceFit(TH2D &histo, TFile *output, double *pars){


	vector<double> mom;
	vector<double> momE;
	vector<double> means;
	vector<double> meansE;
	vector<double> widths;
	vector<double> widthsE;	
	//
	
	vector<double> chi2;
	int nBins = histo.GetNbinsX();
	for(int i = 1; i <= nBins; i++){
	
		string names = "dEdx" + ConvertToString(i);
		TH1D* slice = histo.ProjectionY(names.c_str(), i, i);
		//if(i>140&&i<160) continue;
		if(slice->GetEntries() < 350) continue;	

		mom.push_back(histo.GetXaxis()->GetBinCenter(i));
		momE.push_back(histo.GetXaxis()->GetBinWidth(i)/2.);
		string name = "gausFit" + ConvertToString(i+1);
		TF1 gausFit(name.c_str(), "[0]*([3]*TMath::Gaus(x, [1], [2], 1)+(1-[3])*TMath::Gaus(x, [4], [5], 1))", slice->GetBinCenter(slice->GetMaximumBin())-0.2, slice->GetBinCenter(slice->GetMaximumBin())+0.2);
		
		gausFit.SetParName(0, "N");
		gausFit.SetParameter(0, slice->GetEntries());
		gausFit.SetParLimits(0, 0, slice->GetEntries()+0.2*slice->GetEntries());
	
		gausFit.SetParName(1, "#mu1");
		gausFit.SetParameter(1, slice->GetBinCenter(slice->GetMaximumBin()));
		gausFit.SetParLimits(1, slice->GetBinCenter(slice->GetMaximumBin())-0.05, slice->GetBinCenter(slice->GetMaximumBin())+0.05);	
	
		gausFit.SetParName(2, "#sigma1");
		gausFit.SetParameter(2, 0.04);
		gausFit.SetParLimits(2, 0.025, 0.045);	
		
		gausFit.SetParName(3, "#nu");
		gausFit.SetParameter(3, 0.7);
		gausFit.SetParLimits(3, 0.01, 1);	
		
		gausFit.SetParName(4, "#mu2");
		gausFit.SetParameter(4, slice->GetBinCenter(slice->GetMaximumBin()));
		gausFit.SetParLimits(4, slice->GetBinCenter(slice->GetMaximumBin())-0.05, slice->GetBinCenter(slice->GetMaximumBin())+0.05);	
		
		gausFit.SetParName(5, "#sigma2");
		gausFit.SetParameter(5, 0.05);
		gausFit.SetParLimits(5, 0.045, 0.07);	
		
		TFitResultPtr r = slice->Fit(&gausFit, "SR");
		chi2.push_back(r->Chi2()/r->Ndf());
		output->cd(); 
		slice->Write();


		//means.push_back(gausFit.GetParameter(1));
		means.push_back(gausFit.GetMaximumX());
		meansE.push_back(gausFit.GetParError(1));
		widths.push_back(gausFit.GetParameter(2));
		widthsE.push_back(gausFit.GetParError(2));


	}
	
	if (mom.size()<3) return;
	TGraphErrors *meanGraph = new TGraphErrors(mom.size(), mom.data(), means.data(), momE.data(), meansE.data());
	string meanName = "mean";
	meanGraph->SetName(meanName.c_str());
	TF1* dEdxFit = new TF1("dEdxFit", "[0]*(0.5*[1]*(1+TMath::Power(10,-2*x))*TMath::Log([2]*TMath::Power(10,2*x))-1-[3]*TMath::Power(1+TMath::Power(10,-2*x),[7])-[1]*TMath::Log(10)*(1+TMath::Power(10,-2*x))*((x>=[4]&&x<[5] ? 1.0 : 0.0)*(x-[4]-([5]-[4])/[6]+([5]-[4])*TMath::Power(abs([5]-x)/([5]-[4]),[6])/[6])+(x>=[5] ? 1.0 : 0.0)*(x-[4]-([5]-[4])/[6])))",
	//0.0, 3.2);
	mom.at(0), mom.at(mom.size()-1));
	
	/*TF1* dEdxFit = new TF1("dEdxFit", "[0]*(0.5*[1]*(1+TMath::Power(10,-2*x))*TMath::Log([2]*TMath::Power(10,2*x))-1-[3]*(1+TMath::Power(10,-2*x))-TMath::Power(0.5,[5])*[1]*TMath::Log(10)*(1+TMath::Power(10,-2*x))*TMath::Power(TMath::TanH((x-[4])*[6])+1, [5])*(x-[4]))",
	0.0, 3);
	//mom.at(0), mom.at(mom.size()-1));*/
	//meanLine->FixParameter(1,0);

	
	dEdxFit->SetParName(0, "p_{0}");
	dEdxFit->SetParameter(0, 0.45);
	dEdxFit->SetParLimits(0, 0.1, 0.7);
	
	dEdxFit->SetParName(1, "p_{1}");
	dEdxFit->SetParameter(1, 0.3);
	dEdxFit->SetParLimits(1, 0.1,1);	
	
	dEdxFit->SetParName(2, "p_{2}");
	dEdxFit->SetParameter(2, 2);
	dEdxFit->SetParLimits(2, 1, 5);	
	
	dEdxFit->SetParName(3, "p_{3}");
	dEdxFit->SetParameter(3, -2);
	dEdxFit->SetParLimits(3, -4, -0.5);	
	
	dEdxFit->SetParName(4, "p_{4}");
	dEdxFit->SetParameter(4, 1.);
	dEdxFit->SetParLimits(4, 0.5, 2.5);	
	
	dEdxFit->SetParName(5, "p_{5}");
	dEdxFit->SetParameter(5, 2.7);
	dEdxFit->SetParLimits(5, 2.5, 4);	
	
	dEdxFit->SetParName(6, "p_{6}");
	dEdxFit->SetParameter(6, 2.5);
	dEdxFit->SetParLimits(6, 1.3, 3);	
	
	dEdxFit->SetParName(7, "p_{7}");
	dEdxFit->FixParameter(7, 1);
	dEdxFit->SetParLimits(7, 0, 2);
	meanGraph->Fit("dEdxFit", "MR", "C");
	meanGraph->Fit("dEdxFit", "MR", "C");

	
	//meanLine->FixParameter(1,0);
	for(int i=0; i < 8; i++){
		pars[i] = dEdxFit->GetParameter(i);
	}
	TGraphErrors *widthGraph = new TGraphErrors(mom.size(), mom.data(), widths.data(), momE.data(), widthsE.data());
	string widthName = "width";
	widthGraph->SetName(widthName.c_str());
	TF1* width = new TF1("width", "[0]", mom.at(0), mom.at(mom.size()-1));	
	widthGraph->Fit("width", "R", "P");

	//output.cd();
	TGraph *chi2g = new TGraph(mom.size(), mom.data(), chi2.data());
	chi2g->SetName("chi2");
	output->cd();
	chi2g->Write();
	meanGraph->Write();
	widthGraph->Write();
	
	//double el[4] = {meanLine->GetParameter(0), widthParabola->GetParameter(0), widthParabola->GetParameter(1), widthParabola->GetParameter(2)};
	/*pars[0] = meanLine->GetParameter(0);
	pars[1] = meanLine->GetParameter(1);
	//pars[1] = 0;
	pars[2] = widthParabola->GetParameter(0);
	pars[3] = widthParabola->GetParameter(1);
	pars[4] = widthParabola->GetParameter(2);*/
	//TVectorD parameters(4, el);
	//parameters.SetName(add.c_str());
	//parameters.SetName(add.c_str());
	//parameters.Write();
	/*pars.push_back(meanLine->GetParameter(0));
	pars.push_back(meanLine->GetParameter(1));

	pars.push_back(widthParabola->GetParameter(0));
	pars.push_back(widthParabola->GetParameter(1));
	pars.push_back(widthParabola->GetParameter(2));*/
}
