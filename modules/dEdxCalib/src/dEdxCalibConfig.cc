#include <stdlib.h>

#include "dEdxCalibConfig.h"
#include <fstream>
#include <fwk/CentralConfig.h>
#include <utl/Branch.h>
#include <utl/BranchIterator.h>
#include "CConfig.h"
#include <math.h>  

using namespace std;
using namespace fwk;
using namespace utl; 

void dEdxCalibConfig::Read(){
	Branch tofMap = cConfig.GetTopBranch("dEdxCalib");

	if(tofMap.GetName() != "dEdxCalib"){
		cout << __FUNCTION__ << ": Wrong TOFMap config file! Top branch name doesn't match! Please check your configuration files! Exiting..." << endl;
		exit (EXIT_FAILURE);
	}

	for(BranchIterator it = tofMap.ChildrenBegin(); it != tofMap.ChildrenEnd(); ++it){
		Branch child = *it;
		
		if(child.GetName() == "output"){
			child.GetData(fOutput);
		}		
		else if(child.GetName() == "pid_cuts"){
			for(BranchIterator t = child.ChildrenBegin(); t != child.ChildrenEnd(); ++t){
				Branch pidChild = *t;
				
				if(pidChild.GetName() == "pion"){
					vector<double> val;
					
					for(BranchIterator t1 = pidChild.ChildrenBegin(); t1 != pidChild.ChildrenEnd(); ++t1){
						Branch particleChild = *t1;
						if(particleChild.GetName() == "mass"){
							particleChild.GetData(fPionMass);
						}
						else if(particleChild.GetName() == "val"){
							val.clear();
							particleChild.GetData(val);
							AddPionTOFPid(val);
						}						
					}
				}			

				if(pidChild.GetName() == "proton"){
					vector<double> val;
					
					for(BranchIterator t1 = pidChild.ChildrenBegin(); t1 != pidChild.ChildrenEnd(); ++t1){
						Branch particleChild = *t1;
						if(particleChild.GetName() == "mass"){
							particleChild.GetData(fProtonMass);
						}
						else if(particleChild.GetName() == "val"){
							val.clear();
							particleChild.GetData(val);
							AddProtonTOFPid(val);
						}						
					}
				}	
				
				if(pidChild.GetName() == "electron"){
					vector<double> val;
					
					for(BranchIterator t1 = pidChild.ChildrenBegin(); t1 != pidChild.ChildrenEnd(); ++t1){
						Branch particleChild = *t1;
						if(particleChild.GetName() == "mass"){
							particleChild.GetData(fElectronMass);
						}
						else if(particleChild.GetName() == "val"){
							val.clear();
							particleChild.GetData(val);
							AddElectronTOFPid(val);
						}						
					}
				}	
				
				
				if(pidChild.GetName() == "kaon"){
					vector<double> val;
					
					for(BranchIterator t1 = pidChild.ChildrenBegin(); t1 != pidChild.ChildrenEnd(); ++t1){
						Branch particleChild = *t1;
						if(particleChild.GetName() == "mass"){
							particleChild.GetData(fKaonMass);
						}
						else if(particleChild.GetName() == "val"){
							val.clear();
							particleChild.GetData(val);
							AddKaonTOFPid(val);
						}						
					}
				}						
			}
		}		
		
	}

}

bool dEdxCalibConfig::IsParticle(vector<vector<double> >& pid, double p, double m2ToF, double dEdx){
	if(pid.size() < 1) return false;
	
	bool good = false;

	for(int i = 0; i < pid.size(); ++i){
		if(pid.at(i).at(0) > p || pid.at(i).at(1) < p) continue;
		if(pid.at(i).at(2) > m2ToF || pid.at(i).at(3) < m2ToF) continue;
		if(pid.at(i).size() == 6){
			if(pid.at(i).at(4) > dEdx || pid.at(i).at(5) < dEdx) continue;
		}
		else if(pid.at(i).size() == 8){
			if((pid.at(i).at(4) + pid.at(i).at(5)*log10(p)) > dEdx || (pid.at(i).at(6) + pid.at(i).at(7)*log10(p)) < dEdx) continue;

		}
		good = true;
		break;
	}
	//return (good&&goodP) || (!good && !goodP);
	return good;
}

bool dEdxCalibConfig::IsPion(double p, double m2ToF, double dEdx){
	bool good = IsParticle(fPionPid, p, m2ToF, dEdx);
	return good;
}

bool dEdxCalibConfig::IsKaon(double p, double m2ToF, double dEdx){
	bool good = IsParticle(fKaonPid, p, m2ToF, dEdx);
	return good;
}

bool dEdxCalibConfig::IsProton(double p, double m2ToF, double dEdx){
	bool good = IsParticle(fProtonPid, p, m2ToF, dEdx);
	return good;
}

bool dEdxCalibConfig::IsElectron(double p, double m2ToF, double dEdx){
	bool good = IsParticle(fElectronPid, p, m2ToF, dEdx);
	return good;
}


void dEdxCalibConfig::AddPid(vector<vector<double> >& pids, vector<double> &pid){
	if(pid.size() != 6 && pid.size() != 8){
		cerr << __FUNCTION__ << ": TOF pid information missing! vector<double> must contain pmin, pmax, m2tof_min, m2tof_max." << endl;
		exit(EXIT_FAILURE);
	}
	
	pids.push_back(pid);
}

void dEdxCalibConfig::AddPionTOFPid(vector<double> &pid){
	AddPid(fPionPid, pid);
}

void dEdxCalibConfig::AddProtonTOFPid(vector<double> &pid){
	AddPid(fProtonPid, pid);
}

void dEdxCalibConfig::AddKaonTOFPid(vector<double> &pid){
	AddPid(fKaonPid, pid);
}

void dEdxCalibConfig::AddElectronTOFPid(vector<double> &pid){
	AddPid(fElectronPid, pid);
}
