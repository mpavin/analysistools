#include <vector>
#include <string>
#include <iostream>
#include "Functions.h"
#include "Constants.h"
#include <TH1D.h>
#include <TH2D.h>
#include <TFile.h>
#include <TChain.h>
#include <TTree.h>
#include <TMath.h>
#include <math.h>
#include <TH2Poly.h>

using namespace std;


int main(){

	string input = "list.txt";
	TChain chain("h1000");
	

	ifstream ifs;
	string line;
	ifs.open(input.c_str());
		
	if (!ifs.is_open()){
		cerr << "[ReadInputListFile] Could not open infile. Exiting..." << endl;			
		exit (101);
	}
    
	while (getline(ifs, line)){
		if(line.at(0)=='#')
			continue;
		chain.Add(line.c_str());
	}
	ifs.close();		

	Int_t           protnum;
  	Int_t           ipart;
  	Float_t         mom;
  	Float_t         pos[3];
  	Float_t         vec[3];

  // Declaration of leaf types for replica input
  	Int_t           pgen;
  	Int_t           ng;
  	Float_t         gpx[20];    //[ng]
  	Float_t         gpy[20];    //[ng]
  	Float_t         gpz[20];    //[ng]
  	Float_t         gvx[20];    //[ng]
  	Float_t         gvy[20];    //[ng]
  	Float_t         gvz[20];    //[ng]
  	Int_t           gpid[20];   //[ng]
  	Int_t           gmec[20];   //[ng]

	TBranch        *b_protnum;   //!
  	TBranch        *b_ipart;     //!
  	TBranch        *b_mom;       //!
  	TBranch        *b_pos;       //!
  	TBranch        *b_vec;       //!
  	TBranch        *b_pgen;      //!
  	TBranch        *b_ng;        //!
  	TBranch        *b_gpx;       //!
  	TBranch        *b_gpy;       //!
  	TBranch        *b_gpz;       //!
  	TBranch        *b_gvx;       //!
  	TBranch        *b_gvy;       //!
  	TBranch        *b_gvz;       //!
  	TBranch        *b_gpid;      //!
  	TBranch        *b_gmec;      //!
    
	chain.SetBranchAddress("protnum", &protnum, &b_protnum);
  	chain.SetBranchAddress("ipart", &ipart, &b_ipart);
  	chain.SetBranchAddress("mom", &mom, &b_mom);
  	chain.SetBranchAddress("pos", pos, &b_pos);
  	chain.SetBranchAddress("vec", vec, &b_vec);
  	chain.SetBranchAddress("pgen", &pgen, &b_pgen);
  	chain.SetBranchAddress("ng", &ng, &b_ng);
  	chain.SetBranchAddress("gpx", gpx, &b_gpx);
  	chain.SetBranchAddress("gpy", gpy, &b_gpy);
  	chain.SetBranchAddress("gpz", gpz, &b_gpz);
  	chain.SetBranchAddress("gvx", gvx, &b_gvx);
  	chain.SetBranchAddress("gvy", gvy, &b_gvy);
  	chain.SetBranchAddress("gvz", gvz, &b_gvz);
  	chain.SetBranchAddress("gpid", gpid, &b_gpid);
  	chain.SetBranchAddress("gmec", gmec, &b_gmec);
  	
  	
  	int nEntries = chain.GetEntries();

  	

	TH2D cMat("c", "", nBbins, 0, nBbins, nPbins, 0, nPbins);
	TH2D beam("beam", "", 200, -2, 2, 200, -2, 2);
	TH1D beamspace("beamspace", "", nBbins, 0, nBbins);
  	int eventId = -1;
  	int totev = 0;
	
	double pbins[13] = {0.400, 1.200, 2.000, 2.800, 3.600, 4.400, 5.200, 6.800, 8.400, 10.000, 13.200, 16.400, 19.600};
	TH1D test("test", "", 12, pbins);
	TH2Poly* hists[6];
	TFile fileIn("res2D.root", "READ");
	fileIn.cd();
	for(int i = 0; i < 6; i++){
		string name = "dndp2d_z" + ConvertToString(i+1., 0);
		hists[i] = (TH2Poly*) fileIn.Get(name.c_str());
	}
	 
	vector<TH2Poly*> poly;
	
	for(int i = 0; i < 6; i++){
		string name = "sim_z" + ConvertToString(i+1., 0);
		string title;
		if(i < 5) title = ConvertToString(i*18., 0) + " #leq z < " + ConvertToString(i*18. + 18., 0) + " cm";
		else title = "z = 90 cm";
		TH2Poly *temp;
		poly.push_back(temp);
		poly.at(i) = new TH2Poly(name.c_str(), title.c_str(), 0, 21, 0, 400);
		
		
		for(int j = 0; j < hists[i]->GetBins()->GetSize(); j++){
			TH2PolyBin *bin = (TH2PolyBin*) hists[i]->GetBins()->At(j);
			
			poly.at(i)->AddBin(bin->GetXMin(), bin->GetYMin(), bin->GetXMax(), bin->GetYMax());
		}
	}	
	
	

	//nEntries = 10000000;
	cerr << "radi" << endl;
  	for(int i = 0; i < nEntries; i++){
  		
  		chain.GetEntry(i);

		if(protnum != eventId){
			
			//beam.Fill(-gvz[0]*gpx[0]/gpz[0] + gvx[0]- targX, -gvz[0]*gpy[0]/gpz[0] + gvy[0]- targY);
			
			eventId = protnum;
			totev++;
  			if(!(totev%100000))
  				cout << "Processing event # " << totev << endl;
  				
  			
		}

		if(ipart!=8) continue;
		
		
		double z = pos[2];// + 657.51;
		

		if(z<0) continue;
		if(z>90.01) continue;

		int zbin = -99;
		for(int j = 0; j < nz; j++){
			if(zbord[j] <= z && z < zbord[j+1]){
				zbin = j;
				break;
			}
		}
		if(zbin < 0) continue;
		
		//cerr << "radi2" << endl;
		double th= 1000*acos(vec[2]);

		if(zbin == 0){
			if(th >= 20 && th < 40){
				test.Fill(mom);
			}
		}
		for(int j = 0; j < poly.at(zbin)->GetBins()->GetSize(); j++){
			TH2PolyBin *bin = (TH2PolyBin*) poly.at(zbin)->GetBins()->At(j);
			
			if(mom < bin->GetXMin() ||  mom >= bin->GetXMax()) continue;
			if(th < bin->GetYMin() ||  th >= bin->GetYMax()) continue;
			bin->Fill(1./(bin->GetXMax()-bin->GetXMin())/10500000.);
			//cout << "radi" << endl;
			//cout << bin->GetContent() << endl;
			break;
		}

		
  	}

	for(int i = 0; i < test.GetNbinsX(); i++){
		test.SetBinContent(i+1, test.GetBinContent(i+1)/((pbins[i+1]-pbins[i])*10500000));
	}
	double totevd = totev;
	for(int i = 0; i < 6; i++){
		for(int j = 0; j < poly.at(i)->GetBins()->GetSize(); j++){
			TH2PolyBin *bin0 = (TH2PolyBin*) hists[i]->GetBins()->At(j);
			TH2PolyBin *bin = (TH2PolyBin*) poly.at(i)->GetBins()->At(j);
			if(bin0->GetContent()*100 > 0){
				cout << (100*bin->GetContent())/(100*bin0->GetContent()) << endl;
			  bin->SetContent((100*bin->GetContent())/(100*bin0->GetContent()));
				
			}
			else bin->SetContent(0);
			
		}  
	}	
	cout << totev << endl;
  	TFile out("mcpredKPos.root", "RECREATE");
  	out.cd();
  	for(int i = 0; i < 6; i++){
  		poly.at(i)->Write();
  	}
  	test.Write();
  	out.Close();
}





