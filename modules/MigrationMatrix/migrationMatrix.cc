#include <vector>
#include <string>
#include <iostream>
#include "Functions.h"
#include <TH1D.h>
#include <TH2D.h>
#include <TFile.h>
#include <TChain.h>
#include <TTree.h>
#include <TMath.h>
#include <math.h>
#include <TH2Poly.h>
#include <TCanvas.h>
#include <TLine.h>
#include <TLatex.h>
#include <TStyle.h>
#include <TColor.h>
using namespace std;


int main(int argc, char* argv[]){

	//cout << argc << " " << argv[0] << endl;
	const UInt_t Number = 4;
	Double_t Red[Number]    = { 0.95, 0.35, 0.83, 0.00};
	Double_t Green[Number]  = { 0.95, 0.33, 0.35, 0.00};
	Double_t Blue[Number]   = { 0.95, 0.85, 0.33, 0.00};
	Double_t Length[Number] = { 0.0, 0.33, 0.66, 1.00 };
	Int_t nb=50;


	const int NRGBs = 3, NCont = 50;

	Double_t stops[NRGBs] = { 0.00, 0.50, 1.00 };
	Double_t red[NRGBs]   = { 0.00, 1.00, 1.00 };
	Double_t green[NRGBs] = { 0.00, 1.00, 0.00 };
	Double_t blue[NRGBs]  = { 0.00, 1.00, 0.00 };

	int paletteID = 1;


	if(paletteID == 1){
		TColor::CreateGradientColorTable(Number,Length,Red,Green,Blue,nb);
	}
	else if(paletteID == 2){
		gStyle->SetNumberContours(NCont);
		TColor::CreateGradientColorTable(NRGBs, stops, red, green, blue, NCont);
	}
	string inputFile  ="list.txt";


	TChain chain("Data");
	
	string outName = "migmat.root";
	TFile outFile(outName.c_str(), "RECREATE");
	ifstream ifs;
	string line;
	ifs.open(inputFile.c_str());
		
	if (!ifs.is_open()){
		cerr << "[ReadInputListFile] Could not open infile. Exiting..." << endl;			
		exit (101);
	}
    
	while (getline(ifs, line)){
		if(line.at(0)=='#')
			continue;
		chain.Add(line.c_str());
	}
	ifs.close();		


	//gStyle->SetPalette(87);
	BeamA beamD;
	vector<double> *x = NULL;
	vector<double> *xE = NULL;
	vector<double> *y = NULL;
	vector<double> *yE = NULL;
	vector<double> *z = NULL;
	vector<double> *px = NULL;
	vector<double> *py = NULL;
	vector<double> *pz = NULL;
	vector<double> *distTarg = NULL;
	vector<double> *zStart = NULL;
	vector<double> *zLast = NULL;
	vector<double> *dEdx = NULL;
	vector<int> *qD = NULL;
	vector<int> *nVTPC1 = NULL;
	vector<int> *nVTPC2 = NULL;
	vector<int> *nGTPC = NULL;
	vector<int> *nMTPC = NULL;
	vector<int> *topology = NULL;
	vector<int> *nTOF = NULL;
	vector<double> *m2TOF = NULL;	
	vector<int> *tofScId = NULL;
	
	vector<int> *pdgId = NULL;
	vector<int> *qSim = NULL;
	vector<double> *pxSim = NULL;
	vector<double> *pySim = NULL;
	vector<double> *pzSim = NULL;
	vector<double> *xSim = NULL;
	vector<double> *ySim = NULL;
	vector<double> *zSim = NULL;
	int runNumber;
	
	TBranch *b_aData = chain.GetBranch("Beam");
 	b_aData->SetAddress(&beamD);
	chain.SetBranchAddress("x", &x);
	chain.SetBranchAddress("xE", &xE);
	chain.SetBranchAddress("y", &y);
	chain.SetBranchAddress("yE", &yE);
	chain.SetBranchAddress("z", &z);
	chain.SetBranchAddress("px", &px);
	chain.SetBranchAddress("py", &py);
	chain.SetBranchAddress("pz", &pz);
	chain.SetBranchAddress("distTarg", &distTarg);
	chain.SetBranchAddress("zStart", &zStart);
	chain.SetBranchAddress("zLast", &zLast);
	chain.SetBranchAddress("dEdx", &dEdx);
	chain.SetBranchAddress("q", &qD);
	chain.SetBranchAddress("nVTPC1", &nVTPC1);
	chain.SetBranchAddress("nVTPC2", &nVTPC2);
	chain.SetBranchAddress("nGTPC", &nGTPC);
	chain.SetBranchAddress("nMTPC", &nMTPC);
	chain.SetBranchAddress("topology", &topology);
	chain.SetBranchAddress("nTOF", &nTOF);
	chain.SetBranchAddress("m2TOF", &m2TOF);
	chain.SetBranchAddress("tofScId", &tofScId);
	chain.SetBranchAddress("runNumber", &runNumber);
	
	chain.SetBranchAddress("pdgId", &pdgId);
	chain.SetBranchAddress("pxSim", &pxSim);
	chain.SetBranchAddress("pySim", &pySim);
	chain.SetBranchAddress("pzSim", &pzSim);
	chain.SetBranchAddress("xSim", &xSim);
	chain.SetBranchAddress("ySim", &ySim);
	chain.SetBranchAddress("zSim", &zSim);
	chain.SetBranchAddress("qSim", &qSim);

	
	int nEntries = chain.GetEntries();
	
	double zbor[7] = {0, 18, 36, 54, 72, 89.99, 90.01};
	
	int nth[6] = {15, 15, 15, 15, 15, 11};
	
	double thbor[6][17] = {{0, 20, 40, 60, 80, 100, 120, 140, 160, 180, 200, 220, 260, 300, 340, 380, 0},
							{0, 20, 40, 60, 80, 100, 120, 140, 160, 180, 200, 220, 260, 300, 340, 380, 0},
							{0, 20, 40, 60, 80, 100, 120, 140, 160, 180, 200, 220, 260, 300, 340, 380, 0},
							{0, 20, 40, 60, 80, 100, 120, 140, 160, 180, 200, 220, 260, 300, 340, 380, 0},
							{0, 20, 40, 60, 80, 100, 120, 140, 160, 180, 200, 220, 260, 300, 340, 380, 0},
							{0, 20, 40, 60, 80, 100, 140, 180, 220, 260, 300, 340, 0.0, 0.0, 0.0, 0.0, 0.0}};

	
	TH2D migmat("migmat", "", 92, 0, 92, 92, 0, 92);
	

	migmat.SetStats(kFALSE);

	for(int i = 0; i < nEntries; ++i){
	//for(int i = 0; i < 1000000; ++i){
		
		chain.GetEntry(i,1);
		
		if(!((i+1)%100000))
			cout << "Processing event # "  << i+1 << endl;
		
		//if(nCount > nTrack) break;	
		
		for(int j = 0; j < pdgId->size(); j++){
			double p = sqrt(px->at(j)*px->at(j) + py->at(j)*py->at(j) + pz->at(j)*pz->at(j));
			
			double zb = z->at(j) + 657.51;
			
			if(p < 0.2) continue;
			if(nTOF->at(j) < 1) continue;

			//if(zLast->at(j) < 580) continue;
			if(topology->at(j) == 1){
				if(nVTPC1->at(j) < 25) continue;
			}
			else if(topology->at(j) == 2){
				if(nVTPC2->at(j) < 25) continue;
			}
			else if(topology->at(j) == 9){
				if(nVTPC1->at(j) < 25) continue;
			}			
			else if(topology->at(j) == 10){
				if(nVTPC2->at(j) < 25) continue;
			}
			else if(topology->at(j) == 12){
				if(nGTPC->at(j) < 5) continue;
				if(nMTPC->at(j) < 30) continue;
			}
			else {
				if(nVTPC1->at(j) + nVTPC2->at(j) + nGTPC->at(j) < 20) continue;		
			}
			//cout << "radi2" << endl;
			int zId = -99;
			for(int k = 0; k < 6; k++){
				if(zb >= zbor[k] && zb <= zbor[k+1]){
					zId = k;
					break;
				}
			}
			if(zId < 0) continue;
			
			double theta = 1000*acos(pz->at(j)/sqrt(px->at(j)*px->at(j)+py->at(j)*py->at(j)+pz->at(j)*pz->at(j)));
			double phi = acos(px->at(j)/sqrt(px->at(j)*px->at(j)+py->at(j)*py->at(j)))*180/TMath::Pi();
			
			if(px->at(j)>0 && py->at(j) < 0){
				phi = -1*phi;
			}
			else if(px->at(j)<0 && py->at(j) < 0){
				phi = 360 - phi;
			}
			
			if(theta >= 0 && theta < 20){
				if((phi < -70 || phi > 70) && (phi < 110 || phi > 250)) continue;
			}
			else if(theta >= 20 && theta < 40){
				if((phi < -60 || phi > 60) && (phi < 140 || phi > 220)) continue;
			}
			else if(theta >= 40 && theta < 60){
				if((phi < -45 || phi > 35) && (phi < 145 || phi > 225)) continue;
			}
			else if(theta >= 60 && theta < 80){
				if((phi < -30 || phi > 30) && (phi < 150 || phi > 210)) continue;
			}
			else if(theta >= 80 && theta < 100){
				if((phi < -20 || phi > 20) && (phi < 160 || phi > 200)) continue;
			}
			else if(theta >= 100 && theta < 120){
				if((phi < -20 || phi > 20) && (phi < 160 || phi > 200)) continue;
			}
			else if(theta >= 120 && theta < 140){
				if((phi < -15 || phi > 15) && (phi < 165 || phi > 195)) continue;
			}
			else if(theta >= 140 && theta < 160){
				if((phi < -13 || phi > 13) && (phi < 167 || phi > 193)) continue;
			}
			else if(theta >= 160 && theta < 200){
				if((phi < -12 || phi > 12) && (phi < 168 || phi > 192)) continue;
			}
			else if(theta >= 200 && theta < 240){
				if((phi < -11 || phi > 11) && (phi < 169 || phi > 191)) continue;
			}
			else if(theta >= 240 && theta < 400){
				if((phi < -10 || phi > 10) && (phi < 170 || phi > 190)) continue;
			}	

			double r = sqrt(x->at(j)*x->at(j)+y->at(j)*y->at(j));

			double sigmaR = sqrt(x->at(j)*x->at(j)*xE->at(j)*xE->at(j)+y->at(j)*y->at(j)*yE->at(j)*yE->at(j))/r;
			if(distTarg->at(j)/sigmaR > 3) continue;			
			//if(distTarg->at(j) > 0.013) continue;

			if(zSim->at(j) > -567.50) continue;

						
			double thetaSim = 1000*acos(pzSim->at(j)/sqrt(pxSim->at(j)*pxSim->at(j)+pySim->at(j)*pySim->at(j)+pzSim->at(j)*pzSim->at(j)));
			double phiSim = acos(pxSim->at(j)/sqrt(pxSim->at(j)*pxSim->at(j)+pySim->at(j)*pySim->at(j)))*180/TMath::Pi();
			
			
			int indexz = -1;
			int sh = 0;
			for(int k = 0; k<6; k++){
			  if(z->at(j)+657.51 >= zbor[k] && z->at(j)+657.51 < zbor[k+1]){
			    
			    indexz = k;
			    break;
			  }
			  else{
			  	sh += nth[k]+1;
			  }
			}
			if(indexz < 0) continue;
			int indexth = nth[indexz];
			for(int k = 0; k<nth[indexz]; k++){
			  if(theta >= thbor[indexz][k] && theta < thbor[indexz][k+1]){
			    
			    indexth = k;
			    break;
			  }
			}			
			
			
			int indexzSim = -1;
			int shSim = 0;
			for(int k = 0; k<6; k++){
			  if(zSim->at(j)+657.51 >= zbor[k] && zSim->at(j)+657.51 < zbor[k+1]){
			    
			    indexzSim = k;
			    break;
			  }
			  else{
			  	shSim += nth[k]+1;
			  }
			}
			if(indexzSim < 0) continue;
			int indexthSim = nth[indexzSim];
			for(int k = 0; k<nth[indexzSim]; k++){
			  if(thetaSim >= thbor[indexzSim][k] && thetaSim < thbor[indexzSim][k+1]){
			    
			    indexthSim = k;
			    break;
			  }
			}	
			

			
			migmat.Fill(shSim + indexthSim + 0.5, sh + indexth + 0.5);

			
			

		}

	}
	

	for(int i = 0; i < 96; i++){
		double sum = 0;
		for(int j = 0; j < 96; j++){
			sum += migmat.GetBinContent(i+1, j+1);
			
		}	
		//cout << sum << endl;
		for(int j = 0; j < 96; j++){
			if(sum >0){
				migmat.SetBinContent(i+1, j+1, migmat.GetBinContent(i+1, j+1)/sum);
			}
			else{
				migmat.SetBinContent(i+1, j+1, 0);
			}
		}		
	}
	
	migmat.GetYaxis()->SetLabelSize(0.013);
	migmat.GetXaxis()->SetLabelSize(0.013);
	
	int ind = 0;
	for(int i = 0; i < 6; i++){
		for(int j = 0; j < nth[i]; j++){
			ind++;
			string label = ConvertToString(thbor[i][j], 0) + " - " + ConvertToString(thbor[i][j+1],0);
			migmat.GetXaxis()->SetBinLabel(ind, label.c_str());
			migmat.GetYaxis()->SetBinLabel(ind, label.c_str());
		}
		ind++;
		string outlabel = "> " + ConvertToString(thbor[i][nth[i]], 0);
		migmat.GetXaxis()->SetBinLabel(ind, outlabel.c_str());
		migmat.GetYaxis()->SetBinLabel(ind, outlabel.c_str());
	}

	for(int i = 0; i < 6; i++){
	
	}
	TCanvas c("c", "", 1500, 1500);
	c.cd();
	TLine *linesx[7];
	TLine *linesy[7];
	
	TLatex *latexx[6];
	TLatex *latexy[6];
	migmat.SetMaximum(1);
	migmat.SetMinimum(0);
	migmat.Draw("colz");
	double sh = 1;
	linesx[0] = new TLine(0, -5, 0, 96);
	linesy[0] = new TLine(-5, 0, 96, 0);
		
	linesx[0]->SetLineStyle(2);
	linesy[0]->SetLineStyle(2);
		
	linesx[0]->Draw("same");
	linesy[0]->Draw("same");
	for(int i = 1; i < 7; i++){
	
		string label = ConvertToString(i, 0);
		latexx[i-1] = new TLatex(sh -3 + (nth[i-1]+1.)/2., -10, label.c_str());
		latexy[i-1] = new TLatex(-10, sh-3 + (nth[i-1]+1.)/2., label.c_str());
		
		linesx[i] = new TLine(nth[i-1]+sh, -8, nth[i-1]+sh, 96);
		linesy[i] = new TLine(-8, nth[i-1]+sh, 96, nth[i-1]+sh);
		
		linesx[i]->SetLineStyle(2);
		linesy[i]->SetLineStyle(2);
		
		latexx[i-1]->SetTextSize(0.03);
		latexy[i-1]->SetTextSize(0.03);
		linesx[i]->Draw("same");
		linesy[i]->Draw("same");
		latexx[i-1]->Draw("same");
		latexy[i-1]->Draw("same");
		sh += (nth[i-1]+1);
		//if(i==6) sh+=1;
	}	
	
	TLatex ztitle1(98, -10, "z bin");
	TLatex ztitle2(-10, 99.1, "z");
	TLatex ztitle3(-11.5, 96.1, "bin");
	ztitle1.SetTextSize(0.03);
	ztitle2.SetTextSize(0.03);
	ztitle3.SetTextSize(0.03);
	ztitle1.Draw("same");
	ztitle2.Draw("same");
	ztitle3.Draw("same");
	TLatex thtitle1(97, -4, "#theta_{1}-#theta_{2} [mrad]");
	TLatex thtitle2(-4, 97, "#theta_{1} - #theta_{2} [mrad]");
	thtitle1.SetTextSize(0.015);
	thtitle2.SetTextSize(0.015);
	thtitle1.Draw("same");
	thtitle2.Draw("same");
	
	c.SaveAs("migmat.pdf");

	outFile.cd();

	migmat.Write();
	outFile.Close();
		
}





