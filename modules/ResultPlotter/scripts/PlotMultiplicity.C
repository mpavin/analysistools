#include "TTree.h"
#include "TBranch.h"
#include "TCanvas.h"
#include "TGraphErrors.h"
#include "TH1D.h"
#include "TH2D.h"
#include <TFile.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <cstdlib>
#include <stdexcept>
#include "TF1.h"
#include <string>
#include <vector>

using namespace std;

enum Results{
	eData,
	eMC,
	eMCPred,
	eMCCor,
	eMultiplicity,
	eCompWithPred
};

string yTitle[5] = {"N_{data}(#pi^{+})", "N_{mc}(#pi^{+})", "#frac{1}{N_{pot}}(dN/dp) [(GeV/c)^{-1}]", "C [%]", "#frac{1}{N_{pot}}(dN/dp) [(GeV/c)^{-1}]", ""};
string grTitle[2] = {"RST" , "WST"};
//********************************* PARAMETERS *****************************
double nEventsData = 4114051.;											//**
double nEventsMC = 30000000.;											//**
Results result = eMCPred;											//**
int zBin = 3;															//**
int nRows = 4;															//**
int canvasX = 1200;														//**
int canvasY = 800;														//**
//**************************************************************************

vector<vector<TGraphErrors> > mult;
TCanvas *bin;





void PlotMultiplicity(string list){

	double zMin;
	double zMax;
	double thetaMin;
	double thetaMax;
	vector<double> *p = NULL;
	vector<double> *pWidth = NULL;
	vector<double> *nData = NULL;
	vector<double> *nDataE = NULL;
	vector<double> *nMC = NULL;
	vector<double> *nMCE = NULL;
	vector<double> *nSim = NULL;
	int indexZ;
	int indexTh;	

	vector<string> job;
	ReadInputList(list, job);
	
	for(int i = 0; i < job.size(); i++){
		TFile input(job.at(i).c_str(), "READ");
		TTree *tree = (TTree*) input.Get("Multiplicity");
		
		tree->SetBranchAddress("zMin", &zMin);
		tree->SetBranchAddress("zMax", &zMax);
		tree->SetBranchAddress("thetaMin", &thetaMin);
		tree->SetBranchAddress("thetaMax", &thetaMax);
		tree->SetBranchAddress("p", &p);
		tree->SetBranchAddress("pWidth", &pWidth);
		tree->SetBranchAddress("nData", &nData);
		tree->SetBranchAddress("nDataE", &nDataE);
		tree->SetBranchAddress("nMC", &nMC);
		tree->SetBranchAddress("nMCE", &nMCE);
		tree->SetBranchAddress("nSim", &nSim);
		tree->SetBranchAddress("indexZ", &indexZ);
		tree->SetBranchAddress("indexTh", &indexTh);
		
		int nEntries = tree->GetEntries();
	
		vector<TGraphErrors> multTemp;
		
		string legTitle = "";
		for(int j = 0; j < nEntries; j++){
			tree->GetEntry(j);
			
			if(zBin != indexZ) continue;
			
			/*if(legTitle.size() < 2){
				ligTitle = ConvertToString(zMin,2) + " #leq z < " + ConvertToString(zMax,2) + " cm";
			}*/
			const int nPoints = p->size();
			double x[40];
			double xE[40];
			double y[40];
			double yE[40];
			//cerr << "radi1" << endl;
			for(int k = 0; k < nPoints; k++){
				x[k] = p->at(k);
				xE[k] = pWidth->at(k);
				
				//xE[k] = 0;
				switch (result){
					case eData:
						y[k] = nData->at(k);
						yE[k] = nDataE->at(k);
						if(yE[k] > 0.001) yE[k] = 0;
						break;
					case eMC:
						y[k] = nMC->at(k);
						yE[k] = nMCE->at(k);
						if(yE[k] > 0.001) yE[k] = 0;
						break;		
					case eMCPred:
						y[k] = nSim->at(k)/pWidth->at(k)/nEventsMC;
						yE[k] = 0;
						break;			
		
					case eMCCor:
						y[k] = 100*nMC->at(k)/nSim->at(k);
						yE[k] = 100*sqrt(TMath::Power(nMCE->at(k)/nSim->at(k),2));
						if(yE[k] > 0.1) yE[k] = 0;
						break;
						
					case eMultiplicity:
						y[k] = (1/nEventsData)*(nData->at(k)/pWidth->at(k))/(nMC->at(k)/nSim->at(k));
						if(y[k]>0.02) y[k] = 0;
						yE[k] = (1/nEventsData)*sqrt(TMath::Power((nDataE->at(k)/pWidth->at(k))/(nMC->at(k)/nSim->at(k)),2) + 
							TMath::Power(nMCE->at(k)*(nData->at(k)/pWidth->at(k))/(nMC->at(k)*nMC->at(k)/nSim->at(k)),2));
						if(yE[k] > 0.0005) yE[k] = 0;
						break;
					case eCompWithPred:
						y[k] = ((1/nEventsData)*(nData->at(k)/pWidth->at(k))/(nMC->at(k)/nSim->at(k)))/(nSim->at(k)/pWidth->at(k)/nEventsMC);
						double resE = (1/nEventsData)*sqrt(TMath::Power((nDataE->at(k)/pWidth->at(k))/(nMC->at(k)/nSim->at(k)),2) + 
							TMath::Power(nMCE->at(k)*(nData->at(k)/pWidth->at(k))/(nMC->at(k)*nMC->at(k)/nSim->at(k)),2));
						yE[k] = resE/(nSim->at(k)/pWidth->at(k)/nEventsMC);
						break;
				}
			}
			//cerr << "radi2" << endl;
			TGraphErrors graphTemp(nPoints, x, y, xE, yE);
			string title = ConvertToString(thetaMin,0) + " #leq #theta < " + ConvertToString(thetaMax,0) + " mrad";
			graphTemp.SetTitle(title.c_str());
			multTemp.push_back(graphTemp);
			
		}
		
		mult.push_back(multTemp);
		
	}
	//cout << "radi" << endl;
	TGaxis::SetMaxDigits(2);
	gStyle->SetTitleSize(0.08,"t"); 

	//leg->SetHeader(legTitle.c_str());
	
	for(int i = 0; i < mult.size(); i++){
		for(int j = 0; j < mult.at(i).size(); j++){
			//mult.at(i).at(j).SetMaximum(2);
			mult.at(i).at(j).SetMinimum(0.00000);
			mult.at(i).at(j).SetMarkerColor(i+1);
			mult.at(i).at(j).SetMarkerStyle(20+i);
			mult.at(i).at(j).SetLineWidth(2);
			mult.at(i).at(j).SetLineColor(i+1);
			mult.at(i).at(j).GetXaxis()->SetLabelSize(0.06);
			mult.at(i).at(j).GetXaxis()->SetTitleSize(0.06);
			mult.at(i).at(j).GetXaxis()->SetTitle("p [GeV/c]");
			mult.at(i).at(j).GetYaxis()->SetLabelSize(0.06);
			mult.at(i).at(j).GetYaxis()->SetTitleSize(0.06);
			mult.at(i).at(j).GetYaxis()->SetTitle(yTitle[result].c_str());
		}
		//
		
	}
	
	
	c = new TCanvas("c", "", canvasX, canvasY);
	
	int nGraphs = 0;
	
	
	for(int i = 0; i < mult.size(); i++){
		if (mult.at(i).size() > nGraphs){
			nGraphs = mult.at(i).size();
		}
	}
	
	div_t divresult;
	divresult = div(nGraphs, nRows);
	
	int nCol = divresult.quot;
	
	if(divresult.rem > 0){
		nCol++;
	}
	
	c->Divide(nCol, nRows);
	
	
	vector<int> maxXaxis;
	
	for(int i = 0; i < nGraphs; i++){
		maxXaxis.push_back(0);
		double max = 0;
		for(int j = 0; j < mult.size(); j++){
			if(i >= mult.at(j).size()) continue;
			if(mult.at(j).at(i).GetXaxis()->GetXmax() > max){
				max = mult.at(j).at(i).GetXaxis()->GetXmax();
				maxXaxis.at(i) = j;
			}	
		}
	}
	
	/*TLegend *leg = new TLegend(0.1, 0.1, 0.9, 0.9);
	for(int j = 0; j < mult.size(); j++){
		//leg->AddEntry(&mult.at(j).at(0), grTitle[j].c_str(), "ep");
	}*/
	for(int i = 0; i < nGraphs; i++){
		TPad *pad = c->cd(i+1);//->SetLogy(kTRUE);
		pad->SetMargin(0.15, 0.02, 0.15, 0.09);
		mult.at(maxXaxis.at(i)).at(i).Draw("AP");
		
		for(int j = 0; j < mult.size(); j++){
			
			if(i >= mult.at(j).size()) continue;
			if(j == maxXaxis.at(i)) continue;
			mult.at(j).at(i).Draw("Psame");
		}
		
		if(i == 16){
			//leg->Draw();
		}
		
		
		
	}
}




void ReadInputList(string list, vector<string> &job){

	ifstream ifs;
	string line;
	ifs.open(list.c_str());
		
	if (!ifs.is_open()){
		cerr << "Could not open infile. Exiting..." << endl;			
		exit (101);
	}
    
	while (getline(ifs, line)){
		if(line.at(0)=='#')
			continue;
		job.push_back(line);
	}
	ifs.close();	

}


std::string ConvertToString(double x, int prec){
	std::stringstream ss;
	std::string val;

	ss << std::fixed << std::setprecision(prec);
	ss << x;
	ss >> val;

	return val;
}
