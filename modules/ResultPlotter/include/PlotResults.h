#ifndef PLOTRESULTS_H
#define PLOTRESULTS_H


#include <iostream>
#include <vector>
#include <string>


using namespace std;

class PlotResults{

	public:
		PlotResults(string inputData, string inputMC): fInputDataFile(inputData), fInputMCFile(inputMC) {}
		
		void SetInputDataFile(string inputFile){fInputDataFile = inputFile;}
		void SetInputMCFile(string inputFile){fInputMCFile = inputFile;}
		
		string GetInputDataFile(){return fInputDataFile;}
		string GetInputMCFile(){return fInputMCFile;}
		void Run();
		
	private:
		
		string fInputDataFile;
		string fInputMCFile;

		
};
#endif
