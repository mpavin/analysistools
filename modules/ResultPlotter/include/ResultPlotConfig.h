#ifndef RESULTPLOTCONFIG_H
#define RESULTPLOTCONFIG_H
#include <iostream>
#include <vector>
#include <string>

#include <vector>



using namespace std;

class ResultPlotConfig{

	public:
		ResultPlotConfig(){Read();}

		string GetOutputDir(){return fOutput;}
		int GetParticleId(){return fPDGId;}
		bool IsRST(){return fIsRST;}
		vector<string>& GetMCJob();
		string GetMCList(){return fMCList;}
		
		void SetOutputDir(string value){fOutput = value;}
		void SetParticleId(int id){fPDGId = id;}
		void SetIsRST(bool val){fIsRST = val;}
		void SetMCList(string list){fMCList = list;}
		
	private:

		void Read();
		void ReadInputList();
		string fOutput;
		int fPDGId;
		string fMCList;
		bool fIsRST;
		vector<string> fJob;
};

#endif
