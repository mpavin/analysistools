#include <stdlib.h>
#include "ResultPlotConfig.h"
#include <fstream>
#include <fwk/CentralConfig.h>
#include <utl/Branch.h>
#include <utl/BranchIterator.h>
#include "CConfig.h"
#include <math.h>  

using namespace std;
using namespace fwk;
using namespace utl; 

void ResultPlotConfig::Read(){
	Branch spectrumPlot = cConfig.GetTopBranch("ResultPlotter");

	if(spectrumPlot.GetName() != "ResultPlotter"){
		cout << __FUNCTION__ << ": Wrong ResultPlotter config file! Top branch name doesn't match! Please check your configuration files! Exiting..." << endl;
		exit (EXIT_FAILURE);
	}

	fIsRST = true;
	fMCList = "";
	fPDGId = 211;
	for(BranchIterator it = spectrumPlot.ChildrenBegin(); it != spectrumPlot.ChildrenEnd(); ++it){
		Branch child = *it;
		
		if(child.GetName() == "output"){
			child.GetData(fOutput);
		}
		
		else if(child.GetName() == "mcList"){
			child.GetData(fMCList);
		}
		else if(child.GetName() == "isRST"){
			child.GetData(fIsRST);
		}
		else if(child.GetName() == "pdgId"){
			child.GetData(fPDGId);
		}		
		
	}

}

vector<string>& ResultPlotConfig::GetMCJob(){

	if(fJob.size() == 0)
		ReadInputList();

	return fJob;
}

void ResultPlotConfig::ReadInputList(){

	if (ResultPlotConfig::fMCList != " " ){
		ifstream ifs;
		string line;
		ifs.open(ResultPlotConfig::fMCList.c_str());
		
		if (!ifs.is_open()){
			cerr << "[ReadInputListFile] Could not open infile. Exiting..." << endl;			
			exit (101);
		}
    
		while (getline(ifs, line)){
			if(line.at(0)=='#')
				continue;
			ResultPlotConfig::fJob.push_back(line);
		}
		ifs.close();	
	
	}
	else cout << "WARNING: Empty file list! Jobs not created!" << endl;
}
