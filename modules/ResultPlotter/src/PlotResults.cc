#include <stdlib.h>
#include "PhaseSpace.h"
#include "PlotResults.h"
#include "ResultPlotConfig.h"
#include <sstream>

#include <TFile.h>
#include <TChain.h>
#include <TTree.h>
#include <sstream> 
#include "StrTools.h" 
#include "TreeDataTypes.h"

using namespace StrTools;


void PlotResults::Run(){

	bool data = false;
	bool mc = false;
	

	if(fInputDataFile.size() > 2) data = true;
	if(fInputMCFile.size() > 2) mc = true;

	if(!data && !mc){
		cerr << __FUNCTION__ << ": both mc and data file are not set! Exiting..." << endl;
		exit(EXIT_FAILURE);
	}

	ResultPlotConfig cfg;

	string name = cfg.GetOutputDir() + "Multiplicities_" + ConvertToString(cfg.GetParticleId()) + "_" + GetTimeString() + ".root";
	TFile output(name.c_str(), "RECREATE");



	TTree *outTree = new TTree("Multiplicity", "Multiplicity");
	
	vector<double> binLow;
	vector<double> binHigh;
	vector<double> p;
	vector<double> pWidth;
	vector<double> nData;
	vector<double> nDataE;
	vector<double> nMC;
	vector<double> nMCE;
	vector<double> nSim;
	vector<double> tofEff;
	vector<double> tofEffE;
	vector<double> pidE;
	vector<double> feeddownE;
	vector<double> recE;
	vector<double> pilossE;
	vector<double> targE;
	int indexZ;
	int indexTh;

	
	
	TFile *dataFile = NULL;
	TTree *dataTree = NULL;
	vector<double> binLowd;
	vector<double> binHighd;
	int indexZd, indexThd, indexPd;
	double npid[2];
	double npd[2];
	double nKd[2];
	double ned[2];
	 

	TFile *mcFile = NULL;
	TTree *mcTree = NULL;
	vector<double> binLowm;
	vector<double> binHighm;
	int indexZm, indexThm, indexPm;
	double npim[2];
	double npm[2];
	double nKm[2];
	double nem[2];	

	for(int i = 0; i < 3; i++){
		binLow.push_back(0);
		binHigh.push_back(0);
		binLowd.push_back(0);
		binHighd.push_back(0);
		binLowm.push_back(0);
		binHighm.push_back(0);
	}

	outTree->Branch("zMin", &(binLow.at(0)), "zMin/D");
	outTree->Branch("zMax", &(binHigh.at(0)), "zMax/D");
	outTree->Branch("thetaMin", &(binLow.at(1)), "thetaMin/D");
	outTree->Branch("thetaMax", &(binHigh.at(1)), "thetaMax/D");
	outTree->Branch("indexZ", &indexZ, "indexZ/I");
	outTree->Branch("indexTh", &indexTh, "indexTh/I");
	outTree->Branch("p", "vector<double>", &p);
	outTree->Branch("pWidth", "vector<double>", &pWidth);
	outTree->Branch("nData", "vector<double>", &nData);
	outTree->Branch("nDataE", "vector<double>", &nDataE);
	outTree->Branch("nMC", "vector<double>", &nMC);
	outTree->Branch("nMCE", "vector<double>", &nMCE);
	outTree->Branch("nSim", "vector<double>", &nSim);
	outTree->Branch("tofEff", "vector<double>", &tofEff);
	outTree->Branch("tofEffE", "vector<double>", &tofEffE);
	outTree->Branch("pidE", "vector<double>", &pidE);
	outTree->Branch("feeddownE", "vector<double>", &feeddownE);
	outTree->Branch("recE", "vector<double>", &recE);
	outTree->Branch("pilossE", "vector<double>", &pilossE);
	outTree->Branch("targE", "vector<double>", &targE);
	
	
		
	int nEntriesd = 0;
	int nEntriesm = 0;
	int nEntries = 0;
	
	if(data){
		dataFile = new TFile(fInputDataFile.c_str(), "READ");
		dataTree = (TTree*) dataFile->Get("Multiplicity");
		dataTree->SetBranchAddress("zMin", &(binLowd.at(0)));
		dataTree->SetBranchAddress("zMax", &(binHighd.at(0)));
		dataTree->SetBranchAddress("thetaMin", &(binLowd.at(1)));
		dataTree->SetBranchAddress("thetaMax", &(binHighd.at(1)));
		dataTree->SetBranchAddress("pMin", &(binLowd.at(2)));
		dataTree->SetBranchAddress("pMax", &(binHighd.at(2)));
		dataTree->SetBranchAddress("indexZ", &indexZd);
		dataTree->SetBranchAddress("indexTh", &indexThd);
		dataTree->SetBranchAddress("indexP", &indexPd);
		dataTree->SetBranchAddress("npi", &(npid[0]));
		dataTree->SetBranchAddress("npiE", &(npid[1]));
		dataTree->SetBranchAddress("np", &(npd[0]));
		dataTree->SetBranchAddress("npE", &(npd[1]));
		dataTree->SetBranchAddress("nK", &(nKd[0]));
		dataTree->SetBranchAddress("nKE", &(nKd[1]));
		dataTree->SetBranchAddress("ne", &(ned[0]));
		dataTree->SetBranchAddress("neE", &(ned[1]));
		nEntriesd = dataTree->GetEntries();
		nEntries = nEntriesd;
	}
	//cout << "radi" << endl;
	if(mc){
		mcFile = new TFile(fInputMCFile.c_str(), "READ");	
		mcTree = (TTree*) mcFile->Get("Multiplicity");
		mcTree->SetBranchAddress("zMin", &(binLowm.at(0)));
		mcTree->SetBranchAddress("zMax", &(binHighm.at(0)));
		mcTree->SetBranchAddress("thetaMin", &(binLowm.at(1)));
		mcTree->SetBranchAddress("thetaMax", &(binHighm.at(1)));
		mcTree->SetBranchAddress("pMin", &(binLowm.at(2)));
		mcTree->SetBranchAddress("pMax", &(binHighm.at(2)));
		mcTree->SetBranchAddress("indexZ", &indexZm);
		mcTree->SetBranchAddress("indexTh", &indexThm);
		mcTree->SetBranchAddress("indexP", &indexPm);
		mcTree->SetBranchAddress("npi", &(npim[0]));
		mcTree->SetBranchAddress("npiE", &(npim[1]));
		mcTree->SetBranchAddress("np", &(npm[0]));
		mcTree->SetBranchAddress("npE", &(npm[1]));
		mcTree->SetBranchAddress("nK", &(nKm[0]));
		mcTree->SetBranchAddress("nKE", &(nKm[1]));
		mcTree->SetBranchAddress("ne", &(nem[0]));
		mcTree->SetBranchAddress("neE", &(nem[1]));
		nEntriesm = mcTree->GetEntries();
		nEntries = nEntriesm;
	}
	
	
	if(data && mc){
		if(nEntriesd != nEntriesm){
			cerr << __FUNCTION__ << ": Number of bins in data and MC is not equal! Exiting..." << endl; 
			exit(EXIT_FAILURE);
		}
	}
	
	
	TChain mcChain("SimData");
	for(int i = 0; i < cfg.GetMCJob().size(); i++){
		mcChain.Add(cfg.GetMCJob().at(i).c_str());
	}
	
	BeamA beam;
	vector<double> *z = NULL;
	vector<double> *theta = NULL;
	vector<double> *pSim = NULL;
	vector<double> *pxSim = NULL;
	vector<int> *q = NULL;
	vector<int> *pdgId = NULL;
	vector<int> *iSimZ = NULL;
	vector<int> *iSimTh = NULL;
	vector<int> *iSimP = NULL;
	vector<int> *process = NULL;
	TBranch *b_aData = mcChain.GetBranch("Beam");
 	b_aData->SetAddress(&beam);
	mcChain.SetBranchAddress("z", &z);
	mcChain.SetBranchAddress("theta" , &theta);
	mcChain.SetBranchAddress("p", &pSim);
	mcChain.SetBranchAddress("px", &pxSim);
	mcChain.SetBranchAddress("q", &q);
	mcChain.SetBranchAddress("pdgId", &pdgId);
	mcChain.SetBranchAddress("indexZ", &iSimZ);
	mcChain.SetBranchAddress("indexTh", &iSimTh);
	mcChain.SetBranchAddress("indexP", &iSimP);
	mcChain.SetBranchAddress("process", &process);
	int nEntriesSim = mcChain.GetEntries();
	
	vector<double> simMult;
	for(int i = 0; i < nEntries; i++){
		simMult.push_back(0);
	}
	
	
	vector<vector<int> > binIds;

	for(int k = 0; k < nEntries; k++){

			if(data){
					dataTree->GetEntry(k);
					vector<int> temp;
					temp.push_back(k);
					temp.push_back(indexZd);
					temp.push_back(indexThd);
					temp.push_back(indexPd);	
					binIds.push_back(temp);					
			}
			else{
					mcTree->GetEntry(k);
					vector<int> temp;
					temp.push_back(k);
					temp.push_back(indexZd);
					temp.push_back(indexThd);
					temp.push_back(indexPd);	
					binIds.push_back(temp);		
			}
				
	}
	

	for(int i = 0; i < nEntriesSim; i++){
		mcChain.GetEntry(i);
		if(!((i+1)%100000))
			cout << "Processing MC event # " << i+1 << endl;
			
		for(int j = 0; j < z->size(); j++){

			if(pdgId->at(j) != cfg.GetParticleId()) continue;
			if(process->at(j) != 13) continue;
			if(iSimZ->at(j) < 1) continue;
			if(iSimTh->at(j) < 1) continue;
			if(iSimP->at(j) < 1) continue;
			/*if(cfg.IsRST()){
					if(q->at(j)*pxSim->at(j) < 0) continue;
			}
			else{
				if(q->at(j)*pxSim->at(j) >0 ) continue;
			}*/
			
			for(int k = 0; k < binIds.size(); k++){
				if(binIds.at(k).at(1)!=iSimZ->at(j)) continue;
				if(binIds.at(k).at(2)!=iSimTh->at(j)) continue;
				if(binIds.at(k).at(3)!=iSimP->at(j)) continue;

				simMult.at(binIds.at(k).at(0)) = simMult.at(binIds.at(k).at(0)) + 1;
				

				break;
			}
			
		}
	}
	
	
	int pdg = abs(cfg.GetParticleId());

	int thetaOld = 0; 
	
	if(data){
		dataTree->GetEntry(0);
		thetaOld = indexThd;
		for(int j = 0; j < 3; j++){
			binLow.at(j) = binLowd.at(j);
			binHigh.at(j) = binHighd.at(j);
		}
		indexZ = indexZd;
		indexTh = indexThd;
			
	}
	else if(mc){
		mcTree->GetEntry(0);
		thetaOld = indexThm;	
		for(int j = 0; j < 3; j++){
			binLow.at(j) = binLowm.at(j);
			binHigh.at(j) = binHighm.at(j);
		}
		indexZ = indexZm;
		indexTh = indexThm;
	}
	
	
	
	for(int i = 0; i < nEntries; i++){
	
		cout << "Processing bin # " << i+1 << endl;
		if(mc){
			mcTree->GetEntry(i);	
	
		}
		if(data){
			dataTree->GetEntry(i);
			if(thetaOld !=indexThd){
				thetaOld = indexThd;
				output.cd();
				outTree->Fill();
				nData.clear();
				nDataE.clear();
				nMC.clear();
				nMCE.clear();
				nSim.clear();
				p.clear();
				pWidth.clear();
				tofEff.clear();
				tofEffE.clear();
				pidE.clear();
				feeddownE.clear();
				recE.clear();
				pilossE.clear();
				targE.clear();
				for(int j = 0; j < 3; j++){
					binLow.at(j) = binLowd.at(j);
					binHigh.at(j) = binHighd.at(j);
				}
				indexZ = indexZd;
				indexTh = indexThd;

			}

			p.push_back((binLowd.at(2)+binHighd.at(2))/2.);
			pWidth.push_back((binHighd.at(2)-binLowd.at(2))/2.);			
		}
		else{
			if(thetaOld !=indexThm){
				thetaOld = indexThm;
				outTree->Fill();
				nData.clear();
				nDataE.clear();
				nMC.clear();
				nMCE.clear();
				nSim.clear();
				p.clear();
				pWidth.clear();
				tofEff.clear();
				tofEffE.clear();
				pidE.clear();
				feeddownE.clear();
				recE.clear();
				pilossE.clear();
				targE.clear();
				for(int j = 0; j < 3; j++){
					binLow.at(j) = binLowm.at(j);
					binHigh.at(j) = binHighm.at(j);
				}
				indexZ = indexZm;
				indexTh = indexThm;
			}

			p.push_back((binLowm.at(2)+binHighm.at(2))/2.);
			pWidth.push_back((binHighm.at(2)-binLowm.at(2))/2.);		
		}

		
		
		switch(pdg){
			case 11:
			{
				if(data){
					nData.push_back(ned[0]);
					
					nDataE.push_back(ned[1]);
				}
				else{
					nData.push_back(-999.);
					nDataE.push_back(-999.);					
				}
				
				if(mc){
					nMC.push_back(nem[0]);
					nMCE.push_back(nem[1]);
				}
				else{
					nMC.push_back(-999);
					nMCE.push_back(-999);				
				}
				break;
			}
			case 211:
			{
				if(data){
					nData.push_back(npid[0]);
					nDataE.push_back(npid[1]);
				}
				else{
					nData.push_back(-999.);
					nDataE.push_back(-999.);					
				}
				
				if(mc){
					nMC.push_back(npim[0]);
					nMCE.push_back(npim[1]);
				}
				else{
					nMC.push_back(-999);
					nMCE.push_back(-999);				
				}				
				break;
			}
			case 321:
			{
				if(data){
					nData.push_back(nKd[0]);
					nDataE.push_back(nKd[1]);
				}
				else{
					nData.push_back(-999.);
					nDataE.push_back(-999.);					
				}
				
				if(mc){
					nMC.push_back(nKm[0]);
					nMCE.push_back(nKm[1]);
				}
				else{
					nMC.push_back(-999);
					nMCE.push_back(-999);				
				}				
				break;
			}
			case 2212:
			{
				if(data){
					nData.push_back(npd[0]);
					nDataE.push_back(npd[1]);
				}
				else{
					nData.push_back(-999.);
					nDataE.push_back(-999.);					
				}
				
				if(mc){
					nMC.push_back(npm[0]);
					nMCE.push_back(npm[1]);
				}
				else{
					nMC.push_back(-999);
					nMCE.push_back(-999);				
				}				
				break;
			}
		}
		
		nSim.push_back(simMult.at(i));
		tofEff.push_back(1);
		tofEffE.push_back(0);
		pidE.push_back(0);
		feeddownE.push_back(0);
		recE.push_back(0.02);
		pilossE.push_back(0);
		targE.push_back(0);			
		
	}

	outTree->Write();
	output.Close();
}
