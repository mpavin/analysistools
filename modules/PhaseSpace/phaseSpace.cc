#include <vector>
#include <string>
#include <iostream>
#include "Functions.h"
#include <TH1D.h>
#include <TH2D.h>
#include <TFile.h>
#include <TChain.h>
#include <TTree.h>
#include <TMath.h>
#include <math.h>

using namespace std;


int main(){

	int data = 1;
	double scale;
	string inputFile;
	string add;
	
	if(data==1){
		scale = 1;
		inputFile = "listData.txt";
		add = "data_";	
	}
	else{
		//scale = 0.13870344;
		scale = 1;
		inputFile = "listMC.txt";
		add = "mc_";			
	}
	TChain chain("Data");
	
	string outName = add + "phi.root";
	TFile outFile(outName.c_str(), "RECREATE");
	ifstream ifs;
	string line;
	ifs.open(inputFile.c_str());
		
	if (!ifs.is_open()){
		cerr << "[ReadInputListFile] Could not open infile. Exiting..." << endl;			
		exit (101);
	}
    
	while (getline(ifs, line)){
		if(line.at(0)=='#')
			continue;
		chain.Add(line.c_str());
	}
	ifs.close();		



	BeamA beamD;
	vector<double> *x = NULL;
	vector<double> *xE = NULL;
	vector<double> *y = NULL;
	vector<double> *yE = NULL;
	vector<double> *z = NULL;
	vector<double> *px = NULL;
	vector<double> *py = NULL;
	vector<double> *pz = NULL;
	vector<double> *distTarg = NULL;
	vector<double> *zStart = NULL;
	vector<double> *dEdx = NULL;
	vector<int> *qD = NULL;
	vector<int> *nVTPC1 = NULL;
	vector<int> *nVTPC2 = NULL;
	vector<int> *nGTPC = NULL;
	vector<int> *nMTPC = NULL;
	vector<int> *topology = NULL;
	vector<int> *nTOF = NULL;
	vector<double> *m2TOF = NULL;	
	vector<int> *tofScId = NULL;
	vector<int> *pdgId = NULL;
	int runNumber;
	
	TBranch *b_aData = chain.GetBranch("Beam");
 	b_aData->SetAddress(&beamD);
	chain.SetBranchAddress("x", &x);
	chain.SetBranchAddress("xE", &xE);
	chain.SetBranchAddress("y", &y);
	chain.SetBranchAddress("yE", &yE);
	chain.SetBranchAddress("z", &z);
	chain.SetBranchAddress("px", &px);
	chain.SetBranchAddress("py", &py);
	chain.SetBranchAddress("pz", &pz);
	chain.SetBranchAddress("distTarg", &distTarg);
	chain.SetBranchAddress("zStart", &zStart);
	chain.SetBranchAddress("dEdx", &dEdx);
	chain.SetBranchAddress("q", &qD);
	chain.SetBranchAddress("nVTPC1", &nVTPC1);
	chain.SetBranchAddress("nVTPC2", &nVTPC2);
	chain.SetBranchAddress("nGTPC", &nGTPC);
	chain.SetBranchAddress("nMTPC", &nMTPC);
	chain.SetBranchAddress("topology", &topology);
	chain.SetBranchAddress("nTOF", &nTOF);
	chain.SetBranchAddress("m2TOF", &m2TOF);
	chain.SetBranchAddress("tofScId", &tofScId);
	chain.SetBranchAddress("runNumber", &runNumber);
	//chain.SetBranchAddress("pdgId", &pdgId);
	
	int nEntries = chain.GetEntries();
	
	double zbor[7] = {0, 18, 36, 54, 72, 89.99, 90.01};
	vector<TH2D> zph;
	TH1D zhist("zhist", "", 200, -5, 95);
	TH2D m2tofp("m2tofp", "", 200, 0, 8, 200, -0.6, 1.5);
	for(int i = 0; i < 6; i++){
	  string nm = "zbin" + ConvertToString(i+1., 0);
	  TH2D temp(nm.c_str(), "", 61, -0.3, 30.2, 20, 0, 400);
	  temp.GetXaxis()->SetTitle("p [GeV/c]");
	  temp.GetXaxis()->SetTitleOffset(0.9);
	  temp.GetXaxis()->SetTitleSize(0.06);
	  temp.GetXaxis()->SetLabelSize(0.06);
	  temp.GetYaxis()->SetTitle("#theta [mrad]");
          temp.GetYaxis()->SetTitleOffset(0.9);
          temp.GetYaxis()->SetTitleSize(0.06);
          temp.GetYaxis()->SetLabelSize(0.06);
	  temp.SetStats(kFALSE);
	  string title = ConvertToString(zbor[i],0) + " #leq z < " + ConvertToString(zbor[i+1],0) + " cm";
	  temp.SetTitle(title.c_str());
	  zph.push_back(temp);
	}

	
	cout << nEntries << endl;
	for(int i = 0; i < nEntries; ++i){
		
		chain.GetEntry(i,1);
		
		if(!((i+1)%100000))
			cout << "Processing event # "  << i+1 << endl;
		
		//if(nCount > nTrack) break;	
		for(int j = 0; j < px->size(); j++){
			//cout << "radi1" << endl;
			//cout << pdgId->at(j) << endl;
		  if(qD->at(j) < 0) continue;
			//if(pdgId->at(j) != 321) continue;
			//if(dEdx->at(j) > 1.3) continue;
			//if(qD->at(j)*px->at(j) > 0) continue;
			double p = sqrt(px->at(j)*px->at(j) + py->at(j)*py->at(j) + pz->at(j)*pz->at(j));
			
			double zb = z->at(j) + 657.51;
			
			if(p < 0.2) continue;
			if(nTOF->at(j) < 1) continue;

			//if(zLast->at(j) < 580) continue;
			if(topology->at(j) == 1){
				if(nVTPC1->at(j) < 25) continue;
			}
			else if(topology->at(j) == 2){
				if(nVTPC2->at(j) < 25) continue;
			}
			else if(topology->at(j) == 9){
				if(nVTPC1->at(j) < 25) continue;
			}			
			else if(topology->at(j) == 10){
				if(nVTPC2->at(j) < 25) continue;
			}
			else if(topology->at(j) == 12){
				if(nGTPC->at(j) < 5) continue;
				if(nMTPC->at(j) < 30) continue;
			}
			else {
				if(nVTPC1->at(j) + nVTPC2->at(j) + nGTPC->at(j) < 20) continue;		
			}
			//cout << "radi2" << endl;
			int zId = -99;
			for(int k = 0; k < 6; k++){
				if(zb >= zbor[k] && zb <= zbor[k+1]){
					zId = k;
					break;
				}
			}
			if(zId < 0) continue;
			
			double theta = 1000*acos(pz->at(j)/sqrt(px->at(j)*px->at(j)+py->at(j)*py->at(j)+pz->at(j)*pz->at(j)));
			double phi = acos(px->at(j)/sqrt(px->at(j)*px->at(j)+py->at(j)*py->at(j)))*180/TMath::Pi();
			
			if(px->at(j)>0 && py->at(j) < 0){
				phi = -1*phi;
			}
			else if(px->at(j)<0 && py->at(j) < 0){
				phi = 360 - phi;
			}
			
			if(theta >= 0 && theta < 20){
				if((phi < -70 || phi > 70) && (phi < 110 || phi > 250)) continue;
			}
			else if(theta >= 20 && theta < 40){
				if((phi < -60 || phi > 60) && (phi < 140 || phi > 220)) continue;
			}
			else if(theta >= 40 && theta < 60){
				if((phi < -45 || phi > 35) && (phi < 145 || phi > 225)) continue;
			}
			else if(theta >= 60 && theta < 80){
				if((phi < -30 || phi > 30) && (phi < 150 || phi > 210)) continue;
			}
			else if(theta >= 80 && theta < 100){
				if((phi < -20 || phi > 20) && (phi < 160 || phi > 200)) continue;
			}
			else if(theta >= 100 && theta < 120){
				if((phi < -20 || phi > 20) && (phi < 160 || phi > 200)) continue;
			}
			else if(theta >= 120 && theta < 140){
				if((phi < -15 || phi > 15) && (phi < 165 || phi > 195)) continue;
			}
			else if(theta >= 140 && theta < 160){
				if((phi < -13 || phi > 13) && (phi < 167 || phi > 193)) continue;
			}
			else if(theta >= 160 && theta < 200){
				if((phi < -12 || phi > 12) && (phi < 168 || phi > 192)) continue;
			}
			else if(theta >= 200 && theta < 240){
				if((phi < -11 || phi > 11) && (phi < 169 || phi > 191)) continue;
			}
			else if(theta >= 240 && theta < 400){
				if((phi < -10 || phi > 10) && (phi < 170 || phi > 190)) continue;
			}	

			double r = sqrt(x->at(j)*x->at(j)+y->at(j)*y->at(j));

			double sigmaR = sqrt(x->at(j)*x->at(j)*xE->at(j)*xE->at(j)+y->at(j)*y->at(j)*yE->at(j)*yE->at(j))/r;
			if(distTarg->at(j)/sigmaR > 3) continue;					
			zph.at(zId).Fill(p, theta);
			zhist.Fill(zb);
			m2tofp.Fill(p, m2TOF->at(j));
		}

	}
	//cout << nCount << " " << nEntries << endl;
	outFile.cd();


	for(int i = 0; i < 6; i++){
		//zph.at(i).Scale(0.136966667);
	  zph.at(i).Write();
	}
	zhist.Write();
	//thetaPhi.Write();
	m2tofp.Write();
	outFile.Close();
		
}





