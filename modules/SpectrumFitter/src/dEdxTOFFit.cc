#include <dEdxTOFFit.h>
#include "Functions.h"
#include <sstream>
#include <RooDataHist.h>
#include <RooGaussian.h>
#include <RooFitResult.h>
#include <RooExtendPdf.h>

dEdxTOFFit::dEdxTOFFit(TH2D* inputHist, dEdxResolution* dEdxRes, TOFResolution* tofRes, vector<double> &multRanges, Bin* bin){
	
	if(multRanges.size()!=4){
		cerr << __FUNCTION__ << ": vector<double> multRanges must have size equal to 4 and contain multiplicity range for protons, pions, kaons and electrons." << endl;
		exit(EXIT_FAILURE);
	}
 	fCanvas = NULL;
 	fResult = NULL;
 	fInput = inputHist;
 	fInput->Sumw2();
	fdEdxRes = dEdxRes;
	fTOFRes = tofRes;
	fBin = bin;
	
	fPars.push_back(&fMeanTOFp);
	fPars.push_back(&fMeanTOFpi);
	fPars.push_back(&fMeanTOFK);
	fPars.push_back(&fMeanTOFe);
	fPars.push_back(&fSigTOFp);
	fPars.push_back(&fSigTOFpi);
	fPars.push_back(&fSigTOFK);
	fPars.push_back(&fSigTOFe);
	
	fPars.push_back(&fMeandEdxp);
	fPars.push_back(&fMeandEdxpi);
	fPars.push_back(&fMeandEdxK);
	fPars.push_back(&fMeandEdxe);
	fPars.push_back(&fSigdEdxp);
	fPars.push_back(&fSigdEdxpi);
	fPars.push_back(&fSigdEdxK);
	fPars.push_back(&fSigdEdxe);
	
	fPars.push_back(&fAp);
	fPars.push_back(&fApi);
	fPars.push_back(&fAK);
	fPars.push_back(&fAe);
	
	
	//double borders[4] = {0,0,0,0};
	//FindFitBorders(borders);
	
 	fX = new RooRealVar("dEdx", "dE/dx", fInput->GetXaxis()->GetBinLowEdge(1),  fInput->GetXaxis()->GetBinLowEdge(fInput->GetNbinsX()));
 	fY = new RooRealVar("tof", "m^{2}_{TOF}", fInput->GetYaxis()->GetBinLowEdge(1),  fInput->GetYaxis()->GetBinLowEdge(fInput->GetNbinsY()));
 	 
 	string dataName = fInput->GetName();
 	dataName = "model" + dataName;
 	fData = new RooDataHist(dataName.c_str(), dataName.c_str(),RooArgSet(*fX, *fY), inputHist);

	double p[2] = {fBin->GetLowValue(3), fBin->GetHighValue(3)};
	double mom = fBin->GetMidValue(3);
	
	ax = new RooRealVar("ax", "ax", 2, 1,	3);
	ax->setConstant(kTRUE);
	mu1 = new RooRealVar("mu1", "mu1", 0.7, 0,	1);
	mu2 = new RooRealVar("mu2", "mu2", 0.7, 0,	1);
	mu3 = new RooRealVar("mu3", "mu3", 0.7, 0,	1);
	mu4 = new RooRealVar("mu4", "mu4", 0.7, 0,	1);
	//mu->setConstant(kTRUE);
	fSigdEdxp = new RooRealVar("sigdEdxp", "#sigma_{dE/dx}^{p}", fdEdxRes->GetWidth(p[0]/0.938, p[1]/0.938), 
 	(1-fdEdxRes->GetProtonWidthRange())*fdEdxRes->GetWidth(p[0]/0.938, p[1]/0.938),	(1+fdEdxRes->GetProtonWidthRange())*fdEdxRes->GetWidth(p[0]/0.938, p[1]/0.938));
 	fSigdEdxpi = new RooRealVar("sigdEdxpi", "#sigma_{dE/dx}^{#pi}", fdEdxRes->GetWidth(p[0]/0.139,p[1]/0.139), 
 	(1-fdEdxRes->GetPionWidthRange())*fdEdxRes->GetWidth(p[0]/0.139,p[1]/0.139),	(1+fdEdxRes->GetPionWidthRange())*fdEdxRes->GetWidth(p[0]/0.139,p[1]/0.139));
 	fSigdEdxe = new RooRealVar("sigdEdxe", "#sigma_{dE/dx}^{e}", fdEdxRes->GetWidth(p[0]/0.0005,p[1]/0.0005), 
 	(1-fdEdxRes->GetElectronWidthRange())*fdEdxRes->GetWidth(p[0]/0.0005,p[1]/0.0005), (1+fdEdxRes->GetElectronWidthRange())*fdEdxRes->GetWidth(p[0]/0.0005,p[1]/0.0005));
 	fSigdEdxK = new RooRealVar("sigdEdxK", "#sigma_{dE/dx}^{K}", fdEdxRes->GetWidth(p[0]/0.493,p[1]/0.493), 
 	(1-fdEdxRes->GetKaonWidthRange())*fdEdxRes->GetWidth(p[0]/0.493,p[1]/0.493), (1+fdEdxRes->GetKaonWidthRange())*fdEdxRes->GetWidth(p[0]/0.493,p[1]/0.493));

 	/*fSigdEdxp = new RooRealVar("sigdEdxp", "#sigma_{dE/dx}^{p}", fdEdxRes->GetWidth(p[0]/0.938, p[1]/0.938), 
 	(1-fdEdxRes->GetProtonWidthRange())*fdEdxRes->GetWidth(p[0]/0.938, p[1]/0.938),	(1+fdEdxRes->GetProtonWidthRange())*fdEdxRes->GetWidth(p[0]/0.938, p[1]/0.938));*/

 	/*fSigdEdxp = new RooRealVar("sigdEdxp", "#sigma_{dE/dx}^{p}",0.035, 0.035*0.8,	0.035*1.5);
 	fSigdEdxpi = new RooRealVar("sigdEdxpi", "#sigma_{dE/dx}^{#pi}", 0.035, 0.035*0.8,	0.035*1.5);
 	fSigdEdxe = new RooRealVar("sigdEdxe", "#sigma_{dE/dx}^{e}", 0.035, 0.035*0.8,	0.035*1.5);
 	fSigdEdxK = new RooRealVar("sigdEdxK", "#sigma_{dE/dx}^{K}", 0.035, 0.035*0.8,	0.035*1.5);*/

 	/*fSigdEdxp1 = new RooRealVar("sigdEdxp1", "#sigma1_{dE/dx}^{p}",0.045, 0.045*0.8,	0.045*1.2);
 	fSigdEdxpi1 = new RooRealVar("sigdEdxpi1", "#sigma1_{dE/dx}^{#pi}", 0.045, 0.045*0.8,	0.03*1.2);
 	fSigdEdxe1 = new RooRealVar("sigdEdxe1", "#sigma1_{dE/dx}^{e}", 0.045, 0.045*0.8,	0.045*1.2);
 	fSigdEdxK1 = new RooRealVar("sigdEdxK1", "#sigma1_{dE/dx}^{K}", 0.045, 0.045*0.8,	0.045*1.2);*/
 	 	
	/*fSigdEdxp1 = new RooPolyVar("sigdEdxp1", "#sigma_{dE/dx}^{p}1", *fSigdEdxp, *ax);
	fSigdEdxpi1 = new RooPolyVar("sigdEdxpi1", "#sigma_{dE/dx}^{p}1", *fSigdEdxpi, *ax);
	fSigdEdxe1 = new RooPolyVar("sigdEdxe1", "#sigma_{dE/dx}^{p}1", *fSigdEdxe, *ax);
	fSigdEdxK1 = new RooPolyVar("sigdEdxK1", "#sigma_{dE/dx}^{p}1", *fSigdEdxK, *ax);*/

 	fMeandEdxp = new RooRealVar("meandEdxp", "#mu_{dE/dx}^{p}", fdEdxRes->GetMean(mom/0.938), (1-fdEdxRes->GetProtonMeanRange())*fdEdxRes->GetMean(mom/0.938), 
 	(1+fdEdxRes->GetProtonMeanRange())*fdEdxRes->GetMean(mom/0.938));
 	fMeandEdxpi = new RooRealVar("meandEdxpi", "#mu_{dE/dx}^{#pi}", fdEdxRes->GetMean(mom/0.139), (1-fdEdxRes->GetPionMeanRange())*fdEdxRes->GetMean(mom/0.139), 
 	(1+fdEdxRes->GetPionMeanRange())*fdEdxRes->GetMean(mom/0.139));
 	fMeandEdxe = new RooRealVar("meandEdxe", "#mu_{dE/dx}^{e}", fdEdxRes->GetMean(mom/0.000511), (1-fdEdxRes->GetElectronMeanRange())*fdEdxRes->GetMean(mom/0.000511), 	
 	(1+fdEdxRes->GetElectronMeanRange())*fdEdxRes->GetMean(mom/0.000511));
 	fMeandEdxK = new RooRealVar("meandEdxK", "#mu_{dE/dx}^{K}", fdEdxRes->GetMean(mom/0.493), (1-fdEdxRes->GetKaonMeanRange())*(fdEdxRes->GetMean(mom/0.493)), 
 	(1+fdEdxRes->GetKaonMeanRange())*(fdEdxRes->GetMean(mom/0.493))); 
 	
 		
 	/*fMeanTOFp = new RooRealVar("meanTOFp", "#mu_{TOF}^{p}", fTOFRes->GetProtonMean(mom), (1-fTOFRes->GetProtonMeanRange())*fTOFRes->GetProtonMean(mom), 
 	(1+fTOFRes->GetProtonMeanRange())*fTOFRes->GetProtonMean(mom));
 	fMeanTOFpi = new RooRealVar("meanTOFpi", "#mu_{TOF}^{#pi}", fTOFRes->GetPionMean(mom), (1-fTOFRes->GetPionMeanRange())*fTOFRes->GetPionMean(mom), 
 	(1+fTOFRes->GetPionMeanRange())*fTOFRes->GetPionMean(mom));
 	//fMeanTOFe = new RooRealVar("meanTOFe", "#mu_{TOF}^{e}", fTOFRes->GetElectronMean(mom), (1-fTOFRes->GetElectronMeanRange())*fTOFRes->GetElectronMean(mom), 
 	//(1+fTOFRes->GetElectronMeanRange())*fTOFRes->GetElectronMean(mom));
 	fMeanTOFe = new RooRealVar("meanTOFe", "#mu_{TOF}^{e}", fTOFRes->GetElectronMean(mom), 
 	fTOFRes->GetElectronMean(mom)-fTOFRes->GetElectronMeanRange()*TMath::Abs(fTOFRes->GetElectronMean(mom)), 
 	fTOFRes->GetElectronMean(mom)+fTOFRes->GetElectronMeanRange()*TMath::Abs(fTOFRes->GetElectronMean(mom)));
 	fMeanTOFK = new RooRealVar("meanTOFK", "#mu_{TOF}^{K}", fTOFRes->GetKaonMean(mom), (1-fTOFRes->GetKaonMeanRange())*fTOFRes->GetKaonMean(mom), 
 	(1+fTOFRes->GetKaonMeanRange())*fTOFRes->GetKaonMean(mom));*/

	cout << "radi" << endl;
	double masspi[2] = {fTOFRes->GetPionMean(p[0]), fTOFRes->GetPionMean(p[1])};
	double masse[2] = {fTOFRes->GetElectronMean(p[0]), fTOFRes->GetElectronMean(p[1])};
	double massK[2] = {fTOFRes->GetKaonMean(p[0]), fTOFRes->GetKaonMean(p[1])} ;
	double massp[2] = {fTOFRes->GetProtonMean(p[0]), fTOFRes->GetProtonMean(p[1])};
	
	if(fTOFRes->GetPionMean(p[0]) > fTOFRes->GetPionMean(p[1])){
		masspi[0] = fTOFRes->GetPionMean(p[1]);
		masspi[1] = fTOFRes->GetPionMean(p[0]);
	}
	if(fTOFRes->GetElectronMean(p[0]) > fTOFRes->GetElectronMean(p[1])){
		masse[0] = fTOFRes->GetElectronMean(p[1]);
		masse[1] = fTOFRes->GetElectronMean(p[0]);
	}
	if(fTOFRes->GetKaonMean(p[0]) > fTOFRes->GetKaonMean(p[1])){
		massK[0] = fTOFRes->GetKaonMean(p[1]);
		massK[1] = fTOFRes->GetKaonMean(p[0]);
	}
	if(fTOFRes->GetProtonMean(p[0]) > fTOFRes->GetProtonMean(p[1])){
		massp[0] = fTOFRes->GetProtonMean(p[1]);
		massp[1] = fTOFRes->GetProtonMean(p[0]);
	}
 	fMeanTOFp = new RooRealVar("meanTOFp", "#mu_{TOF}^{p}", fTOFRes->GetProtonMean(mom), (1-fTOFRes->GetProtonMeanRange())*massp[0], 
 	(1+fTOFRes->GetProtonMeanRange())*massp[1]);
 	fMeanTOFpi = new RooRealVar("meanTOFpi", "#mu_{TOF}^{#pi}", fTOFRes->GetPionMean(mom), (1-fTOFRes->GetPionMeanRange())*masspi[0], 
 	(1+fTOFRes->GetPionMeanRange())*masspi[1]);
 	//fMeanTOFe = new RooRealVar("meanTOFe", "#mu_{TOF}^{e}", fTOFRes->GetElectronMean(mom), -0.01, 
 	//0.01);
 	fMeanTOFe = new RooRealVar("meanTOFe", "#mu_{TOF}^{e}", fTOFRes->GetElectronMean(mom), fTOFRes->GetElectronMean(mom)-fTOFRes->GetElectronMeanRange()*abs(fTOFRes->GetElectronMean(mom)), 
 	fTOFRes->GetElectronMean(mom)+fTOFRes->GetElectronMeanRange()*abs(fTOFRes->GetElectronMean(mom))); 	
 	fMeanTOFK = new RooRealVar("meanTOFK", "#mu_{TOF}^{K}", fTOFRes->GetKaonMean(mom), (1-fTOFRes->GetKaonMeanRange())*massK[0], 
 	(1+fTOFRes->GetKaonMeanRange())*massK[1]); 	
cout << "radi1" << endl;
 	/*fSigTOFp = new RooRealVar("sigTOFp", "#sigma_{TOF}^{p}", fTOFRes->GetProtonWidth(mom), (1-multRanges.at(1))*fTOFRes->GetProtonWidth(mom), 
 	(1+multRanges.at(1))*fTOFRes->GetProtonWidth(mom));
 	fSigTOFpi = new RooRealVar("sigTOFpi", "#sigma_{TOF}^{#pi}", fTOFRes->GetPionWidth(mom), (1-multRanges.at(0))*fTOFRes->GetPionWidth(mom), 
 	(1+multRanges.at(0))*fTOFRes->GetPionWidth(mom));
 	fSigTOFe = new RooRealVar("sigTOFe", "#sigma_{TOF}^{e}", fTOFRes->GetElectronWidth(mom), (1-multRanges.at(3))*fTOFRes->GetElectronWidth(mom), 
 	(1+multRanges.at(3))*fTOFRes->GetElectronWidth(mom));
 	fSigTOFK = new RooRealVar("sigTOFK", "#sigma_{TOF}^{K}", fTOFRes->GetPionWidth(mom), (1-multRanges.at(2))*fTOFRes->GetPionWidth(mom), 
 	(1+multRanges.at(2))*fTOFRes->GetPionWidth(mom)); 	*/
 	
 	fSigTOFp = new RooRealVar("sigTOFp", "#sigma_{TOF}^{p}", fTOFRes->GetProtonWidth(mom), (1-fTOFRes->GetProtonWidthRange())*fTOFRes->GetProtonWidth(p[0]), 
 	(1+fTOFRes->GetProtonWidthRange())*fTOFRes->GetProtonWidth(p[1]));
 	fSigTOFpi = new RooRealVar("sigTOFpi", "#sigma_{TOF}^{#pi}", fTOFRes->GetPionWidth(mom), (1-fTOFRes->GetElectronWidthRange())*fTOFRes->GetPionWidth(p[0]), 
 	(1+fTOFRes->GetPionWidthRange())*fTOFRes->GetPionWidth(p[1]));
 	fSigTOFe = new RooRealVar("sigTOFe", "#sigma_{TOF}^{e}", fTOFRes->GetElectronWidth(mom), (1-fTOFRes->GetElectronWidthRange())*fTOFRes->GetElectronWidth(p[0]), 
 	(1+fTOFRes->GetElectronWidthRange())*fTOFRes->GetElectronWidth(p[1]));
 	fSigTOFK = new RooRealVar("sigTOFK", "#sigma_{TOF}^{K}", fTOFRes->GetPionWidth(mom), (1-fTOFRes->GetKaonWidthRange())*fTOFRes->GetPionWidth(p[0]), 
 	(1+fTOFRes->GetKaonWidthRange())*fTOFRes->GetPionWidth(p[1])); 	
	//fSigTOFK = new RooRealVar("sigTOFK", "#sigma_{TOF}^{K}", fTOFRes->GetPionWidth(mom), (1-fTOFRes->GetKaonWidthRange())*fTOFRes->GetPionWidth(p[0]), 
 	//(1+fTOFRes->GetKaonWidthRange())*fTOFRes->GetPionWidth(p[1])); 	 	 
 	
 	double Ap = DoStep(fMeandEdxp->getValV(), fMeanTOFp->getValV(), (fSigdEdxp->getValV()*3), (fSigTOFp->getValV()*3), inputHist);
 	double Api = DoStep(fMeandEdxpi->getValV(), fMeanTOFpi->getValV(), (fSigdEdxpi->getValV()*3), (fSigTOFpi->getValV()*3), inputHist);
 	double Ae = DoStep(fMeandEdxe->getValV(), fMeanTOFe->getValV(), (fSigdEdxe->getValV()*3), (fSigTOFe->getValV()*3), inputHist);
 	double AK = DoStep(fMeandEdxK->getValV(), fMeanTOFK->getValV(), (fSigdEdxK->getValV()*3), (fSigTOFK->getValV()*3), inputHist);

	double nEntries = fInput->GetEntries()-fInput->GetBinContent(0)-fInput->GetBinContent(fInput->GetNbinsX()+1);
	if(Api == 0) Api = 5;
	if(Ap == 0) Ap = 5;
	if(AK == 0) AK = 5;
	if(Ae == 0) Ae = 5;
	double sumParts = 1 + Ap/Api + Ae/Api + AK/Api;

 	
 	fAp = new RooRealVar("Ap", "A_{p}", nEntries/3, 0, nEntries);
 	fApi = new RooRealVar("Api", "A_{#pi}", nEntries/2, 0, nEntries);
 	fAe = new RooRealVar("Ae", "A_{e}", nEntries/2.5, 0, nEntries);
 	fAK = new RooRealVar("AK", "A_{K}", nEntries/6,0., 0.3*nEntries);
 	
 	/*fAp = new RooRealVar("Ap", "A_{p}", Ap, 0.7*Ap, 1.3*Ap);
 	fApi = new RooRealVar("Api", "A_{#pi}", Api, 0.7*Api, 1.3*Api);
 	fAe = new RooRealVar("Ae", "A_{e}", Ae, 0.7*Ae, 1.3*Ae);
 	fAK = new RooRealVar("AK", "A_{K}", AK ,0.7*AK, 1.3*AK);*/
 	
}

dEdxTOFFit::~dEdxTOFFit(){
	if(fCanvas != NULL) {
		delete fCanvas;
		fCanvas = NULL;
	}
	if(fResult != NULL) {
		delete fResult;
		fResult = NULL;
	}
	fPars.clear();
	
	if(fSigdEdxp != NULL){
		delete fSigdEdxp;
		fSigdEdxp = NULL;
	}
	if(fSigdEdxpi != NULL){
		delete fSigdEdxpi;
		fSigdEdxpi = NULL;
	}
	if(fSigdEdxK != NULL){
		delete fSigdEdxK;
		fSigdEdxK = NULL;
	}
	if(fSigdEdxe != NULL){
		delete fSigdEdxe;
		fSigdEdxe = NULL;
	}
	
	if(fMeandEdxp != NULL){
		delete fMeandEdxp;
		fMeandEdxp = NULL;
	}
	if(fMeandEdxpi != NULL){
		delete fMeandEdxpi;
		fMeandEdxpi = NULL;
	}
	if(fMeandEdxK != NULL){
		delete fMeandEdxK;
		fMeandEdxK = NULL;
	}
	if(fMeandEdxe != NULL){
		delete fMeandEdxe;
		fMeandEdxe = NULL;
	}
	
	if(fMeanTOFp != NULL){
		delete fMeanTOFp;
		fMeanTOFp = NULL;
	}
	if(fMeanTOFpi != NULL){
		delete fMeanTOFpi;
		fMeanTOFpi = NULL;
	}
	if(fMeanTOFK != NULL){
		delete fMeanTOFK;
		fMeanTOFK = NULL;
	}
	if(fMeanTOFe != NULL){
		delete fMeanTOFe;
		fMeanTOFe = NULL;
	}
	
	if(fSigTOFp != NULL){
		delete fSigTOFp;
		fSigTOFp = NULL;
	}
	if(fSigTOFpi != NULL){
		delete fSigTOFpi;
		fSigTOFpi = NULL;
	}
	if(fSigTOFK != NULL){
		delete fSigTOFK;
		fSigTOFK = NULL;
	}
	if(fSigTOFe != NULL){
		delete fSigTOFe;
		fSigTOFe = NULL;
	}
	
	if(fAp != NULL){
		delete fAp;
		fAp = NULL;
	}
	if(fApi != NULL){
		delete fApi;
		fApi = NULL;
	}
	if(fAK != NULL){
		delete fAK;
		fAK = NULL;
	}
	if(fAe != NULL){
		delete fAe;
		fAe = NULL;
	}
	
	if(fX != NULL){
		delete fX;
		fX = NULL;
	}
	if(fY != NULL){
		delete fY;
		fY = NULL;
	}
	if(fData != NULL){
		delete fData;
		fData = NULL;
	}
}



int dEdxTOFFit::FitGaus(TH1D& hist, double& npart, double& npartE){
	
 	RooRealVar x("m2tof", "m^{2}_{TOF}", hist.GetXaxis()->GetBinLowEdge(1),  hist.GetXaxis()->GetBinUpEdge(hist.GetNbinsX()));
 	 
 	RooDataHist data("ptof", "", RooArgSet(x), &hist);	
	double mean = hist.GetMean();
	double rms = hist.GetRMS();
	double nEnt = hist.GetEntries()-hist.GetBinContent(0)-hist.GetBinContent(hist.GetNbinsX()+1);
	if(nEnt < 0) nEnt = 1;
	RooRealVar multTOF("multTOF", "#mu_{tof}^{p}", nEnt, nEnt-0.6*nEnt, nEnt+0.6*nEnt);
	RooRealVar meanTOF("meanTOF", "#mu_{tof}^{p}", mean, mean-0.1*mean, mean+0.1*mean);
	RooRealVar widthTOF("widthTOF", "#sigma_{tof}^{p}", rms, rms-0.6*rms, rms+0.5*rms);
	
	RooGaussian tof("tof", "(m^{2}_{TOF})_{p}", x, meanTOF, widthTOF);
	
	RooExtendPdf model("model", "model", tof, multTOF);
	//RooFitResult *result = model.fitTo(data,Save(),Extended());	
	
	npart = multTOF.getValV();
	npartE = multTOF.getError();
	
	//return result->status();
	return 1;
}

