#include <stdlib.h>
#include "FitSpectrum.h"
#include "SpectrumFitConfig.h"
#include "dEdxTOFFit.h"
#include <sstream>
#include <TFile.h>
#include <TTree.h>
#include <TH2D.h>
#include <TCanvas.h>
#include <TKey.h>
#include <TDirectory.h>
#include <dEdxResolution.h>
#include <TOFResolution.h>
#include <algorithm> 
#include "Bin.h"
#include "StrTools.h"
#include <TMath.h>

using namespace StrTools;
void FitSpectrum::Run(){

	SpectrumFitConfig cfg;

	TFile input(fInputFile.c_str(), "READ");

	if (input.IsZombie()){
		cout  << "[ERROR] Error opening file!" << endl;
		exit(-1);
	}	

	TTree *tree = (TTree*) input.Get("dEdx_m2tof");

	vector<double> binLow;
	vector<double> binHigh;
	
	for(int i = 0; i < 3; i++){
		binLow.push_back(0);
		binHigh.push_back(0);
	}	

	
	int indexZ;
	int indexTh;
	int indexP;
	
	double npi[2];
	double np[2];
	double nK[2];
	double ne[2];
	
	vector<double> *dEdxBin = NULL;
	vector<double> *m2tofBin = NULL;
	
	
	tree->SetBranchAddress("zMin", &(binLow.at(0)));
	tree->SetBranchAddress("zMax", &(binHigh.at(0)));
	tree->SetBranchAddress("thetaMin", &(binLow.at(1)));
	tree->SetBranchAddress("thetaMax", &(binHigh.at(1)));
	tree->SetBranchAddress("pMin", &(binLow.at(2)));
	tree->SetBranchAddress("pMax", &(binHigh.at(2)));
	tree->SetBranchAddress("indexZ", &indexZ);
	tree->SetBranchAddress("indexTh", &indexTh);
	tree->SetBranchAddress("indexP", &indexP);
	tree->SetBranchAddress("dEdx", &dEdxBin);
	tree->SetBranchAddress("m2ToF", &m2tofBin);
	
	int nEntries = tree->GetEntries();
	
	string name = cfg.GetOutputDir() + "/SpectrumFit_" + GetTimeString() + ".root";
	TFile output(name.c_str(), "RECREATE");
	
	TTree *outTree = new TTree("Multiplicity", "Multiplicity");
	outTree->Branch("zMin", &(binLow.at(0)), "zMin/D");
	outTree->Branch("zMax", &(binHigh.at(0)), "zMax/D");
	outTree->Branch("thetaMin", &(binLow.at(1)), "thetaMin/D");
	outTree->Branch("thetaMax", &(binHigh.at(1)), "thetaMax/D");
	outTree->Branch("pMin", &(binLow.at(2)), "pMin/D");
	outTree->Branch("pMax", &(binHigh.at(2)), "pMax/D");
	outTree->Branch("indexZ", &indexZ, "indexZ/I");
	outTree->Branch("indexTh", &indexTh, "indexTh/I");
	outTree->Branch("indexP", &indexP, "indexP/I");
	outTree->Branch("npi", &(npi[0]), "npi/D");
	outTree->Branch("npiE", &(npi[1]), "npiE/D");
	outTree->Branch("np", &(np[0]), "np/D");
	outTree->Branch("npE", &(np[1]), "npE/D");
	outTree->Branch("nK", &(nK[0]), "nK/D");
	outTree->Branch("nKE", &(nK[1]), "nKE/D");
	outTree->Branch("ne", &(ne[0]), "ne/D");
	outTree->Branch("neE", &(ne[1]), "neE/D");
		
	vector<double> tofRanges;
	tofRanges.push_back(cfg.GetPionTOFMeanRange());
	tofRanges.push_back(cfg.GetPionTOFWidthRange());
	tofRanges.push_back(cfg.GetProtonTOFMeanRange());
	tofRanges.push_back(cfg.GetProtonTOFWidthRange());
	tofRanges.push_back(cfg.GetKaonTOFMeanRange());
	tofRanges.push_back(cfg.GetKaonTOFWidthRange());
	tofRanges.push_back(cfg.GetElectronTOFMeanRange());
	tofRanges.push_back(cfg.GetElectronTOFWidthRange());

	vector<double> dEdxRanges;
	dEdxRanges.push_back(cfg.GetPiondEdxMeanRange());
	dEdxRanges.push_back(cfg.GetPiondEdxWidthRange());
	dEdxRanges.push_back(cfg.GetProtondEdxMeanRange());
	dEdxRanges.push_back(cfg.GetProtondEdxWidthRange());
	dEdxRanges.push_back(cfg.GetKaondEdxMeanRange());
	dEdxRanges.push_back(cfg.GetKaondEdxWidthRange());
	dEdxRanges.push_back(cfg.GetElectrondEdxMeanRange());
	dEdxRanges.push_back(cfg.GetElectrondEdxWidthRange());
	
	vector<double> multRanges;
	multRanges.push_back(cfg.GetPionMultiplicityRange());
	multRanges.push_back(cfg.GetProtonMultiplicityRange());
	multRanges.push_back(cfg.GetKaonMultiplicityRange());
	multRanges.push_back(cfg.GetElectronMultiplicityRange());
	
	dEdxResolution *dEdxRes = new dEdxResolution(cfg.GetdEdxMap(), dEdxRanges);

	TOFResolution *tofRes = new TOFResolution(cfg.GetTOFMap(), tofRanges);

	int nBinsdEdx ;
	int nBinsTOF ;
	double mindEdx = 0;
	double maxdEdx = 0;
	
	if(cfg.IsTest()){
	
		if(cfg.GetTestHistoId() > nEntries){
			cerr << __FUNCTION__ << ": Test bin number is larger than total number of bins! Exiting..." << endl;
			exit(EXIT_FAILURE);		
		}

		tree->GetEntry(cfg.GetTestHistoId()-1);
		Bin bin(binLow, binHigh);
		
		double dEdxRange[4] = {dEdxRes->GetMean(bin.GetMidValue(3)/0.932), dEdxRes->GetMean(bin.GetMidValue(3)/0.1396), dEdxRes->GetMean(bin.GetMidValue(3)/0.00051), dEdxRes->GetMean(bin.GetMidValue(3)/0.4937)}; 
		std:sort(dEdxRange, dEdxRange+4);

		double mindEdx = dEdxRange[0] - 5*dEdxRes->GetWidth(bin.GetMidValue(3)/0.932);
		double maxdEdx = dEdxRange[3] + 5*dEdxRes->GetWidth(bin.GetMidValue(3)/0.000511);	
			
		double minTOF = tofRes->GetElectronMean(bin.GetMidValue(3)) - 3.5*tofRes->GetElectronWidth(bin.GetHighValue(3));
		//if(minTOF > -0.03) minTOF = -0.3;
		double maxTOF = 0;
		
		bool fitP = cfg.StartFittingProtons(bin.GetMidValue(3));
		bool fitK = cfg.StartFittingKaons(bin.GetMidValue(3));
		if(fitP){
			maxTOF = tofRes->GetProtonMean(bin.GetMidValue(3)) + 3.5*tofRes->GetProtonWidth(bin.GetHighValue(3));
		}
		else if(fitK){
			maxTOF = tofRes->GetKaonMean(bin.GetMidValue(3)) + 3.5*tofRes->GetKaonWidth(bin.GetHighValue(3));
		}
		else{
			maxTOF = tofRes->GetPionMean(bin.GetMidValue(3)) + 3.5*tofRes->GetPionWidth(bin.GetHighValue(3));
		}

		if(bin.GetMidValue(3) < 0.5 && maxdEdx > 5){
			maxdEdx = 4;
			//maxTOF = 0.5;
		}
		//cout << "radi1 " << dEdxBin->size() <<endl;
		nBinsdEdx = OptimizeHistoBinSize(dEdxBin, mindEdx, maxdEdx);
		nBinsTOF = OptimizeHistoBinSize(m2tofBin, minTOF, maxTOF);
		//cout << "radi2" << endl;

		if(nBinsdEdx < cfg.GetNBinsdEdx()) {
			nBinsdEdx = cfg.GetNBinsdEdx();
		}
		if(nBinsTOF < cfg.GetNBinsTOF()){
			nBinsTOF = cfg.GetNBinsTOF();
		}	
		string name = "bin_" + ConvertToString(indexZ) + "_" + ConvertToString(indexTh) + "_" + ConvertToString(indexP);
		string title = ConvertToString(binLow.at(0),0) + " #leq z < " + ConvertToString(binHigh.at(0),0) + " cm, " + 
		ConvertToString(binLow.at(1),0) + " #leq #theta < " + ConvertToString(binHigh.at(1),0) + " mrad, " + 
		ConvertToString(binLow.at(2),2) + " #leq p < " + ConvertToString(binHigh.at(2),2) + " GeV/c";
		TH2D hist(name.c_str(), "",  nBinsdEdx, mindEdx, maxdEdx, nBinsTOF, minTOF, maxTOF);	
		hist.SetTitle(title.c_str());
		int count = 0;
		for(int j = 0; j < dEdxBin->size(); j++){
		
		if(m2tofBin->at(j) > 0.6) continue;
		if(m2tofBin->at(j) > 0.09) count++;
			hist.Fill(dEdxBin->at(j), m2tofBin->at(j));
		}
		cout << "NK = " << count << endl;
		if(!fitP){
			string namep = name + "_tofp";
			TH1D histptof(namep.c_str(), "",  nBinsTOF, tofRes->GetProtonMean(bin.GetMidValue(3)) - 4*tofRes->GetProtonWidth(bin.GetMidValue(3)), 
			tofRes->GetProtonMean(bin.GetMidValue(3)) + 4*tofRes->GetProtonWidth(bin.GetMidValue(3)));
			for(int j = 0; j < m2tofBin->size(); j++){
				histptof.Fill( m2tofBin->at(j));
			}					
			double nprot = 0;
			double nprotE = 0;
			if(!dEdxTOFFit::FitGaus(histptof, nprot, nprotE)){
				cerr << "WARNING: Proton tof fit did not converge! "  << nprot << " " << nprotE <<  endl;
			}
			np[0] = nprot;
			np[1] = nprotE;
					
		}
		if(!fitK){
			
			string nameK = name + "_tofK";
			TH1D histKtof(nameK.c_str(), "",  nBinsTOF, tofRes->GetKaonMean(bin.GetMidValue(3)) - 4*tofRes->GetKaonWidth(bin.GetMidValue(3)), 
			tofRes->GetKaonMean(bin.GetMidValue(3)) + 4*tofRes->GetKaonWidth(bin.GetMidValue(3)));
			for(int j = 0; j < m2tofBin->size(); j++){
				histKtof.Fill( m2tofBin->at(j));
			}					
			double nkaon = 0;
			double nkaonE = 0;
			if(!dEdxTOFFit::FitGaus(histKtof, nkaon, nkaonE)){
				cerr << "WARNING: Kaon tof fit did not converge! "  << nkaon << " " << nkaonE <<  endl;
			}
			nK[0] = nkaon;
			nK[1] = nkaonE;
					
		}
													
		dEdxTOFFit fS(&hist, dEdxRes, tofRes, multRanges, &bin);				

		if(fitP){
			fS.Fit(false, 15);
		}
		else if(fitK){
			fS.Fit(false, 11);
		}
		else{
			fS.Fit(false, 9);
		}
		output.cd();
		fS.GetCanvas()->Write();
		fS.GetCanvas()->SaveAs("fit.pdf");	
	}
	else{

		for(int i = 0; i < nEntries; i++){
			if(!((i+1)%10))
				cerr << "Fitting bin " << i+1 << endl; 
			tree->GetEntry(i);
			
			Bin bin(binLow, binHigh);

			//cerr << "radi1!" << endl;
			double dEdxRange[4] = {dEdxRes->GetMean(bin.GetMidValue(3)/0.932), dEdxRes->GetMean(bin.GetMidValue(3)/0.1396), 
			dEdxRes->GetMean(bin.GetMidValue(3)/0.00051), dEdxRes->GetMean(bin.GetMidValue(3)/0.4937)}; 
			std::sort(dEdxRange, dEdxRange+4);


			double mindEdx = dEdxRange[0] - 4*dEdxRes->GetWidth(bin.GetMidValue(3)/0.000511);
			double maxdEdx = dEdxRange[3] + 5*dEdxRes->GetWidth(bin.GetMidValue(3)/0.000511);
			if(bin.GetMidValue(3) < 0.5 && maxdEdx > 5){
				maxdEdx = 4;
			}	
			
			double minTOF = tofRes->GetElectronMean(bin.GetMidValue(3)) - 5*tofRes->GetElectronWidth(bin.GetMidValue(3));
			//if(minTOF > -0.03) minTOF = -0.3;
			double maxTOF = 0;
			
			bool fitP = cfg.StartFittingProtons(bin.GetMidValue(3));
			bool fitK = cfg.StartFittingKaons(bin.GetMidValue(3));
			if(fitP){
				maxTOF = tofRes->GetProtonMean(bin.GetMidValue(3)) + 4*tofRes->GetProtonWidth(bin.GetMidValue(3));
			}
			else if(fitK){
				maxTOF = tofRes->GetKaonMean(bin.GetMidValue(3)) + 4*tofRes->GetKaonWidth(bin.GetMidValue(3));
			}
			else{
				maxTOF = tofRes->GetPionMean(bin.GetMidValue(3)) + 4*tofRes->GetPionWidth(bin.GetMidValue(3));
			}
		
			string name = "bin_" + ConvertToString(i+1) + "_" + ConvertToString(indexZ) + "_" + ConvertToString(indexTh) + "_" + ConvertToString(indexP);
			string title = ConvertToString(binLow.at(0),0) + " #leq z < " + ConvertToString(binHigh.at(0),0) + " cm, " + 
				ConvertToString(binLow.at(1),0) + " #leq #theta < " + ConvertToString(binHigh.at(1),0) + " mrad, " + 
				ConvertToString(binLow.at(2),2) + " #leq p < " + ConvertToString(binHigh.at(2),2) + " GeV/c";
			
			bool skipFit = false;
			if(dEdxBin->size() < 10){
				skipFit = true;	
				cerr << "WARNING: Skipping fit " << name << endl;
			}
			if(!skipFit){
			
				
				nBinsdEdx = OptimizeHistoBinSize(dEdxBin, mindEdx, maxdEdx);
				nBinsTOF = OptimizeHistoBinSize(m2tofBin, minTOF, maxTOF);
				if(nBinsdEdx < cfg.GetNBinsdEdx()) {
					nBinsdEdx = cfg.GetNBinsdEdx();
				}
				if(nBinsTOF < cfg.GetNBinsTOF()){
					nBinsTOF = cfg.GetNBinsTOF();
				}			
				TH2D hist(name.c_str(), "",  nBinsdEdx, mindEdx, maxdEdx, nBinsTOF, minTOF, maxTOF);
				hist.SetTitle(title.c_str());
				for(int j = 0; j < dEdxBin->size(); j++){
					
					hist.Fill(dEdxBin->at(j), m2tofBin->at(j));
				}
				
				
				if(!fitP){
					string namep = name + "_tofp";
					TH1D histptof(namep.c_str(), "",  nBinsTOF, tofRes->GetProtonMean(bin.GetMidValue(3)) - 4*tofRes->GetProtonWidth(bin.GetMidValue(3)), 
					tofRes->GetProtonMean(bin.GetMidValue(3)) + 4*tofRes->GetProtonWidth(bin.GetMidValue(3)));
					for(int j = 0; j < m2tofBin->size(); j++){
						histptof.Fill( m2tofBin->at(j));
					}					
					double nprot = 0;
					double nprotE = 0;
					if(!dEdxTOFFit::FitGaus(histptof, nprot, nprotE)){
						cerr << "WARNING: Proton tof fit did not converge! "  << nprot << " " << nprotE <<  endl;
					}
					np[0] = nprot;
					np[1] = nprotE;
					
				}
				if(!fitK){
					string nameK = name + "_tofK";
					TH1D histKtof(nameK.c_str(), "",  nBinsTOF, tofRes->GetKaonMean(bin.GetMidValue(3)) - 4*tofRes->GetKaonWidth(bin.GetMidValue(3)), 
					tofRes->GetKaonMean(bin.GetMidValue(3)) + 4*tofRes->GetKaonWidth(bin.GetMidValue(3)));
					for(int j = 0; j < m2tofBin->size(); j++){
						histKtof.Fill( m2tofBin->at(j));
					}					
					double nkaon = 0;
					double nkaonE = 0;
					if(!dEdxTOFFit::FitGaus(histKtof, nkaon, nkaonE)){
						cerr << "WARNING: Proton tof fit did not converge! " << nkaon << " " << nkaonE << endl;
					}
					nK[0] = nkaon;
					nK[1] = nkaonE;
					
				}
													
				dEdxTOFFit fS(&hist, dEdxRes, tofRes, multRanges, &bin);				

				
				map<int, double> fixedMult;
				if(fitP){
					fS.Fit(false, 15);
				}
				else if(fitK){
					fS.Fit(false, 11);
				}
				else{
					fS.Fit(false, 9);
				}
				output.cd();
				fS.GetCanvas()->Write();	
				fS.GetResult()->Write();
				//cerr << "radi2!" << endl;
				RooRealVar* Api = (RooRealVar*) fS.GetResult()->floatParsFinal().find("Api");
				RooRealVar* Ap = (RooRealVar*) fS.GetResult()->floatParsFinal().find("Ap");
				RooRealVar* AK = (RooRealVar*) fS.GetResult()->floatParsFinal().find("AK");
				RooRealVar* Ae = (RooRealVar*) fS.GetResult()->floatParsFinal().find("Ae");
			
				if(Api){
					npi[0] = Api->getValV();
					npi[1] = Api->getError();
				}

				if(Ap){
					np[0] = Ap->getValV();
					np[1] = Ap->getError();
				}

				if(AK){
					nK[0] = AK->getValV();
					nK[1] = AK->getError();
				}

				if(Ae){
					ne[0] = Ae->getValV();
					ne[1] = Ae->getError();
				}
				
				cout << "******************************************************" << endl;
				cout << "Ne = (" << ne[0] << " +/- " << ne[1] << ")" << endl;
				cout << "Npi = (" << npi[0] << " +/- " << npi[1] << ")" << endl;
				cout << "NK = (" << nK[0] << " +/- " << nK[1] << ")" << endl;
				cout << "Np = (" << np[0] << " +/- " << np[1] << ")" << endl;
				cout << "******************************************************" << endl;
			}
			
			else{
				npi[0] = -999;
				npi[1] = -999;

				np[0] = -999;
				np[1] = -999;
	
				nK[0] = -999;
				nK[1] = -999;

				ne[0] = -999;
				ne[1] = -999;
			}
			outTree->Fill();
		}

   				
	}
	outTree->Write();
	output.Close();
	
}



int FitSpectrum::OptimizeHistoBinSize(vector<double> *tofD, double min, double max){
	vector<double> data;
	
	for(int i  = 0; i < tofD->size(); i++){
		data.push_back(tofD->at(i));
	}
	
	std::sort(data.begin(), data.end());

	
	double q1;
	double q3;
	int n = data.size();
		

	
	double nD = (double) n;
	//return (data.at(data.size()-2)-data.at(2))/sqrt(nD);
	if(n % 2 == 0){
		int sizeQ = n/2;
		
		if(sizeQ % 2 == 0){
			int sizeQ1 = sizeQ/2;
			q1 = (data.at(sizeQ1-1) + data.at(sizeQ1))/2.;
			q3 = (data.at(sizeQ+sizeQ1-1) + data.at(sizeQ+sizeQ1))/2.;
		}
		else{
			int sizeQ1 = sizeQ/2;
			q1 = data.at(sizeQ1);
			q3 = data.at(sizeQ + sizeQ1);
		}

	}
	else{
		int sizeQ = n/2;
		if(sizeQ % 2 == 0){
			int sizeQ1 = sizeQ/2;
			q1 = (data.at(sizeQ1-1) + data.at(sizeQ1))/2.;
			q3 = (data.at(sizeQ+sizeQ1) + data.at(sizeQ+sizeQ1 + 1))/2.;
		}
		else{
			int sizeQ1 = sizeQ/2;
			q1 = data.at(sizeQ1);
			q3 = data.at(sizeQ + sizeQ1 + 1);
		}
	}

	double step = 2*(q3-q1)/TMath::Power(n, 1./3.);
	cout << step << " " << 2*(q3-q1) << " " << TMath::Power(n, 1./3.) << endl;
	return (max-min)/step;
}
