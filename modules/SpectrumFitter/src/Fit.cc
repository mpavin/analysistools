#include "dEdxTOFFit.h"
#include <RooGaussian.h>
#include <RooProdPdf.h>
#include <RooExtendPdf.h>
#include <RooAddPdf.h>
#include <RooPlot.h>
#include <RooChi2Var.h>
#include <RooMinuit.h>
#include <TImage.h>
#include <TLegend.h>
#include <TF2.h>
#include <TPaveText.h>
#include <TStyle.h>
#include <RooNLLVar.h>
#include <RooChi2Var.h>
#include <RooMsgService.h>
#include "StrTools.h"

using namespace StrTools;

void dEdxTOFFit::Fit(bool onedim, int fitPart){

	
	if(onedim){		

	/*	Fit1D(fitPart);
	RooGaussian dEdxp("dEdxp", "(dE/dx)_{p}", *fX, *fMeandEdxp, *fSigdEdxp);
	RooGaussian dEdxpi("dEdxpi", "(dE/dx)_{#pi}", *fX, *fMeandEdxpi, *fSigdEdxpi);
	RooGaussian dEdxe("dEdxe", "(dE/dx)_{e}", *fX, *fMeandEdxe, *fSigdEdxe);
	RooGaussian dEdxK("dEdxK", "(dE/dx)_{K}", *fX, *fMeandEdxK, *fSigdEdxK);
		
	RooExtendPdf p("p", "p", dEdxp, *fAp);
	RooExtendPdf pi("pi", "#pi", dEdxpi, *fApi);
	RooExtendPdf e("e", "e", dEdxe, *fAe);
	RooExtendPdf K("K", "K", dEdxK, *fAK);*/
	}
	else{

		Fit2D(fitPart);
	}
}

void dEdxTOFFit::Fit2D(int fitPart){

	string modelName = fInput->GetName();
	cout << "Fitting bin: " << modelName << endl;
	modelName = "model" + modelName;
	RooAddPdf *model;	
	bool drawPart[4] = {false, false, false, false};

	RooGaussian dEdxp("dEdxp", "(dE/dx)_{p}", *fX, *fMeandEdxp, *fSigdEdxp);
	RooGaussian dEdxpi("dEdxpi", "(dE/dx)_{#pi}", *fX, *fMeandEdxpi, *fSigdEdxpi);
	RooGaussian dEdxe("dEdxe", "(dE/dx)_{e}", *fX, *fMeandEdxe, *fSigdEdxe);
	RooGaussian dEdxK("dEdxK", "(dE/dx)_{K}", *fX, *fMeandEdxK, *fSigdEdxK);
	
	/*RooGaussian dEdxp0("dEdxp0", "(dE/dx)_{p}", *fX, *fMeandEdxp, *fSigdEdxp);
	RooGaussian dEdxpi0("dEdxpi0", "(dE/dx)_{#pi}", *fX, *fMeandEdxpi, *fSigdEdxpi);
	RooGaussian dEdxe0("dEdxe0", "(dE/dx)_{e}", *fX, *fMeandEdxe, *fSigdEdxe);
	RooGaussian dEdxK0("dEdxK0", "(dE/dx)_{K}", *fX, *fMeandEdxK, *fSigdEdxK);
	
	RooGaussian dEdxp1("dEdxp1", "(dE/dx)_{p}", *fX, *fMeandEdxp, *fSigdEdxp1);
	RooGaussian dEdxpi1("dEdxpi1", "(dE/dx)_{#pi}", *fX, *fMeandEdxpi, *fSigdEdxpi1);
	RooGaussian dEdxe1("dEdxe1", "(dE/dx)_{e}", *fX, *fMeandEdxe, *fSigdEdxe1);
	RooGaussian dEdxK1("dEdxK1", "(dE/dx)_{K}", *fX, *fMeandEdxK, *fSigdEdxK1);
	
	RooAddPdf dEdxp("dEdxp", "(dE/dx)_{p}", RooArgList(dEdxp0,dEdxp1), *mu1);
	RooAddPdf dEdxpi("dEdxpi", "(dE/dx)_{pi}", RooArgList(dEdxpi0,dEdxpi1), *mu2);
	RooAddPdf dEdxe("dEdxe", "(dE/dx)_{e}", RooArgList(dEdxe0,dEdxe1), *mu3);
	RooAddPdf dEdxK("dEdxK", "(dE/dx)_{K}", RooArgList(dEdxK0,dEdxK1), *mu4);*/
	
	RooGaussian tofp("tofp", "TOF_{p}", *fY, *fMeanTOFp, *fSigTOFp);
	RooGaussian tofpi("tofpi", "TOF_{#pi}", *fY, *fMeanTOFpi, *fSigTOFpi);
	RooGaussian tofe("tofe", "TOF_{e}", *fY, *fMeanTOFe, *fSigTOFe);
	//RooGaussian tofe("tofe", "TOF_{e}", *fY, *fMeanTOFe, *fSigTOFpi);
	RooGaussian tofK("tofK", "TOF_{K}", *fY, *fMeanTOFK, *fSigTOFK);
	
	RooProdPdf up("up", "(dE/dx)_{p}#dot TOF_{p}", dEdxp, tofp);
	RooProdPdf upi("upi", "(dE/dx)_{#pi}#dot TOF_{#pi}", dEdxpi, tofpi);
	RooProdPdf ue("ue", "(dE/dx)_{e}#dot TOF_{e}", dEdxe, tofe);
	RooProdPdf uK("uK", "(dE/dx)_{K}#dot TOF_{K}", dEdxK, tofK);
	
	RooExtendPdf p("p", "p", up, *fAp);
	RooExtendPdf pi("pi", "#pi", upi, *fApi);
	RooExtendPdf e("e", "e", ue, *fAe);
	RooExtendPdf K("K", "K", uK, *fAK);	
	
	switch(fitPart){
		case 1:
		{
			model = new RooAddPdf(modelName.c_str(), modelName.c_str(), RooArgList(pi));
			drawPart[0] = true;
			break;
		}

		case 2:
		{
			model = new RooAddPdf(modelName.c_str(), modelName.c_str(), RooArgList(K));
			drawPart[1] = true;
			break;
		}
		case 3:
		{
			model = new RooAddPdf(modelName.c_str(), modelName.c_str(), RooArgList(pi, K));
			drawPart[0] = true;
			drawPart[1] = true;
			break;
		}		
		case 4:
		{
			model = new RooAddPdf(modelName.c_str(), modelName.c_str(), RooArgList(p));
			drawPart[2] = true;
			break;
		}
		case 5:
		{
			model = new RooAddPdf(modelName.c_str(), modelName.c_str(), RooArgList(pi, p));
			drawPart[0] = true;
			drawPart[2] = true;
			break;
		}
		case 6:
		{
			model = new RooAddPdf(modelName.c_str(), modelName.c_str(), RooArgList(K, p));
			drawPart[1] = true;
			drawPart[2] = true;
			break;
		}
		case 7:
		{
			model = new RooAddPdf(modelName.c_str(), modelName.c_str(), RooArgList(pi, K, p));
			drawPart[0] = true;
			drawPart[1] = true;
			drawPart[2] = true;
			break;
		}		
		case 8:
		{
			model = new RooAddPdf(modelName.c_str(), modelName.c_str(), RooArgList(e));
			drawPart[3] = true;
			break;
		}

		case 9:
		{
			model = new RooAddPdf(modelName.c_str(), modelName.c_str(), RooArgList(pi, e));
			drawPart[0] = true;
			drawPart[3] = true;
			break;
		}
		case 10:
		{
			model = new RooAddPdf(modelName.c_str(), modelName.c_str(), RooArgList(K, e));
			drawPart[1] = true;
			drawPart[3] = true;
			break;
		}
		case 11:
		{
			model = new RooAddPdf(modelName.c_str(), modelName.c_str(), RooArgList(pi, K, e));
			drawPart[0] = true;
			drawPart[1] = true;
			drawPart[3] = true;
			break;
		}
		case 12:
		{
			model = new RooAddPdf(modelName.c_str(), modelName.c_str(), RooArgList(p, e));
			drawPart[2] = true;
			drawPart[3] = true;
			break;
		}
		case 13:
		{
			model = new RooAddPdf(modelName.c_str(), modelName.c_str(), RooArgList(pi, p, e));
			drawPart[0] = true;
			drawPart[2] = true;
			drawPart[3] = true;
			break;
		}
		case 14:
		{
			model = new RooAddPdf(modelName.c_str(), modelName.c_str(), RooArgList(K, p, e));
			drawPart[1] = true;
			drawPart[2] = true;
			drawPart[3] = true;
			break;
		}	
		case 15:
		{
			model = new RooAddPdf(modelName.c_str(), modelName.c_str(), RooArgList(pi, K, p, e));
			drawPart[0] = true;
			drawPart[1] = true;
			drawPart[2] = true;
			drawPart[3] = true;
			break;
		}
	}

	int nTry = 0;
	bool success = false;

	while(nTry < 1 && !success){
		cout << "Number of iterations: " << nTry + 1 << endl;
		nTry++;
		
		if(fResult != NULL) delete fResult;
		
		//fResult = model->fitTo(*fData,Save(),Extended(), PrintLevel(-1), Verbose(kFALSE));
		fResult = model->fitTo(*fData,Save(),Extended());
		success = !(fResult->status());
		cout << fResult->status() << endl;
		for(int i = 0; i < fPars.size(); i++){
			double range = (*fPars.at(i))->getMax() - (*fPars.at(i))->getMin();
			double diffMin = fabs((*fPars.at(i))->getValV() - (*fPars.at(i))->getMin());
			double diffMax = fabs((*fPars.at(i))->getValV() - (*fPars.at(i))->getMax());
			//cout << i << " " << diffMin << " " << diffMax << " " << range*0.001 << endl;
			//cout << (*fPars.at(i))->getBinWidth(1) << " " << range <<endl;
			if(diffMin < range*0.001){
				(*fPars.at(i))->setMin((*fPars.at(i))->getMin()-0.2*range);
				success = false;
			}
			else if(diffMax < range*0.001){
				(*fPars.at(i))->setMax((*fPars.at(i))->getMax()+0.2*range);
				success = false;
			}
		}
	}
	
	fResult->Print();
	string canvasName = fInput->GetName();
	string title = fInput->GetTitle();
	canvasName = "c" + canvasName;
	fCanvas = new TCanvas(canvasName.c_str(), title.c_str(), 1200, 1200);
	
	TPad *padHist = new TPad("padHist","",0.00,0.5,0.5,1);
	TPad *padpX = new TPad("padpX","",0.5,0.50,1,1.0);
	TPad *padpY = new TPad("padpY","",0.50,0.00,1.0,0.5);
	TPad *padL = new TPad("padL","",0.0,0.0,0.245,0.49);
	TPad *padV = new TPad("padV","",0.25,0.0,0.495,0.49);
	
	
	TF2* pf = new TF2("pf", "([0]/(2*TMath::Pi()*[2]*[4]))*TMath::Gaus((x-[1])/(sqrt(2)*[2]))*TMath::Gaus((y-[3])/(sqrt(2)*[4]))", fMeandEdxp->getValV()-fSigdEdxp->getValV()*3, fMeandEdxp->getValV()+fSigdEdxp->getValV()*3, fMeanTOFp->getValV() - fSigTOFp->getValV()*3, fMeanTOFp->getValV() + fSigTOFp->getValV()*3);

	pf->SetLineColor(kRed);
	pf->SetLineStyle(kDashed);
	pf->SetLineWidth(2);
	
	
	TF2* pif = new TF2("pif", "([0]/(2*TMath::Pi()*[2]*[4]))*TMath::Gaus((x-[1])/(sqrt(2)*[2]))*TMath::Gaus((y-[3])/(sqrt(2)*[4]))", fMeandEdxpi->getValV()-fSigdEdxpi->getValV()*3, fMeandEdxpi->getValV()+fSigdEdxpi->getValV()*3, fMeanTOFpi->getValV() - fSigTOFpi->getValV()*3, fMeanTOFpi->getValV() + fSigTOFpi->getValV()*3);

	pif->SetLineColor(kBlue);
	pif->SetLineStyle(kDashed);
	pif->SetLineWidth(2);
	
	TF2* ef = new TF2("ef", "([0]/(2*TMath::Pi()*[2]*[4]))*TMath::Gaus((x-[1])/(sqrt(2)*[2]))*TMath::Gaus((y-[3])/(sqrt(2)*[4]))", fMeandEdxe->getValV()-fSigdEdxe->getValV()*3, fMeandEdxe->getValV()+fSigdEdxe->getValV()*3, fMeanTOFe->getValV() - fSigTOFe->getValV()*3, fMeanTOFe->getValV() + fSigTOFe->getValV()*3);

	ef->SetLineColor(kGreen+3);
	ef->SetLineStyle(kDashed);
	ef->SetLineWidth(2);	
	
	TF2* Kf = new TF2("Kf", "([0]/(2*TMath::Pi()*[2]*[4]))*TMath::Gaus((x-[1])/(sqrt(2)*[2]))*TMath::Gaus((y-[3])/(sqrt(2)*[4]))", fMeandEdxK->getValV()-fSigdEdxK->getValV()*3, fMeandEdxK->getValV()+fSigdEdxK->getValV()*3, fMeanTOFK->getValV() - fSigTOFK->getValV()*3, fMeanTOFK->getValV() + fSigTOFK->getValV()*3);

	Kf->SetLineColor(kViolet);
	Kf->SetLineStyle(kDashed);
	Kf->SetLineWidth(2);

	
	fCanvas->cd();
	padHist->Draw();
	padpX->Draw();
	padpY->Draw();
	padL->Draw();
	padV->Draw();
	
	//****************************************************************************************
	padHist->cd();
	//TH2D* hh_data =(TH2D*) fData->createHistogram("dEdx,tof",50,50) ;
 	TH2D* hh_pdf = (TH2D*) model->createHistogram("dEdx,tof",50,50) ;
 	//hh_data->SetLineWidth(2);
 	hh_pdf->SetLineColor(kRed);
 	hh_pdf->SetStats(kFALSE);
 	hh_pdf->SetLineWidth(1);
 	
 	hh_pdf->SetTitle(fInput->GetTitle());
 	//hh_data->SetStats(kFALSE);
 	gStyle->SetPalette(57);
 	//hh_data->Draw("lego1");
 	hh_pdf->Draw("surf3");
 	
	//****************************************************************************************
	padpX->cd();
	RooPlot* framex = fX->frame() ;
	
	fData->plotOn(framex) ;
	if(fY->getMax() > 0.9)
		model->plotOn(framex, Components("p"),LineStyle(kDashed), LineColor(kRed)) ;
	model->plotOn(framex, Components("pi"),LineStyle(kDashed)) ;
	model->plotOn(framex, Components("e"),LineStyle(kDashed), LineColor(kGreen+3)) ;
	model->plotOn(framex, Components("K"),LineStyle(kDashed), LineColor(kViolet)) ; 	
	model->plotOn(framex,RooFit::Name(modelName.c_str()),LineStyle(kDashed), LineColor(kCyan)) ; 	
	cout << "Chi2/ndf (dE/dx) = " << framex->chiSquare() << endl;
	framex->Draw();
	
	TLegend *leg1 = new TLegend(0.6,0.6,0.9,0.9);
	//if(fY->getMax() > 0.9)
	
	if(drawPart[0]) leg1->AddEntry(pif,"#pi", "LP");
	if(drawPart[1]) leg1->AddEntry(Kf,"K", "LP");
	if(drawPart[2]) leg1->AddEntry(pf,"p", "LP");
	if(drawPart[3]) leg1->AddEntry(ef,"e", "LP");
	
	TObject *obj = framex->findObject(modelName.c_str());
	leg1->AddEntry(framex->findObject(modelName.c_str()),"Model","l");
	leg1->Draw("same");  
	//****************************************************************************************
		
	padpY->cd();
	RooPlot* framey = fY->frame() ;
	fData->plotOn(framey) ;

	if(drawPart[0]) model->plotOn(framey, Components("pi"),LineStyle(kDashed)) ;
	if(drawPart[1]) model->plotOn(framey, Components("K"),LineStyle(kDashed), LineColor(kViolet)) ;
	if(drawPart[2]) model->plotOn(framey, Components("p"),LineStyle(kDashed), LineColor(kRed)) ;
	if(drawPart[3]) model->plotOn(framey, Components("e"),LineStyle(kDashed), LineColor(kGreen+3)) ;
	
	model->plotOn(framey,LineStyle(kDashed), LineColor(kCyan)) ; 	
	cout << "Chi2/ndf (m2TOF) = " << framey->chiSquare() << endl;	
	framey->Draw();	
	
	
	//****************************************************************************************

	
	string s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, s11, s12, s13, s14, s15, s16, s17, s18, s19, s20;
	
	if(drawPart[0]){
		s1 = "A_{#pi} = (" + ConvertToString(fApi->getValV(),0) + "#pm" + ConvertToString(fApi->getError(),0) + ")";
		s2 = "#mu_{dE/dx}^{#pi} = (" + ConvertToString(fMeandEdxpi->getValV(),3) + "#pm" + ConvertToString(fMeandEdxpi->getError(),3) + ") a.u.";
		s3 = "#sigma_{dE/dx}^{#pi} = (" + ConvertToString(fSigdEdxpi->getValV(),3) + "#pm" + ConvertToString(fSigdEdxpi->getError(),3) + ") a.u.";
		s4 = "#mu_{TOF}^{#pi} = (" + ConvertToString(fMeanTOFpi->getValV(),3) + "#pm" + ConvertToString(fMeanTOFpi->getError(),3) + ") GeV^{2}/c^{4}";
		s5 = "#sigma_{TOF}^{#pi} = (" + ConvertToString(fSigTOFpi->getValV(),3) + "#pm" + ConvertToString(fSigTOFpi->getError(),3) + ") GeV^{2}/c^{4}";	
	}
	if(drawPart[1]){
		s6 = "A_{K} = (" + ConvertToString(fAK->getValV(),0) + "#pm" + ConvertToString(fAK->getError(),0) + ")";
		s7 = "#mu_{dE/dx}^{K} = (" + ConvertToString(fMeandEdxK->getValV(),3) + "#pm" + ConvertToString(fMeandEdxK->getError(),3) + ") a.u.";
		s8 = "#sigma_{dE/dx}^{K} = (" + ConvertToString(fSigdEdxK->getValV(),3) + "#pm" + ConvertToString(fSigdEdxK->getError(),3) + ") a.u.";
		s9 = "#mu_{TOF}^{K} = (" + ConvertToString(fMeanTOFK->getValV(),3) + "#pm" + ConvertToString(fMeanTOFK->getError(),3) + ") GeV^{2}/c^{4}";
		s10 = "#sigma_{TOF}^{K} = (" + ConvertToString(fSigTOFK->getValV(),3) + "#pm" + ConvertToString(fSigTOFK->getError(),3) + ") GeV^{2}/c^{4}";	
	}
	if(drawPart[2]){
		s11 = "A_{p} = (" + ConvertToString(fAp->getValV(),0) + "#pm" + ConvertToString(fAp->getError(),0) + ")";
		s12 = "#mu_{dE/dx}^{p} = (" + ConvertToString(fMeandEdxp->getValV(),3) + "#pm" + ConvertToString(fMeandEdxp->getError(),3) + ") a.u.";
		s13 = "#sigma_{dE/dx}^{p} = (" + ConvertToString(fSigdEdxp->getValV(),3) + "#pm" + ConvertToString(fSigdEdxp->getError(),3) + ") a.u.";
		s14 = "#mu_{TOF}^{p} = (" + ConvertToString(fMeanTOFp->getValV(),3) + "#pm" + ConvertToString(fMeanTOFp->getError(),3) + ") GeV^{2}/c^{4}";
		s15 = "#sigma_{TOF}^{p} = (" + ConvertToString(fSigTOFp->getValV(),3) + "#pm" + ConvertToString(fSigTOFp->getError(),3) + ") GeV^{2}/c^{4}";	
	}
	if(drawPart[3]){
		s16 = "A_{e} = (" + ConvertToString(fAe->getValV(),0) + "#pm" + ConvertToString(fAe->getError(),0) + ")";
		s17 = "#mu_{dE/dx}^{e} = (" + ConvertToString(fMeandEdxe->getValV(),3) + "#pm" + ConvertToString(fMeandEdxe->getError(),3) + ") a.u.";
		s18 = "#sigma_{dE/dx}^{e} = (" + ConvertToString(fSigdEdxe->getValV(),3) + "#pm" + ConvertToString(fSigdEdxe->getError(),3) + ") a.u.";
		s19 = "#mu_{TOF}^{e} = (" + ConvertToString(fMeanTOFe->getValV(),3) + "#pm" + ConvertToString(fMeanTOFe->getError(),3) + ") GeV^{2}/c^{4}";
		s20 = "#sigma_{TOF}^{e} = (" + ConvertToString(fSigTOFe->getValV(),3) + "#pm" + ConvertToString(fSigTOFe->getError(),3) + ") GeV^{2}/c^{4}";
	}

	
	padV->cd();
	TPaveText *pt = new TPaveText(0,0,1,1);
	if(drawPart[0]){
		pt->AddText(s1.c_str());
		pt->AddText(s2.c_str());
		pt->AddText(s3.c_str());
		pt->AddText(s4.c_str());
		pt->AddText(s5.c_str());
	}
	if(drawPart[1]){
		pt->AddText(s6.c_str());
		pt->AddText(s7.c_str());
		pt->AddText(s8.c_str());
		pt->AddText(s9.c_str());
		pt->AddText(s10.c_str());
	}

	pt->Draw();
	
	//*************************************************************
	padL->cd();
	TPaveText *pt1 = new TPaveText(0,0,1,1);
	if(drawPart[2]){	
		pt1->AddText(s11.c_str());
		pt1->AddText(s12.c_str());
		pt1->AddText(s13.c_str());
		pt1->AddText(s14.c_str());
		pt1->AddText(s15.c_str());
	}
	if(drawPart[3]){
		pt1->AddText(s16.c_str());
		pt1->AddText(s17.c_str());
		pt1->AddText(s18.c_str());
		pt1->AddText(s19.c_str());
		pt1->AddText(s20.c_str());
	}
	pt1->Draw();
}


