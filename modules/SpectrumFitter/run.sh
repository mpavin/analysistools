#!/bin/sh
 
#set -e


MYDIR="/afs/cern.ch/user/m/mpavin/Work/analysistools/modules/SpectrumFitter"
#**********************************************************************************


#**********************************************************************************
#	SHINE ENV
readonly shine_version='v1r4p0'
readonly shine_root="/afs/cern.ch/na61/Releases/SHINE/${shine_version}"
. "${shine_root}/scripts/env/lxplus_32bit_slc6.sh"
eval $("${shine_root}/bin/shine-offline-config" --env-sh)
#**********************************************************************************

#**********************************************************************************

#**********************************************************************************
#	TMP DIR
if [ -n "${LSB_BATCH_JID}" ]; then
        tmpdir="/pool/lsf/${USER}/${LSB_BATCH_JID}"                                                                                                                          
else
        tmpdir="${TMPDIR:=/tmp/${USER}}"                                                                                                                        
fi
#**********************************************************************************




cd ${MYDIR}

./spectrumFitter -b bootstrap.xml -i $1

#cp -r Results/ ${MYDIR}/Results/
