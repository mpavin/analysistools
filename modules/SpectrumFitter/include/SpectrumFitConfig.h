#ifndef SPECTRUMFITCONFIG_H
#define SPECTRUMFITCONFIG_H
#include <iostream>
#include <vector>
#include <string>

#include <vector>



using namespace std;

class SpectrumFitConfig{

	public:
		SpectrumFitConfig(){Read();}
		string GetOutputDir(){return fOutput;}
		string GetdEdxMap(){return fdEdxMap;}
		string GetTOFMap(){return fTOFMap;}
		int GetTestHistoId(){return fTestHistoId;}
		bool IsTest(){return fTest;}
		int GetNBinsdEdx(){return fNBinsdEdx;}
		int GetNBinsTOF(){return fNBinsTOF;}
		bool StartFittingProtons(double p){return (p >= fFitProtons);}
		bool StartFittingKaons(double p){return (p >= fFitKaons);}
		double GetdEdxWidth(){return fdEdxWidth;}
		double GetPionMultiplicityRange(){return fPionRanges[0];}
		double GetProtonMultiplicityRange(){return fProtonRanges[0];}
		double GetKaonMultiplicityRange(){return fKaonRanges[0];}
		double GetElectronMultiplicityRange(){return fElectronRanges[0];}
		double GetPiondEdxMeanRange(){return fPionRanges[1];}
		double GetPiondEdxWidthRange(){return fPionRanges[2];}
		double GetProtondEdxMeanRange(){return fProtonRanges[1];}
		double GetProtondEdxWidthRange(){return fProtonRanges[2];}
		double GetKaondEdxMeanRange(){return fKaonRanges[1];}
		double GetKaondEdxWidthRange(){return fKaonRanges[2];}
		double GetElectrondEdxMeanRange(){return fElectronRanges[1];}
		double GetElectrondEdxWidthRange(){return fElectronRanges[2];}		
		double GetPionTOFMeanRange(){return fPionRanges[3];}
		double GetPionTOFWidthRange(){return fPionRanges[4];}
		double GetProtonTOFMeanRange(){return fProtonRanges[3];}
		double GetProtonTOFWidthRange(){return fProtonRanges[4];}
		double GetKaonTOFMeanRange(){return fKaonRanges[3];}
		double GetKaonTOFWidthRange(){return fKaonRanges[4];}
		double GetElectronTOFMeanRange(){return fElectronRanges[3];}
		double GetElectronTOFWidthRange(){return fElectronRanges[4];}
		
		void SetOutputDir(string value){fOutput = value;}
		void SetdEdxMap(string value){fdEdxMap = value;}
		void SetTOFMap(string value){fTOFMap = value;}
		void SetdEdxWidth(double val){fdEdxWidth = val;}
		void SetTestHistoId(int test){fTestHistoId = test;}
		void SetIsTest(bool test){fTest = test;}
		void SetNBinsdEdx(int n){fNBinsdEdx = n;}
		void SetNBinsTOF(int n){fNBinsTOF = n;}
		void SetProtonMomentumLimit(double val){fFitProtons = val;}
		void SetPionMultiplicityRange(double val){fPionRanges[0] = val;}
		void SetProtonMultiplicityRange(double val){fProtonRanges[0] = val;}
		void SetKaonMultiplicityRange(double val){fKaonRanges[0] = val;}
		void SetElectronMultiplicityRange(double val){fElectronRanges[0] = val;}
		void SetPiondEdxMeanRange(double val){fPionRanges[1] = val;}
		void SetPiondEdxWidthRange(double val){fPionRanges[2] = val;}
		void SetProtondEdxMeanRange(double val){fProtonRanges[1] = val;}
		void SetProtondEdxWidthRange(double val){fProtonRanges[2] = val;}
		void SetKaondEdxMeanRange(double val){fKaonRanges[1] = val;}
		void SetKaondEdxWidthRange(double val){fKaonRanges[2] = val;}
		void SetElectrondEdxMeanRange(double val){fElectronRanges[1] = val;}
		void SetElectrondEdxWidthRange(double val){fElectronRanges[2] = val;}
		void SetPionTOFMeanRange(double val){fPionRanges[3] = val;}
		void SetPionTOFWidthRange(double val){fPionRanges[4] = val;}
		void SetProtonTOFMeanRange(double val){fProtonRanges[3] = val;}
		void SetProtonTOFWidthRange(double val){fProtonRanges[4] = val;}
		void SetKaonTOFMeanRange(double val){fKaonRanges[3] = val;}
		void SetKaonTOFWidthRange(double val){fKaonRanges[4] = val;}
		void SetElectronTOFMeanRange(double val){fElectronRanges[3] = val;}
		void SetElectronTOFWidthRange(double val){fElectronRanges[4] = val;}
	private:
		
		void Read();
		bool fTest;
		int fTestHistoId;
		//string fTestHistoName;
		string fOutput;
		string fdEdxMap;
		string fTOFMap;
		double fdEdxWidth;
		double fPionRanges[5];
		double fProtonRanges[5];
		double fKaonRanges[5];
		double fElectronRanges[5];
		double fFitProtons;
		double fFitKaons;
		int fNBinsdEdx;
		int fNBinsTOF;
		
};

#endif
