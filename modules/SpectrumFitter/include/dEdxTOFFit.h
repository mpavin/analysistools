#ifndef DEDXTOFFIT_H
#define DEDXTOFFIT_H


#include <iostream>
#include <vector>
#include <string>
#include <RooRealVar.h>
#include <RooPolyVar.h>
#include <RooFitResult.h>
#include <RooDataHist.h>
#include <TH2D.h>
#include <TCanvas.h>
#include "dEdxResolution.h"
#include "TOFResolution.h"
#include "Bin.h"
using namespace std;
using namespace RooFit; 


enum ParticleFit{
	ePion = 1 << 0,
	eKaon = 1 << 1,
	eProton = 1 << 2,
	eElectron = 1 << 3

};
class dEdxTOFFit{

	public:
		dEdxTOFFit(TH2D* inputHist, dEdxResolution* dEdxRes, TOFResolution* tofRes, vector<double> &multRanges, Bin* bin);
		~dEdxTOFFit();
		
		void Fit(bool onedim, int fitPart);
		
		static int FitGaus(TH1D &hist, double& npart, double& npartE);
		RooFitResult* GetResult(){return fResult;}
		TCanvas* GetCanvas(){return fCanvas;}
	private:
		
		void Fit2D(int fitPart);
		TH2D* fInput;
		TCanvas *fCanvas;
		bool fPositive;
		
		RooDataHist *fData;
		
		RooRealVar *fX;
		RooRealVar *fY;
		
		RooRealVar *fAp;
		RooRealVar *fApi;
		RooRealVar *fAe;
		RooRealVar *fAK;
		
		RooRealVar *fMeandEdxp;
		RooRealVar *fMeandEdxpi;
		RooRealVar *fMeandEdxe;
		RooRealVar *fMeandEdxK;
		
		RooRealVar *fMeanTOFp;
		RooRealVar *fMeanTOFpi;
		RooRealVar *fMeanTOFe;
		RooRealVar *fMeanTOFK;
		
		RooRealVar *fSigdEdxp;
		RooRealVar *fSigdEdxpi;
		RooRealVar *fSigdEdxe;
		RooRealVar *fSigdEdxK;	
		
		RooRealVar *fSigdEdxp1;
		RooRealVar *fSigdEdxpi1;
		RooRealVar *fSigdEdxe1;
		RooRealVar *fSigdEdxK1;		
		
		RooRealVar *ax;
		RooRealVar *mu1;
		RooRealVar *mu2;
		RooRealVar *mu3;
		RooRealVar *mu4;
		
		RooRealVar *fSigTOFp;
		RooRealVar *fSigTOFpi;
		RooRealVar *fSigTOFe;
		RooRealVar *fSigTOFK;	
		
		
		vector<RooRealVar**> fPars;
		RooFitResult* fResult;
		
		dEdxResolution *fdEdxRes;
		TOFResolution *fTOFRes;
		Bin *fBin;
		
};
#endif
