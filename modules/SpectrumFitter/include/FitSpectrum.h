#ifndef FITSPECTRUM_H
#define FITSPECTRUM_H


#include <iostream>
#include <vector>
#include <string>


using namespace std;

class FitSpectrum{

	public:
		FitSpectrum(string inputFile): fInputFile(inputFile) {}
		
		void SetInputFile(string inputFile){fInputFile = inputFile;}
		
		void Run();
		
	private:
		int OptimizeHistoBinSize(vector<double> *tofD, double min, double max);
		string fInputFile;

		
};
#endif
