#include <vector>
#include <string>
#include <iostream>
#include "Functions.h"
#include <TH1D.h>
#include <TH2D.h>
#include <TFile.h>
#include <TChain.h>
#include <TTree.h>
#include <TMath.h>
#include <math.h>

using namespace std;


int main(int argc, char* argv[]){

	//cout << argc << " " << argv[0] << endl;

	if(argc < 3){
		cerr << "Number of parameters is different than 2! Exiting..." << endl;
		exit(100);
	}
	
	int data;
	
	stringstream sconv0(argv[1]);
	sconv0 >> data;
	
	int topcut ;
	stringstream sconv(argv[2]);
	sconv >> topcut;
	


	

	double scale;
	string inputFile;
	string add;
	double xt = 0.1350;
	double yt = 0.2253;
	double dEdxCut;
	if(data==1){
		scale = 1;
		inputFile = "listData.txt";
		add = "data_";	
		dEdxCut = 1.4;
	}
	else{
		//scale = 0.099633345;
		scale = 1;
		inputFile = "listMC.txt";
		add = "mc_";	
		yt = 0.1253;	
		dEdxCut = 1.36;	
	}
	TChain chain("Data");
	
	string outName = add + "phi" + argv[2] + ".root";
	TFile outFile(outName.c_str(), "RECREATE");
	ifstream ifs;
	string line;
	ifs.open(inputFile.c_str());
		
	if (!ifs.is_open()){
		cerr << "[ReadInputListFile] Could not open infile. Exiting..." << endl;			
		exit (101);
	}
    
	while (getline(ifs, line)){
		if(line.at(0)=='#')
			continue;
		chain.Add(line.c_str());
	}
	ifs.close();		


	double cuts[8][5];
	for(int i = 0; i < 5; i++){
		for(int j = 0; j < 4; j++){
			cuts[i][j] = 0;
		}
	}

	vector<TH1D> errHistos;
	for(int i = 0; i < 7; i++){
		string name = "errH" + ConvertToString(i+1.,0);
		//TH1D tmp(name.c_str(), "", 50, 0, 0.75);
		TH1D tmp(name.c_str(), "", 100, 0, 1.5);
		errHistos.push_back(tmp);
	}
	BeamA beamD;
	vector<double> *x = NULL;
	vector<double> *xE = NULL;
	vector<double> *y = NULL;
	vector<double> *yE = NULL;
	vector<double> *z = NULL;
	vector<double> *px = NULL;
	vector<double> *py = NULL;
	vector<double> *pz = NULL;
	vector<double> *zLast = NULL;
	vector<double> *distTarg = NULL;
	vector<double> *zStart = NULL;
	vector<double> *dEdx = NULL;
	vector<int> *qD = NULL;
	vector<int> *nVTPC1 = NULL;
	vector<int> *nVTPC2 = NULL;
	vector<int> *nGTPC = NULL;
	vector<int> *nMTPC = NULL;
	vector<int> *topology = NULL;
	vector<int> *nTOF = NULL;
	vector<double> *m2TOF = NULL;	
	vector<int> *tofScId = NULL;
	int runNumber;
	
	TBranch *b_aData = chain.GetBranch("Beam");
 	b_aData->SetAddress(&beamD);
	chain.SetBranchAddress("x", &x);
	chain.SetBranchAddress("xE", &xE);
	chain.SetBranchAddress("y", &y);
	chain.SetBranchAddress("yE", &yE);
	chain.SetBranchAddress("z", &z);
	chain.SetBranchAddress("px", &px);
	chain.SetBranchAddress("py", &py);
	chain.SetBranchAddress("pz", &pz);
	chain.SetBranchAddress("zLast", &zLast);
	chain.SetBranchAddress("distTarg", &distTarg);
	chain.SetBranchAddress("zStart", &zStart);
	chain.SetBranchAddress("dEdx", &dEdx);
	chain.SetBranchAddress("q", &qD);
	chain.SetBranchAddress("nVTPC1", &nVTPC1);
	chain.SetBranchAddress("nVTPC2", &nVTPC2);
	chain.SetBranchAddress("nGTPC", &nGTPC);
	chain.SetBranchAddress("nMTPC", &nMTPC);
	chain.SetBranchAddress("topology", &topology);
	chain.SetBranchAddress("nTOF", &nTOF);
	chain.SetBranchAddress("m2TOF", &m2TOF);
	chain.SetBranchAddress("tofScId", &tofScId);
	chain.SetBranchAddress("runNumber", &runNumber);
	
	int nEntries = chain.GetEntries();
	
	double zbor[7] = {0, 18, 36, 54, 72, 89.99, 90.01};
	double targZ = -657.51;
	vector<TH2D> zphRST;
	vector<TH2D> zphWST;
	
	TH2D zsigd("zsigd", "", 91, 0, 91, 20, 0, 20);
	
	for(int i = 0; i < 6; i++){
	  string nm = "zbinRST" + ConvertToString(i+1., 0);
	  TH2D temp(nm.c_str(), "", 150, 0, 30, 150, 0, 400);
	  temp.GetXaxis()->SetTitle("p [GeV/c]");
	  temp.GetXaxis()->SetTitleOffset(0.9);
	  temp.GetXaxis()->SetTitleSize(0.06);
	  temp.GetXaxis()->SetLabelSize(0.06);
	  temp.GetYaxis()->SetTitle("#theta [mrad]");
      temp.GetYaxis()->SetTitleOffset(0.9);
      temp.GetYaxis()->SetTitleSize(0.06);
      temp.GetYaxis()->SetLabelSize(0.06);
	  temp.SetStats(kFALSE);
	  string title = ConvertToString(zbor[i],0) + " #leq z < " + ConvertToString(zbor[i+1],0) + " cm";
	  temp.SetTitle(title.c_str());
	  zphRST.push_back(temp);
	  
	  
	  string nm1 = "zbinWST" + ConvertToString(i+1., 0);
	  TH2D temp1(nm1.c_str(), "", 150, 0, 30, 150, 0, 400);
	  temp1.GetXaxis()->SetTitle("p [GeV/c]");
	  temp1.GetXaxis()->SetTitleOffset(0.9);
	  temp1.GetXaxis()->SetTitleSize(0.06);
	  temp1.GetXaxis()->SetLabelSize(0.06);
	  temp1.GetYaxis()->SetTitle("#theta [mrad]");
      temp1.GetYaxis()->SetTitleOffset(0.9);
      temp1.GetYaxis()->SetTitleSize(0.06);
      temp1.GetYaxis()->SetLabelSize(0.06);
	  temp1.SetStats(kFALSE);
	  //string title = ConvertToString(zbor[i],0) + " #leq z < " + ConvertToString(zbor[i+1],0) + " cm";
	  temp1.SetTitle(title.c_str());
	  zphWST.push_back(temp1);
	}
	double thetaMin = 0;
	double thetaMax = 400;
	double thetaStep = 20;
	
	int n = (thetaMax - thetaMin)/thetaStep;
	
	int nTrack = 3100000;
	int nCount = 0;
	
	string allName;
	if(data==1){
		allName = "phiAll_data";
	}
	else{
		allName = "phiAll_mc";
	}
	TH2D thetaPhi("thetaPhi", "", 200, -90, 270, 200, 0, 500);
	//TH1D phiAll(allName.c_str(), "", 180, -90, 270);
	
	vector<vector<TH1D> > phiHistoRST;
	vector<vector<TH1D> > phiHistoWST;
	vector<TH1D> phiAll;
	
	for(int j = 0; j < n; ++j){
		string name1 = add + "phiAllth" + ConvertToString(j+1.0, 0);
		string title = ConvertToString(thetaMin+j*thetaStep, 0) + "#leq #theta < " + ConvertToString(thetaMin+(j+1)*thetaStep, 0) + " mrad";
		TH1D temp1(name1.c_str(), "", 360, -90, 270);
		temp1.SetTitle(title.c_str());
		temp1.SetStats(kFALSE);
		temp1.GetXaxis()->SetLabelSize(0.06);
		temp1.GetYaxis()->SetLabelSize(0.06);
		temp1.GetXaxis()->SetTitle("#phi [deg]");
		temp1.SetLineWidth(2);
		temp1.GetXaxis()->SetTitleSize(0.06);
		temp1.SetMinimum(1);
		
		if(data == 0)
			temp1.SetLineColor(2);
		phiAll.push_back(temp1);
		
	}
	TH1D toph("toph", "", 7, 8.5, 15.5);
	
	for(int i = 0; i < 6; i++){
		vector<TH1D> phh1;
		vector<TH1D> phh2;
		for(int j = 0; j < n; ++j){
			string name1 = add + "phiRSTz" + ConvertToString(i+1.0, 0) + "th" + ConvertToString(j+1.0, 0);
			string name2 = add + "phiWSTz" + ConvertToString(i+1.0, 0) + "th" + ConvertToString(j+1.0, 0);
			string title = ConvertToString(thetaMin+j*thetaStep, 0) + "#leq #theta < " + ConvertToString(thetaMin+(j+1)*thetaStep, 0) + " mrad";
			TH1D temp1(name1.c_str(), "", 90, -90, 270);
			TH1D temp2(name2.c_str(), "", 90, -90, 270);
			temp1.SetTitle(title.c_str());
			temp2.SetTitle(title.c_str());
			phh1.push_back(temp1);
			phh2.push_back(temp2);
		}
		phiHistoRST.push_back(phh1);
		phiHistoWST.push_back(phh2);
	}

	TH1D hsigmaR("sigmaR", "", 10, 0, 10);
	TH1D hsigmaR1("sigmaR1", "", 100, 0, 0.6);
	TH1D hz("hz", "", 100, targZ-5, targZ+95);
	TH1D distt("distt", "", 100, 0, 1.5);
	TH2D hxy("hxy", "", 100, -2, 2, 100, -2, 2);
	TH1D onephi("onephi", "", 90, -90, 270);
	TH1D ncl("ncl", "", 250, 0, 250);
	cout << "*******************************" << endl;
	if(data == 1){
		cout << "Processing data." << endl;
	}
	else{
		cout << "Processing MC." << endl;
	}
	cout << "Selected TPC track topology: " << topcut << endl;
	
	cout << "Number of entries: " << nEntries << endl;
	cout << "*******************************" << endl;	
	for(int i = 0; i < nEntries; ++i){
		
		chain.GetEntry(i,1);
		
		if(!((i+1)%100000))
			cout << "Processing event # "  << i+1 << endl;
		
		//if(nCount > nTrack) break;	
		
		for(int j = 0; j < px->size(); j++){
			//if(dEdx->at(j) > dEdxCut) continue;
			//if(qD->at(j) > 0) continue;
			int k = -1;
			if(topology->at(j) == 1) k = 1;
			else if(topology->at(j) == 2) k = 2;
			else if(topology->at(j) == 3) k = 3;
			else if(topology->at(j) == 4) k = 4;
			else if(topology->at(j) == 5) k = 5;
			else if(topology->at(j) == 6) k = 6;
			else if(topology->at(j) == 7) k = 7;
			else if(topology->at(j) == 9) k = 1;
			else if(topology->at(j) == 10) k = 2;
			else if(topology->at(j) == 11) k = 3;
			else if(topology->at(j) == 12) k = 4;
			else if(topology->at(j) == 13) k = 5;
			else if(topology->at(j) == 14) k = 6;
			else if(topology->at(j) == 15) k = 7;
			
			if(k < 0) continue;
			if(sqrt(px->at(j)*px->at(j)+py->at(j)*py->at(j)+pz->at(j)*pz->at(j)) < 0.2) continue;
			cuts[0][0] +=1.;
			cuts[k][0] +=1.;
			if(nTOF->at(j) < 1) continue;
			cuts[0][1] +=1.;
			cuts[k][1] +=1.;
			//if(zLast->at(j) < 580) continue;
			if(topology->at(j) == 1){
				//if(qD->at(j)*px->at(j) < 0)continue;
				if(nVTPC1->at(j) < 25) continue;
				//if(nVTPC1->at(j) < 40) continue;
			}
			else if(topology->at(j) == 2){
				//if(qD->at(j)*px->at(j) < 0)continue;
				if(nVTPC2->at(j) < 25) continue;
				//if(nVTPC2->at(j) < 45) continue;
			}
			else if(topology->at(j) == 9){
				if(nVTPC1->at(j) < 25) continue;
				//if(nVTPC1->at(j) + nMTPC->at(j) < 40) continue;
			}			
			else if(topology->at(j) == 10){
				if(nVTPC2->at(j) < 25) continue;
				//if(nVTPC2->at(j) + nMTPC->at(j) < 45) continue;
			}
			else if(topology->at(j) == 12){
				//if(qD->at(j)*px->at(j) < 0)continue;
				if(nGTPC->at(j) < 5) continue;
				if(nMTPC->at(j) < 30) continue;
				//if(nGTPC->at(j) < 6) continue;
				//if(nMTPC->at(j) < 30) continue;
			}
			else {
				if(nVTPC1->at(j) + nVTPC2->at(j) + nGTPC->at(j) < 20) continue;
				//if(nVTPC1->at(j) + nVTPC2->at(j) + nGTPC->at(j) < 50) continue;
				//if(nMTPC->at(j) < 30) continue;			
			}
			if(nVTPC1->at(j) + nVTPC2->at(j)  +nMTPC->at(j) > 70) continue;
			//if(topology->at(j) < 9) continue;
			//if(px->at(j)*qD->at(j) > 0) continue;
			cuts[0][2] +=1.;
			cuts[k][2] +=1.;			
			if(topcut != 0){
				if(topology->at(j) != topcut) continue;
			}
			//if(z->at(j)+657.51 > 18) continue;
			double theta = 1000*acos(pz->at(j)/sqrt(px->at(j)*px->at(j)+py->at(j)*py->at(j)+pz->at(j)*pz->at(j)));
			double phi = acos(px->at(j)/sqrt(px->at(j)*px->at(j)+py->at(j)*py->at(j)))*180/TMath::Pi();
			
			
			/*if(topology->at(j) == 14){
				if(theta < 40){
					if(px->at(j)*qD->at(j) < 0) continue;
				}			
			}			
			if(theta > 380) continue;
			if(sqrt(px->at(j)*px->at(j)+py->at(j)*py->at(j)+pz->at(j)*pz->at(j)) < 0.1) continue;*/
			//if(theta < 40) continue;
			int indexz = -1;
			for(int k = 0; k<6; k++){
			  if(z->at(j)-targZ >= zbor[k] && z->at(j)-targZ < zbor[k+1]){
			    //if(topology->at(j)>11)
			    
				if(qD->at(j)*px->at(j) > 0){
					zphRST.at(k).Fill(sqrt(px->at(j)*px->at(j)+py->at(j)*py->at(j)+pz->at(j)*pz->at(j)), theta);
				}
				else{
					zphWST.at(k).Fill(sqrt(px->at(j)*px->at(j)+py->at(j)*py->at(j)+pz->at(j)*pz->at(j)), theta);
				}
			    indexz = k;
			  }
			}
			

			nCount++;
			if(px->at(j)>0 && py->at(j) < 0){
				phi = -1*phi;
			}
			else if(px->at(j)<0 && py->at(j) < 0){
				phi = 360 - phi;
			}	
			
			/*if(indexz < 0) continue;
			if(indexz  < 5 && topology->at(j) == 10) continue;
			if(indexz  < 5 && topology->at(j) == 12) continue;
			if(indexz  < 5 && topology->at(j) == 14) continue;
			if(indexz == 5){
				double r = sqrt((x->at(j)-0.1350)*(x->at(j)-0.1350) + (y->at(j)-0.1258)*(y->at(j)-0.1258));
				if(r > 1.1) continue;
			}*/
			/*if(indexz  != 2) continue;
			if(theta > 60) continue;
			if(theta < 40) continue;
			double mom = sqrt(px->at(j)*px->at(j)+py->at(j)*py->at(j)+pz->at(j)*pz->at(j));
			if(mom > 3) continue;
			if(mom < 2) continue;*/
			//if(indexz  == 1) continue;
			//if(theta> 400 && phi > 190) continue;
			//if(theta> 400 && phi < -10 ) continue;	
			//if(theta> 400 && phi > 10 && phi < 170 ) continue;	
			
			if(theta >= 0 && theta < 20){
				if((phi < -70 || phi > 70) && (phi < 110 || phi > 250)) continue;
			}
			else if(theta >= 20 && theta < 40){
				if((phi < -60 || phi > 60) && (phi < 140 || phi > 220)) continue;
			}
			else if(theta >= 40 && theta < 60){
				if((phi < -45 || phi > 35) && (phi < 145 || phi > 225)) continue;
			}
			else if(theta >= 60 && theta < 80){
				if((phi < -30 || phi > 30) && (phi < 150 || phi > 210)) continue;
			}
			else if(theta >= 80 && theta < 100){
				if((phi < -20 || phi > 20) && (phi < 160 || phi > 200)) continue;
			}
			else if(theta >= 100 && theta < 120){
				if((phi < -20 || phi > 20) && (phi < 160 || phi > 200)) continue;
			}
			else if(theta >= 120 && theta < 140){
				if((phi < -15 || phi > 15) && (phi < 165 || phi > 195)) continue;
			}
			else if(theta >= 140 && theta < 160){
				if((phi < -13 || phi > 13) && (phi < 167 || phi > 193)) continue;
			}
			else if(theta >= 160 && theta < 200){
				if((phi < -12 || phi > 12) && (phi < 168 || phi > 192)) continue;
			}
			else if(theta >= 200 && theta < 240){
				if((phi < -11 || phi > 11) && (phi < 169 || phi > 191)) continue;
			}
			else if(theta >= 240 && theta < 400){
				if((phi < -10 || phi > 10) && (phi < 170 || phi > 190)) continue;
			}
			cuts[0][3] +=1.;
			cuts[k][3] +=1.;	
			
			//if((phi < -40 || phi > 45) && (phi < 155 || phi > 225)) continue;		
			/*if(theta >= 0 && theta < 20){
				if((phi < -60 || phi > 60) && (phi < 120 || phi > 240)) continue;
			}
			else if(theta >= 20 && theta < 40){
				if((phi < -60 || phi > 60) && (phi < 150 || phi > 210)) continue;
			}
			else if(theta >= 40 && theta < 60){
				if((phi < -40 || phi > 40) && (phi < 145 || phi > 225)) continue;
			}
			else if(theta >= 60 && theta < 100){
				if((phi < -25 || phi > 25) && (phi < 155 || phi > 205)) continue;
			}
			else if(theta >= 100 && theta < 140){
				if((phi < -15 || phi > 15) && (phi < 165 || phi > 195)) continue;
			}
			else if(theta >= 140 && theta < 600){
				if((phi < -10 || phi > 10) && (phi < 170 || phi > 190)) continue;
			}*/

			if(theta>380) continue;
			//if(topology->at(j) == 10) continue;
			//if(topology->at(j) == 2) continue;
			//double sigmaR = sqrt(x->at(j)*x->at(j)*xE->at(j)*xE->at(j) + y->at(j)*y->at(j)*yE->at(j)*yE->at(j))/sqrt(x->at(j)*x->at(j) + y->at(j)*y->at(j));
			double r = sqrt((x->at(j)-xt)*(x->at(j)-xt)+(y->at(j)-yt)*(y->at(j)-yt));

			double sigmaR = sqrt((x->at(j)-xt)*(x->at(j)-xt)*xE->at(j)*xE->at(j)+(y->at(j)-yt)*(y->at(j)-yt)*yE->at(j)*yE->at(j))/r;
			hsigmaR.Fill(distTarg->at(j)/sigmaR);
			hsigmaR1.Fill(sigmaR);
			thetaPhi.Fill(phi, theta);
			toph.Fill(topology->at(j));
			//hsigd.Fill(sigmaR, distTarg->at(j));
			ncl.Fill(nVTPC1->at(j)+nVTPC2->at(j)+nMTPC->at(j));
			distt.Fill(distTarg->at(j));
			zsigd.Fill(z->at(j)-targZ, distTarg->at(j)/sigmaR);
			
			//errHistos.at(k-1).Fill(sigmaR);
			errHistos.at(k-1).Fill(distTarg->at(j));
			if(indexz==5) hxy.Fill(x->at(j), y->at(j));
			if(distTarg->at(j)/sigmaR > 3) continue;
			
			cuts[0][4] +=1.;
			cuts[k][4] +=1.;
			
			hz.Fill(z->at(j));		
			onephi.Fill(phi);		
			/*if(data==1)
				phiAll.Fill(phi, scale);
			else
				phiAll.Fill(phi, scale);*/
			for(int k = 0; k < n; k++){
				if(theta >= thetaMin + k*thetaStep && theta < thetaMin + (k+1.)*thetaStep){
						
					if(qD->at(j)*px->at(j) > 0){
						phiHistoRST.at(indexz).at(k).Fill(phi, scale);
					}
					else{
						phiHistoWST.at(indexz).at(k).Fill(phi, scale);
					}
					phiAll.at(k).Fill(phi,scale);
					break;
				}
			}	
		}

	}
	cout << nCount << " " << nEntries << endl;
	outFile.cd();

	//phiAll.Scale(scale);
	//phiAll.Write();
	for(int j = 0; j < n; ++j){
		phiAll.at(j).Write();
	}
	/*for(int i = 0; i < 6; ++i){
		zphRST.at(i).Write();
		zphWST.at(i).Write();
		for(int j = 0; j < n; ++j){
			if(data==0){
			phiHistoRST.at(i).at(j).SetLineColor(2);
			phiHistoWST.at(i).at(j).SetLineColor(2);
		}
			phiHistoRST.at(i).at(j).Write();
			phiHistoWST.at(i).at(j).Write();
		}
	}*/
	for(int i = 0; i < 6; i++){
	  
	}
	
	for(int i = 0; i < zsigd.GetNbinsX(); i++){
		double n = 0;
		for(int j = 0; j < zsigd.GetNbinsY()+1; j++){
			n+= zsigd.GetBinContent(i+1, j+1);
		}	
		cout << n << endl;
		if (n <1) continue;
		for(int j = 0; j < zsigd.GetNbinsY(); j++){
			zsigd.SetBinContent(i+1, j+1, zsigd.GetBinContent(i+1, j+1)/n);
		}	
	}
	hsigmaR.Write();
	hsigmaR1.Write();
	thetaPhi.Write();
	toph.Write();
	hz.Write();
	distt.Write();
	zsigd.Write();
	hxy.Write();
	onephi.Write();
	ncl.Write();
	for(int i = 0; i < 7; i++){
		errHistos.at(i).Write();
	}
	outFile.Close();
	
	for(int i = 0; i < 8; i++){
		cout << "-------------------------------------------------------" << endl;
		for(int j = 0; j < 5; j++){
			cout << "$" << cuts[i][j] << "$ & ";
		}
		cout << endl;
		for(int j = 0; j < 5; j++){
			cout << "$" << 100*cuts[i][j]/cuts[0][j] << "$ & ";
		}
		cout << endl;
	}
}





