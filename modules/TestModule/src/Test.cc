#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <cstdlib>
#include <stdexcept>
//#include <TPRegexp.h>
#include "TestConfig.h"
#include <TH1D.h>
#include <TH2D.h>
#include <TTree.h>
#include <TFile.h>
#include <evt/Event.h>
#include <evt/EventHeader.h>
#include <io/EventFileChain.h>
#include <det/TriggerConst.h>
#include <det/BPDConst.h>
#include <evt/RawEvent.h>
#include <boost/format.hpp>
#include "Test.h"
#include "TrackTools.h"
#include <utl/Vector.h>
#include <utl/CovarianceMatrix.h>


using namespace std;
using namespace io;
using namespace evt;
using namespace evt::rec;
using namespace det;
using namespace utl;
using namespace boost;
using namespace TrackTools;

void Test(int n){

	TestConfig cfg;


	EventFileChain eventFileChain(cfg.GetJob());
	Event event;

	
	//cout << cfg.GetTarget().GetLength() << endl;
	unsigned int nEvents = 0;


   	int runNumber;
	while (eventFileChain.Read(event) == eSuccess && nEvents < n) {


	
		bool skipRun = false;
		EventHeader &evHeader = event.GetEventHeader();

		if(evHeader.GetRunNumber() != runNumber){
			runNumber = evHeader.GetRunNumber();
			skipRun = cfg.SkipRun(runNumber);
		}

		if(skipRun) continue;

   		++nEvents;
    
    	if (!(nEvents%1000))
      		cout << " --->Processing event # " << nEvents << endl;


		//cout << event.GetRecEvent().GetBeam().GetMomentum().GetZ() << endl;
		const RecEvent& recEvent = event.GetRecEvent();

		//cout << beamInput.Par[2] << endl;
		for (std::list<rec::Track>::const_iterator iter = recEvent.Begin<rec::Track>();
           iter != recEvent.End<rec::Track>(); ++iter)
      	{
			const Track& track = *iter;
			const utl::CovarianceMatrix& cm = track.GetCovarianceMatrix();
			//cout << cm.GetExtent() << endl;
			
			if(track.GetStatus() != 0) continue;
			if(track.GetNumberOfClusters() < 30) continue;

			TrackPar trackPar(eNA61);
			
			double pxz = sqrt(track.GetMomentum().GetX()*track.GetMomentum().GetX()+track.GetMomentum().GetZ()*track.GetMomentum().GetZ());
			double p1 = track.GetFirstPointOnTrack().GetX();
			double p2 = track.GetFirstPointOnTrack().GetY();
			double p3 = track.GetCharge()/pxz;
			double p4 = track.GetMomentum().GetY()/pxz;
			double p5 = atan(track.GetMomentum().GetZ()/track.GetMomentum().GetX());
			
			//*********************
			double tx = track.GetMomentum().GetX()/track.GetMomentum().GetZ();
			double ty = track.GetMomentum().GetY()/track.GetMomentum().GetZ();
			double qdp = track.GetCharge()/track.GetMomentum().GetMag();
			//cout << p3 << " " << (1-tan(p5/2))/sqrt(tan(p5/2)*(2-tan(p5/2))) << " " << qdp*sqrt(1+tx*tx+ty*ty)/sqrt(1+tx*tx)<<endl;
			//cout << (1-tan(p5/2.))/sqrt(tan(p5/2.)*(2-tan(p5/2.))) << " " << p4/sqrt(tan(p5/2.)*(2-tan(p5/2.))) << " " << p3/sqrt(1+p4*p4) << endl;
			trackPar.SetPar(0, p3);
			trackPar.SetPar(1, p4);
			trackPar.SetPar(2, p5);
			trackPar.SetPar(3, p1);
			trackPar.SetPar(4, p2);
			trackPar.SetCharge(track.GetCharge());
			trackPar.SetZ(track.GetFirstPointOnTrack().GetZ());
			
			int ind = 0;

			for(int i = 0; i < cm.GetExtent(); i++){
				for(int j = 0; j < cm.GetExtent(); j++){
					//cout << cm[i][j] << "	";
					
					if(j>=i){
						trackPar.SetCov(ind, cm[i][j]);
						ind++;
					}
				}
				//cout << endl;			
			}
			if(track.GetFirstPointOnTrack().GetX() > 0 ) continue;
			if( track.GetFirstPointOnTrack().GetZ() > -470) continue;
			cout << "Before Conversion" << endl;
			trackPar.Print();
			cout << "Conversion 1" << endl;
			ConvertTrackPar(trackPar, eKisel);
			trackPar.Print();
			cout << "Conversion 2" << endl;
			ConvertTrackPar(trackPar, eNA61);
			trackPar.Print();
		}
	}

}

