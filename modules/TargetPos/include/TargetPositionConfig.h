#ifndef TARGETPOSITIONCONFIG_H
#define TARGETPOSITIONCONFIG_H
#include <iostream>
#include <vector>
#include <string>
#include "TargetAna.h"


using namespace std;

class TargetPositionConfig{

	public:
		TargetPositionConfig()  {Read();}  //Constructor

		//*************************Get functions***********************************

		string GetOutputDir(){return fOutputDir;}
		bool PlotSlices(){return fPlotSlices;}
		bool CalculateAlignment(){return fCalculateAlignment;}
		bool PlotBPDTPCAlignment(){return fBPDTPCAlignment;}
		bool PlotZDistributions(){return fZDistributions;}
		vector<double>& GetBeamCut(){return fBeam;}
		double GetSliceStep(){return fStep;}
		double GetSliceRadius(){return fRadius;}
		double GetAlignmentSliceMin(){return fAlignmentSlice.at(0);}
		double GetAlignmentSliceMax(){return fAlignmentSlice.at(1);}
		double GetZPar(int i);
		double GetZParMin(int i);
		double GetZParMax(int i);
		
		double GetBPDTPCXPar(int i);
		double GetBPDTPCXParMin(int i);
		double GetBPDTPCXParMax(int i);
		double GetBPDTPCYPar(int i);
		double GetBPDTPCYParMin(int i);
		double GetBPDTPCYParMax(int i);
		//*************************Set functions***********************************
		void Read(); //Reads main config file.

		void SetOutputDir(string outputDir){fOutputDir = outputDir;}
		void SetPlotSlices(bool val){fPlotSlices = val;}
		void SetCalculateAlignment(bool val){fCalculateAlignment = val;}
		void SetBPDTPCAlignment(bool val){fBPDTPCAlignment = val;}
		void SetPlotZDistributions(bool val){fZDistributions = val;}
		void SetBeamCut(vector<double> &beam){fBeam = beam;}
		void SetSliceStep(double step){fStep = step;}
		void SetSliceRadius(double radius){fRadius = radius;}
		void SetAlignmentSliceMin(double min){fAlignmentSlice.at(0) = min;}
		void SetAlignmentSliceMax(double max){fAlignmentSlice.at(1) = max;}
		void SetZPar(int i, double val);
		void SetZParMin(int i, double val);
		void SetZParMax(int i, double val);
		
		void SetBPDTPCXPar(int i, double val);
		void SetBPDTPCXParMin(int i, double val);
		void SetBPDTPCXParMax(int i, double val);
		
		void SetBPDTPCYPar(int i, double val);
		void SetBPDTPCYParMin(int i, double val);
		void SetBPDTPCYParMax(int i, double val);
		string GetTimeString();
		
		bool IsBeamGood(double ax, double bx, double ay, double by);
		bool SkipRun(int runNumber);
	private:


		string fOutputDir;
		vector<double> fBeam;
		bool fPlotSlices;
		bool fCalculateAlignment;
		bool fBPDTPCAlignment;
		bool fZDistributions;
		double fStep;
		double fRadius;
		vector<double> fAlignmentSlice;
		
		vector<double> fZPars;
		vector<double> fZParMin;
		vector<double> fZParMax;
		vector<double> fBPDTPCXPars;
		vector<double> fBPDTPCXParMin;
		vector<double> fBPDTPCXParMax;	
		vector<double> fBPDTPCYPars;
		vector<double> fBPDTPCYParMin;
		vector<double> fBPDTPCYParMax;		
		
};

#endif
