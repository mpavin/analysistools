#include <stdlib.h>
#include "PhaseSpace.h"
#include "TargetPosition.h"
#include "TargetPositionConfig.h"
#include "TrackSelection.h"
#include "Functions.h"
#include "PlotHisto.h"
#include "PlotGraph.h"
#include <sstream>

#include <TFile.h>
#include <TH2D.h>
#include <TCanvas.h>
#include <TTree.h>
#include <TF1.h>
#include <utl/Vector.h>


using namespace utl; 

void TargetPosition(string inputFile){

	TargetPositionConfig cfg;
	PhaseSpace phSpace;

	TFile input(inputFile.c_str(), "READ");
	TTree *tree = (TTree*) input.Get("TargetAnalysis");
		
	AlignmentData aData;
	
	string name = cfg.GetOutputDir() + "targetposition_" + cfg.GetTimeString() + ".root";
	TFile output(name.c_str(), "RECREATE");
	
	vector<double> *xS=0;
	vector<double> *yS=0;
	vector<double> *zS=0;
	
	tree->SetBranchAddress("AlignmentData", &aData);
	tree->SetBranchAddress("xS", &xS);
	tree->SetBranchAddress("yS", &yS);
	tree->SetBranchAddress("zS", &zS);
	
	int nEntries = tree->GetEntries();


	int nBins = phSpace.GetNumberOfBins();
	vector<PlotHisto> slices;
	for(int i = 0; i < nBins+1; ++i){
		PlotHisto temp(2, "TargetHistos","slice_rst","slice_wst", "slice", ConvertToString(i+1));
		slices.push_back(temp);
	}
	PlotHisto BPDTPCx(1, "TargetHistos","bpdtpcX_rst","bpdtpcX_wst", "bpdtpcX", "");
	PlotHisto BPDTPCy(1, "TargetHistos","bpdtpcY_rst","bpdtpcY_wst", "bpdtpcY", "");
	PlotHisto zDist(1, "TargetHistos","zExtrap_rst","zExtrap_wst", "zExtrap", "");
	PlotHisto zPos(1, "TargetHistos","zPosition_rst","zPosition_wst", "zPosition", "");
	PlotHisto BPDTPCProfile(2, "TargetHistos","bpdtpcProfile_rst","bpdtpcProfile_wst", "bpdtpcProfile", "");

	for(int i = 0; i < nEntries; ++i){
		//cout << "radi1" << endl;
		tree->GetEntry(i);
		//cout << "radi2" << endl;
		if((i+1)%10000 == 0) 
			cout << "Processing entry #" << i+1 << "!" << endl;
		int nBin = 0;
		

		for(int j = 0; j < xS->size(); j++){
			if(aData.px*aData.q > 0){
				if(cfg.PlotSlices())
					slices.at(nBins-1-j).Fill(1, xS->at(j), yS->at(j));
			}
			else{
				if(cfg.PlotSlices())
					slices.at(nBins-1-j).Fill(2, xS->at(j), yS->at(j));			
			}
		}
		/*for(int j = 0; j < nBins; j++){
			Bin& bin = phSpace.GetBin(j + 1);
			//z[j] = bin.GetMidValue();
			if(bin.GetLowValue(1) <= aData.zS+657.40 && aData.zS+657.40 < bin.GetHighValue(1)){
				nBin = j;
				break;
			}
			
		}*/

		if(aData.px*aData.q > 0){
			if(cfg.PlotZDistributions())
				zDist.Fill(1, aData.z);
			//if(cfg.PlotSlices())
				//slices.at(nBin).Fill(1, aData.xS, aData.yS);
				//slices.at(nBins).Fill(1, aData.beamX, aData.beamY);
			
		}
		else{
			if(cfg.PlotZDistributions())
				zDist.Fill(2, aData.z);
			//if(cfg.PlotSlices())
				//slices.at(nBin).Fill(2, aData.xS, aData.yS);
				//slices.at(nBins).Fill(2, aData.beamX, aData.beamY);
		}
		
		if(cfg.PlotBPDTPCAlignment()){
			double p = sqrt(aData.px*aData.px + aData.py*aData.py + aData.pz*aData.pz);
			if(p>1 && p<20){
				if(aData.px*aData.q > 0){
					//cout << cfg.GetAlignmentSliceMin() << " " << cfg.GetAlignmentSliceMax() << endl;
					if(aData.z > cfg.GetAlignmentSliceMin() && aData.z < cfg.GetAlignmentSliceMax()){
						BPDTPCx.Fill(1, aData.x - aData.beamX);
						BPDTPCy.Fill(1, aData.y - aData.beamY);
					}			
				}
				else{
					if(aData.z > cfg.GetAlignmentSliceMin() && aData.z < cfg.GetAlignmentSliceMax()){
						BPDTPCx.Fill(2, aData.x - (aData.beamSlopeX*aData.z + aData.beamInterceptX));
						BPDTPCy.Fill(2, aData.y - (aData.beamSlopeY*aData.z + aData.beamInterceptY));
					}			
				}
			}
			if(!cfg.IsBeamGood(aData.beamSlopeX, aData.beamInterceptX, aData.beamSlopeY, aData.beamInterceptY)) continue;
		
			//if(aData.beamX*aData.x < 0) continue;
			//if(aData.beamY*aData.y < 0) continue;
			//if(aData.beamX*aData.px < 0) continue;
			//if(aData.beamY*aData.py < 0) continue;			
			//if(aData.beamX < 0 && aData.x > 0) continue;
			//if(aData.beamX > 0 && aData.x < 0) continue;
			//if(aData.beamX > 0){
			double pxy = sqrt(aData.px*aData.px + aData.py*aData.py);
			if(atan(pxy/aData.pz) < 0.10) continue;
			if(atan(pxy/aData.pz) > 0.18) continue;
			//if(aData.beamX*aData.px < 0) continue;
			//if(aData.beamY*aData.py < 0) continue;
			double rB = sqrt(aData.beamX*aData.beamX + (aData.beamY-0.28)*(aData.beamY-0.28));
			if(rB > 1.28 || rB < 0.8) continue;
			
			//if(aData.px*aData.q > 0){
			//if(aData.beamY - 0.28 > 0 && aData.py > 0){
			if(aData.beamX > 0 && aData.px > 0){
				BPDTPCProfile.Fill(1, aData.beamX, aData.beamY);
				zPos.Fill(1, aData.z);
			}
			//else if(aData.beamY - 0.28 < 0 && aData.py < 0){
			else if(aData.beamX < 0 && aData.px < 0){
				BPDTPCProfile.Fill(2, aData.beamX, aData.beamY);		
				zPos.Fill(2, aData.z);
			}
		}
	}	

	input.Close();
	
	if(cfg.PlotZDistributions()){
		output.mkdir("ZDistribution");
		output.cd("ZDistribution");
		
		zDist.GetAllHisto()->Write();
		zDist.GetRSTHisto()->Write();
		zDist.GetWSTHisto()->Write();
	}
	
	if(cfg.PlotSlices()){
		output.mkdir("Slices");
		output.cd("Slices");
		
		for(int i = 0; i < nBins; ++i){
			slices.at(i).GetAllHisto()->Write();
			slices.at(i).GetRSTHisto()->Write();
			slices.at(i).GetWSTHisto()->Write();
		}
	}	
	
	if(cfg.CalculateAlignment()){
		double val[3];
		double z[nBins];
		for(int j = 0; j < nBins; j++){
			Bin& bin = phSpace.GetBin(j + 1);
			z[j] = bin.GetMidValue();
		}
		double xC[nBins];
		double yC[nBins];

		double xCRST[nBins];
		double yCRST[nBins];
		double xCWST[nBins];
		double yCWST[nBins];
		double xE[nBins];
		double yE[nBins];
		double zE[nBins];
		double stE[nBins];
		
		//FindMaximum(val, cfg.GetSliceRadius(), cfg.GetSliceStep(), slices.at(nBins).GetAllHisto());
		slices.at(nBins).GetAllHisto()->Write();
		//cout << val[0] << " " << val[1] << " " << val[2] << endl;
		for(int i = 0; i < nBins; ++i){
			zE[i] = 0;
			stE[i] = cfg.GetSliceStep();
			
			cout << "Processing Slice #" << i+1 << endl;
			FindMaximum(val, cfg.GetSliceRadius(), cfg.GetSliceRadius(), cfg.GetSliceStep(), slices.at(i).GetAllHisto());
			xC[i] = val[0];
			yC[i] = val[1];

			FindMaximum(val, cfg.GetSliceRadius(), cfg.GetSliceRadius(), cfg.GetSliceStep(), slices.at(i).GetRSTHisto());
			xCRST[i] = val[0];
			yCRST[i] = val[1];
			
			FindMaximum(val, cfg.GetSliceRadius(), cfg.GetSliceRadius(), cfg.GetSliceStep(), slices.at(i).GetWSTHisto());
			xCWST[i] = val[0];
			yCWST[i] = val[1];	
			
			/*xE[i] = abs(xCRST[i]-xCWST[i]);
			yE[i] = abs(yCRST[i]-yCWST[i]);	*/
			xE[i] = cfg.GetSliceStep();	
			yE[i] = cfg.GetSliceStep();	
			
		}
		
		PlotGraph allX(nBins, z, xC, zE, xE, "TargetGraphs", "alignmentX", "");	
		PlotGraph rstX(nBins, z, xCRST, zE, stE, "TargetGraphs", "alignmentX_rst", "");
		PlotGraph wstX(nBins, z, xCWST, zE, stE, "TargetGraphs", "alignmentX_wst", "");
		PlotGraph allY(nBins, z, yC, zE, yE, "TargetGraphs", "alignmentY", "");	
		PlotGraph rstY(nBins, z, yCRST, zE, stE, "TargetGraphs", "alignmentY_rst", "");
		PlotGraph wstY(nBins, z, yCWST, zE, stE, "TargetGraphs", "alignmentY_wst", "");
		output.mkdir("TargetAlignment");
		output.cd("TargetAlignment");
		
		TF1* lineX_all = new TF1("lineX_all", "1++x", zE[0]-(z[nBins-1]-z[nBins-2])/2., zE[nBins-1]+(z[nBins-1]-z[nBins-2])/2.);
		TF1* lineX_rst = new TF1("lineX_rst", "1++x", zE[0]-(z[nBins-1]-z[nBins-2])/2., zE[nBins-1]+(z[nBins-1]-z[nBins-2])/2.);		
		TF1* lineX_wst = new TF1("lineX_wst", "1++x", zE[0]-(z[nBins-1]-z[nBins-2])/2., zE[nBins-1]+(z[nBins-1]-z[nBins-2])/2.);
		TF1* lineY_all = new TF1("lineY_all", "1++x", zE[0]-(z[nBins-1]-z[nBins-2])/2., zE[nBins-1]+(z[nBins-1]-z[nBins-2])/2.);
		TF1* lineY_rst = new TF1("lineY_rst", "1++x", zE[0]-(z[nBins-1]-z[nBins-2])/2., zE[nBins-1]+(z[nBins-1]-z[nBins-2])/2.);	
		TF1* lineY_wst = new TF1("lineY_wst", "1++x", zE[0]-(z[nBins-1]-z[nBins-2])/2., zE[nBins-1]+(z[nBins-1]-z[nBins-2])/2.);

	
		
		allX.FitFunction(lineX_all, 2.5, z[nBins-1]+2*(z[nBins-1]-z[nBins-2]));
		rstX.FitFunction(lineX_rst, 2.5, z[nBins-1]+2*(z[nBins-1]-z[nBins-2]));
		wstX.FitFunction(lineX_wst, 2.5, z[nBins-1]+2*(z[nBins-1]-z[nBins-2]));
		
		allY.FitFunction(lineY_all, 0, z[nBins-1]+2*(z[nBins-1]-z[nBins-2]));
		rstY.FitFunction(lineY_rst, 0, z[nBins-1]+2*(z[nBins-1]-z[nBins-2]));
		wstY.FitFunction(lineY_wst, 0, z[nBins-1]+2*(z[nBins-1]-z[nBins-2]));

		allX.GetGraph()->Write();
		rstX.GetGraph()->Write();
		wstX.GetGraph()->Write();
		allY.GetGraph()->Write();
		rstY.GetGraph()->Write();
		wstY.GetGraph()->Write();	
		

		allX.GetCanvas()->Write();
		rstX.GetCanvas()->Write();
		wstX.GetCanvas()->Write();
		allY.GetCanvas()->Write();
		rstY.GetCanvas()->Write();
		wstY.GetCanvas()->Write();
		
	}
	if(cfg.PlotBPDTPCAlignment()){
		output.mkdir("BPDTPCAlignment");
		output.cd("BPDTPCAlignment");
		
		BPDTPCProfile.GetAllHisto()->Write();
		BPDTPCProfile.GetRSTHisto()->Write();
		BPDTPCProfile.GetWSTHisto()->Write();
		
		TF1* bpdtpcX_all = new TF1("bpdtpcX_all", "[0]*([1]*TMath::Exp(-(x-[2])*(x-[2])/(2*[3]*[3]))+(1-[1])*TMath::Exp(-(x-[2])*(x-[2])/(2*[4]*[4])))", -0.45, 0.45);
		bpdtpcX_all->SetParName(0, "N");
		bpdtpcX_all->SetParameter(0, cfg.GetBPDTPCXPar(0));
		bpdtpcX_all->SetParLimits(0, cfg.GetBPDTPCXParMin(0), cfg.GetBPDTPCXParMax(0));
		bpdtpcX_all->SetParName(1, "p");
		bpdtpcX_all->SetParameter(1, cfg.GetBPDTPCXPar(1));
		bpdtpcX_all->SetParLimits(1, cfg.GetBPDTPCXParMin(1), cfg.GetBPDTPCXParMax(1));
		bpdtpcX_all->SetParName(2, "#mu");
		bpdtpcX_all->SetParameter(2, cfg.GetBPDTPCXPar(2));
		bpdtpcX_all->SetParLimits(2, cfg.GetBPDTPCXParMin(2), cfg.GetBPDTPCXParMax(2));
		bpdtpcX_all->SetParName(3, "#sigma_{1}");
		bpdtpcX_all->SetParameter(3, cfg.GetBPDTPCXPar(3));
		bpdtpcX_all->SetParLimits(3, cfg.GetBPDTPCXParMin(3), cfg.GetBPDTPCXParMax(3));
		bpdtpcX_all->SetParName(4, "#sigma_{2}");
		bpdtpcX_all->SetParameter(4, cfg.GetBPDTPCXPar(4));
		bpdtpcX_all->SetParLimits(4, cfg.GetBPDTPCXParMin(4), cfg.GetBPDTPCXParMax(4));
		
		TF1* bpdtpcX_rst = new TF1("bpdtpcX_rst", "[0]*([1]*TMath::Exp(-(x-[2])*(x-[2])/(2*[3]*[3]))+(1-[1])*TMath::Exp(-(x-[2])*(x-[2])/(2*[4]*[4])))", -0.45, 0.45);
		bpdtpcX_rst->SetParName(0, "N");
		bpdtpcX_rst->SetParameter(0, cfg.GetBPDTPCXPar(0));
		bpdtpcX_rst->SetParLimits(0, cfg.GetBPDTPCXParMin(0), cfg.GetBPDTPCXParMax(0));
		bpdtpcX_rst->SetParName(1, "p");
		bpdtpcX_rst->SetParameter(1, cfg.GetBPDTPCXPar(1));
		bpdtpcX_rst->SetParLimits(1, cfg.GetBPDTPCXParMin(1), cfg.GetBPDTPCXParMax(1));
		bpdtpcX_rst->SetParName(2, "#mu");
		bpdtpcX_rst->SetParameter(2, cfg.GetBPDTPCXPar(2));
		bpdtpcX_rst->SetParLimits(2, cfg.GetBPDTPCXParMin(2), cfg.GetBPDTPCXParMax(2));
		bpdtpcX_rst->SetParName(3, "#sigma_{1}");
		bpdtpcX_rst->SetParameter(3, cfg.GetBPDTPCXPar(3));
		bpdtpcX_rst->SetParLimits(3, cfg.GetBPDTPCXParMin(3), cfg.GetBPDTPCXParMax(3));
		bpdtpcX_rst->SetParName(4, "#sigma_{2}");
		bpdtpcX_rst->SetParameter(4, cfg.GetBPDTPCXPar(4));
		bpdtpcX_rst->SetParLimits(4, cfg.GetBPDTPCXParMin(4), cfg.GetBPDTPCXParMax(4));
		
		TF1* bpdtpcX_wst = new TF1("bpdtpcX_wst", "[0]*([1]*TMath::Exp(-(x-[2])*(x-[2])/(2*[3]*[3]))+(1-[1])*TMath::Exp(-(x-[2])*(x-[2])/(2*[4]*[4])))", -0.45, 0.45);
		bpdtpcX_wst->SetParName(0, "N");
		bpdtpcX_wst->SetParameter(0, cfg.GetBPDTPCXPar(0));
		bpdtpcX_wst->SetParLimits(0, cfg.GetBPDTPCXParMin(0), cfg.GetBPDTPCXParMax(0));
		bpdtpcX_wst->SetParName(1, "p");
		bpdtpcX_wst->SetParameter(1, cfg.GetBPDTPCXPar(1));
		bpdtpcX_wst->SetParLimits(1, cfg.GetBPDTPCXParMin(1), cfg.GetBPDTPCXParMax(1));
		bpdtpcX_wst->SetParName(2, "#mu");
		bpdtpcX_wst->SetParameter(2, cfg.GetBPDTPCXPar(2));
		bpdtpcX_wst->SetParLimits(2, cfg.GetBPDTPCXParMin(2), cfg.GetBPDTPCXParMax(2));
		bpdtpcX_wst->SetParName(3, "#sigma_{1}");
		bpdtpcX_wst->SetParameter(3, cfg.GetBPDTPCXPar(3));
		bpdtpcX_wst->SetParLimits(3, cfg.GetBPDTPCXParMin(3), cfg.GetBPDTPCXParMax(3));
		bpdtpcX_wst->SetParName(4, "#sigma_{2}");
		bpdtpcX_wst->SetParameter(4, cfg.GetBPDTPCXPar(4));
		bpdtpcX_wst->SetParLimits(4, cfg.GetBPDTPCXParMin(4), cfg.GetBPDTPCXParMax(4));
		
		
		TF1* bpdtpcY_all = new TF1("bpdtpcY_all", "[0]*([1]*TMath::Exp(-(x-[2])*(x-[2])/(2*[3]*[3]))+(1-[1])*TMath::Exp(-(x-[2])*(x-[2])/(2*[4]*[4])))", -0.45, 0.45);
		bpdtpcY_all->SetParName(0, "N");
		bpdtpcY_all->SetParameter(0, cfg.GetBPDTPCYPar(0));
		bpdtpcY_all->SetParLimits(0, cfg.GetBPDTPCYParMin(0), cfg.GetBPDTPCYParMax(0));
		bpdtpcY_all->SetParName(1, "p");
		bpdtpcY_all->SetParameter(1, cfg.GetBPDTPCYPar(1));
		bpdtpcY_all->SetParLimits(1, cfg.GetBPDTPCYParMin(1), cfg.GetBPDTPCYParMax(1));
		bpdtpcY_all->SetParName(2, "#mu");
		bpdtpcY_all->SetParameter(2, cfg.GetBPDTPCYPar(2));
		bpdtpcY_all->SetParLimits(2, cfg.GetBPDTPCYParMin(2), cfg.GetBPDTPCYParMax(2));
		bpdtpcY_all->SetParName(3, "#sigma_{1}");
		bpdtpcY_all->SetParameter(3, cfg.GetBPDTPCYPar(3));
		bpdtpcY_all->SetParLimits(3, cfg.GetBPDTPCYParMin(3), cfg.GetBPDTPCYParMax(3));
		bpdtpcY_all->SetParName(4, "#sigma_{2}");
		bpdtpcY_all->SetParameter(4, cfg.GetBPDTPCYPar(4));
		bpdtpcY_all->SetParLimits(4, cfg.GetBPDTPCYParMin(4), cfg.GetBPDTPCYParMax(4));
		
		TF1* bpdtpcY_rst = new TF1("bpdtpcY_rst", "[0]*([1]*TMath::Exp(-(x-[2])*(x-[2])/(2*[3]*[3]))+(1-[1])*TMath::Exp(-(x-[2])*(x-[2])/(2*[4]*[4])))", -0.45, 0.45);
		bpdtpcY_rst->SetParName(0, "N");
		bpdtpcY_rst->SetParameter(0, cfg.GetBPDTPCYPar(0));
		bpdtpcY_rst->SetParLimits(0, cfg.GetBPDTPCYParMin(0), cfg.GetBPDTPCYParMax(0));
		bpdtpcY_rst->SetParName(1, "p");
		bpdtpcY_rst->SetParameter(1, cfg.GetBPDTPCYPar(1));
		bpdtpcY_rst->SetParLimits(1, cfg.GetBPDTPCYParMin(1), cfg.GetBPDTPCYParMax(1));
		bpdtpcY_rst->SetParName(2, "#mu");
		bpdtpcY_rst->SetParameter(2, cfg.GetBPDTPCYPar(2));
		bpdtpcY_rst->SetParLimits(2, cfg.GetBPDTPCYParMin(2), cfg.GetBPDTPCYParMax(2));
		bpdtpcY_rst->SetParName(3, "#sigma_{1}");
		bpdtpcY_rst->SetParameter(3, cfg.GetBPDTPCYPar(3));
		bpdtpcY_rst->SetParLimits(3, cfg.GetBPDTPCYParMin(3), cfg.GetBPDTPCYParMax(3));
		bpdtpcY_rst->SetParName(4, "#sigma_{2}");
		bpdtpcY_rst->SetParameter(4, cfg.GetBPDTPCYPar(4));
		bpdtpcY_rst->SetParLimits(4, cfg.GetBPDTPCYParMin(4), cfg.GetBPDTPCYParMax(4));
		
		TF1* bpdtpcY_wst = new TF1("bpdtpcY_wst", "[0]*([1]*TMath::Exp(-(x-[2])*(x-[2])/(2*[3]*[3]))+(1-[1])*TMath::Exp(-(x-[2])*(x-[2])/(2*[4]*[4])))", -0.45, 0.45);
		bpdtpcY_wst->SetParName(0, "N");
		bpdtpcY_wst->SetParameter(0, cfg.GetBPDTPCYPar(0));
		bpdtpcY_wst->SetParLimits(0, cfg.GetBPDTPCYParMin(0), cfg.GetBPDTPCYParMax(0));
		bpdtpcY_wst->SetParName(1, "p");
		bpdtpcY_wst->SetParameter(1, cfg.GetBPDTPCYPar(1));
		bpdtpcY_wst->SetParLimits(1, cfg.GetBPDTPCYParMin(1), cfg.GetBPDTPCYParMax(1));
		bpdtpcY_wst->SetParName(2, "#mu");
		bpdtpcY_wst->SetParameter(2, cfg.GetBPDTPCYPar(2));
		bpdtpcY_wst->SetParLimits(2, cfg.GetBPDTPCYParMin(2), cfg.GetBPDTPCYParMax(2));
		bpdtpcY_wst->SetParName(3, "#sigma_{1}");
		bpdtpcY_wst->SetParameter(3, cfg.GetBPDTPCYPar(3));
		bpdtpcY_wst->SetParLimits(3, cfg.GetBPDTPCYParMin(3), cfg.GetBPDTPCYParMax(3));
		bpdtpcY_wst->SetParName(4, "#sigma_{2}");
		bpdtpcY_wst->SetParameter(4, cfg.GetBPDTPCYPar(4));
		bpdtpcY_wst->SetParLimits(4, cfg.GetBPDTPCYParMin(4), cfg.GetBPDTPCYParMax(4));
		//cout << cfg.GetBPDTPCYPar(1) << " " << cfg.GetBPDTPCYParMin(1) << " " << cfg.GetBPDTPCYParMax(1) << endl;
		//TF1* bpdtpcX_all = new TF1("bpdtpcX_all", "gaus", -0.3, 0.3);
		
		BPDTPCx.GetAllHisto()->Fit(bpdtpcX_all, "R", "Csame", -0.5, 0.35);
		BPDTPCx.GetRSTHisto()->Fit(bpdtpcX_rst, "R", "Csame", -0.5, 0.35);
		BPDTPCx.GetWSTHisto()->Fit(bpdtpcX_wst, "R", "Csame", -0.5, 0.35);
		
		BPDTPCx.GetAllHisto()->Write();
		BPDTPCx.GetRSTHisto()->Write();
		BPDTPCx.GetWSTHisto()->Write();

		BPDTPCy.GetAllHisto()->Fit(bpdtpcY_all, "R", "Csame", -0.45, 0.45);
		BPDTPCy.GetRSTHisto()->Fit(bpdtpcY_rst, "R", "Csame", -0.45, 0.45);
		BPDTPCy.GetWSTHisto()->Fit(bpdtpcY_wst, "R", "Csame", -0.45, 0.45);
				
		BPDTPCy.GetAllHisto()->Write();
		BPDTPCy.GetRSTHisto()->Write();
		BPDTPCy.GetWSTHisto()->Write();
		
		/*************************************************************************************************************************
		*	Z distribution of the p.c.a should be exponential, but because of poor extrapolation, 
		*	it is convolution of exponential function with Gaussian
		* 	Parameters: [0] = N0 - number of particles
		*				[1] = dz - size of the histo bin (should be fixed)
		*				[2] = Rho*sigma - product of the target density [cm^-3] and total cross section
		*				[3] = z0 - upstream z position of the target
		*				[4] = lambda - width of the Gaussian (uncertainty of the extrapolation)
		*************************************************************************************************************************/
		TF1* zFit_all = new TF1("zFit_all", "0.5*[0]*[1]*[2]*TMath::Exp(0.5*[2]*(2*[3] + [2]*[4]*[4] - 2*x))*TMath::Erfc(([3] + [2]*[4]*[4] - x)/(sqrt(2)*[4]))", -665., -620);
		TF1* zFit_rst = new TF1("zFit_rst", "0.5*[0]*[1]*[2]*TMath::Exp(0.5*[2]*(2*[3] + [2]*[4]*[4] - 2*x))*TMath::Erfc(([3] + [2]*[4]*[4] - x)/(sqrt(2)*[4]))", -665., -620);
		TF1* zFit_wst = new TF1("zFit_wst", "0.5*[0]*[1]*[2]*TMath::Exp(0.5*[2]*(2*[3] + [2]*[4]*[4] - 2*x))*TMath::Erfc(([3] + [2]*[4]*[4] - x)/(sqrt(2)*[4]))", -665., -620);

				
		/// Setting parameter 0
		zFit_all->SetParName(0, "N");
		zFit_all->SetParameter(0, 520);
		zFit_all->SetParLimits(0, 30, 5000);	

		zFit_rst->SetParName(0, "N");
		zFit_rst->SetParameter(0, cfg.GetZPar(0));
		zFit_rst->SetParLimits(0, cfg.GetZParMin(0), cfg.GetZParMax(0));	

		zFit_wst->SetParName(0, "N");
		zFit_wst->SetParameter(0, cfg.GetZPar(0));
		zFit_wst->SetParLimits(0, cfg.GetZParMin(0), cfg.GetZParMax(0));	
								
		/// Fixing parameter 1
		zFit_all->SetParName(1, "#Delta z");
		zFit_all->FixParameter(1, zPos.GetAllHisto()->GetBinWidth(1));
		
		zFit_rst->SetParName(1, "#Delta z");
		zFit_rst->FixParameter(1, zPos.GetAllHisto()->GetBinWidth(1));
		
		zFit_wst->SetParName(1, "#Delta z");
		zFit_wst->FixParameter(1, zPos.GetAllHisto()->GetBinWidth(1));
		
		/// Setting parameter 2
		zFit_all->SetParName(2, "#rho#sigma");
		zFit_all->SetParameter(2, cfg.GetZPar(1));
		zFit_all->SetParLimits(2, cfg.GetZParMin(1), cfg.GetZParMax(1));

		zFit_rst->SetParName(2, "#rho#sigma");
		zFit_rst->SetParameter(2, cfg.GetZPar(1));
		zFit_rst->SetParLimits(2, cfg.GetZParMin(1), cfg.GetZParMax(1));
		
		zFit_wst->SetParName(2, "#rho#sigma");
		zFit_wst->SetParameter(2, cfg.GetZPar(1));
		zFit_wst->SetParLimits(2, cfg.GetZParMin(1), cfg.GetZParMax(1));
		
		/// Setting parameter 3		
		zFit_all->SetParName(3, "z_{0}");
		zFit_all->SetParameter(3, cfg.GetZPar(2));
		zFit_all->SetParLimits(3, cfg.GetZParMin(2), cfg.GetZParMax(2));

		zFit_rst->SetParName(3, "z_{0}");
		zFit_rst->SetParameter(3, cfg.GetZPar(2));
		zFit_rst->SetParLimits(3, cfg.GetZParMin(2), cfg.GetZParMax(2));
		
		zFit_wst->SetParName(3, "z_{0}");
		zFit_wst->SetParameter(3, cfg.GetZPar(2));
		zFit_wst->SetParLimits(3, cfg.GetZParMin(2), cfg.GetZParMax(2));	
		
		/// Setting parameter 4
		zFit_all->SetParName(4, "#sigma");
		zFit_all->SetParameter(4, cfg.GetZPar(3));
		zFit_all->SetParLimits(4, cfg.GetZParMin(3), cfg.GetZParMax(3));
		
		zFit_rst->SetParName(4, "#sigma");
		zFit_rst->SetParameter(4, cfg.GetZPar(3));
		zFit_rst->SetParLimits(4, cfg.GetZParMin(3), cfg.GetZParMax(3));
		
		zFit_wst->SetParName(4, "#sigma");
		zFit_wst->SetParameter(4, cfg.GetZPar(3));
		zFit_wst->SetParLimits(4, cfg.GetZParMin(3), cfg.GetZParMax(3));
		
				
		///Fiting		
		zPos.GetAllHisto()->Fit(zFit_all, "LLR", "Csame");
		zPos.GetRSTHisto()->Fit(zFit_rst, "LLR", "Csame");
		zPos.GetWSTHisto()->Fit(zFit_wst, "LLR", "Csame");
		
		double max_all = zFit_all->GetMaximum(-665., -600);
		double max_allX = zFit_all->GetMaximumX(-665., -600);
		TF1 *zero_all = new TF1("zero_all", "zFit_all - [0]", -665., -600);
		zero_all->SetParameter(0, max_all/2);
		double zTarg_all = zero_all->GetX(0, max_allX-20, max_allX);
		
		double max_rst = zFit_rst->GetMaximum(-665., -600);
		double max_rstX = zFit_rst->GetMaximumX(-665., -600);
		TF1 *zero_rst = new TF1("zero_rst", "zFit_rst - [0]", -665., -600);
		zero_rst->SetParameter(0, max_rst/2);
		double zTarg_rst = zero_rst->GetX(0, max_rstX-20, max_rstX);
		
		double max_wst = zFit_wst->GetMaximum(-665., -600);
		double max_wstX = zFit_wst->GetMaximumX(-665., -600);
		TF1 *zero_wst = new TF1("zero_wst", "zFit_wst - [0]", -665., -600);
		zero_wst->SetParameter(0, max_wst/2);
		double zTarg_wst = zero_wst->GetX(0, max_wstX-20, max_wstX);
		
		cout << zFit_all->GetParameter(3) << "+-" << zFit_all->GetParError(3) << endl;
		cout << zFit_rst->GetParameter(3) << "+-" << zFit_rst->GetParError(3) << endl;
		cout << zFit_wst->GetParameter(3) << "+-" << zFit_wst->GetParError(3) << endl;
		cout << zTarg_all << " " << zTarg_rst << " " << zTarg_wst << endl;
		//cout << zFit_all->GetParameter(2) << " " << zFit_rst->GetParameter(2) << " " << zFit_wst->GetParameter(2) << endl;
		zPos.GetAllHisto()->Write();
		zPos.GetRSTHisto()->Write();
		zPos.GetWSTHisto()->Write();
	}	
	output.Close();
	
	
}


