#include <stdlib.h>
#include "TargetPositionConfig.h"
#include <fstream>
#include <fwk/CentralConfig.h>
#include <utl/Branch.h>
#include <utl/BranchIterator.h>
#include "CConfig.h"

using namespace std;
using namespace fwk;
using namespace utl; 

void TargetPositionConfig::Read(){


	/*if(cConfig == NULL){
		cout << __FUNCTION__ << ": Central config not initialized! Exiting..." << endl;
		exit (EXIT_FAILURE);
	}*/
	Branch dataAnaConf = cConfig.GetTopBranch("TargetPosition");

	if(dataAnaConf.GetName() != "TargetPosition"){
		cout << __FUNCTION__ << ": Wrong TargetPosition config file! Top branch name doesn't match! Please check your configuration files! Exiting..." << endl;
		exit (EXIT_FAILURE);
	}

	for(int i = 0; i < 3; ++i){
		fZPars.push_back(0);
		fZParMin.push_back(0);
		fZParMax.push_back(0);
		
		fBPDTPCXPars.push_back(0);
		fBPDTPCXParMin.push_back(0);
		fBPDTPCXParMax.push_back(0);
		fBPDTPCYPars.push_back(0);
		fBPDTPCYParMin.push_back(0);
		fBPDTPCYParMax.push_back(0);
	}
	
	fAlignmentSlice.push_back(0);
	fAlignmentSlice.push_back(0);
	
	for(BranchIterator it = dataAnaConf.ChildrenBegin(); it != dataAnaConf.ChildrenEnd(); ++it){
		Branch child = *it;

		if(child.GetName() == "output"){
			child.GetData(fOutputDir);

		}

		else if(child.GetName() == "plotSlices"){
			child.GetData(fPlotSlices);
		}
		
		else if(child.GetName() == "calculateAlignment"){
			child.GetData(fCalculateAlignment);
		}

		else if(child.GetName() == "BPDTPCAlignment"){
			child.GetData(fBPDTPCAlignment);
		}
		
		else if(child.GetName() == "zDistributions"){
			child.GetData(fZDistributions);
		}

		else if(child.GetName() == "beam"){
			child.GetData(fBeam);
		}
		
		else if(child.GetName() == "sliceStep"){
			child.GetData(fStep);
		}

		else if(child.GetName() == "sliceRadius"){
			child.GetData(fRadius);
		}
		
		else if(child.GetName() == "alignmentSlice"){
			fAlignmentSlice.clear();
			child.GetData(fAlignmentSlice);
		}	
			
		else if(child.GetName() == "zParams"){

			
			for(BranchIterator t = child.ChildrenBegin(); t != child.ChildrenEnd(); ++t){
				Branch targetChild = *t;
				
				if(targetChild.GetName() == "values"){
					fZPars.clear();
					targetChild.GetData(fZPars);
				}
				else if(targetChild.GetName() == "min"){
					fZParMin.clear();
					targetChild.GetData(fZParMin);
				}
				else if(targetChild.GetName() == "max"){
					fZParMax.clear();
					targetChild.GetData(fZParMax);
				}
			}

		}		

		else if(child.GetName() == "bpdtpcXParams"){

			
			for(BranchIterator t = child.ChildrenBegin(); t != child.ChildrenEnd(); ++t){
				Branch targetChild = *t;
				
				if(targetChild.GetName() == "values"){
					fBPDTPCXPars.clear();
					targetChild.GetData(fBPDTPCXPars);
				}
				else if(targetChild.GetName() == "min"){
					fBPDTPCXParMin.clear();
					targetChild.GetData(fBPDTPCXParMin);
				}
				else if(targetChild.GetName() == "max"){
					fBPDTPCXParMax.clear();
					targetChild.GetData(fBPDTPCXParMax);
				}
			}

		}	
		
		else if(child.GetName() == "bpdtpcYParams"){

			
			for(BranchIterator t = child.ChildrenBegin(); t != child.ChildrenEnd(); ++t){
				Branch targetChild = *t;
				
				if(targetChild.GetName() == "values"){
					fBPDTPCYPars.clear();
					targetChild.GetData(fBPDTPCYPars);
				}
				else if(targetChild.GetName() == "min"){
					fBPDTPCYParMin.clear();
					targetChild.GetData(fBPDTPCYParMin);
				}
				else if(targetChild.GetName() == "max"){
					fBPDTPCYParMax.clear();
					targetChild.GetData(fBPDTPCYParMax);
				}
			}

		}				
	}
	
}


bool TargetPositionConfig::IsBeamGood(double ax, double bx, double ay, double by){

	if(fBeam.size() != 5){
		cerr << "ERROR: " << __FUNCTION__ << ": Not all beam data is provided!" << endl;
		exit(EXIT_FAILURE); 
	}
	double x = ax*fBeam.at(2) + bx;
	double y = ay*fBeam.at(2) + by;
	
	double ok = false;
	
	double r = sqrt((x-fBeam.at(0))*(x-fBeam.at(0)) + (y-fBeam.at(1))*(y-fBeam.at(1)));
	if(fBeam.at(3) < r && fBeam.at(4) > r) ok = true;
	
	return ok;
}


void TargetPositionConfig::SetZPar(int i, double value){
	
	if(i < 0 || i > 3){
		cerr << "WARNING: " << __FUNCTION__ << ": parameter number should be between 0 and 3!" << endl;
		cerr << "Parameter value not set!" << endl;
		
	}
	else{
		fZPars.at(i) = value;
	}

}


double TargetPositionConfig::GetZPar(int i){
	
	if(i < 0 || i > 3){
		cerr << "WARNING: " << __FUNCTION__ << ": parameter number should be between 0 and 3!" << endl;
		cerr << "Zero value returned!" << endl;
		return 0;
	}
	else{
		return fZPars.at(i);
	}

}

void TargetPositionConfig::SetZParMin(int i, double value){
	
	if(i < 0 || i > 3){
		cerr << "WARNING: " << __FUNCTION__ << ": parameter number should be between 0 and 3!" << endl;
		cerr << "Parameter minimum value not set!" << endl;
		
	}
	else{
		fZParMin.at(i) = value;
	}

}

double TargetPositionConfig::GetZParMin(int i){
	
	if(i < 0 || i > 3){
		cerr << "WARNING: " << __FUNCTION__ << ": parameter number should be between 0 and 3!" << endl;
		cerr << "Zero value returned!" << endl;
		return 0;
	}
	else{
		return fZParMin.at(i);
	}

}

void TargetPositionConfig::SetZParMax(int i, double value){
	
	if(i < 0 || i > 3){
		cerr << "WARNING: " << __FUNCTION__ << ": parameter number should be between 0 and 3!" << endl;
		cerr << "Parameter maximum value not set!" << endl;
		
	}
	else{
		fZParMax.at(i) = value;
	}

}

double TargetPositionConfig::GetZParMax(int i){
	
	if(i < 0 || i > 3){
		cerr << "WARNING: " << __FUNCTION__ << ": parameter number should be between 0 and 3!" << endl;
		cerr << "Zero value returned!" << endl;
		return 0;
	}
	else{
		return fZParMax.at(i);
	}

}



void TargetPositionConfig::SetBPDTPCXPar(int i, double value){
	
	if(i < 0 || i > 4){
		cerr << "WARNING: " << __FUNCTION__ << ": parameter number should be between 0 and 4!" << endl;
		cerr << "Parameter value not set!" << endl;
		
	}
	else{
		fBPDTPCXPars.at(i) = value;
	}

}


double TargetPositionConfig::GetBPDTPCXPar(int i){
	
	if(i < 0 || i > 4){
		cerr << "WARNING: " << __FUNCTION__ << ": parameter number should be between 0 and 4!" << endl;
		cerr << "Zero value returned!" << endl;
		return 0;
	}
	else{
		return fBPDTPCXPars.at(i);
	}

}

void TargetPositionConfig::SetBPDTPCXParMin(int i, double value){
	
	if(i < 0 || i > 4){
		cerr << "WARNING: " << __FUNCTION__ << ": parameter number should be between 0 and 4!" << endl;
		cerr << "Parameter minimum value not set!" << endl;
		
	}
	else{
		fBPDTPCXParMin.at(i) = value;
	}

}

double TargetPositionConfig::GetBPDTPCXParMin(int i){
	
	if(i < 0 || i > 4){
		cerr << "WARNING: " << __FUNCTION__ << ": parameter number should be between 0 and 4!" << endl;
		cerr << "Zero value returned!" << endl;
		return 0;
	}
	else{
		return fBPDTPCXParMin.at(i);
	}

}

void TargetPositionConfig::SetBPDTPCXParMax(int i, double value){
	
	if(i < 0 || i > 4){
		cerr << "WARNING: " << __FUNCTION__ << ": parameter number should be between 0 and 4!" << endl;
		cerr << "Parameter maximum value not set!" << endl;
		
	}
	else{
		fBPDTPCXParMax.at(i) = value;
	}

}

double TargetPositionConfig::GetBPDTPCXParMax(int i){
	
	if(i < 0 || i > 4){
		cerr << "WARNING: " << __FUNCTION__ << ": parameter number should be between 0 and 4!" << endl;
		cerr << "Zero value returned!" << endl;
		return 0;
	}
	else{
		return fBPDTPCXParMax.at(i);
	}

}



void TargetPositionConfig::SetBPDTPCYPar(int i, double value){
	
	if(i < 0 || i > 4){
		cerr << "WARNING: " << __FUNCTION__ << ": parameter number should be between 0 and 4!" << endl;
		cerr << "Parameter value not set!" << endl;
		
	}
	else{
		fBPDTPCYPars.at(i) = value;
	}

}


double TargetPositionConfig::GetBPDTPCYPar(int i){
	
	if(i < 0 || i > 4){
		cerr << "WARNING: " << __FUNCTION__ << ": parameter number should be between 0 and 4!" << endl;
		cerr << "Zero value returned!" << endl;
		return 0;
	}
	else{
		return fBPDTPCYPars.at(i);
	}

}

void TargetPositionConfig::SetBPDTPCYParMin(int i, double value){
	
	if(i < 0 || i > 4){
		cerr << "WARNING: " << __FUNCTION__ << ": parameter number should be between 0 and 4!" << endl;
		cerr << "Parameter minimum value not set!" << endl;
		
	}
	else{
		fBPDTPCYParMin.at(i) = value;
	}

}

double TargetPositionConfig::GetBPDTPCYParMin(int i){
	
	if(i < 0 || i > 4){
		cerr << "WARNING: " << __FUNCTION__ << ": parameter number should be between 0 and 4!" << endl;
		cerr << "Zero value returned!" << endl;
		return 0;
	}
	else{
		return fBPDTPCYParMin.at(i);
	}

}

void TargetPositionConfig::SetBPDTPCYParMax(int i, double value){
	
	if(i < 0 || i > 4){
		cerr << "WARNING: " << __FUNCTION__ << ": parameter number should be between 0 and 4!" << endl;
		cerr << "Parameter maximum value not set!" << endl;
		
	}
	else{
		fBPDTPCYParMax.at(i) = value;
	}

}

double TargetPositionConfig::GetBPDTPCYParMax(int i){
	
	if(i < 0 || i > 4){
		cerr << "WARNING: " << __FUNCTION__ << ": parameter number should be between 0 and 4!" << endl;
		cerr << "Zero value returned!" << endl;
		return 0;
	}
	else{
		return fBPDTPCYParMax.at(i);
	}

}
//*********************************************************************************************
string TargetPositionConfig::GetTimeString(){
	time_t rawtime;
	struct tm* timeinfo;

	time(&rawtime);
	timeinfo = localtime(&rawtime);

	string res;
	stringstream s;

	s << timeinfo->tm_mon + 1;
	s << "_";
	s << timeinfo->tm_mday;
	s << "_";
	s << timeinfo->tm_year + 1900;
	s << "_";
	s << timeinfo->tm_hour;
	s << "_";
	s << timeinfo->tm_min;
	s << "_";
	s << timeinfo->tm_sec;

	s >> res;

	return res;
}


