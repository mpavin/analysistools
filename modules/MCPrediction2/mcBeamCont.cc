#include <vector>
#include <string>
#include <iostream>
#include "Functions.h"
#include "Constants.h"
#include <TH1D.h>
#include <TH2D.h>
#include <TFile.h>
#include <TChain.h>
#include <TTree.h>
#include <TMath.h>
#include <math.h>

using namespace std;


int main(int argc, char* argv[]){

	string input;
	string output;
	string opt = argv[1];
	
	if(opt == "0"){
		cout << "FLUKA 2011.2c.5" << endl;
		input = "list.txt";
		output = "FLUKA.root";
	}
	else if(opt == "1"){
		cout << "NuBeam" << endl;
		input = "list1.txt";
		targX = 0.1153;
		output = "NuBeam.root";
	}
	else if(opt == "2"){
		cout << "QGSP_BERT" << endl;
		input = "list2.txt";
		output = "QGSPBert.root";
		targX = 0.1153;
	}
	
	TChain chain("h1000");
	

	ifstream ifs;
	string line;
	ifs.open(input.c_str());
		
	if (!ifs.is_open()){
		cerr << "[ReadInputListFile] Could not open infile. Exiting..." << endl;			
		exit (101);
	}
    
	while (getline(ifs, line)){
		if(line.at(0)=='#')
			continue;
		chain.Add(line.c_str());
	}
	ifs.close();		

	Int_t           protnum;
  	Int_t           ipart;
  	Float_t         mom;
  	Float_t         pos[3];
  	Float_t         vec[3];

  // Declaration of leaf types for replica input
  	Int_t           pgen;
  	Int_t           ng;
  	Float_t         gpx[20];    //[ng]
  	Float_t         gpy[20];    //[ng]
  	Float_t         gpz[20];    //[ng]
  	Float_t         gvx[20];    //[ng]
  	Float_t         gvy[20];    //[ng]
  	Float_t         gvz[20];    //[ng]
  	Int_t           gpid[20];   //[ng]
  	Int_t           gmec[20];   //[ng]

	TBranch        *b_protnum;   //!
  	TBranch        *b_ipart;     //!
  	TBranch        *b_mom;       //!
  	TBranch        *b_pos;       //!
  	TBranch        *b_vec;       //!
  	TBranch        *b_pgen;      //!
  	TBranch        *b_ng;        //!
  	TBranch        *b_gpx;       //!
  	TBranch        *b_gpy;       //!
  	TBranch        *b_gpz;       //!
  	TBranch        *b_gvx;       //!
  	TBranch        *b_gvy;       //!
  	TBranch        *b_gvz;       //!
  	TBranch        *b_gpid;      //!
  	TBranch        *b_gmec;      //!
    
	chain.SetBranchAddress("protnum", &protnum, &b_protnum);
  	chain.SetBranchAddress("ipart", &ipart, &b_ipart);
  	chain.SetBranchAddress("mom", &mom, &b_mom);
  	chain.SetBranchAddress("pos", pos, &b_pos);
  	chain.SetBranchAddress("vec", vec, &b_vec);
  	chain.SetBranchAddress("pgen", &pgen, &b_pgen);
  	chain.SetBranchAddress("ng", &ng, &b_ng);
  	chain.SetBranchAddress("gpx", gpx, &b_gpx);
  	chain.SetBranchAddress("gpy", gpy, &b_gpy);
  	chain.SetBranchAddress("gpz", gpz, &b_gpz);
  	chain.SetBranchAddress("gvx", gvx, &b_gvx);
  	chain.SetBranchAddress("gvy", gvy, &b_gvy);
  	chain.SetBranchAddress("gvz", gvz, &b_gvz);
  	chain.SetBranchAddress("gpid", gpid, &b_gpid);
  	chain.SetBranchAddress("gmec", gmec, &b_gmec);
  	
  	
  	int nEntries = chain.GetEntries();

  	vector<vector<TH1D> > mcpredPiPos;
  	vector<vector<TH1D> > mcpredPiNeg;
  	for(int i = 0; i < nz; i++){
  	
  		vector<TH1D> tmpPos;
  		vector<TH1D> tmpNeg;
  		for(int j = 0; j < nth[i]; j++){
	  		string name1 = "mcPos_z" + ConvertToString(i+1.,0) + "_th" + ConvertToString(j+1.,0);
	  		string name2 = "mcNeg_z" + ConvertToString(i+1.,0) + "_th" + ConvertToString(j+1.,0);

	  		TH1D temp1(name1.c_str(), "", np[i][j], 0, pbord[i][j]);
	  		TH1D temp2(name2.c_str(), "", np[i][j], 0, pbord[i][j]);
	  		
	  		tmpPos.push_back(temp1);
	  		tmpNeg.push_back(temp2);
  		}
  		
  		mcpredPiPos.push_back(tmpPos);
  		mcpredPiNeg.push_back(tmpNeg);
  	}

  	int eventId = -1;
  	int totev = 0;

	int bBinId = -99;

	//nEntries = 10000000;

	
	
	double sc = 1;
  	for(int i = 0; i < nEntries; i++){
  		
  		chain.GetEntry(i);
		if(sqrt((gvx[0]-targX)*(gvx[0]-targX) + (gvy[0]-targY)*(gvy[0]-targY))>1.30) continue;
		

		if(protnum != eventId){
				
			eventId = protnum;
			totev++;
  			if(!(totev%100000))
  				cout << "Processing event # " << totev << endl;
	
		}
		
		
		if(ipart !=8 && ipart != 9) continue;
		
		if(sqrt((pos[0]-targX)*(pos[0]-targX) + (pos[1]-targY)*(pos[1]-targY))>1.32) continue;
		
		int zbin = -99;
		for(int j = 0; j < nz; j++){
			if(zbord[j] <= pos[2] && pos[2] < zbord[j+1]){
				zbin = j;
				break;
			} 
		}
		if(zbin < 0) continue;


		double theta = 1000*acos(vec[2]);
		int thbin = -99;
		for(int j = 0; j < nth[zbin]; j++){
			if(thbord[zbin][j] <= theta && theta < thbord[zbin][j+1]){
				thbin = j;
				break;
			} 
		}
		if(thbin < 0) continue;
				
		if(ipart ==8){
			mcpredPiPos.at(zbin).at(thbin).Fill(mom);
		}
		else if(ipart == 9){
			mcpredPiNeg.at(zbin).at(thbin).Fill(mom);
		}

  	}
  	
  	cout << totev << endl;
  	TFile out(output.c_str(), "RECREATE");
  	out.cd();
  	
  	double tev = totev;
  	for(int i = 0; i < nz; i++){
  		for(int j = 0; j < nth[i]; j++){
  			double scale = np[i][j]/pbord[i][j];
  			scale *= 1000/(thbord[i][j+1]-thbord[i][j]);
  			scale *= 1/tev;
  			
	  		mcpredPiPos.at(i).at(j).Scale(scale);
	  		mcpredPiNeg.at(i).at(j).Scale(scale);
	  		mcpredPiPos.at(i).at(j).Write();
	  		mcpredPiNeg.at(i).at(j).Write();
  		}
  	}

  	out.Close();
}





