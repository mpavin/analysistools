#include <vector>
#include <string>
#include <iostream>
#include "Functions.h"
#include <TH1D.h>
#include <TH2D.h>
#include <TFile.h>
#include <TChain.h>
#include <TTree.h>
#include <TMath.h>
#include <math.h>

using namespace std;


int main(){

	int data = 1;
	double scale;
	string inputFile;
	string add;
	
	if(data==1){
		scale = 1;
		inputFile = "list1.txt";
		add = "data_";	
	}
	else{
		//scale = 0.13870344;
		scale = 0.107088869;
		inputFile = "list2.txt";
		add = "mc_";			
	}
	TChain chain("Data");
	
	string outName = add + "phase.root";
	TFile outFile(outName.c_str(), "RECREATE");
	ifstream ifs;
	string line;
	ifs.open(inputFile.c_str());
		
	if (!ifs.is_open()){
		cerr << "[ReadInputListFile] Could not open infile. Exiting..." << endl;			
		exit (101);
	}
    
	while (getline(ifs, line)){
		if(line.at(0)=='#')
			continue;
		chain.Add(line.c_str());
	}
	ifs.close();		



	BeamA beamD;
	vector<double> *x = NULL;
	vector<double> *xE = NULL;
	vector<double> *y = NULL;
	vector<double> *yE = NULL;
	vector<double> *z = NULL;
	vector<double> *px = NULL;
	vector<double> *py = NULL;
	vector<double> *pz = NULL;
	vector<double> *distTarg = NULL;
	vector<double> *zStart = NULL;
	vector<double> *dEdx = NULL;
	vector<int> *qD = NULL;
	vector<int> *nVTPC1 = NULL;
	vector<int> *nVTPC2 = NULL;
	vector<int> *nGTPC = NULL;
	vector<int> *nMTPC = NULL;
	vector<int> *topology = NULL;
	vector<int> *nTOF = NULL;
	vector<double> *m2TOF = NULL;	
	vector<int> *tofScId = NULL;
	vector<int> *pdgId = NULL;
	int runNumber;
	
	TBranch *b_aData = chain.GetBranch("Beam");
 	b_aData->SetAddress(&beamD);
	chain.SetBranchAddress("x", &x);
	chain.SetBranchAddress("xE", &xE);
	chain.SetBranchAddress("y", &y);
	chain.SetBranchAddress("yE", &yE);
	chain.SetBranchAddress("z", &z);
	chain.SetBranchAddress("px", &px);
	chain.SetBranchAddress("py", &py);
	chain.SetBranchAddress("pz", &pz);
	chain.SetBranchAddress("distTarg", &distTarg);
	chain.SetBranchAddress("zStart", &zStart);
	chain.SetBranchAddress("dEdx", &dEdx);
	chain.SetBranchAddress("q", &qD);
	chain.SetBranchAddress("nVTPC1", &nVTPC1);
	chain.SetBranchAddress("nVTPC2", &nVTPC2);
	chain.SetBranchAddress("nGTPC", &nGTPC);
	chain.SetBranchAddress("nMTPC", &nMTPC);
	chain.SetBranchAddress("topology", &topology);
	chain.SetBranchAddress("nTOF", &nTOF);
	chain.SetBranchAddress("m2TOF", &m2TOF);
	chain.SetBranchAddress("tofScId", &tofScId);
	chain.SetBranchAddress("runNumber", &runNumber);
	//chain.SetBranchAddress("pdgId", &pdgId);
	
	int nEntries = chain.GetEntries();
	
	double zbor[7] = {0, 18, 36, 54, 72, 89.99, 90.01};
	vector<TH2D> zph;

	for(int i = 0; i < 6; i++){
	  string nm = add + "zbin" + ConvertToString(i+1., 0);
	  TH2D temp(nm.c_str(), "", 40, 0, 400, 80, 0, 80);
	  temp.GetXaxis()->SetTitle("#theta [mrad]");
	  temp.GetXaxis()->SetTitleOffset(0.9);
	  temp.GetXaxis()->SetTitleSize(0.06);
	  temp.GetXaxis()->SetLabelSize(0.06);
	  temp.GetYaxis()->SetTitle("Scintillator ID");
          temp.GetYaxis()->SetTitleOffset(0.9);
          temp.GetYaxis()->SetTitleSize(0.06);
          temp.GetYaxis()->SetLabelSize(0.06);
	  temp.SetStats(kFALSE);
	  string title = ConvertToString(zbor[i],0) + " #leq z < " + ConvertToString(zbor[i+1],0) + " cm";
	  temp.SetTitle(title.c_str());
	  zph.push_back(temp);
	}

	
	TH1D scints("scints", "", 80, 0, 80);
	TH1D topsRST("topsRST", "", 16, 0, 16);
	TH1D topsWST("topsWST", "", 16, 0, 16);
	TH1D mgtop("mgtop", "", 2, 0, 2);
	TH2D topsp("topsp", "", 40, 0, 20, 15, 1, 16);
	TH1D phih("phi", "", 90, -90, 270);
	TH1D vtpc("vtpc", "", 70, 0, 70);
	TH1D mtpc("mtpc", "", 90, 0, 90);
	
	
	cout << nEntries << endl;
	for(int i = 0; i < nEntries; ++i){
		
		chain.GetEntry(i,1);
		
		if(!((i+1)%100000))
			cout << "Processing event # "  << i+1 << endl;
		
		//if(nCount > nTrack) break;	
		for(int j = 0; j < px->size(); j++){

			//if(topology->at(j) < 9) continue;
			/*if(tofScId->at(j) == 8) continue;
			if(tofScId->at(j) == 40) continue;
			if(tofScId->at(j) == 47) continue;
			if(tofScId->at(j) == 57) continue;
			if(tofScId->at(j) == 79) continue;*/
			
			/*if(topology->at(j) == 2) continue;
			if(topology->at(j) == 10) continue;
			
			if(topology->at(j) == 1){
				if(nVTPC1->at(j) < 20) continue;
			}
			if(topology->at(j) == 3){
				if(nVTPC1->at(j)+nVTPC2->at(j) < 20) continue;
			}
			if(topology->at(j) == 5){
				if(nVTPC1->at(j) < 20) continue;
			}
			if(topology->at(j) == 6){
				if(nVTPC1->at(j) < 20) continue;
			}*/
			
			if(nTOF->at(j) == 0) continue;
			if(qD->at(j)>0) continue;
			double theta = 1000*acos(pz->at(j)/sqrt(px->at(j)*px->at(j)+py->at(j)*py->at(j)+pz->at(j)*pz->at(j)));
			
			if(theta < 40) continue;
			if(theta >= 60) continue;
			double p = sqrt(px->at(j)*px->at(j) + py->at(j)*py->at(j) + pz->at(j)*pz->at(j));
			
			double zb = z->at(j) + 657.51;

			int zId = -99;
			for(int k = 0; k < 6; k++){
				if(zb >= zbor[k] && zb <= zbor[k+1]){
					zId = k;
					break;
				}
			}
			if(zId < 0) continue;
			
			zph.at(zId).Fill(theta, tofScId->at(j)-0.5);
			
			double mom = sqrt(px->at(j)*px->at(j)+py->at(j)*py->at(j)+pz->at(j)*pz->at(j));
			//if(mom < 7) continue;
			if(zId != 0) continue;
			
			double phi = acos(px->at(j)/sqrt(px->at(j)*px->at(j)+py->at(j)*py->at(j)))*180/TMath::Pi();
			if(px->at(j)>0 && py->at(j) < 0){
				phi = -1*phi;
			}
			else if(px->at(j)<0 && py->at(j) < 0){
				phi = 360 - phi;
			}
			scints.Fill(tofScId->at(j));
			topsp.Fill(p, topology->at(j));
			
			phih.Fill(phi);
			vtpc.Fill(nVTPC1->at(j)+nVTPC2->at(j));
			mtpc.Fill(nMTPC->at(j));
			if(px->at(j)*qD->at(j) < 0){
				topsWST.Fill(topology->at(j));
				mgtop.Fill(0);
			}
			else{ 
				mgtop.Fill(1);
				topsRST.Fill(topology->at(j));
			}
			
		}

	}
	//cout << nCount << " " << nEntries << endl;
	outFile.cd();


	for(int i = 0; i < 6; i++){
		
	  zph.at(i).Write();
	}
	
	if(data == 0){
		topsRST.SetLineColor(kRed);
		topsWST.SetLineColor(kRed);
		scints.SetLineColor(kRed);
		mgtop.SetLineColor(kRed);
		phih.SetLineColor(kRed);
		vtpc.SetLineColor(kRed);
		mtpc.SetLineColor(kRed);
		
		topsRST.Scale(scale);
		topsWST.Scale(scale);
		scints.Scale(scale);
		mgtop.Scale(scale);
		phih.Scale(scale);
		vtpc.Scale(scale);
		mtpc.Scale(scale);
	}
	topsp.Write();
	scints.Write();
	topsRST.Write();
	topsWST.Write();
	mgtop.Write();
	phih.Write();
	vtpc.Write();
	mtpc.Write();
	outFile.Close();
		
}





