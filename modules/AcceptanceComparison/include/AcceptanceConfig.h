#ifndef ACCEPTANCECONFIG_H
#define ACCEPTANCECONFIG_H
#include <iostream>
#include <vector>
#include <string>

#include "TargetAna.h"



using namespace std;

class AcceptanceConfig{

	public:
		AcceptanceConfig(): fTrackExtrapolationOn(true)  {Read();}  //Constructor

		//*************************Get functions***********************************
		TargetAna& GetTarget(){return fTarget;} //Returns target.

		string GetOutputDir(){return fOutputDir;}

		string GetInputListFile(){return fInputData;}
		vector<double>& GetTargetZPositions(){return fTargetZ;}
		bool TrackExtrapolationOn(){return fTrackExtrapolationOn;}

		vector<int>& GetRunNumbers(){return fRunNumbers;}

		//*************************Set functions***********************************
		void Read(); //Reads main config file.

		void SetTarget(string name, ETargetType type, double length, double radius, double positionX, double positionY, double positionZ, double tilt1, double tilt2); //Sets target's parameters.

		void SetTarget(TargetAna target){fTarget = target;} //Sets target's parameters.


		void SetInputDataFile(string inputData){fInputData = inputData;}
		void SetOutputDir(string outputDir){fOutputDir = outputDir;}

		void SetTrackExtrapolationOn(bool val){fTrackExtrapolationOn = val;}
		void SetRunNumbers(vector<int> fRunNumbers);
		void SetTargetZPositions(vector<double> pos){fTargetZ = pos;}

		vector<string>& GetJob();
		void ReadInputList();
		void ReadInputList(string inputData);

		

		bool SkipRun(int runNumber);
	private:


		TargetAna fTarget; //Target.

		
		string fInputData;
		string fOutputDir;

		vector<string> fJob;
		vector<double> fTargetZ;
		bool fTrackExtrapolationOn;

		vector<int> fRunNumbers;

};

#endif
