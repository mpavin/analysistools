#ifndef ACCEPTANCE_H
#define ACCEPTANCE_H

#include <vector>
#include <string>

#include "EventSelection.h"
#include "TrackSelection.h"

#include "AcceptanceConfig.h"
#include "TreeDataTypes.h"
#include <TTree.h>

using namespace std;

class Acceptance {

	public:

		Acceptance (unsigned int max): fMaxEv(max)  {}

		void SetMainConfigFile(string file){fMainConfigFile = file;}
		void SetMaxNumEvents(unsigned int num){fMaxEv = num;}

		void Run();
		void WriteData();
		

		

	private:


		string fMainConfigFile;

		
		unsigned int fMaxEv;
		AcceptanceConfig fAcceptanceConfig;
	

};

#endif
