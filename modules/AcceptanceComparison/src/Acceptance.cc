#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <cstdlib>
#include <stdexcept>
#include <TTree.h>
#include <TFile.h>
#include <TDirectory.h>
#include <TH1D.h>
#include <cstdlib>
#include <evt/Event.h>
#include <evt/EventHeader.h>
#include <io/EventFileChain.h>
#include <det/TriggerConst.h>
#include <det/BPDConst.h>
#include <evt/RawEvent.h>

#include <boost/format.hpp>
#include "Acceptance.h"
#include "TrackExtrapolation.h"
#include "Functions.h"
#include "PhaseSpace.h"
#include "PlotHisto.h"
#include "StrTools.h"
#include "TrackTools.h"

using namespace std;
using namespace io;
using namespace evt;
using namespace evt::rec;
using namespace det;
using namespace boost;
using namespace TrackTools;
//using namespace StrTools;

void Acceptance::Run(){
	
	//string outputFileName = fAcceptanceConfig.GetOutputDir() + "acceptance_" + StrTools::GetTimeString() + ".root";
	string outputFileName = fAcceptanceConfig.GetOutputDir() + "/acceptance.root";

	vector<string>&  jobs = fAcceptanceConfig.GetJob();
	MCData mcData;
	
	TFile* outputFile = new TFile(outputFileName.c_str(), "RECREATE");
	outputFile->cd();
	TTree *tree = new TTree("MCData", "MCData");
	tree->SetAutoSave(-300000000);
	
	
	double pExtrap[3] = {0,0,0};
	double posExtrap[3] = {0,0,0};
	int qRec;
	int nTPC[4] = {0,0,0,0};
	double dEdx;
	int topology;
	double distTarg;
	int nToF = 0;
	double m2ToF[3] = {0,0,0};
	int scId[3] = {0,0,0};
	double xE;
	double yE;
	double sigmaP;
	double zLast;
	double zFirst;
	
	int nSim = 0;
	double pxSim[10] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
	double pySim[10] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
	double pzSim[10] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
	double xSim[10] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
	double ySim[10] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
	double zSim[10] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
	int pdgId[10] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
	int qSim[10] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
	int nUnique[10] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
	int nCommon[10] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
	
	tree->Branch("Beam", &(mcData.beam.aX), "aX/D:bX:aY:bY");
	
	tree->Branch("pExtrap",pExtrap,"pExtrap[3]/D");
	tree->Branch("posExtrap", posExtrap, "posExtrap[3]/D");
	tree->Branch("qRec", &qRec, "qRec/I");
	tree->Branch("nTPC", nTPC, "nTPC[4]/I");
	tree->Branch("dEdx", &dEdx, "dEdx/D");
	tree->Branch("topology", &topology, "topology/I");
	tree->Branch("distTarg", &distTarg, "distTarg/D");
	tree->Branch("nToF", &nToF, "nToF/I");
	tree->Branch("m2ToF", m2ToF, "m2ToF[nToF]/D");
	tree->Branch("scId", scId, "scId[nToF]/I");
	tree->Branch("zFirst", &zFirst, "zFirst/D");
	tree->Branch("zLast", &zLast, "zLast/D");
	tree->Branch("xE", &xE, "xE/D");
	tree->Branch("yE", &yE, "yE/D");
	tree->Branch("sigmaP", &sigmaP, "sigmaP/D");
		
	tree->Branch("nSim", &nSim, "nSim/I");
	tree->Branch("pxSim",pxSim,"pxSim[nSim]/D");
	tree->Branch("pySim",pySim,"pySim[nSim]/D");
	tree->Branch("pzSim",pzSim,"pzSim[nSim]/D");
	tree->Branch("xSim", xSim, "xSim[nSim]/D");
	tree->Branch("ySim", ySim, "ySim[nSim]/D");
	tree->Branch("zSim", zSim, "zSim[nSim]/D");
	tree->Branch("pdgId", pdgId, "pdgId[nSim]/I");
	tree->Branch("qSim", qSim, "qSim[nSim]/I");
	tree->Branch("nUnique", nUnique, "nUnique[nSim]/I");
	tree->Branch("nCommon", nCommon, "nCommon[nSim]/I");
	

	TrackSelection trSel;
	TrackExtrapolation trackExtrapolation(true, 0.1, 30400.);
	trackExtrapolation.SetTarget(fAcceptanceConfig.GetTarget());	
	//AnaTrack anaTrack(trackExtrapolation);
	
	

	EventFileChain eventFileChain(jobs);
	Event event;
	int nEvents = 0;
	
	int runNumber = 0;
	while (eventFileChain.Read(event) == eSuccess && nEvents < fMaxEv) {

		bool skipRun = false;
		EventHeader &evHeader = event.GetEventHeader();

		if(evHeader.GetRunNumber() != runNumber){
			runNumber = evHeader.GetRunNumber();
			skipRun = fAcceptanceConfig.SkipRun(runNumber);
		}

		if(skipRun) continue;

   		++nEvents;
    
    	if (!(nEvents%10000))
      		cout << " --->Processing event # " << nEvents << endl;



		const SimEvent& simEvent = event.GetSimEvent();
		const RecEvent& recEvent = event.GetRecEvent();
	
		const sim::Beam &beam = simEvent.GetBeam();
		mcData.beam.aX = beam.GetMomentum().GetX()/beam.GetMomentum().GetZ();
		mcData.beam.bX = beam.GetOrigin().GetX() - mcData.beam.aX*beam.GetOrigin().GetZ();
		mcData.beam.aY = beam.GetMomentum().GetY()/beam.GetMomentum().GetZ();
		mcData.beam.bY = beam.GetOrigin().GetY() - mcData.beam.aY*beam.GetOrigin().GetZ();
		



		for (std::list<rec::Track>::const_iterator iter = recEvent.Begin<rec::Track>();
           iter != recEvent.End<rec::Track>(); ++iter)
      	{
			
			nToF = 0;
			nSim = 0;
			const Track& track = *iter;

			bool hasToF = false;

			AnaTrack anaTrack(trackExtrapolation);

			if(track.GetNumberOfVertexTracks()< 1) 
				continue;
			
			
			
			anaTrack.SetTrack(track);

			for(evt::rec::VertexTrackIndexIterator vtxIter = track.VertexTracksBegin(); vtxIter != track.VertexTracksEnd(); vtxIter++){		
				const evt::rec::VertexTrack vtxRecTr = recEvent.Get(*vtxIter);
					
				for (rec::TOFMassIndexIterator tofIter = vtxRecTr.TOFMassBegin();
				   		tofIter != vtxRecTr.TOFMassEnd(); ++tofIter)
			  	{
					const rec::TOFMass& tof = recEvent.Get(*tofIter);
					if(tof.GetTOFId() != TOFConst::eTOFF)
						continue;
					
					m2ToF[nToF] = tof.GetSquaredMass();
					scId[nToF] = tof.GetScintillatorId();
					nToF++;
					hasToF = true;		

				}
			}
			
			if(hasToF){
				anaTrack.SetToFMassSquared(m2ToF[0]);
				anaTrack.SetToFScintillatorId(scId[0]);
			}

			if(!trSel.IsTrackGood(anaTrack))
				continue;
			
			posExtrap[0] = anaTrack.GetExtrapolatedPosition().GetX();
			posExtrap[1] = anaTrack.GetExtrapolatedPosition().GetY();
			posExtrap[2] = anaTrack.GetExtrapolatedPosition().GetZ();

			xE = anaTrack.GetPositionErrors().at(0);
			yE = anaTrack.GetPositionErrors().at(1);
			
			distTarg = anaTrack.GetDistanceFromTarget();
			pExtrap[0] = anaTrack.GetMomentum().GetX();
			pExtrap[1] = anaTrack.GetMomentum().GetY();
			pExtrap[2] = anaTrack.GetMomentum().GetZ();
			TrackPar temp;
			temp = trackExtrapolation.GetStopTrackParam();
			double pinv = temp.GetPar(4);
			sigmaP = sqrt(temp.GetStd(4)*temp.GetStd(4))/pinv/pinv;
			
			zFirst = track.GetFirstPointOnTrack().GetZ();
			zLast = track.GetLastPointOnTrack().GetZ();
			qRec = track.GetCharge();
			
			nTPC[0] = anaTrack.GetNumberOfClusters().VTPC1;
			nTPC[1] = anaTrack.GetNumberOfClusters().VTPC2;
			nTPC[2] = anaTrack.GetNumberOfClusters().GTPC;
			nTPC[3] = anaTrack.GetNumberOfClusters().MTPC;

			topology = anaTrack.GetTopology();
			dEdx = track.GetEnergyDeposit(TrackConst::eAll);

			for(sim::VertexTrackIndexIterator trIter = track.SimVertexTracksBegin(); trIter != track.SimVertexTracksEnd(); ++trIter){
				const sim::VertexTrack& vtxTrack = simEvent.Get(*trIter);

				if(!vtxTrack.HasStartVertex())
					continue;
	
				if(vtxTrack.GetNumberOfRecTracks() < 1) continue;	

				const sim::Vertex &vtx = simEvent.Get(vtxTrack.GetStartVertexIndex());
				xSim[nSim] = vtx.GetPosition().GetX();
				ySim[nSim] = vtx.GetPosition().GetY();
				zSim[nSim] = vtx.GetPosition().GetZ();

				pxSim[nSim] = vtxTrack.GetMomentum().GetX();
				pySim[nSim] = vtxTrack.GetMomentum().GetY();
				pzSim[nSim] = vtxTrack.GetMomentum().GetZ();


			
				qSim[nSim] = vtxTrack.GetCharge();
				pdgId[nSim] = vtxTrack.GetParticleId();

				nUnique[nSim] = vtxTrack.GetNumberOfUniquePoints(track.GetIndex());
				nCommon[nSim] = vtxTrack.GetNumberOfCommonPoints(track.GetIndex());
				
				nSim++;
				
				
			}	
			if(nSim > 10) continue;
			tree->Fill();		
		}
	}



	

	outputFile->cd();
	tree->Write();
	outputFile->Close();

}



