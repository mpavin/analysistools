#include <vector>
#include <string>
#include <iostream>
#include "Functions.h"
#include <TH1D.h>
#include <TH2D.h>
#include <TFile.h>
#include <TChain.h>
#include <TTree.h>
#include <TMath.h>
#include <math.h>

using namespace std;



int main(){


	//**************** file name *******************

	//string fileName = "acceptance_5_5_2016_10_35_28.root";
	//string fileName = "acceptance_5_21_2016_1_57_1.root";
	string input = "listacc.txt";
	//**************** z bins **********************
	const double zTarg = -657.51;
	const int n = 6;
	double zBins[n+1] = {0., 18., 36., 54., 72., 89.98, 90.04};

	int nDelta = 200;
	double deltaMin = -20;
	double deltaMax = 20;

	int nDeltax = 200;
	double deltaMinx = 0.0;
	double deltaMaxx = 400;

	double bdx = 2.5;
	double bdy = 2.5;
	double bp = 2;
	TH2D thp[n];
	TH2D thpRST[n];
	TH2D thpWST[n];

	TH2D migzbin("migzbin", "", 6, 0.5, 6.5, 6, 0.5, 6.5);
	TH1D normzbin("normzbin", "", 6, 0.5, 6.5);

	TH1D toph("toph", "", 7, 8.5, 15.5);
	//**************** check ***********************
	TH1D dx("dx", "", 200, -bdx, bdx);
	TH1D dxRST("dxRST", "", 200, -bdx, bdx);
	TH1D dxWST("dxWST", "", 200, -bdx, bdx);
	TH1D dy("dy", "", 100, -bdy, bdy);
	TH1D dyRST("dyRST", "", 100, -bdy, bdy);
	TH1D dyWST("dyWST", "", 100, -bdy, bdy);

	TH1D dist("dist", "", 100, 0, 100);
	TH1D distRST("distRST", "", 100, 0, 100);
	TH1D distWST("distWST", "", 100, 0, 100);

	TH1D dist1("dist1", "", 100, 0, 100);
	TH1D distRST1("distRST1", "", 100, 0, 100);
	TH1D distWST1("distWST1", "", 100, 0, 100);

	TH1D phi("phi", "", 180, -180, 180);
	TH1D phiRST("phiRST", "", 180, -180, 180);
	TH1D phiWST("phiWST", "", 180, -180, 180);

	TH1D th("th", "", 100, -10, 10);
	TH1D thRST("thRST", "", 100, -10, 10);
	TH1D thWST("thWST", "", 100, -10, 10);

	TH1D mom("mom", "", 100, -bp, bp);
	TH1D momRST("momRST", "", 100, -bp, bp);
	TH1D momWST("momWST", "", 100, -bp, bp);

	//**************** z migration *****************

	TH2D zrvszs("dzrvszs", "", 100, -5, 95, 100, -5, 95);
	TH2D zrvszsRST("dzrvszsRST", "", 100, -5, 95, 100, -5, 95);
	TH2D zrvszsWST("dzrvszsWST", "", 100, -5, 95, 100, -5, 95);

	TH2D thrvsths("thrvsths", "", 100, 0, 500, 100, 0, 500);
	TH2D thrvsthsRST("thrvsthsRST", "", 100, 0, 500, 100, 0, 500);
	TH2D thrvsthsWST("thrvsthsWST", "", 100, 0, 500, 100, 0, 500);

	TH2D prvsps("prvsps", "", 96, 0, 32, 96, 0, 32);
	TH2D prvspsRST("prvspsRST", "", 96, 0, 32, 96, 0, 32);
	TH2D prvspsWST("prvspsWST", "", 96, 0, 32, 96, 0, 32);


	TH1D zSimDist("zSimDist", "", 100, -5, 95);
	TH1D zSimDistRST("zSimDistRST", "", 100, -5, 95);
	TH1D zSimDistWST("zSimDistWST", "", 100, -5, 95);

	TH1D zRecDist("zRecDist", "", 100, -5, 95);
	TH1D zRecDistRST("zRecDistRST", "", 100, -5, 95);
	TH1D zRecDistWST("zRecDistWST", "", 100, -5, 95);
	//**************** struct definitions ***********
	SetHistoStyle(zrvszs, "", 0.06, 0.06, "z_{sim} [cm]", "z_{rec} [cm]");
	SetHistoStyle(zrvszsRST, "", 0.06, 0.06, "z_{sim} [cm]", "z_{rec} [cm]");
	SetHistoStyle(zrvszsWST, "", 0.06, 0.06, "z_{sim} [cm]", "z_{rec} [cm]");
	
	SetHistoStyle(thrvsths, "", 0.06, 0.06, "#theta_{sim} [mrad]", "#theta_{rec} [mrad]");
	SetHistoStyle(thrvsthsRST, "", 0.06, 0.06, "#theta_{sim} [mrad]", "#theta_{rec} [mrad]");
	SetHistoStyle(thrvsthsWST, "", 0.06, 0.06, "#theta_{sim} [mrad]", "#theta_{rec} [mrad]");	


	SetHistoStyle(prvsps, "", 0.06, 0.06, "p_{sim} [GeV/c]", "p_{rec} [GeV/c]");	
	SetHistoStyle(prvspsRST, "", 0.06, 0.06, "p_{sim} [GeV/c]", "p_{rec} [GeV/c]");	
	SetHistoStyle(prvspsWST, "", 0.06, 0.06, "p_{sim} [GeV/c]", "p_{rec} [GeV/c]");		
	
	SetHistoStyle(zSimDist, "", 0.06, 0.06, "z_{sim} [cm]", "");
	SetHistoStyle(zSimDistRST, "", 0.06, 0.06, "z_{sim} [cm]", "");
	SetHistoStyle(zSimDistWST, "", 0.06, 0.06, "z_{sim} [cm]", "");
	SetHistoStyle(zRecDist, "", 0.06, 0.06, "z_{rec} [cm]", "");
	SetHistoStyle(zRecDistRST, "", 0.06, 0.06, "z_{rec} [cm]", "");
	SetHistoStyle(zRecDistWST, "", 0.06, 0.06, "z_{rec} [cm]", "");
	
	SetHistoStyle(dx, "", 0.06, 0.06, "x_{rec} - x_{sim} [cm]", "");
	SetHistoStyle(dxRST, "", 0.06, 0.06, "x_{rec} - x_{sim} [cm]", "");
	SetHistoStyle(dxWST, "", 0.06, 0.06, "x_{rec} - x_{sim} [cm]", "");
	SetHistoStyle(dy, "", 0.06, 0.06, "y_{rec} - y_{sim} [cm]", "");
	SetHistoStyle(dyRST, "", 0.06, 0.06, "y_{rec} - y_{sim} [cm]", "");
	SetHistoStyle(dyWST, "", 0.06, 0.06, "y_{rec} - y_{sim} [cm]", "");

	SetHistoStyle(phi, "", 0.06, 0.06, "#phi_{rec} - #phi_{sim} [deg]", "");
	SetHistoStyle(phiRST, "", 0.06, 0.06, "#phi_{rec} - #phi_{sim} [deg]", "");
	SetHistoStyle(phiWST, "", 0.06, 0.06, "#phi_{rec} - #phi_{sim} [deg]", "");
		
	SetHistoStyle(th, "", 0.06, 0.06, "#theta_{rec} - #theta_{sim} [mrad]", "");
	SetHistoStyle(thRST, "", 0.06, 0.06, "#theta_{rec} - #theta_{sim} [mrad]", "");
	SetHistoStyle(thWST, "", 0.06, 0.06, "#theta_{rec} - #theta_{sim} [mrad]", "");
	
	SetHistoStyle(mom, "", 0.06, 0.06, "p_{rec} - p_{sim} [GeV/c]", "");
	SetHistoStyle(momRST, "", 0.06, 0.06, "p_{rec} - p_{sim} [GeV/c]", "");
	SetHistoStyle(momWST, "", 0.06, 0.06, "p_{rec} - p_{sim} [GeV/c]", "");
	
	SetHistoStyle(dist, "", 0.06, 0.06, "d_{targ}/#sigma_{R}", "");
	SetHistoStyle(distRST, "", 0.06, 0.06, "d_{targ}/#sigma_{R}", "");
	SetHistoStyle(distWST, "", 0.06, 0.06, "d_{targ}/#sigma_{R}", "");
	
	SetHistoStyle(dist1, "", 0.06, 0.06, "d_{targ}/#sigma_{R}", "");
	SetHistoStyle(distRST1, "", 0.06, 0.06, "d_{targ}/#sigma_{R}", "");
	SetHistoStyle(distWST1, "", 0.06, 0.06, "d_{targ}/#sigma_{R}", "");
	
	
	for(int i = 0; i < n; i++){
		string name = "thp" + ConvertToString(i+1);
		string nameRST = "thpRST" + ConvertToString(i+1);
		string nameWST = "thpWST" + ConvertToString(i+1);
		
		string title = ConvertToString(zBins[i]) + " #leq z < " + ConvertToString(zBins[i+1]) + " cm";
		if(i==5){
			title = "z = 90 cm"; 
		}
		TH2D temp(name.c_str(), title.c_str(), 90, 0, 30, 100, 0, 400);
		TH2D tempRST(nameRST.c_str(), title.c_str(), 90, 0, 30, 100, 0, 400);
		TH2D tempWST(nameWST.c_str(), title.c_str(), 90, 0, 30, 100, 0, 400);
		
		SetHistoStyle(temp, "", 0.06, 0.06, "p [GeV/c]", "#theta [mrad]");
		SetHistoStyle(tempRST, "", 0.06, 0.06, "p [GeV/c]", "#theta [mrad]");
		SetHistoStyle(tempWST, "", 0.06, 0.06, "p [GeV/c]", "#theta [mrad]");
		
		thp[i] = temp;
		thpRST[i] = tempRST;
		thpWST[i] = tempWST;
	}
	
	
	double pExtrap[3];
	double posExtrap[3];
	int qRec;
	int nTPC[4];
	double dEdx;
	int topology;
	double distTarg;
	int nToF;
	double m2ToF[3];
	int scId[3];
	double xE;
	double yE;
	double sigmaP;
	double zLast;
	double zFirst;
	
	int nSim;
	double pxSim[7];
	double pySim[7];
	double pzSim[7];
	double xSim[7];
	double ySim[7];
	double zSim[7];
	int pdgId[7];
	int qSim[7];
	int nUnique[7];
	int nCommon[7];	
	
	
	TChain chain("MCData");
	

	ifstream ifs;
	string line;
	ifs.open(input.c_str());
		
	if (!ifs.is_open()){
		cerr << "[ReadInputListFile] Could not open infile. Exiting..." << endl;			
		exit (101);
	}
    
	while (getline(ifs, line)){
		if(line.at(0)=='#')
			continue;
		chain.Add(line.c_str());
	}
	ifs.close();	

 	//chain.SetBranchAddress("Beam", &beam);
 	chain.SetBranchAddress("posExtrap", posExtrap);
 	chain.SetBranchAddress("pExtrap", pExtrap);
 	chain.SetBranchAddress("qRec", &qRec);
 	chain.SetBranchAddress("nTPC", nTPC);
 	chain.SetBranchAddress("dEdx", &dEdx);
 	chain.SetBranchAddress("topology", &topology);
 	chain.SetBranchAddress("distTarg", &distTarg);
 	chain.SetBranchAddress("nToF", &nToF);
 	chain.SetBranchAddress("m2ToF", m2ToF);
 	chain.SetBranchAddress("scId", scId);	
 	chain.SetBranchAddress("xE", &xE);
 	chain.SetBranchAddress("yE", &yE);
	chain.SetBranchAddress("zLast", &zLast);
	chain.SetBranchAddress("zFirst", &zFirst);
	chain.SetBranchAddress("sigmaP", &sigmaP);
	
	chain.SetBranchAddress("nSim", &nSim);
	chain.SetBranchAddress("pxSim", pxSim);
	chain.SetBranchAddress("pySim", pySim);
	chain.SetBranchAddress("pzSim", pzSim);
	chain.SetBranchAddress("xSim", xSim);
	chain.SetBranchAddress("ySim", ySim);
	chain.SetBranchAddress("zSim", zSim);
	chain.SetBranchAddress("pdgId", pdgId);
	chain.SetBranchAddress("qSim", qSim);
	chain.SetBranchAddress("nUnique", nUnique);
	chain.SetBranchAddress("nCommon", nCommon);

 	
	int nEntries = chain.GetEntries(); 	

	for(int i = 0; i < nEntries; ++i){
		//cout << topology << endl;
		chain.GetEntry(i);

		//if(i > 10000000) break;
		if(!((i+1)%10000))
			cout << "Procesing entry # " << i+1 << " out of " << nEntries << "." << endl;
			

		if(nSim < 1) continue;
		
		double pSim = sqrt(pxSim[0]*pxSim[0] + pySim[0]*pySim[0] + pzSim[0]*pzSim[0]);
		double pRec = sqrt(pExtrap[0]*pExtrap[0] + pExtrap[1]*pExtrap[1] + pExtrap[2]*pExtrap[2]);
		double zS = zSim[0] - zTarg;
		double zR = posExtrap[2] - zTarg - 0.01;
		//cout << zRec << " " << zSim<< endl;
		int index;
		for(int j = 0; j < n; j++){
			if(zBins[j] <= zR	&& zBins[j+1] > zR){
				index = j;
				break;
			}
		}	
		
		int indexM;
		for(int j = 0; j < n; j++){
			if(zBins[j] <= zS	&& zBins[j+1] > zS){
				indexM = j;
				break;
			}
		}	
		
		//cout << index << endl;
		double sigmaR = sqrt(posExtrap[0]*posExtrap[0]*xE*xE + posExtrap[1]*posExtrap[1]*yE*yE)/sqrt(posExtrap[0]*posExtrap[0] + posExtrap[1]*posExtrap[1]);
		
		//cout << SimPos.z << endl;
		//if(zSim>90.02) continue;
		
		//if(nToF>0) continue;
		//cout << "radi1" << endl;
		//cout << topology << endl;
		if(topology == 13) continue;
		if(topology == 10) continue;
		//if(topology != 10) continue;
		
		//cout << distTarg << " " << sigmaR <<endl;
		if(topology == 12){
			if(nTPC[2] < 5) continue;
			if(nTPC[3] < 40) continue;		
		}
		else{
			if(nTPC[0] + nTPC[1] < 20) continue;
			if(nTPC[3] < 30) continue;
		}
		
		
		if(index==5){
			dist1.Fill(distTarg/sigmaR);
			if(qRec*pExtrap[0] > 0){
				distRST1.Fill(distTarg/sigmaR);
			}
			else{
				distWST1.Fill(distTarg/sigmaR);
			}
		}
		else{
			dist.Fill(distTarg/sigmaR);
			if(qRec*pExtrap[0] > 0){
				distRST.Fill(distTarg/sigmaR);
			}
			else{
				distWST.Fill(distTarg/sigmaR);
			}
		}
		
		//if(index<5) {if (distTarg < 1*sigmaR) continue;}
		if (distTarg > 1*sigmaR) continue;
		

		toph.Fill(topology);
		//if(pRec < 2) continue;
		//if(indexM == 5) continue;
		migzbin.Fill(indexM+1, index+1);
		normzbin.Fill(indexM+1);
		//********************TEMP CUTS***************************
		
		
		double thetaSim = acos(pzSim[0]/pSim);
		//cout << zSim << " " << zRec << endl;
		double thetaRec = acos(pExtrap[2]/pRec);
		

		//sigmap = pSim;
		//sigmath = thetaSim;

		/*double drx = posExtrap[0] - 0.2391;
		double dry = posExtrap[1] - 0.1258;
		double drxs = xSim[0] - 0.2391;
		double drys = ySim[0] - 0.1258;

		if(index==5){
			dx.Fill(posExtrap[0]-xSim[0]);
			dy.Fill(posExtrap[1]-ySim[0]);
			
		}
		else{
			phi.Fill(180*(atan(dry/drx)-atan(drys/drxs))/TMath::Pi());
		}
		zrvszs.Fill(zS, zR);
		thrvsths.Fill(1000*thetaSim, 1000*thetaRec);
		prvsps.Fill(pSim, pRec);
		
		zSimDist.Fill(zS);
		zRecDist.Fill(zR);
		th.Fill(1000*thetaRec - 1000*thetaSim);
		mom.Fill(pRec-pSim);
		thp[index].Fill(pRec, thetaRec*1000);*/
		if(qRec*pExtrap[0] > 0){
			/*if(index==5){
				dxRST.Fill(posExtrap[0]-xSim[0]);
				dyRST.Fill(posExtrap[1]-ySim[0]);
			}
			else{
				phiRST.Fill(180*(atan(dry/drx)-atan(drys/drxs))/TMath::Pi());
			}
			zSimDistRST.Fill(zS);
			zRecDistRST.Fill(zR);
			zrvszsRST.Fill(zS, zR);
			thrvsthsRST.Fill(1000*thetaSim, 1000*thetaRec);
			prvspsRST.Fill(pSim, pRec);
			thRST.Fill(1000*thetaRec - 1000*thetaSim);
			momRST.Fill(pRec-pSim);*/
			
			thpRST[index].Fill(pRec, thetaRec*1000);
		}
		else if(qRec*pExtrap[0] < 0){
			/*if(index==5){
				dxWST.Fill(posExtrap[0]-xSim[0]);
				dyWST.Fill(posExtrap[1]-ySim[0]);
			}
			else{
				phiWST.Fill(180*(atan(dry/drx)-atan(drys/drxs))/TMath::Pi());
			}
			zSimDistWST.Fill(zS);
			zRecDistWST.Fill(zR);
			zrvszsWST.Fill(zS, zR);
			
			thrvsthsWST.Fill(1000*thetaSim, 1000*thetaRec);
			prvspsWST.Fill(pSim, pRec);
			thWST.Fill(1000*thetaRec - 1000*thetaSim);
			momWST.Fill(pRec-pSim);*/
			
			thpWST[index].Fill(pRec, thetaRec*1000);
		}
	}
	

	for(int i = 0; i < 6; i++){
		for(int j = 0; j < 6; j++){
			migzbin.SetBinContent(i+1, j+1, migzbin.GetBinContent(i+1, j+1)/normzbin.GetBinContent(i+1));
		}	
	}
	
	TFile out("accRes.root", "RECREATE");
	out.cd();
	migzbin.SetMaximum(1.0);
	migzbin.SetMinimum(0.0);	
	migzbin.Write();
	
	toph.Scale(1/toph.GetEntries());
	toph.Write();
	
	dx.Scale(1/dx.GetEntries());
	dxRST.Scale(1/dxRST.GetEntries());
	dxWST.Scale(1/dxWST.GetEntries());
	dx.Write();
	dxRST.Write();
	dxWST.Write();

	dy.Scale(1/dy.GetEntries());
	dyRST.Scale(1/dyRST.GetEntries());
	dyWST.Scale(1/dyWST.GetEntries());	
	dy.Write();
	dyRST.Write();
	dyWST.Write();

	phi.Scale(1/phi.GetEntries());
	phiRST.Scale(1/phiRST.GetEntries());
	phiWST.Scale(1/phiWST.GetEntries());
	phi.Write();
	phiRST.Write();
	phiWST.Write();

	dist.Scale(1/dist.GetEntries());
	distRST.Scale(1/distRST.GetEntries());
	distWST.Scale(1/distWST.GetEntries());	
	dist.Write();
	distRST.Write();
	distWST.Write();

	dist1.Scale(1/dist1.GetEntries());
	distRST1.Scale(1/distRST1.GetEntries());
	distWST1.Scale(1/distWST1.GetEntries());	
	dist1.Write();
	distRST1.Write();
	distWST1.Write();
	
	zSimDist.Write();
	zSimDistRST.Write();
	zSimDistWST.Write();
	
	zRecDist.Write();
	zRecDistRST.Write();
	zRecDistWST.Write();

	th.Scale(1/th.GetEntries());
	thRST.Scale(1/thRST.GetEntries());
	thWST.Scale(1/thWST.GetEntries());		
	th.Write();
	thRST.Write();
	thWST.Write();

	mom.Scale(1/mom.GetEntries());
	momRST.Scale(1/momRST.GetEntries());
	momWST.Scale(1/momWST.GetEntries());		
	mom.Write();
	momRST.Write();
	momWST.Write();
	
	zrvszs.Write();
	zrvszsRST.Write();
	zrvszsWST.Write();
	
	thrvsths.Write();
	thrvsthsRST.Write();
	thrvsthsWST.Write();
	
	prvsps.Write();
	prvspsRST.Write();
	prvspsWST.Write();

	//sigmapvsp.Write();

	for(int i = 0; i < n; i++){
		thp[i].Write();
		thpRST[i].Write();
		thpWST[i].Write();
	}
	out.Close();
}


