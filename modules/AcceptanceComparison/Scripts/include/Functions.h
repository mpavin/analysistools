#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <fstream>
#include <vector>
#include <string>
#include <sstream>
#include <iostream>    
#include <iomanip> 
#include <stdlib.h>
#include <TH1.h>
using namespace std;

std::string ConvertToString(int x){
	std::stringstream ss;
	std::string val;

	ss << x;
	ss >> val;

	return val;
}

void SetHistoStyle(TH1& hist, string title, double axlbsize, double axtsize, string xtitle, string ytitle){
	hist.SetTitle(title.c_str());
	hist.GetXaxis()->SetTitle(xtitle.c_str());
	hist.GetXaxis()->SetTitleSize(axtsize);
	hist.GetXaxis()->SetLabelSize(axlbsize);
	
	hist.GetYaxis()->SetTitle(ytitle.c_str());
	hist.GetYaxis()->SetTitleSize(axtsize);
	hist.GetYaxis()->SetLabelSize(axlbsize);
	
}
#endif
