#!/bin/sh
 
#set -e


MYDIR="/afs/cern.ch/user/m/mpavin/Work/analysistools/modules/AcceptanceComparison"
#**********************************************************************************

MCDIR="/eos/na61/data/Simulation/p_T2K_replica_target_10/v14e/FLUKA2011.2c.5_GCALOR/10_035/20MF_all_pp/SHOE"
#MCDIR="/eos/na61/data/Simulation/p_T2K_replica_target_10/v15a/FLUKA2011.2c.2_GCALOR/10_030/160MF_all_pp/shoe"
RESULTS="/afs/cern.ch/work/m/mpavin/Results/ACC"
#**********************************************************************************
#	SHINE ENV
readonly shine_version='v1r4p0'
readonly shine_root="/afs/cern.ch/na61/Releases/SHINE/${shine_version}"
. "${shine_root}/scripts/env/lxplus_32bit_slc6.sh"
eval $("${shine_root}/bin/shine-offline-config" --env-sh)
#**********************************************************************************
#**********************************************************************************
#	EOS
eos="/afs/cern.ch/project/eos/installation/0.3.84-aquamarine/bin/eos.select"
eosumount="/afs/cern.ch/project/eos/installation/0.3.84-aquamarine/bin/eos.select -b fuse umount"                                                                               
eosmount="/afs/cern.ch/project/eos/installation/0.3.84-aquamarine/bin/eos.select -b fuse mount"    
#**********************************************************************************

#**********************************************************************************
#	TMP DIR
if [ -n "${LSB_BATCH_JID}" ]; then
        tmpdir="/pool/lsf/${USER}/${LSB_BATCH_JID}"                                                                                                                          
else
        tmpdir="${TMPDIR:=/tmp/${USER}}"                                                                                                                        
fi
#**********************************************************************************

#**********************************************************************************
#	DIR FOR RESULTS
export RES="${tmpdir}/Results"
cd $tmpdir
mkdir Results
#**********************************************************************************

#**********************************************************************************

UDIR=$MCDIR
"$eos" ls "${UDIR}/*.root" > "${tmpdir}/log.txt"
COUNT=0
START=$1
STEP=$2
STOP=$((START+STEP))

while read line
do
		COUNT=$((COUNT+1))
		if [ "$COUNT" -ge "$START" ] && [ "$COUNT" -lt "$STOP" ]; then
			"$eos" cp "$UDIR/$line" "./"		
		fi
done < "${tmpdir}/log.txt"

for i in *.root; 
do 
	echo "${tmpdir}/${i}" >> list.txt
done;

cd ${MYDIR}
export INPUTFILE="${tmpdir}/list.txt"
./acceptanceComp -b bootstrap.xml
cp "${RES}/acceptance.root" "${RESULTS}/acceptance_${START}-${STOP}.root"



#cd ${MYDIR}

#./acceptanceComp -b bootstrap.xml -n 3500000

#cp -r Results/ ${MYDIR}/Results/
