#include <TFile.h>
#include <TH1D.h>
#include <TH2D.h>
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <TChain.h>
#include <TBranch.h>

using namespace std;

//string input1 = "listshift.txt";
string input1 = "list1.txt";

	const int nr = 26;
	const int nr1[nr] = {3, 4, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5};						
	double rbord[27] = {0.00, 0.05, 0.10, 0.15, 0.20, 0.25, 0.30, 0.35, 0.40, 0.45, 0.50, 
						0.55, 0.60, 0.65, 0.70, 0.75, 0.80, 0.85, 0.90, 0.95, 1.00, 1.05, 1.10, 1.15, 1.20, 1.25, 1.30};
						
	double r1bord[26][6] = {{0.00, 0.05, 0.10, 0.15, 0.00, 0.00},
							{-0.05, 0.00, 0.05, 0.10, 0.15, 0.00},
							{-0.10, -0.05, 0.00, 0.05, 0.10, 0.15},
							{-0.10, -0.05, 0.00, 0.05, 0.10, 0.15},
							{-0.10, -0.05, 0.00, 0.05, 0.10, 0.15},
							{-0.10, -0.05, 0.00, 0.05, 0.10, 0.15},
							{-0.10, -0.05, 0.00, 0.05, 0.10, 0.15},
							{-0.10, -0.05, 0.00, 0.05, 0.10, 0.15},
							{-0.10, -0.05, 0.00, 0.05, 0.10, 0.15},
							{-0.10, -0.05, 0.00, 0.05, 0.10, 0.15},
							{-0.10, -0.05, 0.00, 0.05, 0.10, 0.15},
							{-0.10, -0.05, 0.00, 0.05, 0.10, 0.15},
							{-0.10, -0.05, 0.00, 0.05, 0.10, 0.15},
							{-0.10, -0.05, 0.00, 0.05, 0.10, 0.15},
							{-0.10, -0.05, 0.00, 0.05, 0.10, 0.15},
							{-0.10, -0.05, 0.00, 0.05, 0.10, 0.15},
							{-0.10, -0.05, 0.00, 0.05, 0.10, 0.15},
							{-0.10, -0.05, 0.00, 0.05, 0.10, 0.15},
							{-0.10, -0.05, 0.00, 0.05, 0.10, 0.15},
							{-0.10, -0.05, 0.00, 0.05, 0.10, 0.15},
							{-0.10, -0.05, 0.00, 0.05, 0.10, 0.15},
							{-0.10, -0.05, 0.00, 0.05, 0.10, 0.15},
							{-0.10, -0.05, 0.00, 0.05, 0.10, 0.15},
							{-0.10, -0.05, 0.00, 0.05, 0.10, 0.15},
							{-0.10, -0.05, 0.00, 0.05, 0.10, 0.15},
							{-0.10, -0.05, 0.00, 0.05, 0.10, 0.15}};

TH2D beam("beam", "", 200, -2, 2, 200, -2, 2);
//TH2D beam2("beam2", "", 200, -2, 2, 200, -2, 2);

//**********************************************************
//******************Downstream face*************************
TH2D downstream("downstream", "", 200, -2, 2, 200, -2, 2);

//******************FLANGE******************************

TH2D xflangez("xflangez", "", 200, -5, 95, 200, -10, 10);
TH2D yflangez("yflangez", "", 200, -5, 95, 200, -10, 10);
TH2D rflangez("rflangez", "", 200, -5, 95, 200, 0, 10);
TH1D momflange("momflange", "", 200, 0, 10); 
TH1D zdist("zdist", "", 180, 0, 90); 
TH1D cth("cth", "", 100, -1, 1); 

vector<vector<TH1D> > pred;
//vector<vector<TH1D> > wsth;


std::string ConvertToString(double x, int prec){
	std::stringstream ss;
	std::string val;

	ss << std::fixed << std::setprecision(prec);
	ss << x;
	ss >> val;

	return val;
}

	double targX = 0.1153;
	double targY = 0.1258;
	//double targX = 0.2391;
	//double targY = 0.1258;
	
	
double pbord[12] = {32, 24, 20, 16, 15, 12, 10, 8, 7, 6, 5, 5};
int ipbord[12] = {64, 48, 40, 32, 60, 48, 40, 32, 56, 48, 40, 40};
double w[12] = {0.5, 0.5, 0.5, 0.5, 0.25, 0.25, 0.25, 0.25, 0.125, 0.125, 0.125, 0.125};
void CheckMC(){
	double zbor[7] = {0, 18, 36, 54, 72, 89.99, 90.01};
	double thbor[12] = {0, 20, 40, 60, 80, 100, 140, 180, 220, 260, 300, 340};
	double pb[11] = {19., 33., 21., 16., 13., 11., 9., 8., 6., 5., 3.};
	int np[11] = {95, 165, 105, 80, 65, 55, 45, 40, 30, 25, 15};
	
	
	for(int i = 0; i<6; i++){
		vector<TH1D> temph;

		for(int j = 0; j < 11; j++){
			string name = "dndp_z" + ConvertToString(i+1.,0) + "_th" + ConvertToString(j+1.,0);
			
			
			//cout << pbord[j]/ipbord[j] << endl;
			TH1D temp(name.c_str(), "", ipbord[j], 0, pbord[j]);

			
			temp.GetXaxis()->SetTitle("p [GeV/c]");
			temp.GetXaxis()->SetLabelSize(0.06);
			temp.GetXaxis()->SetTitleSize(0.06);
			
			temp.GetYaxis()->SetTitle("dn/dp [GeV/c]^{-1}");
			temp.GetYaxis()->SetLabelSize(0.06);
			temp.GetYaxis()->SetTitleSize(0.06);
			

			
			string title = ConvertToString(thbor[j],0)+ "#leq #theta < " + ConvertToString(thbor[j+1],0) + " mrad";
			temp.SetTitle(title.c_str());
			temp.SetLineColor(1);
		
			temph.push_back(temp);

		}
		pred.push_back(temph);

	}
	TChain chain("h1000");
	

	ifstream ifs;
	string line;
	ifs.open(input1.c_str());
		
	if (!ifs.is_open()){
		cerr << "[ReadInputListFile] Could not open infile. Exiting..." << endl;			
		exit (101);
	}
    
	while (getline(ifs, line)){
		if(line.at(0)=='#')
			continue;
		chain.Add(line.c_str());
	}
	ifs.close();		

	Int_t           protnum;
  	Int_t           ipart;
  	Float_t         mom;
  	Float_t         pos[3];
  	Float_t         vec[3];

  // Declaration of leaf types for replica input
  	Int_t           pgen;
  	Int_t           ng;
  	Float_t         gpx[20];    //[ng]
  	Float_t         gpy[20];    //[ng]
  	Float_t         gpz[20];    //[ng]
  	Float_t         gvx[20];    //[ng]
  	Float_t         gvy[20];    //[ng]
  	Float_t         gvz[20];    //[ng]
  	Int_t           gpid[20];   //[ng]
  	Int_t           gmec[20];   //[ng]

	TBranch        *b_protnum;   //!
  	TBranch        *b_ipart;     //!
  	TBranch        *b_mom;       //!
  	TBranch        *b_pos;       //!
  	TBranch        *b_vec;       //!
  	TBranch        *b_pgen;      //!
  	TBranch        *b_ng;        //!
  	TBranch        *b_gpx;       //!
  	TBranch        *b_gpy;       //!
  	TBranch        *b_gpz;       //!
  	TBranch        *b_gvx;       //!
  	TBranch        *b_gvy;       //!
  	TBranch        *b_gvz;       //!
  	TBranch        *b_gpid;      //!
  	TBranch        *b_gmec;      //!
    
	chain.SetBranchAddress("protnum", &protnum, &b_protnum);
  	chain.SetBranchAddress("ipart", &ipart, &b_ipart);
  	chain.SetBranchAddress("mom", &mom, &b_mom);
  	chain.SetBranchAddress("pos", pos, &b_pos);
  	chain.SetBranchAddress("vec", vec, &b_vec);
  	chain.SetBranchAddress("pgen", &pgen, &b_pgen);
  	chain.SetBranchAddress("ng", &ng, &b_ng);
  	chain.SetBranchAddress("gpx", gpx, &b_gpx);
  	chain.SetBranchAddress("gpy", gpy, &b_gpy);
  	chain.SetBranchAddress("gpz", gpz, &b_gpz);
  	chain.SetBranchAddress("gvx", gvx, &b_gvx);
  	chain.SetBranchAddress("gvy", gvy, &b_gvy);
  	chain.SetBranchAddress("gvz", gvz, &b_gvz);
  	chain.SetBranchAddress("gpid", gpid, &b_gpid);
  	chain.SetBranchAddress("gmec", gmec, &b_gmec);
  	
  	
  	int nEntries = chain.GetEntries();
  	int pnum = 1;
  	
  	double npot1 = 1;
  	double npNI1 = 0;
  	
  	int count = 0;
  	int lzero = 0;
  	double gz1 = 0;
  	

  	int c = 0;
  	bool inter = false;
  	int eventId = 0;
  	int totev = 0;
  	double totev1 = 0;
  	
  	int bBinId = -99;
  	for(int i = 0; i < nEntries; i++){
  		
  		chain.GetEntry(i);
  		//if(i == 1000000) break;

		
		//if(mom<0.1) continue;
		if(protnum != eventId){
			double r = sqrt(TMath::Power(-gvz[0]*gpx[0]/gpz[0] + gvx[0] - targX,2) + TMath::Power(-gvz[0]*gpy[0]/gpz[0] + gvy[0]- targY,2));
			double r1 = sqrt(TMath::Power((90-gvz[0])*gpx[0]/gpz[0] + gvx[0]- targX ,2) + TMath::Power((90-gvz[0])*gpy[0]/gpz[0] + gvy[0]- targY ,2));
			
			//beam.Fill(-gvz[0]*gpx[0]/gpz[0] + gvx[0]- targX, -gvz[0]*gpy[0]/gpz[0] + gvy[0]- targY);
			
			int rbin = -99;
			for(int j = 0; j < nr; j++){
				if(rbord[j] <= r && r < rbord[j+1]){
					rbin = j;
					break;
				}
			}
			if(rbin >= 0){
				int r1bin = -99;
				for(int j = 0; j < nr1[rbin]; j++){
					if(rbord[rbin]+r1bord[rbin][j] <= r1 && r1 < rbord[rbin]+r1bord[rbin][j+1]){
						r1bin = j;
						break;
					}
				}
				if(r1bin >= 0){
					bBinId = r1bin+1;
					for(int j = 0; j < rbin; j++){
						bBinId += nr1[j];
					}

					totev1++;
				}
				else{
					bBinId = -99;
				}
			}
			else{
				bBinId = -99;
			}
			eventId = protnum;
			totev++;
			
  			if(!(totev%100000))
  				cout << "Processing event # " << totev << endl;
		}
		
		if (bBinId < 0) continue;
		
		if(ipart!=8) continue;
		

		
		double z = pos[2];// + 657.51;
		
		double rp = sqrt((pos[0]-targX)*(pos[0]-targX) + (pos[1]-targY)*(pos[1]-targY));
		if(rp>1.31) continue;
		if(z<0) continue;
		if(z>90.01) continue;

		int zid = -99;
		for(int j = 0; j < 6; j++){
			if(zbor[j] <= z && z < zbor[j+1]){
				zid = j;
				break;
			}
		}
		if(zid < 0) continue;
		
		double th= 1000*acos(vec[2]);
		if(th > 340) continue;
		int thid = -99;
		for(int j = 0; j < 11; j++){
			if(th >= thbor[j] && th < thbor[j+1]){
				thid = j;
				break;
			}
		}

		
		if(thid < 0) continue;
		
		
		pred.at(zid).at(thid).Fill(mom);


  	}
  	
  	TFile out("res.root", "RECREATE");
  	out.cd();
  	
  	for(int i = 0; i<6; i++){
  		for(int j = 0; j < 11; j++){
 
 			for(int k = 0; k < pred.at(i).at(j).GetNbinsX(); k++){
 				double dp = pbord[j]/ipbord[j];
 				pred.at(i).at(j).SetBinContent(k+1, pred.at(i).at(j).GetBinContent(k+1)/totev1/dp);
 			}
 			//double scale = 1/totev1/0.2;
   			//pred.at(i).at(j).Scale(scale);
   			pred.at(i).at(j).Write();
  			
  		}
  	}

  	out.Close();
 	
}
