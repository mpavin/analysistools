#include <vector>
#include <string>
#include <iostream>
#include "Functions.h"
#include <TH1D.h>
#include <TH2D.h>
#include <TFile.h>
#include <TChain.h>
#include <TTree.h>
#include <TMath.h>
#include <math.h>
#include <TCanvas.h>
#include <TF1.h>
#include "dEdxResolution.h"
using namespace std;


int main(){

	//cout << argc << " " << argv[0] << endl;

	

	
	string inputFile = "list.txt";

	TChain chain("Data");

	ifstream ifs;
	string line;
	ifs.open(inputFile.c_str());
		
	if (!ifs.is_open()){
		cerr << "[ReadInputListFile] Could not open infile. Exiting..." << endl;			
		exit (101);
	}
    
	while (getline(ifs, line)){
		if(line.at(0)=='#')
			continue;
		chain.Add(line.c_str());
	}
	ifs.close();		



	BeamA beamD;
	vector<double> *x = NULL;
	vector<double> *xE = NULL;
	vector<double> *y = NULL;
	vector<double> *yE = NULL;
	vector<double> *z = NULL;
	vector<double> *px = NULL;
	vector<double> *py = NULL;
	vector<double> *pz = NULL;
	vector<double> *zLast = NULL;
	vector<double> *distTarg = NULL;
	vector<double> *zStart = NULL;
	vector<double> *dEdx = NULL;
	vector<int> *qD = NULL;
	vector<int> *nVTPC1 = NULL;
	vector<int> *nVTPC2 = NULL;
	vector<int> *nGTPC = NULL;
	vector<int> *nMTPC = NULL;
	vector<int> *topology = NULL;
	vector<int> *nTOF = NULL;
	vector<double> *m2TOF = NULL;	
	vector<int> *tofScId = NULL;
	vector<int> *pdgId = NULL;
	int runNumber;
	
	TBranch *b_aData = chain.GetBranch("Beam");
 	b_aData->SetAddress(&beamD);
	chain.SetBranchAddress("x", &x);
	chain.SetBranchAddress("xE", &xE);
	chain.SetBranchAddress("y", &y);
	chain.SetBranchAddress("yE", &yE);
	chain.SetBranchAddress("z", &z);
	chain.SetBranchAddress("px", &px);
	chain.SetBranchAddress("py", &py);
	chain.SetBranchAddress("pz", &pz);
	chain.SetBranchAddress("zLast", &zLast);
	chain.SetBranchAddress("distTarg", &distTarg);
	chain.SetBranchAddress("zStart", &zStart);
	chain.SetBranchAddress("dEdx", &dEdx);
	chain.SetBranchAddress("q", &qD);
	chain.SetBranchAddress("nVTPC1", &nVTPC1);
	chain.SetBranchAddress("nVTPC2", &nVTPC2);
	chain.SetBranchAddress("nGTPC", &nGTPC);
	chain.SetBranchAddress("nMTPC", &nMTPC);
	chain.SetBranchAddress("topology", &topology);
	chain.SetBranchAddress("nTOF", &nTOF);
	chain.SetBranchAddress("m2TOF", &m2TOF);
	chain.SetBranchAddress("tofScId", &tofScId);
	chain.SetBranchAddress("runNumber", &runNumber);
	chain.SetBranchAddress("pdgId", &pdgId);
	
	int nEntries = chain.GetEntries();
	
	vector<double> ranges;
	for(int i = 0; i < 8; i++){
		ranges.push_back(0);
	}
	string dEdxFile = "dEdxParMC.root";
	dEdxResolution *dEdxRes = new dEdxResolution(dEdxFile, ranges);

	TH2D dEdxCl("dEdxCl", "", 50, 0, 250, 100, -0.15, 0.26);
	TH1D dEdx1D("dEdxCl1", "", 50, 0,250);
	for(int i = 0; i < nEntries; ++i){
		
		chain.GetEntry(i,1);
		
		if(!((i+1)%100000))
			cout << "Processing event # "  << i+1 << endl;	
		
		for(int j = 0; j < pdgId->size(); j++){

			/*if(topology->at(j) == 12){
				if(px->at(j)*qD < 0) continue;
				if(nGTPC->at(j) < 5) continue;
				if(nMTPC->at(j) < 60) continue;
			}
			else{
				if(nVTPC1->at(j) + nVTPC2->at(j) < 20) continue;
				if(nMTPC->at(j) < 30) continue;			
			}
			

			double theta = 1000*acos(pz->at(j)/sqrt(px->at(j)*px->at(j)+py->at(j)*py->at(j)+pz->at(j)*pz->at(j)));
			if(topology->at(j) == 14) {
				if(theta > 40){
					if(px->at(j)*qD < 0) continue;
				}
			}*/
			//if(topology->at(j) == 12) continue;
			if(topology->at(j) == 10) continue;
			double theta = 1000*acos(pz->at(j)/sqrt(px->at(j)*px->at(j)+py->at(j)*py->at(j)+pz->at(j)*pz->at(j)));
			double pVal = sqrt(px->at(j)*px->at(j) + py->at(j)*py->at(j) + pz->at(j)*pz->at(j));
			double ds = sqrt(TMath::Power((x->at(j)-0.2391)*xE->at(j),2)+TMath::Power((y->at(j)-0.1258)*yE->at(j),2))/distTarg->at(j);
			//if(ds/distTarg->at(j) > 1) continue;
			
			double ncl = nVTPC1->at(j) + nVTPC2->at(j) + nGTPC->at(j) + nMTPC->at(j);
			//double ncl =  nMTPC->at(j);
			//if(z->at(j)+657.51 > 18) continue;
			
			double phi = acos(px->at(j)/sqrt(px->at(j)*px->at(j)+py->at(j)*py->at(j)))*180/TMath::Pi();
			if(px->at(j)>0 && py->at(j) < 0){
				phi = -1*phi;
			}
			else if(px->at(j)<0 && py->at(j) < 0){
				phi = 360 - phi;
			}
						
			if(theta >= 0 && theta < 20){
				if( (phi < -70 || phi > 70) && (phi < 110 || phi > 250)) continue;
			}
			else if (theta >= 20 && theta < 40){
				if( (phi < -60 || phi > 60) && (phi < 140 || phi > 220)) continue;
			}
			else if (theta >= 40 && theta < 60){
				if( (phi < -45 || phi > 35) && (phi < 145 || phi > 225)) continue;
			}
			else if (theta >= 60 && theta < 80){
				if( (phi < -30 || phi > 30) && (phi < 150 || phi > 210)) continue;
			}
			else if (theta >= 80 && theta < 100){
				if( (phi < -20 || phi > 20) && (phi < 160 || phi > 200)) continue;
			}	
			else if (theta >= 100 && theta < 120){
				if( (phi < -17 || phi > 17) && (phi < 163 || phi > 197)) continue;
			}
			else if (theta >= 120 && theta < 140){
				if( (phi < -15 || phi > 15) && (phi < 165 || phi > 195)) continue;
			}			
			else if (theta >= 140 && theta < 160){
				if( (phi < -13 || phi > 13) && (phi < 167 || phi > 193)) continue;
			}	
			else if (theta >= 160 && theta < 200){
				if( (phi < -12 || phi > 12) && (phi < 168 || phi > 192)) continue;
			}	
			else if (theta >= 200 && theta < 240){
				if( (phi < -11 || phi > 11) && (phi < 169 || phi > 191)) continue;
			}		
			else if (theta >= 240 && theta < 600){
				if( (phi < -10 || phi > 10) && (phi < 170 || phi > 190)) continue;
			}
			
			/*if(abs(pdgId->at(j)) == 211){
				dEdxCl.Fill(ncl, (dEdx->at(j) - dEdxRes->GetMean(pVal/0.139570))/dEdxRes->GetMean(pVal/0.139570));
				//dEdxCl.Fill(ncl, dEdx->at(j));
			}
			else if(abs(pdgId->at(j)) == 321){
				dEdxCl.Fill(ncl, (dEdx->at(j) - dEdxRes->GetMean(pVal/0.493667))/dEdxRes->GetMean(pVal/0.493667));
			}
			else if(abs(pdgId->at(j)) == 2212){
				dEdxCl.Fill(ncl, (dEdx->at(j) - dEdxRes->GetMean(pVal/0.938272))/dEdxRes->GetMean(pVal/0.938272));
			}
			else if(abs(pdgId->at(j)) == 11){
				dEdxCl.Fill(ncl, (dEdx->at(j) - dEdxRes->GetMean(pVal/0.000510999))/dEdxRes->GetMean(pVal/0.000510999));
			}*/
			if(pVal < 7){
				if(m2TOF->at(j) > 0.70){
					dEdxCl.Fill(ncl, (dEdx->at(j) - dEdxRes->GetMean(pVal/0.938272))/dEdxRes->GetMean(pVal/0.938272));
					continue;
				}
			}
			else{
				
			}
			
			if(m2TOF->at(j) < 0.15 && pVal < 2.5 && dEdx->at(j) < 1.35){
				dEdxCl.Fill(ncl, (dEdx->at(j) - dEdxRes->GetMean(pVal/0.139570))/dEdxRes->GetMean(pVal/0.139570));
				continue;
			}
			
			if(pVal < 7 && dEdx->at(j) > 1.35){
				dEdxCl.Fill(ncl, (dEdx->at(j) - dEdxRes->GetMean(pVal/0.000510999))/dEdxRes->GetMean(pVal/0.000510999));
				continue;
			}
		}

	}

	for(int i = 1; i < 51; i++){
		TH1D* pr= dEdxCl.ProjectionY("py", i, i);
		
		TF1 gaus("gaus", "[0]*TMath::Gaus(x, [1], [2], 1)", pr->GetMean()-2*pr->GetRMS(), pr->GetMean()+2*pr->GetRMS());
		gaus.SetParameter(0, pr->GetEntries());
		gaus.SetParameter(1, pr->GetMean());
		gaus.SetParameter(2, pr->GetRMS());
		
		gaus.SetParLimits(0, 0, 2*pr->GetEntries() );
		gaus.SetParLimits(1, pr->GetMean()-0.5*pr->GetRMS(), 0.5*pr->GetMean()+0.7*pr->GetRMS());
		if(i < 6){
			gaus.SetParLimits(2, 0.6*pr->GetRMS(), 1.6*pr->GetRMS() );
		}
		else{
			gaus.SetParLimits(2, 0.3*pr->GetRMS(), 1.3*pr->GetRMS() );
		}
		pr->Fit(&gaus, "R");
		if(gaus.GetParameter(2) < 0.02) continue;
		if(gaus.GetParError(2)/gaus.GetParameter(2) > 0.2) continue;
		//dEdx1D.SetBinContent(i, gaus.GetParameter(2)/gaus.GetParameter(1));
		//dEdx1D.SetBinError(i, sqrt(gaus.GetParError(2)*gaus.GetParError(2) + TMath::Power(gaus.GetParError(1)*gaus.GetParameter(2)/gaus.GetParameter(1),2))/gaus.GetParameter(1));
		if(gaus.GetParError(2)/gaus.GetParameter(2) > 0.25) continue;
		dEdx1D.SetBinContent(i, gaus.GetParameter(2));
		dEdx1D.SetBinError(i, gaus.GetParError(2));
		
	}
	TCanvas c("dEdxW", "", 1600, 1000);
	c.cd()->SetMargin(0.2, 0.05, 0.15, 0.09);
	dEdx1D.SetStats(0);
	dEdx1D.SetMinimum(0);
	dEdx1D.SetMaximum(0.15);
	dEdx1D.GetXaxis()->SetTitle("n_{clusters}");
	dEdx1D.GetXaxis()->SetTitleSize(0.06);
	dEdx1D.GetXaxis()->SetLabelSize(0.05);
	
	dEdx1D.GetYaxis()->SetTitle("#sigma(dE/dx/#mu_{dE/dx})");
	dEdx1D.GetYaxis()->SetTitleOffset(1.45);
	dEdx1D.GetYaxis()->SetTitleSize(0.06);
	dEdx1D.GetYaxis()->SetLabelSize(0.05);
	dEdx1D.SetMarkerStyle(21);
	dEdx1D.Draw("P");
	//dEdxCl.Draw("colz");
	TFile out("dEdxW.root", "RECREATE");
	out.cd();
	dEdx1D.Write();
	c.SaveAs("dEdxW.pdf");
	out.Close();
	
		
}





