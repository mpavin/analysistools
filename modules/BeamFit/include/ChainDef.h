	
#ifndef CHAINDEF_H
#define CHAINDEF_H

#include <vector>
#include <string>
#include <TChain.h>
#include <TBranch.h>
#include "Functions.h"

using namespace std;

struct BeamA{
	double aX;
	double bX;
	double aY;
	double bY;
};
	
int runNumber = 0;
int eventId = 0;
BeamA beamD;
int trigger = 0;

vector<int> *trackId=NULL;
vector<int> *vtxTrackId=NULL;
vector<int> *tdcS1=NULL;
vector<int> *qdcS1=NULL;
vector<int> *tdc=NULL;
vector<int> *qdc=NULL;
vector<double> *xStart=NULL;
vector<double> *yStart=NULL;
vector<double> *xEStart=NULL;
vector<double> *yEStart=NULL;
vector<double> *distToTarg=NULL;
vector<double> *zStart=NULL;
vector<double> *xTOF1=NULL;
vector<double> *yTOF1=NULL;
vector<double> *xETOF1=NULL;
vector<double> *yETOF1=NULL;
vector<double> *zTOF1=NULL;
vector<double> *xTOF2=NULL;
vector<double> *yTOF2=NULL;
vector<double> *xETOF2=NULL;
vector<double> *yETOF2=NULL;
vector<double> *zTOF2=NULL;
	
vector<double> *p=NULL;
vector<int> *RST=NULL;
vector<int> *topology=NULL;
vector<int> *qD=NULL;
vector<double> *pE=NULL;
vector<double> *length=NULL;
vector<double> *zTPCFirst=NULL;
vector<double> *zTPCLast=NULL;
vector<double> *zVert=NULL;
vector<int> *nVTPC=NULL;
vector<int> *nGTPC=NULL;
vector<int> *nMTPC=NULL;
vector<double> *dEdx=NULL;
vector<double> *m2TOF=NULL;
vector<int> *nTOF=NULL;
vector<int> *scintId=NULL;
vector<int> *vType=NULL;
vector<double> *theta=NULL;
vector<double> *phi=NULL;


TBranch *b_aData;

void ReadData(TChain &chain, string fileName){
	vector<string> job;
	ReadInputList(fileName, job);
	
	
	for(unsigned int i = 0; i < job.size(); i++){
		chain.Add(job.at(i).c_str());
	}
	
	//b_aData = chain.GetBranch("Beam");
 	//b_aData->SetAddress(&beamD);
 	chain.SetBranchAddress("Beam", &beamD);
 	chain.SetBranchAddress("tdcS1", &tdcS1);
 	chain.SetBranchAddress("qdcS1", &qdcS1);
 	chain.SetBranchAddress("tdc", &tdc);
 	chain.SetBranchAddress("qdc", &qdc);
 	chain.SetBranchAddress("runNumber", &runNumber);
 	chain.SetBranchAddress("eventId", &eventId);
 	chain.SetBranchAddress("trackId", &trackId);
 	chain.SetBranchAddress("vtxTrackId", &vtxTrackId);
 	chain.SetBranchAddress("trigger", &trigger);
 	
 	chain.SetBranchAddress("xStart", &xStart);
 	chain.SetBranchAddress("yStart", &yStart);
 	chain.SetBranchAddress("xEStart", &xEStart);
 	chain.SetBranchAddress("yEStart", &yEStart);
 	chain.SetBranchAddress("zStart", &zStart);
	chain.SetBranchAddress("distToTarg", &distToTarg);
	
 	chain.SetBranchAddress("xTOF1", &xTOF1);
 	chain.SetBranchAddress("yTOF1", &yTOF1);
 	chain.SetBranchAddress("xETOF1", &xETOF1);
 	chain.SetBranchAddress("yETOF1", &yETOF1);
 	chain.SetBranchAddress("zTOF1", &zTOF1);

 	chain.SetBranchAddress("xTOF2", &xTOF2);
 	chain.SetBranchAddress("yTOF2", &yTOF2);
 	chain.SetBranchAddress("xETOF2", &xETOF2);
 	chain.SetBranchAddress("yETOF2", &yETOF2);
 	chain.SetBranchAddress("zTOF2", &zTOF2);
 	 
 	chain.SetBranchAddress("length", &length);
 	chain.SetBranchAddress("topology", &topology);
 	chain.SetBranchAddress("zTPCLast", &zTPCLast);
 	chain.SetBranchAddress("zTPCFirst", &zTPCFirst);
 	chain.SetBranchAddress("zVert", &zVert);
 	chain.SetBranchAddress("m2TOF", &m2TOF);
 	chain.SetBranchAddress("nTOF", &nTOF);
 	chain.SetBranchAddress("scintId", &scintId);
 	chain.SetBranchAddress("nVTPC", &nVTPC);
 	chain.SetBranchAddress("nGTPC", &nGTPC);
 	chain.SetBranchAddress("nMTPC", &nMTPC);
 	
 	chain.SetBranchAddress("dEdx", &dEdx);
 	chain.SetBranchAddress("p", &p);
 	chain.SetBranchAddress("RST", &RST);
 	chain.SetBranchAddress("pE", &pE);
 	chain.SetBranchAddress("q", &qD);
 	chain.SetBranchAddress("vType", &vType);
 	chain.SetBranchAddress("theta", &theta);
 	chain.SetBranchAddress("phi", &phi);
}
#endif





