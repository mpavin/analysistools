#include <vector>
#include <string>
#include <iostream>
#include "Functions.h"
#include <TH1D.h>
#include <TH2D.h>
#include <TFile.h>
#include <TChain.h>
#include <TMath.h>
#include <TGraphErrors.h>
#include <TRandom3.h>
using namespace std;

int main(){

	//double scales[26] = {1.000000, 1.000000, 1.000000, 1.000000, 0.983498, 0.965453, 0.943489, 0.939549, 0.943786, 0.957217, 0.954915, 0.959167, 0.949229,
	//					 0.928582, 0.888608, 0.842815, 0.784939, 0.708758, 0.628582, 0.553404, 0.485817, 0.425765, 0.368863, 0.326295, 0.301674, 0.415107};
						 
	double scales[26] = {1.000000, 1.000000, 1.000000, 1.000000, 0.992064, 0.969018, 0.943545, 0.932879, 0.930098, 0.930543, 0.925791, 0.920248, 0.903039,
						 0.871257, 0.819720, 0.772932, 0.710796, 0.634465, 0.550527, 0.478132, 0.414155, 0.358499, 0.311369, 0.268681, 0.247347, 0.336810};
						 
	/*double bpd1pos[3] = {-0.1900, -0.3075, -3620.01};
	double bpd2pos[3] = {0.7000, 0.3550, -1490.63};
	double bpd3pos[3] = {0.10, 0.5425, -663.75};*/
	
	double bpd1pos[3] = {-0.1575, -0.0428, -3619.01};
	double bpd2pos[3] = {0.1552, 0.2624, -1489.62};
	double bpd3pos[3] = {0.078, 0.8628, -664.73};
	
	
	/*double bpd1pos[3] = {-0.1575, -0.0428, -3619.01};
	double bpd2pos[3] = {0.1552, 0.2624, -1489.62};
	double bpd3pos[3] = {0.078, 0.8628, -664.73};*/
	double stripwidth = 0.2;
	double nmax = 24;
	double zT = -657.51;
	//double zT = -664.73;
	//double zT = -1416;
	vector<double> z;
	z.push_back(bpd1pos[2]);
	z.push_back(bpd2pos[2]);
	z.push_back(bpd3pos[2]);
	string inputFile = "list.txt";

	
	TChain chain("BeamData");
	

	ifstream ifs;
	string line;
	ifs.open(inputFile.c_str());
		
	if (!ifs.is_open()){
		cerr << "[ReadInputListFile] Could not open infile. Exiting..." << endl;			
		exit (101);
	}
    
	while (getline(ifs, line)){
		if(line.at(0)=='#')
			continue;
		chain.Add(line.c_str());
	}
	ifs.close();		

	unsigned int runNumber;
	double slopeX;
	double slopeY;
	double interceptX;
	double interceptY;
	int trig;
	vector<double> *signal1x = NULL;
	vector<double> *signal1y = NULL;
	vector<double> *signal2x = NULL;
	vector<double> *signal2y = NULL;
	vector<double> *signal3x = NULL;
	vector<double> *signal3y = NULL;
	
	//TBranch *b_aData = chain.GetBranch("Beam");
 	//b_aData->SetAddress(&beamD);
 	chain.SetBranchAddress("runNum", &runNumber);
 	chain.SetBranchAddress("slopeX", &slopeX);
 	chain.SetBranchAddress("slopeY", &slopeY);
 	chain.SetBranchAddress("interceptX", &interceptX);
 	chain.SetBranchAddress("interceptY", &interceptY);
 	chain.SetBranchAddress("trigger", &trig);
 	chain.SetBranchAddress("signal1x", &signal1x);
 	chain.SetBranchAddress("signal1y", &signal1y);
 	chain.SetBranchAddress("signal2x", &signal2x);
 	chain.SetBranchAddress("signal2y", &signal2y);
 	chain.SetBranchAddress("signal3x", &signal3x);
 	chain.SetBranchAddress("signal3y", &signal3y);

	
	
	TH2D beamxy("beamxy", "Target pos. w.r.t. beam: x = 0.015, y = 0.285 cm ", 400, -2, 2, 400, -2, 2);
	TH1D beamx("beamx", "", 26, -1.3, 1.3);
	TH1D beamy("beamy", "", 26, -1.3, 1.3);
	TH2D beamxys("beamxys", "", 26, -1.3, 1.3, 26, -1.3,1.3);
	TH1D beamr("beamr", "", 26, 0, 1.3);
	double brd[4] = {0, 0.6, 1.3};
	//TH1D beamr("beamr", "", 2, brd);
	TH2D divxx("divx", "", 100, -2, 2, 100, -0.001, 0.001); 
	TH2D divyy("divy", "", 100, -2, 2, 100, -0.001, 0.001); 
	TH1D chi2x("chi2x", "", 400, 0, 20);
	TH1D chi2y("chi2y", "", 400, 0, 20);
	TH1D cx1("cx1", "", 200, -0.25, 0.25);
	TH1D cx2("cx2", "", 200, -0.25, 0.25);
	TH1D cx3("cx3", "", 200, -0.25, 0.25);
	
	TH1D cy1("cy1", "", 200, -0.25, 0.25);
	TH1D cy2("cy2", "", 200, -0.25, 0.25);
	TH1D cy3("cy3", "", 200, -0.25, 0.25);
	

	TH2D resxx1("resxx1", "", 200, -2., 2., 200, -0.2, 0.2);
	TH2D resyy1("resyy1", "", 200, -2., 2., 200, -0.2, 0.2);
	TH2D resxx2("resxx2", "", 200, -2., 2., 200, -0.2, 0.2);
	TH2D resyy2("resyy2", "", 200, -2., 2., 200, -0.2, 0.2);
	TH2D resxx3("resxx3", "", 200, -2., 2., 200, -0.2, 0.2);
	TH2D resyy3("resyy3", "", 200, -2., 2., 200, -0.2, 0.2);
	
	TH1D xbpd1("xbpd1", "", 200, -2, 2);
	TH1D xbpd2("xbpd2", "", 200, -2, 2);
	TH1D xbpd3("xbpd3", "", 200, -2, 2);
	
	TH1D ybpd1("ybpd1", "", 200, -2, 2);
	TH1D ybpd2("ybpd2", "", 200, -2, 2);
	TH1D ybpd3("ybpd3", "", 200, -2, 2);


    TH1D bpd1xcluster("bpd1xcluster", "", 24, 0, 24);
    TH1D bpd1ycluster("bpd1ycluster", "", 24, 0, 24);
    TH1D bpd2xcluster("bpd2xcluster", "", 24, 0, 24);
    TH1D bpd2ycluster("bpd2ycluster", "", 24, 0, 24);
    TH1D bpd3xcluster("bpd3xcluster", "", 24, 0, 24);
    TH1D bpd3ycluster("bpd3ycluster", "", 24, 0, 24);
    
    
    TH1D clbpd1x("clbpd1x", "", 13, -6.5, 6.5);
    TH1D clbpd1y("clbpd1y", "", 13, -6.5, 6.5);
    TH1D clbpd2x("clbpd2x", "", 13, -6.5, 6.5);
    TH1D clbpd2y("clbpd2y", "", 13, -6.5, 6.5);
    TH1D clbpd3x("clbpd3x", "", 13, -6.5, 6.5);
    TH1D clbpd3y("clbpd3y", "", 13, -6.5, 6.5);
    
    double gain1x[13];
    double gain1y[13];
    double gain2x[13];
    double gain2y[13];
    double gain3x[13];
    double gain3y[13];
    double n1x[13];
    double n1y[13];
    double n2x[13];
    double n2y[13];
    double n3x[13];
    double n3y[13];
    
    for(int j = 0; j < 13; j++){
    	gain1x[j] = 0;
    	gain1y[j] = 0;
    	gain2x[j] = 0;
    	gain2y[j] = 0;
    	gain3x[j] = 0;
    	gain3y[j] = 0;
    	n1x[j] = 0;
    	n1y[j] = 0;
    	n2x[j] = 0;
    	n2y[j] = 0;
    	n3x[j] = 0;
    	n3y[j] = 0;
    }

	/*TH1D test1("test1", "", 24, -0.5, 23.5);
	TH1D test2("test2", "", 24, -0.5, 23.5);
	TH1D test3("test3", "", 24, -0.5, 23.5);*/
	TRandom3 r1;
	r1.SetSeed();
	
	int nEntries = chain.GetEntries();
	//nEntries = 1697723;
	for(int i = 0; i < nEntries; i++){
		chain.GetEntry(i);
		if(!((i+1)%100000)) cout << "Entry: " << i+1 << endl;

		if(trig == 2) cout << trig << endl;
		//if (trig == 2 ) continue;
		if(trig < 1) continue;
		int n[6] = {0,0,0,0,0,0};
		double val[6] = {0,0,0,0,0,0};
		
		vector<double> xpos;
		vector<double> xEpos;
		vector<double> ypos;
		vector<double> yEpos;
		for(int j = 0; j < 3; j++){
			xpos.push_back(0);
			xEpos.push_back(0);
			ypos.push_back(0);
			yEpos.push_back(0);
		}

		int dox = 0;
		int doy = 0;
		//***********************************************************************************

		vector<double> x1j;
		vector<double> sigx1;
		if(FindCluster("BPD1x", signal1x, x1j, sigx1, gain1x, n1x)){

			FitGauss(x1j, sigx1, xpos[0], xEpos[0]);
			//xpos[0] = (xpos[0]-(nmax+0.5)*0.5)*stripwidth + bpd1pos[0];
			xpos[0] = (xpos[0]-(nmax-1)*0.5)*stripwidth + bpd1pos[0];
			xEpos[0] *= stripwidth;
			dox++;
			bpd1xcluster.Fill(x1j.size());
		
		}
		//***********************************************************************************

		vector<double> y1j;
		vector<double> sigy1;
		if(FindCluster("BPD1y", signal1y, y1j, sigy1, gain1y, n1y)){
			FitGauss(y1j, sigy1, ypos[0], yEpos[0]);		
			//ypos[0] = (ypos[0]-(nmax+0.5)*0.5)*stripwidth + bpd1pos[1];
			ypos[0] = (ypos[0]-(nmax-1)*0.5)*stripwidth + bpd1pos[1];
			yEpos[0] *= stripwidth;
			doy++;
			bpd1ycluster.Fill(y1j.size());
		}
		//***********************************************************************************

		vector<double> x2j;
		vector<double> sigx2;
		if(FindCluster("BPD2x", signal2x, x2j, sigx2, gain2x, n2x)){
			FitGauss(x2j, sigx2, xpos[1], xEpos[1]);		
			//xpos[1] = (xpos[1]-(nmax+0.5)*0.5)*stripwidth + bpd2pos[0];
			xpos[1] = (xpos[1]-(nmax-1)*0.5)*stripwidth + bpd2pos[0];
			xEpos[1] *= stripwidth;
			dox++;
			bpd2xcluster.Fill(x2j.size());
		}
		//***********************************************************************************

		vector<double> y2j;
		vector<double> sigy2;
		if(FindCluster("BPD2y", signal2y, y2j, sigy2, gain2y, n2y)){		
			FitGauss(y2j, sigy2, ypos[1], yEpos[1]);	
			//ypos[1] = (ypos[1]-(nmax+0.5)*0.5)*stripwidth + bpd2pos[1];
			ypos[1] = (ypos[1]-(nmax-1)*0.5)*stripwidth + bpd2pos[1];
			yEpos[1] *= stripwidth;
			doy++;
			bpd2ycluster.Fill(y2j.size());
		}
		//***********************************************************************************

		vector<double> x3j;
		vector<double> sigx3;
		if(FindCluster("BPD3x", signal3x, x3j, sigx3, gain3x, n3x)){
			FitGauss(x3j, sigx3, xpos[2], xEpos[2]);	
			//xpos[2] = (xpos[2]-(nmax+0.5)*0.5)*stripwidth + bpd3pos[0];
			xpos[2] = (xpos[2]-(nmax-1)*0.5)*stripwidth + bpd3pos[0];
			xEpos[2] *= stripwidth;
			dox++;
			bpd3xcluster.Fill(x3j.size());
		}
		//***********************************************************************************

		vector<double> y3j;
		vector<double> sigy3;
		if(FindCluster("BPD3y", signal3y, y3j, sigy3, gain3y, n3y)){
			FitGauss(y3j, sigy3, ypos[2], yEpos[2]);
			//ypos[2] = (ypos[2]-(nmax+0.5)*0.5)*stripwidth + bpd3pos[1];
			ypos[2] = (ypos[2]-(nmax-1)*0.5)*stripwidth + bpd3pos[1];
			yEpos[2] *= stripwidth;
			doy++;
			bpd3ycluster.Fill(y3j.size());
		}
		//***********************************************************************************	
		if(dox == 3 && doy == 3){
			double aX = 0;
			double bX = 0;
			double aY = 0;
			double bY = 0;
		
			xbpd1.Fill(xpos.at(0));
			xbpd2.Fill(xpos.at(1));
			xbpd3.Fill(xpos.at(2));
		
			ybpd1.Fill(ypos.at(0));
			ybpd2.Fill(ypos.at(1));
			ybpd3.Fill(ypos.at(2));
			double ch1 = FitLine(z, xpos, xEpos, aX, bX);
			double ch2 = FitLine(z, ypos, yEpos, aY, bY);
			//double xval = aX*zT + bX + 0.1303;
			//double yval = aY*zT + bY - 0.0592;		
			double xval = aX*zT + bX;
			double yval = aY*zT + bY;		
			//double xval = slopeX*zT + interceptX;
			//double yval = slopeY*zT + interceptY;			
			chi2x.Fill(ch1);
			chi2y.Fill(ch2);		
			//if(sqrt((xval+0.024)*(xval+0.024) + (yval-0.065)*(yval-0.065)) > 0.8) continue;
			if(ch1 > 6.64) continue;
			if(ch2 > 6.64) continue;

			/*double r = sqrt((xval - 0.1153)*(xval - 0.1153) + (yval - 0.1258)*(yval - 0.1258));
			if(r > 1.3) continue;
			int nbin = r/0.05;
			//cout << r << " " << nbin << endl;
			if(r1.Uniform() > scales[nbin]) continue;*/
			cx1.Fill((xpos[0] - aX*bpd1pos[2] - bX));
			cx2.Fill((xpos[1] - aX*bpd2pos[2] - bX));
			cx3.Fill((xpos[2] - aX*bpd3pos[2] - bX));
		
			cy1.Fill((ypos[0] - aY*bpd1pos[2] - bY));
			cy2.Fill((ypos[1] - aY*bpd2pos[2] - bY));
			cy3.Fill((ypos[2] - aY*bpd3pos[2] - bY));
		
			resxx1.Fill(xpos[0], (xpos[0] - aX*bpd1pos[2] - bX));
			resyy1.Fill(ypos[0], (ypos[0] - aY*bpd1pos[2] - bY));
			resxx2.Fill(xpos[1], (xpos[1] - aX*bpd2pos[2] - bX));
			resyy2.Fill(ypos[1], (ypos[1] - aY*bpd2pos[2] - bY));
			resxx3.Fill(xpos[2], (xpos[2] - aX*bpd3pos[2] - bX));
			resyy3.Fill(ypos[2], (ypos[2] - aY*bpd3pos[2] - bY));
			//cout <<xEpos[0]  << " " << xEpos[1] << " " << xEpos[2] << endl;
			
			
			beamxy.Fill(xval, yval);
			beamxys.Fill(xval-0.1153, yval-0.1258);
			beamr.Fill(sqrt((xval-0.1153)*(xval-0.1153) + (yval-0.1258)*(yval-0.1258)));
			beamx.Fill(xval-0.1153);
			beamy.Fill(yval-0.1258);
			
			divxx.Fill(xval, aX);
			divyy.Fill(yval, aY);
		}
		//cout << aX << " " << bX << " " << aY << " " << bY << endl;
	}
	
	
	double val[3] = {0,0,0};
	//FindMaximum(val, 0.5,0.5, 0.005, &beamxy);
	cout << val[0] << " " << val[1] << endl;
	
	
	double x = 0;
	double y = 0;
	//FindMaximum(x, 2.6, beamx);
	//FindMaximum(y, 2.6, beamy);
	//cout << x << " " << y << endl;
	double zE[3] = {0, 0, 0};
	double bpdx[3] = {xbpd1.GetMean(), xbpd2.GetMean(), xbpd3.GetMean()};
	double bpdxE[3] = {xbpd1.GetRMS(), xbpd2.GetRMS(), xbpd3.GetRMS()};
	
	double bpdy[3] = {ybpd1.GetMean(), ybpd2.GetMean(), ybpd3.GetMean()};
	double bpdyE[3] = {ybpd1.GetRMS(), ybpd2.GetRMS(), ybpd3.GetRMS()};
	

	beamxys.Scale(1./beamxys.GetEntries()/0.1/0.1);
	//beamx.Scale(1./ beamx.GetEntries())
	//beamy.Scale(1./ beamy.GetEntries())


	
	TH2D beamxyrw("beamxyrw", "", 200, -2, 2, 200, -2, 2);

	
	
	for(int j = 0; j < 13; j++){
		if(n1x[j] > 100)
			clbpd1x.Fill(j-6, gain1x[j]/n1x[j]);
		if(n1y[j] > 100)
			clbpd1y.Fill(j-6, gain1y[j]/n1y[j]);
		if(n2x[j] > 100)
			clbpd2x.Fill(j-6, gain2x[j]/n2x[j]);
		if(n2y[j] > 100)
			clbpd2y.Fill(j-6, gain2y[j]/n2y[j]);
		if(n3x[j] > 100)
			clbpd3x.Fill(j-6, gain3x[j]/n3x[j]);
		if(n3y[j] > 100)
			clbpd3y.Fill(j-6, gain3y[j]/n3y[j]);
	}		

	
	TFile out("bpd.root" ,"RECREATE");
	out.cd();
	beamxy.Write();
	/*beamxys.Write();
	beamr.Write();*/
	/*beamx.Write();
	beamy.Write();*/
	divxx.Write();
	divyy.Write();
	//chi2x.Scale(1/(chi2x.GetEntries()*20/200));
	//chi2y.Scale(1/(chi2y.GetEntries()*20/200));
	/*chi2x.Write();
	chi2y.Write();
	cx1.Write();
	cx2.Write();
	cx3.Write();
	cy1.Write();
	cy2.Write();
	cy3.Write();
	
	resxx1.Write();
	resyy1.Write();
	resxx2.Write();
	resyy2.Write();
	resxx3.Write();
	resyy3.Write();
	
	xbpd1.Write();
	xbpd2.Write();
	xbpd3.Write();
	ybpd1.Write();
	ybpd2.Write();
	ybpd3.Write();


	bpd1xcluster.Write();
	bpd1ycluster.Write();
	bpd2xcluster.Write();
	bpd2ycluster.Write();
	bpd3xcluster.Write();
	bpd3ycluster.Write();
	//test1.Write();
	//test2.Write();
	//test3.Write();
	clbpd1x.Write();
	clbpd1y.Write();
	clbpd2x.Write();
	clbpd2y.Write();
	clbpd3x.Write();
	clbpd3y.Write();*/
	
	
	out.Close();

}





