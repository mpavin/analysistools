#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <fstream>
#include <vector>
#include <string>
#include <sstream>
#include <iostream>    
#include <iomanip> 
#include <stdlib.h>
#include <TH2D.h>
using namespace std;

struct Hit{
	int tr;
	double tof[2];
	
};

struct BeamA{
	double aX;
	double bX;
	double aY;
	double bY;
};

struct ImpactPar{
	int scint;
	double x;
	double y;
	double dx;
};

void ReadInputList(string list, vector<string> &job){

	ifstream ifs;
	string line;
	ifs.open(list.c_str());
		
	if (!ifs.is_open()){
		cerr << "Could not open infile. Exiting..." << endl;			
		exit (101);
	}
    
	while (getline(ifs, line)){
		if(line.at(0)=='#')
			continue;
		job.push_back(line);
	}
	ifs.close();	

}


std::string ConvertToString(double x, int prec){
	std::stringstream ss;
	std::string val;

	ss << std::fixed << std::setprecision(prec);
	ss << x;
	ss >> val;

	return val;
}


int FindZBin(double z){
	double zT = z + 657.41;
	
	//cout << zT << endl;
	if(zT >= 0 && zT < 18) return 0;
	else if(zT >= 18 && zT < 36) return 1;
	else if(zT >= 36 && zT < 54) return 2;
	else if(zT >= 54 && zT < 72) return 3;
	else if(zT >= 72 && zT < 89.99) return 4;
	else if(zT >= 89.99 && zT < 90.01) return 5;
	else{
		cerr << "Bad z bin!" << endl;
		return 5;
	}
}



double DoStep(double x, double y, double r1, double r2, TH1 *hist){
	unsigned int startX = hist->GetXaxis()->FindBin(x-r1);
	unsigned int stopX = hist->GetXaxis()->FindBin(x+r1);

	unsigned int startY = hist->GetYaxis()->FindBin(y-r2);
	unsigned int stopY = hist->GetYaxis()->FindBin(y+r2);

	double sum = 0;
	for(unsigned int k = startX; k <= stopX; ++k){
		for(unsigned int m = startY; m <= stopY; ++m){
			double x1 = hist->GetXaxis()->GetBinCenter(k);
			double y1 = hist->GetYaxis()->GetBinCenter(m);
			unsigned int nBin = hist->GetBin(k, m);

			//double dist = sqrt((x-x1)*(x-x1) + (y-y1)*(y-y1));
			double dist = (x-x1)*(x-x1)/r1/r1 + (y-y1)*(y-y1)/r2/r2;
			if(dist < 1){
				sum += hist->GetBinContent(nBin);

			}

		}
				
	}
	//if(sum > 0) cout << "SUM: " << sum << endl;
	return sum;

}

bool FindMaximum(double *val, double r1, double r2, double step, TH1 *hist){

	//double r = 1.3; //radius = 1.3 cm

	unsigned int nBinsX = hist->GetNbinsX();
	unsigned int nBinsY = hist->GetNbinsY();
	//double step = 0.1;
	
	double startX = hist->GetXaxis()->GetBinLowEdge(1)+r1;
	double stopX = hist->GetXaxis()->GetBinUpEdge(nBinsX)-r1;

	double startY = hist->GetYaxis()->GetBinLowEdge(1)+r2;
	double stopY = hist->GetYaxis()->GetBinUpEdge(nBinsY)-r2;
	
	//double startY = -0.3;
	//double stopY = 0.3;
	unsigned int nStepsX = (stopX - startX)/step;
	unsigned int nStepsY = (stopY - startY)/step;

	double valXY[3] = {0, 0, 0};
	double sum = 0;
	for(unsigned int i = 0; i < nStepsX; ++i){

	  double x = startX + i*step;
	  for(unsigned int j = 0; j < nStepsY; ++j){
	    double y = startY + j*step;
	    sum = DoStep(x,y,r1, r2,hist);
	    
	    if(sum > valXY[2]){
	      valXY[2] = sum;
	      valXY[0] = x;
	      valXY[1] = y;
	    }
	    if(sum == valXY[2]){
	      if((fabs(x-valXY[0]) == step)||(fabs(y-valXY[1]) == step)){
		valXY[0] += (x-valXY[0])/2;
		valXY[1] += (y-valXY[1])/2;
		}
		}
	  }
	}
	
	//cout << "F:" << valXY[0] << " " << valXY[1] << endl;
	val[0] = valXY[0];
	val[1] = valXY[1];
	val[2] = valXY[2];
	return true;
	
}
#endif
