#include <vector>
#include <string>
#include <iostream>
#include "Functions.h"
#include <TH1D.h>
#include <TH2D.h>
#include <TFile.h>
#include <TChain.h>
#include <TMath.h>


using namespace std;

int main(){

	
	string inputFile = "list.txt";

	
	TChain chain("Data");
	

	ifstream ifs;
	string line;
	ifs.open(inputFile.c_str());
		
	if (!ifs.is_open()){
		cerr << "[ReadInputListFile] Could not open infile. Exiting..." << endl;			
		exit (101);
	}
    
	while (getline(ifs, line)){
		if(line.at(0)=='#')
			continue;
		chain.Add(line.c_str());
	}
	ifs.close();		

	BeamA beamD;
	vector<double> *x = NULL;
	vector<double> *xE = NULL;
	vector<double> *y = NULL;
	vector<double> *yE = NULL;
	vector<double> *z = NULL;
	vector<double> *px = NULL;
	vector<double> *py = NULL;
	vector<double> *pz = NULL;
	vector<double> *distTarg = NULL;
	vector<double> *zStart = NULL;
	vector<double> *dEdx = NULL;
	vector<int> *qD = NULL;
	vector<int> *nVTPC1 = NULL;
	vector<int> *nVTPC2 = NULL;
	vector<int> *nGTPC = NULL;
	vector<int> *nMTPC = NULL;
	vector<int> *topology = NULL;
	vector<int> *nTOF = NULL;
	vector<double> *m2TOF = NULL;	
	vector<int> *tofScId = NULL;
	int runNumber;
	
	//TBranch *b_aData = chain.GetBranch("Beam");
 	//b_aData->SetAddress(&beamD);
 	chain.SetBranchAddress("Beam", &beamD);
	chain.SetBranchAddress("x", &x);
	chain.SetBranchAddress("xE", &xE);
	chain.SetBranchAddress("y", &y);
	chain.SetBranchAddress("yE", &yE);
	chain.SetBranchAddress("z", &z);
	chain.SetBranchAddress("px", &px);
	chain.SetBranchAddress("py", &py);
	chain.SetBranchAddress("pz", &pz);
	chain.SetBranchAddress("distTarg", &distTarg);
	chain.SetBranchAddress("zStart", &zStart);
	chain.SetBranchAddress("dEdx", &dEdx);
	chain.SetBranchAddress("q", &qD);
	chain.SetBranchAddress("nVTPC1", &nVTPC1);
	chain.SetBranchAddress("nVTPC2", &nVTPC2);
	chain.SetBranchAddress("nGTPC", &nGTPC);
	chain.SetBranchAddress("nMTPC", &nMTPC);
	chain.SetBranchAddress("topology", &topology);
	chain.SetBranchAddress("nTOF", &nTOF);
	chain.SetBranchAddress("m2TOF", &m2TOF);
	chain.SetBranchAddress("tofScId", &tofScId);
	chain.SetBranchAddress("runNumber", &runNumber);	
	

	int nEntries = chain.GetEntries();
	
	TH1D xh("xh", "", 200, -1, 1);
	TH1D yh("yh", "", 200, -1, 1);
	TH2D beam("beam", "", 200, -2, 2, 200, -2, 2);
	TH2D xy("xy", "", 200, -2, 2, 200, -2, 2);
	for(int i = 0; i < nEntries; i++){
		chain.GetEntry(i);
		
		//cout << beamD.aY << endl;
		if(!((i+1)%100000)) cout << "Entry: " << i+1 << endl;
		for(int j = 0; j < x->size(); j++){
		
			if(nGTPC->at(j) + nVTPC2->at(j) < 20) continue;
			if(nMTPC->at(j) < 30) continue;
			if(z->at(j) < -567.42) continue;
			if(xE->at(j) > 0.3) continue;
			if(yE->at(j) > 0.3) continue;
			double p = sqrt(px->at(j)*px->at(j)+py->at(j)*py->at(j)+pz->at(j)*pz->at(j));
			if(p < 30.5) continue;
			double theta = 1000*acos((beamD.aX*px->at(j)+beamD.aY*py->at(j)+pz->at(j))/(sqrt(beamD.aX*beamD.aX+beamD.aY*beamD.aY+1)*p));
			//if(theta > 0.4) continue;
			
			xh.Fill(x->at(j) - (-567.4*beamD.aX + beamD.bX));
			yh.Fill(y->at(j) - (-567.4*beamD.aY + beamD.bY));
			beam.Fill(-567.4*beamD.aX + beamD.bX, -567.4*beamD.aY + beamD.bY);
			xy.Fill(x->at(j), y->at(j));
		}
	}
	
	
	TFile out("tpos.root" ,"RECREATE");
	out.cd();
	xh.Write();
	yh.Write();
	beam.Write();
	xy.Write();
	out.Close();

}





