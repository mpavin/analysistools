#ifndef DEDXTOFFIT_H
#define DEDXTOFFIT_H


#include <iostream>
#include <vector>
#include <string>
#include <RooRealVar.h>
#include <RooPolyVar.h>
#include <RooFitResult.h>
#include <RooDataHist.h>
#include <TH2D.h>
#include <TCanvas.h>
#include "dEdxResolution.h"
#include "TOFResolution.h"
#include "Bin.h"

using namespace std;
using namespace RooFit; 


enum ParticleMult{
	eProton = 0,
	eKPlus,
	ePiPlus,
	ePositron,
	eAntiProton,
	eKMinus,
	ePiMinus,
	eElectron,
	eNPart

};
class dEdxTOFFit{

	public:
		dEdxTOFFit(dEdxResolution* dEdxRes, TOFResolution* tofRes, vector<double> *dEdx, vector<double> *m2TOF, vector<double> *charge, Bin* bin);
		//~dEdxTOFFit();
		
		void SetPrintStatus(bool print){fPrint = print;}
		void SetProtonTreshold(double prTr){fProtonTreshold = prTr;}
		void SetKaonTreshold(double kTr){fKaonTreshold = kTr;}
		void ConstrainKPlusPars(bool val){fKaonParConstraint = val;}
		void SetMinBinsdEdx(int n){fMinBinsdEdx = n;}
		void SetMinBinsTOF(int n){fMinBinsTOF = n;}
		void SetDoubleGaussTOFTreshold(double val){fDoubleGaussTOFTreshold = val;}
		void SetOutFile(string file){fOutFile = file;}
		
		bool GetPrintStatus(){return fPrint;}
		double GetProtonTreshold(){return fProtonTreshold;}
		double GetKaonTreshold(){return fKaonTreshold;}
		double GetDoubleGaussTOFTreshold(){return fDoubleGaussTOFTreshold;}
		bool AreKPlusParsConstrained(){return fKaonParConstraint;}
		bool GetMinBinsdEdx(){return fMinBinsdEdx;}
		bool GetMinBinsTOF(){return fMinBinsTOF;}
		void Fit();
		
		void GetParticleMult(ParticleMult type, double &n, double &ne);

		double GetNormChi2(){return fNormChi2;}

	private:
		
		void Fit2D(TH2D &hist, bool positive);
		void Fit1D(TH1D &hist, ParticleMult part);
		void DrawPlots(RooRealVar &x, RooRealVar &y, RooAbsPdf *model, TH2D &hist, RooDataHist &data, bool positive);
		void DrawPlots(RooRealVar &x, RooAbsPdf &model, TH1D &hist, RooDataHist &data, ParticleMult part);
		
		int OptimizeHistoBinSize(vector<double> *tofD, vector<double> *charge, int chargeCut, double min, double max);
		void FillHisto(TH2D &hist, int chargeCut, double massMax);
		void FillHisto(TH1D &hist, int chargeCut, double massMin, double massMax);
		void FillHisto(TH1D &hist, int chargeCut);
		
		dEdxResolution *fdEdxRes;
		TOFResolution *fTOFRes;
		Bin *fBin;
		
		vector<double> *fdEdx;
		vector<double> *fm2TOF;
		vector<double> *fCharge;
		
		string fOutFile;
		bool fPrint;
		double fKaonTreshold;
		double fProtonTreshold;
		double fDoubleGaussTOFTreshold;
		
		bool fKaonParConstraint;
		
		// p, K+, pi+, e+, ap, K-, pi-, e-
		double fMult[8];
		double fMultE[8];
		
		double fTOFMasses[8];
		double fTOFMassesE[8];
		double fTOFWidths[8];
		double fTOFWidthsE[8];


		double fdEdxMeans[8];
		double fdEdxMeansE[8];
		double fdEdxWidths1[8];
		double fdEdxWidths1E[8];
		double fdEdxWidths2[8];
		double fdEdxWidths2E[8];	
		double fdEdxFractions[8];
		double fdEdxFractionsE[8];	

		bool fIsFitted;
		
		double fNormChi2;
		
		int fMinBinsdEdx;
		int fMinBinsTOF;
		
};
#endif
