#!/bin/sh                                                                                                                                                                


INPUT1="../SpectrumPlotter/Results/dEdx_m2tof_1_28_17_mc_neg_all.root"
INPUT2="../SpectrumPlotter/Results/dEdx_m2tof_1_28_17_mc_neg_RST.root"
INPUT3="../SpectrumPlotter/Results/dEdx_m2tof_1_28_17_mc_neg_WST.root"
INPUT4="../SpectrumPlotter/Results/dEdx_m2tof_1_28_17_mc_neg_all_MTPC-20.root"
INPUT5="../SpectrumPlotter/Results/dEdx_m2tof_1_28_17_mc_neg_all_MTPC+20.root"
INPUT6="../SpectrumPlotter/Results/dEdx_m2tof_1_28_17_mc_neg_all_GTPC4.root"
INPUT7="../SpectrumPlotter/Results/dEdx_m2tof_1_28_17_mc_neg_all_GTPC7.root"

#bsub -q 8nh -J "FIT_1" -e /afs/cern.ch/work/m/mpavin/logs/fit_%J.err -o /afs/cern.ch/work/m/mpavin/logs/fit_%J.log run.sh ${INPUT1};
#sleep 200
#bsub -q 8nh -J "FIT_2" -e /afs/cern.ch/work/m/mpavin/logs/fit_%J.err -o /afs/cern.ch/work/m/mpavin/logs/fit_%J.log run.sh ${INPUT2};
#sleep 200
#bsub -q 8nh -J "FIT_3" -e /afs/cern.ch/work/m/mpavin/logs/fit_%J.err -o /afs/cern.ch/work/m/mpavin/logs/fit_%J.log run.sh ${INPUT3};
#sleep 200
#bsub -q 8nh -J "FIT_4" -e /afs/cern.ch/work/m/mpavin/logs/fit_%J.err -o /afs/cern.ch/work/m/mpavin/logs/fit_%J.log run.sh ${INPUT4};
#sleep 200
#bsub -q 8nh -J "FIT_5" -e /afs/cern.ch/work/m/mpavin/logs/fit_%J.err -o /afs/cern.ch/work/m/mpavin/logs/fit_%J.log run.sh ${INPUT5};
#sleep 200
#bsub -q 8nh -J "FIT_6" -e /afs/cern.ch/work/m/mpavin/logs/fit_%J.err -o /afs/cern.ch/work/m/mpavin/logs/fit_%J.log run.sh ${INPUT6};
#sleep 200
#bsub -q 8nh -J "FIT_7" -e /afs/cern.ch/work/m/mpavin/logs/fit_%J.err -o /afs/cern.ch/work/m/mpavin/logs/fit_%J.log run.sh ${INPUT7};
#sleep 200

./run.sh ${INPUT1} >log1.txt 2>log1.err
./run.sh ${INPUT2} >log1.txt 2>log1.err
./run.sh ${INPUT3} >log1.txt 2>log1.err
./run.sh ${INPUT4} >log1.txt 2>log1.err
./run.sh ${INPUT5} >log1.txt 2>log1.err
./run.sh ${INPUT6} >log1.txt 2>log1.err
./run.sh ${INPUT7} >log1.txt 2>log1.err
