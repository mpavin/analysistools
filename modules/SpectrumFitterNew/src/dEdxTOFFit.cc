#include <dEdxTOFFit.h>
#include "Functions.h"
#include <sstream>
#include <RooDataHist.h>
#include <RooGaussian.h>
#include <RooFitResult.h>
#include <RooExtendPdf.h>
#include <TMath.h>
#include <algorithm> 
 
dEdxTOFFit::dEdxTOFFit(dEdxResolution* dEdxRes, TOFResolution* tofRes, vector<double> *dEdx, vector<double> *m2TOF, vector<double> *charge, Bin* bin){
	

 	fdEdx = dEdx;
 	fm2TOF = m2TOF;
 	fCharge = charge;
 	
 	fdEdxRes = dEdxRes;
 	fTOFRes = tofRes;
 	
 	fBin = bin;
 	
 	for(int i = 0; i < 8; i++){
 		fMult[i] = 0;
 		fMultE[i] = 0;
 		fTOFMasses[i] = 0;
 		fTOFMassesE[i] = 0;
 		fdEdxMeans[i] = 0;
 		fdEdxMeansE[i] = 0;
 		fTOFWidths[i] = 0;
 		fTOFWidthsE[i] = 0;
 		fdEdxMeans[i] = 0;
 		fdEdxMeansE[i] = 0;
 		fdEdxWidths1[i] = 0;
 		fdEdxWidths1E[i] = 0;
 		fdEdxWidths2[i] = 0;
 		fdEdxWidths2E[i] = 0;

 		if(i < 4){
 			fdEdxFractions[i] = 0;
 			fdEdxFractionsE[i] = 0;
 		}
 	}
 	
 	fIsFitted = false;
 	string fOutFile = "";
 	fPrint = true;
 	
 	fKaonTreshold = 0;
 	fProtonTreshold = 0;
 	fKaonParConstraint = false;
 	fDoubleGaussTOFTreshold = 0;
 	fNormChi2 = 0;
 	
 	fMinBinsdEdx = 15;
 	fMinBinsTOF = 15;
}

/*dEdxTOFFit::~dEdxTOFFit(){

}*/


int dEdxTOFFit::OptimizeHistoBinSize(vector<double> *tofD, vector<double> *charge, int chargeCut, double min, double max){
	vector<double> data;
	
	for(int i  = 0; i < tofD->size(); i++){
		if(charge->at(i) != chargeCut) continue;
		data.push_back(tofD->at(i));
	}
	
	std::sort(data.begin(), data.end());

	
	double q1;
	double q3;
	int n = data.size();
		
	
	if(n<3) return 20;
	double nD = (double) n;
	//return (data.at(data.size()-2)-data.at(2))/sqrt(nD);
	if(n % 2 == 0){
		
		int sizeQ = n/2;
		
		if(sizeQ % 2 == 0){
			cout << "radi0a" << endl;
			int sizeQ1 = sizeQ/2;
			q1 = (data.at(sizeQ1-1) + data.at(sizeQ1))/2.;
			q3 = (data.at(sizeQ+sizeQ1-1) + data.at(sizeQ+sizeQ1))/2.;
			cout << "radi0b" << endl;
		}
		else{
			cout << "radi0c" << endl;
			int sizeQ1 = sizeQ/2;
			q1 = data.at(sizeQ1);
			q3 = data.at(sizeQ + sizeQ1);
			cout << "radi0d" << endl;
		}

	}
	else{
		int sizeQ = n/2;
		if(sizeQ % 2 == 0){
			cout << "radi0e" << endl;
			int sizeQ1 = sizeQ/2;
			q1 = (data.at(sizeQ1-1) + data.at(sizeQ1))/2.;
			q3 = (data.at(sizeQ+sizeQ1) + data.at(sizeQ+sizeQ1 + 1))/2.;
			cout << "radi0f" << endl;
		}
		else{
			cout << "radi0g" << endl;
			int sizeQ1 = sizeQ/2;
			q1 = data.at(sizeQ1);
			q3 = data.at(sizeQ + sizeQ1 + 1);
			cout << "radi0h" << endl;
		}
	}
	
	double step = 2*(q3-q1)/TMath::Power(n, 1./3.);
	//cout << step << " " << 2*(q3-q1) << " " << TMath::Power(n, 1./3.) << endl;
	return (max-min)/step;
}


void dEdxTOFFit::FillHisto(TH2D &hist, int chargeCut, double massMax){
	for(unsigned int i = 0; i < fCharge->size(); i++){
		if(fCharge->at(i) != chargeCut) continue;
		if(fm2TOF->at(i) > massMax) continue;
		
		hist.Fill(fdEdx->at(i), fm2TOF->at(i));
	}
}


void dEdxTOFFit::FillHisto(TH1D &hist, int chargeCut, double massMin, double massMax){
	for(unsigned int i = 0; i < fCharge->size(); i++){
		if(fCharge->at(i) != chargeCut) continue;
		if(fm2TOF->at(i) < massMin) continue;
		if(fm2TOF->at(i) > massMax) continue;
		
		hist.Fill(fm2TOF->at(i));
	}
}

void dEdxTOFFit::FillHisto(TH1D &hist, int chargeCut){
	for(unsigned int i = 0; i < fCharge->size(); i++){
		if(fCharge->at(i) != chargeCut) continue;

		hist.Fill(fdEdx->at(i));
	}
}

void dEdxTOFFit::GetParticleMult(ParticleMult type, double &n, double &ne){
	n = fMult[type];
	ne = fMultE[type];
}
