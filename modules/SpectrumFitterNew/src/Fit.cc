#include "dEdxTOFFit.h"
#include <RooGaussian.h>
#include <RooProdPdf.h>
#include <RooExtendPdf.h>
#include <RooAddPdf.h>
#include <RooFormulaVar.h>
#include <RooPolyVar.h>
#include <RooNovosibirsk.h>
#include <RooPlot.h>
#include <RooChi2Var.h>
#include <RooMinuit.h>
#include <RooHist.h>
#include <TImage.h>
#include <TLegend.h>
#include <TF2.h>
#include <TFile.h>
#include <TText.h>
#include <TPaveText.h>
#include <TStyle.h>
#include <RooNLLVar.h>
#include <RooMsgService.h>
#include "StrTools.h"
#include <algorithm> 
#include <iomanip> 
#include <TMath.h>
using namespace StrTools;

void dEdxTOFFit::Fit(){
	cout << fProtonTreshold << " " << fKaonTreshold << endl;
	if(fProtonTreshold < fKaonTreshold){
		cerr << __FUNCTION__ << ": Proton treshold must be higher than kaon treshold! m2TOF separation between protons and other particles is the largest one." << endl;
		cerr << __FUNCTION__ << ": Please check you configuration files and run the fitter again. Exiting ..." << endl;
		exit(100);
	}
	double pMid = fBin->GetMidValue(3);
	double pHigh = fBin->GetHighValue(3);

	
	
	double dEdxLowBorders[4] = {fdEdxRes->GetMean(pMid/0.932), fdEdxRes->GetMean(pMid/0.1396), fdEdxRes->GetMean(pMid/0.00051), fdEdxRes->GetMean(pMid/0.4937)};
	std:sort(dEdxLowBorders, dEdxLowBorders+4);
	
	double mindEdx = dEdxLowBorders[0] - 5*fdEdxRes->GetWidth(pMid/0.932);
	double maxdEdx = dEdxLowBorders[3] + 5*fdEdxRes->GetWidth(pMid/0.000511);
	if(maxdEdx > 2) maxdEdx = 2;
	double minTOF =  fTOFRes->GetElectronMean(pMid) - 3.5*fTOFRes->GetElectronWidth(pHigh);
	double maxTOF =  fTOFRes->GetProtonMean(pMid) + 3.5*fTOFRes->GetProtonWidth(pHigh);
	
	if(pHigh < fProtonTreshold){
		maxTOF = fTOFRes->GetKaonMean(pMid) + 3.5*fTOFRes->GetKaonWidth(pHigh);
	}
	if(pHigh < fKaonTreshold){
		maxTOF = fTOFRes->GetPionMean(pMid) + 3.5*fTOFRes->GetPionWidth(pHigh);
	}
	int nBinsdEdxPos = fMinBinsdEdx;
	int nBinsdEdxNeg = fMinBinsdEdx;
	
	
	int nBinsdEdxTempP = OptimizeHistoBinSize(fdEdx, fCharge, 1, mindEdx, maxdEdx);
	
	int nBinsdEdxTempN = OptimizeHistoBinSize(fdEdx, fCharge, -1, mindEdx, maxdEdx);

	if(nBinsdEdxTempP > nBinsdEdxPos) nBinsdEdxPos = nBinsdEdxTempP;
	if(nBinsdEdxTempN > nBinsdEdxNeg) nBinsdEdxNeg = nBinsdEdxTempN;
	
	
	int nBinsTOFPos = fMinBinsTOF;
	int nBinsTOFNeg = fMinBinsTOF;
	int nBinsTOFTempP = OptimizeHistoBinSize(fm2TOF, fCharge, 1, minTOF, maxTOF);
	int nBinsTOFTempN = OptimizeHistoBinSize(fm2TOF, fCharge, -1, minTOF, maxTOF);
	if(nBinsTOFTempP > nBinsTOFPos)	nBinsTOFPos = nBinsTOFTempP;
	if(nBinsTOFTempN > nBinsTOFNeg) nBinsTOFNeg = nBinsTOFTempN;

	
	if(pHigh > fProtonTreshold){

		TH2D hPos("hPos", "", nBinsdEdxNeg, mindEdx, maxdEdx, nBinsTOFNeg, minTOF, maxTOF);
		TH2D hNeg("hNeg", "", nBinsdEdxNeg, mindEdx, maxdEdx, nBinsTOFNeg, minTOF, maxTOF);
		hPos.Sumw2();
		hNeg.Sumw2();
		FillHisto(hPos, 1, 1000);
		FillHisto(hNeg, -1, 1000);
		
		if(hNeg.GetEntries()>1) Fit2D(hNeg, false);
		//if(hPos.GetEntries()>1) Fit2D(hPos, true);
		
	}
	else{
	
		TH1D hPos1DP("hPos1DP", "", nBinsTOFPos, fTOFRes->GetProtonMean(pMid) - 3.5*fTOFRes->GetProtonWidth(pHigh), 
			fTOFRes->GetProtonMean(pMid) + 3.5*fTOFRes->GetProtonWidth(pHigh));
		TH1D hPos1DAP("hPos1DAP", "", nBinsTOFNeg, fTOFRes->GetProtonMean(pMid) - 3.5*fTOFRes->GetProtonWidth(pHigh), 
			fTOFRes->GetProtonMean(pMid) + 3.5*fTOFRes->GetProtonWidth(pHigh));		
			
		FillHisto(hPos1DP, 1, fTOFRes->GetProtonMean(pMid) - 3.5*fTOFRes->GetProtonWidth(pHigh), fTOFRes->GetProtonMean(pMid) + 3.5*fTOFRes->GetProtonWidth(pHigh));
		FillHisto(hPos1DAP, -1, fTOFRes->GetProtonMean(pMid) - 3.5*fTOFRes->GetProtonWidth(pHigh), fTOFRes->GetProtonMean(pMid) + 3.5*fTOFRes->GetProtonWidth(pHigh));
		
		Fit1D(hPos1DP, eProton);
		Fit1D(hPos1DAP, eAntiProton);
		
		if(pHigh > fKaonTreshold){
			maxTOF = fTOFRes->GetKaonMean(pMid) + 3.5*fTOFRes->GetPionWidth(pHigh);
			TH2D hPos("hPos", "", nBinsdEdxPos, mindEdx, maxdEdx, nBinsTOFPos, minTOF, maxTOF);
			TH2D hNeg("hNeg", "", nBinsdEdxNeg, mindEdx, maxdEdx, nBinsTOFNeg, minTOF, maxTOF);
			

			FillHisto(hPos, 1, maxTOF);
			FillHisto(hNeg, -1, maxTOF);
			//cout << " TEST: " << hPos.GetEntries() << " " << hNeg.GetEntries() << endl;			
			if(hNeg.GetEntries()>1) Fit2D(hNeg, false);
			//if(hPos.GetEntries()>1) Fit2D(hPos, true);
						
			
		}
		else{
			TH1D hPos1DKP("hPos1DKP", "", nBinsTOFPos, fTOFRes->GetProtonMean(pMid) - 3.5*fTOFRes->GetProtonWidth(pHigh), 
				fTOFRes->GetProtonMean(pMid) + 3.5*fTOFRes->GetProtonWidth(pHigh));
			TH1D hPos1DKM("hPos1DKM", "", nBinsTOFNeg, fTOFRes->GetProtonMean(pMid) - 3.5*fTOFRes->GetProtonWidth(pHigh), 
				fTOFRes->GetProtonMean(pMid) + 3.5*fTOFRes->GetProtonWidth(pHigh));		
			
			FillHisto(hPos1DKP, 1, fTOFRes->GetKaonMean(pMid) - 3.5*fTOFRes->GetPionWidth(pHigh), fTOFRes->GetKaonMean(pMid) + 3.5*fTOFRes->GetPionWidth(pHigh));
			FillHisto(hPos1DKM, -1, fTOFRes->GetKaonMean(pMid) - 3.5*fTOFRes->GetPionWidth(pHigh), fTOFRes->GetKaonMean(pMid) + 3.5*fTOFRes->GetPionWidth(pHigh));
		
			Fit1D(hPos1DKP, eKPlus);
			Fit1D(hPos1DKM, eKMinus);	
			
			maxTOF = fTOFRes->GetPionMean(pMid) + 3.5*fTOFRes->GetPionWidth(pHigh);
			TH2D hPos("hPos", "", nBinsdEdxPos, mindEdx, maxdEdx, nBinsTOFNeg, minTOF, maxTOF);
			TH2D hNeg("hNeg", "", nBinsdEdxPos, mindEdx, maxdEdx, nBinsTOFNeg, minTOF, maxTOF);
			
			FillHisto(hNeg, -1, maxTOF);
			FillHisto(hPos, 1, maxTOF);
			
			
			
			if(hNeg.GetEntries()>1) Fit2D(hNeg, false);	
			//if(hPos.GetEntries()>1) Fit2D(hPos, true);				
		}	
	
	}
	
}

void dEdxTOFFit::Fit2D(TH2D &hist, bool positive){

	cout << "*******************************************" << endl;
	cout << "Fitting dE/dx - m2TOF distribution" << endl;
	cout << std::setprecision(2) << fBin->GetLowValue(1) << " <= z < " << fBin->GetHighValue(1) << " cm" << endl;
	cout << std::setprecision(2) << fBin->GetLowValue(2) << " <= theta < " << fBin->GetHighValue(2) << " mrad" << endl;
	cout << std::setprecision(3) << fBin->GetLowValue(3) << " <= p < " << fBin->GetHighValue(3) << " GeV/c" << endl;
	cout << "*******************************************" << endl;
	
	double pLow = fBin->GetLowValue(3);
	double pMid = fBin->GetMidValue(3);
	double pHigh = fBin->GetHighValue(3);
	
	string modelName = "m2TOF-dEdx_z" + ConvertToString(fBin->GetLowValue(1), 0) + "-" + ConvertToString(fBin->GetHighValue(1),0) + "_th" + 
	ConvertToString(fBin->GetLowValue(2), 0) + "-" + ConvertToString(fBin->GetHighValue(2), 0) + "_p" +
	ConvertToString(fBin->GetLowValue(3), 3) + "-" + ConvertToString(fBin->GetHighValue(3), 3);
	if(positive) modelName = modelName + "_pos";
	else  modelName = modelName + "_neg";
	//model
	RooAddPdf *model;	

	//dEdx and m2TOF variables
	RooRealVar x("dEdx", "dE/dx", hist.GetXaxis()->GetBinLowEdge(1),  hist.GetXaxis()->GetBinLowEdge(hist.GetNbinsX()));
 	RooRealVar y("tof", "m^{2}_{TOF}", hist.GetYaxis()->GetBinLowEdge(1),  hist.GetYaxis()->GetBinLowEdge(hist.GetNbinsY()));

	x.setBins(hist.GetNbinsX());
	y.setBins(hist.GetNbinsY());
	//data
	RooDataHist data("data", "" ,RooArgSet(x, y), &hist);
		
	//Multiplicity parameters
	double nEntries = hist.GetEntries()-hist.GetBinContent(0)-hist.GetBinContent(hist.GetNbinsX()+1);
	
 	RooRealVar ap("Ap", "A_{p}", nEntries/3, 0, nEntries);
 	RooRealVar api("Api", "A_{#pi}", nEntries/2, 0, nEntries);
 	RooRealVar ae("Ae", "A_{e}", nEntries/2.5, 0, nEntries);
 	RooRealVar aK("AK", "A_{K}", nEntries/6,0., 0.4*nEntries);
 	
 	
 	//dEdx parameters
 	//	Fractions
 	RooRealVar f1pi("f1pi", "f_{dE/dx}^{#pi}", 0.5, 0, 1);
 	RooRealVar f1e("f1e", "f_{dE/dx}^{e}", 0.5, 0, 1);
 	RooRealVar f1p("f1p", "f_{dE/dx}^{p}", 0.5, 0, 1);
	RooRealVar f1K("f1K", "f_{dE/dx}^{K}", 0.5, 0, 1);
	
	/*f1.setVal(0);
	f1e.setVal(0);
	f1.setConstant(kTRUE);
	f1e.setConstant(kTRUE);*/
	//	dE/dx mean

 	RooRealVar meandEdxp("meandEdxp", "#mu_{dE/dx}^{p}", fdEdxRes->GetMean(pMid/0.938), (1-fdEdxRes->GetProtonMeanRange())*fdEdxRes->GetMean(pMid/0.938), 
 	(1+fdEdxRes->GetProtonMeanRange())*fdEdxRes->GetMean(pMid/0.938));
 	RooRealVar meandEdxpi("meandEdxpi", "#mu_{dE/dx}^{#pi}", fdEdxRes->GetMean(pMid/0.139), (1-fdEdxRes->GetPionMeanRange())*fdEdxRes->GetMean(pMid/0.139), 
 	(1+fdEdxRes->GetPionMeanRange())*fdEdxRes->GetMean(pMid/0.139));
 	RooRealVar meandEdxe("meandEdxe", "#mu_{dE/dx}^{e}", fdEdxRes->GetMean(pMid/0.000511), (1-fdEdxRes->GetElectronMeanRange())*fdEdxRes->GetMean(pMid/0.000511), 	
 	(1+fdEdxRes->GetElectronMeanRange())*fdEdxRes->GetMean(pMid/0.000511));
 	RooRealVar meandEdxK("meandEdxK", "#mu_{dE/dx}^{K}", fdEdxRes->GetMean(pMid/0.493), (1-fdEdxRes->GetKaonMeanRange())*(fdEdxRes->GetMean(pMid/0.493)), 
 	(1+fdEdxRes->GetKaonMeanRange())*(fdEdxRes->GetMean(pMid/0.493))); 
 	
 	// 	dEdx width

	RooRealVar sigdEdxp("sigdEdxp", "#sig_{dE/dx}", 0.035*fdEdxRes->GetMean(pMid/0.938), 0.7*0.035*fdEdxRes->GetMean(pMid/0.938),	
	1.1*0.04*fdEdxRes->GetMean(pMid/0.938));
	RooRealVar sigdEdxpi("sigdEdxpi", "#sig_{dE/dx}", 0.035*fdEdxRes->GetMean(pMid/0.139), 0.7*0.035*fdEdxRes->GetMean(pMid/0.139),
	1.1*0.04*fdEdxRes->GetMean(pMid/0.139));
	RooRealVar sigdEdxK("sigdEdxK", "#sig_{dE/dx}", 0.035*fdEdxRes->GetMean(pMid/0.493), 0.7*0.035*fdEdxRes->GetMean(pMid/0.493),
	1.1*0.04*fdEdxRes->GetMean(pMid/0.493));
	RooRealVar sigdEdxe("sigdEdxe", "#sig_{dE/dx}", 0.035*fdEdxRes->GetMean(pMid/0.000511), 0.7*0.035*fdEdxRes->GetMean(pMid/0.000511),	
	1.1*0.04*fdEdxRes->GetMean(pMid/0.000511)); 
	
	RooRealVar sigdEdx2pi("sigdEdx2pi", "#sig_{2,dE/dx}^{#pi}", 0.04*fdEdxRes->GetMean(pMid/0.139)+fdEdxRes->GetDelta(pLow/0.139, pHigh/0.139), 
 	1.1*0.035*fdEdxRes->GetMean(pMid/0.139),	
 	(1+fdEdxRes->GetPionWidthRange())*(0.04*fdEdxRes->GetMean(pMid/0.139)+fdEdxRes->GetDelta(pLow/0.139, pHigh/0.139)));
	RooRealVar sigdEdx2p("sigdEdx2p", "#sig_{2,dE/dx}^{p}", 0.04*fdEdxRes->GetMean(pMid/0.938)+fdEdxRes->GetDelta(pLow/0.938, pHigh/0.938), 
 	1.1*0.035*fdEdxRes->GetMean(pMid/0.938),	
 	(1+fdEdxRes->GetProtonWidthRange())*(0.04*fdEdxRes->GetMean(pMid/0.938)+fdEdxRes->GetDelta(pLow/0.938, pHigh/0.938)));
	RooRealVar sigdEdx2K("sigdEdx2K", "#sig_{2,dE/dx}^{K}", 0.04*fdEdxRes->GetMean(pMid/0.493)+fdEdxRes->GetDelta(pLow/0.493, pHigh/0.493), 
 	1.1*0.035*fdEdxRes->GetMean(pMid/0.493),	
 	(1+fdEdxRes->GetKaonWidthRange())*(0.04*fdEdxRes->GetMean(pMid/0.493)+fdEdxRes->GetDelta(pLow/0.493, pHigh/0.493))); 	
	RooRealVar sigdEdx2e("sigdEdx2e", "#sig_{2,dE/dx}^{e}", 0.04*fdEdxRes->GetMean(pMid/0.000511)+fdEdxRes->GetDelta(pLow/0.000511, pHigh/0.000511), 
 	1.1*0.035*fdEdxRes->GetMean(pMid/0.000511),	
 	(1+fdEdxRes->GetElectronWidthRange())*(0.04*fdEdxRes->GetMean(pMid/0.000511)+fdEdxRes->GetDelta(pLow/0.000511, pHigh/0.000511))); 	
 
 	//*********************************************************************************************
 	//Temp
 	
	
	RooGaussian dEdxpG1("dEdxpG1", "(dE/dx)_{1}^{p}", x, meandEdxp, sigdEdxp);
	RooGaussian dEdxpiG1("dEdxpiG1", "(dE/dx)_{1}^{#pi}", x, meandEdxpi, sigdEdxpi);
	RooGaussian dEdxKG1("dEdxKG1", "(dE/dx)_{1}^{K}", x, meandEdxK, sigdEdxK);
	RooGaussian dEdxeG1("dEdxeG1", "(dE/dx)_{1}^{e}", x, meandEdxe, sigdEdxe);
	
	RooGaussian dEdxpG2("dEdxpG2", "(dE/dx)_{2}^{p}", x, meandEdxp, sigdEdx2p);
	RooGaussian dEdxpiG2("dEdxpiG2", "(dE/dx)_{2}^{#pi}", x, meandEdxpi, sigdEdx2pi);
	RooGaussian dEdxKG2("dEdxKG2", "(dE/dx)_{2}^{K}", x, meandEdxK, sigdEdx2K);
	RooGaussian dEdxeG2("dEdxeG2", "(dE/dx)_{2}^{e}", x, meandEdxe, sigdEdx2e);	

	RooAddPdf dEdxp("dEdxp", "(dE/dx)_{p}", RooArgList(dEdxpG1,dEdxpG2), f1pi);
	RooAddPdf dEdxpi("dEdxpi", "(dE/dx)_{pi}", RooArgList(dEdxpiG1,dEdxpiG2), f1pi);
	RooAddPdf dEdxe("dEdxe", "(dE/dx)_{e}", RooArgList(dEdxeG1,dEdxeG2), f1pi);
	RooAddPdf dEdxK("dEdxK", "(dE/dx)_{K}", RooArgList(dEdxKG1,dEdxKG2), f1pi);

	/*RooGaussian dEdxp("dEdxp", "(dE/dx)_{1}^{p}", x, meandEdxp, sigdEdxp);
	RooGaussian dEdxpi("dEdxpi", "(dE/dx)_{1}^{#pi}", x, meandEdxpi, sigdEdxpi);
	RooGaussian dEdxK("dEdxK", "(dE/dx)_{1}^{K}", x, meandEdxK, sigdEdxK);
	RooGaussian dEdxe("dEdxe", "(dE/dx)_{1}^{e}", x, meandEdxe, sigdEdxe);*/
	//tof parameters
	

 	RooRealVar meanTOFp("meanTOFp", "#mu_{TOF}^{p}", fTOFRes->GetProtonMean(pMid), (1-fTOFRes->GetProtonMeanRange())*fTOFRes->GetProtonMean(pMid), 
 	(1+fTOFRes->GetProtonMeanRange())*fTOFRes->GetProtonMean(pMid));
 	RooRealVar meanTOFpi("meanTOFpi", "#mu_{TOF}^{#pi}", fTOFRes->GetPionMean(pMid), fTOFRes->GetPionMean(pMid)-TMath::Abs(fTOFRes->GetPionMeanRange()*fTOFRes->GetPionMean(pMid)), 
 	fTOFRes->GetPionMean(pMid)+TMath::Abs(fTOFRes->GetPionMeanRange()*fTOFRes->GetPionMean(pMid)));
 	RooRealVar meanTOFe("meanTOFe", "#mu_{TOF}^{e}", fTOFRes->GetElectronMean(pMid), 
 	fTOFRes->GetElectronMean(pMid)-fTOFRes->GetElectronMeanRange()*TMath::Abs(fTOFRes->GetElectronMean(pMid)), 
 	fTOFRes->GetElectronMean(pMid)+fTOFRes->GetElectronMeanRange()*TMath::Abs(fTOFRes->GetElectronMean(pMid))); 	
 	RooRealVar meanTOFK("meanTOFK", "#mu_{TOF}^{K}", fTOFRes->GetKaonMean(pMid), (1-fTOFRes->GetKaonMeanRange())*fTOFRes->GetKaonMean(pMid), 
 	(1+fTOFRes->GetKaonMeanRange())*fTOFRes->GetKaonMean(pMid)); 
 	
 	
 	/*RooRealVar sigTOFpi("sigTOFpi", "#sig_{TOF}", fTOFRes->GetPionWidth(pMid), (1-fTOFRes->GetPionWidthRange())*fTOFRes->GetPionWidth(pLow), 
 	(1+fTOFRes->GetPionWidthRange())*fTOFRes->GetPionWidth(pHigh)); 	
 	RooRealVar sigTOFe("sigTOFe", "#sig_{TOF}", fTOFRes->GetPionWidth(pMid), (1-fTOFRes->GetElectronWidthRange())*fTOFRes->GetPionWidth(pLow), 
 	(1+fTOFRes->GetElectronWidthRange())*fTOFRes->GetPionWidth(pHigh)); 	
 	RooRealVar sigTOFp("sigTOFp", "#sig_{TOF}^{p}", fTOFRes->GetProtonWidth(pMid), (1-fTOFRes->GetProtonWidthRange())*fTOFRes->GetPionWidth(pLow), 
 	(1+fTOFRes->GetProtonWidthRange())*fTOFRes->GetProtonWidth(pHigh));
 	RooRealVar sigTOFK("sigTOFK", "#sig_{TOF}^{K}", fTOFRes->GetKaonWidth(pMid), (1-fTOFRes->GetKaonWidthRange())*fTOFRes->GetKaonWidth(pLow), 
 	(1+fTOFRes->GetKaonWidthRange())*fTOFRes->GetKaonWidth(pHigh)); 


 	RooRealVar sigTOFpi1("sigTOFpi1", "#sig_{TOF}", fTOFRes->GetPionWidth(pMid), (1-fTOFRes->GetPionWidthRange())*fTOFRes->GetPionWidth(pLow), 
 	(1+fTOFRes->GetPionWidthRange())*fTOFRes->GetPionWidth(pHigh)); 	
 	RooRealVar sigTOFe1("sigTOFe1", "#sig_{TOF}", fTOFRes->GetPionWidth(pMid), (1-fTOFRes->GetElectronWidthRange())*fTOFRes->GetPionWidth(pLow), 
 	(1+fTOFRes->GetElectronWidthRange())*fTOFRes->GetPionWidth(pHigh)); 	
 	RooRealVar sigTOFp1("sigTOFp1", "#sig_{TOF}^{p}", fTOFRes->GetProtonWidth(pMid), (1-fTOFRes->GetProtonWidthRange())*fTOFRes->GetPionWidth(pLow), 
 	(1+fTOFRes->GetProtonWidthRange())*fTOFRes->GetProtonWidth(pHigh));
 	RooRealVar sigTOFK1("sigTOFK1", "#sig_{TOF}^{K}", fTOFRes->GetKaonWidth(pMid), (1-fTOFRes->GetKaonWidthRange())*fTOFRes->GetKaonWidth(pLow), 
 	(1+fTOFRes->GetKaonWidthRange())*fTOFRes->GetKaonWidth(pHigh)); 
 	 
	//RooRealVar tailpi("tailpi", "tailpi", -0.01, -0.2, 0.2);

	RooGaussian tofp("tofp", "(tof)_{1}^{p}", y, meanTOFp, sigTOFp);
	RooGaussian tofpi("tofpi", "(tof)_{1}^{#pi}", y, meanTOFpi, sigTOFpi);
	RooGaussian tofK("tofK", "(tof)_{1}^{K}", y, meanTOFK, sigTOFK);
	RooGaussian tofe("tofe", "(tof)_{1}^{e}", y, meanTOFe, sigTOFe);*/

	RooRealVar f2("f2", "f2", 0.5, 0, 1);
	RooRealVar sigTOFpi("sigTOFpi", "#sig_{TOF}", (1-0.5*fTOFRes->GetPionWidthRange())*fTOFRes->GetPionWidth(pMid), (1-fTOFRes->GetPionWidthRange())*fTOFRes->GetPionWidth(pLow), 
 	fTOFRes->GetPionWidth(pHigh)); 	
 	RooRealVar sigTOFe("sigTOFe", "#sig_{TOF}", (1-0.5*fTOFRes->GetElectronWidthRange())*fTOFRes->GetPionWidth(pMid), (1-fTOFRes->GetElectronWidthRange())*fTOFRes->GetPionWidth(pLow), 
 	fTOFRes->GetPionWidth(pHigh)); 	
 	RooRealVar sigTOFp("sigTOFp", "#sig_{TOF}^{p}", (1-0.5*fTOFRes->GetProtonWidthRange())*fTOFRes->GetProtonWidth(pMid), (1-fTOFRes->GetProtonWidthRange())*fTOFRes->GetPionWidth(pLow), 
 	fTOFRes->GetProtonWidth(pHigh));
 	RooRealVar sigTOFK("sigTOFK", "#sig_{TOF}^{K}", (1-0.5*fTOFRes->GetKaonWidthRange())*fTOFRes->GetKaonWidth(pMid), (1-fTOFRes->GetKaonWidthRange())*fTOFRes->GetKaonWidth(pLow), 
 	fTOFRes->GetKaonWidth(pHigh)); 


 	RooRealVar sigTOFpi1("sigTOFpi1", "#sig_{TOF}", (1+0.5*fTOFRes->GetPionWidthRange())*fTOFRes->GetPionWidth(pMid), fTOFRes->GetPionWidth(pLow), 
 	(1+fTOFRes->GetPionWidthRange())*fTOFRes->GetPionWidth(pHigh)); 	
 	RooRealVar sigTOFe1("sigTOFe1", "#sig_{TOF}", (1+0.5*fTOFRes->GetElectronWidthRange())*fTOFRes->GetPionWidth(pMid), fTOFRes->GetPionWidth(pLow), 
 	(1+fTOFRes->GetElectronWidthRange())*fTOFRes->GetPionWidth(pHigh)); 	
 	RooRealVar sigTOFp1("sigTOFp1", "#sig_{TOF}^{p}", (1+0.5*fTOFRes->GetProtonWidthRange())*fTOFRes->GetProtonWidth(pMid), fTOFRes->GetPionWidth(pLow), 
 	(1+fTOFRes->GetProtonWidthRange())*fTOFRes->GetProtonWidth(pHigh));
 	RooRealVar sigTOFK1("sigTOFK1", "#sig_{TOF}^{K}", (1+0.5*fTOFRes->GetKaonWidthRange())*fTOFRes->GetKaonWidth(pMid), fTOFRes->GetKaonWidth(pLow), 
 	(1+fTOFRes->GetKaonWidthRange())*fTOFRes->GetKaonWidth(pHigh)); 
 	 
	

	RooGaussian tofp1("tofp1", "(tof)_{1}^{p}", y, meanTOFp, sigTOFp);
	RooGaussian tofpi1("tofpi1", "(tof)_{1}^{#pi}", y, meanTOFpi, sigTOFpi);
	RooGaussian tofK1("tofK1", "(tof)_{1}^{K}", y, meanTOFK, sigTOFK);
	RooGaussian tofe1("tofe1", "(tof)_{1}^{e}", y, meanTOFe, sigTOFe);

	RooGaussian tofp2("tofp2", "(tof)_{1}^{p}", y, meanTOFp, sigTOFp1);
	RooGaussian tofpi2("tofpi2", "(tof)_{1}^{#pi}", y, meanTOFpi, sigTOFpi1);
	RooGaussian tofK2("tofK2", "(tof)_{1}^{K}", y, meanTOFK, sigTOFK1);
	RooGaussian tofe2("tofe2", "(tof)_{1}^{e}", y, meanTOFe, sigTOFe1);			 	 	

	RooAddPdf tofp("tofp", "(tof)_{1}^{p}", RooArgList(tofp1, tofp2), f2);
	RooAddPdf tofpi("tofpi", "(tof)_{1}^{pi}", RooArgList(tofpi1, tofpi2), f2);
	RooAddPdf tofK("tofK", "(tof)_{1}^{K}", RooArgList(tofK1, tofK2), f2);
	RooAddPdf tofe("tofe", "(tof)_{1}^{e}", RooArgList(tofe1, tofe2), f2);	 	
	
	if(pHigh > fDoubleGaussTOFTreshold){
		sigTOFpi.setVal(fTOFRes->GetPionWidth(pMid));
		sigTOFpi.setRange(fTOFRes->GetPionWidth(pMid)*(1-fTOFRes->GetPionWidthRange()), fTOFRes->GetPionWidth(pMid)*(1+fTOFRes->GetPionWidthRange()));
		sigTOFpi1.setConstant(kTRUE);
		
		sigTOFp.setVal(fTOFRes->GetProtonWidth(pMid));
		sigTOFp.setRange(fTOFRes->GetProtonWidth(pMid)*(1-fTOFRes->GetProtonWidthRange()), fTOFRes->GetProtonWidth(pMid)*(1+fTOFRes->GetProtonWidthRange()));
		sigTOFp1.setConstant(kTRUE);
		
		sigTOFK.setVal(fTOFRes->GetKaonWidth(pMid));
		sigTOFK.setRange(fTOFRes->GetKaonWidth(pMid)*(1-fTOFRes->GetKaonWidthRange()), fTOFRes->GetKaonWidth(pMid)*(1+fTOFRes->GetKaonWidthRange()));
		sigTOFK1.setConstant(kTRUE);
		
		sigTOFe.setVal(fTOFRes->GetPionWidth(pMid));
		sigTOFe.setRange(fTOFRes->GetPionWidth(pMid)*(1-fTOFRes->GetElectronWidthRange()), fTOFRes->GetPionWidth(pMid)*(1+fTOFRes->GetElectronWidthRange()));
		sigTOFe1.setConstant(kTRUE);
		
		f2.setVal(1);
		f2.setConstant(kTRUE);
	}	
		
	RooProdPdf up("up", "(dE/dx)_{p}#dot TOF_{p}", dEdxp, tofp);
	RooProdPdf upi("upi", "(dE/dx)_{#pi}#dot TOF_{#pi}", dEdxpi, tofpi);
	RooProdPdf ue("ue", "(dE/dx)_{e}#dot TOF_{e}", dEdxe, tofe);
	RooProdPdf uK("uK", "(dE/dx)_{K}#dot TOF_{K}", dEdxK, tofK);
	
	RooProdPdf up1("up1", "(dE/dx)_{p}#dot TOF_{p}", dEdxp, tofp1);
	RooProdPdf upi1("upi1", "(dE/dx)_{#pi}#dot TOF_{#pi}", dEdxpi, tofpi1);
	RooProdPdf ue1("ue1", "(dE/dx)_{e}#dot TOF_{e}", dEdxe, tofe1);
	RooProdPdf uK1("uK1", "(dE/dx)_{K}#dot TOF_{K}", dEdxK, tofK1);

		
	RooExtendPdf *p;
	RooExtendPdf *pi;
	RooExtendPdf *e;
	RooExtendPdf *K;

	if(	pHigh > fDoubleGaussTOFTreshold){
		p = new RooExtendPdf("p", "p", up1, ap);
		pi = new RooExtendPdf("pi", "pi", upi1, api);
		K = new RooExtendPdf("K", "K", uK1, aK);
		e = new RooExtendPdf("e", "e", ue1, ae);
	}
	else{
		p = new RooExtendPdf("p", "p", up, ap);
		pi = new RooExtendPdf("pi", "pi", upi, api);
		K = new RooExtendPdf("K", "K", uK, aK);
		e = new RooExtendPdf("e", "e", ue, ae);	
	}
	
	if(positive && fKaonParConstraint){
		meanTOFK.setVal(fTOFMasses[5]);
		meanTOFK.setConstant(kTRUE);
		
		sigTOFK.setVal(fTOFWidths[5]);	
		sigTOFK.setConstant(kTRUE);		
		
		sigdEdxK.setVal(fdEdxWidths1[5]);	
		sigdEdxK.setConstant(kTRUE);

		meandEdxK.setVal(fdEdxMeans[5]);	
		meandEdxK.setConstant(kTRUE);				
	}

	if(pHigh > fProtonTreshold){
		model = new RooAddPdf(modelName.c_str(), "", RooArgList(*pi, *K, *p, *e));
		//model = new RooAddPdf(modelName.c_str(), "", RooArgList(upi, uK, up, ue), RooArgList(api, aK, ap, ae));
	}
	else{
		if(pHigh > fKaonTreshold){
			model = new RooAddPdf(modelName.c_str(), "", RooArgList(*pi, *K, *e));
			//model = new RooAddPdf(modelName.c_str(), "", RooArgList(upi, uK, ue), RooArgList(api, aK, ae));
		}
		else{
			model = new RooAddPdf(modelName.c_str(), "", RooArgList(*pi, *e));
			//model = new RooAddPdf(modelName.c_str(), "", RooArgList(upi, ue), RooArgList(api, ae));
		}
	}

	RooFitResult *result;
	if(fPrint){
		result = model->fitTo(data, Extended(kTRUE), Save());
		
	}
	else{
		result = model->fitTo(data,Save(), Extended(kTRUE), PrintLevel(-1), Verbose(kFALSE));
	}
	result->Print();
	
	if(positive){
	
		if(pHigh > fProtonTreshold){
			fMult[0] = ap.getVal();
			fMultE[0] = ap.getError();
			fTOFMasses[0] = meanTOFp.getVal();
			fTOFMassesE[0] = meanTOFp.getError();
			fTOFWidths[0] = sigTOFp.getVal();
			fTOFWidthsE[0] = sigTOFp.getError();
			fdEdxMeans[0] = meandEdxp.getVal();
			fdEdxMeansE[0] = meandEdxp.getError();
			fdEdxWidths1[0] = sigdEdxp.getVal();
			fdEdxWidths1E[0] = sigdEdxp.getError();
			fdEdxWidths2[0] = sigdEdx2p.getVal();
			fdEdxWidths2E[0] = sigdEdx2p.getError();
		}
		if(pHigh > fKaonTreshold){
			fMult[1] = aK.getVal();
			fMultE[1] = aK.getError();
			fTOFMasses[1] = meanTOFK.getVal();
			fTOFMassesE[1] = meanTOFK.getError();
			fTOFWidths[1] = sigTOFK.getVal();
			fTOFWidthsE[1] = sigTOFK.getError();
			fdEdxMeans[1] = meandEdxK.getVal();
			fdEdxMeansE[1] = meandEdxK.getError();
			fdEdxWidths1[1] = sigdEdxK.getVal();
			fdEdxWidths1E[1] = sigdEdxK.getError();
			fdEdxWidths2[1] = sigdEdx2K.getVal();
			fdEdxWidths2E[1] = sigdEdx2K.getError();
		}

		fMult[2] = api.getVal();
		fMultE[2] = api.getError();
		fMult[3] = ae.getVal();
		fMultE[3] = ae.getError();		
		fTOFMasses[2] = meanTOFpi.getVal();
		fTOFMassesE[2] = meanTOFpi.getError();
		fTOFMasses[3] = meanTOFe.getVal();
		fTOFMassesE[3] = meanTOFe.getError();

		fTOFWidths[2] = sigTOFpi.getVal();
		fTOFWidthsE[2] = sigTOFpi.getError();		
		fTOFWidths[3] = sigTOFe.getVal();
		fTOFWidthsE[3] = sigTOFe.getError();


		fdEdxMeans[2] = meandEdxpi.getVal();
		fdEdxMeansE[2] = meandEdxpi.getError();
		fdEdxMeans[3] = meandEdxe.getVal();
		fdEdxMeansE[3] = meandEdxe.getError();
		
		fdEdxWidths1[2] = sigdEdxpi.getVal();
		fdEdxWidths1E[2] = sigdEdxpi.getError();
		fdEdxWidths2[2] = sigdEdx2pi.getVal();
		fdEdxWidths2E[2] = sigdEdx2pi.getError();
		fdEdxWidths1[3] = sigdEdxe.getVal();
		fdEdxWidths1E[3] = sigdEdxe.getError();
		fdEdxWidths2[3] = sigdEdx2e.getVal();
		fdEdxWidths2E[3] = sigdEdx2e.getError();

		fdEdxFractions[0] = f1pi.getVal();
		fdEdxFractionsE[0] = f1pi.getError();					
		fdEdxFractions[1] = f1e.getVal();
		fdEdxFractionsE[1] = f1e.getError();
	}
	else{
	
		if(pHigh > fProtonTreshold){
			fMult[4] = ap.getVal();
			fMultE[4] = ap.getError();
			fTOFMasses[4] = meanTOFp.getVal();
			fTOFMassesE[4] = meanTOFp.getError();
			fTOFWidths[4] = sigTOFp.getVal();
			fTOFWidthsE[4] = sigTOFp.getError();
			fdEdxMeans[4] = meandEdxp.getVal();
			fdEdxMeansE[4] = meandEdxp.getError();
			fdEdxWidths1[4] = sigdEdxp.getVal();
			fdEdxWidths1E[4] = sigdEdxp.getError();
			fdEdxWidths2[4] = sigdEdx2p.getVal();
			fdEdxWidths2E[4] = sigdEdx2p.getError();
		}
		if(pHigh > fKaonTreshold){
			fMult[5] = aK.getVal();
			fMultE[5] = aK.getError();
			fTOFMasses[5] = meanTOFK.getVal();
			fTOFMassesE[5] = meanTOFK.getError();
			fTOFWidths[5] = sigTOFK.getVal();
			fTOFWidthsE[5] = sigTOFK.getError();
			fdEdxMeans[5] = meandEdxK.getVal();
			fdEdxMeansE[5] = meandEdxK.getError();
			fdEdxWidths1[5] = sigdEdxK.getVal();
			fdEdxWidths1E[5] = sigdEdxK.getError();
			fdEdxWidths2[5] = sigdEdx2K.getVal();
			fdEdxWidths2E[5] = sigdEdx2K.getError();
		}
		
		fMult[6] = api.getVal();
		fMultE[6] = api.getError();
		fMult[7] = ae.getVal();
		fMultE[7] = ae.getError();
		fTOFMasses[6] = meanTOFpi.getVal();
		fTOFMassesE[6] = meanTOFpi.getError();
		fTOFMasses[7] = meanTOFe.getVal();
		fTOFMassesE[7] = meanTOFe.getError();

		fTOFWidths[6] = sigTOFpi.getVal();
		fTOFWidthsE[6] = sigTOFpi.getError();		
		fTOFWidths[7] = sigTOFe.getVal();
		fTOFWidthsE[7] = sigTOFe.getError();


		fdEdxMeans[6] = meandEdxpi.getVal();
		fdEdxMeansE[6] = meandEdxpi.getError();
		fdEdxMeans[7] = meandEdxe.getVal();
		fdEdxMeansE[7] = meandEdxe.getError();
		
		fdEdxWidths1[6] = sigdEdxpi.getVal();
		fdEdxWidths1E[6] = sigdEdxpi.getError();
		fdEdxWidths2[6] = sigdEdx2pi.getVal();
		fdEdxWidths2E[6] = sigdEdx2pi.getError();
		fdEdxWidths1[7] = sigdEdxe.getVal();
		fdEdxWidths1E[7] = sigdEdxe.getError();
		fdEdxWidths2[7] = sigdEdx2e.getVal();
		fdEdxWidths2E[7] = sigdEdx2e.getError();

		fdEdxFractions[2] = f1pi.getVal();
		fdEdxFractionsE[2] = f1pi.getError();					
		fdEdxFractions[3] = f1e.getVal();
		fdEdxFractionsE[3] = f1e.getError();
	}
	
	if(!fPrint){
		TFile file(fOutFile.c_str(), "UPDATE");
		file.cd();
		result->Write();
		file.Close();
	}

	DrawPlots(x, y, model, hist, data, positive);
	
	delete p;
	p = NULL;
	
	delete pi;
	pi = NULL;
	
	delete e;
	e = NULL;
	
	delete K;
	K = NULL;
	
	delete model;
	model = NULL;
}

void dEdxTOFFit::Fit1D(TH1D &hist, ParticleMult part){

	cout << "*******************************************" << endl;
	cout << "Fitting m2TOF distribution" << endl;
	cout << std::setprecision(2) << fBin->GetLowValue(1) << " <= z < " << fBin->GetHighValue(1) << " cm" << endl;
	cout << std::setprecision(2) << fBin->GetLowValue(2) << " <= theta < " << fBin->GetHighValue(2) << " mrad" << endl;
	cout << std::setprecision(3) << fBin->GetLowValue(3) << " <= p < " << fBin->GetHighValue(3) << " GeV/c" << endl;
	cout << "*******************************************" << endl;
	
	double pLow = fBin->GetLowValue(3);
	double pMid = fBin->GetMidValue(3);
	double pHigh = fBin->GetHighValue(3);
	
	string modelName = "m2TOF" + ConvertToString(fBin->GetLowValue(1), 0) + "-" + ConvertToString(fBin->GetHighValue(1),0) + "_th" + 
	ConvertToString(fBin->GetLowValue(2), 0) + "-" + ConvertToString(fBin->GetHighValue(2), 0) + "_p" +
	ConvertToString(fBin->GetLowValue(3), 3) + "-" + ConvertToString(fBin->GetHighValue(3), 3);
	
	double nEntries = hist.GetEntries()-hist.GetBinContent(0)-hist.GetBinContent(hist.GetNbinsX()+1);
	if(nEntries < 1) return;
 	RooRealVar ap("Ap", "A_{p}", nEntries/3, 0, nEntries);
 	RooRealVar aK("AK", "A_{K}", nEntries/6,0., 0.5*nEntries);

	//m2TOF variable
 	RooRealVar x("tof", "m^{2}_{TOF}", hist.GetXaxis()->GetBinLowEdge(1),  hist.GetXaxis()->GetBinLowEdge(hist.GetNbinsX()));

	//data
	RooDataHist data("data", "" , x, &hist);

 	RooRealVar meanTOFp("meanTOFp", "#mu_{TOF}^{p}", fTOFRes->GetProtonMean(pMid), (1-fTOFRes->GetProtonMeanRange())*fTOFRes->GetProtonMean(pMid), 
 	(1+fTOFRes->GetProtonMeanRange())*fTOFRes->GetProtonMean(pMid));	
 	RooRealVar meanTOFK("meanTOFK", "#mu_{TOF}^{K}", fTOFRes->GetKaonMean(pMid), (1-fTOFRes->GetKaonMeanRange())*fTOFRes->GetKaonMean(pMid), 
 	(1+fTOFRes->GetKaonMeanRange())*fTOFRes->GetKaonMean(pMid)); 
 		
 	RooRealVar sigTOFp("sigTOFp", "#sig_{TOF}^{p}", fTOFRes->GetProtonWidth(pMid), (1-fTOFRes->GetProtonWidthRange())*fTOFRes->GetPionWidth(pLow), 
 	(1+fTOFRes->GetProtonWidthRange())*fTOFRes->GetProtonWidth(pHigh));
 	RooRealVar sigTOFK("sigTOFK", "#sig_{TOF}^{K}", fTOFRes->GetKaonWidth(pMid), (1-fTOFRes->GetKaonWidthRange())*fTOFRes->GetKaonWidth(pLow), 
 	(1+fTOFRes->GetKaonWidthRange())*fTOFRes->GetKaonWidth(pHigh)); 
 


	RooGaussian tofp("tofp", "(tof)_{1}^{p}", x, meanTOFp, sigTOFp);
	RooGaussian tofK("tofK", "(tof)_{1}^{K}", x, meanTOFK, sigTOFK);

	RooExtendPdf modelp("p", "p", tofp, ap);
	RooExtendPdf modelK("K", "K", tofK, aK);	
			
	RooFitResult *result; 	 			
	if(part == eProton || part == eAntiProton){
		if(fPrint){
			result = modelp.fitTo(data,Save());
		}
		else{
			result = modelp.fitTo(data,Save(), PrintLevel(-1), Verbose(kFALSE));
		}
		if(part == eProton){
			fMult[0] = ap.getVal();
			//fMultE[0] = ap.getError();
			fMultE[0] = sqrt(ap.getVal());
			fTOFMasses[0] = meanTOFp.getVal();
			fTOFMassesE[0] = meanTOFp.getError();
			fTOFWidths[0] = sigTOFp.getVal();
			fTOFWidthsE[0] = sigTOFp.getError();
					
		}
		else if(part == eAntiProton){
			fMult[4] = ap.getVal();
			//fMultE[4] = ap.getError();
			fMultE[4] = sqrt(ap.getVal());
			fTOFMasses[4] = meanTOFp.getVal();
			fTOFMassesE[4] = meanTOFp.getError();
			fTOFWidths[4] = sigTOFp.getVal();
			fTOFWidthsE[4] = sigTOFp.getError();
					
		}
		DrawPlots(x, modelp, hist, data, part);		
	}
	else if(part == eKPlus || part == eKMinus){
		if(fPrint){
			result = modelK.fitTo(data,Save());	
		}
		else{
			result = modelK.fitTo(data,Save(), PrintLevel(-1), Verbose(kFALSE));
		}
		if(part == eKPlus){
			fMult[1] = aK.getVal();
			fMultE[1] = aK.getError();
			fTOFMasses[1] = meanTOFK.getVal();
			fTOFMassesE[1] = meanTOFK.getError();
			fTOFWidths[1] = sigTOFK.getVal();
			fTOFWidthsE[1] = sigTOFK.getError();
	
		}
		else if(part == eKMinus){
			fMult[5] = aK.getVal();
			fMultE[5] = aK.getError();
			fTOFMasses[5] = meanTOFK.getVal();
			fTOFMassesE[5] = meanTOFK.getError();
			fTOFWidths[5] = sigTOFK.getVal();
			fTOFWidthsE[5] = sigTOFK.getError();
	
		}	
		DrawPlots(x, modelK, hist, data, part);	
	}
	
	if(!fPrint){
		TFile file(fOutFile.c_str(), "UPDATE");
		file.cd();
		result->Write();
		file.Close();
	}


}


void dEdxTOFFit::DrawPlots(RooRealVar &x, RooRealVar &y, RooAbsPdf *model, TH2D &hist, RooDataHist &data, bool positive){
	//gStyle->SetTitleSize(0.20, "t");
	double pHigh = fBin->GetHighValue(3);
	RooPlot* framex = x.frame();
	RooPlot* framey = y.frame();
	
	RooPlot* framex1 = x.frame();
	RooPlot* framey1 = y.frame();
	
	
	string cName = "plot_" + ConvertToString(fBin->GetLowValue(1), 0) + "-" + ConvertToString(fBin->GetHighValue(1),0) + "_th" + 
	ConvertToString(fBin->GetLowValue(2), 0) + "-" + ConvertToString(fBin->GetHighValue(2), 0) + "_p" +
	ConvertToString(fBin->GetLowValue(3), 3) + "-" + ConvertToString(fBin->GetHighValue(3), 3);
	
	if(positive) cName = cName + "_pos";
	else cName = cName + "_neg";
	
	string title;
	if(fBin->GetHighValue(1) < 90){
		title = ConvertToString(fBin->GetLowValue(1), 0) + " #leq z < " + ConvertToString(fBin->GetHighValue(1), 0) + " cm, " +
		ConvertToString(fBin->GetLowValue(2), 0) + " #leq #theta < " + ConvertToString(fBin->GetHighValue(2), 0) + " mrad, " +
		ConvertToString(fBin->GetLowValue(3), 2) + " #leq p < " + ConvertToString(fBin->GetHighValue(3), 2) + " GeV/c";
	}
	else{
		title = "z = " + ConvertToString(fBin->GetHighValue(1), 0) + " cm, " +
		ConvertToString(fBin->GetLowValue(2), 0) + " #leq #theta < " + ConvertToString(fBin->GetHighValue(2), 0) + " mrad, " +
		ConvertToString(fBin->GetLowValue(3), 2) + " #leq p < " + ConvertToString(fBin->GetHighValue(3), 2) + " GeV/c";
	}

	TH2D* hh_pdf = (TH2D*) model->createHistogram("dEdx,tof", hist.GetNbinsX(), hist.GetNbinsY());


	TCanvas *c = new TCanvas(cName.c_str(), "", 2000, 1250);
	TPad *pad0 = new TPad("pad0","",0.0,0.9,0.5,1.0);
	TPad *pad1 = new TPad("pad1","",0.0,0.25,0.5,0.9);
	TPad *pad2 = new TPad("pad2","",0.5,0.65,1.0,1.0);
	TPad *pad3 = new TPad("pad3","",0.5,0.50,1.0,0.65);
	TPad *pad4 = new TPad("pad4","",0.0,0.0,0.5,0.25);
	TPad *pad5 = new TPad("pad5","",0.5,0.15,1.0,0.50);
	TPad *pad6 = new TPad("pad6","",0.5,0.0,1.0,0.15);

	c->cd();
	pad0->Draw();
	pad1->Draw();
	pad2->Draw();
	pad3->Draw();
	pad4->Draw();
	pad5->Draw();
	pad6->Draw();
	

	//hist.SetTitle(title.c_str());
	hist.GetXaxis()->SetTitle("dE/dx [mip]");
	hist.GetXaxis()->SetTitleSize(0.06);
	hist.GetXaxis()->SetLabelSize(0.06);
	hist.GetYaxis()->SetTitle("m^{2}_{TOF} [GeV^{2}/c^{4}]");
	hist.GetYaxis()->SetTitleSize(0.06);
	hist.GetYaxis()->SetTitleOffset(1.1);
	hist.GetYaxis()->SetLabelSize(0.06);
	hist.SetStats(kFALSE);
	
	hh_pdf->SetLineColor(kRed);
	hh_pdf->SetLineWidth(2);

	framex->SetTitle("");
	//framex->GetXaxis()->SetTitle("dE/dx [mip]");
	framex->GetXaxis()->SetTitleSize(0);
	framex->GetXaxis()->SetLabelSize(0);
	framex->GetYaxis()->SetTitle("N_{tracks}");
	framex->GetYaxis()->SetTitleSize(0.08);
	framex->GetYaxis()->SetLabelSize(0.08);
	framex->GetYaxis()->SetTitleOffset(0.60);
	framex->SetMinimum(-1);
	
	framey->SetTitle("");
	//framey->GetXaxis()->SetTitle("m^{2}_{TOF} [GeV^{2}/c^{4}]");
	framey->GetXaxis()->SetTitleSize(0);
	framey->GetXaxis()->SetLabelSize(0);
	framey->GetYaxis()->SetTitle("N_{tracks}");
	framey->GetYaxis()->SetTitleSize(0.08);
	framey->GetYaxis()->SetLabelSize(0.08);
	framey->GetYaxis()->SetTitleOffset(0.60);
	framey->SetMinimum(-1);
	
	pad0->SetMargin(0.15, 0.05, 0.0, 0.0);
	pad0->cd();	
	TPaveText *ptTitle = new TPaveText(0.05,0.15,1.0,0.85);
	ptTitle->AddText(title.c_str());
   	ptTitle->SetBorderSize(1);
   	ptTitle->SetFillColor(0);
	ptTitle->Draw();
	
	pad1->SetMargin(0.15, 0.05, 0.15, 0.0);
	pad1->cd();
	pad1->SetLogz();
	hist.Draw();
	hh_pdf->Draw("CONT2same");

	pad2->cd();
	pad2->SetMargin(0.10, 0.10, 0.02, 0.05);
	data.plotOn(framey,RooFit::Name("Data"));
	model->plotOn(framey,RooFit::Name(model->GetName()),RooFit::Name("Model"), LineStyle(kDashed), LineColor(kRed)) ;
	if(pHigh < fProtonTreshold){
		if(pHigh < fKaonTreshold){
			cout << "Chi2/ndf(m2TOF) = " << framey->chiSquare() << endl;
		}
		else cout << "Chi2/ndf(m2TOF) = " << framey->chiSquare() << endl;	
	}
	else cout << "Chi2/ndf(m2TOF) = " << framey->chiSquare() << endl;
	RooHist* hresidy = framey->residHist();
	if(pHigh > fProtonTreshold) model->plotOn(framey, Components("p"),RooFit::Name("pplot"), LineColor(kBlue+2), FillColor(kBlue+2),DrawOption("F")) ;
	model->plotOn(framey, Components("pi"), LineColor(kOrange),RooFit::Name("piplot"), FillColor(kOrange),DrawOption("F")) ;
	model->plotOn(framey, Components("e"), LineColor(kGreen+3),RooFit::Name("eplot"), FillColor(kGreen+3),DrawOption("F")) ;
	if(pHigh > fKaonTreshold) model->plotOn(framey, Components("K"),RooFit::Name("Kplot"), LineColor(kViolet),  FillColor(kViolet),DrawOption("F")) ; 	
	framey->Draw();
	TLegend *leg0= new TLegend(0.75,0.6,0.95,0.9);	
	
	leg0->AddEntry(framey->findObject("Data"),"Data","lep");
	leg0->AddEntry(framey->findObject("Model"),"Model","L");
	leg0->Draw("same");

	
	pad3->cd();
	pad3->SetMargin(0.10, 0.10, 0.40, 0.0);
	hresidy->SetTitle("");
	hresidy->GetXaxis()->SetTitle("m^{2}_{TOF} [GeV^{2}/c^{4}]");
	hresidy->GetXaxis()->SetTitleSize(0.18);
	hresidy->GetXaxis()->SetLabelSize(0.18);
	hresidy->GetXaxis()->SetRangeUser(hist.GetYaxis()->GetBinLowEdge(1), hist.GetYaxis()->GetBinUpEdge(hist.GetYaxis()->GetNbins()-1));
	hresidy->GetYaxis()->SetTitle("Residual");
	hresidy->GetYaxis()->SetTitleSize(0.1);
	hresidy->GetYaxis()->SetLabelSize(0.1);
	hresidy->GetYaxis()->SetNdivisions(4,0,0, kTRUE);
	hresidy->SetMinimum(-TMath::Ceil(sqrt(hist.GetEntries())));
	hresidy->SetMaximum(TMath::Ceil(sqrt(hist.GetEntries())));
	hresidy->Draw("AP");
	TLine line1(hist.GetYaxis()->GetBinLowEdge(1), 0, hist.GetYaxis()->GetBinUpEdge(hist.GetYaxis()->GetNbins()-1), 0);
	line1.SetLineStyle(kDashed);
	line1.SetLineWidth(2);
	line1.SetLineColor(kRed);
	line1.Draw("same");
	pad5->cd();
	pad5->SetMargin(0.10, 0.10, 0.02, 0.05);
	data.plotOn(framex);
	model->plotOn(framex,RooFit::Name(model->GetName()),LineStyle(kDashed), LineColor(kRed)) ;
	if(pHigh < fProtonTreshold){
		if(pHigh < fKaonTreshold){
			cout << "Chi2/ndf(dE/dx) = " << framex->chiSquare() << endl;
		}
		else cout << "Chi2/ndf(dE/dx) = " << framex->chiSquare() << endl;	
	}
	else cout << "Chi2/ndf(dE/dx) = " << framex->chiSquare() << endl;
	RooHist* hresidx = framex->residHist();
	if(pHigh > fProtonTreshold) model->plotOn(framex, Components("p"),RooFit::Name("pplot"), LineColor(kBlue+2), FillColor(kBlue+2),DrawOption("F")) ;
	model->plotOn(framex, Components("pi"), LineColor(kOrange),RooFit::Name("piplot"), FillColor(kOrange),DrawOption("F")) ;
	model->plotOn(framex, Components("e"), LineColor(kGreen+3),RooFit::Name("eplot"), FillColor(kGreen+3),DrawOption("F")) ;
	if(pHigh > fKaonTreshold) model->plotOn(framex, Components("K"),RooFit::Name("Kplot"), LineColor(kViolet),  FillColor(kViolet),DrawOption("F")) ; 	
	
	TLegend *leg1 = new TLegend(0.8,0.4,0.95,1.0);	
	if(positive){
		leg1->AddEntry(framex->findObject("piplot")," #pi^{+}","f");
		leg1->AddEntry(framex->findObject("eplot")," e^{+}","f");
		if(pHigh > fProtonTreshold)	leg1->AddEntry(framex->findObject("pplot")," p","f");
		if(pHigh > fKaonTreshold)	leg1->AddEntry(framex->findObject("Kplot")," K^{+}","f");
		
	}
	else{
		leg1->AddEntry(framex->findObject("piplot")," #pi^{-}","f");
		leg1->AddEntry(framex->findObject("eplot")," e^{-}","f");
		if(pHigh > fProtonTreshold)	leg1->AddEntry(framex->findObject("pplot")," #bar{p}","f");
		if(pHigh > fKaonTreshold)	leg1->AddEntry(framex->findObject("Kplot")," K^{-}","f");		
	}
	
	framex->Draw();
	leg1->Draw("same");
	
	pad6->cd();
	pad6->SetMargin(0.10, 0.10, 0.40, 0.0);
	hresidx->SetTitle("");
	hresidx->GetXaxis()->SetTitle("dE/dx [mip]");
	hresidx->GetXaxis()->SetTitleSize(0.18);
	hresidx->GetXaxis()->SetLabelSize(0.18);
	hresidx->GetXaxis()->SetRangeUser(hist.GetXaxis()->GetBinUpEdge(1), hist.GetXaxis()->GetBinUpEdge(hist.GetXaxis()->GetNbins()-1));
	hresidx->GetYaxis()->SetTitle("Residual");
	hresidx->GetYaxis()->SetTitleSize(0.1);
	hresidx->GetYaxis()->SetLabelSize(0.1);
	hresidx->GetYaxis()->SetNdivisions(4,0,0, kTRUE);
	hresidx->SetMinimum(-TMath::Ceil(sqrt(hist.GetEntries())));
	hresidx->SetMaximum(TMath::Ceil(sqrt(hist.GetEntries())));
	hresidx->Draw("AP");
	TLine line2(hist.GetXaxis()->GetBinUpEdge(1), 0, hist.GetXaxis()->GetBinUpEdge(hist.GetXaxis()->GetNbins()-1), 0);
	line2.SetLineStyle(kDashed);
	line2.SetLineWidth(2);
	line2.SetLineColor(kRed);
	line2.Draw("same");
		
	string s1, s2, s3, s4;
	
	if(positive){
		s1 = "A_{p} = (" + ConvertToString(fMult[0],0) + "#pm" + ConvertToString(fMultE[0],0) + ")\n";
		s2 = "A_{K^{+}} = (" + ConvertToString(fMult[1],0) + "#pm" + ConvertToString(fMultE[1],0) + ")\n";
		s3 = "A_{#pi^{+}} = (" + ConvertToString(fMult[2],0) + "#pm" + ConvertToString(fMultE[2],0) + ")\n";
		s4 = "A_{e^{+}} = (" + ConvertToString(fMult[3],0) + "#pm" + ConvertToString(fMultE[3],0) + ")\n";
	}
	else{
		s1 = "A_{#bar{p}} = (" + ConvertToString(fMult[4],0) + "#pm" + ConvertToString(fMultE[4],0) + ")\n";
		s2 = "A_{K^{-}} = (" + ConvertToString(fMult[5],0) + "#pm" + ConvertToString(fMultE[5],0) + ")\n";
		s3 = "A_{#pi^{-}} = (" + ConvertToString(fMult[6],0) + "#pm" + ConvertToString(fMultE[6],0) + ")\n";
		s4 = "A_{e^{-}} = (" + ConvertToString(fMult[7],0) + "#pm" + ConvertToString(fMultE[7],0) + ")\n";	
	}
	
	pad4->cd();
	TPaveText *pt = new TPaveText(0.25,0.1,0.75,1);
	if(pHigh > fProtonTreshold) pt->AddText(s1.c_str());
	if(pHigh > fKaonTreshold) pt->AddText(s2.c_str());
	pt->SetBorderSize(1);
	pt->SetFillColor(0);
	pt->AddText(s3.c_str());
	pt->AddText(s4.c_str()); 	
	
	pt->Draw();
	

	
	if(fPrint){
		if(positive)
			c->Print("fitpos.pdf");
		else
			c->Print("fitneg.pdf");
	}
	else{
		TFile file(fOutFile.c_str(), "UPDATE");
		file.cd();
		c->Write();
		file.Close();		
	}
	
	delete pt;
	pt = NULL;

	delete ptTitle;
	ptTitle = NULL;
	
	delete leg0;
	leg0 = NULL;
		
	delete leg1;
	leg1 = NULL;

	
	delete pad0;
	pad0 = NULL;	
	delete pad1;
	pad1 = NULL;
	delete pad2;
	pad2 = NULL;
	delete pad3;
	pad3 = NULL;
	delete pad4;
	pad4 = NULL;
	delete pad5;
	pad5 = NULL;
	delete pad6;
	pad6 = NULL;
	
	delete c;
	c = NULL;
}

void dEdxTOFFit::DrawPlots(RooRealVar &x, RooAbsPdf &model, TH1D &hist, RooDataHist &data, ParticleMult part){
	double pHigh = fBin->GetHighValue(3);
	RooPlot* framex = x.frame();
	RooPlot* framex1 = x.frame();
	
	string cName = "plot_";
	string fname = "fit";
	if(part == eProton){
		cName = cName + "pr_";
		fname = fname + "p";
	}
	else if(part == eAntiProton){
		cName = cName + "apr_";
		fname = fname + "ap";
	}
	else if(part == eKPlus){
		cName = cName + "Kp_";
		fname = fname + "Kp";
	}
	else if(part == eKMinus){
		cName = cName + "Km_";
		fname = fname + "Km";
	}	
	cName = cName + ConvertToString(fBin->GetLowValue(1), 0) + "-" + ConvertToString(fBin->GetHighValue(1),0) + "_th" + 
	ConvertToString(fBin->GetLowValue(2), 0) + "-" + ConvertToString(fBin->GetHighValue(2), 0) + "_p" +
	ConvertToString(fBin->GetLowValue(3), 3) + "-" + ConvertToString(fBin->GetHighValue(3), 3);
	
	string title;
	if(fBin->GetHighValue(1) < 90){
		title = ConvertToString(fBin->GetLowValue(1), 0) + " #leq z < " + ConvertToString(fBin->GetHighValue(1), 0) + " cm, " +
		ConvertToString(fBin->GetLowValue(2), 0) + " #leq #theta < " + ConvertToString(fBin->GetHighValue(2), 0) + " mrad, " +
		ConvertToString(fBin->GetLowValue(3), 0) + " #leq p < " + ConvertToString(fBin->GetHighValue(3), 0) + " GeV/c";
	}
	else{
		title = "z = " + ConvertToString(fBin->GetHighValue(1), 0) + " cm, " +
		ConvertToString(fBin->GetLowValue(2), 0) + " #leq #theta < " + ConvertToString(fBin->GetHighValue(2), 0) + " mrad, " +
		ConvertToString(fBin->GetLowValue(3), 0) + " #leq p < " + ConvertToString(fBin->GetHighValue(3), 0) + " GeV/c";
	}
	

	
	TCanvas *c = new TCanvas(cName.c_str(), "", 500, 312);
	//TPad *pad0 = new TPad("pad1","",0.0,0.9,1.0,1);
	TPad *pad1 = new TPad("pad1","",0.0,0.30,1.0,1.);
	TPad *pad2 = new TPad("pad2","",0.0,0.0,1.0,0.30);
	c->cd();
	//pad0->Draw();
	pad1->Draw();
	pad2->Draw();	
	
	/*pad0->cd();
	pad0->SetMargin(0.00, 0.00, 0.00, 0.0);

	pt->Draw("same");*/
		
	pad1->cd();	
	pad1->SetMargin(0.10, 0.10, 0.05, 0.0);
	framex->SetTitle(title.c_str());
	//framex->GetXaxis()->SetTitle("dE/dx [mip]");
	framex->GetXaxis()->SetTitleSize(0.00);
	framex->GetXaxis()->SetLabelSize(0.00);
	framex->GetYaxis()->SetTitle("N_{tracks}");
	framex->GetYaxis()->SetTitleOffset(0.65);
	framex->GetYaxis()->SetTitleSize(0.08);
	framex->GetYaxis()->SetLabelSize(0.08);
	string s1;	
	if(part == eProton){
		s1 = "A_{p} = (" + ConvertToString(fMult[0],0) + "#pm" + ConvertToString(fMultE[0],0) + ")";
		//cout << "str: " << s1 << endl;
	}
	else if(part == eProton){
		s1 = "A_{#bar{p}} = (" + ConvertToString(fMult[4],0) + "#pm" + ConvertToString(fMultE[4],0) + ")";
	}
	else if(part == eKPlus){
		s1 = "A_{K^{+}} = (" + ConvertToString(fMult[1],0) + "#pm" + ConvertToString(fMultE[1],0) + ")";
	}
	else if(part == eKMinus){
		s1 = "A_{K^{-}} = (" + ConvertToString(fMult[5],0) + "#pm" + ConvertToString(fMultE[5],0) + ")";
	}
	TPaveText *pt = new TPaveText(0.1,0.6,1.,0.8);
	pt->AddText(s1.c_str());
	//framex->addPlotable(pt,"");
	data.plotOn(framex);
	model.plotOn(framex,RooFit::Name(model.GetName()),LineStyle(kDashed), LineColor(kRed)) ;
	RooHist* hresidx = framex->residHist();
	framex->Draw();
	//pt->Draw("same");
	
	pad2->cd();
	pad2->SetMargin(0.10, 0.10, 0.40, 0.0);
	hresidx->SetTitle("");
	hresidx->GetXaxis()->SetTitle("dE/dx [mip]");
	hresidx->GetXaxis()->SetTitleSize(0.18);
	hresidx->GetXaxis()->SetLabelSize(0.18);
	hresidx->GetXaxis()->SetRangeUser(hist.GetXaxis()->GetBinUpEdge(1), hist.GetXaxis()->GetBinUpEdge(hist.GetXaxis()->GetNbins()-1));
	hresidx->GetYaxis()->SetTitle("Residual");
	hresidx->GetYaxis()->SetTitleSize(0.1);
	hresidx->GetYaxis()->SetLabelSize(0.1);
	hresidx->GetYaxis()->SetNdivisions(4,0,0, kTRUE);
	hresidx->SetMinimum(-TMath::Ceil(sqrt(hist.GetEntries())));
	hresidx->SetMaximum(TMath::Ceil(sqrt(hist.GetEntries())));
	hresidx->Draw("AP");
	TLine line2(hist.GetXaxis()->GetBinUpEdge(1), 0, hist.GetXaxis()->GetBinUpEdge(hist.GetXaxis()->GetNbins()-1), 0);
	line2.SetLineStyle(kDashed);
	line2.SetLineWidth(2);
	line2.SetLineColor(kRed);
	line2.Draw("same");

	if(fPrint){
		fname = fname + ".pdf";
		c->Print(fname.c_str());
	}
	else{
		TFile file(fOutFile.c_str(), "UPDATE");
		file.cd();
		c->Write();
		file.Close();		
	}
	
	delete pt;
	pt = NULL;


	
	delete pad1;
	pad1 = NULL;
	delete pad2;
	pad2 = NULL;
	
	delete c;
	c = NULL;	

}
