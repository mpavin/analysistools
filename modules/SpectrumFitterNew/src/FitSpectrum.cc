#include <stdlib.h>
#include "FitSpectrum.h"
#include "SpectrumFitConfig.h"
#include "dEdxTOFFit.h"
#include <sstream>
#include <TFile.h>
#include <TTree.h>
#include <TH2D.h>
#include <TCanvas.h>
#include <TKey.h>
#include <TDirectory.h>
#include <dEdxResolution.h>
#include <TOFResolution.h>
#include <algorithm> 
#include "Bin.h"
#include "StrTools.h"
#include <TMath.h>

using namespace StrTools;
void FitSpectrum::Run(){

	SpectrumFitConfig cfg;

	TFile input(fInputFile.c_str(), "READ");

	if (input.IsZombie()){
		cout  << "[ERROR] Error opening file!" << endl;
		exit(-1);
	}	

	TTree *tree = (TTree*) input.Get("dEdx_m2tof");

	vector<double> binLow;
	vector<double> binHigh;
	
	for(int i = 0; i < 3; i++){
		binLow.push_back(0);
		binHigh.push_back(0);
	}	

	
	int indexZ;
	int indexTh;
	int indexP;
	
	double npip[2];
	double np[2];
	double nKp[2];
	double nep[2];
	
	double npim[2];
	double nap[2];
	double nKm[2];
	double nem[2];
	
	vector<double> *dEdxBin = NULL;
	vector<double> *m2tofBin = NULL;
	vector<double> *charge = NULL;
	
	tree->SetBranchAddress("zMin", &(binLow.at(0)));
	tree->SetBranchAddress("zMax", &(binHigh.at(0)));
	tree->SetBranchAddress("thetaMin", &(binLow.at(1)));
	tree->SetBranchAddress("thetaMax", &(binHigh.at(1)));
	tree->SetBranchAddress("pMin", &(binLow.at(2)));
	tree->SetBranchAddress("pMax", &(binHigh.at(2)));
	tree->SetBranchAddress("indexZ", &indexZ);
	tree->SetBranchAddress("indexTh", &indexTh);
	tree->SetBranchAddress("indexP", &indexP);
	tree->SetBranchAddress("dEdx", &dEdxBin);
	tree->SetBranchAddress("m2ToF", &m2tofBin);
	tree->SetBranchAddress("charge", &charge);
	
	int nEntries = tree->GetEntries();
	
	string namepos = cfg.GetOutputDir() + "/SpectrumFit_" + GetTimeString() + "_pos.root";
	string nameneg = cfg.GetOutputDir() + "/SpectrumFit_" + GetTimeString() + "_neg.root";
	string nameFitFile = cfg.GetOutputDir() + "/SpectrumFit_" + GetTimeString() + "_plots.root";
	
	TFile outputPos(namepos.c_str(), "RECREATE");
	outputPos.cd();
	TTree *outTreePos = new TTree("Multiplicity", "Multiplicity");
	outTreePos->Branch("zMin", &(binLow.at(0)), "zMin/D");
	outTreePos->Branch("zMax", &(binHigh.at(0)), "zMax/D");
	outTreePos->Branch("thetaMin", &(binLow.at(1)), "thetaMin/D");
	outTreePos->Branch("thetaMax", &(binHigh.at(1)), "thetaMax/D");
	outTreePos->Branch("pMin", &(binLow.at(2)), "pMin/D");
	outTreePos->Branch("pMax", &(binHigh.at(2)), "pMax/D");
	outTreePos->Branch("indexZ", &indexZ, "indexZ/I");
	outTreePos->Branch("indexTh", &indexTh, "indexTh/I");
	outTreePos->Branch("indexP", &indexP, "indexP/I");
	outTreePos->Branch("npi", &(npip[0]), "npi/D");
	outTreePos->Branch("npiE", &(npip[1]), "npiE/D");
	outTreePos->Branch("np", &(np[0]), "np/D");
	outTreePos->Branch("npE", &(np[1]), "npE/D");
	outTreePos->Branch("nK", &(nKp[0]), "nK/D");
	outTreePos->Branch("nKE", &(nKp[1]), "nKE/D");
	outTreePos->Branch("ne", &(nep[0]), "ne/D");
	outTreePos->Branch("neE", &(nep[1]), "neE/D");
	
	TFile outputNeg(nameneg.c_str(), "RECREATE");
	outputNeg.cd();
	TTree *outTreeNeg = new TTree("Multiplicity", "Multiplicity");
	outTreeNeg->Branch("zMin", &(binLow.at(0)), "zMin/D");
	outTreeNeg->Branch("zMax", &(binHigh.at(0)), "zMax/D");
	outTreeNeg->Branch("thetaMin", &(binLow.at(1)), "thetaMin/D");
	outTreeNeg->Branch("thetaMax", &(binHigh.at(1)), "thetaMax/D");
	outTreeNeg->Branch("pMin", &(binLow.at(2)), "pMin/D");
	outTreeNeg->Branch("pMax", &(binHigh.at(2)), "pMax/D");
	outTreeNeg->Branch("indexZ", &indexZ, "indexZ/I");
	outTreeNeg->Branch("indexTh", &indexTh, "indexTh/I");
	outTreeNeg->Branch("indexP", &indexP, "indexP/I");
	outTreeNeg->Branch("npi", &(npim[0]), "npi/D");
	outTreeNeg->Branch("npiE", &(npim[1]), "npiE/D");
	outTreeNeg->Branch("np", &(nap[0]), "np/D");
	outTreeNeg->Branch("npE", &(nap[1]), "npE/D");
	outTreeNeg->Branch("nK", &(nKm[0]), "nK/D");
	outTreeNeg->Branch("nKE", &(nKm[1]), "nKE/D");
	outTreeNeg->Branch("ne", &(nem[0]), "ne/D");
	outTreeNeg->Branch("neE", &(nem[1]), "neE/D");
		
	vector<double> tofRanges;
	tofRanges.push_back(cfg.GetPionTOFMeanRange());
	tofRanges.push_back(cfg.GetPionTOFWidthRange());
	tofRanges.push_back(cfg.GetProtonTOFMeanRange());
	tofRanges.push_back(cfg.GetProtonTOFWidthRange());
	tofRanges.push_back(cfg.GetKaonTOFMeanRange());
	tofRanges.push_back(cfg.GetKaonTOFWidthRange());
	tofRanges.push_back(cfg.GetElectronTOFMeanRange());
	tofRanges.push_back(cfg.GetElectronTOFWidthRange());

	vector<double> dEdxRanges;
	dEdxRanges.push_back(cfg.GetPiondEdxMeanRange());
	dEdxRanges.push_back(cfg.GetPiondEdxWidthRange());
	dEdxRanges.push_back(cfg.GetProtondEdxMeanRange());
	dEdxRanges.push_back(cfg.GetProtondEdxWidthRange());
	dEdxRanges.push_back(cfg.GetKaondEdxMeanRange());
	dEdxRanges.push_back(cfg.GetKaondEdxWidthRange());
	dEdxRanges.push_back(cfg.GetElectrondEdxMeanRange());
	dEdxRanges.push_back(cfg.GetElectrondEdxWidthRange());
	
	
	dEdxResolution *dEdxRes = new dEdxResolution(cfg.GetdEdxMap(), dEdxRanges);

	TOFResolution *tofRes = new TOFResolution(cfg.GetTOFMap(), tofRanges);

	int nBinsdEdx ;
	int nBinsTOF ;
	double mindEdx = 0;
	double maxdEdx = 0;
	
	if(cfg.IsTest()){
	
		if(cfg.GetTestHistoId() > nEntries){
			cerr << __FUNCTION__ << ": Test bin number is larger than total number of bins! Exiting..." << endl;
			exit(EXIT_FAILURE);		
		}

		tree->GetEntry(cfg.GetTestHistoId()-1);
		Bin bin(binLow, binHigh);
		

													
		dEdxTOFFit fS(dEdxRes, tofRes, dEdxBin, m2tofBin, charge, &bin);		
		fS.SetPrintStatus(true);
		fS.SetProtonTreshold(cfg.GetProtonTreshold());
		fS.SetKaonTreshold(cfg.GetKaonTreshold());
		fS.SetDoubleGaussTOFTreshold(cfg.GetDoubleGaussTOFTreshold());
		fS.ConstrainKPlusPars(cfg.ConstrainKPlusPars());
		fS.SetMinBinsdEdx(cfg.GetNBinsdEdx());
		fS.SetMinBinsTOF(cfg.GetNBinsTOF());
		
		fS.Fit();
		
		fS.GetParticleMult(eProton, np[0], np[1]);
		fS.GetParticleMult(eAntiProton, nap[0], nap[1]);
		fS.GetParticleMult(eKPlus, nKp[0], nKp[1]);
		fS.GetParticleMult(eKMinus, nKm[0], nKm[1]);
		fS.GetParticleMult(ePiPlus, npip[0], npip[1]);
		fS.GetParticleMult(ePiMinus, npim[0], npim[1]);
		fS.GetParticleMult(eElectron, nem[0], nem[1]);
		fS.GetParticleMult(ePositron, nep[0], nep[1]);

				
		cout << "******************************************************" << endl;
		cout << "Ne+ = (" << nep[0] << " +/- " << nep[1] << ")" << endl;
		cout << "Npi+ = (" << npip[0] << " +/- " << npip[1] << ")" << endl;
		cout << "NK+ = (" << nKp[0] << " +/- " << nKp[1] << ")" << endl;
		cout << "Np+ = (" << np[0] << " +/- " << np[1] << ")" << endl;
		cout << "Ne- = (" << nem[0] << " +/- " << nem[1] << ")" << endl;
		cout << "Npi- = (" << npim[0] << " +/- " << npim[1] << ")" << endl;
		cout << "NK- = (" << nKm[0] << " +/- " << nKm[1] << ")" << endl;
		cout << "Nap = (" << nap[0] << " +/- " << nap[1] << ")" << endl;
		cout << "******************************************************" << endl;
	}
	else{

		for(int i = 0; i < nEntries; i++){
			if(!((i+1)%10))
				cerr << "Fitting bin " << i+1 << endl; 
			tree->GetEntry(i);
			
			Bin bin(binLow, binHigh);

			if(dEdxBin->size() > 10){ 	
				dEdxTOFFit fS(dEdxRes, tofRes, dEdxBin, m2tofBin, charge, &bin);					
				fS.SetPrintStatus(false);
				fS.SetProtonTreshold(cfg.GetProtonTreshold());
				fS.SetKaonTreshold(cfg.GetKaonTreshold());
				fS.SetDoubleGaussTOFTreshold(cfg.GetDoubleGaussTOFTreshold());
				fS.ConstrainKPlusPars(cfg.ConstrainKPlusPars());
				fS.SetMinBinsdEdx(cfg.GetNBinsdEdx());
				fS.SetMinBinsTOF(cfg.GetNBinsTOF());
				fS.SetOutFile(nameFitFile);
				
				fS.Fit();
				
				
				fS.GetParticleMult(eProton, np[0], np[1]);
				fS.GetParticleMult(eAntiProton, nap[0], nap[1]);
				fS.GetParticleMult(eKPlus, nKp[0], nKp[1]);
				fS.GetParticleMult(eKMinus, nKm[0], nKm[1]);
				fS.GetParticleMult(ePiPlus, npip[0], npip[1]);
				fS.GetParticleMult(ePiMinus, npim[0], npim[1]);
				fS.GetParticleMult(eElectron, nem[0], nem[1]);
				fS.GetParticleMult(ePositron, nep[0], nep[1]);

				
				cout << "******************************************************" << endl;
				cout << "Ne+ = (" << nep[0] << " +/- " << nep[1] << ")" << endl;
				cout << "Npi+ = (" << npip[0] << " +/- " << npip[1] << ")" << endl;
				cout << "NK+ = (" << nKp[0] << " +/- " << nKp[1] << ")" << endl;
				cout << "Np+ = (" << np[0] << " +/- " << np[1] << ")" << endl;
				cout << "Ne- = (" << nem[0] << " +/- " << nem[1] << ")" << endl;
				cout << "Npi- = (" << npim[0] << " +/- " << npim[1] << ")" << endl;
				cout << "NK- = (" << nKm[0] << " +/- " << nKm[1] << ")" << endl;
				cout << "Nap = (" << nap[0] << " +/- " << nap[1] << ")" << endl;
				cout << "******************************************************" << endl;
			}
			
			else{
				npip[0] = -999;
				npip[1] = -999;

				np[0] = -999;
				np[1] = -999;
	
				nKp[0] = -999;
				nKp[1] = -999;

				nep[0] = -999;
				nep[1] = -999;

				npim[0] = -999;
				npim[1] = -999;

				nap[0] = -999;
				nap[1] = -999;
	
				nKm[0] = -999;
				nKm[1] = -999;

				nem[0] = -999;
				nem[1] = -999;
			}
			outTreePos->Fill();
			outTreeNeg->Fill();
		}

   				
	}
	outputPos.cd();
	outTreePos->Write();
	outputPos.Close();
	
	outputNeg.cd();
	outTreeNeg->Write();
	outputNeg.Close();
	
}

