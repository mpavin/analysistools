#include <stdlib.h>

#include "SpectrumFitConfig.h"
#include <fstream>
#include <fwk/CentralConfig.h>
#include <utl/Branch.h>
#include <utl/BranchIterator.h>
#include "CConfig.h"

using namespace std;
using namespace fwk;
using namespace utl; 

void SpectrumFitConfig::Read(){
	Branch spectrumFit = cConfig.GetTopBranch("SpectrumFitter");

	if(spectrumFit.GetName() != "SpectrumFitter"){
		cout << __FUNCTION__ << ": Wrong SpectrumFitter config file! Top branch name doesn't match! Please check your configuration files! Exiting..." << endl;
		exit (EXIT_FAILURE);
	}

	fTest = false;
	fTestHistoId = 999999;
	for(int i = 0; i < 5; i++){
		fPionRanges[i] = 0.2;
		fProtonRanges[i] = 0.2;
		fKaonRanges[i] = 0.2;
		fElectronRanges[i] = 0.2;
	}
	for(BranchIterator it = spectrumFit.ChildrenBegin(); it != spectrumFit.ChildrenEnd(); ++it){
		Branch child = *it;

		if(child.GetName() == "output"){
			child.GetData(fOutput);
		}
		else if(child.GetName() == "dEdxMap"){
			child.GetData(fdEdxMap);
		}
		else if(child.GetName() == "tofMap"){
			child.GetData(fTOFMap);
		}
		else if(child.GetName() == "dEdxWidth"){
			child.GetData(fdEdxWidth);
		}
		else if(child.GetName() == "nBinsdEdx"){
			child.GetData(fNBinsdEdx);
		}
		else if(child.GetName() == "nBinsTOF"){
			child.GetData(fNBinsTOF);
		}
		else if(child.GetName() == "startFitProtons"){
			child.GetData(fFitProtons);
		}
		else if(child.GetName() == "startFitKaons"){
			child.GetData(fFitKaons);
		}
		else if(child.GetName() == "constrainKPlusPars"){
			child.GetData(fConstrainKPlusPars);
		}
		else if(child.GetName() == "doubleGaussTOF"){
			child.GetData(fDoubleGauss);
		}
		else if(child.GetName() == "test"){

			AttributeMap atts = child.GetAttributes();
			child.GetData(fTestHistoId);
			
			if(atts["on"] == "1"){
				fTest = true;
			} 
		}
		else if(child.GetName() == "fitRange"){
			double rA;
			double rdEdxMean;
			double rdEdxWidth ;
			double rTOFMean;
			double rTOFWidth;
			

			
			for(BranchIterator rit = child.ChildrenBegin(); rit != child.ChildrenEnd(); ++rit){
				Branch rChild = *rit;
				if(rChild.GetName() == "multiplicity"){
					rChild.GetData(rA);
				}
				else if(rChild.GetName() == "dEdxMean"){
					rChild.GetData(rdEdxMean);

				}
				else if(rChild.GetName() == "dEdxWidth"){
					rChild.GetData(rdEdxWidth);
				}
				else if(rChild.GetName() == "tofMean"){
					rChild.GetData(rTOFMean);
				}
				else if(rChild.GetName() == "tofWidth"){
					rChild.GetData(rTOFWidth);
				}
			}
			AttributeMap atts = child.GetAttributes();

			if(atts["particle"] == "pion"){
				fPionRanges[0] = rA;
				fPionRanges[1] = rdEdxMean;
				fPionRanges[2] = rdEdxWidth;
				fPionRanges[3] = rTOFMean;
				fPionRanges[4] = rTOFWidth;
			}	
			if(atts["particle"] == "proton"){
				fProtonRanges[0] = rA;
				fProtonRanges[1] = rdEdxMean;
				fProtonRanges[2] = rdEdxWidth;
				fProtonRanges[3] = rTOFMean;
				fProtonRanges[4] = rTOFWidth;
			}	
			if(atts["particle"] == "kaon"){

				fKaonRanges[0] = rA;
				fKaonRanges[1] = rdEdxMean;
				fKaonRanges[2] = rdEdxWidth;
				fKaonRanges[3] = rTOFMean;
				fKaonRanges[4] = rTOFWidth;
			}	
			if(atts["particle"] == "electron"){
				fElectronRanges[0] = rA;
				fElectronRanges[1] = rdEdxMean;
				fElectronRanges[2] = rdEdxWidth;
				fElectronRanges[3] = rTOFMean;
				fElectronRanges[4] = rTOFWidth;
			}			
		}		

	}

}

