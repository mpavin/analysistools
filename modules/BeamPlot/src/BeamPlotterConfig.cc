#include <stdlib.h>
#include "BeamPlotterConfig.h"
#include <fstream>
#include <fwk/CentralConfig.h>
#include <utl/Branch.h>
#include <utl/BranchIterator.h>
#include "CConfig.h"

using namespace std;
using namespace fwk;
using namespace utl; 

void BeamPlotterConfig::Read(){


	/*if(cConfig == NULL){
		cout << __FUNCTION__ << ": Central config not initialized! Exiting..." << endl;
		exit (EXIT_FAILURE);
	}*/
	Branch dataAnaConf = cConfig.GetTopBranch("BeamPlot");

	if(dataAnaConf.GetName() != "BeamPlot"){
		cout << __FUNCTION__ << ": Wrong BeamPlotter config file! Top branch name doesn't match! Please check your configuration files! Exiting..." << endl;
		exit (EXIT_FAILURE);
	}
	
	fTriggers.push_back(2);
	fZSlices.push_back(-657.43);
	fPlotDivVsCor = false;
	fPlotChi2 = false; 
	fPlotProfile = false;
	fPlotSlope = false;
	fPlotIntercept = false;
	fMCBeam = false;
	fMCTrigger = 4;
	fBPDTPCx = 0;
	fBPDTPCy = 0;
	fMCBeamZ = -657.4 - 5.2;
	
	for(BranchIterator it = dataAnaConf.ChildrenBegin(); it != dataAnaConf.ChildrenEnd(); ++it){
		Branch child = *it;

		if(child.GetName() == "outputDir"){
			child.GetData(fOutputDir);

		}

		else if(child.GetName() == "zSlices"){
			fZSlices.clear();
			child.GetData(fZSlices);
		}
		
		else if(child.GetName() == "triggers"){
			fTriggers.clear();
			child.GetData(fTriggers);
		}

		else if(child.GetName() == "plotDivVsCor"){
			child.GetData(fPlotDivVsCor);
		}
		
		else if(child.GetName() == "plotChi2"){
			child.GetData(fPlotChi2);
		}

		else if(child.GetName() == "plotProfile"){
			child.GetData(fPlotProfile);
		}
		
		else if(child.GetName() == "plotSlope"){
			child.GetData(fPlotSlope);
		}

		else if(child.GetName() == "plotIntercept"){
			child.GetData(fPlotIntercept);
		}

		else if(child.GetName() == "createMCBeamInput"){
			child.GetData(fMCBeam);
		}	
		
		else if(child.GetName() == "MCTrigger"){
			child.GetData(fMCTrigger);
		}	
		
		else if(child.GetName() == "MCBeamZ"){
			child.GetData(fMCBeamZ);
		}
		
		else if(child.GetName() == "BPDTPCcorrectionX"){
			child.GetData(fBPDTPCx);
		}			

		else if(child.GetName() == "BPDTPCcorrectionY"){
			child.GetData(fBPDTPCy);
		}		
		
							
	}
	
}

//*********************************************************************************************
string BeamPlotterConfig::GetTimeString(){
	time_t rawtime;
	struct tm* timeinfo;

	time(&rawtime);
	timeinfo = localtime(&rawtime);

	string res;
	stringstream s;

	s << timeinfo->tm_mon + 1;
	s << "_";
	s << timeinfo->tm_mday;
	s << "_";
	s << timeinfo->tm_year + 1900;
	s << "_";
	s << timeinfo->tm_hour;
	s << "_";
	s << timeinfo->tm_min;
	s << "_";
	s << timeinfo->tm_sec;

	s >> res;

	return res;
}


