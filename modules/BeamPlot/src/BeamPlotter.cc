#include <stdlib.h>
#include "BeamPlotterConfig.h"
#include "Functions.h"
#include "PlotHisto.h"
#include "BeamPlotter.h"
#include <sstream>
#include "TreeDataTypes.h"
#include <TFile.h>
#include <TH2D.h>
#include <TCanvas.h>
#include <TTree.h>
#include <TF1.h>
#include <utl/Vector.h>
#include "StrTools.h"

using namespace utl; 
using namespace StrTools;

void BeamPlotter(string inputFile){

	BeamPlotterConfig cfg;

	TFile input(inputFile.c_str(), "READ");
	TTree *tree = (TTree*) input.Get("BeamData");
		
	BeamData bData;
	
	string name = cfg.GetOutputDir() + "beamplots_" + cfg.GetTimeString() + ".root";
	TFile output(name.c_str(), "RECREATE");
	
	tree->SetBranchAddress("BeamData", &bData);
	int nEntries = tree->GetEntries();

		
	int nSlices = cfg.GetZSlices().size();
	int nTriggers = cfg.GetTriggers().size();
	
	vector<vector<PlotHisto> > slice;
	vector<vector<PlotHisto> > divergence;
	vector<PlotHisto> slopes;
	vector<PlotHisto> intercepts;
	vector<PlotHisto> chi2;
	PlotHisto mcbeam(2, "BeamHistos","divergence_x","divergence_y", "divergence", "");
	for(int i = 0; i < nTriggers; ++i){
		PlotHisto slope_temp(1, "BeamHistos","slope_x","slope_y", "slope", ConvertToString(i+1));
		slopes.push_back(slope_temp);
		PlotHisto intercept_temp(1, "BeamHistos","intercept_x","intercept_y", "intercept", ConvertToString(i+1));
		intercepts.push_back(intercept_temp);
		PlotHisto chi2_temp (1, "BeamHistos","chi2_x","chi2_y", "chi2", ConvertToString(i+1));
		chi2.push_back(chi2_temp);
		
		vector<PlotHisto> sl;
		vector<PlotHisto> div;
		
		for(int j = 0; j < nSlices; ++j){
			string add = ConvertToString(i+1) + "_" + ConvertToString(j+1);
			PlotHisto slice_temp(2, "BeamHistos","slice_left","slice_right", "slice", add);
			PlotHisto divergence_temp(2, "BeamHistos","divergence_x","divergence_y", "divergence", add);

			sl.push_back(slice_temp);
			div.push_back(divergence_temp);
		}
		slice.push_back(sl);
		divergence.push_back(div);	
	}
	

	for(int i = 0; i < nEntries; ++i){
		tree->GetEntry(i);
		//cout << bData.slopex << endl;
		if((i+1)%100000 == 0) 
			cout << "Processing entry #" << i+1 << "!" << endl;

		if(cfg.CreateMCBeamInputFile()){
			if(bData.trigger & cfg.GetMCTrigger()){
				double xMC = bData.slopex*cfg.GetMCBeamZ() + bData.interceptx + cfg.GetBPDTPCCorX();
				double yMC = bData.slopey*cfg.GetMCBeamZ() + bData.intercepty + cfg.GetBPDTPCCorY();
				mcbeam.Fill(1, xMC, bData.slopex);
				mcbeam.Fill(2, yMC, bData.slopey);
			}
		}
		for(int j = 0; j < nTriggers; ++j){
			int trig = cfg.GetTriggers().at(j);
			int good = bData.trigger & trig;
			
			if(good == 0) continue;
			
			if(cfg.PlotSlope()){
				slopes.at(j).Fill(1, bData.slopex);
				slopes.at(j).Fill(2, bData.slopey);
			}

			if(cfg.PlotIntercept()){
				intercepts.at(j).Fill(1, bData.interceptx);
				intercepts.at(j).Fill(2, bData.intercepty);
			}
			
			if(cfg.PlotChi2()){
				chi2.at(j).Fill(1, bData.chi2Ndfx);
				chi2.at(j).Fill(2, bData.chi2Ndfy);
			}
					 
			for(int k = 0; k < nSlices; ++k){
				double x = bData.slopex*cfg.GetZSlices().at(k) + bData.interceptx + cfg.GetBPDTPCCorX();
				double y = bData.slopey*cfg.GetZSlices().at(k) + bData.intercepty + cfg.GetBPDTPCCorY();
				
				if(cfg.PlotProfile()){
					if(x>0){
						slice.at(j).at(k).Fill(1, x, y);
					}
					else{
						slice.at(j).at(k).Fill(1, x, y);
					}
				}
				
				if(cfg.PlotDivVsCor()){
					divergence.at(j).at(k).Fill(1, x, bData.slopex);
					divergence.at(j).at(k).Fill(2, y, bData.slopey);
				}
			}			
		}
		

	}	

	input.Close();
	
	cout << "Saving Histograms..." << endl;
	for(int i = 0; i < nTriggers; ++i){
		string dir = "trigger" + ConvertToString(cfg.GetTriggers().at(i));
		output.mkdir(dir.c_str());
		output.cd(dir.c_str());
		
		if(cfg.PlotSlope()){
			slopes.at(i).GetRSTHisto()->Write();
			slopes.at(i).GetWSTHisto()->Write();
			
			if(slopes.at(i).GetRSTCanvas() != NULL){
				slopes.at(i).GetRSTCanvas()->Write();
			}
			
			if(slopes.at(i).GetWSTCanvas() != NULL){
				slopes.at(i).GetWSTCanvas()->Write();
			}
		}

		if(cfg.PlotIntercept()){
			intercepts.at(i).GetRSTHisto()->Write();
			intercepts.at(i).GetWSTHisto()->Write();
			
			if(intercepts.at(i).GetRSTCanvas() != NULL){
				intercepts.at(i).GetRSTCanvas()->Write();
			}
			
			if(intercepts.at(i).GetWSTCanvas() != NULL){
				intercepts.at(i).GetWSTCanvas()->Write();
			}
		}
			
		if(cfg.PlotChi2()){
			chi2.at(i).GetRSTHisto()->Write();
			chi2.at(i).GetWSTHisto()->Write();
			
			if(chi2.at(i).GetRSTCanvas() != NULL){
				chi2.at(i).GetRSTCanvas()->Write();
			}
			
			if(chi2.at(i).GetWSTCanvas() != NULL){
				chi2.at(i).GetWSTCanvas()->Write();
			}
		}		
		for(int j = 0; j < nSlices; ++j){
			if(cfg.PlotProfile()){
				slice.at(i).at(j).GetAllHisto()->Write();
			
				if(slice.at(i).at(j).GetAllCanvas() != NULL){
					slice.at(i).at(j).GetAllCanvas()->Write();
				}
			
			}
				
			if(cfg.PlotDivVsCor()){
				divergence.at(i).at(j).GetRSTHisto()->Write();
				divergence.at(i).at(j).GetWSTHisto()->Write();
			
				if(divergence.at(i).at(j).GetRSTCanvas() != NULL){
					divergence.at(i).at(j).GetRSTCanvas()->Write();
				}
			
				if(divergence.at(i).at(j).GetWSTCanvas() != NULL){
					divergence.at(i).at(j).GetWSTCanvas()->Write();
				}
			}
		}
	}
	
	
	//output.Close();
	
	if(cfg.CreateMCBeamInputFile()){
		cout << "Creating MC beam input..." << endl;
		string mcbName = cfg.GetOutputDir() + "mc_beam_input.root";

		TFile mcb(mcbName.c_str(), "RECREATE");
		mcb.cd();
		mcbeam.GetRSTHisto()->Write();
		mcbeam.GetWSTHisto()->Write();
		mcb.Close();
	}
	output.Close();
}


