#ifndef BEAMPLOTTERCONFIG_H
#define BEAMPLOTTERCONFIG_H
#include <iostream>
#include <vector>
#include <map>
#include <string>

using namespace std;

class BeamPlotterConfig{

	public:
		BeamPlotterConfig()  {Read();}  //Constructor

		//*************************Get functions***********************************

		string GetOutputDir(){return fOutputDir;}
		bool PlotDivVsCor(){return fPlotDivVsCor;}
		bool PlotChi2(){return fPlotChi2;}
		bool PlotProfile(){return fPlotProfile;}
		bool PlotSlope(){return fPlotSlope;}
		bool PlotIntercept(){return fPlotIntercept;}
		vector<int>& GetTriggers(){return fTriggers;}
		vector<double>& GetZSlices(){return fZSlices;}
		bool CreateMCBeamInputFile(){return fMCBeam;}
		double GetBPDTPCCorX(){return fBPDTPCx;}
		double GetBPDTPCCorY(){return fBPDTPCy;}
		int GetMCTrigger(){return fMCTrigger;}
		double GetMCBeamZ(){return fMCBeamZ;}
		//*************************Set functions***********************************
		void Read(); //Reads main config file.

		void SetPlotDivVsCor(bool val){fOutputDir = val;}
		void SetPlotChi2(bool val){fPlotChi2 = val;}
		void SetOutputDir(string outputDir){fOutputDir = outputDir;}
		void SetPlotProfile(bool val){fPlotProfile = val;}
		void SetPlotSlope(bool val){fPlotSlope = val;}
		void SetPlotIntercept(bool val){fPlotIntercept = val;}
		void SetTriggers(vector<int> trig){fTriggers = trig;}
		void AddTrigger(int trig){fTriggers.push_back(trig);}
		void SetZSlices(vector<double> z){fZSlices = z;}
		void AddZSlice(double z){fZSlices.push_back(z);}
		void SetCreateMCBeamInputFile(bool val){fMCBeam = val;}
		void SetBPDTPCCorX(double val){fBPDTPCx = val;}
		void SetBPDTPCCorY(double val){fBPDTPCy = val;}
		void SetMCTrigger(int trig){fMCTrigger = trig;}
		void SetMCBeamZ(double val){fMCBeamZ = val;}
		string GetTimeString();
		
	private:

		vector<int> fTriggers;
		vector<double> fZSlices;
		string fOutputDir;
		bool fPlotDivVsCor;
		bool fPlotChi2;
		bool fPlotProfile;
		bool fPlotSlope;
		bool fPlotIntercept;
		bool fMCBeam;
		int fMCTrigger;
		double fBPDTPCx;
		double fBPDTPCy;
		double fMCBeamZ;
};

#endif
