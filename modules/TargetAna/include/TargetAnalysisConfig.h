#ifndef TARGETANALYSISCONFIG_H
#define TARGETANALYSISCONFIG_H
#include <iostream>
#include <vector>
#include <string>
#include "TargetAna.h"




using namespace std;

class TargetAnalysisConfig{

	public:
		TargetAnalysisConfig()  {Read();}  //Constructor

		//*************************Get functions***********************************

		string GetOutputDir(){return fOutputDir;}
		string GetInputListFile(){return fInputData;}

		vector<int>& GetRunNumbers(){return fRunNumbers;}
		TargetAna& GetTarget(){return fTarget;}
		
		int GetNSlices(){return fNSlices;}
		//*************************Set functions***********************************
		void Read(); //Reads main config file.


		void SetInputDataFile(string inputData){fInputData = inputData;}
		void SetOutputDir(string outputDir){fOutputDir = outputDir;}

		void SetRunNumbers(vector<int> fRunNumbers);
		void SetTarget(string name, ETargetType type, double length, double radius, double positionX, double positionY, double positionZ, double tilt1, double tilt2);
		void SetNSlices(int slices){fNSlices = slices;}
		vector<string>& GetJob();
		void ReadInputList();
		void ReadInputList(string inputData);


		bool SkipRun(int runNumber);
	private:


		
		string fInputData;
		string fOutputDir;

		vector<string> fJob;

		vector<int> fRunNumbers;
		TargetAna fTarget;
		int fNSlices;
};

#endif
