#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <fstream>
#include <vector>
#include <string>
#include <sstream>
#include <iostream>    
#include <iomanip> 
#include <stdlib.h>
#include <TH2D.h>
using namespace std;

struct Hit{
	int tr;
	double tof[2];
	
};

struct BeamA{
	double aX;
	double bX;
	double aY;
	double bY;
};

struct ImpactPar{
	int scint;
	double x;
	double y;
	double dx;
};

void ReadInputList(string list, vector<string> &job){

	ifstream ifs;
	string line;
	ifs.open(list.c_str());
		
	if (!ifs.is_open()){
		cerr << "Could not open infile. Exiting..." << endl;			
		exit (101);
	}
    
	while (getline(ifs, line)){
		if(line.at(0)=='#')
			continue;
		job.push_back(line);
	}
	ifs.close();	

}


std::string ConvertToString(double x, int prec){
	std::stringstream ss;
	std::string val;

	ss << std::fixed << std::setprecision(prec);
	ss << x;
	ss >> val;

	return val;
}


int FindZBin(double z){
	double zT = z + 657.41;
	
	//cout << zT << endl;
	if(zT >= 0 && zT < 18) return 0;
	else if(zT >= 18 && zT < 36) return 1;
	else if(zT >= 36 && zT < 54) return 2;
	else if(zT >= 54 && zT < 72) return 3;
	else if(zT >= 72 && zT < 89.99) return 4;
	else if(zT >= 89.99 && zT < 90.01) return 5;
	else{
		cerr << "Bad z bin!" << endl;
		return 5;
	}
}



double DoStep(double x, double y, double r1, double r2, TH1 *hist){
	unsigned int startX = hist->GetXaxis()->FindBin(x-r1);
	unsigned int stopX = hist->GetXaxis()->FindBin(x+r1);

	unsigned int startY = hist->GetYaxis()->FindBin(y-r2);
	unsigned int stopY = hist->GetYaxis()->FindBin(y+r2);

	double sum = 0;
	for(unsigned int k = startX; k <= stopX; ++k){
		for(unsigned int m = startY; m <= stopY; ++m){
			double x1 = hist->GetXaxis()->GetBinCenter(k);
			double y1 = hist->GetYaxis()->GetBinCenter(m);
			unsigned int nBin = hist->GetBin(k, m);

			//double dist = sqrt((x-x1)*(x-x1) + (y-y1)*(y-y1));
			double dist = (x-x1)*(x-x1)/r1/r1 + (y-y1)*(y-y1)/r2/r2;
			if(dist < 1){
				sum += hist->GetBinContent(nBin);

			}

		}
				
	}
	//if(sum > 0) cout << "SUM: " << sum << endl;
	return sum;

}


bool FindMaximum(double &x, double r1, TH1D &hist){
	
	double max = 0;
	double xt=0;
	for(int i = 1; i <= hist.GetNbinsX(); i++){
		xt = hist.GetBinLowEdge(i);
		double maxt = 0;
		for(int j = i; j <= hist.GetNbinsX(); j++){
			if(hist.GetBinLowEdge(j)+hist.GetBinWidth(j) <= xt+r1){
				maxt += hist.GetBinContent(j);
			}
			else 
				break;
		
		}
		if(maxt > max){
			x = xt + r1/2.;
			max = maxt;
		}
	}

	return true;
}


bool FindMaximum(double *val, double r1, double r2, double step, TH1 *hist){

	//double r = 1.3; //radius = 1.3 cm

	unsigned int nBinsX = hist->GetNbinsX();
	unsigned int nBinsY = hist->GetNbinsY();
	//double step = 0.1;
	
	double startX = hist->GetXaxis()->GetBinLowEdge(1)+r1;
	double stopX = hist->GetXaxis()->GetBinUpEdge(nBinsX)-r1;

	double startY = hist->GetYaxis()->GetBinLowEdge(1)+r2;
	double stopY = hist->GetYaxis()->GetBinUpEdge(nBinsY)-r2;
	
	//double startY = -0.3;
	//double stopY = 0.3;
	unsigned int nStepsX = (stopX - startX)/step;
	unsigned int nStepsY = (stopY - startY)/step;

	double valXY[3] = {0, 0, 0};
	double sum = 0;
	for(unsigned int i = 0; i < nStepsX; ++i){

	  double x = startX + i*step;
	  for(unsigned int j = 0; j < nStepsY; ++j){
	    double y = startY + j*step;
	    sum = DoStep(x,y,r1, r2,hist);
	    
	    if(sum > valXY[2]){
	      valXY[2] = sum;
	      valXY[0] = x;
	      valXY[1] = y;
	    }
	    if(sum == valXY[2]){
	      if((fabs(x-valXY[0]) == step)||(fabs(y-valXY[1]) == step)){
		valXY[0] += (x-valXY[0])/2;
		valXY[1] += (y-valXY[1])/2;
		}
		}
	  }
	}
	
	//cout << "F:" << valXY[0] << " " << valXY[1] << endl;
	val[0] = valXY[0];
	val[1] = valXY[1];
	val[2] = valXY[2];
	return true;
	
}

void FitGauss(vector<double> &p , vector<double> &s, double &pos, double &posE){
	double q = 0;
	double mean = 0;
	double mean2 = 0;
	for(int i = 0; i < p.size(); i++){
		q += s.at(i);
	}
	for(int i = 0; i < p.size(); i++){
		
		mean += p.at(i)*s.at(i)/q;
		mean2 += p.at(i)*p.at(i)*s.at(i)*sqrt(2)/q/q;
		//mean2 += 0.25*s.at(i)*s.at(i)/q/q;
	} 	
	pos = mean;
	//posE = sqrt(mean2 - mean*mean)*sqrt(2/q);
	posE = sqrt(mean2);
	//cout << pos << " " << posE << " " << mean2 - mean*mean << endl;
	
}


double FitLine(vector<double> &z , vector<double> &x, vector<double> &xE, double &a, double &b){
	
	double v11 = 0.;
	double v12 = 0.;
	double v22 = 0.;
	double g1 = 0.;
	double g2 = 0.;
	double sumpos2 = 0;
	
	for(int i = 0; i < z.size(); i++){
		v11 += 1/(xE.at(i)*xE.at(i));
		v12 += z.at(i)/(xE.at(i)*xE.at(i));
		v22 += z.at(i)*z.at(i)/(xE.at(i)*xE.at(i));
		g1 += x.at(i)/(xE.at(i)*xE.at(i));
		g2 += z.at(i)*x.at(i)/(xE.at(i)*xE.at(i));
		sumpos2 += x.at(i)*x.at(i)/(xE.at(i)*xE.at(i));
	}
	
	//xmean = xmean/W;
	//xmean2 = xmean2/W;
	//zxmean = zxmean/Wz;
	double d = v11*v22 - v12*v12;
	a = (g2*v11 - g1*v12)/d;
	b = (g1*v22 - g2*v12)/d;

	/*double chi2 = 0;
	for(int i = 0; i < z.size(); i++){
		chi2 += (x.at(i)-a*z.at(i) - b)*(x.at(i)-a*z.at(i) - b)/(z.at(i)*v11/d + v22/d);
	
	}*/
	return sumpos2 - a*g2 - b*g1;
	//return chi2;
}



bool FindCluster(string name, vector<double> *signal, vector<double> &pos, vector<double> &charge, double mp[11], double num[11]){

	double max = 0;
	int nmax = -1;
	vector<int> npos;
	
	for(int j = 0; j < signal->size(); j++){
		if(signal->at(j) > max){
			max = signal->at(j);
			nmax = j;
		}
			//test1.Fill(j, signal1x->at(j));
	}	
	
	/*for(int j = 0; j < signal->size(); j++){
		if(signal->at(j) > 0.5*max){
			npos.push_back(j);
		}
	}	

	for(int i = 0; i < npos.size()-1; i++){
		if(abs(npos.at(i+1)-npos.at(i)) > 3){
			cerr << name << "2 clusters found" << endl;
			for(int j = 0; j < npos.size(); j++){
				cerr << "(" << npos.at(j) << ", " << signal->at(npos.at(j)) << ") ";
			}
			cerr << endl;
			return false;
		}
	}*/
	mp[6] += max;
	num[6] += 1.;
	int assym = 0;
	pos.push_back(nmax);
	charge.push_back(max);
	int count = 0;
	for(int i = nmax-1; i >= 0; i--){
		if(signal->at(i) > 0.10*max){
			count++;
			pos.insert(pos.begin(), i);
			charge.insert(charge.begin(), signal->at(i));
			if(count < 7){
				mp[6-count] += signal->at(i);
				num[6-count] += 1.;
			}
		}
	} 
	count = 0;
	for(int i = nmax+1; i < signal->size() ; i++){
		if(signal->at(i) > 0.10*max){
			count++;
			pos.push_back(i);
			charge.push_back(signal->at(i));
			if(count < 7){
				mp[6+count] += signal->at(i);
				num[6+count] += 1.;
			}
		}
	} 
	
	/*cout << name << ": " ;
	for(int i = 0; i < pos.size(); i++){
		cout  << charge.at(i) << " ";
	}
	cout << endl;*/
	return true;
}
#endif
