#include <vector>
#include <string>
#include <iostream>
#include "Functions.h"
#include <TH1D.h>
#include <TH2D.h>
#include <TFile.h>
#include <TChain.h>
#include <TTree.h>
#include <TMath.h>
#include <math.h>
#include <TF1.h>

using namespace std;


int main(){

	string inputFile = "list1.txt";

	TChain chain("TargetAnalysis");
	
	string outName = "targetz.root";
	TFile outFile(outName.c_str(), "RECREATE");
	ifstream ifs;
	string line;
	ifs.open(inputFile.c_str());
		
	if (!ifs.is_open()){
		cerr << "[ReadInputListFile] Could not open infile. Exiting..." << endl;			
		exit (101);
	}
    
	while (getline(ifs, line)){
		if(line.at(0)=='#')
			continue;
		chain.Add(line.c_str());
	}
	ifs.close();		

	vector<double> *x = NULL;
	vector<double> *xE = NULL;
	vector<double> *y = NULL;
	vector<double> *yE = NULL;
	vector<double> *z = NULL;
	vector<double> *zS = NULL;
	vector<double> *px = NULL;
	vector<double> *py = NULL;
	vector<double> *pz = NULL;
	vector<int> *q = NULL;
	vector<int> *topology = NULL;
	vector<int> *nVTPC = NULL;
	vector<int> *nGTPC = NULL;
	vector<int> *nMTPC = NULL;
	double aX;
	double bX;
	double aY;
	double bY;
	int runNumber;	
	

	
	chain.SetBranchAddress("x", &x);
	chain.SetBranchAddress("xE", &xE);
	chain.SetBranchAddress("y", &y);
	chain.SetBranchAddress("yE", &yE);
	chain.SetBranchAddress("z", &z);
	chain.SetBranchAddress("zS", &zS);
	chain.SetBranchAddress("px", &px);
	chain.SetBranchAddress("py", &py);
	chain.SetBranchAddress("pz", &pz);
	chain.SetBranchAddress("q", &q);
	chain.SetBranchAddress("nVTPC", &nVTPC);
	chain.SetBranchAddress("nGTPC", &nGTPC);
	chain.SetBranchAddress("nMTPC", &nMTPC);
	chain.SetBranchAddress("topology", &topology);
	chain.SetBranchAddress("aX", &aX);
	chain.SetBranchAddress("aY", &aY);
	chain.SetBranchAddress("bX", &bX);
	chain.SetBranchAddress("bY", &bY);
	chain.SetBranchAddress("runNumber", &runNumber);

	chain.SetBranchAddress("runNumber", &runNumber);

	
	int nEntries = chain.GetEntries();
	
	TH1D zpos("zpos", "", 130, -677, -547);
	
	vector<TH1D> xslices;
	vector<TH1D> yslices;
	
	vector<TH1D> xslicesp;
	vector<TH1D> yslicesp;
	TH2D vertxz("vertxz", "", 130, -677, -547, 100, -2, 2);
	TH2D vertyz("vertyz", "", 130, -677, -547, 100, -2, 2);
	TH1D tiltx("tiltx", "", 15, 0, 90);
	TH1D tilty("tilty", "", 15, 0, 90);
	TH1D tiltxp("tiltxp", "", 15, 0, 90);
	TH1D tiltyp("tiltyp", "", 15, 0, 90);
	TH1D xeh("xeh", "", 100, 0, 1);
	TH1D yeh("yeh", "", 100, 0, 1);
	
	for(int i = 0; i < 15; i++){
		string namex = "x" + ConvertToString(i+1.,0);
		string namey = "y" + ConvertToString(i+1.,0);
		string namexp = "x" + ConvertToString(i+1.,0) + "p";
		string nameyp = "y" + ConvertToString(i+1.,0) + "p";
		TH1D tempx(namex.c_str(), "", 200, -2, 2);
		TH1D tempy(namey.c_str(), "", 200, -2, 2);
		TH1D tempxp(namexp.c_str(), "", 200, -2, 2);
		TH1D tempyp(nameyp.c_str(), "", 200, -2, 2);
		xslices.push_back(tempx);
		yslices.push_back(tempy);
		xslicesp.push_back(tempxp);
		yslicesp.push_back(tempyp);
		
		/*xslices.at(i).Sumw2();
		yslices.at(i).Sumw2();
		xslicesp.at(i).Sumw2();
		yslicesp.at(i).Sumw2()*/;
	}
	cout << nEntries << endl;
	for(int i = 0; i < nEntries; ++i){
		
		chain.GetEntry(i);
		if(!((i+1)%100000)) cout << "Processing entry " << i+1 << "." << endl;
		
		double zval = 0;
		double xval = 0;
		double yval = 0;
		double n = 0.;
		double r = sqrt((-657.41*aX+bX-0.015)*(-657.41*aX+bX-0.015)+(-657.41*aY+bY-0.285)*(-657.41*aY+bY-0.285));
		if(r > 1.30) continue;
		for(int j = 0; j < 15; j++){
			double xb = aX*(-657.41+j*2+1) + bX;
			double yb = aY*(-657.41+j*2+1) + bY;
			if(sqrt((xb-0.015)*(xb-0.015)+(yb-0.285)*(yb-0.285))>1.30) continue;
			xslicesp.at(j).Fill(xb  + 0.2241);
			yslicesp.at(j).Fill(yb  - 0.1592);
		}
		
		for(int j = 0; j < z->size(); j++){
			double p = sqrt(px->at(j)*px->at(j)+py->at(j)*py->at(j)+pz->at(j)*pz->at(j));
			//if(p < 0.5 && p > 20) continue;
			
			double theta = 1000*acos(pz->at(j)/p);
			//if(theta < 40 || theta > 260) continue; 
			//zval += z->at(j);
			//xval += x->at(j);
			//yval += y->at(j);
			//n += 1.;
			int ind = (z->at(j) + 657.41)/6.;
			xeh.Fill(xE->at(j));
			yeh.Fill(yE->at(j));
			if(xE->at(j)>0.16) continue;
			if(yE->at(j)>0.1) continue;
			
			if(ind < 0) continue;
			if(ind > 14) continue;	
			
			zpos.Fill(z->at(j));
			xslices.at(ind).Fill(aX*z->at(j) + bX + 0.2241);
			yslices.at(ind).Fill(aY*z->at(j) + bY - 0.1592);
			vertxz.Fill(z->at(j), aX*z->at(j) + bX + 0.2241);
			vertyz.Fill(z->at(j), aY*z->at(j) + bY - 0.1592);		

				
		}
		//zpos.Fill(zval/n);
		/*int ind = (zval/n + 657.41)/2.;
		if(ind < 0) continue;
		if(ind > 44) continue;*/
		//cout << aX << " " << aY << " " << bX << " " << bY << endl;
		/*xslices.at(ind).Fill(aX*zval/n + bX + 0.2241);
		yslices.at(ind).Fill(aY*zval/n + bY - 0.1592);
		vertxz.Fill(zval, aX*zval/n + bX + 0.2241);
		vertyz.Fill(zval, aY*zval/n + bY - 0.1592);*/
		
		/*xslices.at(ind).Fill(xval/n);
		yslices.at(ind).Fill(yval/n);
		vertxz.Fill(zval/n,xval/n);
		vertyz.Fill(zval/n, yval/n);*/

		
	}
	//cout << nCount << " " << nEntries << endl;


	TF1* zFit = new TF1("zFit", "([0]*TMath::Erf((x-[1])/[2])+[0])+[3]", -663., -642);
	zFit->SetParName(0, "N");
	zFit->SetParameter(0, 25000);
	zFit->SetParLimits(0, 1000, 110000000);	
	
	
	zFit->SetParName(1, "z_{0}");
	zFit->SetParameter(1, -657);
	zFit->SetParLimits(1, -665, -640);	
	
	zFit->SetParName(2, "#sigma");
	zFit->SetParameter(2, 2);
	zFit->SetParLimits(2, 1, 20);	


	zFit->SetParName(3, "#Delta");
	zFit->SetParameter(3, 20);
	zFit->SetParLimits(3, 0, 200);		

	zpos.Fit(zFit, "R");
	outFile.cd();
	zpos.Write();

	for(int i = 0; i < 15; i++){
		double x;
		double y;
		FindMaximum(x, 2.58, xslices.at(i));
		FindMaximum(y, 2.58, yslices.at(i));

		for(int j = 1; j < 201; j++){
			double scalex = 0;
			double scaley = 0;
			double xe = 0;
			double ye = 0;
			if(xslicesp.at(i).GetBinContent(j) > 50){
				double x1 = xslices.at(i).GetBinError(j)/xslicesp.at(i).GetBinContent(j);
				double x2 = xslices.at(i).GetBinContent(j)*xslicesp.at(i).GetBinError(j)/xslicesp.at(i).GetBinContent(j)/xslicesp.at(i).GetBinContent(j);
				xe = sqrt(x1*x1+x2*x2);
				scalex = xslices.at(i).GetBinContent(j)/xslicesp.at(i).GetBinContent(j);
			}
			if(yslicesp.at(i).GetBinContent(j) > 50){
				double y1 = yslices.at(i).GetBinError(j)/yslicesp.at(i).GetBinContent(j);
				double y2 = yslices.at(i).GetBinContent(j)*yslicesp.at(i).GetBinError(j)/yslicesp.at(i).GetBinContent(j)/yslicesp.at(i).GetBinContent(j);
				ye = sqrt(y1*y1+y2*y2);
				scaley = yslices.at(i).GetBinContent(j)/yslicesp.at(i).GetBinContent(j);
			}
			xslices.at(i).SetBinContent(j, scalex);
			xslices.at(i).SetBinError(j, xe);
			yslices.at(i).SetBinContent(j, scaley);
			yslices.at(i).SetBinError(j, ye);
		}
	
		/*tiltx.SetBinContent(i+1, x);
		tiltx.SetBinError(i+1, 0.005);
		tilty.SetBinContent(i+1, y);
		tilty.SetBinError(i+1, 0.005);*/


		/*TF1 gx("gx", "[0]*(TMath::TanH([1]*(x-[2]+[3]))-TMath::TanH([1]*(x-[2]-[3])))/2.", -1.6, 1.6);
		TF1 gy("gy", "[0]*(TMath::TanH([1]*(x-[2]+[3]))-TMath::TanH([1]*(x-[2]-[3])))/2.", -1.6, 1.6);

		gx.SetParName(0, "N");
		gx.SetParameter(0, 0.05);
		gx.SetParLimits(0, 0.001, 0.1);	
	
		gx.SetParName(1, "#mu_{1}");
		gx.SetParameter(1, 23);
		gx.SetParLimits(1, 22, 32);
	
		gx.SetParName(2, "z_{0}");
		gx.SetParameter(2, 0.2);
		gx.SetParLimits(2, 0, 0.4);	
		
		gx.SetParName(3, "#delta");
		gx.SetParameter(3, 1.3);
		gx.SetParLimits(3, 1, 1.6);	
	
		gy.SetParName(0, "N");
		gy.SetParameter(0, 0.05);
		gy.SetParLimits(0, 0.001, 0.1);	
	
		gy.SetParName(1, "#mu_{1}");
		gy.SetParameter(1, 23);
		gy.SetParLimits(1, 22, 32);
	
		gy.SetParName(2, "z_{0}");
		gy.SetParameter(2, 0.2);
		gy.SetParLimits(2, 0, 0.4);	
		
		gy.SetParName(3, "#delta");
		gy.SetParameter(3, 1.3);
		gy.SetParLimits(3, 1, 1.6);		
	
		xslices.at(i).Fit(&gx, "R");
		yslices.at(i).Fit(&gy, "R");*/
		xslices.at(i).Write();
		yslices.at(i).Write();	
		/*gx.SetParName(0, "N");
		gx.SetParameter(0, 10000);
		gx.SetParLimits(0, 100, 200000);	
		gy.SetParName(0, "N");
		gy.SetParameter(0, 10000);
		gy.SetParLimits(0, 100, 200000);	
		gxp.SetParName(0, "N");
		gxp.SetParameter(0, 10000);
		gxp.SetParLimits(0, 100, 200000);		
		gyp.SetParName(0, "N");
		gyp.SetParameter(0, 10000);
		gyp.SetParLimits(0, 100, 200000);	
		
		gx.SetParName(1, "#mu");
		gx.SetParameter(1, xslices.at(i).GetMean());
		gx.SetParLimits(1, xslices.at(i).GetMean()-xslices.at(i).GetRMS(), xslices.at(i).GetMean()+xslices.at(i).GetRMS());
		gy.SetParName(1, "#mu");
		gy.SetParameter(1, yslices.at(i).GetMean());
		gy.SetParLimits(1, yslices.at(i).GetMean()-yslices.at(i).GetRMS(), yslices.at(i).GetMean()+yslices.at(i).GetRMS());	
		gxp.SetParName(1, "#mu");
		gxp.SetParameter(1, xslicesp.at(i).GetMean());
		gxp.SetParLimits(1, xslicesp.at(i).GetMean()-xslicesp.at(i).GetRMS(), xslicesp.at(i).GetMean()+xslicesp.at(i).GetRMS());
		gyp.SetParName(1, "#mu");
		gyp.SetParameter(1, yslicesp.at(i).GetMean());
		gyp.SetParLimits(1, yslicesp.at(i).GetMean()-yslicesp.at(i).GetRMS(), yslicesp.at(i).GetMean()+yslicesp.at(i).GetRMS());	
		
		gx.SetParName(2, "#sigma");
		gx.SetParameter(2, xslices.at(i).GetRMS());
		gx.SetParLimits(2, 0.5*xslices.at(i).GetRMS(), 1.5*xslices.at(i).GetRMS());
		gy.SetParName(2, "#sigma");
		gy.SetParameter(2, yslices.at(i).GetRMS());
		gy.SetParLimits(2, 0.5*yslices.at(i).GetRMS(), 1.5*yslices.at(i).GetRMS());	
		gxp.SetParName(2, "#sigma");
		gxp.SetParameter(2, xslicesp.at(i).GetRMS());
		gxp.SetParLimits(2, 0.5*xslicesp.at(i).GetRMS(), 1.5*xslicesp.at(i).GetRMS());
		gyp.SetParName(2, "#sigma");
		gyp.SetParameter(2, yslicesp.at(i).GetRMS());
		gyp.SetParLimits(2, 0.5*yslicesp.at(i).GetRMS(), 1.5*yslicesp.at(i).GetRMS());	

		cout << "***********************************************" << endl;
		cout << "FIT vertx" << endl;		
		xslices.at(i).Fit(&gx, "R");
		cout << "***********************************************" << endl;
		cout << "FIT verty" << endl;
		yslices.at(i).Fit(&gy, "R");
		cout << "***********************************************" << endl;
		cout << "FIT beamx" << endl;
		xslicesp.at(i).Fit(&gxp, "R");
		cout << "***********************************************" << endl;
		cout << "FIT beamy" << endl;
		yslicesp.at(i).Fit(&gyp, "R");*/


		/*tiltx.SetBinContent(i+1, gx.GetParameter(2));
		tiltx.SetBinError(i+1, gx.GetParError(2));
		tilty.SetBinContent(i+1, gy.GetParameter(2));
		tilty.SetBinError(i+1, gy.GetParError(2));	*/
		double xmin;
		double xmax;
		double ymin;
		double ymax;
		for(int j = 1; j < 201; j++){
			if(xslices.at(i).GetBinContent(j) != 0){
				xmin = xslices.at(i).GetBinCenter(j);
				break;
			}
		}
		for(int j = 201; j > 0; j--){
			if(xslices.at(i).GetBinContent(j) != 0){
				xmax = xslices.at(i).GetBinCenter(j);
				break;
			}		
		}
		
		for(int j = 1; j < 201; j++){
			if(yslices.at(i).GetBinContent(j) != 0){
				ymin = yslices.at(i).GetBinCenter(j);
				break;
			}
		}
		for(int j = 200; j > 0; j--){
			if(yslices.at(i).GetBinContent(j) != 0){
				ymax = yslices.at(i).GetBinCenter(j);
				break;
			}		
		}

		tiltx.SetBinContent(i+1, xmax-(xmax-xmin)/2.);
		tiltx.SetBinError(i+1, 0.0025);
		tilty.SetBinContent(i+1, ymax-(ymax-ymin)/2.);
		tilty.SetBinError(i+1, 0.0025);
				
		/*tiltx.SetBinContent(i+1, xslices.at(i).GetMean());
		tiltx.SetBinError(i+1, xslices.at(i).GetMeanError());
		tilty.SetBinContent(i+1, yslices.at(i).GetMean());
		tilty.SetBinError(i+1, yslices.at(i).GetMeanError());*/

		
		tiltxp.SetBinContent(i+1, xslicesp.at(i).GetMean());
		tiltxp.SetBinError(i+1, xslicesp.at(i).GetMeanError());
		tiltyp.SetBinContent(i+1, yslicesp.at(i).GetMean());
		tiltyp.SetBinError(i+1, yslicesp.at(i).GetMeanError());		
		
		/*tiltxp.SetBinContent(i+1, gxp.GetParameter(1));
		tiltxp.SetBinError(i+1, gxp.GetParError(1));
		tiltyp.SetBinContent(i+1, gyp.GetParameter(1));
		tiltyp.SetBinError(i+1, gyp.GetParError(1));	*/	
		

	}
	vertxz.Write();
	vertyz.Write();
	tiltx.Write();
	tilty.Write();
	tiltxp.Write();
	tiltyp.Write();
	xeh.Write();
	yeh.Write();
	outFile.Close();
		
}





