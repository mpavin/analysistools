#include <stdlib.h>
#include "TargetAnalysisConfig.h"
#include <fstream>
#include <fwk/CentralConfig.h>
#include <utl/Branch.h>
#include <utl/BranchIterator.h>
#include "CConfig.h"

using namespace std;
using namespace fwk;
using namespace utl; 

void TargetAnalysisConfig::Read(){


	/*if(cConfig == NULL){
		cout << __FUNCTION__ << ": Central config not initialized! Exiting..." << endl;
		exit (EXIT_FAILURE);
	}*/
	Branch dataAnaConf = cConfig.GetTopBranch("TargetAnalysis");

	if(dataAnaConf.GetName() != "TargetAnalysis"){
		cout << __FUNCTION__ << ": Wrong TargetAnalysis config file! Top branch name doesn't match! Please check your configuration files! Exiting..." << endl;
		exit (EXIT_FAILURE);
	}

	for(BranchIterator it = dataAnaConf.ChildrenBegin(); it != dataAnaConf.ChildrenEnd(); ++it){
		Branch child = *it;
		if(child.GetName() == "data"){
			child.GetData(fInputData);
			if(fInputData.at(0)=='$'){
				string run = fInputData.substr(1, fInputData.size()-1);
				//cout << run << endl;
				fInputData = getenv(run.c_str());
			}
		}

		else if(child.GetName() == "outputDir"){
			child.GetData(fOutputDir);
			if(fOutputDir.at(0)=='$'){
				string run = fOutputDir.substr(1, fOutputDir.size()-1);
				//cout << run << endl;
				fOutputDir = getenv(run.c_str());
			}
		}

		else if(child.GetName() == "runNumbers"){
			child.GetData(fRunNumbers);

		}
		
		else if(child.GetName() == "target"){
			vector<double> position;
			position.push_back(0);
			position.push_back(0);
			position.push_back(-657.43);
			
			double tiltX = 0;
			double tiltY = 0;

			ETargetType type = eLong;
			int iType = 0;
			string name = "long";
			double length = 90;
			double radius = 1.3;

			for(BranchIterator t = child.ChildrenBegin(); t != child.ChildrenEnd(); ++t){
				Branch targetChild = *t;

				if(targetChild.GetName() == "name"){
					targetChild.GetData(name);
				}
				else if(targetChild.GetName() == "type"){
					targetChild.GetData(iType);
				}
				else if(targetChild.GetName() == "position"){
					targetChild.GetData(position);
				}
				else if(targetChild.GetName() == "radius"){
					targetChild.GetData(radius);
				}
				else if(targetChild.GetName() == "tiltX"){
					targetChild.GetData(tiltX);
				}
				else if(targetChild.GetName() == "tiltY"){
					targetChild.GetData(tiltY);
				}
			}

			if(iType == 1) type = eThin;
			if(iType == 2) type = eNominalBeamline;
			SetTarget(name, type, length, radius, position.at(0), position.at(1), position.at(2), tiltX, tiltY);
		}
		else if(child.GetName() == "nSlices"){
			child.GetData(fNSlices);
		} 
	}
	
}

//*********************************************************************************************
void TargetAnalysisConfig::SetTarget(string name, ETargetType type, double length, double radius, double positionX, double positionY, double positionZ, double tilt1, double tilt2){

	TargetAnalysisConfig::fTarget.SetName(name);
	TargetAnalysisConfig::fTarget.SetType(type);
	TargetAnalysisConfig::fTarget.SetLength(length);
	TargetAnalysisConfig::fTarget.SetRadius(radius);
	TargetAnalysisConfig::fTarget.SetUpstreamPosition(positionX, positionY, positionZ);
	TargetAnalysisConfig::fTarget.SetTiltXZ(tilt1);
	TargetAnalysisConfig::fTarget.SetTiltYZ(tilt2);

}
//*********************************************************************************************
void TargetAnalysisConfig::ReadInputList(string inputData){
	fInputData = inputData;
	ReadInputList();
}

//*********************************************************************************************
void TargetAnalysisConfig::ReadInputList(){



	if (TargetAnalysisConfig::fInputData != " " ){
		ifstream ifs;
		string line;
		ifs.open(TargetAnalysisConfig::fInputData.c_str());
		cout << fInputData << endl;
		if (!ifs.is_open()){
			cerr << "[ReadInputListFile] Could not open infile. Exiting..." << endl;			
			exit (101);
		}
    
		while (getline(ifs, line)){
			if(line.at(0) != '#')
				TargetAnalysisConfig::fJob.push_back(line);
		}
		ifs.close();	
	
	}
	else cout << "WARNING: Empty file list! Jobs not created!" << endl;


}


//*********************************************************************************************
vector<string>& TargetAnalysisConfig::GetJob(){

	if(TargetAnalysisConfig::fJob.size() == 0)
		TargetAnalysisConfig::ReadInputList();

	return TargetAnalysisConfig::fJob;

}

//*********************************************************************************************



bool TargetAnalysisConfig::SkipRun(int runNumber){

	div_t res = div(fRunNumbers.size(),2);
	bool good = false;
	for(unsigned int i = 0; i < res.quot; i++){
		if(runNumber >= fRunNumbers.at(2*i) && runNumber < fRunNumbers.at(2*i+1)){
			good = true;
			break;
		}
	}
	return good;
}
