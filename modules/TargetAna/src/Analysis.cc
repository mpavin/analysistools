#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <cstdlib>
#include <stdexcept>
//#include <TPRegexp.h>

#include <TTree.h>
#include <TFile.h>
#include <TH2D.h>
#include <evt/Event.h>
#include <evt/EventHeader.h>
#include <io/EventFileChain.h>
#include <det/TriggerConst.h>
#include <det/BPDConst.h>
#include <evt/RawEvent.h>
#include <evt/SimEvent.h>
#include <boost/format.hpp>
#include "Analysis.h"
#include "TargetAnalysisConfig.h"
#include "TargetAna.h"
#include "AnaTrack.h"
#include <utl/Vector.h>
#include "EventSelection.h"
#include "TrackSelection.h"
#include "TrackExtrapolation.h"
#include "StrTools.h"
#include "TrackTools.h"

using namespace std;
using namespace io;
using namespace evt;
using namespace evt::rec;
using namespace evt::sim;
using namespace det;
using namespace utl;
using namespace boost;
using namespace StrTools;
using namespace TrackTools;

void Analysis(int n){

	TargetAnalysisConfig cfg;
	string fileName = cfg.GetOutputDir() + "/targetana.root";

	EventSelection evSel;
	TrackSelection trSel;
	EventFileChain eventFileChain(cfg.GetJob());
	Event event;


	unsigned int nEvents = 0;


	TFile *outputFile = new TFile(fileName.c_str(), "RECREATE");
	outputFile->cd();
	TTree* anaTree = new TTree("TargetAnalysis", "TargetAnalysis");
	anaTree->SetAutoSave(-300000000);

	AlignmentData aData;
	vector<double> x;
	vector<double> xE;
	vector<double> y;
	vector<double> yE;
	vector<double> z;
	vector<double> zS;
	//double zS = 0;
	vector<double> px;
	vector<double> py;
	vector<double> pz;
	vector<int> q;
	vector<int> topology;
	vector<int> nVTPC;
	vector<int> nGTPC;
	vector<int> nMTPC;
	/*int q;
	int topology;
	int nVTPC;
	int nGTPC;
	int nMTPC;*/
	//vector<double> pS;
	//vector<vector<double> > thetaS;

	double aX = 0;
	double bX = 0;
	double aY = 0;
	double bY = 0;
	unsigned int runNumber = 0;
	//anaTree->Branch("AlignmentData", &(aData.beamX), "beamX/D:beamY:beamSlopeX:beamInterceptX:beamSlopeY:beamInterceptY:px:py:pz:x:y:z:distToBeamTrack:q/I");

	anaTree->Branch("x", "vector<double>", &x);
	anaTree->Branch("xE", "vector<double>", &xE);
	anaTree->Branch("y", "vector<double>", &y);
	anaTree->Branch("yE", "vector<double>", &yE);
	anaTree->Branch("z", "vector<double>", &z);
	anaTree->Branch("zS", "vector<double>", &zS);
	//anaTree->Branch("zS", &zS, "zS/D");
	anaTree->Branch("px", "vector<double>", &px);
	anaTree->Branch("py", "vector<double>", &py);
	anaTree->Branch("pz", "vector<double>", &pz);
	anaTree->Branch("q", "vector<int>", &q);
	anaTree->Branch("topology", "vector<int>", &topology);
	anaTree->Branch("nVTPC", "vector<int>", &nVTPC);
	anaTree->Branch("nGTPC", "vector<int>", &nGTPC);
	anaTree->Branch("nMTPC", "vector<int>", &nMTPC);
	/*anaTree->Branch("q", &q, "q/I");
	anaTree->Branch("topology", &topology, "topology/I");
	anaTree->Branch("nVTPC", &nVTPC, "nVTPC/I");
	anaTree->Branch("nGTPC", &nGTPC, "nGTPC/I");
	anaTree->Branch("nMTPC", &nMTPC, "nMTPC/I");*/
	anaTree->Branch("aX", &aX, "aX/D");
	anaTree->Branch("aY", &aY, "aY/D");
	anaTree->Branch("bX", &bX, "bX/D");
	anaTree->Branch("bY", &bY, "bY/D");
	anaTree->Branch("runNumber", &runNumber, "runNumber/I");
	
	
	TargetAna target;
	TrackExtrapolation trackExtrapolation(true, 0.1, 30400.);
	trackExtrapolation.SetTarget(target);
	//TrackExtrapolation trkExtrapPos(cfg.GetTarget());
	AnaTrack anaTrack(trackExtrapolation);
	//AnaTrack anaTrackPos(trkExtrapPos);	
	double targetZ = cfg.GetTarget().GetUpstreamPosition().GetZ()-20;

	while (eventFileChain.Read(event) == eSuccess && nEvents < n) {

		bool skipRun = false;
		EventHeader &evHeader = event.GetEventHeader();

		x.clear();
		xE.clear();
		y.clear();
		yE.clear();
		z.clear();
		zS.clear();
		px.clear();
		py.clear();
		pz.clear();
		q.clear();
		topology.clear();
		nVTPC.clear();
		nGTPC.clear();
		nMTPC.clear();
		if(evHeader.GetRunNumber() != runNumber){
			runNumber = evHeader.GetRunNumber();
			skipRun = cfg.SkipRun(runNumber);
		}

		if(skipRun) continue;

   		++nEvents;
    
    	if (!(nEvents%1000))
      		cout << " --->Processing event # " << nEvents << endl;

		//******************************** MC ******************************************
		/*const SimEvent& simEvent = event.GetSimEvent();
		const evt::sim::Beam& beam = simEvent.GetBeam();
		
		aX = beam.GetMomentum().GetX()/beam.GetMomentum().GetZ();
		aY = beam.GetMomentum().GetY()/beam.GetMomentum().GetZ();
		bX = beam.GetOrigin().GetX() - aX*beam.GetOrigin().GetZ();
		bY = beam.GetOrigin().GetY() - aY*beam.GetOrigin().GetZ();
		//******************************** MC ******************************************
		//if(sqrt((aX*targetZ + bX+0.096)*(aX*targetZ + bX+0.096) + (aY*targetZ + bY-0.233)*(aX*targetZ + bX-0.233)) > 0.5 ) continue;
		/*if(evSel.IsEventGood(event) == false)
			continue;	
		*/

				
		const RecEvent& recEvent = event.GetRecEvent();
    	const Fitted2DLine lineX = event.GetRecEvent().GetBeam().Get(BPDConst::eX);
    	const Fitted2DLine lineY = event.GetRecEvent().GetBeam().Get(BPDConst::eY);

		aX = lineX.GetSlope();
		aY = lineY.GetSlope();
		bX = lineX.GetIntercept();
		bY = lineY.GetIntercept();    	
		target.SetType(eNominalBeamline);
		
		double xT = lineX.GetValueAtZ(targetZ) + 0.141;
		double yT = lineY.GetValueAtZ(targetZ) - 0.173;
		double zT = targetZ;
		target.SetUpstreamPosition(xT, yT, zT);
		target.SetTiltXZ(lineX.GetSlope());
		target.SetTiltYZ(lineY.GetSlope());
		target.SetLength(130);
		target.SetRadius(0);
		
		aData.beamX = lineX.GetValueAtZ(targetZ);
		aData.beamY = lineY.GetValueAtZ(targetZ);
		aData.beamSlopeX = lineX.GetSlope();
		aData.beamSlopeY = lineY.GetSlope();
		aData.beamInterceptX = lineX.GetIntercept();
		aData.beamInterceptY = lineY.GetIntercept();
		
		//beamX = lineX.GetValueAtZ(targetZ);
		//beamY = lineY.GetValueAtZ(targetZ);
		/*aX = lineX.GetSlope();
		bX = lineX.GetIntercept();
		aY = lineY.GetSlope();
		bY = lineY.GetIntercept();*/
		//cout << target.GetRadius() << " " << target.GetUpstreamPosition().GetX() << " " << target.GetUpstreamPosition().GetY() << " " << target.GetUpstreamPosition().GetZ() << endl;
		
		for (std::list<rec::Track>::const_iterator iter = recEvent.Begin<rec::Track>();
           iter != recEvent.End<rec::Track>(); ++iter)
      	{


			//cout << "radi" << endl;
			const Track& track = *iter;
		
			anaTrack.SetTrack(track);
			//cout << "radi1" << endl;
			if(!trSel.IsTrackGood(anaTrack)) continue;
			//cout << "radi2" << endl;
			//cout << "radi" << endl;
			//anaTrack.DoExtrapolation();

		
			/*aData.px = anaTrack.GetMomentum().GetX();
			aData.py = anaTrack.GetMomentum().GetY();
			aData.pz = anaTrack.GetMomentum().GetZ();
			aData.x = anaTrack.GetExtrapolatedPosition().GetX();
			aData.y = anaTrack.GetExtrapolatedPosition().GetY();
			aData.z = anaTrack.GetExtrapolatedPosition().GetZ();
			aData.distToBeamTrack = anaTrack.GetDistanceFromTarget();
			aData.q = track.GetCharge();*/
			

			//anaTrackPos.SetTrack(track);

			//double res[4] = {0, 0, 0, 0};
			//trkExtrapPos.FindTargetPosition(res);
			
			/*aData.xS = res[0];
			aData.yS = res[1];
			aData.zS = res[2];*/
			
			//double step = target.GetLength()/cfg.GetNSlices();

			//double zExtrap =target.GetUpstreamPosition().GetZ() + target.GetLength() - 0.5*step;
			//trackExtrapolation.Extrapolate(zExtrap);
			//double dist = target.GetDistanceFromTargetCenter(&tempD.Par[4], &tempD.Par[5], &tempD.Par[0]);
			
			//TrackPar &trackPar = trackExtrapolation.GetStopTrackParam();
			//double pzt = fabs(1/trackPar.GetPar(4))/sqrt(1 + trackPar.GetPar(2)*trackPar.GetPar(2) + trackPar.GetPar(3)*trackPar.GetPar(3));
			
			x.push_back(anaTrack.GetExtrapolatedPosition().GetX());
			xE.push_back(anaTrack.GetPositionErrors().at(0));
			y.push_back(anaTrack.GetExtrapolatedPosition().GetY());
			yE.push_back(anaTrack.GetPositionErrors().at(1));
			z.push_back(anaTrack.GetExtrapolatedPosition().GetZ());
			px.push_back(anaTrack.GetMomentum().GetX());
			py.push_back(anaTrack.GetMomentum().GetY());
			pz.push_back(anaTrack.GetMomentum().GetZ());
			/*xStemp.push_back(tempD.Par[4]);
			yStemp.push_back(tempD.Par[5]);
			zStemp.push_back(tempD.Par[0]);	
			
			pxStemp.push_back(tempD.Par[1]);
			pyStemp.push_back(tempD.Par[2]);
			pzStemp.push_back(tempD.Par[3]);	*/
			//double p = sqrt(tempD.Par[1]*tempD.Par[1]+tempD.Par[2]*tempD.Par[2]+tempD.Par[3]*tempD.Par[3]);
			
			//cout << p << endl;

			//thetaStemp.push_back(acos(tempD.Par[3]/p));
			//thetaStemp.push_back(acos(track.GetMomentum().GetZ()/track.GetMomentum().GetMag()));
			
			//int index = 0;
			/*for(int i = 0; i < cfg.GetNSlices()-1; ++i){
				//index = i;
				//double r = sqrt(tempD.Par[4]*tempD.Par[4] + tempD.Par[5]*tempD.Par[5]);
				zExtrap -= step;

				
				trackExtrapolation.Extrapolate(zExtrap);
				//double distTemp = target.GetDistanceFromTargetCenter(&temp.Par[4], &temp.Par[5], &temp.Par[0]);
				//if(distTemp>dist) break;
				
				//dist = distTemp;
				//double r1 = sqrt(tempD.Par[4]*tempD.Par[4] + tempD.Par[5]*tempD.Par[5]);
				pzt = fabs(1/trackPar.GetPar(4))/sqrt(1 + trackPar.GetPar(2)*trackPar.GetPar(2) + trackPar.GetPar(3)*trackPar.GetPar(3));
			
				x.push_back(trackPar.GetPar(0));
				xE.push_back(trackPar.GetStd(0));
				y.push_back(trackPar.GetPar(1));
				yE.push_back(trackPar.GetStd(1));
				z.push_back(zExtrap);
				px.push_back(pzt*trackPar.GetPar(2));
				py.push_back(pzt*trackPar.GetPar(3));
				pz.push_back(pzt);	
				
				//thetaStemp.push_back(acos(tempD.Par[3]/p));
			}*/
			//if(index == cfg.GetNSlices()-2) continue;
			//cout << dist << endl;
			q.push_back(track.GetCharge());
			topology.push_back(anaTrack.GetTopology());
			zS.push_back(track.GetFirstPointOnTrack().GetZ());
			nVTPC.push_back(anaTrack.GetNumberOfClusters().VTPC1 + anaTrack.GetNumberOfClusters().VTPC2);
			nGTPC.push_back(anaTrack.GetNumberOfClusters().GTPC);
			nMTPC.push_back(anaTrack.GetNumberOfClusters().MTPC);	
			/*q = track.GetCharge();
			topology = anaTrack.GetTopology();
			zS = track.GetFirstPointOnTrack().GetZ();
			nVTPCanaTrack.GetNumberOfClusters().VTPC1 + anaTrack.GetNumberOfClusters().VTPC2;
			nGTPC = anaTrack.GetNumberOfClusters().GTPC;
			nMTPC = anaTrack.GetNumberOfClusters().MTPC;	*/

			/*xS.push_back(xStemp);
			yS.push_back(yStemp);
         	zS.push_back(zStemp);
         	
         	pxS.push_back(pxStemp);
         	pyS.push_back(pyStemp);
         	pzS.push_back(pzStemp);*/


		}
		
			if(x.size() != 0){
				//cout << aX << " " << aY << " " << bX << " " << bY << endl;
				outputFile->cd();
				anaTree->Fill();		
			}
		

		/*if(xS.size()>1){
			outputFile->cd();
			anaTree->Fill();
		}*/
	}
	outputFile->cd();
	anaTree->Write();
	//trSel.WriteToRoot("","Results/cuts.root");
	outputFile->Close();

}

