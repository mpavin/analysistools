#include <vector>
#include <string>
#include <iostream>
#include "Functions.h"
#include "Constants.h"
#include <TH1D.h>
#include <TH2D.h>
#include <TFile.h>
#include <TChain.h>
#include <TTree.h>
#include <TMath.h>
#include <math.h>

using namespace std;


int main(int argc, char* argv[]){

	string input;
	string output;
	string opt = argv[1];
	
	if(opt == "0"){
		cout << "FLUKA 2011.2c.5" << endl;
		input = "list.txt";
		output = "/eos/user/m/mpavin/FLUKA.root";
	}
	else if(opt == "1"){
		cout << "NuBeam" << endl;
		input = "list1.txt";
		targX = 0.1153;
		output = "/eos/user/m/mpavin/NuBeam.root";
	}
	else if(opt == "2"){
		cout << "QGSP_BERT" << endl;
		input = "list2.txt";
		output = "/eos/user/m/mpavin/QGSPBert.root";
		targX = 0.1153;
	}
	else if(opt == "3"){
		cout << "FLUKA 2009" << endl;
		input = "list3.txt";
		output = "/eos/user/m/mpavin/FLUKA2009.root";
		targX = 0.16;
		targY = 0.21;
	}
        else if(opt == "4"){
	  cout << "FLUKA HMF" << endl;
	  input = "list4.txt";
	  output = "/eos/user/m/mpavin/FLUKAHMF.root";
	  targX = 0.1153;
	  targY = 0.1258;
        }

	TChain chain("h1000");
	

	ifstream ifs;
	string line;
	ifs.open(input.c_str());
		
	if (!ifs.is_open()){
		cerr << "[ReadInputListFile] Could not open infile. Exiting..." << endl;			
		exit (101);
	}
    
	while (getline(ifs, line)){
		if(line.at(0)=='#')
			continue;
		chain.Add(line.c_str());
	}
	ifs.close();		

	Int_t           protnum;
  	Int_t           ipart;
  	Float_t         mom;
  	Float_t         pos[3];
  	Float_t         vec[3];

  // Declaration of leaf types for replica input
  	Int_t           pgen;
  	Int_t           ng;
  	Float_t         gpx[20];    //[ng]
  	Float_t         gpy[20];    //[ng]
  	Float_t         gpz[20];    //[ng]
  	Float_t         gvx[20];    //[ng]
  	Float_t         gvy[20];    //[ng]
  	Float_t         gvz[20];    //[ng]
  	Int_t           gpid[20];   //[ng]
  	Int_t           gmec[20];   //[ng]

	TBranch        *b_protnum;   //!
  	TBranch        *b_ipart;     //!
  	TBranch        *b_mom;       //!
  	TBranch        *b_pos;       //!
  	TBranch        *b_vec;       //!
  	TBranch        *b_pgen;      //!
  	TBranch        *b_ng;        //!
  	TBranch        *b_gpx;       //!
  	TBranch        *b_gpy;       //!
  	TBranch        *b_gpz;       //!
  	TBranch        *b_gvx;       //!
  	TBranch        *b_gvy;       //!
  	TBranch        *b_gvz;       //!
  	TBranch        *b_gpid;      //!
  	TBranch        *b_gmec;      //!
    
	chain.SetBranchAddress("protnum", &protnum, &b_protnum);
  	chain.SetBranchAddress("ipart", &ipart, &b_ipart);
  	chain.SetBranchAddress("mom", &mom, &b_mom);
  	chain.SetBranchAddress("pos", pos, &b_pos);
  	chain.SetBranchAddress("vec", vec, &b_vec);
  	chain.SetBranchAddress("pgen", &pgen, &b_pgen);
  	chain.SetBranchAddress("ng", &ng, &b_ng);
  	chain.SetBranchAddress("gpx", gpx, &b_gpx);
  	chain.SetBranchAddress("gpy", gpy, &b_gpy);
  	chain.SetBranchAddress("gpz", gpz, &b_gpz);
  	chain.SetBranchAddress("gvx", gvx, &b_gvx);
  	chain.SetBranchAddress("gvy", gvy, &b_gvy);
  	chain.SetBranchAddress("gvz", gvz, &b_gvz);
  	chain.SetBranchAddress("gpid", gpid, &b_gpid);
  	chain.SetBranchAddress("gmec", gmec, &b_gmec);
  	
  	
  	int nEntries = chain.GetEntries();

	double xb;
	double yb;
	double zb;
	double txb;
	double tyb;
	vector<int> pdgId;
	vector<double> z;
	vector<double> p;
	vector<double> theta;
	vector<double> px;
	vector<double> py;
	
  	int eventId = -1;
  	int totev = 0;



	//nEntries = 10000000;

	TFile outFile(output.c_str(), "RECREATE");
	TTree *outTree = new TTree("MC", "MC");
	
	outTree->Branch("xb", &xb, "xb/D");
	outTree->Branch("yb", &yb, "yb/D");
	outTree->Branch("zb", &zb, "zb/D");
	outTree->Branch("txb", &txb, "txb/D");
	outTree->Branch("tyb", &tyb, "tyb/D");
	outTree->Branch("pdgId", "vector<int>", &pdgId);
	outTree->Branch("z", "vector<double>", &z);
	outTree->Branch("p", "vector<double>", &p);
	outTree->Branch("theta", "vector<double>", &theta);
	outTree->Branch("px", "vector<double>", &px);
	outTree->Branch("py", "vector<double>", &py);

  	for(int i = 0; i < nEntries; i++){
  		
  		chain.GetEntry(i);
		if(sqrt((gvx[0]-targX)*(gvx[0]-targX) + (gvy[0]-targY)*(gvy[0]-targY))>1.30) continue;
		

		if(i == 0){
			xb = gvx[0];
			yb = gvy[0];
			zb = gvz[0];
			txb = gpx[0]/gpz[0];
			tyb = gpy[0]/gpz[0];		
		}
		if(protnum != eventId){
				
			eventId = protnum;
			totev++;
			
			
			outTree->Fill();
			
			pdgId.clear();
			z.clear();
			p.clear();
			theta.clear();
			px.clear();
			py.clear();

			xb = gvx[0];
			yb = gvy[0];
			zb = gvz[0];
			txb = gpx[0]/gpz[0];
			tyb = gpy[0]/gpz[0];
			
  			if(!(totev%100000))
  				cout << "Processing event # " << totev << endl;
	
		}
		
		
		//if(ipart !=11 && ipart != 12) continue;
		
		if(sqrt((pos[0]-targX)*(pos[0]-targX) + (pos[1]-targY)*(pos[1]-targY))>1.32) continue;
		//if(ipart !=11 && ipart != 12 && ipart != 8 && ipart != 9 && ipart != 14) continue;
		if(ipart !=14) continue;
		if(1000*acos(vec[2])>400) continue;
		pdgId.push_back(ipart);
		z.push_back(pos[2]);
		p.push_back(mom);
		theta.push_back(1000*acos(vec[2]));
		px.push_back(mom*vec[0]);
		py.push_back(mom*vec[1]);
  	}
  	outTree->Fill();
  	
  	cout << totev << endl;
  	
  	outFile.cd();
	outTree->Write();
  	outFile.Close();
  	

}





