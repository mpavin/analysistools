#include <stdlib.h>

#include "BinOptimizationConfig.h"
#include <fstream>
#include <fwk/CentralConfig.h>
#include <utl/Branch.h>
#include <utl/BranchIterator.h>
#include "CConfig.h"
#include <math.h>  

using namespace std;
using namespace fwk;
using namespace utl; 

void BinOptimizationConfig::Read(){
	Branch spectrumPlot = cConfig.GetTopBranch("BinOptimization");

	if(spectrumPlot.GetName() != "BinOptimization"){
		cout << __FUNCTION__ << ": Wrong BinOptimiziation config file! Top branch name doesn't match! Please check your configuration files! Exiting..." << endl;
		exit (EXIT_FAILURE);
	}

	for(BranchIterator it = spectrumPlot.ChildrenBegin(); it != spectrumPlot.ChildrenEnd(); ++it){
		Branch child = *it;
		
		if(child.GetName() == "dEdxMap"){
			child.GetData(fdEdxMap);
		} 			
		else if(child.GetName() == "tofMap"){
			child.GetData(fTOFMap);
		}		
		else if(child.GetName() == "dEdxWidth"){
			child.GetData(fdEdxWidth);
		}		
		else if(child.GetName() == "targetZ"){
			child.GetData(fTargetZ);
		}
		else if(child.GetName() == "pid"){
			child.GetData(fParticleId);
		}
		else if(child.GetName() == "nSigma"){
			child.GetData(fNSigma);
		}
		else if(child.GetName() == "nMaxTracks"){
			child.GetData(fNMaxTracks);
		}
		else if(child.GetName() == "output"){
			child.GetData(fOutput);
		}
		else if(child.GetName() == "minBinSize"){
			child.GetData(fMinBinSize);
		}
		else if(child.GetName() == "RSTorWST"){
			child.GetData(fRST);
		}
		else if(child.GetName() == "mc"){
			child.GetData(fMC);
		}

	}

}

