#include <stdlib.h>
#include "Optimization.h"
#include "Functions.h"
#include "TreeDataTypes.h"
#include <sstream>

#include <TFile.h>
#include <TH2D.h>
#include <TCanvas.h>
#include <TTree.h>
#include <TBranch.h>



void Optimization::CalcdEdxTOFBins(){
	
	
	//*********************************************************
	TFile input(fInputFile.c_str(), "READ");
	TTree *tree = (TTree*) input.Get("Spectrum");
	SpectrumdEdxToF spectrum;
	TBranch *b_aData = tree->GetBranch("fSpectrumStruct");
 	b_aData->SetAddress(&spectrum);	
 	int nEntries = tree->GetEntries();
	//*********************************************************
	
	for(int i = 0; i < fPhaseSpace->GetDimBins(1); i++){
		int nTheta = fPhaseSpace->GetDimBins(2,i+1);

		for(int j = 0; j < nTheta; j++){

			for(int k = 0; k < fMomRangePos[i].at(j).size()-1; k++){
				vector<double> temp;
				fDatadEdxPos[i].at(j).push_back(temp);
				fDataTOFPos[i].at(j).push_back(temp);
			}
			for(int k = 0; k < fMomRangeNeg[i].at(j).size()-1; k++){
				vector<double> temp;
				fDatadEdxNeg[i].at(j).push_back(temp);
				fDataTOFNeg[i].at(j).push_back(temp);
			}
		}
	}
	
	for(int i = 0; i < nEntries; ++i){
		tree->GetEntry(i);

		if(!((i+1)%100000))
			cout << "Procesing entry # " << i+1 << " out of " << nEntries << "." << endl;

		double p = sqrt(spectrum.px*spectrum.px + spectrum.py*spectrum.py + spectrum.pz*spectrum.pz);
		double theta = 1000*acos(spectrum.pz / p);
		double z = spectrum.z - fConfig->GetTargetZPosition();	
	
	
		int indexZ = -999;
		int indexTheta = -999;
		int indexP = -999;
		
		bool selected = SelectParticle(p, spectrum.dEdx, spectrum.m2ToF);
		
		if(!selected) continue;
		
		for(int j = 0; j < fPhaseSpace->GetDimBins(1); j++){
			Bin &bin = fPhaseSpace->GetBin(j+1, 1, 1);	
			if(bin.GetLowValue(1) <= z && bin.GetHighValue(1) > z){
				indexZ = j;
				break;
			}
		}
		
		if(indexZ < 0) continue;
		
		int nTheta = fPhaseSpace->GetDimBins(2, indexZ+1);
		for(int j = 0; j < nTheta; j++){
			Bin &bin = fPhaseSpace->GetBin(indexZ+1, j+1, 1);	
			if(bin.GetLowValue(2) <= theta && bin.GetHighValue(2) > theta){
				indexTheta = j;
				break;
			}			
		}
		
		if(indexTheta < 0) continue;
		
		if(spectrum.q > 0){
			for(int j = 0; j < fMomRangePos[indexZ].at(indexTheta).size()-1; j++){
				if(fMomRangePos[indexZ].at(indexTheta).at(j) <= p && p < fMomRangePos[indexZ].at(indexTheta).at(j+1)){
					indexP = j;
					break;					
				}
			}	
		}
		else if(spectrum.q < 0){
			for(int j = 0; j < fMomRangeNeg[indexZ].at(indexTheta).size()-1; j++){
				if(fMomRangeNeg[indexZ].at(indexTheta).at(j) <= p && p < fMomRangeNeg[indexZ].at(indexTheta).at(j+1)){
					indexP = j;
					break;					
				}
			}	
		}
		
		if(indexP < 0) continue;
		
		if(spectrum.q > 0){
			fDatadEdxPos[indexZ].at(indexTheta).at(indexP).push_back(spectrum.dEdx);
			fDataTOFPos[indexZ].at(indexTheta).at(indexP).push_back(spectrum.m2ToF);
		}
		else if(spectrum.q < 0){
			fDatadEdxNeg[indexZ].at(indexTheta).at(indexP).push_back(spectrum.dEdx);
			fDataTOFNeg[indexZ].at(indexTheta).at(indexP).push_back(spectrum.m2ToF);		
		}
	}
		
	for(int i = 0; i < fPhaseSpace->GetDimBins(1); i++){
		for(int j = 0; j < fDatadEdxPos[i].size(); j++){
			for(int k = 0; k < fDatadEdxPos[i].at(j).size(); k++){
				fdEdxBinSizePos[i].at(j).push_back(GetHistoBinSize(fDatadEdxPos[i].at(j).at(k)));
				fTOFBinSizePos[i].at(j).push_back(GetHistoBinSize(fDataTOFPos[i].at(j).at(k)));
			}
			for(int k = 0; k < fDatadEdxNeg[i].at(j).size(); k++){
				fdEdxBinSizeNeg[i].at(j).push_back(GetHistoBinSize(fDatadEdxNeg[i].at(j).at(k)));
				fTOFBinSizeNeg[i].at(j).push_back(GetHistoBinSize(fDataTOFNeg[i].at(j).at(k)));
			}
		}
	}
	
	WriteToXML(1);
	WriteToXML(-1);
}



double Optimization::GetHistoBinSize(vector<double>& data){

	//Uses Freedman-Diaconis rule for determining histogram bin size
	//h = 2*IQR*n^(-1/3)
	//h = bin size
	//IQR = interquartile range, Q3-Q1
	//n = number of measurements
	
	std::sort(data.begin(), data.end());

	
	double q1;
	double q3;
	int n = data.size();
	//cout << n << endl;
	if(n==0) return 0.02;
	if(n<5) return 0.02;
	
	double nD = (double) n;
	return (data.at(data.size()-2)-data.at(2))/sqrt(nD);
	if(n % 2 == 0){
		int sizeQ = n/2;
		
		if(sizeQ % 2 == 0){
			int sizeQ1 = sizeQ/2;
			q1 = (data.at(sizeQ1-1) + data.at(sizeQ1))/2.;
			q3 = (data.at(sizeQ+sizeQ1-1) + data.at(sizeQ+sizeQ1))/2.;
		}
		else{
			int sizeQ1 = sizeQ/2;
			q1 = data.at(sizeQ1);
			q3 = data.at(sizeQ + sizeQ1);
		}

	}
	else{
		int sizeQ = n/2;
		if(sizeQ % 2 == 0){
			int sizeQ1 = sizeQ/2;
			q1 = (data.at(sizeQ1-1) + data.at(sizeQ1))/2.;
			q3 = (data.at(sizeQ+sizeQ1) + data.at(sizeQ+sizeQ1 + 1))/2.;
		}
		else{
			int sizeQ1 = sizeQ/2;
			q1 = data.at(sizeQ1);
			q3 = data.at(sizeQ + sizeQ1 + 1);
		}
	}
	double nV = (double) n;

	return 2*(q3-q1)*pow(nV, -1/3);
	
}
