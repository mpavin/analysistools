#include <iostream>
#include <fstream>
#include "Optimization.h"
#include "StrTools.h" 
#include <stdlib.h>
 
using namespace StrTools;

void Optimization::WriteToXML(int q){
	
	string add;
	if(fConfig->DoRST()){
		add="RST";
	}
	else{
		add="WST";
	}	
	string fileName;
	if(q > 0){
		fileName = fConfig->GetOutputDir() + "/PhaseSpace+" + add + "_" + ConvertToString(fConfig->GetParticleId()) + ".xml";
	}
	else if( q < 0){
		fileName = fConfig->GetOutputDir() + "/PhaseSpace-" + add + "_" + ConvertToString(fConfig->GetParticleId()) + ".xml";
	}
	
	ofstream xmlfile (fileName.c_str());
	
	if(xmlfile.is_open()){

		xmlfile << "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n";
		xmlfile << "<PhaseSpace>\n";
		xmlfile << "<!-- Bin or var types: 1 - p, 2 - pt, 4 - pL, 8 - y, 16 - x, 32 - theta, 64 - z, 128 - m_inv, 256 - dE/dx, 512 - m2_TOF -->\n";
		xmlfile << "\t<header>\n";
		xmlfile << "\t\t<dim>\n";
		xmlfile << "\t\t\t<name> z </name>\n";
		xmlfile << "\t\t\t<unit> cm </unit>\n";
		xmlfile << "\t\t\t<type> 64 </type>\n";
		xmlfile << "\t\t</dim>\n";

		xmlfile << "\t\t<dim>\n";
		xmlfile << "\t\t\t<name> #theta </name>\n";
		xmlfile << "\t\t\t<unit> mrad </unit>\n";
		xmlfile << "\t\t\t<type> 32 </type>\n";
		xmlfile << "\t\t</dim>\n";
		
		xmlfile << "\t\t<dim>\n";
		xmlfile << "\t\t\t<name> p </name>\n";
		xmlfile << "\t\t\t<unit> GeV/c </unit>\n";
		xmlfile << "\t\t\t<type> 1 </type>\n";
		xmlfile << "\t\t</dim>\n";
		
		xmlfile << "\t\t<var>\n";
		xmlfile << "\t\t\t<name> dE/dx </name>\n";
		xmlfile << "\t\t\t<unit> a.u. </unit>\n";
		xmlfile << "\t\t\t<type> 256 </type>\n";
		xmlfile << "\t\t</var>\n";
		
		xmlfile << "\t\t<var>\n";
		xmlfile << "\t\t\t<name> m_{TOF}^{2} </name>\n";
		xmlfile << "\t\t\t<unit> GeV^2/c^4 </unit>\n";
		xmlfile << "\t\t\t<type> 512 </type>\n";
		xmlfile << "\t\t</var>\n";
		xmlfile << "\t</header>\n";
		
		for(int i = 0; i < fPhaseSpace->GetDimBins(1); i++){
			Bin& bin = fPhaseSpace->GetBin(i+1,1,1);
			xmlfile << "\t<bin>\n";
			xmlfile << "\t\t<low1> " << ConvertToString(bin.GetLowValue(1),2) << " </low1>\n";
			xmlfile << "\t\t<high1> " << ConvertToString(bin.GetHighValue(1),2) << " </high1>\n";
			
			int nTheta = fPhaseSpace->GetDimBins(2, i+1);
			//cout << "*************************" <<endl;
			for(int j = 0; j < nTheta; j++){
			
			  if(q>0 && fMomRangePos[i].at(j).size() < 4) continue;
			  if(q<0 && fMomRangeNeg[i].at(j).size() < 4) continue;
				Bin& bin1 = fPhaseSpace->GetBin(i+1,j+1,1);
				xmlfile << "\t\t<subbin>\n";
				xmlfile << "\t\t\t<low2> " << ConvertToString(bin1.GetLowValue(2),0) << " </low2>\n";
				xmlfile << "\t\t\t<high2> " << ConvertToString(bin1.GetHighValue(2),0) << " </high2>\n";
				
				string low3;
				string high3;
				string dEdxBin;
				string tofBin;
				//cout << fMomRangePos[i].at(j).size() << endl;
				if(q > 0){
					for(int k = 0; k < fMomRangePos[i].at(j).size() -1; k++){
						
						low3 = low3 + ConvertToString(fMomRangePos[i].at(j).at(k),2) + " ";
						high3 = high3 + ConvertToString(fMomRangePos[i].at(j).at(k+1),2) + " ";
						//dEdxBin = dEdxBin + ConvertToString(fdEdxBinSizePos[i].at(j).at(k),3) + " ";
						//tofBin = tofBin + ConvertToString(fTOFBinSizePos[i].at(j).at(k),3) + " ";
					}
				}
				else if(q < 0){
					for(int k = 0; k < fMomRangeNeg[i].at(j).size() -1; k++){
						low3 = low3 + ConvertToString(fMomRangeNeg[i].at(j).at(k),2) + " ";
						high3 = high3 + ConvertToString(fMomRangeNeg[i].at(j).at(k+1),2) + " ";
						//dEdxBin = dEdxBin + ConvertToString(fdEdxBinSizeNeg[i].at(j).at(k),3) + " ";
						//tofBin = tofBin + ConvertToString(fTOFBinSizeNeg[i].at(j).at(k),3) + " ";					
					}				
				}
				xmlfile << "\t\t\t<low3> " << low3 << "</low3>\n";
				xmlfile << "\t\t\t<high3> " << high3 << "</high3>\n";
				xmlfile << "\t\t\t<varBinSize1> " << dEdxBin << "</varBinSize1>\n";
				xmlfile << "\t\t\t<varBinSize2> " << tofBin << "</varBinSize2>\n";
				xmlfile << "\t\t</subbin>\n";				
			}

			xmlfile << "\t</bin>\n";
		}
		xmlfile << "</PhaseSpace>";
	}
	else{
		cerr << __FUNCTION__ << ": Unable to open file " << fileName << ". Exiting ..." << endl;
		exit(EXIT_FAILURE);
	}
}
