#include <stdlib.h>
#include "Optimization.h"
#include "PlotHisto.h"
#include "dEdxResolution.h"
#include "TOFResolution.h"
#include "TreeDataTypes.h"
#include <sstream>
#include <fstream>
#include <TChain.h>
#include <TFile.h>
#include <TH2D.h>
#include <TCanvas.h>
#include <TTree.h>
#include <TBranch.h>
#include <TLine.h>
#include "StrTools.h"
#include <math.h>

using namespace StrTools;

Optimization::Optimization(string inputFile, bool isList){
	fInputFile = inputFile;
	fIsList = isList;
	
	fConfig = new BinOptimizationConfig();
	fPhaseSpace = new PhaseSpace();

	const int n = fPhaseSpace->GetDimBins(1);
	fMomRangePos = new vector<vector<double> >[n];
	fMomRangeNeg = new vector<vector<double> >[n];	
	fDatadEdxPos = new vector<vector<vector<double> > >[n];
	fDatadEdxNeg = new vector<vector<vector<double> > >[n];
	fDataTOFPos = new vector<vector<vector<double> > >[n];
	fDataTOFNeg = new vector<vector<vector<double> > >[n];
	fdEdxBinSizePos = new vector<vector<double> >[n];
	fTOFBinSizePos = new vector<vector<double> >[n];	
	fdEdxBinSizeNeg = new vector<vector<double> >[n];
	fTOFBinSizeNeg = new vector<vector<double> >[n];
	for(int i = 0; i < n; i++){
		
		vector<vector<double> > tempP;

		vector<vector<vector<double > > > tempData;
		
		int nTheta = fPhaseSpace->GetDimBins(2,i+1);
		for(int j = 0; j < nTheta; j++){
			vector<double> thetap;
			vector<vector<double> > data;
			tempP.push_back(thetap);
			tempData.push_back(data);
		}
		
		fMomRangePos[i] = tempP;
		fMomRangeNeg[i] = tempP;
		fdEdxBinSizePos[i] = tempP;
		fTOFBinSizePos[i] = tempP;
		fdEdxBinSizeNeg[i] = tempP;
		fTOFBinSizeNeg[i] = tempP;
		fDatadEdxPos[i] = tempData;
		fDataTOFPos[i] = tempData;
		fDatadEdxNeg[i] = tempData;
		fDataTOFNeg[i] = tempData;
	}
	
	vector<double> ranges;
	for(int i = 0; i < 8; i++){
		ranges.push_back(0);
	}
	
	fdEdxRes = new dEdxResolution(fConfig->GetdEdxMap(), ranges);
	fTOFRes = new TOFResolution(fConfig->GetTOFMap(), ranges);
	
	//*********************************************************
	//Particle masses: 1-e, 2-pi, 3-K, 4-p
	fMasses.insert(std::pair<int,double>(11,0.000511));
	fMasses.insert(std::pair<int,double>(211,0.139570));
	fMasses.insert(std::pair<int,double>(321,0.493667));
	fMasses.insert(std::pair<int,double>(2212,0.938272));
	//*********************************************************
}

void Optimization::Run(){

	//*********************************************************

	TChain chain("Data");

	if(fIsList){
		ifstream ifs;
		string line;
		
		ifs.open(fInputFile.c_str());
		
		if (!ifs.is_open()){
			cerr << "[ReadInputListFile] Could not open infile. Exiting..." << endl;			
			exit (101);
		}
    
		while (getline(ifs, line)){
			if(line.at(0)=='#')
				continue;
				
			chain.Add(line.c_str());
		}
		ifs.close();		
	}
	else {
		chain.Add(fInputFile.c_str());
	}
	//TFile input(fInputFile.c_str(), "READ");
	//TTree *tree = (TTree*) input.Get("Data");
	chain.LoadTree(0);

	BeamA beamD;
	vector<double> *x = NULL;
	vector<double> *xE = NULL;
	vector<double> *y = NULL;
	vector<double> *yE = NULL;
	vector<double> *z = NULL;
	vector<double> *px = NULL;
	vector<double> *py = NULL;
	vector<double> *pz = NULL;
	vector<double> *distTarg = NULL;
	vector<double> *zStart = NULL;
	vector<double> *dEdx = NULL;
	vector<int> *qD = NULL;
	vector<int> *nVTPC1 = NULL;
	vector<int> *nVTPC2 = NULL;
	vector<int> *nGTPC = NULL;
	vector<int> *nMTPC = NULL;
	vector<int> *topology = NULL;
	vector<int> *nTOF = NULL;
	vector<double> *m2TOF = NULL;	
	vector<int> *tofScId = NULL;
	vector<int> *pdgId = NULL;
	int runNumber;
	
	TBranch *b_aData = chain.GetBranch("Beam");
 	b_aData->SetAddress(&beamD);
	chain.SetBranchAddress("x", &x);
	chain.SetBranchAddress("xE", &xE);
	chain.SetBranchAddress("y", &y);
	chain.SetBranchAddress("yE", &yE);
	chain.SetBranchAddress("z", &z);
	chain.SetBranchAddress("px", &px);
	chain.SetBranchAddress("py", &py);
	chain.SetBranchAddress("pz", &pz);
	chain.SetBranchAddress("distTarg", &distTarg);
	chain.SetBranchAddress("zStart", &zStart);
	chain.SetBranchAddress("dEdx", &dEdx);
	chain.SetBranchAddress("q", &qD);
	chain.SetBranchAddress("nVTPC1", &nVTPC1);
	chain.SetBranchAddress("nVTPC2", &nVTPC2);
	chain.SetBranchAddress("nGTPC", &nGTPC);
	chain.SetBranchAddress("nMTPC", &nMTPC);
	chain.SetBranchAddress("topology", &topology);
	chain.SetBranchAddress("nTOF", &nTOF);
	chain.SetBranchAddress("m2TOF", &m2TOF);
	chain.SetBranchAddress("tofScId", &tofScId);
	chain.SetBranchAddress("runNumber", &runNumber);
	//chain.SetBranchAddress("pdgId", &pdgId);
	
	//*********************************************************
	string add;
	if(fConfig->DoRST()){
		add="RST";
	}
	else{
		add="WST";
	}
	string name = fConfig->GetOutputDir() + "/BinOptimization_" + add + GetTimeString() + ".root";
	
	vector<PlotHisto> zBins;

	for(int i = 0; i < fPhaseSpace->GetDimBins(1); i++){
		string add = "_bin" + ConvertToString(i+1);
		
		PlotHisto zTemp(2, "OptimizationHistos", "zPos", "zNeg", "zAll", add, NULL, NULL, NULL, NULL);
		zBins.push_back(zTemp);
		
	}
	
	//TTree *tree = chain.GetTree();
	//cout << tree->GetEntries() << endl;
	//*********************************************************
	
	int nEntries = chain.GetEntries();
	//cout << nEntries << endl;
	for(int i = 0; i < nEntries; ++i){
		chain.GetEntry(i, 0);
		
		if(!((i+1)%100000))
			cout << "Procesing entry # " << i+1 << " out of " << nEntries << "." << endl;
		
		for(int j = 0; j < x->size(); j++){
			double p = sqrt(px->at(j)*px->at(j) + py->at(j)*py->at(j) + pz->at(j)*pz->at(j));
			double theta = 1000*acos(pz->at(j) / p);
			double zV = z->at(j) - fConfig->GetTargetZPosition();
		
			if(fConfig->DoRST() == 0){
				if(qD->at(j)*px->at(j) > 0) 
					continue;
			}
			else if(fConfig->DoRST() == 1){
				if(qD->at(j)*px->at(j) < 0) 
					continue;			
			}
			bool selected = false;
			if(fConfig->IsMC()){
				//cout << "radi1mc" << endl;
				if(pdgId->at(j) == fConfig->GetParticleId())
					selected = true;
			}
			else{
				//cout << "radi1" << endl;
				selected = SelectParticle(p, dEdx->at(j), m2TOF->at(j));
				//cout << "radi2" << endl;
			}
			if(!selected) continue;
		
			int index = -999;

			for(int k = 0; k < fPhaseSpace->GetDimBins(1); k++){

				Bin &bin = fPhaseSpace->GetBin(k+1, 1, 1);
			
				if(bin.GetLowValue(1) <= zV && bin.GetHighValue(1) > zV){
					index = k;
					break;
				}

			}

			if(index < 0) continue;
		
			if(qD->at(j) > 0){
				zBins.at(index).Fill(1, p, theta);
			}
			else if(qD->at(j) < 0){
				zBins.at(index).Fill(2, p, theta);
			}
		}
	}
	//*********************************************************
	
	
	
	TFile output(name.c_str(), "RECREATE");	
	output.cd();
	output.mkdir("Positive");
	output.mkdir("Negative");

	for(int j = 0; j < fPhaseSpace->GetDimBins(1); j++){
		output.cd("Positive");
		zBins.at(j).GetRSTHisto()->Write();
		
		int nTheta = fPhaseSpace->GetDimBins(2, j+1);
		vector<double> ranges;

		for(int k = 0; k < nTheta; k++){
			Bin& bin = fPhaseSpace->GetBin(j+1, k+1, 1);
			ranges.push_back(bin.GetLowValue(2));
			
			if(k == nTheta-1)
				ranges.push_back(bin.GetHighValue(2));
		}
		OptimizeBins(1,j+1, zBins.at(j).GetRSTHisto(), ranges);

		
		string canvasName = "cPos" + ConvertToString(j+1);
		TCanvas cPos(canvasName.c_str(), "", 800, 600);
		cPos.cd();
		zBins.at(j).GetRSTHisto()->Draw("colz");
		cPos.Update();
		
		for(int k = 0; k < nTheta; k++){

			int sizeP = fMomRangePos[j].at(k).size();
			if(sizeP < 4) continue;

			TLine *thLMin = new TLine(fMomRangePos[j].at(k).at(0), ranges.at(k), fMomRangePos[j].at(k).at(sizeP-1), ranges.at(k));

			thLMin->SetLineColor(2);
			thLMin->SetLineWidth(2);
			thLMin->Draw();
			

			TLine *thLMax = new TLine(fMomRangePos[j].at(k).at(0), ranges.at(k+1), fMomRangePos[j].at(k).at(sizeP-1), ranges.at(k+1));
			thLMax->SetLineColor(2);
			thLMax->SetLineWidth(2);
			thLMax->Draw();

			for(int l = 0; l < sizeP; l++){
				TLine *tempL = new TLine(fMomRangePos[j].at(k).at(l), ranges.at(k), fMomRangePos[j].at(k).at(l), ranges.at(k+1));
				tempL->SetLineColor(2);
				tempL->SetLineWidth(2);
				tempL->Draw();
			}

		}
		cPos.Write();
		
		
		
		output.cd("Negative");
		zBins.at(j).GetWSTHisto()->Write();
		
		OptimizeBins(-1, j+1, zBins.at(j).GetWSTHisto(), ranges);
		
		canvasName = "cNeg" + ConvertToString(j+1);
		TCanvas cNeg(canvasName.c_str(), "", 800, 600);
		cNeg.cd();
		
		zBins.at(j).GetWSTHisto()->Draw("colz");
		cNeg.Update();
		
		for(int k = 0; k < nTheta; k++){
			int sizeP = fMomRangeNeg[j].at(k).size();
			if(sizeP < 4) continue;
			
			TLine *thLMin = new TLine(fMomRangeNeg[j].at(k).at(0), ranges.at(k), fMomRangeNeg[j].at(k).at(sizeP-1), ranges.at(k));
			thLMin->SetLineColor(2);
			thLMin->SetLineWidth(2);
			thLMin->Draw();
			
			TLine *thLMax = new TLine(fMomRangeNeg[j].at(k).at(0), ranges.at(k+1), fMomRangeNeg[j].at(k).at(sizeP-1), ranges.at(k+1));
			thLMax->SetLineColor(2);
			thLMax->SetLineWidth(2);
			thLMax->Draw();
			
			for(int l = 0; l < sizeP; l++){
				TLine *tempL = new TLine(fMomRangeNeg[j].at(k).at(l), ranges.at(k), fMomRangeNeg[j].at(k).at(l), ranges.at(k+1));
				tempL->SetLineColor(2);
				tempL->SetLineWidth(2);
				tempL->Draw();
			}
			
		}
		cNeg.Write();
	}

	//input.Close();
	output.Close();
	//*********************************************************
	
}



void Optimization::OptimizeBins(int q, int zBin, TH1* hist, vector<double>& ranges){
	if(ranges.size()<2){
		cerr << __FUNCTION__ << ": there should be at least one theta bin. Exiting ..." << endl;
		exit(EXIT_FAILURE);
	}
	
	vector<vector<double> > *fMomRange;
	
	if(q > 0){
		fMomRange = fMomRangePos;
	}
	else if(q < 0){
		fMomRange = fMomRangeNeg;
	}
	else{
		cerr << __FUNCTION__ << ": Charge cannot be 0. Exiting ..." << endl;
		exit(EXIT_FAILURE);
	}
	int nx = hist->GetNbinsX();
	int ny = hist->GetNbinsY();
	
	
	
	for(int i = 0; i < ranges.size()-1; i++){
		bool start = false;
		double min = 0;
		double max = 0;
		double count = 0;
		double last[5] = {0,0,0,0,0};
		for(int j = 1; j <= nx; j++){
			double p = hist->GetXaxis()->GetBinLowEdge(j);
			for(int k = 1; k <= ny; k++){
				
				double theta = hist->GetYaxis()->GetBinLowEdge(k);
				if(ranges.at(i) > theta) continue;
				if(theta >= ranges.at(i+1)) break;
				double ent = hist->GetBinContent(j,k);
				if(!start){
					if(ent != 0){
						min = p;
						start = true;
						fMomRange[zBin-1].at(i).push_back(min);
						count += ent;
					}
				}
				else{
					count += ent;
					max = p + hist->GetXaxis()->GetBinWidth(j);
					if(ent != 0){
						last[4] = last[3];
						last[3] = last[2];
						last[2] = last[1];
						last[1] = last[0];
						last[0] = max;
					}
				}
			}
			if(count > fConfig->GetNMaxTracks() && (max-min) > fConfig->GetMinBinSize()){
				min = max;
				fMomRange[zBin-1].at(i).push_back(min);
				count = 0;
			}
				
		}
		
		
		if(count > fConfig->GetNMaxTracks()*0.1)
			fMomRange[zBin-1].at(i).push_back(last[4]);
	}
}



bool Optimization::SelectParticle(double p, double dEdx, double m2TOF){
		
		bool selected = false;
		int n = fConfig->GetParticleId();
		double nSigma = fConfig->GetNumberOfSigmas();
		if(n == 11){
			double bg = p/fMasses[n];
			double tofDist = fabs(fTOFRes->GetElectronMean(p)-m2TOF)/fTOFRes->GetElectronWidth(p);
			double dEdxDist = fabs(fdEdxRes->GetMean(bg)-dEdx)/fdEdxRes->GetWidth(bg);
			
			if(tofDist <= nSigma && dEdxDist <= nSigma)
				selected = true;
		}
		else if(n == 211){
			double bg = p/fMasses[n];
			double tofDist = fabs(fTOFRes->GetPionMean(p)-m2TOF)/fTOFRes->GetPionWidth(p);
			double dEdxDist = fabs(fdEdxRes->GetMean(bg)-dEdx)/fdEdxRes->GetWidth(bg);
			
			if(tofDist <= nSigma && dEdxDist <= nSigma)
				selected = true;
		}
		else if(n == 321){
			double bg = p/fMasses[n];
			double tofDist = fabs(fTOFRes->GetKaonMean(p)-m2TOF)/fTOFRes->GetKaonWidth(p);
			double dEdxDist = fabs(fdEdxRes->GetMean(bg)-dEdx)/fdEdxRes->GetWidth(bg);
			
			if(tofDist <= nSigma && dEdxDist <= nSigma)
				selected = true;
		}
		else if(n == 2212){
			double bg = p/fMasses[n];
			double tofDist = fabs(fTOFRes->GetProtonMean(p)-m2TOF)/fTOFRes->GetProtonWidth(p);
			double dEdxDist = fabs(fdEdxRes->GetMean(bg)-dEdx)/fdEdxRes->GetWidth(bg);
			
			if(tofDist <= nSigma && dEdxDist <= nSigma)
				selected = true;
		}
		else{
			cerr << __FUNCTION__ << ": Wrong particle id! Only pi+(-), K+(-), e+(-) and p are possible. Check you config files! Exiting ..." << endl;
			exit(EXIT_FAILURE);
		}
		
		return selected;
}
