#ifndef BINOPTIMIZATIONCONFIG_H
#define BINOPTIMIZATIONCONFIG_H
#include <iostream>
#include <vector>
#include <string>

#include <vector>



using namespace std;

class BinOptimizationConfig{

	public:
		BinOptimizationConfig(){Read();}


		string GetOutputDir(){return fOutput;}
		double GetdEdxWidth(){return fdEdxWidth;}
		string GetdEdxMap(){return fdEdxMap;}
		string GetTOFMap(){return fTOFMap;}
		double GetTargetZPosition(){return fTargetZ;}
		int GetParticleId(){return fParticleId;}
		double GetNumberOfSigmas(){return fNSigma;}
		double GetNMaxTracks(){return fNMaxTracks;}
		double GetMinBinSize(){return fMinBinSize;}
		int DoRST(){return fRST;}
		bool IsMC(){return fMC;}
		
		void SetdEdxWidth(double val){fdEdxWidth = val;}
		void SetdEdxMap(string map){fdEdxMap = map;}
		void SetTOFMap(string map){fTOFMap = map;}
		void SetTargetZPosition(double z){fTargetZ = z;}
		void SetParticleId(int id){fParticleId = id;}
		void SetOutputDir(string value){fOutput = value;}
		void SetNumberOfSigmas(double val){fNSigma = val;}
		void SetNMaxTracks(double n){fNMaxTracks = n;}
		void SetMinBinSize(double val){fMinBinSize = val;}
		void SetDoRST(bool rst){fRST = rst;}
		void SetMC(bool mc){fMC = mc;}
	private:

		void Read();
		double fdEdxWidth;
		double fTargetZ;
		double fNSigma;
		int fParticleId;
		double fNMaxTracks;
		double fMinBinSize;
		string fdEdxMap;
		string fTOFMap;
		string fOutput;
		int fRST;
		bool fMC;
		bool fPDGId;
		

};

#endif
