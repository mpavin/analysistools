#ifndef OPTIMIZATION_H
#define OPTIMIZATION_H

#include "BinOptimizationConfig.h"
#include "dEdxResolution.h"
#include "TOFResolution.h"
#include "PhaseSpace.h"
#include <iostream>
#include <vector>
#include <string>
#include <TH1D.h>
#include <map>

using namespace std;

class Optimization{

	public:
		//Optimization(string inputFile): fInputFile(inputFile) {fConfig = new BinOptimizationConfig(); fMomRangePos = NULL; fMomRangeNeg = NULL;}
		Optimization(string inputFile, bool isList);
		string GetInputFile(){return fInputFile;}
		void SetInputFile(string inputFile){fInputFile = inputFile;}
		
		void Run();
		void WriteToXML(int q);
		void CalcdEdxTOFBins();
	private:
		
		void OptimizeBins(int q, int zBin, TH1* hist, vector<double>& ranges);
		bool Comp(double a, double b);
		bool SelectParticle(double p, double dEdx, double m2TOF);
		
		double GetHistoBinSize(vector<double>& data);
		string fInputFile;
		bool fIsList;
		
		BinOptimizationConfig *fConfig;
		PhaseSpace *fPhaseSpace;
		dEdxResolution *fdEdxRes;
		TOFResolution *fTOFRes;
		
		vector<vector<double> > *fMomRangePos;
		vector<vector<vector<double> > > *fDatadEdxPos;
		vector<vector<vector<double> > > *fDataTOFPos;
		vector<vector<double> > *fdEdxBinSizePos;
		vector<vector<double> > *fTOFBinSizePos;
		
		vector<vector<double> > *fMomRangeNeg;
		vector<vector<vector<double> > > *fDatadEdxNeg;
		vector<vector<vector<double> > > *fDataTOFNeg;
		vector<vector<double> > *fdEdxBinSizeNeg;
		vector<vector<double> > *fTOFBinSizeNeg;
		map<int, double> fMasses;
		
		
};
#endif
