/*!
* \file
*/
#include <iostream>
#include "CConfig.h"
#include <fstream>
#include <sstream>
#include <iomanip>
#include <cstdlib>
#include <stdexcept>
#include <TTree.h>
#include <TFile.h>
#include <cstdlib>
#include <evt/Event.h>
#include <evt/EventHeader.h>
#include <io/EventFileChain.h>
#include <evt/RawEvent.h>
#include <utl/TimeStamp.h>
#include "dEdx.h"
#include "dEdxMapConfig.h"
#include "det/Detector.h"
#include "det/TPC.h"
#include "StrTools.h"

using namespace utl;
using namespace io;
using namespace evt;
using namespace evt::rec;
using namespace det;
using namespace boost;
using namespace StrTools;
/*!
 *	\fn 
 *	Function Run reads SHOE files. For this class EventFileChain is used. It needs vector containing paths to the SHOE files.
 *	This vector is obtained from the DataAnaConfig class.
*/

Detector &detector = Detector::GetInstance();

void dEdx(string &fileName){

	dEdxMapConfig cfg;
	
	vector<string> inputFiles;
	inputFiles.push_back(fileName);
	EventFileChain eventFileChain(inputFiles);
	Event event;
	int runNumber = 0;
	

	
	int nEvents = 0;
	while (eventFileChain.Read(event) == eSuccess && nEvents < 1) {
		nEvents++;
		EventHeader &evHeader = event.GetEventHeader();	
		runNumber = evHeader.GetRunNumber();
		const TimeStamp& timeStamp = evHeader.GetTime();
		
		detector.Update(timeStamp, runNumber);
	}
	

	const TPC& tpc = detector.GetTPC();

	int nbins = cfg.GetNBinsBetaGamma();
	double low = cfg.GetMinBetaGamma();
	double high = cfg.GetMaxBetaGamma();
	double width = cfg.GetBetaGammaBinWidth();

	string outputFileName = cfg.GetOutputDir() + "/dEdxMap_run" + ConvertToString(runNumber) +  ".root";

	TFile *outputFile = new TFile(outputFileName.c_str(), "RECREATE");
	outputFile->cd();	
	TTree *tree = new TTree("dEdxMap", "dEdxMap");
	double bg = 0;
	double dEdx = 0;
	tree->Branch("bg", &bg, "bg/D");
	tree->Branch("dEdx", &dEdx, "dEdx/D");
	
	for(int i = 0; i < nbins; ++i){
		bg = low + (i+0.5)*width;
		
		dEdx = tpc.GetBetheBlochValue(bg);
		
		tree->Fill();
	}
	


	
	tree->Write();
	outputFile->Close();


}



