#include <stdlib.h>

#include "dEdxMapConfig.h"
#include <fstream>
#include <fwk/CentralConfig.h>
#include <utl/Branch.h>
#include <utl/BranchIterator.h>
#include "CConfig.h"

using namespace std;
using namespace fwk;
using namespace utl; 

void dEdxMapConfig::Read(){


	/*if(cConfig == NULL){
		cout << __FUNCTION__ << ": Central config not initialized! Exiting..." << endl;
		exit (EXIT_FAILURE);
	}*/
	Branch dEdxMapConf = cConfig.GetTopBranch("dEdxMap");

	if(dEdxMapConf.GetName() != "dEdxMap"){
		cout << __FUNCTION__ << ": Wrong dEdxMap config file! Top branch name doesn't match! Please check your configuration files! Exiting..." << endl;
		exit (EXIT_FAILURE);
	}

	fNBetaGamma = 100;
	fBetaGamaMin = 0;
	fBetaGamaMax = 1000;
	for(BranchIterator it = dEdxMapConf.ChildrenBegin(); it != dEdxMapConf.ChildrenEnd(); ++it){
		Branch child = *it;
		if(child.GetName() == "outputDir"){
			child.GetData(fOutputDir);
		}

		if(child.GetName() == "minBetaGama"){
			child.GetData(fBetaGamaMin);
		}

		if(child.GetName() == "maxBetaGama"){
			child.GetData(fBetaGamaMax);
		}
		if(child.GetName() == "nBetaGamma"){
			child.GetData(fNBetaGamma);
		}
	}
	
}



//*********************************************************************************************
string dEdxMapConfig::GetTimeString(){
	time_t rawtime;
	struct tm* timeinfo;

	time(&rawtime);
	timeinfo = localtime(&rawtime);

	string res;
	stringstream s;

	s << timeinfo->tm_mon + 1;
	s << "_";
	s << timeinfo->tm_mday;
	s << "_";
	s << timeinfo->tm_year + 1900;
	s << "_";
	s << timeinfo->tm_hour;
	s << "_";
	s << timeinfo->tm_min;
	s << "_";
	s << timeinfo->tm_sec;

	s >> res;

	return res;
}


