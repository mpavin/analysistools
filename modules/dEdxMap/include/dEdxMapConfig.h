#ifndef DEDXMAPCONFIG_H
#define DEDXMAPCONFIG_H
#include <iostream>
#include <vector>
#include <string>




using namespace std;

class dEdxMapConfig{

	public:
		dEdxMapConfig(){Read();}  //Constructor

		//*************************Get functions***********************************

		string GetOutputDir(){return fOutputDir;}
		double GetMinBetaGamma(){return fBetaGamaMin;}
		double GetMaxBetaGamma(){return fBetaGamaMax;}
		int GetNBinsBetaGamma(){return fNBetaGamma;}
		double GetBetaGammaBinWidth(){return (fBetaGamaMax - fBetaGamaMin)/fNBetaGamma;}


		//*************************Set functions***********************************
		void Read(); //Reads main config file.

		void SetOutputDir(string outputDir){fOutputDir = outputDir;}

		void SetMinBetaGamma(double val){fBetaGamaMin = val;}
		void SetMaxBetaGamma(double val){fBetaGamaMax = val;}
		void SetNBinsBetaGamma(int n){fNBetaGamma = n;}

		string GetTimeString();
		
	private:

		string fOutputDir;

		double fBetaGamaMin;
		double fBetaGamaMax;
		int fNBetaGamma;
};

#endif
