#include <stdlib.h>
#include "TOFResCalc.h"
#include "TreeDataTypes.h"
#include "TOFMapConfig.h"
#include "dEdxResolution.h"
#include "Functions.h"
#include <sstream>
#include <TChain.h>
#include <TFile.h>
#include <TF1.h>
#include <TH1D.h>
#include <TCanvas.h>
#include <TTree.h>
#include <TVectorD.h>
#include <TBranch.h>
#include <utl/Vector.h>
#include "StrTools.h"

using namespace utl; 
using namespace StrTools;
void TOFResCalc(string inputFile, bool isList){

	TOFMapConfig cfg;
	
	vector<double> ranges;
	for(int i = 0; i< 8; i++){
		ranges.push_back(0.3);
	}
	dEdxResolution dEdxRes(cfg.GetdEdxMapFile(),ranges);
	
	TChain chain("Data");
	
	if(isList){
		ifstream ifs;
		string line;
		ifs.open(inputFile.c_str());
		
		if (!ifs.is_open()){
			cerr << "[ReadInputListFile] Could not open infile. Exiting..." << endl;			
			exit (101);
		}
    
		while (getline(ifs, line)){
			if(line.at(0)=='#')
				continue;
			chain.Add(line.c_str());
		}
		ifs.close();		
	}
	else {
		chain.Add(inputFile.c_str());
	}
	//TFile input(fInputFile.c_str(), "READ");
	//TTree *tree = (TTree*) input.Get("Data");


	BeamA beamD;
	vector<double> *x = NULL;
	vector<double> *xE = NULL;
	vector<double> *y = NULL;
	vector<double> *yE = NULL;
	vector<double> *z = NULL;
	vector<double> *px = NULL;
	vector<double> *py = NULL;
	vector<double> *pz = NULL;
	vector<double> *distTarg = NULL;
	vector<double> *zStart = NULL;
	vector<double> *dEdx = NULL;
	vector<int> *qD = NULL;
	vector<int> *nVTPC1 = NULL;
	vector<int> *nVTPC2 = NULL;
	vector<int> *nGTPC = NULL;
	vector<int> *nMTPC = NULL;
	vector<int> *topology = NULL;
	vector<int> *nTOF = NULL;
	vector<double> *m2TOF = NULL;	
	vector<int> *tofScId = NULL;
	int runNumber;
	
	TBranch *b_aData = chain.GetBranch("Beam");
 	b_aData->SetAddress(&beamD);
	chain.SetBranchAddress("x", &x);
	chain.SetBranchAddress("xE", &xE);
	chain.SetBranchAddress("y", &y);
	chain.SetBranchAddress("yE", &yE);
	chain.SetBranchAddress("z", &z);
	chain.SetBranchAddress("px", &px);
	chain.SetBranchAddress("py", &py);
	chain.SetBranchAddress("pz", &pz);
	chain.SetBranchAddress("distTarg", &distTarg);
	chain.SetBranchAddress("zStart", &zStart);
	chain.SetBranchAddress("dEdx", &dEdx);
	chain.SetBranchAddress("q", &qD);
	chain.SetBranchAddress("nVTPC1", &nVTPC1);
	chain.SetBranchAddress("nVTPC2", &nVTPC2);
	chain.SetBranchAddress("nGTPC", &nGTPC);
	chain.SetBranchAddress("nMTPC", &nMTPC);
	chain.SetBranchAddress("topology", &topology);
	chain.SetBranchAddress("nTOF", &nTOF);
	chain.SetBranchAddress("m2TOF", &m2TOF);
	chain.SetBranchAddress("tofScId", &tofScId);
	chain.SetBranchAddress("runNumber", &runNumber);

	
	

	int nEntries = chain.GetEntries();
	//cout << cfg.GetElectronMass() << endl;

	
	
	double minp = 0.6;
	double maxp = 4.6;
	double stepp = 0.1;
	double deltam0 = 0.05;
	double deltam1 = 0.5;
	
	//int np = (int) (maxp-minp)/stepp;
	int np = 40;
	vector<TH1D> tof_pion;
	vector<TH1D> tof_proton;
	vector<TH1D> tof_kaon;
	vector<TH1D> tof_electron;
	vector<double> pvals;
	
	TH2D tof_p("tof_p", "", 200, -10, 10, 200, -0.9, 1.8);
	TH1D err("err0", "", 30, 0, 30);
	for(int i = 0; i < np; i++){
		string namepi = "tof_pi" + ConvertToString(i+1);
		string nameproton = "tof_proton" + ConvertToString(i+1);
		string namekaon = "tof_kaon" + ConvertToString(i+1);
		string nameelectron = "tof_electron" + ConvertToString(i+1);
		
		double ptemp = minp + (i+0.5)*stepp;
		pvals.push_back(ptemp);
		TH1D pi(namepi.c_str(), "", 100, cfg.GetPionMass()*cfg.GetPionMass() - deltam0 - (deltam1-deltam0)*ptemp/(maxp-minp), 
		cfg.GetPionMass()*cfg.GetPionMass() + deltam0 + (deltam1-deltam0)*ptemp/(maxp-minp));
		TH1D pr(nameproton.c_str(), "", 100, cfg.GetProtonMass()*cfg.GetProtonMass() - deltam0 - (deltam1-deltam0)*ptemp/(maxp-minp), 
		cfg.GetProtonMass()*cfg.GetProtonMass() + deltam0 + (deltam1-deltam0)*ptemp/(maxp-minp));
		
		TH1D Ka(namekaon.c_str(), "", 100, cfg.GetKaonMass()*cfg.GetKaonMass() - deltam0 - (deltam1-deltam0)*ptemp/(maxp-minp), 
		cfg.GetKaonMass()*cfg.GetKaonMass() + deltam0 + (deltam1-deltam0)*ptemp/(maxp-minp));
		TH1D el(nameelectron.c_str(), "", 100, cfg.GetElectronMass()*cfg.GetElectronMass() - (deltam1-deltam0-0.5)*ptemp/(maxp-minp), 
		cfg.GetElectronMass()*cfg.GetElectronMass() + (deltam1-deltam0-0.5)*ptemp/(maxp-minp));
		
		tof_pion.push_back(pi);
		tof_proton.push_back(pr);
		tof_kaon.push_back(Ka);
		tof_electron.push_back(el);
	}
	
	TH2D m2TOF_pLeft("m2TOF_pLeft", "", 200, -10, 10, 200, -0.9, 1.8);
	TH2D m2TOF_pRight("m2TOF_pRight", "", 200, -10, 10, 200, -0.9, 1.8);
	TH2D m2TOF_pLeftRST("m2TOF_pLeftRST", "", 200, -10, 10, 200, -0.9, 1.8);
	TH2D m2TOF_pRightRST("m2TOF_pRightRST", "", 200, -10, 10, 200, -0.9, 1.8);
	TH2D m2TOF_pLeftWST("m2TOF_pLeftWST", "", 200, -10, 10, 200, -0.9, 1.8);
	TH2D m2TOF_pRightWST("m2TOF_pRightWST", "", 200, -10, 10, 200, -0.9, 1.8);
	vector<TH2D> m2TOF_pscint;
	for(int i = 0; i < 80; i++){
		string nmScint = "m2TOF_pSc" + ConvertToString(i+1);
		TH2D temp(nmScint.c_str(), "", 200, -10, 10, 200, -0.9, 1.8);
		m2TOF_pscint.push_back(temp);
		
	}
	for(int i = 0; i < nEntries; ++i){
		chain.GetEntry(i);

		if(!((i+1)%100000))
			cout << "Procesing entry # " << i+1 << " out of " << nEntries << "." << endl;
			
		for(int j = 0; j < x->size(); j++){	
			if(topology->at(j) == 10) continue;
			//if(runNumber > 10) continue;
			double p = sqrt(px->at(j)*px->at(j) + py->at(j)*py->at(j) + pz->at(j)*pz->at(j));
			double sigmaR = sqrt(pow(xE->at(j)*x->at(j),2)+pow(y->at(j)*yE->at(j),2))/sqrt(x->at(j)*x->at(j)+y->at(j)*y->at(j));
			if(nGTPC->at(j) == 0) err.Fill(distTarg->at(j)/sigmaR);
			if(qD->at(j) > 0){
				tof_p.Fill(p, m2TOF->at(j));
				m2TOF_pscint.at(tofScId->at(j)-1).Fill(p, m2TOF->at(j));
				
				if(tofScId->at(j) < 40){
					m2TOF_pLeft.Fill(p, m2TOF->at(j));
					if(px->at(j)*qD->at(j)>0){
						m2TOF_pLeftRST.Fill(p, m2TOF->at(j));
					}
					else if(px->at(j)*qD->at(j)<0){
						m2TOF_pLeftWST.Fill(p, m2TOF->at(j));
					}
				}
				else if(tofScId->at(j) > 40){
					m2TOF_pRight.Fill(p, m2TOF->at(j));
					if(px->at(j)*qD->at(j)>0){
						m2TOF_pRightRST.Fill(p, m2TOF->at(j));
					}
					else if(px->at(j)*qD->at(j)<0){
						m2TOF_pRightWST.Fill(p, m2TOF->at(j));
					}
				}
			}
			else if(qD->at(j) < 0){
				tof_p.Fill(-p, m2TOF->at(j));
				m2TOF_pscint.at(tofScId->at(j)-1).Fill(-p, m2TOF->at(j));
				
				if(tofScId->at(j) < 40){
					m2TOF_pLeft.Fill(-p, m2TOF->at(j));
					if(px->at(j)*qD->at(j)>0){
						m2TOF_pLeftRST.Fill(-p, m2TOF->at(j));
					}
					else if(px->at(j)*qD->at(j)<0){
						m2TOF_pLeftWST.Fill(-p, m2TOF->at(j));
					}
				}
				else if(tofScId->at(j) > 40){
					m2TOF_pRight.Fill(-p, m2TOF->at(j));
					if(px->at(j)*qD->at(j)>0){
						m2TOF_pRightRST.Fill(-p, m2TOF->at(j));
					}
					else if(px->at(j)*qD->at(j)<0){
						m2TOF_pRightWST.Fill(-p, m2TOF->at(j));
					}
				}
			}
			short int selectedpi = 0;
			short int selectedp = 0;
			short int selectedK = 0;
			short int selectede = 0;
			double dEdxV;
			
			int indexp = -1;
			for(int k = 0; k < np; k++){
				if(p > (minp + k*stepp) && p <= (minp + (k+1)*stepp)){
					indexp = k;
					break;
				}
			}
			if (indexp < 0) continue;
			
			if(cfg.IsProton(p, m2TOF->at(j))){
				bool selectedTOF = true;
				dEdxV = dEdxRes.GetMean(p/cfg.GetProtonMass());
				if(fabs(dEdxV - dEdx->at(j)) < cfg.GetdEdxCutValue()*cfg.GetdEdxWidth()){
					//tof_proton.Fill(p, spectrum.m2TOF);
					selectedp = 1;
				}
			}
			if(cfg.IsPion(p, m2TOF->at(j))){
				dEdxV = dEdxRes.GetMean(p/cfg.GetPionMass());
				if(fabs(dEdxV - dEdx->at(j)) < cfg.GetdEdxCutValue()*cfg.GetdEdxWidth()){
					//tof_pion.Fill(p, spectrum.m2TOF);
					selectedpi = 1;
				}
			}


			if(cfg.IsElectron(p, m2TOF->at(j))){

				dEdxV = dEdxRes.GetMean(p/cfg.GetElectronMass());
				if(fabs(dEdxV - dEdx->at(j)) < cfg.GetdEdxCutValue()*cfg.GetdEdxWidth()){
					//tof_electron.Fill(p, spectrum.m2TOF);
					selectede = 1;
				}
			}
			if(cfg.IsKaon(p, m2TOF->at(j))){
				dEdxV = dEdxRes.GetMean(p/cfg.GetKaonMass());
				if(fabs(dEdxV - dEdx->at(j)) < cfg.GetdEdxCutValue()*cfg.GetdEdxWidth()){
					//tof_kaon.Fill(p, spectrum.m2TOF);
					selectedK = 1;
				}
				//if(!selectedpi && !selectedp && !selectede) selectedK=1;
			}		
			short int selected = selectedpi + selectedp + selectedK + selectede;
			if(selected == 1){
				if(selectedpi == 1){
					tof_pion.at(indexp).Fill(m2TOF->at(j));
				}
				else if(selectedp == 1){
					tof_proton.at(indexp).Fill(m2TOF->at(j));
				}
				else if(selectedK == 1){
					tof_kaon.at(indexp).Fill(m2TOF->at(j));
				}
				else if(selectede == 1){
					tof_electron.at(indexp).Fill(m2TOF->at(j));
				}
		
			}
		}
	
	}
		
		
	
	
	/*for(int i = 0; i<np; i++){
		tof_pion.at(i).Write();
		tof_proton.at(i).Write();
		tof_kaon.at(i).Write();
		tof_electron.at(i).Write();		
	}*/

	
	/*TGraphErrors *pMean;
	TGraphErrors *pWidth;
	TGraphErrors *piMean;
	TGraphErrors *piWidth;
	TGraphErrors *KMean;
	TGraphErrors *KWidth;
	TGraphErrors *eMean;
	TGraphErrors *eWidth;*/
	
	//vector<double> pars;
	
	int n = 6;
	double pars[6];
	string particle; 
	string name = cfg.GetOutputDir() + "/TOFMap.root";
	TFile *output = new TFile(name.c_str(), "RECREATE");
	output->cd();
	TTree *tofPars = new TTree("tofPars", "tofPars");
	tofPars->Branch("particle", &particle);
	tofPars->Branch("n", &n, "n/I");
	tofPars->Branch("pars", pars, "pars[n]/D");
	
	SliceFit(tof_pion, pvals, output, "pion", pars);
	particle = "pion";
	tofPars->Fill();
	
	SliceFit(tof_proton, pvals, output, "proton", pars);
	particle = "proton";
	tofPars->Fill();
	
	SliceFit(tof_kaon, pvals, output, "kaon", pars);
	particle = "kaon";
	tofPars->Fill();
	
	SliceFit(tof_electron, pvals, output, "electron", pars);
	particle = "electron";
	tofPars->Fill();	
	/*cout << "radi1" << endl;
	output.cd();
	if(piMean !=NULL) piMean->Write();
	cout << "radi2" << endl;
	if(piWidth !=NULL) piWidth->Write();
	cout << "radi3" << endl;
	if(pMean !=NULL) pMean->Write();
	cout << "radi4" << endl;
	if(pWidth !=NULL) pWidth->Write();
	cout << "radi5" << endl;
	if(KMean !=NULL) KMean->Write();
	cout << "radi6" << endl;
	if(KWidth !=NULL) KWidth->Write();
	cout << "radi7" << endl;
	if(eMean !=NULL) eMean->Write();
	cout << "radi8" << endl;
	if(eWidth !=NULL) eWidth->Write();*/

	tofPars->Write();
	tof_p.Write();
	m2TOF_pLeft.Write();
	m2TOF_pRight.Write();
	m2TOF_pRightRST.Write();
	m2TOF_pRightWST.Write();
	m2TOF_pLeftRST.Write();
	m2TOF_pLeftWST.Write();
	err.Write();
	for(int i = 0; i < 80; i++){
		m2TOF_pscint.at(i).Write();
		
	}
	output->Close();

}

void SliceFit(vector<TH1D> &histos, vector<double> &pvals, TFile *output, string add, double *pars){


	vector<double> mom;
	vector<double> momE;
	vector<double> means;
	vector<double> meansE;
	vector<double> widths;
	vector<double> widthsE;	
	//
	for(int i = 0; i < histos.size(); i++){
	
		if(histos.at(i).GetEntries()<2) continue;
		//double p = histo.GetXaxis()->GetBinCenter(i);
		string name = "gausFit" + ConvertToString(i+1);
		TF1 gausFit(name.c_str(), "[0]*TMath::Gaus(x, [1], [2], 1)", histos.at(i).GetMean()-2.5*histos.at(i).GetRMS(), histos.at(i).GetMean()+2.5*histos.at(i).GetRMS());
		gausFit.SetParName(0, "N");
		gausFit.SetParameter(0, histos.at(i).GetEntries());
		gausFit.SetParLimits(0, 0, histos.at(i).GetEntries()+0.2*histos.at(i).GetEntries());
	
		gausFit.SetParName(1, "#mu");
		gausFit.SetParameter(1, histos.at(i).GetMean());
		gausFit.SetParLimits(1, histos.at(i).GetMean()-0.1, histos.at(i).GetMean()+0.1);	
	
		gausFit.SetParName(2, "#sigma");
		gausFit.SetParameter(2, histos.at(i).GetRMS());
		gausFit.SetParLimits(2, histos.at(i).GetRMS()-0.5*histos.at(i).GetRMS(), histos.at(i).GetRMS()+0.5*histos.at(i).GetRMS());	
		
		histos.at(i).Fit(&gausFit, "R");
		output->cd(); 
		histos.at(i).Write();
		/*TF1* resFit = new TF1("resFit", "([0]/[2]*sqrt(2*TMath::Pi()))*TMath::Gaus(0.5*(x-[1])*(x-[1])/[2]/[2])", mean-2*width, mean+2*width);
		resFit->SetParameter(0, entries);
		resFit->SetParLimits(0, 0.50*entries, 1.50*entries);		
		
		resFit->SetParameter(1, mean);
		resFit->SetParLimits(1, 0.90*mean, 1.1*mean);	
		
		resFit->SetParameter(2, width);
		resFit->SetParLimits(2, 0.80*width, 1.2*width);				
		
		slice->Fit(resFit, "QR", "Csame");	*/
		
		mom.push_back(pvals.at(i));
		/*momE.push_back(histo.GetXaxis()->GetBinWidth(i));
		means.push_back(resFit->GetParameter(1));
		meansE.push_back(resFit->GetParError(1));
		widths.push_back(resFit->GetParameter(2));
		widthsE.push_back(resFit->GetParError(2));*/
		means.push_back(gausFit.GetParameter(1));
		meansE.push_back(gausFit.GetParError(1));
		widths.push_back(gausFit.GetParameter(2));
		widthsE.push_back(gausFit.GetParError(2));

		cout << i << endl;
		cout << "+++++++++++++++++++++++++++++++++" << endl;
	}
	
	if (mom.size()<3) return;
	TGraphErrors *meanGraph = new TGraphErrors(mom.size(), mom.data(), means.data(), momE.data(), meansE.data());
	string meanName = "mean" + add;
	meanGraph->SetName(meanName.c_str());
	TF1* meanLine = new TF1("meanLine", "[0]+[1]*x+[2]*x*x", mom.at(0), mom.at(mom.size()-1));
	meanLine->FixParameter(1,0);
	meanGraph->Fit("meanLine", "R", "P");

	
	//meanLine->FixParameter(1,0);

	TGraphErrors *widthGraph = new TGraphErrors(mom.size(), mom.data(), widths.data(), momE.data(), widthsE.data());
	string widthName = "width" + add;
	widthGraph->SetName(widthName.c_str());
	TF1* widthParabola = new TF1("widthParabola", "[0]+[1]*x+[2]*x*x", mom.at(0), mom.at(mom.size()-1));	
	widthGraph->Fit("widthParabola", "R", "P");

	//output.cd();
	//cout << "radi" << endl;
	output->cd();
	meanGraph->Write();
	widthGraph->Write();
	
	//double el[4] = {meanLine->GetParameter(0), widthParabola->GetParameter(0), widthParabola->GetParameter(1), widthParabola->GetParameter(2)};
	pars[0] = meanLine->GetParameter(0);
	pars[1] = meanLine->GetParameter(1);
	pars[2] = meanLine->GetParameter(2);
	
	//pars[1] = 0;
	pars[3] = widthParabola->GetParameter(0);
	pars[4] = widthParabola->GetParameter(1);
	pars[5] = widthParabola->GetParameter(2);
	//TVectorD parameters(4, el);
	//parameters.SetName(add.c_str());
	//parameters.SetName(add.c_str());
	//parameters.Write();
	/*pars.push_back(meanLine->GetParameter(0));
	pars.push_back(meanLine->GetParameter(1));

	pars.push_back(widthParabola->GetParameter(0));
	pars.push_back(widthParabola->GetParameter(1));
	pars.push_back(widthParabola->GetParameter(2));*/
}
