#ifndef TOFMAPCONFIG_H
#define TOFMAPCONFIG_H
#include <iostream>
#include <vector>
#include <string>

#include <vector>



using namespace std;

class TOFMapConfig{

	public:
		TOFMapConfig(){Read();}

		string GetOutputDir(){return fOutput;}
		double GetPionMass(){return fPionMass;}
		double GetKaonMass(){return fKaonMass;}
		double GetProtonMass(){return fProtonMass;}
		double GetElectronMass(){return fElectronMass;}
		string GetdEdxMapFile(){return fdEdxMapFile;}
		double GetdEdxWidth(){return fdEdxWidth;}
		double GetdEdxCutValue(){return fdEdxCut;}
		
		
		bool IsPion(double p, double m2ToF);
		bool IsKaon(double p, double m2ToF);
		bool IsProton(double p, double m2ToF);
		bool IsElectron(double p, double m2ToF);
		
		void SetOutputDir(string value){fOutput = value;}
		void SetPionMass(double mass){fPionMass = mass;}
		void SetKaonMass(double mass){fKaonMass = mass;}
		void SetProtonMass(double mass){fKaonMass = mass;}
		void SetElectronMass(double mass){fKaonMass = mass;}
		void SetdEdxMapFile(string file){fdEdxMapFile = file;}
		void SetdEdxWidth(double width){fdEdxWidth = width;}
		void SetdEdxCutValue(double val){fdEdxCut = val;}
		void AddPionTOFPid(vector<double> &pid);
		void AddKaonTOFPid(vector<double> &pid);
		void AddProtonTOFPid(vector<double> &pid);
		void AddElectronTOFPid(vector<double> &pid);
		
		
	private:
		bool IsParticle(vector<vector<double> >& pid, double p,  double m2ToF);
		void AddPid(vector<vector<double> >& pids, vector<double> &pid);
		void Read();

		string fOutput;
		string fdEdxMapFile;
		double fdEdxWidth;
		double fdEdxCut;
		double fPionMass;
		double fProtonMass;
		double fKaonMass;
		double fElectronMass;
		vector<vector<double> > fPionPid;
		vector<vector<double> > fProtonPid;
		vector<vector<double> > fKaonPid;
		vector<vector<double> > fElectronPid;

};



#endif
