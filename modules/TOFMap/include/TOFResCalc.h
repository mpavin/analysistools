#ifndef TOFRESCALC_H
#define TOFRESCALC_H


#include <iostream>
#include <vector>
#include <string>
#include <TH2D.h>
#include <TGraphErrors.h>
#include <TFile.h>

using namespace std;

void TOFResCalc(string inputFile, bool isList);
void SliceFit(vector<TH1D> &histo, vector<double> &pvals, TFile *output, string add, double *pars);
#endif
