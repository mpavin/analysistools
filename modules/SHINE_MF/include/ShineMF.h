#include <det/Detector.h>
#include <utl/Point.h>
#include <utl/Vector.h>



extern fwk::CentralConfig& cConfig;
extern det::Detector &detector;
extern const det::MagneticField &magField;

void TrackExtrapolation::GetMagField(double x, double y, double z, double &Bx, double &By, double &Bz){
	
	double zmin = -592;
	double norm = 100000;
	if(point.GetZ() > zmin){
		utl::Point pField(x,y,z);
		const Vector &magVec = fMagField.GetField(pField);
		Bx = norm*magVec.GetX();
		By = norm*magVec.GetY();
		Bz = norm*magVec.GetZ();
	}
	else{
		
	 	double supr = 0.03;
	 	double dist = zmin - point.GetZ();
	 	
		utl::Point pField(point.GetX(), point.GetY(), zmin);
		const Vector &magVec = fMagField.GetField(pField);
		
		Bx = norm*magVec.GetX()*exp(-dist*supr);
		By = norm*magVec.GetY()*exp(-dist*supr);
		Bz = norm*magVec.GetZ()*exp(-dist*supr);
		
	}
}
