#include <iostream>
#include <stdlib>

#include <boost/format.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/program_options.hpp>

#include "ShineMF.h"

using namespace std;
using namespace boost;

namespace po = boost::program_options;

int main(int argc, char* argv[])
{

	
  string cfgFile = " ";	//Config file

//**************************************OPTIONS*************************************  
  po::options_description desc("Usage");

  desc.add_options()
    ("help,h",
     "Output this help.")
    ("bootstrap,b",
     po::value<string>(&cfgFile), "Bootstrap file.")  
    ;
  
  po::variables_map vm;
  try {
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);
  } catch (po::error& err) {
    cerr << "Command line error : " << err.what() << '\n'
	 << desc << endl;
    exit(EXIT_FAILURE);
  }
//*********************************************************************************

	//these objects are defined in ShineMF.h
	cConfig.GetInstance(cfgFile, false, false);
	detector = Detector::GetInstance();
	magField = detector.GetMagneticField();
	
}
