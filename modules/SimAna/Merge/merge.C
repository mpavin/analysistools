#include <stdlib.h>
#include <TTree.h>
#include <TFile.h>
#include <TChain.h>
#include <string>
#include <vector>
#include <iostream> 
#include <fstream>   
#include <sstream>
#include <time.h>   
using namespace std;

void merge(string fileList, string add){
	vector<string> jobs;
	ReadInputList(fileList, jobs);
	
	TChain ch("SimData");
	
	for(int i = 0; i < jobs.size(); i++){
		cout << "Processing file: " << jobs.at(i) << endl;
		ch.Add(jobs.at(i).c_str());
	}
	
	string title = add + "spectrum" + GetTimeString() + ".root";
	
	cout << "Merging files..." << endl;
	ch.Merge(title.c_str());
}

void ReadInputList(string input, vector<string> &jobs){



	if (input != " " ){
		ifstream ifs;
		string line;
		ifs.open(input.c_str());
		
		if (!ifs.is_open()){
			cerr << "[ReadInputListFile] Could not open infile. Exiting..." << endl;			
			exit (101);
		}
    
		while (getline(ifs, line)){
			if(line.at(0)=='#')
				continue;
			jobs.push_back(line);
		}
		ifs.close();	
	
	}
	else cout << "WARNING: Empty file list! Jobs not created!" << endl;


}

string GetTimeString(){
	time_t rawtime;
	struct tm* timeinfo;

	time(&rawtime);
	timeinfo = localtime(&rawtime);

	std::string res;
	std::stringstream s;

	s << timeinfo->tm_mon + 1;
	s << "_";
	s << timeinfo->tm_mday;
	s << "_";
	s << timeinfo->tm_year + 1900;
	s << "_";
	s << timeinfo->tm_hour;
	s << "_";
	s << timeinfo->tm_min;
	s << "_";
	s << timeinfo->tm_sec;

	s >> res;

	return res;
}
