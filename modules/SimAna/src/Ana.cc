/*!
* \file
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <cstdlib>
#include <stdexcept>
#include <TTree.h>
#include <TFile.h>
#include <cstdlib>
#include <TMath.h>
#include <evt/Event.h>
#include <evt/EventHeader.h>
#include <io/EventFileChain.h>
#include <det/TriggerConst.h>
#include <det/BPDConst.h>
#include <evt/RawEvent.h>
#include <evt/SimEvent.h>
#include <evt/sim/Hit.h>
#include "StrTools.h"
#include <boost/format.hpp>
#include "Ana.h"
#include "PhaseSpace.h"
#include "TrackExtrapolation.h"
#include "AnaTrack.h"

using namespace std;
using namespace io;
using namespace evt;
using namespace evt::sim;
using namespace evt::rec;
using namespace det;
using namespace boost;
using namespace StrTools;
/*!
 *	\fn 
 *	Function Run reads SHOE files. For this class EventFileChain is used. It needs vector containing paths to the SHOE files.
 *	This vector is obtained from the DataAnaConfig class.
*/
void Ana::Run(){

	//string spectrumFileName = fDataAnaConfig.GetOutputDir() + "/spectrum_" + GetTimeString() + ".root";
	//string cutFile = fDataAnaConfig.GetOutputDir() + "/cuts_" + GetTimeString() + ".root";
	string spectrumFileName = fSimAnaConfig.GetOutputDir() + "/sim_spectrum.root";
	//cout << "radi1" << endl;
	PhaseSpace phaseSpace;
	//cout << "radi2" << endl;
	EventFileChain eventFileChain(fSimAnaConfig.GetJob());
	Event event;

	EventSelection evSel;
	unsigned int nEvents = 0;
	
	BeamA beamD;
	vector<double> z;
	vector<double> p;
	vector<double> px;
	vector<double> theta;
	vector<double> phi;
	vector<int> q;
	vector<int> pdgId;
	vector<int> indexZ;
	vector<int> indexZRec;
	vector<int> indexThRec;
	vector<int> indexTh;
	vector<int> indexP;
	vector<int> process;
	vector<int> hasTOF;
	vector<int> nGTPC;
	vector<int> nVTPC1;
	vector<int> nVTPC2;
	vector<int> nMTPC;
	int runNumber;

	TFile *spectrumFile = new TFile(spectrumFileName.c_str(), "RECREATE");
	spectrumFile->cd();
	
	fSpectrum = new TTree("SimData", "SimData");
	fSpectrum->SetAutoSave(-300000000);

	fSpectrum->Branch("Beam", &(beamD.aX), "aX/D:aY:bX:bY");
	fSpectrum->Branch("z", "vector<double>", &z);
	fSpectrum->Branch("theta", "vector<double>", &theta);
	fSpectrum->Branch("phi", "vector<double>", &phi);
	fSpectrum->Branch("p", "vector<double>", &p);
	fSpectrum->Branch("px", "vector<double>", &px);
	fSpectrum->Branch("q", "vector<int>", &q);
	fSpectrum->Branch("pdgId", "vector<int>", &pdgId);
	fSpectrum->Branch("indexZ" , "vector<int>", &indexZ);
	fSpectrum->Branch("indexZRec" , "vector<int>", &indexZRec);
	fSpectrum->Branch("indexThRec" , "vector<int>", &indexThRec);
	fSpectrum->Branch("indexTh" , "vector<int>", &indexTh);
	fSpectrum->Branch("indexP" , "vector<int>", &indexP);
	fSpectrum->Branch("process", "vector<int>", &process);
	fSpectrum->Branch("hasTOF", "vector<int>", &hasTOF);
	fSpectrum->Branch("nVTPC1", "vector<int>", &nVTPC1);
	fSpectrum->Branch("nVTPC2", "vector<int>", &nVTPC2);
	fSpectrum->Branch("nGTPC", "vector<int>", &nGTPC);
	fSpectrum->Branch("nMTPC", "vector<int>", &nMTPC);
	bool mc = false;
	if(eventFileChain.Read(event) == eSuccess){
		const SimEvent& simEvent = event.GetSimEvent();
		/*if(!simEvent.HasMainVertex()){
			cerr << __FUNCTION__ << ": Provided files are not MC! Exiting..." << endl;
			exit(EXIT_FAILURE);
		}*/
	}
	else{
		cerr << __FUNCTION__ << ": Error in input files! Exiting..." << endl;
		exit(EXIT_FAILURE);
	}
	
	
	double ntracks = 0;
	double ntracksE = 0;
	
	
	TrackExtrapolation trackExtrapolation(true, 0.1, 30400.);
	trackExtrapolation.SetTarget(fSimAnaConfig.GetTarget());
	
	while (eventFileChain.Read(event) == eSuccess && nEvents < Ana::fMaxEv) {


		z.clear();
		px.clear();
		p.clear();
		theta.clear();
		phi.clear();
		q.clear();
		pdgId.clear();
		indexZ.clear();
		indexZRec.clear();
		indexThRec.clear();
		indexTh.clear();
		indexP.clear();
		process.clear();
		hasTOF.clear();
		nVTPC1.clear();
		nVTPC2.clear();
		nGTPC.clear();
		nMTPC.clear();
		bool skipRun = false;
		EventHeader &evHeader = event.GetEventHeader();

		if(evHeader.GetRunNumber() != runNumber){
			runNumber = evHeader.GetRunNumber();
			skipRun = fSimAnaConfig.SkipRun(runNumber);
		}

		if(skipRun) continue;

		if(evSel.IsEventGood(event) == false)
			continue;	
   		
   		++nEvents;
    
    	if (!(nEvents%1000))
      		cout << " --->Processing event # " << nEvents << endl;


		const SimEvent& simEvent = event.GetSimEvent();
		if(!simEvent.HasMainVertex()){
			cerr << __FUNCTION__ << ": Provided files are not MC! Exiting..." << endl;
			continue;
		}
		const evt::sim::Beam& beam = simEvent.GetBeam();
		
		beamD.aX = beam.GetMomentum().GetX()/beam.GetMomentum().GetZ();
		beamD.aY = beam.GetMomentum().GetY()/beam.GetMomentum().GetZ();
		beamD.bX = beam.GetOrigin().GetX() - beamD.aX*beam.GetOrigin().GetZ();
		beamD.bY = beam.GetOrigin().GetY() - beamD.aY*beam.GetOrigin().GetZ();		

		
		for (std::list< evt::sim::VertexTrack>::const_iterator iter = simEvent.Begin<evt::sim::VertexTrack>();
           iter != simEvent.End< evt::sim::VertexTrack>(); ++iter)
      	{

			//ntracks++;
			int nToF = 0;
			const evt::sim::VertexTrack& vtxTrack = *iter;

			if(!vtxTrack.HasStartVertex()) continue;
			
			if(vtxTrack.GetCharge() == 0) continue;
			
			const sim::Vertex &vtx = simEvent.Get(vtxTrack.GetStartVertexIndex());	
			
			//if((vtx.GetPosition().GetZ() - fSimAnaConfig.GetTarget().GetUpstreamPosition().GetZ()) < (fSimAnaConfig.GetTarget().GetLength() +1)) cout << "BEFORE " << vtx.GetType() << " " << vtx.GetPosition().GetZ() << endl;
			//if(fSimAnaConfig.GetTarget().GetDistanceFromTargetSurface(vtx.GetPosition().GetX(), vtx.GetPosition().GetY(), vtx.GetPosition().GetZ()) > 0.3)
				//continue;
				
			if((vtx.GetPosition().GetZ() - fSimAnaConfig.GetTarget().GetUpstreamPosition().GetZ()) < -1)
				continue;
			
			if((vtx.GetPosition().GetZ() - fSimAnaConfig.GetTarget().GetUpstreamPosition().GetZ()) > (fSimAnaConfig.GetTarget().GetLength() +1))
				continue;			
			
			
			double thetaVal = 1000*acos(vtxTrack.GetMomentum().GetZ()/vtxTrack.GetMomentum().GetMag());
			double zVal = vtx.GetPosition().GetZ() - fSimAnaConfig.GetTarget().GetUpstreamPosition().GetZ();
			double pVal = vtxTrack.GetMomentum().GetMag();

			double phiVal = acos( vtxTrack.GetMomentum().GetX()/sqrt(vtxTrack.GetMomentum().GetX()*vtxTrack.GetMomentum().GetX()+vtxTrack.GetMomentum().GetY()*vtxTrack.GetMomentum().GetY()))*180/TMath::Pi();
			if(vtxTrack.GetMomentum().GetX()>0 && vtxTrack.GetMomentum().GetY() < 0){
				phiVal = -1*phiVal;
			}
			else if(vtxTrack.GetMomentum().GetX()<0 && vtxTrack.GetMomentum().GetY() < 0){
				phiVal = 360 - phiVal;
			}			
			//if(vtx.GetType() == 9) cout << "AFTER " << vtx.GetType() << " " << thetaVal << endl;	
			//if(thetaVal > 500) continue;
			if(vtxTrack.GetMomentum().GetMag() < 0.1) continue;
			
			int iZ = -99;
			int iTh = -99;
			int iP = -99;
			int nBinsz = phaseSpace.GetDimBins(1);	
			
			for(int k=0; k < nBinsz; k++){
				Bin& bin = phaseSpace.GetBin(k+1, 1, 1);
				if(bin.GetLowValue(1) > zVal || zVal >= bin.GetHighValue(1)) continue;
				iZ = k+1;	
				break;	
			}
			if(iZ > 0){
			
				int nBinsth = phaseSpace.GetDimBins(2, iZ);
			
				for(int k=0; k < nBinsth; k++){
					Bin& bin = phaseSpace.GetBin(iZ, k+1, 1);
					if(bin.GetLowValue(2) > thetaVal || thetaVal >= bin.GetHighValue(2)) continue;
					iTh = k+1;		
					break;			
				}	
				if(iTh > 0) {	
			
					int nBinsp = phaseSpace.GetDimBins(3, iZ, iTh);	
			
					for(int k=0; k < nBinsp; k++){
						Bin& bin = phaseSpace.GetBin(iZ, iTh, k+1);
						if(bin.GetLowValue(3) > pVal || pVal >= bin.GetHighValue(3)) continue;
						iP = k+1;
						break;		
					}	
				}
			}

			int htof = 0;
			int count = 0;
			for(HitIndexIterator it = vtxTrack.HitsBegin(); it != vtxTrack.HitsEnd(); it++){
				//cout << count << endl;
				count++;
				if(count > 11) break;
				const Hit &hit = simEvent.Get(*it);
				
				if(hit.GetDetectorId() == det::Const::eTOFF){
					htof = 1;
					
				}

			}
			//cout << htof << endl;
			int iZRec = -99;
			int iThRec = -99;
			evt::Index< rec::Track> trit = vtxTrack.GetRecTrackWithMaxCommonPoints();
			if(trit.IsValid()){
				const rec::Track& track = event.GetRecEvent().Get(trit);
				
				if(track.GetStatus() == 0){
					//ntracks+=1;
					AnaTrack anaTrack(trackExtrapolation); 
					anaTrack.SetTrack(track);
					anaTrack.DoExtrapolation();
				
					double zValRec = anaTrack.GetExtrapolatedPosition().GetZ() - fSimAnaConfig.GetTarget().GetUpstreamPosition().GetZ();
					
					for(int k=0; k < nBinsz; k++){
						Bin& bin = phaseSpace.GetBin(k+1, 1, 1);
						if(bin.GetLowValue(1) > zValRec || zValRec >= bin.GetHighValue(1)) continue;
						iZRec = k+1;	
						break;	
					}
					double thetaValRec = 1000*acos(anaTrack.GetMomentum().GetZ()/anaTrack.GetMomentum().GetMag());
					
					if(iZRec > 0){
						int nBinsth = phaseSpace.GetDimBins(2, iZRec);
			
						for(int k=0; k < nBinsth; k++){
							Bin& bin = phaseSpace.GetBin(iZRec, k+1, 1);
							if(bin.GetLowValue(2) > thetaValRec || thetaValRec >= bin.GetHighValue(2)) continue;
							iThRec = k+1;		
							break;			
						}
					}
				}				
			}

				
	

			//cout << iZ << " " << iZRec << endl;
			//cout << count << endl;
			//if(nbin == 0) continue;
			indexZ.push_back(iZ);
			indexZRec.push_back(iZRec);
			indexThRec.push_back(iThRec);
			indexTh.push_back(iTh);
			indexP.push_back(iP);
			
			//cout << "radi" << endl;
			process.push_back(vtx.GetType());
			z.push_back(vtx.GetPosition().GetZ());
			theta.push_back(thetaVal);
			phi.push_back(phiVal);
			p.push_back(vtxTrack.GetMomentum().GetMag());
			px.push_back(vtxTrack.GetMomentum().GetX());
			q.push_back(vtxTrack.GetCharge());
			pdgId.push_back(vtxTrack.GetParticleId());
			hasTOF.push_back(htof);
			nVTPC1.push_back(vtxTrack.GetNumberOfHits( rec::TrackConst::eVTPC1));
			nVTPC2.push_back(vtxTrack.GetNumberOfHits( rec::TrackConst::eVTPC2));
			nGTPC.push_back(vtxTrack.GetNumberOfHits( rec::TrackConst::eGTPC));
			nMTPC.push_back(vtxTrack.GetNumberOfHits( rec::TrackConst::eMTPC));
			
	
			
		}

		if(z.size()>0){
			spectrumFile->cd();
			fSpectrum->Fill();
		}
	}
	
	//cout << ntracksE/ntracks << endl;
	spectrumFile->cd();
	fSpectrum->Write();
	spectrumFile->Close();

}


