#include <stdlib.h>

#include "SimAnaConfig.h"
#include <fstream>
#include <fwk/CentralConfig.h>
#include <utl/Branch.h>
#include <utl/BranchIterator.h>
#include "CConfig.h"

using namespace std;
using namespace fwk;
using namespace utl; 

void SimAnaConfig::Read(){


	/*if(cConfig == NULL){
		cout << __FUNCTION__ << ": Central config not initialized! Exiting..." << endl;
		exit (EXIT_FAILURE);
	}*/
	Branch simAnaConf = cConfig.GetTopBranch("SimAna");

	if(simAnaConf.GetName() != "SimAna"){
		cout << __FUNCTION__ << ": Wrong SimAna config file! Top branch name doesn't match! Please check your configuration files! Exiting..." << endl;
		exit (EXIT_FAILURE);
	}

	for(BranchIterator it = simAnaConf.ChildrenBegin(); it != simAnaConf.ChildrenEnd(); ++it){
		Branch child = *it;
		if(child.GetName() == "data"){
			child.GetData(fInputData);
			if(fInputData.at(0)=='$'){
				string run = fInputData.substr(1, fInputData.size()-1);
				//cout << run << endl;
				fInputData = getenv(run.c_str());
			}
			

		}

		if(child.GetName() == "outputDir"){
			child.GetData(fOutputDir);
			if(fOutputDir.at(0)=='$'){
				string run = fOutputDir.substr(1, fOutputDir.size()-1);
				//cout << run << endl;
				fOutputDir = getenv(run.c_str());
			}
		}

		if(child.GetName() == "runNumbers"){
			child.GetData(fRunNumbers);

		}
	
		if(child.GetName() == "target"){
			vector<double> position;
			position.push_back(0);
			position.push_back(0);
			position.push_back(-657.40);
			
			double tiltX = 0;
			double tiltY = 0;

			ETargetType type = eLong;
			int iType = 0;
			string name = "long";
			double length = 90;
			double radius = 1.3;

			for(BranchIterator t = child.ChildrenBegin(); t != child.ChildrenEnd(); ++t){
				Branch targetChild = *t;

				if(targetChild.GetName() == "name"){
					targetChild.GetData(name);
				}
				else if(targetChild.GetName() == "type"){

					targetChild.GetData(iType);
				}
				else if(targetChild.GetName() == "position"){
					position.clear();
					targetChild.GetData(position);
				}
				else if(targetChild.GetName() == "radius"){
					targetChild.GetData(radius);
				}
				else if(targetChild.GetName() == "tiltX"){
					targetChild.GetData(tiltX);
				}
				else if(targetChild.GetName() == "tiltY"){
					targetChild.GetData(tiltY);
				}
			}

			if(iType == 1) type = eThin;
			if(iType == 2) type = eNominalBeamline;
			SetTarget(name, type, length, radius, position.at(0), position.at(1), position.at(2), tiltX, tiltY);
			
		}
	}
	
}

//*********************************************************************************************
void SimAnaConfig::SetTarget(string name, ETargetType type, double length, double radius, double positionX, double positionY, double positionZ, double tilt1, double tilt2){

	SimAnaConfig::fTarget.SetName(name);
	SimAnaConfig::fTarget.SetType(type);
	SimAnaConfig::fTarget.SetLength(length);
	SimAnaConfig::fTarget.SetRadius(radius);
	SimAnaConfig::fTarget.SetUpstreamPosition(positionX, positionY, positionZ);
	SimAnaConfig::fTarget.SetTiltXZ(tilt1);
	SimAnaConfig::fTarget.SetTiltYZ(tilt2);

}


//*********************************************************************************************
void SimAnaConfig::ReadInputList(string inputData){
	
	fInputData = inputData;
	ReadInputList();
}

//*********************************************************************************************
void SimAnaConfig::ReadInputList(){

	cout << fInputData << endl;

	if (SimAnaConfig::fInputData != " " ){
		ifstream ifs;
		string line;
		ifs.open(SimAnaConfig::fInputData.c_str());
		
		if (!ifs.is_open()){
			cerr << "[ReadInputListFile] Could not open infile. Exiting..." << endl;			
			exit (101);
		}
    
		while (getline(ifs, line)){
			if(line.at(0)=='#')
				continue;
			SimAnaConfig::fJob.push_back(line);
		}
		ifs.close();	
	
	}
	else cout << "WARNING: Empty file list! Jobs not created!" << endl;


}


//*********************************************************************************************
vector<string>& SimAnaConfig::GetJob(){

	if(SimAnaConfig::fJob.size() == 0)
		SimAnaConfig::ReadInputList();

	return SimAnaConfig::fJob;

}

//*********************************************************************************************

bool SimAnaConfig::SkipRun(int runNumber){

	div_t res = div(fRunNumbers.size(),2);
	bool good = false;
	for(unsigned int i = 0; i < res.quot; i++){
		if(runNumber >= fRunNumbers.at(2*i) && runNumber < fRunNumbers.at(2*i+1)){
			good = true;
			break;
		}
	}
	return good;
}
