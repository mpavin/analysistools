#include <vector>
#include <string>
#include <iostream>
#include "Functions.h"
#include <TH1D.h>
#include <TH2D.h>
#include <TFile.h>
#include <TChain.h>
#include <TTree.h>
#include <TMath.h>
#include <math.h>

using namespace std;


int main(){

	string inputFile = "list1.txt";

	TChain chain("data");
	
	string outName = "alignmentdata.root";
	TFile outFile(outName.c_str(), "RECREATE");
	ifstream ifs;
	string line;
	ifs.open(inputFile.c_str());
		
	if (!ifs.is_open()){
		cerr << "[ReadInputListFile] Could not open infile. Exiting..." << endl;			
		exit (101);
	}
    
	while (getline(ifs, line)){
		if(line.at(0)=='#')
			continue;
		chain.Add(line.c_str());
	}
	ifs.close();		

	int runNumber;
	double aX;
	double aY;
	double bX;
	double bY;
	double aX1;
	double aY1;
	double xBeam;
	double yBeam;
	double x;
	double y;
	double z;
	double px;
	double py;
	double pz;
	
	int q;
	int nVTPC;
	int nGTPC;
	int nMTPC;
	
	TBranch *b_aData = chain.GetBranch("Data");
 	chain.SetBranchAddress("aX", &aX);
 	chain.SetBranchAddress("bX", &bX);
 	chain.SetBranchAddress("aY", &aY);
 	chain.SetBranchAddress("bY", &bY);
 	chain.SetBranchAddress("aX1", &aX1);
 	chain.SetBranchAddress("aY1", &aY1);
	chain.SetBranchAddress("x", &x);
	chain.SetBranchAddress("xBeam", &xBeam);
	chain.SetBranchAddress("y", &y);
	chain.SetBranchAddress("yBeam", &yBeam);
	chain.SetBranchAddress("z", &z);
	chain.SetBranchAddress("px", &px);
	chain.SetBranchAddress("py", &py);
	chain.SetBranchAddress("pz", &pz);
	chain.SetBranchAddress("q", &q);

	chain.SetBranchAddress("runNumber", &runNumber);

	
	int nEntries = chain.GetEntries();
	
	TH2D beam("beam", "", 200, -2, 2, 200, -2, 2);
	TH1D xalign("xalign", "", 100, -3, 3);
	TH1D yalign("yalign", "", 100, -2, 2);
	TH1D xalign1("xalign1", "", 200, -1, 1);
	TH1D yalign1("yalign1", "", 200, -1, 1);
	TH2D xalignrun("xalignrun", "", 200, 10500, 10700, 100, -6, 6);
	TH2D yalignrun("yalignrun", "", 200, 10500, 10700, 100, -5, 5);
	TH1D momentum("mom", "", 200, 28, 32);
	TH1D theta("theta", "", 160, -1, 15);
	TH1D thetarel("thetarel", "", 160, -1, 15);
	
	TH2D thetamom("thetamom", "", 160, -1., 15., 100, 29.5, 31.5);
	double zu = -657.51;
	double zd = -567.51;
	cout << nEntries << endl;
	for(int i = 0; i < nEntries; ++i){
		
		chain.GetEntry(i);
		
		if(!((i+1)%100000))
			cout << "Processing event # "  << i+1 << endl;
		
		double r = sqrt((aX*zu+bX)*(aX*zu+bX) + (aY*zu+bX-0.285)*(aY*zu+bY-0.285));
		//if(r < 1.34) continue;
		
		double p = sqrt(px*px+py*py+pz*pz);
		if(p < 30) continue;
		
		double thb = 1000*atan(sqrt(aX1*aX1 + aY1*aY1));
		double th = 1000*atan(sqrt(px*px+py*py)/pz);
		
		double threl = 1000*acos((aX1*px+aY1*py+pz)/(sqrt(aX1*aX1+aY1*aY1+1)*p));
		//if(th - thb > 2) continue;
		if(threl >2.0) continue;
		thetamom.Fill(threl, p);
		xalign.Fill(xBeam - x+0.065);
		yalign.Fill(yBeam - y);	
		xalign1.Fill(aX*zd+bX - x);
		yalign1.Fill(aY*zd+bY - y);	
		xalignrun.Fill(runNumber, xBeam - x);
		yalignrun.Fill(runNumber, yBeam - y);
		momentum.Fill(p);
		beam.Fill(xBeam, yBeam);
		theta.Fill(th-thb);
		thetarel.Fill(threl);
	}
	//cout << nCount << " " << nEntries << endl;
	outFile.cd();
	xalign.Write();
	yalign.Write();
	xalign1.Write();
	yalign1.Write();
	xalignrun.Write();
	yalignrun.Write();
	momentum.Write();
	theta.Write();
	thetamom.Write();
	beam.Write();
	outFile.Close();
		
}





