#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <cstdlib>
#include <stdexcept>
//#include <TPRegexp.h>

#include <TH1D.h>
#include <TH2D.h>
#include <TTree.h>
#include <TFile.h>
#include <TMath.h>
#include <evt/Event.h>
#include <evt/EventHeader.h>
#include <io/EventFileChain.h>
#include <det/TriggerConst.h>
#include <det/BPDConst.h>
#include <evt/RawEvent.h>
#include <boost/format.hpp>
#include "BPDTPC.h"
#include "BPDTPCConfig.h"
#include <utl/Vector.h>
#include "EventSelection.h"
#include "TrackSelection.h"
#include "Constants.h"
#include "TreeDataTypes.h"
#include "TrackExtrapolation.h"
#include "TrackTools.h"
#include "AnaTrack.h"
#include "TargetAna.h"
//#include "Functions.h"

using namespace std;
using namespace io;
using namespace evt;
using namespace evt::rec;
using namespace det;
using namespace utl;
using namespace boost;
using namespace TrackTools;

void BPDTPC(int n){

	BPDTPCConfig cfg;
	string fileName = cfg.GetOutputDir() + "/beamtpc.root";

	EventSelection evSel;
	TrackSelection trSel;
	EventFileChain eventFileChain(cfg.GetJob());
	Event event;
	double targetZ = cfg.GetTarget().GetUpstreamPosition().GetZ();
	//TargetAna target;
	TrackExtrapolation trackExtrapolationBeam(false, 0.05, 30400);
	TrackExtrapolation trackExtrapolation(true, 0.05, 30400);
	trackExtrapolation.SetTarget(cfg.GetTarget());
	unsigned int nEvents = 0;


	TFile *outputFile = new TFile(fileName.c_str(), "RECREATE");

	unsigned int runNumber = 0;
	
	double aX = 0;
	double aY = 0;
	double bX = 0;
	double bY = 0;
	double aX1 = 0;
	double aY1 = 0;
	double xBeam = 0;
	double yBeam = 0;
	double x = 0;
	double y = 0;
	double z = 0;
	
	int q = 0;
	int nVTPC = 0;
	int nGTPC = 0;
	int nMTPC = 0;

	double px = 0;
	double py = 0;
	double pz = 0;
	
	outputFile->cd();
	TTree *tree = new TTree("data","");
   	tree->Branch("aX",&aX,"aX/D");
   	tree->Branch("aY",&aY,"aY/D");
   	tree->Branch("bX",&bX,"bX/D");
   	tree->Branch("bY",&bY,"bY/D");
   	tree->Branch("aX1",&aY,"aX1/D");
   	tree->Branch("aY1",&aY,"aY1/D");
   	tree->Branch("runNumber",&runNumber,"runNumber/I");
   	tree->Branch("xBeam",&xBeam,"xBeam/D");
   	tree->Branch("yBeam",&yBeam,"yBeam/D");
   	tree->Branch("x",&x,"x/D");
   	tree->Branch("y",&y,"y/D");
   	tree->Branch("z",&z,"z/D");
   	tree->Branch("nVTPC",&nVTPC,"nVTPC/I");
   	tree->Branch("nGTPC",&nGTPC,"nGTPC/I");
   	tree->Branch("nMTPC",&nMTPC,"nMTPC/I");
	tree->Branch("q",&q,"q/I");
	
   	tree->Branch("px",&px,"px/D");
   	tree->Branch("py",&py,"py/D");
   	tree->Branch("pz",&pz,"pz/D");
   	
	double startCov[15] = {25, 0, 0, 0, 0, 25, 0, 0, 0, 25, 0, 0, 25, 0, 25};
	bool isSim = false;
	bool check = false;
	while (eventFileChain.Read(event) == eSuccess && nEvents < n) {


		if(!check){
			if(event.GetSimEvent().HasMainVertex()){
				check = true;
				isSim = true;
			}
		}
		bool skipRun = false;
		EventHeader &evHeader = event.GetEventHeader();

		if(evHeader.GetRunNumber() != runNumber){
			runNumber = evHeader.GetRunNumber();
			skipRun = cfg.SkipRun(runNumber);
		}

		if(skipRun) continue;

   		++nEvents;
    
    	if (!(nEvents%1000))
      		cout << " --->Processing event # " << nEvents << endl;


		if(evSel.IsEventGood(event) == false)
			continue;	
		//cout << event.GetRecEvent().GetBeam().GetMomentum().GetZ() << endl;
		const RecEvent& recEvent = event.GetRecEvent();

		if(isSim){
			const evt::sim::Beam& beam = event.GetSimEvent().GetBeam();
		
			aX = beam.GetMomentum().GetX()/beam.GetMomentum().GetZ();
			aY = beam.GetMomentum().GetY()/beam.GetMomentum().GetZ();
			bX = beam.GetOrigin().GetX() - aX*beam.GetOrigin().GetZ();
			bY = beam.GetOrigin().GetY() - aY*beam.GetOrigin().GetZ();		
			//cout << aX << endl;			
		}
		else{
			const Fitted2DLine lineX = recEvent.GetBeam().Get(BPDConst::eX);
			const Fitted2DLine lineY = recEvent.GetBeam().Get(BPDConst::eY); 
			
			aX = lineX.GetSlope();
			bX = lineX.GetIntercept();
			aY = lineY.GetSlope();
			bY = lineY.GetIntercept();
		}
		double zDownstream = cfg.GetTarget().GetUpstreamPosition().GetZ() + cfg.GetTarget().GetLength(); 
		double zUpstream = cfg.GetTarget().GetUpstreamPosition().GetZ();
		//cout << zUpstream << " " << zDownstream << endl;
		//cout << lineX.GetSlope() << endl;
		TrackPar& startPar = trackExtrapolationBeam.GetStartTrackParam();
		TrackPar& stopPar = trackExtrapolationBeam.GetStopTrackParam();
		
		startPar.SetType(eKisel);
		stopPar.SetType(eKisel);		
		startPar.SetPar(0, aX*zUpstream + bX);
		startPar.SetPar(1, aY*zUpstream + bY);
		startPar.SetPar(2, aX);
		startPar.SetPar(3, aY);
		if(isSim){
			startPar.SetPar(4, 1/event.GetSimEvent().GetBeam().GetMomentum().GetMag());
		}
		else
			startPar.SetPar(4, 1/30.9236);
		startPar.SetZ(zUpstream);
		//cout << recEvent.GetBeam().GetMomentum().GetMag() << endl;
		stopPar = startPar;
		
		trackExtrapolationBeam.Extrapolate(zDownstream);
		//z = zDownstream;
		xBeam = stopPar.GetPar(0);
		yBeam = stopPar.GetPar(1);
		aX1 = stopPar.GetPar(2);
		aY1 = stopPar.GetPar(3);
		
		int nTracks = 0;
		//cout << beamInput.Par[2] << endl;
		for (std::list<rec::Track>::const_iterator iter = recEvent.Begin<rec::Track>();
           iter != recEvent.End<rec::Track>(); ++iter)
      	{

			int nToF = 0;
			const Track& track = *iter;
			double tofMass = 0;
			bool hasToF = false;
			int tofSId = 0;
			
			AnaTrack anaTrack(track, trackExtrapolation);

			for (std::list<rec::TOFMass>::const_iterator tofIter = recEvent.Begin<rec::TOFMass>();
           	tofIter != recEvent.End<rec::TOFMass>(); ++tofIter)
      		{

				nToF++;
				if(hasToF) break;

				const rec::TOFMass& tof = *tofIter;
				if(!tof.HasVertexTrack()) continue;
			
				const rec::VertexTrack& vtx = recEvent.Get(tof.GetVertexTrackId());

				if(!vtx.HasTrack()) continue;
			
				if(vtx.GetTrackIndex() != track.GetIndex()) continue;
				nToF++;
				if(vtx.GetNumberOfTOFMasses()<3){ 
					if(tof.GetTOFId() == TOFConst::eTOFF){
						tofMass = tof.GetSquaredMass();
						tofSId = tof.GetScintillatorId();
						hasToF = true;
					}
				}
			}
			

			if(hasToF){
				anaTrack.SetToFMassSquared(tofMass);
				anaTrack.SetToFScintillatorId(tofSId);
			}

			if(!trSel.IsTrackGood(anaTrack))
				continue;
			
			vector<TPCCluster> clusters;
			ClusterIndexIterator it;
			for(it = track.ClustersBegin(); it!= track.ClustersEnd(); it++){
				 
				const Cluster clust= recEvent.Get(*it);
				const Point& pos = clust.GetPosition();

				

				TPCCluster temp(pos.GetX(), pos.GetY(), pos.GetZ(), clust.GetPositionUncertainty( evt::rec::ClusterConst::eX), 
					clust.GetPositionUncertainty( evt::rec::ClusterConst::eY), clust.GetTPCId());
					//cout << clust.GetStatus() << endl;
				if(clust.GetTPCId() == det::TPCConst::eVTPC2 || clust.GetTPCId() == det::TPCConst::eMTPCL){
					clusters.push_back(temp);
				}

			}	
			if(clusters.size() < 30) continue;	
				
			TrackPar &trackPar = trackExtrapolation.GetStopTrackParam();
			trackPar.SetType(eKisel);
			trackPar.SetPar(0, clusters.at(0).GetPosition(eX));
			trackPar.SetPar(1, clusters.at(0).GetPosition(eY));
			trackPar.SetZ(clusters.at(0).GetPosition(eZ));

			double dz = clusters.at(1).GetPosition(eZ)-clusters.at(0).GetPosition(eZ);

			trackPar.SetPar(2, (clusters.at(1).GetPosition(eX)-clusters.at(0).GetPosition(eX))/dz);
			trackPar.SetPar(3, (clusters.at(1).GetPosition(eY)-clusters.at(0).GetPosition(eY))/dz);
			startCov[9] = TMath::Power((clusters.at(1).GetPosition(eX)-clusters.at(0).GetPosition(eX))/dz,2);
			startCov[12] = TMath::Power((clusters.at(1).GetPosition(eY)-clusters.at(0).GetPosition(eY))/dz,2);
				
			trackPar.SetPar(4, track.GetCharge()/track.GetMomentum().GetMag());

			trackPar.SetCov(startCov);
			trackPar.SetCharge(track.GetCharge());

			for(int i = 1; i < clusters.size(); i++){
				trackExtrapolation.DoKalmanStep(clusters.at(i));

			}	
			for(int i = 1; i < clusters.size(); i++){
				trackExtrapolation.DoKalmanStep(clusters.at(clusters.size()-1-i));
			}		
			//cout << 1/trackPar.GetPar(4)<< " " << track.GetMomentum().GetMag() << endl;			
			//trackPar.SetPar(4, 1./(0.29+1./trackPar.GetPar(4)));			
			nTracks++;
			anaTrack.DoExtrapolation();
			
			x = anaTrack.GetExtrapolatedPosition().GetX();
			y = anaTrack.GetExtrapolatedPosition().GetY();
			z = anaTrack.GetExtrapolatedPosition().GetZ();
			//cout << z << endl;
			px = anaTrack.GetMomentum().GetX();
			py = anaTrack.GetMomentum().GetY();
			pz = anaTrack.GetMomentum().GetZ();
			q = track.GetCharge();
			nVTPC = track.GetNumberOfClusters(eVTPC1) +  track.GetNumberOfClusters(eVTPC2);
			nGTPC = track.GetNumberOfClusters(eGTPC);
			nMTPC = track.GetNumberOfClusters(eMTPC);

			//cout << "radi" << endl;
		}
		
		if(nTracks==1){
			outputFile->cd();
			tree->Fill();
		}	
	}
	outputFile->cd();
	tree->Write();
	outputFile->Close();

}

