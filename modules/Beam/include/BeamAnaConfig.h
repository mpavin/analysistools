#ifndef BEAMANACONFIG_H
#define BEAMANACONFIG_H
#include <iostream>
#include <vector>
#include <string>

using namespace std;

class BeamAnaConfig{

	public:
		BeamAnaConfig()  {Read();}  //Constructor

		//*************************Get functions***********************************

		string GetOutputDir(){return fOutputDir;}
		string GetInputListFile(){return fInputData;}

		vector<int>& GetRunNumbers(){return fRunNumbers;}

		//*************************Set functions***********************************
		void Read(); //Reads main config file.


		void SetInputDataFile(string inputData){fInputData = inputData;}
		void SetOutputDir(string outputDir){fOutputDir = outputDir;}

		void SetRunNumbers(vector<int> fRunNumbers);

		vector<string>& GetJob();
		void ReadInputList();
		void ReadInputList(string inputData);

		string GetTimeString();
		

		bool SkipRun(int runNumber);
	private:


		
		string fInputData;
		string fOutputDir;

		vector<string> fJob;

		vector<int> fRunNumbers;
};

#endif
