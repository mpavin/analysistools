#!/bin/sh                                                                                                                                                                

MC=0
START=1
STEP=20
for (( i = 0 ; i < 838 ; i++ )); do
    bsub -q 1nh -J "beam_${i}" -e /afs/cern.ch/work/m/mpavin/logs/beam.err -o /afs/cern.ch/work/m/mpavin/logs/beam.log run.sh $((MC)) $((START)) $((STEP));
    START=$((START+STEP))

    sleep 0.2
done;

