#!/bin/sh

readonly shine_version='v1r4p0'
readonly shine_root="/afs/cern.ch/na61/Releases/SHINE/${shine_version}"
. "${shine_root}/scripts/env/lxplus_32bit_slc6.sh"
eval $("${shine_root}/bin/shine-offline-config" --env-sh)

RESDIR="../Results"
ADD=$1

PATH="${RESDIR}/${ADD}_beam*.root"

root="${ROOTSYS}/bin/root"
#echo $ROOTSYS
/bin/rm "list.txt"
for line in $PATH
do
	echo $line >>list.txt 
	#echo $line
done

if [[ $add == "mc" ]]; then
	$root -l 'merge.C("list.txt", "mc")'
else
	$root -l 'merge.C("list.txt", "data")'
fi
