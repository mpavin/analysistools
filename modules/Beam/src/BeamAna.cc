#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <cstdlib>
#include <stdexcept>
//#include <TPRegexp.h>

#include <TTree.h>
#include <TFile.h>
#include <evt/Event.h>
#include <evt/EventHeader.h>
#include <io/EventFileChain.h>
#include <det/TriggerConst.h>
#include <det/BPDConst.h>
#include <evt/RawEvent.h>
#include <boost/format.hpp>
#include "BeamAna.h"
#include "BeamAnaConfig.h"
#include <utl/Vector.h>
#include "EventSelection.h"
#include "Constants.h"
#include "TreeDataTypes.h"

using namespace std;
using namespace io;
using namespace evt;
using namespace evt::rec;
using namespace det;
using namespace utl;
using namespace boost;


void BeamAna(int n){

	BeamAnaConfig cfg;
	string fileName = cfg.GetOutputDir() + "/beam.root";

	EventSelection evSel;
	EventFileChain eventFileChain(cfg.GetJob());
	Event event;


	unsigned int nEvents = 0;


	TFile *outputFile = new TFile(fileName.c_str(), "RECREATE");
	outputFile->cd();
	TTree* beamTree = new TTree("BeamData", "BeamData");
	beamTree->SetAutoSave(-300000000);


	
	//BeamData bm;
	//beamTree->Branch("BeamData", &(bm.bpd1x), "bpd1x/D:bpd1y:bpd1z:bpd1Ex:bpd1Ey:bpd2x:bpd2y:bpd2z:bpd2Ex:bpd2Ey:bpd3x:bpd3y:bpd3z:bpd3Ex:bpd3Ey:slopex:interceptx:chi2Ndfx:slopey:intercepty:chi2Ndfy:trigger/I");
	unsigned int runNumber = 0;
	double slopeX = 0;
	double slopeY = 0;
	double interceptX = 0;
	double interceptY = 0;
	int trig = 0;
	vector<double> signal1x;
	vector<double> signal1y;
	vector<double> signal2x;
	vector<double> signal2y;
	vector<double> signal3x;
	vector<double> signal3y;
	
	beamTree->Branch("slopeX", &slopeX, "slopeX/D");
	beamTree->Branch("slopeY", &slopeY, "slopeY/D");
	beamTree->Branch("interceptX", &interceptX, "interceptX/D");
	beamTree->Branch("interceptY", &interceptY, "interceptY/D");
	beamTree->Branch("trigger", &trig, "trigger/I");
	beamTree->Branch("runNum", &runNumber, "runNum/I");
	beamTree->Branch("signal1x", "vector<double>", &signal1x);
	beamTree->Branch("signal1y", "vector<double>", &signal1y);
	beamTree->Branch("signal2x", "vector<double>", &signal2x);
	beamTree->Branch("signal2y", "vector<double>", &signal2y);
	beamTree->Branch("signal3x", "vector<double>", &signal3x);
	beamTree->Branch("signal3y", "vector<double>", &signal3y);
	
	int count = 0;
	
	while (eventFileChain.Read(event) == eSuccess && nEvents < n) {

		signal1x.clear();
		signal1y.clear();
		signal2x.clear();
		signal2y.clear();
		signal3x.clear();
		signal3y.clear();
		
	
		bool skipRun = false;
		EventHeader &evHeader = event.GetEventHeader();

		if(evHeader.GetRunNumber() != runNumber){
			runNumber = evHeader.GetRunNumber();
			skipRun = cfg.SkipRun(runNumber);
		}

		if(skipRun) continue;

	
   		++nEvents;
    
    	if (!(nEvents%1000))
      		cout << " --->Processing event # " << nEvents << endl;


		/*if(evSel.IsEventGood(event) == false)
			continue;	*/

		const RecEvent& recEvent = event.GetRecEvent();
		//if(recEvent.GetBeam().GetStatus() != 0) continue;
		
		trig = 0;
		const evt::raw::Trigger& trigger = event.GetRawEvent().GetBeam().GetTrigger();

		if(trigger.IsTrigger(TriggerConst::eT1, TriggerConst::ePrescaled))
			trig = trig | 1;
		if(trigger.IsTrigger(TriggerConst::eT2, TriggerConst::eAll))
			trig = trig | 2;
                if(trigger.IsTrigger(TriggerConst::eT3, TriggerConst::ePrescaled))
		  trig = trig | 4;
                if(trigger.IsTrigger(TriggerConst::eT4, TriggerConst::ePrescaled))
		  trig = trig | 8;

		//if(!trigger.IsTrigger(TriggerConst::eT2, TriggerConst::ePrescaled)) cout << "works" << endl;
		//if(!trigger.IsTrigger(TriggerConst::eT2, TriggerConst::ePrescaled) && trigger.IsTrigger(TriggerConst::eT3, TriggerConst::eAll)) cout << "radi" << endl;
		//if(!trigger.IsTrigger(TriggerConst::eT2, TriggerConst::ePrescaled) && !trigger.IsTrigger(TriggerConst::eT3, TriggerConst::eAll))
			continue;
		
		//if(trig==0) cout << "0" << endl;
		count++;
		evt::raw::BPDPlane rawBPD1x = event.GetRawEvent().GetBeam().GetBPDPlane(BPDConst::eBPD1x);
		evt::raw::BPDPlane rawBPD1y = event.GetRawEvent().GetBeam().GetBPDPlane(BPDConst::eBPD1y);
		evt::raw::BPDPlane rawBPD2x = event.GetRawEvent().GetBeam().GetBPDPlane(BPDConst::eBPD2x);
		evt::raw::BPDPlane rawBPD2y = event.GetRawEvent().GetBeam().GetBPDPlane(BPDConst::eBPD2y);
		evt::raw::BPDPlane rawBPD3x = event.GetRawEvent().GetBeam().GetBPDPlane(BPDConst::eBPD3x);
		evt::raw::BPDPlane rawBPD3y = event.GetRawEvent().GetBeam().GetBPDPlane(BPDConst::eBPD3y);
		
		for(int i = 0; i < rawBPD1x.GetNStrips(); i++){
			signal1x.push_back(rawBPD1x.GetSignal(i));
			signal1y.push_back(rawBPD1y.GetSignal(i));
			signal2x.push_back(rawBPD2x.GetSignal(i));
			signal2y.push_back(rawBPD2y.GetSignal(i));
			signal3x.push_back(rawBPD3x.GetSignal(i));
			signal3y.push_back(rawBPD3y.GetSignal(i));
		}
		
		//bm.trigger = trig;
		/*if(trigger.IsTrigger(TriggerConst::eT2, TriggerConst::eAll)){
			trig = 2;
		}
		else if(trigger.IsTrigger(TriggerConst::eT3, TriggerConst::eAll)){
			trig = 3;
		}
		else{
			trig = 0;
			continue;
		}*/
		/*const evt::raw::BPDPlane rawBPD1x = event.GetRawEvent().GetBeam().GetBPDPlane(BPDConst::eBPD1x);
		const evt::raw::BPDPlane rawBPD1y = event.GetRawEvent().GetBeam().GetBPDPlane(BPDConst::eBPD1y);
		const evt::raw::BPDPlane rawBPD2x = event.GetRawEvent().GetBeam().GetBPDPlane(BPDConst::eBPD2x);
		const evt::raw::BPDPlane rawBPD2y = event.GetRawEvent().GetBeam().GetBPDPlane(BPDConst::eBPD2y);
		const evt::raw::BPDPlane rawBPD3x = event.GetRawEvent().GetBeam().GetBPDPlane(BPDConst::eBPD3x);
		const evt::raw::BPDPlane rawBPD3y = event.GetRawEvent().GetBeam().GetBPDPlane(BPDConst::eBPD3y);

		double qX[3] = {0, 0, 0};
		double qY[3] = {0, 0, 0};
		double x[3] = {0, 0, 0};
		double y[3] = {0, 0, 0};
		double xS2[3] = {0, 0, 0};
		double yS2[3] = {0, 0, 0};
		double signalX[3] = {0, 0, 0};
		double signalY[3] = {0, 0, 0};*/

		/*for(int i = 0; i < rawBPD1x.GetNStrips(); ++i){
			signalX[0] = rawBPD1x.GetSignal(i);
			signalX[1] = rawBPD2x.GetSignal(i);
			signalX[2] = rawBPD3x.GetSignal(i);
			
			signalY[0] = rawBPD1y.GetSignal(i);
			signalY[1] = rawBPD2y.GetSignal(i);
			signalY[2] = rawBPD3y.GetSignal(i);

			for(int j = 0; j < 3; ++j){
				qX[j] += signalX[j];
				qY[j] += signalY[j];
				x[j] += signalX[j]*(i+1);
				y[j] += signalY[j]*(i+1);
				//xS2[j] += signalX[j]*signalX[j]*(i+1);
				//yS2[j] += signalY[j]*signalY[j]*(i+1);
				xS2[j] += signalX[j]*signalX[j];
				yS2[j] += signalY[j]*signalY[j];
			}

		}
		//cout << 0.2*sqrt(test)/qX[0]/sqrt(24) << endl;
		
      	const evt::rec::BPDPlane bpd1X = recEvent.GetBeam().GetBPDPlane(BPDConst::eBPD1, BPDConst::eX);
      	const evt::rec::BPDPlane bpd1Y = recEvent.GetBeam().GetBPDPlane(BPDConst::eBPD1, BPDConst::eY);
      	bm.bpd1x = bpd1X.GetPosition();
      	bm.bpd1y = bpd1Y.GetPosition();
      	bm.bpd1z = bpd1X.GetZ();
      	
      	//cout << bm.bpd1x << endl;
		bm.bpd1Ex = bpd1X.GetPositionError();
		bm.bpd1Ey = bpd1Y.GetPositionError();	
		//bm.bpd1Ex = sqrt(xS2[0]/qX[0] - x[0]*x[0]/qX[0]/qX[0])*sqrt(2)/sqrt(qX[0])*0.2;
		//bm.bpd1Ey = sqrt(yS2[0]/qY[0] - y[0]*y[0]/qY[0]/qY[0])*sqrt(2)/sqrt(qY[0])*0.2;	
		//bm.bpd1Ex = 0.2*sqrt(xS2[0]/24)/qX[0];
		//bm.bpd1Ey = 0.2*sqrt(yS2[0]/24)/qY[0];	
		
		//cout << bm.bpd1Ex << endl;
		
      	const evt::rec::BPDPlane bpd2X = recEvent.GetBeam().GetBPDPlane(BPDConst::eBPD2, BPDConst::eX);
      	const evt::rec::BPDPlane bpd2Y = recEvent.GetBeam().GetBPDPlane(BPDConst::eBPD2, BPDConst::eY);
      	bm.bpd2x = bpd2X.GetPosition();
      	bm.bpd2y = bpd2Y.GetPosition();
      	bm.bpd2z = bpd2X.GetZ();
		bm.bpd2Ex = bpd2X.GetPositionError();
		bm.bpd2Ey = bpd2Y.GetPositionError();	
		//bm.bpd2Ex = sqrt(xS2[1]/qX[1] - x[1]*x[1]/qX[1]/qX[1])*sqrt(2)/sqrt(qX[1])*0.2;
		//bm.bpd2Ey = sqrt(yS2[1]/qY[1] - y[1]*y[1]/qY[1]/qY[1])*sqrt(2)/sqrt(qY[1])*0.2;	
		//bm.bpd2Ex = 0.2*sqrt(xS2[1]/24)/qX[1];
		//bm.bpd2Ey = 0.2*sqrt(yS2[1]/24)/qY[1];	
						      
      	const evt::rec::BPDPlane bpd3X = recEvent.GetBeam().GetBPDPlane(BPDConst::eBPD3, BPDConst::eX);
      	const evt::rec::BPDPlane bpd3Y = recEvent.GetBeam().GetBPDPlane(BPDConst::eBPD3, BPDConst::eY);
      	bm.bpd3x = bpd3X.GetPosition();
      	bm.bpd3y = bpd3Y.GetPosition();
      	bm.bpd3z = bpd3X.GetZ();
		bm.bpd3Ex = bpd3X.GetPositionError();
		bm.bpd3Ey = bpd3Y.GetPositionError();	*/
		//bm.bpd3Ex = sqrt(xS2[2]/qX[2] - x[2]*x[2]/qX[2]/qX[2])*sqrt(2)/sqrt(qX[2])*0.2;
		//bm.bpd3Ey = sqrt(yS2[2]/qY[2] - y[2]*y[2]/qY[2]/qY[2])*sqrt(2)/sqrt(qY[2])*0.2;	
		//bm.bpd3Ex = 0.2*sqrt(xS2[2]/24)/qX[2];
		//bm.bpd3Ey = 0.2*sqrt(yS2[2]/24)/qY[2];	
					      	
     	const Fitted2DLine lineX = recEvent.GetBeam().Get(BPDConst::eX);
    	const Fitted2DLine lineY = recEvent.GetBeam().Get(BPDConst::eY); 
    	
    	slopeX = lineX.GetSlope();
    	slopeY = lineY.GetSlope();
    	interceptX = lineX.GetIntercept();
    	interceptY = lineY.GetIntercept();
    	/*bm.slopex = lineX.GetSlope();
    	bm.interceptx = lineX.GetIntercept();    
    	bm.chi2Ndfx = lineX.GetChi2();
    	//bm.chi2Ndfx = pow((bm.bpd1x - (bm.slopex*bm.bpd1z + bm.interceptx))/bm.bpd1Ex,2) + pow((bm.bpd2x - (bm.slopex*bm.bpd2z + bm.interceptx))/bm.bpd2Ex,2) + pow((bm.bpd3x - (bm.slopex*bm.bpd3z + bm.interceptx))/bm.bpd3Ex,2);
     	bm.slopey = lineY.GetSlope();
    	bm.intercepty = lineY.GetIntercept(); 	
		bm.chi2Ndfy = lineY.GetChi2();*/
    	//bm.chi2Ndfy = pow((bm.bpd1y - (bm.slopey*bm.bpd1z + bm.intercepty))/bm.bpd1Ey,2) + pow((bm.bpd2y - (bm.slopey*bm.bpd2z + bm.intercepty))/bm.bpd2Ey,2) + pow((bm.bpd3y - (bm.slopey*bm.bpd3z + bm.intercepty))/bm.bpd3Ey,2);		
		//outputFile->cd();
		//cout << bm.slopey << " " << bm.intercepty << endl;	
		beamTree->Fill();
	}
	outputFile->cd();
	beamTree->Write();
	cout << count << endl;
	outputFile->Close();

}

