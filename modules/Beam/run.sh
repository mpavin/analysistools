#!/bin/sh
 
#set -e


MYDIR="/afs/cern.ch/user/m/mpavin/Work/analysistools/modules/Beam"

DATADIR="/eos/experiment/na61/data/test/p_C_31_10/030_14d_v0r8p0_pp_slc6_phys_FTOF_test/updated"
MCDIR="/eos/na61/data/Simulation/p_T2K_replica_target_10/v15a/FLUKA2011.2c.2_GCALOR/10_030/20MF_all_pp/shoe"

#**********************************************************************************
# SELECT DATA OR MC
if [[ $1 == "1" ]]; then
	UDIR=${MCDIR}
	FILEADD="mc"
	
else
	UDIR=${DATADIR}
	FILEADD="data"
fi
#**********************************************************************************

#**********************************************************************************
# ARE DATA(MC) FILES ON EOS?
if [[ $UDIR == *"eos"* ]]; then
	EOSIN="1"
else
	EOSIN="0"
fi
#**********************************************************************************

#**********************************************************************************
#	SHINE ENV
readonly shine_version='v1r4p0'
readonly shine_root="/afs/cern.ch/na61/Releases/SHINE/${shine_version}"
. "${shine_root}/scripts/env/lxplus_32bit_slc6.sh"
eval $("${shine_root}/bin/shine-offline-config" --env-sh)
#**********************************************************************************

#**********************************************************************************
#	EOS
eos="/afs/cern.ch/project/eos/installation/0.3.84-aquamarine/bin/eos.select"
eosumount="/afs/cern.ch/project/eos/installation/0.3.84-aquamarine/bin/eos.select -b fuse umount"                                                                               
eosmount="/afs/cern.ch/project/eos/installation/0.3.84-aquamarine/bin/eos.select -b fuse mount"    
#**********************************************************************************

#**********************************************************************************
#	TMP DIR
if [ -n "${LSB_BATCH_JID}" ]; then
        tmpdir="/pool/lsf/${USER}/${LSB_BATCH_JID}"                                                                                                                          
else
        tmpdir="${TMPDIR:=/tmp/${USER}}"                                                                                                                        
fi
#**********************************************************************************

#**********************************************************************************
#	DIR FOR RESULTS
export RES="${tmpdir}/Results"
cd $tmpdir
mkdir Results
#**********************************************************************************

if [[ $EOSIN == "1" ]]; then
	ls ${UDIR}/ > "${tmpdir}/log.txt"
else
	ls "${UDIR}/*.root" > "${tmpdir}/log.txt"
fi

COUNT=0
START=$2
STEP=$3
STOP=$((START+STEP))

while read line
do
		COUNT=$((COUNT+1))
		if [ "$COUNT" -ge "$START" ] && [ "$COUNT" -lt "$STOP" ]; then
			if [[ $EOSIN == "1" ]]; then
				cp "$UDIR/$line" "./"
				echo "$UDIR/$line"
			else
				cp "$UDIR/$line" "./"
			fi			
		fi
done < "${tmpdir}/log.txt"

for i in *.root; 
do 
	echo "${tmpdir}/${i}" >> list.txt
done;

cd ${MYDIR}
export INPUTFILE="${tmpdir}/list.txt"
./beam -b bootstrap.xml
#cp "${RES}/beam.root" "/afs/cern.ch/work/m/mpavin/Results/BEAM/${FILEADD}_beam_${START}-${STOP}.root"
#cp "${RES}/cuts.root" "Results/${FILEADD}_cuts_${START}-${STOP}.root"
#cp -r Results/ ${MYDIR}/Results/
