#ifndef CONSTANTS_H
#define CONSTANTS_H

const int nZBins = 6;
const int zBins[7] = {0, 18., 36., 54., 72., 89.99, 90.1};

const int nThBins = 40;
const double thetaMin = 0;
const double thetaStep = 10;
const double thetaMax = 400;

const int nPBins = 155;
const double pMin = 0;
const double pStep = 0.02;
const double pMax = 31;


const double xTarget = 0.19;
const double yTarget = 0.18;
const double zTarget = 0.00;
const double rTarget = 1.3;

const double zBeam = -6.10;

#endif
