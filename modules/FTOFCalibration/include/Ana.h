#ifndef ANA_H
#define ANA_H

#include <vector>
#include <string>

#include "EventSelection.h"
#include "TrackSelection.h"

#include "FTOFConfig.h"
#include "TreeDataTypes.h"
#include <TTree.h>

using namespace std;

class Ana {

	public:

		Ana (unsigned int max): fFTOFConfig(FTOFConfig()), fFTOF(NULL)  {
			SetMaxNumEvents(max);
		}

		void SetMainConfigFile(string file){fMainConfigFile = file;}
		void SetMaxNumEvents(unsigned int num){fMaxEv = num;}

		void ExtractTOFData();
		void UpdateTOFData();
		void UpdateTOFMC();
		void WriteData();
		

		

	private:


		string fMainConfigFile;
		string fInputFile;
		
		unsigned int fMaxEv;
		FTOFConfig fFTOFConfig;
		TTree *fFTOF;


};

#endif
