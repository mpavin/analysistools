#ifndef FTOFCONFIG_H
#define FTOFCONFIG_H
#include <iostream>
#include <vector>
#include <string>

#include "TargetAna.h"



using namespace std;


class FTOFConfig{

	public:
		FTOFConfig()  {Read();}  //Constructor

		//*************************Get functions***********************************
		TargetAna& GetTarget(){return fTarget;} //Returns target.

		string GetOutputDir(){return fOutputDir;}
		double GetFTOFZ(){return fFTOFZ;}
		string GetInputListFile(){return fInputData;}
		double GetScintillatorXPosition(int scintId);
		double GetTopPMTT0(int scintId);
		double GetBottomPMTT0(int scintId);
		double GetCInv(){return fCInv;}
		double GetS1T0(int pmtId);
		double GetS1Res(int pmtId);
		double GetTDCRes(){return fTDCRes;}
		double GetTOFCut(){return fTOFCut;}
		string GetTOFRes(){return fTOFRes;}
		bool IsActive(int scintId, int pmtId);
		vector<int>& GetRunNumbers(){return fRunNumbers;}

		//*************************Set functions***********************************
		void Read(); //Reads main config file.

		void SetTarget(string name, ETargetType type, double length, double radius, double positionX, double positionY, double positionZ, double tilt1, double tilt2); //Sets target's parameters.

		void SetTarget(TargetAna target){fTarget = target;} //Sets target's parameters.


		void SetInputDataFile(string inputData){fInputData = inputData;}
		void SetOutputDir(string outputDir){fOutputDir = outputDir;}
		void SetFTOFZ(double z){fFTOFZ = z;}
		void SetRunNumbers(vector<int> fRunNumbers);

		vector<string>& GetJob();
		void ReadInputList();
		void ReadInputList(string inputData);
	

		bool SkipRun(int runNumber);
	private:


		TargetAna fTarget; //Target.

		double fFTOFZ;
		string fInputData;
		string fOutputDir;

		string fTOFRes;
		vector<string> fJob;
		double fTDCRes;
		double fTOFCut;
		vector<int> fRunNumbers;
		vector<bool> fActive1;
		vector<bool> fActive2;
		vector<double> fXPos;
		vector<double> fT0Top;
		vector<double> fT0Bottom;
		double fCInv;
		vector<double> fS1T0;
		vector<double> fS1Res;
		
};

#endif
