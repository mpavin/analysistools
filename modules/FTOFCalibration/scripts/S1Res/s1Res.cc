#include <stdlib.h>
#include <iostream>
#include <vector>
#include <string>
#include <ChainDef.h>
#include <Functions.h>
#include <TOFMapping.h>
#include <TChain.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TFile.h>
#include <sstream>
#include <math.h>
#include <T0Def.h>
#include <CDef.h>
#include <TMath.h>
#include <dEdxResolution.h>
#include <TMatrixD.h>
using namespace std;

int main(){

	string fileName = "../../input/list2010.txt";
	string scintPos = "scintPos.dat";
	string t0file = "t0Values2010.root";
	string cfile = "cValues.root";
	string dEdxName = "dEdxParData.root";
	
	TH1D var1("var1", "", 200, -0.05, 0.09);
	TH1D var2("var2", "", 200, -0.05, 0.09);
	TH2D tpix("tpix", "", 200, -3, 3, 200, -1500, 1500);
	TH2D tpiy("tpiy", "", 200, -3, 3, 200, -1500, 1500);
	TH2D tpis12("tpis12", "", 100, -200, 200, 100, -1000, 1000);
	TH2D tpis13("tpis13", "", 100, -200, 200, 100, -1000, 1000);
	TH2D tpis14("tpis14", "", 100, -200, 200, 100, -1000, 1000);
	TH2D tpis12a("tpis12a", "", 100, -200, 200, 100, -1000, 1000);
	TH2D tpis13a("tpis13a", "", 100, -200, 200, 100, -1000, 1000);
	TH2D tpis14a("tpis14a", "", 100, -200, 200, 100, -1000, 1000);
	//TH1D s1corr("s14tdcr", "", 200, 0, -1500, 1500); 	

	TH2D tpixpa("tpixpa", "", 200, -3, 3, 200, -1500, 1500);
	TH2D tpiypa("tpiypa", "", 200, -3, 3, 200, -1500, 1500);
	double pMin = 0.5;
	double pMax = 1.;	

	
	double dEdxMax = 1.20;
	//double zLastCut = 650;

	double zcutMin = -657.41;
	double zcutMax = -567.40;
	
	/*vector<double> ranges;
	for(int i = 0; i < 8; i++){
		ranges.push_back(0.3);
	}
	dEdxResolution dEdxRes(dEdxName, ranges);*/
	
	TChain chaint0("T0");
	ReadT0(chaint0, t0file);

	TChain chainc("cScint");
	ReadC(chainc, cfile);
	
	TChain chain("FTOF");
	ReadData(chain, fileName);

	vector<string> sPos;
	vector<double> xPos;
	ReadInputList(scintPos, sPos);
	for(unsigned int i = 0; i < sPos.size(); i++){
		
		stringstream convert(sPos.at(i));
		double res;
		convert >> res;
		xPos.push_back(res);
	}
	
	double nmaxpi = 145000.;
	vector<double> tpi;
	vector<double> ppi;
	vector<double> lpi;
	vector<double> ts12;
	vector<double> ts13;
	vector<double> ts14;
	vector<double> xb;
	vector<double> yb;
	unsigned int nEntries = chain.GetEntries();
	cout << "Number of entries: " << nEntries << endl;
	int n = 0;
	for(unsigned int i = 0; i < nEntries; i++){
		if(!((i+1)%100000)) cout << "Processing entry: " << i+1 << endl;
		chain.GetEntry(i);

		if(n > nmaxpi) break;
		if(trigger < 1) continue;
 		
		if(runNumber > 10864 && runNumber < 10874) continue;
		if(runNumber > 10939 && runNumber < 10945) continue; 	
		if(runNumber > 11089) continue;  
		if(tdcS1->at(1) > 2500 || tdcS1->at(2) > 2500 || tdcS1->at(3) > 2500) continue;

 		vector<int> active;
		vector<double> tof1;
		vector<double> tof2;
		
		//****************************************************************************************************************
		// Cheching for active scintillators
		//double fact[2] = {-1, 1};

		for(unsigned int j = 0; j < nScint; j++){
 			
 			chaint0.GetEntry(j);
			
			
			int both[2] = {0,0};
			double tof[2] = {0, 0};
 			for(unsigned int k = 0; k < 2; k++){
 			 
 			 	if(maskPMT[j][k]==0){
 			 		both[k] = 1;
 			 		continue;
 			 	}
 			 	
 				if(tdc->at(pmtId[j][k]) < 1) continue;
 				if(tdc->at(pmtId[j][k]) >= tdcChan+93) continue;
 				
			
 				tof[k] = tdc->at(pmtId[j][k])*channelSize - t0[k]+27;
 				both[k] = 1;
 							
 			}
 			
 			if(both[0]&&both[1]){
				
 				active.push_back(j);
 				tof1.push_back(tof[0]);
 				tof2.push_back(tof[1]);
 			}			
		}	
 		
 		
		
		//****************************************************************************************************************
		// Matching tracks and scintillators	
		vector<int> trIndex;
		vector<int> nTr;
		//cout << "radi1" << endl;
		for(int j = 0; j < active.size(); j++){
			trIndex.push_back(-999);
			nTr.push_back(0);
		}
		for(unsigned int j = 0; j < xStart->size(); j++){

			for(int k = 0; k < active.size(); k++){

				
 				if(!(active.at(k)%2)){
 				//if((j%2)){
 					if(abs(xTOF2->at(j)-xPos.at(active.at(k))) > swidth/2.) continue;
 					if(abs(yTOF2->at(j)) > slength/2.) continue;

 				}
 				else{
 					if(abs(xTOF1->at(j)-xPos.at(active.at(k))) > swidth/2.) continue;
 					if(abs(yTOF1->at(j)) > slength/2.) continue;
 				}
				nTr[k]++;
				trIndex.at(k) = j;
				
			}
		
		} 		
 		
		//****************************************************************************************************************
		// Calculating m2TOF 	
		
		for(int j = 0; j < active.size(); j++){
			
			
			chainc.GetEntry(active.at(j));
			//if(active.at(j) == 26) cout << c[0] << " " << c[1] << endl;
			
			if(nTr.at(j) != 1) continue;
			if(p->at(trIndex[j]) < pMin || p->at(trIndex[j]) > pMax) continue;
			if(dEdx->at(trIndex[j]) > dEdxMax) continue;
			//if(RST->at(trIndex[j])!=0) continue;

			if(zStart->at(trIndex[j])<zcutMin || zStart->at(trIndex[j]) >= zcutMax) continue;
 			//if(zVert->at(k)<zcutMin || zVert->at(k) >= zcutMax) continue;


 			//if(zTPCLast->at(k) < zLastCut) continue;
			if(topology->at(trIndex[j]) == 9){
				if(nVTPC->at(trIndex[j]) < 20) continue;
				if(nMTPC->at(trIndex[j]) < 30) continue;
			}
			else if(topology->at(trIndex[j]) == 10) continue;
			else if(topology->at(trIndex[j]) == 11){
				if(nVTPC->at(trIndex[j]) < 20) continue;
				if(nMTPC->at(trIndex[j]) < 30) continue;
			}
			else if(topology->at(trIndex[j]) == 12){
				if(nGTPC->at(trIndex[j]) < 6) continue;
				if(nMTPC->at(trIndex[j]) < 70) continue;
			}	
			else if(topology->at(trIndex[j]) == 13) continue;

			else if(topology->at(trIndex[j]) == 14){
				if(nVTPC->at(trIndex[j]) < 20) continue;
				if(nMTPC->at(trIndex[j]) < 30) continue;
			}	
			else if(topology->at(trIndex[j]) == 15){
				if(nVTPC->at(trIndex[j]) < 20) continue;
				if(nMTPC->at(trIndex[j]) < 30) continue;
			}	
			if(distToTarg->at(trIndex[j]) > sqrt(TMath::Power(xStart->at(trIndex[j])*xEStart->at(trIndex[j]),2)+TMath::Power(xStart->at(trIndex[j])*xEStart->at(trIndex[j]),2))/sqrt(xStart->at(trIndex[j])*xStart->at(trIndex[j])+yStart->at(trIndex[j])*yStart->at(trIndex[j])))	
				continue;	

			if(RST->at(trIndex[j]) == 1){
				if(theta->at(trIndex[j]) > 0.18){
					if((phi->at(trIndex[j])<-10 || phi->at(trIndex[j])>10) && (phi->at(trIndex[j])<170 || phi->at(trIndex[j])>190)) continue;
				}
				else if(theta->at(trIndex[j]) > 0.13 && theta->at(trIndex[j]) < 0.18){
					if((phi->at(trIndex[j])<-15 || phi->at(trIndex[j])>15) && (phi->at(trIndex[j])<165 || phi->at(trIndex[j])>195)) continue;
				}
				else if(theta->at(trIndex[j]) > 0.11 && theta->at(trIndex[j]) < 0.13){
					if((phi->at(trIndex[j])<-20 || phi->at(trIndex[j])>20) && (phi->at(trIndex[j])<160 || phi->at(trIndex[j])>200)) continue;
				}
				else if(theta->at(trIndex[j]) > 0.09 && theta->at(trIndex[j]) < 0.11){
					if((phi->at(trIndex[j])<-25 || phi->at(trIndex[j])>25) && (phi->at(trIndex[j])<155 || phi->at(trIndex[j])>205)) continue;
				}
				else if(theta->at(trIndex[j]) > 0.07 && theta->at(trIndex[j]) < 0.09){
					if((phi->at(trIndex[j])<-35 || phi->at(trIndex[j])>35) && (phi->at(trIndex[j])<145 || phi->at(trIndex[j])>215)) continue;
				}
				else if(theta->at(trIndex[j]) > 0.06 && theta->at(trIndex[j]) < 0.07){
					if((phi->at(trIndex[j])<-45 || phi->at(trIndex[j])>40) && (phi->at(trIndex[j])<140 || phi->at(trIndex[j])>225)) continue;
				}
				else if(theta->at(trIndex[j]) > 0.05 && theta->at(trIndex[j]) < 0.06){
					if((phi->at(trIndex[j])<-55 || phi->at(trIndex[j])>50) && (phi->at(trIndex[j])<130 || phi->at(trIndex[j])>235)) continue;
				}
				else if(theta->at(trIndex[j]) > 0.04 && theta->at(trIndex[j]) < 0.05){
					if((phi->at(trIndex[j])<-70|| phi->at(trIndex[j])>65) && (phi->at(trIndex[j])<115 || phi->at(trIndex[j])>250)) continue;
				}
				else if(theta->at(trIndex[j]) > 0.03 && theta->at(trIndex[j]) < 0.04){
					if((phi->at(trIndex[j])<-65|| phi->at(trIndex[j])>60) && (phi->at(trIndex[j])<110 || phi->at(trIndex[j])>250)) continue;
				}
				else if(theta->at(trIndex[j]) > 0.01 && theta->at(trIndex[j]) < 0.03){
					if((phi->at(trIndex[j])<-70|| phi->at(trIndex[j])>70) && (phi->at(trIndex[j])<110 || phi->at(trIndex[j])>250)) continue;
				}
				else if(theta->at(trIndex[j]) > 0.00 && theta->at(trIndex[j]) < 0.01){
					if((phi->at(trIndex[j])<-70|| phi->at(trIndex[j])>70) && (phi->at(trIndex[j])<110 || phi->at(trIndex[j])>250)) continue;
				}
			}
			else{
				if(theta->at(trIndex[j]) > 0.20){
					if((phi->at(trIndex[j])<-15 || phi->at(trIndex[j])>13) && (phi->at(trIndex[j])<167 || phi->at(trIndex[j])>195)) continue;
				}
				else if(theta->at(trIndex[j]) > 0.14 && theta->at(trIndex[j]) < 0.20){
					if((phi->at(trIndex[j])<-18|| phi->at(trIndex[j])>16) && (phi->at(trIndex[j])<164 || phi->at(trIndex[j])>198)) continue;
				}
				else if(theta->at(trIndex[j]) > 0.12 && theta->at(trIndex[j]) < 0.14){
					if((phi->at(trIndex[j])<-22|| phi->at(trIndex[j])>20) && (phi->at(trIndex[j])<160 || phi->at(trIndex[j])>202)) continue;
				}
				else if(theta->at(trIndex[j]) > 0.10 && theta->at(trIndex[j]) < 0.12){
					if((phi->at(trIndex[j])<-25|| phi->at(trIndex[j])>23) && (phi->at(trIndex[j])<157 || phi->at(trIndex[j])>205)) continue;
				}
				else if(theta->at(trIndex[j]) > 0.09 && theta->at(trIndex[j]) < 0.10){
					if((phi->at(trIndex[j])<-28|| phi->at(trIndex[j])>26) && (phi->at(trIndex[j])<154 || phi->at(trIndex[j])>208)) continue;
				}
				else if(theta->at(trIndex[j]) > 0.08 && theta->at(trIndex[j]) < 0.09){
					if((phi->at(trIndex[j])<-32|| phi->at(trIndex[j])>30) && (phi->at(trIndex[j])<150 || phi->at(trIndex[j])>212)) continue;
				}
				else if(theta->at(trIndex[j]) > 0.07 && theta->at(trIndex[j]) < 0.08){
					if((phi->at(trIndex[j])<-35|| phi->at(trIndex[j])>32) && (phi->at(trIndex[j])<148 || phi->at(trIndex[j])>215)) continue;
				}
				else if(theta->at(trIndex[j]) > 0.06 && theta->at(trIndex[j]) < 0.07){
					if((phi->at(trIndex[j])<-44|| phi->at(trIndex[j])>40) && (phi->at(trIndex[j])<140 || phi->at(trIndex[j])>224)) continue;
				}
				else if(theta->at(trIndex[j]) > 0.05 && theta->at(trIndex[j]) < 0.06){
					if((phi->at(trIndex[j])<-48|| phi->at(trIndex[j])>44) && (phi->at(trIndex[j])<136 || phi->at(trIndex[j])>228)) continue;
				}
				else if(theta->at(trIndex[j]) > 0.04 && theta->at(trIndex[j]) < 0.05){
					if((phi->at(trIndex[j])<-53|| phi->at(trIndex[j])>53) && (phi->at(trIndex[j])<140 || phi->at(trIndex[j])>220)) continue;
				}
				else if(theta->at(trIndex[j]) > 0.02 && theta->at(trIndex[j]) < 0.04){
					if((phi->at(trIndex[j])<-40|| phi->at(trIndex[j])>40) && (phi->at(trIndex[j])<150 || phi->at(trIndex[j])>210)) continue;
				}
				else if(theta->at(trIndex[j]) > 0.00 && theta->at(trIndex[j]) < 0.02){
					if((phi->at(trIndex[j])<-50|| phi->at(trIndex[j])>50) && (phi->at(trIndex[j])<130 || phi->at(trIndex[j])>230)) continue;
				}
			}
 				 				
 			//if(theta->at(trIndex[j])>0.380) continue;
 			//zVertexPos.Fill(zVert->at(trIndex[j]));
 			double len = length->at(trIndex[j]);
			double yVal = yTOF1->at(trIndex[j]);

 		
 			if(!(active.at(j)%2)){
 					//if((j%2)){
				yVal = yTOF2->at(trIndex[j]);
 				len+=sqrt(pow(xTOF2->at(trIndex[j])-xTOF1->at(trIndex[j]), 2)+pow(yTOF2->at(trIndex[j])-yTOF1->at(trIndex[j]), 2)+pow(zTOF2->at(trIndex[j])-zTOF1->at(trIndex[j]), 2));
 			}

 			
			
			//if(len > 0.000386*len + 1395) continue;
			double tc = 0;
			double count = 0;
 			if(maskPMT[active.at(j)][0]==1){

 				tof1.at(j) = tof1.at(j) - 70.25*(60-yVal);
				tc += tof1.at(j);
				count +=1.;
 			}
 			if(maskPMT[active.at(j)][1]==1){

 				tof2.at(j) = tof2.at(j) - 70.25*(60+yVal);
 				tc += tof2.at(j);	
 				count +=1.;						
 			}
 			tc =tc/count;

			if(maskPMT[active.at(j)][0]==1&&maskPMT[active.at(j)][1]==1){
				if(abs(tof1.at(j) -tof2.at(j))>2000) continue;
				
			}					
 		
 			double mc2 = p->at(trIndex[j])*p->at(trIndex[j])*(pow(0.029979*tc,2)/pow(len,2)-1);
 			if(mc2 > 0.17) continue;
 			var1.Fill(mc2);
 			n++;
 			
 			double x = beamD.aX*zS1 + beamD.bX;
			double y = beamD.aY*zS1 + beamD.bY;
			/*tpix.Fill((x+y)/sqrt(2), tc-(len/cl)*sqrt(0.139470*0.139470/(p->at(trIndex[j])*p->at(trIndex[j]))+1));
			tpiy.Fill((-x+y)/sqrt(2), tc-(len/cl)*sqrt(0.139470*0.139470/(p->at(trIndex[j])*p->at(trIndex[j]))+1));*/
			tpix.Fill(x, tc-(len/cl)*sqrt(0.139470*0.139470/(p->at(trIndex[j])*p->at(trIndex[j]))+1));
			tpiy.Fill(y, tc-(len/cl)*sqrt(0.139470*0.139470/(p->at(trIndex[j])*p->at(trIndex[j]))+1));
			tpis12.Fill(tdcS1->at(1)-s120, tc-(len/cl)*sqrt(0.139470*0.139470/(p->at(trIndex[j])*p->at(trIndex[j]))+1)-8);
			tpis13.Fill(tdcS1->at(2)-s130, tc-(len/cl)*sqrt(0.139470*0.139470/(p->at(trIndex[j])*p->at(trIndex[j]))+1)-8);
			tpis14.Fill(tdcS1->at(3)-s140, tc-(len/cl)*sqrt(0.139470*0.139470/(p->at(trIndex[j])*p->at(trIndex[j]))+1)-8);
			tpi.push_back(tc);
			ppi.push_back(p->at(trIndex[j]));
			lpi.push_back(len);
 			ts12.push_back(tdcS1->at(1)-s120);
 			ts13.push_back(tdcS1->at(2)-s130);
 			ts14.push_back(tdcS1->at(3)-s140);
 			/*xb.push_back((x+y)/sqrt(2));
 			yb.push_back((-x+y)/sqrt(2));*/
 			xb.push_back(x);
 			yb.push_back(y);
 			/*ts12.push_back(tdcS1->at(1)-1530);
 			ts13.push_back(tdcS1->at(2)-1530);
 			ts14.push_back(tdcS1->at(3)-1530);*/

						

		} 		
	}
	
	

	double r[3] = {2,2,2};
	
	

	double mm2 = 0;		

	double rtemp1[3] = {0, 0, 0}; //E(p^4*c^4*t^3*ts12/l^4), E(p^4*c^4*t^3*ts13/l^4), E(p^4*c^4*t^3*ts14/l^4)
	double rtemp2[3] = {0, 0, 0}; //E(p^2*c^2*t*ts12/l^2), E(p^2*c^2*t*ts13/l^2), E(p^2*c^2*t*ts14/l^2)

	
	double Jtemp1[6] = {0,0,0,0,0,0}; 
	double Jtemp2[6] = {0,0,0,0,0,0}; 

	
	for(int i = 0; i < 10; i++){
		cout << "Step: " << i+1 << endl;
		mm2 = 0;
		for(int k = 0; k < 6; k++){
			if(k < 3){
				rtemp1[k] = 0;
				rtemp2[k] = 0;

			}
			Jtemp1[k] = 0;
			Jtemp2[k] = 0;

			
		}
		if(i==9) s1corr.Fill();
		for(int j = 0; j < ppi.size(); j++){
			
			double t = tpi.at(j) - 0.25*(r[0]*ts12.at(j)+r[1]*ts13.at(j)+r[2]*ts14.at(j));
			if(i == 9) {
				var2.Fill(TMath::Power(ppi.at(j),2)*(TMath::Power(cl*t/lpi.at(j),2)-1));
				tpis12a.Fill(ts12.at(j), t-(lpi.at(j)/cl)*sqrt(0.139470*0.139470/(ppi.at(j)*ppi.at(j))+1)-8);
				tpis13a.Fill(ts13.at(j), t-(lpi.at(j)/cl)*sqrt(0.139470*0.139470/(ppi.at(j)*ppi.at(j))+1)-8);
				tpis14a.Fill(ts14.at(j), t-(lpi.at(j)/cl)*sqrt(0.139470*0.139470/(ppi.at(j)*ppi.at(j))+1)-8);
				
				tpixpa.Fill(xb.at(j), t-(lpi.at(j)/cl)*sqrt(0.139470*0.139470/(ppi.at(j)*ppi.at(j))+1));
				tpiypa.Fill(yb.at(j), t-(lpi.at(j)/cl)*sqrt(0.139470*0.139470/(ppi.at(j)*ppi.at(j))+1));
			}
			mm2 += TMath::Power(ppi.at(j),2)*(TMath::Power(cl*t/lpi.at(j),2)-1);
			rtemp1[0] += TMath::Power(ppi.at(j),4)*TMath::Power(cl/lpi.at(j),2)*t*ts12.at(j)*(TMath::Power(cl*t/lpi.at(j),2)-1);
			rtemp1[1] += TMath::Power(ppi.at(j),4)*TMath::Power(cl/lpi.at(j),2)*t*ts13.at(j)*(TMath::Power(cl*t/lpi.at(j),2)-1);
			rtemp1[2] += TMath::Power(ppi.at(j),4)*TMath::Power(cl/lpi.at(j),2)*t*ts14.at(j)*(TMath::Power(cl*t/lpi.at(j),2)-1);
			
			rtemp2[0] += TMath::Power(ppi.at(j)*cl/lpi.at(j),2)*t*ts12.at(j);
			rtemp2[1] += TMath::Power(ppi.at(j)*cl/lpi.at(j),2)*t*ts13.at(j);
			rtemp2[2] += TMath::Power(ppi.at(j)*cl/lpi.at(j),2)*t*ts14.at(j);
			
			Jtemp1[0] += TMath::Power(ppi.at(j)*ppi.at(j)*cl/lpi.at(j),2)*(3*TMath::Power(cl*t/lpi.at(j),2)-1)*ts12.at(j)*ts12.at(j);
			Jtemp1[1] += TMath::Power(ppi.at(j)*ppi.at(j)*cl/lpi.at(j),2)*(3*TMath::Power(cl*t/lpi.at(j),2)-1)*ts12.at(j)*ts13.at(j);
			Jtemp1[2] += TMath::Power(ppi.at(j)*ppi.at(j)*cl/lpi.at(j),2)*(3*TMath::Power(cl*t/lpi.at(j),2)-1)*ts12.at(j)*ts14.at(j);
			Jtemp1[3] += TMath::Power(ppi.at(j)*ppi.at(j)*cl/lpi.at(j),2)*(3*TMath::Power(cl*t/lpi.at(j),2)-1)*ts13.at(j)*ts13.at(j);
			Jtemp1[4] += TMath::Power(ppi.at(j)*ppi.at(j)*cl/lpi.at(j),2)*(3*TMath::Power(cl*t/lpi.at(j),2)-1)*ts13.at(j)*ts14.at(j);
			Jtemp1[5] += TMath::Power(ppi.at(j)*ppi.at(j)*cl/lpi.at(j),2)*(3*TMath::Power(cl*t/lpi.at(j),2)-1)*ts14.at(j)*ts14.at(j);
			
			Jtemp2[0] += TMath::Power(ppi.at(j)*cl/lpi.at(j),2)*ts12.at(j)*ts12.at(j);
			Jtemp2[1] += TMath::Power(ppi.at(j)*cl/lpi.at(j),2)*ts12.at(j)*ts13.at(j);
			Jtemp2[2] += TMath::Power(ppi.at(j)*cl/lpi.at(j),2)*ts12.at(j)*ts14.at(j);
			Jtemp2[3] += TMath::Power(ppi.at(j)*cl/lpi.at(j),2)*ts13.at(j)*ts13.at(j);
			Jtemp2[4] += TMath::Power(ppi.at(j)*cl/lpi.at(j),2)*ts13.at(j)*ts14.at(j);
			Jtemp2[5] += TMath::Power(ppi.at(j)*cl/lpi.at(j),2)*ts14.at(j)*ts14.at(j);
			

		}
		mm2 = mm2/nmaxpi;
		for(int k = 0; k < 6; k++){
			if(k < 3){
				rtemp1[k] = rtemp1[k]/nmaxpi;
				rtemp2[k] = rtemp2[k]/nmaxpi;

			}
			Jtemp1[k] = Jtemp1[k]/nmaxpi;
			Jtemp2[k] = Jtemp2[k]/nmaxpi;
			
		}		
		TMatrixD col(3, 1);
		TMatrixD Jac(3,3);
		for(int j = 0; j < 3; j++){
			col[j][0] = -rtemp1[j] + mm2*rtemp2[j];
		}
		Jac[0][0] = 0.25*Jtemp1[0] - 0.5*rtemp2[0]*rtemp2[0] - 0.25*mm2*Jtemp2[0];
		Jac[0][1] = 0.25*Jtemp1[1] - 0.5*rtemp2[0]*rtemp2[1] - 0.25*mm2*Jtemp2[1];
		Jac[0][2] = 0.25*Jtemp1[2] - 0.5*rtemp2[0]*rtemp2[2] - 0.25*mm2*Jtemp2[2];
		Jac[1][0] = Jac[0][1];
		Jac[1][1] = 0.25*Jtemp1[3] - 0.5*rtemp2[1]*rtemp2[1] - 0.25*mm2*Jtemp2[3];
		Jac[1][2] = 0.25*Jtemp1[4] - 0.5*rtemp2[1]*rtemp2[2] - 0.25*mm2*Jtemp2[4];
		Jac[2][0] = Jac[0][2];
		Jac[2][1] = Jac[1][2];
		Jac[2][2] = 0.25*Jtemp1[5] - 0.5*rtemp2[2]*rtemp2[2] - 0.25*mm2*Jtemp2[5];
		
		TMatrixD JacInv = Jac.Invert();
		TMatrixD dx = JacInv*col;
		
		
		
		r[0] = r[0] - dx[0][0];
		r[1] = r[1] - dx[1][0];
		r[2] = r[2] - dx[2][0];
		cout << r[0] << " " << r[1] << " " << r[2] << endl;
		cout << "*******************************************" << endl;
	}
	TFile out("piVar.root", "RECREATE");
	out.cd();
	var1.Write();
	var2.Write();
	tpix.Write();
	tpiy.Write();
	tpixpa.Write();
	tpiypa.Write();
	tpis12.Write();
	tpis13.Write();
	tpis14.Write();
	tpis12a.Write();
	tpis13a.Write();
	tpis14a.Write();
	out.Close();
}
