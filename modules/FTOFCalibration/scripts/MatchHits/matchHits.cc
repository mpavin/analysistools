#include <stdlib.h>
#include <iostream>
#include <vector>
#include <string>
#include <ChainDef.h>
#include <Functions.h>
#include <TOFMapping.h>
#include <TChain.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TFile.h>
#include <sstream>
#include <math.h>
#include <T0Def.h>
#include <CDef.h>
#include <TMath.h>
#include <dEdxResolution.h>
using namespace std;

int main(){

	string fileName = "../../input/list2010.txt";
	string scintPos = "scintPos.dat";
	string t0file = "t0Values2010.root";
	string cfile = "cValues.root";
	string dEdxName = "dEdxParData.root";
	
	int nm2TOF = 200;
	double m2TOFMin = -0.9;
	double m2TOFMax = 1.8;	
	
	int ntof = 200;
	double tofMin = 45000;
	double tofMax = 49000;
	int np = 200;
	double pMin = -10.;
	double pMax = 10;	

	double dEdxMin = 0;
	double dEdxMax = 1.20;
	double zLastCut = 650;

	double zcutMin = -657.41;
	double zcutMax = -567.40;
	
	vector<double> ranges;
	for(int i = 0; i < 8; i++){
		ranges.push_back(0.3);
	}
	dEdxResolution dEdxRes(dEdxName, ranges);
	
	TChain chaint0("T0");
	ReadT0(chaint0, t0file);

	TChain chainc("cScint");
	ReadC(chainc, cfile);
	
	TChain chain("FTOF");
	ReadData(chain, fileName);

	vector<string> sPos;
	vector<double> xPos;
	ReadInputList(scintPos, sPos);
	for(unsigned int i = 0; i < sPos.size(); i++){
		
		stringstream convert(sPos.at(i));
		double res;
		convert >> res;
		xPos.push_back(res);
	}
	
	
	vector<TH2D> m2TOFpHisto;

	TH2D m2TOFpAll("m2TOFAll", "", np, 0, 6, nm2TOF, -0.5, 2);
	//vector<TH2D> tof2yHisto;
	//vector<TH2D> tof12yHisto;
	TH1I tofeff("tofeff", "", 2, 0.5, 2.5);
	TH1I nHits("nHits", "", 5, 0.5, 5.5);
	TH1I topHisto("topHisto", "", 15, 0.5, 15.5);
	TH2D tofdiff("tofdiff", "", 200, -60, 60, 200, -10000, 10000);
	TH1D tdcHistoAc("tdcHistoAc", "", 400, 0, 4400);
	TH1D qdcHistoAc("qdcHistoAc", "", 400, 0, 4400);
	TH2D qdctdcHistoAc("qdctdcHistoAc", "", 400, 0, 4400, 4400, 0, 4400);
	TH1D zVertexPos("zVertexPos", "", 200, zcutMin, zcutMax+650);
	TH2D runCor("runCor", "", 600, 10521, 11121, 200, -200, 200);
	TH1D s1corr("s1corr", "", 200, -1500, 1500);
	vector<TH1D> m2vsppi;
	vector<TH1D> m2vspp;
	vector<TH1D> m2vspe;
	vector<TH1D> m2vspK;
	
	for(unsigned int i = 0; i < 47; i++){
		string name1 = "m2vsppi" + ConvertToString(i+1., 0);
		string name2 = "m2vspp" + ConvertToString(i+1., 0);
		string name3 = "m2vspe" + ConvertToString(i+1., 0);
		string name4 = "m2vspK" + ConvertToString(i+1., 0);	
		if(0.30 + i*0.2 <= 1.5){

			TH1D temp4(name4.c_str(), "", 100, 0.14, 0.36);

			m2vspK.push_back(temp4);
			//cout << i << endl;
		}
		else if(0.30 + i*0.2 > 1.5 && 0.30 + i*0.2 <= 2.7){

			TH1D temp4(name4.c_str(), "", 100, 0.05, 0.45);

			m2vspK.push_back(temp4);
			//cout << i << endl;				
		}
				
		if(0.30 + i*0.1 <= 1.5){
			TH1D temp1(name1.c_str(), "", 100, -0.04, 0.08);
			TH1D temp3(name3.c_str(), "", 100, -0.06, 0.06);
			//TH1D temp4(name4.c_str(), "", 100, 0.14, 0.36);
			
			m2vsppi.push_back(temp1);
			m2vspe.push_back(temp3);
			//m2vspK.push_back(temp4);
		}
		else if(0.30 + i*0.1 > 1.5 && 0.30 + i*0.1 <= 2.7){
			TH1D temp1(name1.c_str(), "", 100, -0.2, 0.2);
			TH1D temp3(name3.c_str(), "", 100, -0.2, 0.2);
			//TH1D temp4(name4.c_str(), "", 100, 0.05, 0.45);
			
			m2vsppi.push_back(temp1);
			m2vspe.push_back(temp3);
			//m2vspK.push_back(temp4);				
		}
		else{
			TH1D temp1(name1.c_str(), "", 100, -0.4, 0.4);
			TH1D temp3(name3.c_str(), "", 100, -0.4, 0.4);
			//TH1D temp4(name4.c_str(), "", 100, 0.0, 0.50);
			
			m2vsppi.push_back(temp1);
			m2vspe.push_back(temp3);
			//m2vspK.push_back(temp4);		
		}
		
		

		TH1D temp2(name2.c_str(), "", 100, 0.3, 1.4);

		
		
		m2vspp.push_back(temp2);

	}
	for(unsigned int i = 0; i < nScint; i++){
		string name1 = "m2TOFp" + ConvertToString(i+1., 0);


		TH2D temp1(name1.c_str(), "", np, pMin, pMax, nm2TOF, m2TOFMin, m2TOFMax);

		//TH2D temp3(name3.c_str(), "", ny, yMin, yMax, nTOF, tofMin, tofMax);

		m2TOFpHisto.push_back(temp1);
		//tof12yHisto.push_back(temp3);

	}
	
	unsigned int nEntries = chain.GetEntries();
	cout << "Number of entries: " << nEntries << endl;
	
	for(unsigned int i = 0; i < nEntries; i++){
		if(!((i+1)%100000)) cout << "Processing entry: " << i+1 << endl;
		chain.GetEntry(i);
		/*if(runNumber > 10864 && runNumber < 10874) continue;
		if(runNumber > 10939 && runNumber < 10945) continue; 	
		if(runNumber > 11089) continue; */	

		if(trigger < 1) continue;
 		
		/*if(runNumber > 10864 && runNumber < 10874) continue;
		if(runNumber > 10939 && runNumber < 10945) continue; 	
		if(runNumber > 11089) continue;  */
		
		double s1cor = 0;
		if(tdcS1->at(1)<3000 && tdcS1->at(2)<3000 && tdcS1->at(3)<3000){
			s1cor = 0.25*aS1*(s13k*(tdcS1->at(2)-s130)+s12k*(tdcS1->at(1)-s120)+s14k*(tdcS1->at(3)-s140));
			
		}
		else if(tdcS1->at(2)>=3000 && tdcS1->at(1)<3000 && tdcS1->at(3)<3000){
			s1cor = aS1*(s12k*(tdcS1->at(1)-s120)+s14k*(tdcS1->at(3)-s140))/3.;
		}
		else if(tdcS1->at(2)>=3000 && (tdcS1->at(1)>=3000 || tdcS1->at(3)>=3000)){
			s1cor = 0.5*aS1*(s13k*(tdcS1->at(2)-s130));
		}
		
		s1cor += -12;
		//s1cor = 0;
 		vector<int> active;
		vector<double> tof1;
		vector<double> tof2;
		
		
		//****************************************************************************************************************
		// Cheching for active scintillators
		//double fact[2] = {-1, 1};
		runCor.Fill(runNumber, s1cor);
		for(unsigned int j = 0; j < nScint; j++){
 			
 			chaint0.GetEntry(j);
			
			
			int both[2] = {0,0};
			double tof[2] = {0, 0};
 			for(unsigned int k = 0; k < 2; k++){
 			 
 			 	if(maskPMT[j][k]==0){
 			 		both[k] = 1;
 			 		continue;
 			 	}
 			 	
 				if(tdc->at(pmtId[j][k]) < 1) continue;
 				if(tdc->at(pmtId[j][k]) >= tdcChan+93) continue;
 				
			
 				tof[k] = tdc->at(pmtId[j][k])*channelSize - t0[k] - s1cor;
 				both[k] = 1;
 							
 			}
 			
 			if(both[0]&&both[1]){
				
 				active.push_back(j);
 				tof1.push_back(tof[0]);
 				tof2.push_back(tof[1]);
 			}			
		}	
 		
 		
		
		//****************************************************************************************************************
		// Matching tracks and scintillators	
		vector<int> trIndex;
		vector<int> nTr;
		//cout << "radi1" << endl;
		for(int j = 0; j < active.size(); j++){
			trIndex.push_back(-999);
			nTr.push_back(0);
		}
		for(unsigned int j = 0; j < xStart->size(); j++){
			if(nTOF->at(j) > 0) tofeff.Fill(1);
			for(int k = 0; k < active.size(); k++){

				
 				if(!(active.at(k)%2)){
 				//if((j%2)){
 					if(abs(xTOF2->at(j)-xPos.at(active.at(k))) > swidth/2.) continue;
 					if(abs(yTOF2->at(j)) > slength/2.) continue;

 				}
 				else{
 					if(abs(xTOF1->at(j)-xPos.at(active.at(k))) > swidth/2.) continue;
 					if(abs(yTOF1->at(j)) > slength/2.) continue;
 				}
				nTr[k]++;
				trIndex.at(k) = j;
				
			}
		
		} 		
 		
		//****************************************************************************************************************
		// Calculating m2TOF 	
		for(int j = 0; j < active.size(); j++){
			
			chainc.GetEntry(active.at(j));
			//if(active.at(j) == 26) cout << c[0] << " " << c[1] << endl;
			nHits.Fill(nTr.at(j));
			//if(nTr.at(j) > 0) tofeff.Fill(2, nTr.at(j));
			//if(nTr.at(j) > 0) tofeff.Fill(2);	
			if(nTr.at(j) != 1) continue;
			tofeff.Fill(2);
			//if(RST->at(trIndex[j])!=0) continue;

			if(zStart->at(trIndex[j])<zcutMin || zStart->at(trIndex[j]) >= zcutMax) continue;
 			//if(zVert->at(k)<zcutMin || zVert->at(k) >= zcutMax) continue;


 			//if(zTPCLast->at(k) < zLastCut) continue;
			if(topology->at(trIndex[j]) == 9){
				if(nVTPC->at(trIndex[j]) < 20) continue;
				if(nMTPC->at(trIndex[j]) < 30) continue;
			}
			else if(topology->at(trIndex[j]) == 10) continue;
			else if(topology->at(trIndex[j]) == 11){
				if(nVTPC->at(trIndex[j]) < 20) continue;
				if(nMTPC->at(trIndex[j]) < 30) continue;
			}
			else if(topology->at(trIndex[j]) == 12){
				if(nGTPC->at(trIndex[j]) < 6) continue;
				if(nMTPC->at(trIndex[j]) < 70) continue;
			}	
			else if(topology->at(trIndex[j]) == 13) continue;

			else if(topology->at(trIndex[j]) == 14){
				if(nVTPC->at(trIndex[j]) < 20) continue;
				if(nMTPC->at(trIndex[j]) < 30) continue;
			}	
			else if(topology->at(trIndex[j]) == 15){
				if(nVTPC->at(trIndex[j]) < 20) continue;
				if(nMTPC->at(trIndex[j]) < 30) continue;
			}	
			if(distToTarg->at(trIndex[j]) > sqrt(TMath::Power(xStart->at(trIndex[j])*xEStart->at(trIndex[j]),2)+TMath::Power(xStart->at(trIndex[j])*xEStart->at(trIndex[j]),2))/sqrt(xStart->at(trIndex[j])*xStart->at(trIndex[j])+yStart->at(trIndex[j])*yStart->at(trIndex[j])))	
				continue;	

			if(RST->at(trIndex[j]) == 1){
				if(theta->at(trIndex[j]) > 0.18){
					if((phi->at(trIndex[j])<-10 || phi->at(trIndex[j])>10) && (phi->at(trIndex[j])<170 || phi->at(trIndex[j])>190)) continue;
				}
				else if(theta->at(trIndex[j]) > 0.13 && theta->at(trIndex[j]) < 0.18){
					if((phi->at(trIndex[j])<-15 || phi->at(trIndex[j])>15) && (phi->at(trIndex[j])<165 || phi->at(trIndex[j])>195)) continue;
				}
				else if(theta->at(trIndex[j]) > 0.11 && theta->at(trIndex[j]) < 0.13){
					if((phi->at(trIndex[j])<-20 || phi->at(trIndex[j])>20) && (phi->at(trIndex[j])<160 || phi->at(trIndex[j])>200)) continue;
				}
				else if(theta->at(trIndex[j]) > 0.09 && theta->at(trIndex[j]) < 0.11){
					if((phi->at(trIndex[j])<-25 || phi->at(trIndex[j])>25) && (phi->at(trIndex[j])<155 || phi->at(trIndex[j])>205)) continue;
				}
				else if(theta->at(trIndex[j]) > 0.07 && theta->at(trIndex[j]) < 0.09){
					if((phi->at(trIndex[j])<-35 || phi->at(trIndex[j])>35) && (phi->at(trIndex[j])<145 || phi->at(trIndex[j])>215)) continue;
				}
				else if(theta->at(trIndex[j]) > 0.06 && theta->at(trIndex[j]) < 0.07){
					if((phi->at(trIndex[j])<-45 || phi->at(trIndex[j])>40) && (phi->at(trIndex[j])<140 || phi->at(trIndex[j])>225)) continue;
				}
				else if(theta->at(trIndex[j]) > 0.05 && theta->at(trIndex[j]) < 0.06){
					if((phi->at(trIndex[j])<-55 || phi->at(trIndex[j])>50) && (phi->at(trIndex[j])<130 || phi->at(trIndex[j])>235)) continue;
				}
				else if(theta->at(trIndex[j]) > 0.04 && theta->at(trIndex[j]) < 0.05){
					if((phi->at(trIndex[j])<-70|| phi->at(trIndex[j])>65) && (phi->at(trIndex[j])<115 || phi->at(trIndex[j])>250)) continue;
				}
				else if(theta->at(trIndex[j]) > 0.03 && theta->at(trIndex[j]) < 0.04){
					if((phi->at(trIndex[j])<-65|| phi->at(trIndex[j])>60) && (phi->at(trIndex[j])<110 || phi->at(trIndex[j])>250)) continue;
				}
				else if(theta->at(trIndex[j]) > 0.01 && theta->at(trIndex[j]) < 0.03){
					if((phi->at(trIndex[j])<-70|| phi->at(trIndex[j])>70) && (phi->at(trIndex[j])<110 || phi->at(trIndex[j])>250)) continue;
				}
				else if(theta->at(trIndex[j]) > 0.00 && theta->at(trIndex[j]) < 0.01){
					if((phi->at(trIndex[j])<-70|| phi->at(trIndex[j])>70) && (phi->at(trIndex[j])<110 || phi->at(trIndex[j])>250)) continue;
				}
			}
			else{
				if(theta->at(trIndex[j]) > 0.20){
					if((phi->at(trIndex[j])<-15 || phi->at(trIndex[j])>13) && (phi->at(trIndex[j])<167 || phi->at(trIndex[j])>195)) continue;
				}
				else if(theta->at(trIndex[j]) > 0.14 && theta->at(trIndex[j]) < 0.20){
					if((phi->at(trIndex[j])<-18|| phi->at(trIndex[j])>16) && (phi->at(trIndex[j])<164 || phi->at(trIndex[j])>198)) continue;
				}
				else if(theta->at(trIndex[j]) > 0.12 && theta->at(trIndex[j]) < 0.14){
					if((phi->at(trIndex[j])<-22|| phi->at(trIndex[j])>20) && (phi->at(trIndex[j])<160 || phi->at(trIndex[j])>202)) continue;
				}
				else if(theta->at(trIndex[j]) > 0.10 && theta->at(trIndex[j]) < 0.12){
					if((phi->at(trIndex[j])<-25|| phi->at(trIndex[j])>23) && (phi->at(trIndex[j])<157 || phi->at(trIndex[j])>205)) continue;
				}
				else if(theta->at(trIndex[j]) > 0.09 && theta->at(trIndex[j]) < 0.10){
					if((phi->at(trIndex[j])<-28|| phi->at(trIndex[j])>26) && (phi->at(trIndex[j])<154 || phi->at(trIndex[j])>208)) continue;
				}
				else if(theta->at(trIndex[j]) > 0.08 && theta->at(trIndex[j]) < 0.09){
					if((phi->at(trIndex[j])<-32|| phi->at(trIndex[j])>30) && (phi->at(trIndex[j])<150 || phi->at(trIndex[j])>212)) continue;
				}
				else if(theta->at(trIndex[j]) > 0.07 && theta->at(trIndex[j]) < 0.08){
					if((phi->at(trIndex[j])<-35|| phi->at(trIndex[j])>32) && (phi->at(trIndex[j])<148 || phi->at(trIndex[j])>215)) continue;
				}
				else if(theta->at(trIndex[j]) > 0.06 && theta->at(trIndex[j]) < 0.07){
					if((phi->at(trIndex[j])<-44|| phi->at(trIndex[j])>40) && (phi->at(trIndex[j])<140 || phi->at(trIndex[j])>224)) continue;
				}
				else if(theta->at(trIndex[j]) > 0.05 && theta->at(trIndex[j]) < 0.06){
					if((phi->at(trIndex[j])<-48|| phi->at(trIndex[j])>44) && (phi->at(trIndex[j])<136 || phi->at(trIndex[j])>228)) continue;
				}
				else if(theta->at(trIndex[j]) > 0.04 && theta->at(trIndex[j]) < 0.05){
					if((phi->at(trIndex[j])<-53|| phi->at(trIndex[j])>53) && (phi->at(trIndex[j])<140 || phi->at(trIndex[j])>220)) continue;
				}
				else if(theta->at(trIndex[j]) > 0.02 && theta->at(trIndex[j]) < 0.04){
					if((phi->at(trIndex[j])<-40|| phi->at(trIndex[j])>40) && (phi->at(trIndex[j])<150 || phi->at(trIndex[j])>210)) continue;
				}
				else if(theta->at(trIndex[j]) > 0.00 && theta->at(trIndex[j]) < 0.02){
					if((phi->at(trIndex[j])<-50|| phi->at(trIndex[j])>50) && (phi->at(trIndex[j])<130 || phi->at(trIndex[j])>230)) continue;
				}
			}
 								
 			//if(theta->at(trIndex[j])>0.380) continue;
 			//zVertexPos.Fill(zVert->at(trIndex[j]));
 			double len = length->at(trIndex[j]);
			double yVal = yTOF1->at(trIndex[j]);

 		
 			if(!(active.at(j)%2)){
 					//if((j%2)){
				yVal = yTOF2->at(trIndex[j]);
 				len+=sqrt(pow(xTOF2->at(trIndex[j])-xTOF1->at(trIndex[j]), 2)+pow(yTOF2->at(trIndex[j])-yTOF1->at(trIndex[j]), 2)+pow(zTOF2->at(trIndex[j])-zTOF1->at(trIndex[j]), 2));
 			}

 			
			
			//if(len > 0.000386*len + 1395) continue;
			double tc = 0;
			double count = 0;
 			if(maskPMT[active.at(j)][0]==1){
 						
 				tdcHistoAc.Fill(tdc->at(pmtId[active.at(j)][0]));
 				qdcHistoAc.Fill(qdc->at(pmtId[active.at(j)][0]));
 				qdctdcHistoAc.Fill(qdc->at(pmtId[active.at(j)][0]), tdc->at(pmtId[active.at(j)][0]));
 				//tof1.at(j) = tof1.at(j) - (2.12/0.0299792)*(60-yVal);
 				//tof1.at(j) = tof1.at(j) - TMath::Abs(c[0])*(60-yVal);
 				//tof1.at(j) = tof1.at(j) - 69.76*(60-yVal);
 				tof1.at(j) = tof1.at(j) - 70.25*(60-yVal);
 				//tof1.at(j) = tof1.at(j) - 70.25*yVal;
				tc += tof1.at(j);
				count +=1.;
 			}
 			if(maskPMT[active.at(j)][1]==1){
 				tdcHistoAc.Fill(tdc->at(pmtId[active.at(j)][1]));
 				qdcHistoAc.Fill(qdc->at(pmtId[active.at(j)][1]));
 				//qdctdcHistoAc.Fill(qdc->at(pmtId[active.at(j)][1]), tdc->at(pmtId[active.at(j)][1]));
 				//tof2.at(j) = tof2.at(j) - (2.12/0.0299792)*(60+yVal);
 				//tof2.at(j) = tof2.at(j) - TMath::Abs(c[1])*(60+yVal);
 				//tof2.at(j) = tof2.at(j) - 71.23*(60+yVal);
 				tof2.at(j) = tof2.at(j) - 70.25*(60+yVal);
 				//tof2.at(j) = tof2.at(j) + 70.25*yVal;
 				tc += tof2.at(j);	
 				count +=1.;						
 			}
 			tc =tc/count ;

			if(maskPMT[active.at(j)][0]==1&&maskPMT[active.at(j)][1]==1){
				tofdiff.Fill(yVal, tof1.at(j) -tof2.at(j));
				if(abs(tof1.at(j) -tof2.at(j))>2000) continue;
				
			}					
 		
			double mc2 = p->at(trIndex[j])*p->at(trIndex[j])*(pow(0.029979*tc,2)/pow(len,2)-1);
			//if(mc2 < 0.3 || mc2 > 0.75) continue;
			//if(p->at(trIndex[j]) > 3) continue;
			//if(topology->at(trIndex[j]) == 10) continue;
			int sl = (p->at(trIndex[j]) - 0.3)*10.;
			int slk = (p->at(trIndex[j]) - 0.3)*5.;
			//cout << (p->at(trIndex[j]) - 0.3)*10 << " " << sl << endl;

			if(sl < 47 && sl > 0){
						
				if(mc2 > 0.5){
					m2vspp.at(sl).Fill(mc2);
				}
				else if(mc2 > -0.15 && mc2 < 0.15 && p->at(trIndex[j]) < 2){
					//cout << "radi1" << endl; 
					if(TMath::Abs(dEdxRes.GetMean(p->at(trIndex[j])/0.139470)-dEdx->at(trIndex[j])) < 2.0*dEdxRes.GetWidth(p->at(trIndex[j])/0.139470)){
						m2vsppi.at(sl).Fill(mc2);
					}
					else if(TMath::Abs(dEdxRes.GetMean(p->at(trIndex[j])/0.0005)-dEdx->at(trIndex[j])) < 2.0*dEdxRes.GetWidth(p->at(trIndex[j])/0.0005)){
						m2vspe.at(sl).Fill(mc2);
					}	
					//cout << "radi2" << endl; 				
				}
				else if(mc2 > -0.45 && mc2 < 0.45 && p->at(trIndex[j]) > 2){
					if(TMath::Abs(dEdxRes.GetMean(p->at(trIndex[j])/0.139470)-dEdx->at(trIndex[j])) < 2.0*dEdxRes.GetWidth(p->at(trIndex[j])/0.139470)){
						m2vsppi.at(sl).Fill(mc2);
					}
					else if(TMath::Abs(dEdxRes.GetMean(p->at(trIndex[j])/0.0005)-dEdx->at(trIndex[j])) < 2.0*dEdxRes.GetWidth(p->at(trIndex[j])/0.0005)){
						m2vspe.at(sl).Fill(mc2);
					}					
				}

					
					//m2vspp.at(sl).Fill(mc2);
				//}
			}
			
			if(slk < 13){
				if(mc2 > 0.15 && mc2 < 0.35 && p->at(trIndex[j]) < 2.7){
					if(TMath::Abs(dEdxRes.GetMean(p->at(trIndex[j])/0.49367)-dEdx->at(trIndex[j])) < 2.0*dEdxRes.GetWidth(p->at(trIndex[j])/0.49367)){
						m2vspK.at(slk).Fill(mc2);
					}				
				}			
			}
			zVertexPos.Fill(zStart->at(trIndex[j]));
			topHisto.Fill(topology->at(trIndex[j]));
 			m2TOFpHisto.at(active.at(j)).Fill(qD->at(trIndex[j])*p->at(trIndex[j]), mc2);
			m2TOFpAll.Fill(p->at(trIndex[j]), mc2);
			//m2TOFpHisto.at(active.at(j)).Fill(p->at(trIndex[j]), mc2);
			//m2TOFpAll.Fill(p->at(trIndex[j]), mc2);
						

		} 		
	}
	
	TFile out("m2TOFHistos.root", "RECREATE");
	out.cd();
	nHits.Write();
	tofeff.Write();
	topHisto.Write();
	tdcHistoAc.Write();
	qdcHistoAc.Write();
	qdctdcHistoAc.Write();
	tofdiff.Write();
	m2TOFpAll.Write();
	zVertexPos.Write();
	runCor.Write();
	for(int i = 0; i < m2vsppi.size(); i++){
		m2vsppi.at(i).Write();
		m2vspp.at(i).Write();
		if(i<13)
			m2vspK.at(i).Write();
		m2vspe.at(i).Write();
	}
	for(int i = 0; i < m2TOFpHisto.size(); i++){
		m2TOFpHisto.at(i).Write();
		//tof12yHisto.at(i).Write();
	}
	out.Close();
}
