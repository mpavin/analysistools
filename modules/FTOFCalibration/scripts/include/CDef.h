#ifndef CDEF_H
#define CDEF_H

#include <TTree.h>
#include <TChain.h>

//int scId;
double c[2] = {0,0};
double ty[2] = {0,0};

void ReadC(TChain &chain, string fileName){
		
	chain.Add(fileName.c_str());

	
 	//chain.SetBranchAddress("scintId", &scId);
 	chain.SetBranchAddress("c1", &c[0]);
 	chain.SetBranchAddress("c2", &c[1]);
 	chain.SetBranchAddress("ty1", &ty[0]);
 	chain.SetBranchAddress("ty2", &ty[1]);
 
}

#endif
