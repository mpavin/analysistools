#ifndef T0Def
#define T0Def

#include <TTree.h>
#include <TChain.h>

int scId;
double t0[2] = {0,0};

void ReadT0(TChain &chain, string fileName){
		
	chain.Add(fileName.c_str());

	
 	chain.SetBranchAddress("scintId", &scId);
 	chain.SetBranchAddress("t01", &t0[0]);
 	chain.SetBranchAddress("t02", &t0[1]);
 
}

#endif
