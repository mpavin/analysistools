#include <stdlib.h>
#include <iostream>
#include <vector>
#include <string>
#include <ChainDef.h>
#include <Functions.h>
#include <TOFMapping.h>
#include <TChain.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TFile.h>
#include <sstream>
#include <math.h>
#include <T0Def.h>
#include <TMath.h>
using namespace std;

int main(){

	string fileName = "../../input/list2010v2.txt";
	string scintPos = "scintPos.dat";
	string t0file = "t0Values.root";
	
	int nTOF = 200;
	double tofMin = 42000;
	double tofMax = 59000;	
	int ny = 200;
	double yMin = -60;
	double yMax = 60;	

	double dEdxMin = 0;
	double dEdxMax = 1.20;
	double zLastCut = 650;

	double zcutMin = -657.4;
	double zcutMax = -567.4;
	

	TChain chaint0("T0");
	ReadT0(chaint0, t0file);

	TChain chain("FTOF");
	ReadData(chain, fileName);

	vector<string> sPos;
	vector<double> xPos;
	ReadInputList(scintPos, sPos);
	for(unsigned int i = 0; i < sPos.size(); i++){
		
		stringstream convert(sPos.at(i));
		double res;
		convert >> res;
		xPos.push_back(res);
	}
	
	
	vector<TH2D> tof1yHisto;
	vector<TH2D> tof2yHisto;
	vector<TH2D> tof12yHisto;
	TH2D tofdiff("tofdiff", "", nScint+1, 0, nScint+1, 200, -15000, 15000);

	for(unsigned int i = 0; i < nScint; i++){
		string name1 = "tof1y" + ConvertToString(i+1., 0);
		string name2 = "tof2y" + ConvertToString(i+1., 0);
		string name3 = "tof12y" + ConvertToString(i+1., 0);

		TH2D temp1(name1.c_str(), "", ny, yMin, yMax, nTOF, tofMin, tofMax);
		TH2D temp2(name2.c_str(), "", ny, yMin, yMax, nTOF, tofMin, tofMax);
		TH2D temp3(name3.c_str(), "", ny, yMin, yMax, nTOF, tofMin, tofMax);

		tof1yHisto.push_back(temp1);
		tof2yHisto.push_back(temp2);
		tof12yHisto.push_back(temp3);

	}
	
	unsigned int nEntries = chain.GetEntries();
	cout << "Number of entries: " << nEntries << endl;
	
	for(unsigned int i = 0; i < nEntries; i++){
		if(!((i+1)%100000)) cout << "Processing entry: " << i+1 << endl;
		chain.GetEntry(i);
		
		if (trigger < 1) continue;
		
		
		vector<int> active;
		vector<double> tof1;
		vector<double> tof2;
		
		//****************************************************************************************************************
		// Cheching for active scintillators
		//double fact[2] = {-1, 1};
		for(unsigned int j = 0; j < nScint; j++){
 			
 			chaint0.GetEntry(j);

			int both[2] = {0,0};
			double tof[2] = {0, 0};
 			for(unsigned int k = 0; k < 2; k++){
 			 

 			 	if(maskPMT[j][k]==0){
 			 		both[k] = 1;
 			 		continue;
 			 	}
 			 	
 				if(tdc->at(pmtId[j][k]) < 1) continue;
 				if(tdc->at(pmtId[j][k]) >= tdcChan) continue;
 				
			
 				tof[k] = tdc->at(pmtId[j][k])*channelSize - t0[k];
 				both[k] = 1;
 							
 			}
 			
 			if(both[0]&&both[1]){

 				active.push_back(j);
 				tof1.push_back(tof[0]);
 				tof2.push_back(tof[1]);
 			}			
		}
		
		
		//****************************************************************************************************************
		// Matching tracks and scintillators	
		vector<int> trIndex;
		vector<int> nTr;
		//cout << "radi1" << endl;
		for(int j = 0; j < active.size(); j++){
			trIndex.push_back(-999);
			nTr.push_back(0);
		}
		for(unsigned int j = 0; j < xStart->size(); j++){
			for(int k = 0; k < active.size(); k++){

 				if(!(active.at(k)%2)){
 				//if((j%2)){
 					if(abs(xTOF2->at(j)-xPos.at(active.at(k))) > swidth/2.) continue;
 					if(abs(yTOF2->at(j)) > slength/2.) continue;

 				}
 				else{
 					if(abs(xTOF1->at(j)-xPos.at(active.at(k))) > swidth/2.) continue;
 					if(abs(yTOF1->at(j)) > slength/2.) continue;
 				}
				nTr[k]++;
				trIndex.at(k) = j;
			}
		
		}		


		//****************************************************************************************************************
		// Calculating t0 	
		for(int j = 0; j < active.size(); j++){
			if(nTr.at(j) != 1) continue;
			
			if(zStart->at(trIndex[j])<zcutMin || zStart->at(trIndex[j]) >= zcutMax) continue;
 			//if(zVert->at(k)<zcutMin || zVert->at(k) >= zcutMax) continue;


 			//if(zTPCLast->at(k) < zLastCut) continue;
			if(topology->at(trIndex[j]) == 9){
				if(nVTPC->at(trIndex[j]) < 20) continue;
				if(nMTPC->at(trIndex[j]) < 30) continue;
			}
			else if(topology->at(trIndex[j]) == 10) continue;
			else if(topology->at(trIndex[j]) == 11){
				if(nVTPC->at(trIndex[j]) < 20) continue;
				if(nMTPC->at(trIndex[j]) < 30) continue;
			}
			else if(topology->at(trIndex[j]) == 12){
				if(nGTPC->at(trIndex[j]) < 6) continue;
				if(nMTPC->at(trIndex[j]) < 70) continue;
			}	
			else if(topology->at(trIndex[j]) == 13) continue;

			else if(topology->at(trIndex[j]) == 14){
				if(nVTPC->at(trIndex[j]) < 20) continue;
				if(nMTPC->at(trIndex[j]) < 30) continue;
			}	
			else if(topology->at(trIndex[j]) == 15){
				if(nVTPC->at(trIndex[j]) < 20) continue;
				if(nMTPC->at(trIndex[j]) < 30) continue;
			}	
			if(distToTarg->at(trIndex[j]) > sqrt(TMath::Power(xStart->at(trIndex[j])*xEStart->at(trIndex[j]),2)+TMath::Power(xStart->at(trIndex[j])*xEStart->at(trIndex[j]),2))/sqrt(xStart->at(trIndex[j])*xStart->at(trIndex[j])+yStart->at(trIndex[j])*yStart->at(trIndex[j])))	
				continue;	
				
			if(RST->at(trIndex[j]) == 1){
				if(theta->at(trIndex[j])>0&&theta->at(trIndex[j])<0.02){
					if((phi->at(trIndex[j])<-50||phi->at(trIndex[j])>50)&&(phi->at(trIndex[j])<130||phi->at(trIndex[j])>230)) continue;
				}
				if(theta->at(trIndex[j])>0.02&&theta->at(trIndex[j])<0.04){
					if((phi->at(trIndex[j])<-60||phi->at(trIndex[j])>60)&&(phi->at(trIndex[j])<155||phi->at(trIndex[j])>205)) continue;
				}
				if(theta->at(trIndex[j])>0.04&&theta->at(trIndex[j])<0.06){
					if((phi->at(trIndex[j])<-40||phi->at(trIndex[j])>40)&&(phi->at(trIndex[j])<145||phi->at(trIndex[j])>215)) continue;
				}
				if(theta->at(trIndex[j])>0.08&&theta->at(trIndex[j])<0.09){
					if((phi->at(trIndex[j])<-27||phi->at(trIndex[j])>27)&&(phi->at(trIndex[j])<153||phi->at(trIndex[j])>207)) continue;
				}
				if(theta->at(trIndex[j])>0.06&&theta->at(trIndex[j])<0.08){
					if((phi->at(trIndex[j])<-30||phi->at(trIndex[j])>30)&&(phi->at(trIndex[j])<150||phi->at(trIndex[j])>210)) continue;
				}
				if(theta->at(trIndex[j])>0.09&&theta->at(trIndex[j])<0.1){
					if((phi->at(trIndex[j])<-25||phi->at(trIndex[j])>25)&&(phi->at(trIndex[j])<155||phi->at(trIndex[j])>205)) continue;
				}
				if(theta->at(trIndex[j])>0.1&&theta->at(trIndex[j])<0.14){
					if((phi->at(trIndex[j])<-15||phi->at(trIndex[j])>15)&&(phi->at(trIndex[j])<165||phi->at(trIndex[j])>195)) continue;
				}
				if(theta->at(trIndex[j])>0.14){
					if((phi->at(trIndex[j])<-10||phi->at(trIndex[j])>10)&&(phi->at(trIndex[j])<170||phi->at(trIndex[j])>190)) continue;
				}
			}
			else{
				if(theta->at(trIndex[j])>0&&theta->at(trIndex[j])<0.02){
					if((phi->at(trIndex[j])<-60||phi->at(trIndex[j])>60)&&(phi->at(trIndex[j])<120||phi->at(trIndex[j])>240)) continue;
				}
				if(theta->at(trIndex[j])>0.02&&theta->at(trIndex[j])<0.04){
					if((phi->at(trIndex[j])<-60||phi->at(trIndex[j])>60)&&(phi->at(trIndex[j])<120||phi->at(trIndex[j])>240)) continue;
				}
				if(theta->at(trIndex[j])>0.04&&theta->at(trIndex[j])<0.06){
					if((phi->at(trIndex[j])<-40||phi->at(trIndex[j])>40)&&(phi->at(trIndex[j])<145||phi->at(trIndex[j])>215)) continue;
				}
				if(theta->at(trIndex[j])>0.08&&theta->at(trIndex[j])<0.09){
					if((phi->at(trIndex[j])<-30||phi->at(trIndex[j])>27)&&(phi->at(trIndex[j])<150||phi->at(trIndex[j])>207)) continue;
				}
				if(theta->at(trIndex[j])>0.06&&theta->at(trIndex[j])<0.08){
					if((phi->at(trIndex[j])<-35||phi->at(trIndex[j])>35)&&(phi->at(trIndex[j])<145||phi->at(trIndex[j])>215)) continue;
				}
				if(theta->at(trIndex[j])>0.09&&theta->at(trIndex[j])<0.1){
					if((phi->at(trIndex[j])<-25||phi->at(trIndex[j])>25)&&(phi->at(trIndex[j])<155||phi->at(trIndex[j])>205)) continue;
				}
				if(theta->at(trIndex[j])>0.1&&theta->at(trIndex[j])<0.14){
					if((phi->at(trIndex[j])<-15||phi->at(trIndex[j])>15)&&(phi->at(trIndex[j])<165||phi->at(trIndex[j])>195)) continue;
				}
				if(theta->at(trIndex[j])>0.14){
					if((phi->at(trIndex[j])<-10||phi->at(trIndex[j])>10)&&(phi->at(trIndex[j])<170||phi->at(trIndex[j])>190)) continue;
				}
			}
 					
 					
 			double len = length->at(trIndex[j]);
 			double yVal = yTOF1->at(trIndex[j]);
 			double xVal = xTOF1->at(trIndex[j]);	
 			if(!(active.at(j)%2)){
 					//if((j%2)){

 				len+=sqrt(pow(xTOF2->at(trIndex[j])-xTOF1->at(trIndex[j]), 2)+pow(yTOF2->at(trIndex[j])-yTOF1->at(trIndex[j]), 2)+pow(zTOF2->at(trIndex[j])-zTOF1->at(trIndex[j]), 2));
 				yVal = yTOF2->at(trIndex[j]);
 				xVal = xTOF2->at(trIndex[j]);
 			}



			if(maskPMT[active.at(j)][0]==1 && maskPMT[active.at(j)][1]==1){
				tofdiff.Fill(active.at(j)+0.5, tof1.at(j)-tof2.at(j));
			}
 			if(maskPMT[active.at(j)][0]==1){
 				tof1yHisto.at(active.at(j)).Fill(yVal,tof1.at(j));
 				tof12yHisto.at(active.at(j)).Fill(yVal,tof1.at(j));
 			}
 			if(maskPMT[active.at(j)][1]==1){
 				tof2yHisto.at(active.at(j)).Fill(yVal,tof2.at(j));
 				tof12yHisto.at(active.at(j)).Fill(yVal,tof2.at(j));
 			}
		}
	}
	
	TFile out("cHistos.root", "RECREATE");
	out.cd();

	tofdiff.Write();
	for(int i = 0; i < tof1yHisto.size(); i++){
		tof1yHisto.at(i).Write();
		tof2yHisto.at(i).Write();
		tof12yHisto.at(i).Write();
	}
	out.Close();
}
