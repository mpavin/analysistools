#include <stdlib.h>
#include <iostream>
#include <vector>
#include <string>
#include <ChainDef.h>
#include <Functions.h>
#include <TOFMapping.h>
#include <TChain.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TFile.h>
#include <sstream>
#include <math.h>
#include <T0Def.h>
#include <CDef.h>
#include <map>
#include <TEfficiency.h>
#include <dEdxResolution.h>
#include <TMath.h>
using namespace std;

int main(){

	string fileName = "../../input/list2010.txt";
	string scintPos = "scintPos.dat";
	string t0file = "t0Values2010.root";
	string cfile = "cValues.root";
	string dEdxName = "dEdxParData.root";
	int nm2TOF = 200;
	double m2TOFMin = -0.9;
	double m2TOFMax = 1.8;	
	int np = 200;
	double pMin = -10.;
	double pMax = 10;	

	double tofcut = 2000;
	double zLastCut = 680;

	double zcutMin = -657.4;
	double zcutMax = -567.4;
	

	TChain chaint0("T0");
	ReadT0(chaint0, t0file);

	TChain chainc("cScint");
	ReadC(chainc, cfile);
	
	TChain chain("FTOF");
	ReadData(chain, fileName);

	vector<string> sPos;
	vector<double> xPos;
	ReadInputList(scintPos, sPos);
	for(unsigned int i = 0; i < sPos.size(); i++){	
		stringstream convert(sPos.at(i));
		double res;
		convert >> res;
		xPos.push_back(res);
	}
	
	
	vector<double> ranges;
	for(int i = 0; i < 8; i++){
		ranges.push_back(0.3);
	}

	vector<TH1D> tofres;

	TH1D tofresAll("tofresAll", "", 200, -1500, 1500);
	for(int i = 0; i <nScint-1; i++){
		string name1 = "tofres" + ConvertToString(i+1., 0) + "_" + ConvertToString(i+2., 0);

		
		TH1D temp1(name1.c_str(), "", 200, -1500, 1500);

		tofres.push_back(temp1);

	}

	unsigned int nEntries = chain.GetEntries();
	cout << "Number of entries: " << nEntries << endl;
	
	for(unsigned int i = 0; i < nEntries; i++){
		if(!((i+1)%100000)) cout << "Processing entry: " << i+1 << endl;
		chain.GetEntry(i);
		//cout << trigger << endl;
 		
 		
 		
		
		//****************************************************************************************************************
		// Cheching for active scintillators
		if(trigger < 0) continue;
		//if(xStart->size() > 2) continue;
		map<int, Hit> active;
		for(unsigned int j = 0; j < nScint; j++){
 			
 			chaint0.GetEntry(j);
 			
			Hit hit;
			hit.tr = -999;
			hit.tof[0] = -999;
			hit.tof[1] = -999;
			int both[2] = {0,0};

 			for(unsigned int k = 0; k < 2; k++){
 			 
 			 	if(maskPMT[j][k]==0){
 			 		both[k] = 1;
 			 		continue;
 			 	}
 			 	
 				if(tdc->at(pmtId[j][k]) < 1) continue;
 				if(tdc->at(pmtId[j][k]) >= tdcChan) continue;
 				
			
 				hit.tof[k] = tdc->at(pmtId[j][k])*channelSize - t0[k];
 				both[k] = 1;
 							
 			}
 			
 			if(both[0]&&both[1]){
				//tofMatched.Fill(j+1);
 				active.insert(std::pair<int,Hit>(j,hit) );
 				//if(j==56) cout << "Hit "  << j << endl;
 			}			
		}	
 		
		std::map<int,Hit>::iterator it = active.begin();

		//****************************************************************************************************************
		// Matching tracks and scintillators	
		//map<int, ImpactPar> acTr;
		if(active.size() == 0) continue;

		vector<int> matched;
		vector<double> xVal;
		vector<double> yVal;
		vector<double> dxVal;
		
	
		
		for(unsigned int j = 0; j < xStart->size(); j++){
			//if(RST->at(j) != 0) continue;
			//cout <<vType->at(j) << endl;
			//if(vType->at(j) != 3) continue;
			
			if(zTPCLast->at(j) < zLastCut) continue;
			if(topology->at(j) == 9){
				if(nVTPC->at(j) < 20) continue;
				if(nMTPC->at(j) < 30) continue;
			}
			else if(topology->at(j) == 10) continue;
			else if(topology->at(j) == 11){
				if(nVTPC->at(j) < 20) continue;
				if(nMTPC->at(j) < 30) continue;
			}
			else if(topology->at(j) == 12){
				if(nGTPC->at(j) < 6) continue;
				if(nMTPC->at(j) < 70) continue;
			}	
			else if(topology->at(j) == 13) continue;

			else if(topology->at(j) == 14){
				if(nVTPC->at(j) < 20) continue;
				if(nMTPC->at(j) < 30) continue;
			}	
			else if(topology->at(j) == 15){
				if(nVTPC->at(j) < 20) continue;
				if(nMTPC->at(j) < 30) continue;
			}	
			if(distToTarg->at(j) > sqrt(TMath::Power(xStart->at(j)*xEStart->at(j),2)+TMath::Power(xStart->at(j)*xEStart->at(j),2))/sqrt(xStart->at(j)*xStart->at(j)+yStart->at(j)*yStart->at(j)))	
				continue;	
			//if(topology->at(j) > 11) continue;
			if(TMath::Abs(yTOF1->at(j))>60) continue;
			if(xTOF2->at(j) > xPos.at(0) + 5) continue;
			if(xTOF1->at(j) < xPos.at(xPos.size()-1) - 5.5) continue;
			if(zStart->at(j) < zcutMin || zStart->at(j) > zcutMax) continue;
			//if(zVert->at(j) < zcutMin || zVert->at(j) > zcutMax) continue;
			//if(TMath::Abs(dEdxRes.GetMean(p->at(j)/0.139470)-dEdx->at(j)) > 2.5*dEdxRes.GetWidth(p->at(j)/0.139470)) continue;
			
			//if(theta->at(j)<0.01) continue;
			if(RST->at(j) == 1){
				if(theta->at(j) > 0.18){
					if((phi->at(j)<-10 || phi->at(j)>10) && (phi->at(j)<170 || phi->at(j)>190)) continue;
				}
				else if(theta->at(j) > 0.13 && theta->at(j) < 0.18){
					if((phi->at(j)<-15 || phi->at(j)>15) && (phi->at(j)<165 || phi->at(j)>195)) continue;
				}
				else if(theta->at(j) > 0.11 && theta->at(j) < 0.13){
					if((phi->at(j)<-20 || phi->at(j)>20) && (phi->at(j)<160 || phi->at(j)>200)) continue;
				}
				else if(theta->at(j) > 0.09 && theta->at(j) < 0.11){
					if((phi->at(j)<-25 || phi->at(j)>25) && (phi->at(j)<155 || phi->at(j)>205)) continue;
				}
				else if(theta->at(j) > 0.07 && theta->at(j) < 0.09){
					if((phi->at(j)<-35 || phi->at(j)>35) && (phi->at(j)<145 || phi->at(j)>215)) continue;
				}
				else if(theta->at(j) > 0.06 && theta->at(j) < 0.07){
					if((phi->at(j)<-45 || phi->at(j)>40) && (phi->at(j)<140 || phi->at(j)>225)) continue;
				}
				else if(theta->at(j) > 0.05 && theta->at(j) < 0.06){
					if((phi->at(j)<-55 || phi->at(j)>50) && (phi->at(j)<130 || phi->at(j)>235)) continue;
				}
				else if(theta->at(j) > 0.04 && theta->at(j) < 0.05){
					if((phi->at(j)<-70|| phi->at(j)>65) && (phi->at(j)<115 || phi->at(j)>250)) continue;
				}
				else if(theta->at(j) > 0.03 && theta->at(j) < 0.04){
					if((phi->at(j)<-65|| phi->at(j)>60) && (phi->at(j)<110 || phi->at(j)>250)) continue;
				}
				else if(theta->at(j) > 0.01 && theta->at(j) < 0.03){
					if((phi->at(j)<-70|| phi->at(j)>70) && (phi->at(j)<110 || phi->at(j)>250)) continue;
				}
				else if(theta->at(j) > 0.00 && theta->at(j) < 0.01){
					if((phi->at(j)<-70|| phi->at(j)>70) && (phi->at(j)<110 || phi->at(j)>250)) continue;
				}
			}
			else{
				if(theta->at(j) > 0.20){
					if((phi->at(j)<-15 || phi->at(j)>13) && (phi->at(j)<167 || phi->at(j)>195)) continue;
				}
				else if(theta->at(j) > 0.14 && theta->at(j) < 0.20){
					if((phi->at(j)<-18|| phi->at(j)>16) && (phi->at(j)<164 || phi->at(j)>198)) continue;
				}
				else if(theta->at(j) > 0.12 && theta->at(j) < 0.14){
					if((phi->at(j)<-22|| phi->at(j)>20) && (phi->at(j)<160 || phi->at(j)>202)) continue;
				}
				else if(theta->at(j) > 0.10 && theta->at(j) < 0.12){
					if((phi->at(j)<-25|| phi->at(j)>23) && (phi->at(j)<157 || phi->at(j)>205)) continue;
				}
				else if(theta->at(j) > 0.09 && theta->at(j) < 0.10){
					if((phi->at(j)<-28|| phi->at(j)>26) && (phi->at(j)<154 || phi->at(j)>208)) continue;
				}
				else if(theta->at(j) > 0.08 && theta->at(j) < 0.09){
					if((phi->at(j)<-32|| phi->at(j)>30) && (phi->at(j)<150 || phi->at(j)>212)) continue;
				}
				else if(theta->at(j) > 0.07 && theta->at(j) < 0.08){
					if((phi->at(j)<-35|| phi->at(j)>32) && (phi->at(j)<148 || phi->at(j)>215)) continue;
				}
				else if(theta->at(j) > 0.06 && theta->at(j) < 0.07){
					if((phi->at(j)<-44|| phi->at(j)>40) && (phi->at(j)<140 || phi->at(j)>224)) continue;
				}
				else if(theta->at(j) > 0.05 && theta->at(j) < 0.06){
					if((phi->at(j)<-48|| phi->at(j)>44) && (phi->at(j)<136 || phi->at(j)>228)) continue;
				}
				else if(theta->at(j) > 0.04 && theta->at(j) < 0.05){
					if((phi->at(j)<-53|| phi->at(j)>53) && (phi->at(j)<140 || phi->at(j)>220)) continue;
				}
				else if(theta->at(j) > 0.02 && theta->at(j) < 0.04){
					if((phi->at(j)<-40|| phi->at(j)>40) && (phi->at(j)<150 || phi->at(j)>210)) continue;
				}
				else if(theta->at(j) > 0.00 && theta->at(j) < 0.02){
					if((phi->at(j)<-50|| phi->at(j)>50) && (phi->at(j)<130 || phi->at(j)>230)) continue;
				}
			}
			//if(zVert->at(j) < zcutMin || zVert->at(j) > zcutMax) continue;
			//if(TMath::Abs(dEdxRes.GetMean(p->at(j)/0.139470)-dEdx->at(j)) > 2.5*dEdxRes.GetWidth(p->at(j)/0.139470)) continue;

			
			int actId[2] = {0,0};

			double tof1[2] = {0,0};
			double tof2[2] = {0,0};
			
			int n = 0;
			
			for(it = active.begin(); it != active.end(); it++){
				if(n==2) break;
				
				chainc.GetEntry(it->first);
				if(!(it->first%2)){
					if(TMath::Abs(xPos.at(it->first)-xTOF2->at(j))<5)
					{
						actId[n] = it->first;
						tof1[n] = it->second.tof[0] - 70.26*(60-yTOF2->at(j))-20;
						tof2[n] = it->second.tof[1] - 70.26*(60+yTOF2->at(j))-20;
						//tof1[n] = it->second.tof[0] - TMath::Abs(c[0])*(60-yTOF2->at(j));
						//tof2[n] = it->second.tof[1] - TMath::Abs(c[1])*(60+yTOF2->at(j));
						n++;
					}
				}
				else{
					if(TMath::Abs(xPos.at(it->first)-xTOF1->at(j))<5)
					{
						actId[n] = it->first;
						tof1[n] = it->second.tof[0] - 70.26*(60-yTOF1->at(j))-20;
						tof2[n] = it->second.tof[1] - 70.26*(60+yTOF1->at(j))-20;
						//tof1[n] = it->second.tof[0] - TMath::Abs(c[0])*(60-yTOF1->at(j));
						//tof2[n] = it->second.tof[1] - TMath::Abs(c[1])*(60+yTOF1->at(j));
						n++;
					}				
				}
				
			}
			
			if(n!=2) continue;
			//cout << actId[0] << " " << actId[1] << endl;
			if(maskPMT[actId[0]][0] && maskPMT[actId[0]][1]){
				if(TMath::Abs(tof1[0]-tof2[0]) > tofcut) continue;
			}	
			if(maskPMT[actId[1]][0] && maskPMT[actId[1]][1]){
				if(TMath::Abs(tof1[1]-tof2[1]) > tofcut) continue;
			}
			
						
			double t1 = 0;
			double t2 = 0;
			
			if(maskPMT[actId[0]][0] && maskPMT[actId[0]][1]){
				t1 = (tof1[0]+tof2[0])/2.;
			}
			else if(maskPMT[actId[0]][0]){
				t1 = tof1[0];
			}
			else if(maskPMT[actId[0]][1]){
				t1 = tof2[0];
			}
			
			if(maskPMT[actId[1]][0] && maskPMT[actId[1]][1]){
				t2 = (tof1[1]+tof2[1])/2.;
			}
			else if(maskPMT[actId[1]][0]){
				t2 = tof1[1];
			}
			else if(maskPMT[actId[1]][1]){
				t2 = tof2[1];
			}
			
			
			if(!(actId[0]%2)){
				tofresAll.Fill(t1-t2);
				tofres.at((actId[0]+actId[1]-1)/2).Fill(t1-t2);
			}
			//if(actId > 0) tofMatched.Fill(0.5*(xTOF1->at(j)+xTOF2->at(j)), 0.5*(yTOF1->at(j)+yTOF2->at(j)));
			//if(dx < 6)
				//tofMatched.Fill(0.5*(xTOF1->at(j)+xTOF2->at(j)), 0.5*(yTOF1->at(j)+yTOF2->at(j)));
				//yMatched.Fill(0.5*(yTOF1->at(j)+yTOF2->at(j)));
			//if(actId < 0) cout << actId << endl;
		}
		
	
	}
	
	TFile out("tofRes.root", "RECREATE");
	out.cd();
	tofresAll.Write();
	for(int i = 0; i < tofres.size(); i++){
		
		tofres.at(i).Write();
	}
	/*TEfficiency efficiency(tofAll,tofMatched);
	efficiency.SetName("efficiency");
	efficiency.Write();*/
	out.Close();
}
