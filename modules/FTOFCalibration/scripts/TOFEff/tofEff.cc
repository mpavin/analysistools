#include <stdlib.h>
#include <iostream>
#include <vector>
#include <string>
#include <ChainDef.h>
#include <Functions.h>
#include <TOFMapping.h>
#include <TChain.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TFile.h>
#include <sstream>
#include <math.h>
#include <TMath.h>
#include <T0Def.h>
#include <CDef.h>
#include <map>
#include <TEfficiency.h>
#include <dEdxResolution.h>
using namespace std;

int main(){

	string fileName = "../../input/list2010.txt";
	string scintPos = "scintPos.dat";
	string t0file = "t0Values2010.root";
	string cfile = "cValues.root";
	string dEdxName = "dEdxParData.root";
	int nm2TOF = 200;
	double m2TOFMin = -0.9;
	double m2TOFMax = 1.8;	
	int np = 200;
	double pMin = -10.;
	double pMax = 10;	

	double tofcut = 2000;
	double zLastCut = 660;

	double zcutMin = -652.4;
	double zcutMax = -567.4;
	

	TChain chaint0("T0");
	ReadT0(chaint0, t0file);

	TChain chainc("cScint");
	ReadC(chainc, cfile);
	
	TChain chain("FTOF");
	ReadData(chain, fileName);

	vector<string> sPos;
	vector<double> xPos;
	ReadInputList(scintPos, sPos);
	for(unsigned int i = 0; i < sPos.size(); i++){	
		stringstream convert(sPos.at(i));
		double res;
		convert >> res;
		xPos.push_back(res);
	}
	
	
	vector<double> ranges;
	for(int i = 0; i < 8; i++){
		ranges.push_back(0.3);
	}
	dEdxResolution dEdxRes(dEdxName, ranges);

	TH1D s1charge("s1charge", "", 200, 0, 200);
	TH1D s11charge("s11charge", "", 150, 50, 200);
	TH1D sums1charge("sums1charge", "", 200, 0, 200);
	TH2D poss11charge("poss11charge", "", 200, 0, 7, 200, 50, 250);
	TH1D yAll("yAll", "", 120, -60, 60);
	TH1D yMatched("yMatched", "", 120, -60, 60);
	//TH1D xAll("xAll", "", 82, -370, 370);
	//TH1D xMatched("xMatched", "", 82, -370, 370);
	double xbord[81];
	for(int i = 0; i < 40; i++){
		xbord[i] = xPos.at(79-i)-5;
	}
	xbord[80] = xPos.at(39)+5;
	TH1D xAll("xAll", "", 80, xbord);
	TH1D xMatched("xMatched", "", 80, xbord);
	TH1D scAll("scAll", "", 80, 0, 80);
	TH1D scMatched("scMatched", "", 80, 0, 80);
	TH2D tofAll("tofAll", "", 82, -370, 370, 28, -70, 70);
	TH2D tofMatched("tofMatched", "", 82, -370, 370, 28, -70, 70);
	TH2D xerror("xerror", "", 100, -370, 370, 100, 0, 0.2);
	TH1D mult("mult", "", 15, 0., 15.);
	TH2D thetaphi("thetaphi", "", 200, -90, 270, 200, 0, 0.4);
	TH2D lenx("lenx","", 200, 1330, 1430, 200, -365, 365); 
	//TH1I tofOld("tofOld", "", nScint, 0.5, nScint+0.5);
	TH1D vertPos("vertPos", "", 200, -680, 200);
	TH1I eff("eff", "", 3, 0.5, 3.5);
	TH1D dxHisto("dxHisto", "", 200, 0, 20);
	TH2D dxxHisto("dxxHisto", "", 200, -370, 370, 200, 0, 100);
	TH1D tofDiff("tofDiff", "", 200, -5000, 5000);
	TH1D res("res", "", 200, -2000, 2000);
	
	TH2D thetapEff[6];
	TH2D thetapEffAcc[6];
	TH1D thetaEff[6];
	TH1D thetaEffAcc[6];
	TH1D pEff[6];
	TH1D pEffAcc[6];
	
	for(int i = 0; i < 6; i++){
		string name1 = "thetapEffz" + ConvertToString(i+1.,0);
		string name2 = "thetapEffAccz" + ConvertToString(i+1.,0);
		string name3 = "thetaEffz" + ConvertToString(i+1.,0);
		string name4 = "thetaEffAccz" + ConvertToString(i+1.,0);
		string name5 = "pEffz" + ConvertToString(i+1.,0);
		string name6 = "pEffAccz" + ConvertToString(i+1.,0);
		
		TH2D temp1(name1.c_str(), "", 120, 0, 30, 100, 0, 400);
		TH2D temp2(name2.c_str(), "", 120, 0, 30, 100, 0, 400);
		TH1D temp3(name3.c_str(), "", 40, 0, 400);
		TH1D temp4(name4.c_str(), "", 40, 0, 400);
		TH1D temp5(name5.c_str(), "", 60, 0, 30);
		TH1D temp6(name6.c_str(), "", 60, 0, 30);	
		
		temp1.Sumw2();
		temp2.Sumw2();
		temp3.Sumw2();
		temp4.Sumw2();
		temp5.Sumw2();
		temp6.Sumw2();
		
		thetapEff[i] = temp1;
		thetapEffAcc[i] = temp2;
		thetaEff[i] = temp3;
		thetaEffAcc[i] = temp4;
		pEff[i] = temp5;
		pEffAcc[i] = temp6;
	}
	
	/*TH2D thetapEff("thetapEff", "", 200, 0, 30, 200, 0, 400);
	TH2D thetapEffAcc("thetapEffAcc", "", 200, 0, 30, 200, 0, 400);
	TH1D thetaEff("thetaEff", "", 200, 0, 400);
	TH1D thetaEffAcc("thetaEffAcc", "", 200, 0, 400);
	TH1D pEff("pEff", "", 200, 0, 30);
	TH1D pEffAcc("pEffAcc", "", 200, 0, 30);*/
	
	unsigned int nEntries = chain.GetEntries();
	cout << "Number of entries: " << nEntries << endl;
	
	for(unsigned int i = 0; i < nEntries; i++){
		if(!((i+1)%100000)) cout << "Processing entry: " << i+1 << endl;
		chain.GetEntry(i);
		//b_aData->GetEntry(i);
		//cout << trigger << endl;
 		
 		if(trigger < 0) continue;
 		//cout << qdcS1->size() << endl;
 		//cout << tdc->size() << endl;
		s1charge.Fill(qdcS1->at(0));
		s11charge.Fill(qdcS1->at(1));
		sums1charge.Fill((qdcS1->at(1)+qdcS1->at(2)+qdcS1->at(3)+qdcS1->at(4))/4.);
		poss11charge.Fill(sqrt(TMath::Power(-3642*beamD.aX+beamD.bX,2)+TMath::Power(-3642*beamD.aY+beamD.bY-3,2)), qdcS1->at(1));
		//if((qdcS1->at(1)+qdcS1->at(2)+qdcS1->at(3)+qdcS1->at(4))/4. > 65) continue;
		//****************************************************************************************************************
		// Cheching for active scintillators
		
		//if(TMath::Abs(sqrt(TMath::Power(-3642*beamD.aX+beamD.bX,2)+TMath::Power(-3642*beamD.aY+beamD.bY-3,2))-3.026) > 2*0.54438) continue;
		//if(TMath::Abs(qdcS1->at(1)-105.66) > 2*12.68) continue;
		if(xStart->size() > 4) continue;
		//if(qdcS1->at(1) > 105) continue;
		map<int, Hit> active;
		
		
		for(unsigned int j = 0; j < nScint; j++){
 			
 			chaint0.GetEntry(j);
			Hit hit;
			hit.tr = -999;
			hit.tof[0] = -999;
			hit.tof[1] = -999;
			int both[2] = {0,0};
			
 			for(unsigned int k = 0; k < 2; k++){
 			 
 			 	if(maskPMT[j][k]==0){
 			 		both[k] = 1;
 			 		continue;
 			 	}
 			 	//cout << j << " " << k << endl;
 				if(tdc->at(pmtId[j][k]) < 1) continue;
 				if(tdc->at(pmtId[j][k]) >= tdcChan) continue;
 				
				//cout << j << " " << k << endl;
 				hit.tof[k] = tdc->at(pmtId[j][k])*channelSize - t0[k];
 				//cout << "END " << j << " " << k << endl;
 				both[k] = 1;
 							
 			}
 			
 			if(both[0]&&both[1]){
				//tofMatched.Fill(j+1);
 				active.insert(std::pair<int,Hit>(j,hit) );
 				//if(j==56) cout << "Hit "  << j << endl;
 			}			
		}	
 		
 		//cout << "radi1" << endl;
		std::map<int,Hit>::iterator it = active.begin();

		
		//****************************************************************************************************************
		// Matching tracks and scintillators	
		//map<int, ImpactPar> acTr;
		//if(active.size() == 0) continue;
		/*if(active.size() < xStart->size())
			cout << active.size() <<  " " << xStart->size() <<endl;*/
		vector<int> matched;
		vector<double> xVal;
		vector<double> yVal;
		vector<double> dxVal;
		vector<double> thetaVal;
		vector<double> pVal;
		vector<int> idZVal;
		int nTr = 0;
		mult.Fill(xStart->size());
		
		for(unsigned int j = 0; j < xStart->size(); j++){
		  //if(RST->at(j) != 0) continue;
			//cout <<vType->at(j) << endl;
			//if(vType->at(j) != 3) continue;
			vertPos.Fill(zVert->at(j));

			
			if(zTPCLast->at(j) < zLastCut) continue;
			if(topology->at(j) == 9){
				if(nVTPC->at(j) < 10) continue;
				if(nMTPC->at(j) < 5) continue;
			}
			else if(topology->at(j) == 10) continue;
			else if(topology->at(j) == 11){
				if(nVTPC->at(j) < 10) continue;
				if(nMTPC->at(j) < 5) continue;
			}
			else if(topology->at(j) == 12){
				if(nGTPC->at(j) < 5) continue;
				if(nMTPC->at(j) < 10) continue;
			}	
			else if(topology->at(j) == 13) continue;

			else if(topology->at(j) == 14){
				if(nVTPC->at(j) < 10) continue;
				if(nMTPC->at(j) < 5) continue;
			}	
			else if(topology->at(j) == 15){
				if(nVTPC->at(j) < 10) continue;
				if(nMTPC->at(j) < 5) continue;
			}	
			if(distToTarg->at(j) > sqrt(TMath::Power(xStart->at(j)*xEStart->at(j),2)+TMath::Power(xStart->at(j)*xEStart->at(j),2))/sqrt(xStart->at(j)*xStart->at(j)+yStart->at(j)*yStart->at(j)))	
				continue;	
			//if(topology->at(j) > 11) continue;
			if(TMath::Abs(yTOF1->at(j))>60) continue;
			if(xTOF2->at(j) > xPos.at(0) + 5) continue;
			if(xTOF1->at(j) < xPos.at(xPos.size()-1) - 5) continue;
			if(zStart->at(j) < zcutMin || zStart->at(j) > zcutMax) continue;
			//if(zVert->at(j) < zcutMin || zVert->at(j) > zcutMax) continue;
			//if(TMath::Abs(dEdxRes.GetMean(p->at(j)/0.139470)-dEdx->at(j)) > 2.5*dEdxRes.GetWidth(p->at(j)/0.139470)) continue;
			
			//if(theta->at(j)<0.01) continue;
			/*if(RST->at(j) == 1){
				if(theta->at(j) > 0.18){
					if((phi->at(j)<-10 || phi->at(j)>10) && (phi->at(j)<170 || phi->at(j)>190)) continue;
				}
				else if(theta->at(j) > 0.13 && theta->at(j) < 0.18){
					if((phi->at(j)<-15 || phi->at(j)>15) && (phi->at(j)<165 || phi->at(j)>195)) continue;
				}
				else if(theta->at(j) > 0.11 && theta->at(j) < 0.13){
					if((phi->at(j)<-20 || phi->at(j)>20) && (phi->at(j)<160 || phi->at(j)>200)) continue;
				}
				else if(theta->at(j) > 0.09 && theta->at(j) < 0.11){
					if((phi->at(j)<-25 || phi->at(j)>25) && (phi->at(j)<155 || phi->at(j)>205)) continue;
				}
				else if(theta->at(j) > 0.07 && theta->at(j) < 0.09){
					if((phi->at(j)<-35 || phi->at(j)>35) && (phi->at(j)<145 || phi->at(j)>215)) continue;
				}
				else if(theta->at(j) > 0.06 && theta->at(j) < 0.07){
					if((phi->at(j)<-45 || phi->at(j)>40) && (phi->at(j)<140 || phi->at(j)>225)) continue;
				}
				else if(theta->at(j) > 0.05 && theta->at(j) < 0.06){
					if((phi->at(j)<-55 || phi->at(j)>50) && (phi->at(j)<130 || phi->at(j)>235)) continue;
				}
				else if(theta->at(j) > 0.04 && theta->at(j) < 0.05){
					if((phi->at(j)<-70|| phi->at(j)>65) && (phi->at(j)<115 || phi->at(j)>250)) continue;
				}
				else if(theta->at(j) > 0.03 && theta->at(j) < 0.04){
					if((phi->at(j)<-65|| phi->at(j)>60) && (phi->at(j)<110 || phi->at(j)>250)) continue;
				}
				else if(theta->at(j) > 0.01 && theta->at(j) < 0.03){
					if((phi->at(j)<-70|| phi->at(j)>70) && (phi->at(j)<110 || phi->at(j)>250)) continue;
				}
				else if(theta->at(j) > 0.00 && theta->at(j) < 0.01){
					if((phi->at(j)<-70|| phi->at(j)>70) && (phi->at(j)<110 || phi->at(j)>250)) continue;
				}
			}
			else{
				if(theta->at(j) > 0.20){
					if((phi->at(j)<-15 || phi->at(j)>13) && (phi->at(j)<167 || phi->at(j)>195)) continue;
				}
				else if(theta->at(j) > 0.14 && theta->at(j) < 0.20){
					if((phi->at(j)<-18|| phi->at(j)>16) && (phi->at(j)<164 || phi->at(j)>198)) continue;
				}
				else if(theta->at(j) > 0.12 && theta->at(j) < 0.14){
					if((phi->at(j)<-22|| phi->at(j)>20) && (phi->at(j)<160 || phi->at(j)>202)) continue;
				}
				else if(theta->at(j) > 0.10 && theta->at(j) < 0.12){
					if((phi->at(j)<-25|| phi->at(j)>23) && (phi->at(j)<157 || phi->at(j)>205)) continue;
				}
				else if(theta->at(j) > 0.09 && theta->at(j) < 0.10){
					if((phi->at(j)<-28|| phi->at(j)>26) && (phi->at(j)<154 || phi->at(j)>208)) continue;
				}
				else if(theta->at(j) > 0.08 && theta->at(j) < 0.09){
					if((phi->at(j)<-32|| phi->at(j)>30) && (phi->at(j)<150 || phi->at(j)>212)) continue;
				}
				else if(theta->at(j) > 0.07 && theta->at(j) < 0.08){
					if((phi->at(j)<-35|| phi->at(j)>32) && (phi->at(j)<148 || phi->at(j)>215)) continue;
				}
				else if(theta->at(j) > 0.06 && theta->at(j) < 0.07){
					if((phi->at(j)<-44|| phi->at(j)>40) && (phi->at(j)<140 || phi->at(j)>224)) continue;
				}
				else if(theta->at(j) > 0.05 && theta->at(j) < 0.06){
					if((phi->at(j)<-48|| phi->at(j)>44) && (phi->at(j)<136 || phi->at(j)>228)) continue;
				}
				else if(theta->at(j) > 0.04 && theta->at(j) < 0.05){
					if((phi->at(j)<-53|| phi->at(j)>53) && (phi->at(j)<140 || phi->at(j)>220)) continue;
				}
				else if(theta->at(j) > 0.02 && theta->at(j) < 0.04){
					if((phi->at(j)<-40|| phi->at(j)>40) && (phi->at(j)<150 || phi->at(j)>210)) continue;
				}
				else if(theta->at(j) > 0.00 && theta->at(j) < 0.02){
					if((phi->at(j)<-50|| phi->at(j)>50) && (phi->at(j)<130 || phi->at(j)>230)) continue;
				}
			}*/
			//if((phi->at(j)<-15||phi->at(j)>15)&&(phi->at(j)<165||phi->at(j)>195)) continue;
			if(nTOF->at(j) > 0) {
				//tofOld.Fill(scintId->at(j));
				eff.Fill(3);
			}
			eff.Fill(1);
			thetaphi.Fill(phi->at(j), theta->at(j));
			lenx.Fill(length->at(j), 0.5*(xTOF1->at(j)+xTOF2->at(j)));
			//nTr++;
			tofAll.Fill(0.5*(xTOF1->at(j)+xTOF2->at(j)), 0.5*(yTOF1->at(j)+yTOF2->at(j)));
			xerror.Fill(0.5*(xTOF1->at(j)+xTOF2->at(j)), xETOF1->at(j));
			yAll.Fill(0.5*(yTOF1->at(j)+yTOF2->at(j)));
			xAll.Fill(0.5*(xTOF1->at(j)+xTOF2->at(j)));
			
			double scdx = 100;
			for(int k = 0; k < 80; k++){
				if(!(k%2)){
					if(TMath::Abs(xTOF2->at(j)-xPos.at(k)) < 4.5){
						scAll.Fill(k+0.5);
					}					
				}
				else{
					if(TMath::Abs(xTOF1->at(j)-xPos.at(k)) < 4.5){
						scAll.Fill(k+0.5);
					}				
				}
			}
			int idZ = FindZBin(zStart->at(j));
			thetapEff[idZ].Fill(p->at(j), 1000*theta->at(j));
			thetaEff[idZ].Fill(1000*theta->at(j));
			pEff[idZ].Fill( p->at(j));
			
			
			int actId = -999;
			double dx = 800;
			double tof1 = 0;
			double tof2 = 0;

			for(it = active.begin(); it != active.end(); it++){
				if(!(it->first%2)){
					if(TMath::Abs(xPos.at(it->first)-xTOF2->at(j))<dx)
					{
						dx = TMath::Abs(xPos.at(it->first)-xTOF2->at(j));
						actId = it->first;
						tof1 = it->second.tof[0] - (2.1/0.0299792)*(60-yTOF2->at(j));
						tof2 = it->second.tof[1] - (2.1/0.0299792)*(60+yTOF2->at(j));
					}
				}
				else{
					if(TMath::Abs(xPos.at(it->first)-xTOF1->at(j))<dx)
					{
						dx = TMath::Abs(xPos.at(it->first)-xTOF1->at(j));
						actId = it->first;
						tof1 = it->second.tof[0] - (2.1/0.0299792)*(60-yTOF1->at(j));
						tof2 = it->second.tof[1] - (2.1/0.0299792)*(60+yTOF1->at(j));
					}				
				}
				
			}
			
			dxHisto.Fill(dx);
			dxxHisto.Fill(0.5*(xTOF1->at(j)+xTOF2->at(j)), dx);
			if(actId < 0) continue;
			tofDiff.Fill(tof1-tof2+12);
			if(maskPMT[actId][0] && maskPMT[actId][1]){
				if(TMath::Abs(tof1-tof2) > tofcut) continue;
			}
			matched.push_back(actId);
			xVal.push_back(0.5*(xTOF1->at(j)+xTOF2->at(j)));
			yVal.push_back(0.5*(yTOF1->at(j)+yTOF2->at(j)));
			dxVal.push_back(dx);
			thetaVal.push_back(1000*theta->at(j));
			pVal.push_back(p->at(j));
			idZVal.push_back(idZ);
			//if(actId > 0) tofMatched.Fill(0.5*(xTOF1->at(j)+xTOF2->at(j)), 0.5*(yTOF1->at(j)+yTOF2->at(j)));
			//if(dx < 6)
				//tofMatched.Fill(0.5*(xTOF1->at(j)+xTOF2->at(j)), 0.5*(yTOF1->at(j)+yTOF2->at(j)));
				//yMatched.Fill(0.5*(yTOF1->at(j)+yTOF2->at(j)));
			//if(actId < 0) cout << actId << endl;
		}
		
		
		for(int j = 0; j < matched.size(); j++){
			if(matched.at(j) < 0) continue;
			for(int k = 0; k < matched.size(); k++){
				if(matched.at(k) < 0) continue;
				if(k==j) continue;
				if(matched.at(j) == matched.at(k)){
					matched.at(j) = -999;
					matched.at(k) = -999;
				}
			}			
		}
		
		for(int j = 0; j < matched.size(); j++){
			if(matched.at(j) < 0) continue;
			if(dxVal.at(j) < 6){
				eff.Fill(2);
				tofMatched.Fill(xVal.at(j), yVal.at(j));
				yMatched.Fill(yVal.at(j));
				xMatched.Fill(xVal.at(j));
				if(dxVal.at(j) < 4.5){
					scMatched.Fill(matched.at(j)+0.5);
				}
				thetapEffAcc[idZVal.at(j)].Fill(pVal.at(j), thetaVal.at(j));
				thetaEffAcc[idZVal.at(j)].Fill(thetaVal.at(j));
				pEffAcc[idZVal.at(j)].Fill(pVal.at(j));
			}
		}
		
			
	}
	
	TFile out("tofEff.root", "RECREATE");
	out.cd();
	s1charge.Write();
	s11charge.Write();
	sums1charge.Write();
	poss11charge.Write();
	res.Write();
	tofMatched.Divide(&tofAll);
	tofAll.Write();
	xerror.Write();
	thetaphi.Write();
	//tofOld.Write();
	tofMatched.Write();
	eff.Write();
	vertPos.Write();
	dxHisto.Write();
	tofDiff.Write();
	dxxHisto.Write();

	for(int i =0;  i < 6; i++){
		//thetapEffAcc[i].Divide(&thetapEff[i]);
		thetapEffAcc[i].Write();	
		thetapEff[i].Write();	

		//thetaEffAcc[i].Sumw2();
		thetaEffAcc[i].Divide(&thetaEff[i]);
		thetaEffAcc[i].Write();
		//pEffAcc[i].Sumw2();
		pEffAcc[i].Divide(&pEff[i]);
		pEffAcc[i].Write();
	}	



	
	//yMatched.Sumw2();
	//xMatched.Sumw2();
	//yAll.Sumw2();
	//xAll.Sumw2();
	//yMatched.Divide(&yAll);
	//xMatched.Divide(&xAll);
	TH1D xeff("xeff", "" , 80, xbord);
	TH1D yeff("yeff", "" , 120, -60, 60);
	TH1D sceff("sceff", "" , 80, 0, 80);
	for(int i = 0; i < 82; i++){
		if(xAll.GetBinContent(i+1) < 1) continue;
		double eff = xMatched.GetBinContent(i+1)/xAll.GetBinContent(i+1);
		xeff.SetBinContent(i+1, eff);
		xeff.SetBinError(i+1, sqrt(eff*(1-eff)/xAll.GetBinContent(i+1)));
	}
	for(int i = 0; i < 120; i++){
		if(yAll.GetBinContent(i+1) < 1) continue;
		double eff = yMatched.GetBinContent(i+1)/yAll.GetBinContent(i+1);
		yeff.SetBinContent(i+1, eff);
		yeff.SetBinError(i+1, sqrt(eff*(1-eff)/yAll.GetBinContent(i+1)));
	}
	for(int i = 0; i < 80; i++){
		if(scAll.GetBinContent(i+1) < 1) continue;
		double eff = scMatched.GetBinContent(i+1)/scAll.GetBinContent(i+1);
		sceff.SetBinContent(i+1, eff);
		if(eff <= 1)
			sceff.SetBinError(i+1, sqrt(eff*(1-eff)/scAll.GetBinContent(i+1)));
	}
	scAll.Write();
	scMatched.Write();
	sceff.Write();
	xeff.Write();
	yeff.Write();
	lenx.Write();

	yAll.Write();
	yMatched.Write();
	xAll.Write();
	xMatched.Write();
	mult.Write();
	/*for(int i = 0; i <nScint; i++){
		
		qdcHisto1.at(i).Write();
		qdcHistoAcc1.at(i).Write();
		qdcHisto2.at(i).Write();
		qdcHistoAcc2.at(i).Write();
	}*/
	/*TEfficiency efficiency(tofAll,tofMatched);
	efficiency.SetName("efficiency");
	efficiency.Write();*/
	out.Close();
}
