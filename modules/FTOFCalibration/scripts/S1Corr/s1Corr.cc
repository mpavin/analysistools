#include <vector>
#include <string>
#include <iostream>
#include "TOFMapping.h"
#include "Functions.h"
#include "ChainDef.h"
#include <TH1D.h>
#include <TH2D.h>
#include <TFile.h>

using namespace std;


int main(){

	string fileName = "../../input/list2010v3.txt";

 	
 	TH2D lencor("lencor", "", 200, 0., 6, 200, -5000, 5000);
 	TH2D beamxy("beamxy", "", 400, -5, 5, 400, -5, 5);
 	TH2D beamxyrot("beamxyrot", "", 200, -3, 3, 200, -3, 3);
 	TH1D beamxp("beamxp", "", 200, -3, 3);
 	TH1D beamyp("beamyp", "", 200, -3, 3);
 	
 	TH2D qdctdc2("qdctdc2", "", 200, 0, 400, 200, 1000, s1NChannels);
 	TH2D qdctdc3("qdctdc3", "", 200, 0, 400, 200, 1000, s1NChannels);
 	TH2D qdctdc4("qdctdc4", "", 200, 0, 400, 200, 1000, s1NChannels);
 	TH1D s11qdc("s11qdc", "", 200, 0, 600);
 	TH1D s12qdc("s12qdc", "", 200, 0, 600);
 	TH1D s13qdc("s13qdc", "", 200, 0, 600);
 	TH1D s14qdc("s14qdc", "", 200, 0, 600);
 	TH1D s12tdc("s12tdc", "", 200, 0, 4100);
 	TH1D s13tdc("s13tdc", "", 200, 0, 4100);
 	TH1D s14tdc("s14tdc", "", 200, 0, 4100);
 	TH1D s12tdcr("s12tdcr", "", 200, 0, 4100);
 	TH1D s13tdcr("s13tdcr", "", 200, 0, 4100);
 	TH1D s14tdcr("s14tdcr", "", 200, 0, 4100); 	
 	TH2D t24t13("t24t13","", 200, -500, 500, 200, -1000, 1000);
 	TH1D deltat("deltat", "", 200, -1000, 1000);

 	
 	TH2D s13tdcyp("s13tdcyp","", 200, -3, 3, 200, -1000, 1000);
 	TH2D s24tdcxp("s24tdcxp","", 200, -3, 3, 200, -1000, 1000);
 	TH2D s13tdcxp("s13tdcxp","", 200, -3, 3, 200, -1000, 1000);
 	TH2D s24tdcyp("s24tdcyp","", 200, -3, 3, 200, -1000, 1000);

 	
 	TH2D runs12tdc("runs12tdc","", 600, 10521, 11121, 200, 0, 4100);
 	TH2D runs13tdc("runs13tdc","", 600, 10521, 11121, 200, 0, 4100);
 	TH2D runs14tdc("runs14tdc","", 600, 10521, 11121, 200, 0, 4100);
 	
 	TH2D runs11qdc("runs11qdc","", 600, 10521, 11121, 200, 0, 600);
 	TH2D runs12qdc("runs12qdc","", 600, 10521, 11121, 200, 0, 600);
 	TH2D runs13qdc("runs13qdc","", 600, 10521, 11121, 200, 0, 600);
 	TH2D runs14qdc("runs14qdc","", 600, 10521, 11121, 200, 0, 600);
	TChain chain("FTOF");
	ReadData(chain, fileName); 	
 	int nEntries = chain.GetEntries();
 	//nEntries = 10000;
 	
 	cout << "Number of entries: " << nEntries << endl;
 	for(int i = 0; i < nEntries; i++){
 		
 		chain.GetEntry(i);
 		if(!((i+1)%100000))
 			cout << "Proccessing entry #" << i+1 << endl;
 		
 		if(trigger <1) continue;
		/*if(runNumber > 10864 && runNumber < 10874) continue;
		if(runNumber > 10939 && runNumber < 10945) continue; 	
		if(runNumber > 11089) continue;*/ 	
		double x = beamD.aX*zS1 + beamD.bX;
		double y = beamD.aY*zS1 + beamD.bY;
		double zT = -612.4;
		double xT = beamD.aX*zT + beamD.bX;
		double yT = beamD.aY*zT + beamD.bY;
		double xp = (x-y)/sqrt(2);
		double yp = (x+y)/sqrt(2);
		beamxy.Fill(x, y);
		beamxyrot.Fill(xp,yp);
		beamxp.Fill(xp);
		beamyp.Fill(yp);
		s12tdc.Fill(tdcS1->at(1));
		s13tdc.Fill(tdcS1->at(2));
		s14tdc.Fill(tdcS1->at(3));
		
		if((runNumber > 10864 && runNumber < 10874) || (runNumber > 10939 && runNumber < 10945) || (runNumber > 11089)){
			s12tdcr.Fill(tdcS1->at(1));
			s13tdcr.Fill(tdcS1->at(2));
			s14tdcr.Fill(tdcS1->at(3));			
		}
		s11qdc.Fill(qdcS1->at(1));
		s12qdc.Fill(qdcS1->at(2));
		s13qdc.Fill(qdcS1->at(3));
		s14qdc.Fill(qdcS1->at(4));
		lencor.Fill(sqrt((xT-x)*(xT-x) + (yT-y)*(yT-y) + (zT-zS1)*(zT-zS1))-(zT-zS1), (s12k*(tdcS1->at(1)-s120)+s13k*(tdcS1->at(2)-s130)+s14k*(tdcS1->at(3)-s140))/4.);
		t24t13.Fill((tdcS1->at(2)-s130)/2., (s12k*(tdcS1->at(1)-s120)+s14k*(tdcS1->at(3)-s140))/2.);
		deltat.Fill(s13k*(tdcS1->at(2)-s130)-(s12k*(tdcS1->at(1)-s120)+s14k*(tdcS1->at(3)-s140)));
		qdctdc2.Fill(qdcS1->at(1),tdcS1->at(1));
		qdctdc3.Fill(qdcS1->at(2),tdcS1->at(2));
		qdctdc4.Fill(qdcS1->at(3),tdcS1->at(3));
		s13tdcyp.Fill(yp, s13k*(tdcS1->at(2)-s130));
		s24tdcxp.Fill(xp, s12k*(tdcS1->at(1)-s120)-s14k*(tdcS1->at(3)-s140));
		s13tdcxp.Fill(xp, s13k*(tdcS1->at(2)-s130));
		s24tdcyp.Fill(yp, (s12k*(tdcS1->at(1)-s120)-s14k*(tdcS1->at(3)-s140)));

		runs12tdc.Fill(runNumber, tdcS1->at(1));
		runs13tdc.Fill(runNumber, tdcS1->at(2));
		runs14tdc.Fill(runNumber, tdcS1->at(3));
		
		runs11qdc.Fill(runNumber, qdcS1->at(1));
		runs12qdc.Fill(runNumber, qdcS1->at(2));
		runs13qdc.Fill(runNumber, qdcS1->at(3));
		runs14qdc.Fill(runNumber, qdcS1->at(4));
				
		
 	}
 	


 	

 	
 	TFile outFile("s1res.root", "RECREATE");
 	
 	outFile.cd();

 	beamxy.Write();
 	beamxyrot.Write();
 	beamxp.Write();
 	beamyp.Write();
 	s12tdc.Write();
 	s13tdc.Write();
 	s14tdc.Write();
 	s12tdcr.Write();
 	s13tdcr.Write();
 	s14tdcr.Write();
	lencor.Write();
 	qdctdc2.Write();
 	qdctdc3.Write();
 	qdctdc4.Write();
 	s11qdc.Write();
 	s12qdc.Write();
 	s13qdc.Write();
 	s14qdc.Write();
 	s24tdcxp.Write();
 	s13tdcyp.Write();
 	s24tdcyp.Write();
 	s13tdcxp.Write();
	deltat.Write();
 	t24t13.Write();


	runs12tdc.Write();
	runs13tdc.Write();
	runs14tdc.Write();
 	
 	runs11qdc.Write();
 	runs12qdc.Write();
 	runs13qdc.Write();
 	runs14qdc.Write();
 	outFile.Close();
 	
}





