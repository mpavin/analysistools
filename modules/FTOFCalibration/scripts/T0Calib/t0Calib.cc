#include <stdlib.h>
#include <iostream>
#include <vector>
#include <string>
#include <ChainDef.h>
#include <Functions.h>
#include <TOFMapping.h>
#include <TChain.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TFile.h>
#include <sstream>
#include <math.h>
#include <T0Def.h>
#include <CDef.h>
#include <TMath.h>

using namespace std;

int main(){

	bool first = true;
	
	string fileName = "../../input/list2010v3.txt";
	string scintPos = "scintPos.dat";
	string t0file = "t0Values.root";
	string cfile = "filec.root";
	
	int nt0Bins = 200;
	double t0min = -15000.;
	double t0max = 20000.;
	
	double tofmin = 44000;
	double tofmax = 49000;
	
	if(!first){
		t0min = -2000;
		t0max = 2000;
	}
	
	double mass = 0.139570;
	double pMin = 0.0;
	double pMax = 2;
	double dEdxMin = 0;
	double dEdxMax = 1.20;
	double zLastCut = 650;
	double m2TOFMin = -2.15;
	double m2TOFMax = 0.18;
	double zcutMin = -657.4;
	double zcutMax = -567.3;
	
	TChain chaint0("T0");

	TChain chainc("cScint");
	
	if(!first){
		ReadT0(chaint0, t0file);
		//ReadC(chainc, cfile);
	}
	
	TChain chain("FTOF");
	ReadData(chain, fileName);
	
	vector<string> sPos;
	vector<double> xPos;
	ReadInputList(scintPos, sPos);
	for(unsigned int i = 0; i < sPos.size(); i++){
		
		stringstream convert(sPos.at(i));
		double res;
		convert >> res;
		xPos.push_back(res);
	}
	
	TH2D m2TOF1("m2TOF1", "", 200, 0, 2, 200, -1, 2);
	TH2D m2TOF2("m2TOF2", "", 200, 0, 2, 200, -1, 2);
	TH1D lenHisto("lenHisto", "", 200, 1340, 1490);
	TH2D lenxHisto("lenxHisto", "", 200, 1340, 1490, 200, -370, 370);
	TH1D zStartHisto("zStartHisto", "", 200, -680, -530);
	TH1D tdcHisto("tdcHisto", "", 400, 0, 4400);
	TH1D qdcHisto("qdcHisto", "", 400, 0, 4400);
	TH2D qdctdcHisto("qdctdcHisto", "", 400, 0, 4400, 400, 0, 4400);
	
	TH1D tdcHistoAc("tdcHistoAc", "", 400, 0, 4400);
	TH1D qdcHistoAc("qdcHistoAc", "", 400, 0, 4400);
	TH2D qdctdcHistoAc("qdctdcHistoAc", "", 400, 0, 4400, 400, 0, 4400);
	vector<TH1D> t01Histo;
	vector<TH1D> t02Histo;
	vector<TH2D> t01HistoRun;
	vector<TH2D> t02HistoRun;
	vector<TH2D> t01yHisto;
	vector<TH2D> t02yHisto;
	vector<TH1D> tof1Histo;
	vector<TH1D> tof2Histo;
	for(unsigned int i = 0; i < nScint; i++){
		string name1 = "t01PMT" + ConvertToString(i+1., 0);
		string name2 = "t02PMT" + ConvertToString(i+1., 0);
		string name3 = "runt01PMT" + ConvertToString(i+1., 0);
		string name4 = "runt02PMT" + ConvertToString(i+1., 0);
		string name5 = "t01yPMT" + ConvertToString(i+1., 0);
		string name6 = "t02yPMT" + ConvertToString(i+1., 0);
		string name7 = "tof1_" + ConvertToString(i+1., 0);
		string name8 = "tof2_" + ConvertToString(i+1., 0);
		TH1D temp1(name1.c_str(), "", nt0Bins, t0min, t0max);
		TH1D temp2(name2.c_str(), "", nt0Bins, t0min, t0max);
		TH2D temp3(name3.c_str(), "", 650, 10500, 11150, nt0Bins, t0min, t0max);
		TH2D temp4(name4.c_str(), "", 650, 10500, 11150, nt0Bins, t0min, t0max);
		TH2D temp5(name5.c_str(), "", 200, -60, 60, nt0Bins, t0min+50000, t0max+40000);
		TH2D temp6(name6.c_str(), "", 200, -60, 60, nt0Bins, t0min+50000, t0max+40000);
		TH1D temp7(name7.c_str(), "", nt0Bins, tofmin, tofmax);
		TH1D temp8(name8.c_str(), "", nt0Bins, tofmin, tofmax);
		t01Histo.push_back(temp1);
		t02Histo.push_back(temp2);
		t01HistoRun.push_back(temp3);
		t02HistoRun.push_back(temp4);
		t01yHisto.push_back(temp5);
		t02yHisto.push_back(temp6);
		tof1Histo.push_back(temp7);
		tof2Histo.push_back(temp8);
	}
	
	unsigned int nEntries = chain.GetEntries();
	cout << "Number of entries: " << nEntries << endl;
	
	for(unsigned int i = 0; i < nEntries; i++){
		if(!((i+1)%100000)) cout << "Processing entry: " << i+1 << endl;
		chain.GetEntry(i);
		
		if (trigger < 1) continue;
		
		
		vector<int> active;
		vector<double> tof1;
		vector<double> tof2;
		
		//****************************************************************************************************************
		// Cheching for active scintillators
		//double fact[2] = {-1, 1};
		for(unsigned int j = 0; j < nScint; j++){
 			if(!first){
 				chaint0.GetEntry(j);
 			}			
			
			int both[2] = {0,0};
			double tof[2] = {0, 0};
 			for(unsigned int k = 0; k < 2; k++){
 			 
 			 	qdcHisto.Fill(qdc->at(pmtId[j][k]));
 			 	tdcHisto.Fill(tdc->at(pmtId[j][k]));
 			 	qdctdcHisto.Fill(qdc->at(pmtId[j][k]), tdc->at(pmtId[j][k]));
 			 	if(maskPMT[j][k]==0){
 			 		both[k] = 1;
 			 		continue;
 			 	}
 			 	
 				if(tdc->at(pmtId[j][k]) < 1) continue;
 				if(tdc->at(pmtId[j][k]) >= tdcChan) continue;
 				
			
 				tof[k] = tdc->at(pmtId[j][k])*channelSize - t0[k];
 				both[k] = 1;
 							
 			}
 			
 			if(both[0]&&both[1]){

 				active.push_back(j);
 				tof1.push_back(tof[0]);
 				tof2.push_back(tof[1]);
 			}			
		}
		
		//****************************************************************************************************************
		// Matching tracks and scintillators	
		vector<int> trIndex;
		vector<int> nTr;
		
		for(int j = 0; j < active.size(); j++){
			trIndex.push_back(-999);
			nTr.push_back(0);
		}
		for(unsigned int j = 0; j < xStart->size(); j++){
			for(int k = 0; k < active.size(); k++){

 				if(!(active.at(k)%2)){
 				//if((j%2)){
 					if(abs(xTOF2->at(j)-xPos.at(active.at(k))) > swidth/2.) continue;
 					if(abs(yTOF2->at(j)) > slength/2.) continue;

 				}
 				else{
 					if(abs(xTOF1->at(j)-xPos.at(active.at(k))) > swidth/2.) continue;
 					if(abs(yTOF1->at(j)) > slength/2.) continue;
 				}
				nTr[k]++;
				trIndex.at(k) = j;
			}
		
		}

		//****************************************************************************************************************
		// Calculating t0 	
		for(int j = 0; j < active.size(); j++){
			if(nTr.at(j) != 1) continue;
			
			if(RST->at(trIndex[j])!=0) continue;
 			if(zStart->at(trIndex[j])<zcutMin || zStart->at(trIndex[j]) >= zcutMax) continue;
 			//if(zVert->at(k)<zcutMin || zVert->at(k) >= zcutMax) continue;

 			if(p->at(trIndex[j]) < pMin || p->at(trIndex[j]) >= pMax) continue;
 			if(dEdx->at(trIndex[j]) < dEdxMin || dEdx->at(trIndex[j]) >= dEdxMax) continue;
 			//if(zTPCLast->at(k) < zLastCut) continue;
			if(topology->at(trIndex[j]) == 9){
				if(nVTPC->at(trIndex[j]) < 20) continue;
				if(nMTPC->at(trIndex[j]) < 30) continue;
			}
			else if(topology->at(trIndex[j]) == 10) continue;
			else if(topology->at(trIndex[j]) == 11){
				if(nVTPC->at(trIndex[j]) < 20) continue;
				if(nMTPC->at(trIndex[j]) < 30) continue;
			}
			else if(topology->at(trIndex[j]) == 12){
				if(nGTPC->at(trIndex[j]) < 6) continue;
				if(nMTPC->at(trIndex[j]) < 70) continue;
			}	
			else if(topology->at(trIndex[j]) == 13) continue;

			else if(topology->at(trIndex[j]) == 14){
				if(nVTPC->at(trIndex[j]) < 20) continue;
				if(nMTPC->at(trIndex[j]) < 30) continue;
			}	
			else if(topology->at(trIndex[j]) == 15){
				if(nVTPC->at(trIndex[j]) < 20) continue;
				if(nMTPC->at(trIndex[j]) < 30) continue;
			}	
			if(distToTarg->at(trIndex[j]) > sqrt(TMath::Power(xStart->at(trIndex[j])*xEStart->at(trIndex[j]),2)+TMath::Power(xStart->at(trIndex[j])*xEStart->at(trIndex[j]),2))/sqrt(xStart->at(trIndex[j])*xStart->at(trIndex[j])+yStart->at(trIndex[j])*yStart->at(trIndex[j])))	
				continue;	
				
			if(RST->at(trIndex[j]) == 1){
				if(theta->at(trIndex[j])>0&&theta->at(trIndex[j])<0.02){
					if((phi->at(trIndex[j])<-50||phi->at(trIndex[j])>50)&&(phi->at(trIndex[j])<130||phi->at(trIndex[j])>230)) continue;
				}
				if(theta->at(trIndex[j])>0.02&&theta->at(trIndex[j])<0.04){
					if((phi->at(trIndex[j])<-60||phi->at(trIndex[j])>60)&&(phi->at(trIndex[j])<155||phi->at(trIndex[j])>205)) continue;
				}
				if(theta->at(trIndex[j])>0.04&&theta->at(trIndex[j])<0.06){
					if((phi->at(trIndex[j])<-40||phi->at(trIndex[j])>40)&&(phi->at(trIndex[j])<145||phi->at(trIndex[j])>215)) continue;
				}
				if(theta->at(trIndex[j])>0.08&&theta->at(trIndex[j])<0.09){
					if((phi->at(trIndex[j])<-27||phi->at(trIndex[j])>27)&&(phi->at(trIndex[j])<153||phi->at(trIndex[j])>207)) continue;
				}
				if(theta->at(trIndex[j])>0.06&&theta->at(trIndex[j])<0.08){
					if((phi->at(trIndex[j])<-30||phi->at(trIndex[j])>30)&&(phi->at(trIndex[j])<150||phi->at(trIndex[j])>210)) continue;
				}
				if(theta->at(trIndex[j])>0.09&&theta->at(trIndex[j])<0.1){
					if((phi->at(trIndex[j])<-25||phi->at(trIndex[j])>25)&&(phi->at(trIndex[j])<155||phi->at(trIndex[j])>205)) continue;
				}
				if(theta->at(trIndex[j])>0.1&&theta->at(trIndex[j])<0.14){
					if((phi->at(trIndex[j])<-15||phi->at(trIndex[j])>15)&&(phi->at(trIndex[j])<165||phi->at(trIndex[j])>195)) continue;
				}
				if(theta->at(trIndex[j])>0.14){
					if((phi->at(trIndex[j])<-10||phi->at(trIndex[j])>10)&&(phi->at(trIndex[j])<170||phi->at(trIndex[j])>190)) continue;
				}
			}
			else{
				if(theta->at(trIndex[j])>0&&theta->at(trIndex[j])<0.02){
					if((phi->at(trIndex[j])<-60||phi->at(trIndex[j])>60)&&(phi->at(trIndex[j])<120||phi->at(trIndex[j])>240)) continue;
				}
				if(theta->at(trIndex[j])>0.02&&theta->at(trIndex[j])<0.04){
					if((phi->at(trIndex[j])<-60||phi->at(trIndex[j])>60)&&(phi->at(trIndex[j])<120||phi->at(trIndex[j])>240)) continue;
				}
				if(theta->at(trIndex[j])>0.04&&theta->at(trIndex[j])<0.06){
					if((phi->at(trIndex[j])<-40||phi->at(trIndex[j])>40)&&(phi->at(trIndex[j])<145||phi->at(trIndex[j])>215)) continue;
				}
				if(theta->at(trIndex[j])>0.08&&theta->at(trIndex[j])<0.09){
					if((phi->at(trIndex[j])<-30||phi->at(trIndex[j])>27)&&(phi->at(trIndex[j])<150||phi->at(trIndex[j])>207)) continue;
				}
				if(theta->at(trIndex[j])>0.06&&theta->at(trIndex[j])<0.08){
					if((phi->at(trIndex[j])<-35||phi->at(trIndex[j])>35)&&(phi->at(trIndex[j])<145||phi->at(trIndex[j])>215)) continue;
				}
				if(theta->at(trIndex[j])>0.09&&theta->at(trIndex[j])<0.1){
					if((phi->at(trIndex[j])<-25||phi->at(trIndex[j])>25)&&(phi->at(trIndex[j])<155||phi->at(trIndex[j])>205)) continue;
				}
				if(theta->at(trIndex[j])>0.1&&theta->at(trIndex[j])<0.14){
					if((phi->at(trIndex[j])<-15||phi->at(trIndex[j])>15)&&(phi->at(trIndex[j])<165||phi->at(trIndex[j])>195)) continue;
				}
				if(theta->at(trIndex[j])>0.14){
					if((phi->at(trIndex[j])<-10||phi->at(trIndex[j])>10)&&(phi->at(trIndex[j])<170||phi->at(trIndex[j])>190)) continue;
				}
			}

 					
 					
 			double len = length->at(trIndex[j]);
 			/*double dx0 = beamD.bX - xStart->at(trIndex[j]) + zStart->at(trIndex[j])*cos(phi->at(trIndex[j]))*cos(theta->at(trIndex[j]));
 			double dy0 = beamD.bY - yStart->at(trIndex[j]) + zStart->at(trIndex[j])*sin(phi->at(trIndex[j]))*cos(theta->at(trIndex[j]));
 			double dtx = beamD.aX - cos(phi->at(trIndex[j]))*cos(theta->at(trIndex[j]));
 			double dty = beamD.aY - sin(phi->at(trIndex[j]))*cos(theta->at(trIndex[j]));
 			double zMin = -(dx0*dtx+dy0*dty)/(dtx*dtx+dty*dty);
 			
 			double dlen = sqrt(TMath::Power((zMin+612.4)*cos(phi->at(trIndex[j]))*cos(theta->at(trIndex[j])),2) + 
 			TMath::Power((zMin+612.4)*sin(phi->at(trIndex[j]))*cos(theta->at(trIndex[j])),2) + TMath::Power(zMin+612.4,2)); 
 			if(zMin > -612.4){
 				len = len-dlen;
 			}
 			else{
 				len = len+dlen;
 			}*/
 			double yVal = yTOF1->at(trIndex[j]);
 			double xVal = xTOF1->at(trIndex[j]);	
 			if(!(active.at(j)%2)){
 					//if((j%2)){

 				len+=sqrt(pow(xTOF2->at(trIndex[j])-xTOF1->at(trIndex[j]), 2)+pow(yTOF2->at(trIndex[j])-yTOF1->at(trIndex[j]), 2)+pow(zTOF2->at(trIndex[j])-zTOF1->at(trIndex[j]), 2));
 				yVal = yTOF2->at(trIndex[j]);
 				xVal = xTOF2->at(trIndex[j]);
 			}

 			double tofCalc = sqrt(1+pow(mass/p->at(trIndex[j]),2))*len*33.3564;
 			lenHisto.Fill(len);
 			lenxHisto.Fill(len, xVal);
 			zStartHisto.Fill(zStart->at(trIndex[j]));	
 					
 			if(maskPMT[active.at(j)][0]==1){
				qdcHistoAc.Fill(qdc->at(pmtId[active.at(j)][0]));
 				tdcHistoAc.Fill(tdc->at(pmtId[active.at(j)][0]));
 				qdctdcHistoAc.Fill(qdc->at(pmtId[active.at(j)][0]), tdc->at(pmtId[active.at(j)][0]));		
 				tof1.at(j) +=  -(2.1/0.0299792)*(60-yVal);
 				double mc2 = p->at(trIndex[j])*p->at(trIndex[j])*(pow(0.029979*tof1.at(j),2)/pow(len,2)-1);
 				m2TOF1.Fill(p->at(trIndex[j]), mc2);
 				bool skip = false;
 				if(!first){
 					if(mc2 < m2TOFMin || mc2 >= m2TOFMax) skip = true;
 				}
 				if(!skip){
	 				t01Histo.at(active.at(j)).Fill(tof1.at(j)-tofCalc);
	 				t01yHisto.at(active.at(j)).Fill(yVal,tof1.at(j));
	 						//t01HistoRun.at(j).Fill(runNumber,tof[0]-sqrt(1+0.938/30.92)*(zStart->at(k)+657.4)*33.3564-tofCalc);
	 				t01HistoRun.at(active.at(j)).Fill(runNumber,tof1.at(j)-tofCalc);
	 				tof1Histo.at(active.at(j)).Fill(tof1.at(j));
 				}
 			}
 			if(maskPMT[active.at(j)][1]==1){
				qdcHistoAc.Fill(qdc->at(pmtId[active.at(j)][1]));
 				tdcHistoAc.Fill(tdc->at(pmtId[active.at(j)][1]));
 				qdctdcHistoAc.Fill(qdc->at(pmtId[active.at(j)][1]), tdc->at(pmtId[active.at(j)][1]));	
				tof2.at(j) +=  -(2.1/0.0299792)*(60+yVal);
 				double mc2 = p->at(trIndex[j])*p->at(trIndex[j])*(pow(0.029979*tof2.at(j),2)/pow(len,2)-1);
 				m2TOF2.Fill(p->at(trIndex[j]), mc2);
 				bool skip = false;
 				if(!first){	
 					if(mc2 < m2TOFMin || mc2 >= m2TOFMax) skip = true;;
 				}
 				if(!skip){
	 				t02Histo.at(active.at(j)).Fill(tof2.at(j)-tofCalc);
	 				t02yHisto.at(active.at(j)).Fill(yVal,tof2.at(j));
	 				//t02HistoRun.at(j).Fill(runNumber,tof[1]-sqrt(1+0.938/30.92)*(zStart->at(k)+657.4)*33.3564-tofCalc);
	 				t02HistoRun.at(active.at(j)).Fill(runNumber,tof2.at(j)-tofCalc);
	 				tof2Histo.at(active.at(j)).Fill(tof2.at(j));
 				}
 			}	
		}
		
	}
	
	TFile out("t0Histos.root", "RECREATE");
	out.cd();
	//eff.Write();
	qdcHisto.Write();
	tdcHisto.Write();
	qdctdcHisto.Write();
	qdcHistoAc.Write();
	tdcHistoAc.Write();
	qdctdcHistoAc.Write();	
	m2TOF1.Write();
	m2TOF2.Write();
	lenHisto.Write();
	lenxHisto.Write();
	zStartHisto.Write();
	for(int i = 0; i < t01Histo.size(); i++){
		t01Histo.at(i).Write();
		t02Histo.at(i).Write();
		t01HistoRun.at(i).Write();
		t02HistoRun.at(i).Write();
		t01yHisto.at(i).Write();
		t02yHisto.at(i).Write();
		tof1Histo.at(i).Write();
		tof2Histo.at(i).Write();
		cout << t01Histo.at(i).GetMean() << endl;
		cout << t02Histo.at(i).GetMean() << endl;
	}
	out.Close();
	
}
