#include <vector>
#include <string>
#include <iostream>
#include "TOFMapping.h"
#include "Functions.h"
#include "ChainDef.h"
#include <TH1D.h>
#include <TH2D.h>
#include <TFile.h>

using namespace std;


int main(){

	string fileName = "../../input/list2010v2.txt";
	vector<TH1D> xPos;
	vector<TH1D> yPos;
	vector<TH2D> xyPos;

	TH1D activePMTs("activePMTs", "", nChannels+2, 0,nChannels+2);
	TH1D activeScint1("activeScint1", "Active scintillators (1 PMT)", nScint+2, 0, nScint+2);
	TH1D activeScint2("activeScint2", "Active scintillators (2 PMTs)", nScint+2, 0, nScint+2);
	TH1D pmtRatio("pmtRatio", "", nScint+2, 0, nScint+2);
	//TCanvas activeC("activeC", "", 1200, 800);

	TH1D tdcDiff("tdcDiff", "TDC1-TDC2", 2000, -1000, 1000);
	TH2D tdcDiff2D("tdcDiff2D", "", nScint+2, 0, nScint+2, 3000, -3000, 3000); 
	 

	TH1D pos("pos", "", 360, -360,360);
	TH1D tofRec("tofRec", "", 2, 0,2);

	for(int i = 0; i < nScint; i++){
		string namex = "xPos" + ConvertToString(i+1,0);
		string namey = "yPos" + ConvertToString(i+1,0);
		string namexy = "xyPos" + ConvertToString(i+1,0);

		double min = 0;
		double max = 0;
		
		if(i < 40){
			min = 355.5-4.5-9.*(i)-25.;
			max = 355.5-4.5-9.*(i)+25.;		
		}
		else{
			min = -360.+4.5-9.*(i-79)-25.;
			max = -360.+4.5-9.*(i-79)+25.;	
				
		}
		//min = -380;
		//max = 380;
		TH1D tempx(namex.c_str(), "" , 152, min, max);
		TH1D tempy(namey.c_str(), "", 200, -100, 100);	
		TH2D tempxy(namexy.c_str(), "", 200, min, max, 200, -100, 100);
		xPos.push_back(tempx);
		yPos.push_back(tempy);
		xyPos.push_back(tempxy);
		
	}

 	int nHitsPMTs[nChannels];
 	for(unsigned int i = 0; i < nChannels; i++){
 		nHitsPMTs[i] = 0;
 	}	

	TChain chain("FTOF");
	
	ReadData(chain, fileName);
	

 	
 	int nEntries = chain.GetEntries();
 	//nEntries = 10000;
 	
 	cout << "Number of entries: " << nEntries << endl;
 	for(int i = 0; i < nEntries; i++){
 		
 		chain.GetEntry(i);
 		if(!((i+1)%100000))
 			cout << "Proccessing entry #" << i+1 << endl;
 		
 		
 		if(tdc->size() == 0) continue;
 		
 		
 		for(unsigned int j = 0; j < nScint; j++){
 		
 			int both = 0;
 			
 			for(unsigned int k = 0; k < 2; k++){
 				if(tdc->at(pmtId[j][k]) < 1) continue;
 				if(tdc->at(pmtId[j][k]) >= tdcChan) continue;
 				//cout << "radi" << endl;
 				nHitsPMTs[2*j + k]++;
 				both++;
 				activePMTs.Fill(2*j + k + 1); 				
 			}
			
 			if(both > 0){
 				activeScint1.Fill(j+1);
 				if((j==7)||(j==46)||(j==56)||(j==78)){
		 				for(unsigned int k = 0; k < xTOF1->size(); k++){
		 					
		 					if((j%2)){
		 						
			 					xPos.at(j).Fill(xTOF1->at(k));
			 					yPos.at(j).Fill(yTOF1->at(k));
			 					xyPos.at(j).Fill(xTOF1->at(k), yTOF1->at(k));	
			 										
		 					}
							else{

			 					xPos.at(j).Fill(xTOF2->at(k));
			 					
			 					yPos.at(j).Fill(yTOF2->at(k));
			 					xyPos.at(j).Fill(xTOF2->at(k), yTOF2->at(k));	
			 					 					
							}
						
		 				} 				
 				}
 			}
 			
 			if(both == 2){
 				tofRec.Fill(0);
 				activeScint2.Fill(j+1);
 				if(abs(tdc->at(pmtId[j][0])-tdc->at(pmtId[j][1]))<2000){
 					if((j!=7)||(j!=46)||(j!=56)||(j!=78)){
		 				for(unsigned int k = 0; k < xTOF1->size(); k++){
		 					
		 				if(zStart->at(k) < -657.41 || zStart->at(k) > -567.42) continue;	
						if(topology->at(k) == 9){
							if(nVTPC->at(k) < 20) continue;
							if(nMTPC->at(k) < 30) continue;
						}
						else if(topology->at(k) == 10) continue;
						else if(topology->at(k) == 11){
							if(nVTPC->at(k) < 20) continue;
							if(nMTPC->at(k) < 30) continue;
						}
						else if(topology->at(k) == 12){
							if(nGTPC->at(k) < 6) continue;
							if(nMTPC->at(k) < 70) continue;
						}	
						else if(topology->at(k) == 13) continue;

						else if(topology->at(k) == 14){
							if(nVTPC->at(k) < 20) continue;
							if(nMTPC->at(k) < 30) continue;
						}	
						else if(topology->at(k) == 15){
							if(nVTPC->at(k) < 20) continue;
							if(nMTPC->at(k) < 30) continue;
						}	
		 					//if(zTPCLast->at(k) < 700) continue;
		 					//double sigmaR = sqrt(xTOF1->at(k)*xTOF1->at(k)*xETOF1->at(k)*xETOF1->at(k)+yTOF1->at(k)*yTOF1->at(k)*yETOF1->at(k)*yETOF1->at(k))/
		 					//sqrt(xTOF1->at(k)*xTOF1->at(k)+yTOF1->at(k)*yTOF1->at(k));
		 					//if(xETOF1->at(k) > 1) continue;
		 					//if(yETOF1->at(k) > 1) continue;
		 					//cerr << "radi1 " << xTOF1->at(k) << " " << yTOF1->at(k) << " "  << xTOF2->at(k) << " " << yTOF2->at(k) << endl;
		 					if((j%2)){
		 						
			 					xPos.at(j).Fill(xTOF1->at(k));
			 					yPos.at(j).Fill(yTOF1->at(k));
			 					xyPos.at(j).Fill(xTOF1->at(k), yTOF1->at(k));	
			 										
		 					}
							else{

			 					xPos.at(j).Fill(xTOF2->at(k));
			 					
			 					yPos.at(j).Fill(yTOF2->at(k));
			 					xyPos.at(j).Fill(xTOF2->at(k), yTOF2->at(k));	
			 					 					
							}
						
		 				} 
	 				}
	 								
 				}
 				//if(nChannels/nScint == 2){
 					tdcDiff.Fill(tdc->at(pmtId[j][0])-tdc->at(pmtId[j][1]));
 					tdcDiff2D.Fill(j+1, tdc->at(pmtId[j][0])-tdc->at(pmtId[j][1]));
 				//}
 			}
 		}
 			
 		for(unsigned int k = 0; k < xTOF1->size(); k++){
 			//diffLen.Fill(length->at(k)-vtxLength->at(k));
 			pos.Fill(xTOF1->at(k));
 			if(nTOF->at(k)>0){
				tofRec.Fill(1);
			}
 		}
 	}
 	

 	//if(nChannels/nScint == 2){
 		for(unsigned int j = 0; j < nScint; j++){
 			double nom = (double) (nHitsPMTs[2*j] - nHitsPMTs[2*j+1])*1.;
 			double denom = (double) (nHitsPMTs[2*j]+nHitsPMTs[2*j+1])*1.; 
 			pmtRatio.Fill(j+1, nom/denom);

 		} 	
 	//}
 	

 	
 	TFile outFile("ftof_res.root", "RECREATE");
 	
 	outFile.cd();
 	activePMTs.Write();
 	activeScint1.Write();
 	activeScint2.Write();
 	pmtRatio.Write();
 	tdcDiff.Write();
 	tdcDiff2D.Write();
 	pos.Write();
 	//diffLen.Write();
 	tofRec.Write();
 	
 	for(unsigned int i = 0; i<xPos.size(); i++){
 		xPos.at(i).Write();
 		yPos.at(i).Write();
 		xyPos.at(i).Write();
 	}
 	
 	outFile.Close();
}





