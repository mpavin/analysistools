#include <stdlib.h>
#include <iostream>
#include <vector>
#include <string>
#include <ChainDef.h>
#include <Functions.h>
#include <TOFMapping.h>
#include <TChain.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TFile.h>
#include <sstream>
#include <math.h>
#include <T0Def.h>
#include <CDef.h>
#include <TMath.h>
#include <dEdxResolution.h>
#include <TMatrixD.h>
using namespace std;

int main(){

	string fileName = "../../input/list2010v3.txt";
	string scintPos = "scintPos.dat";
	string t0file = "t0Values2010.root";
	string cfile = "cValues.root";
	string dEdxName = "dEdxParData.root";
	

	double pMin = 0.5;
	double pMax = 1.;	

	
	double dEdxMax = 1.20;
	//double zLastCut = 650;

	double zcutMin = -657.41;
	double zcutMax = -567.40;
	
	/*vector<double> ranges;
	for(int i = 0; i < 8; i++){
		ranges.push_back(0.3);
	}
	dEdxResolution dEdxRes(dEdxName, ranges);*/
	
	TChain chaint0("T0");
	ReadT0(chaint0, t0file);

	TChain chainc("cScint");
	ReadC(chainc, cfile);
	
	TChain chain("FTOF");
	ReadData(chain, fileName);

	vector<string> sPos;
	vector<double> xPos;
	ReadInputList(scintPos, sPos);
	for(unsigned int i = 0; i < sPos.size(); i++){
		
		stringstream convert(sPos.at(i));
		double res;
		convert >> res;
		xPos.push_back(res);
	}
	
	double nmaxpi = 50000.;

	vector<double> ts12;
	vector<double> ts13;
	vector<double> ts14;
	
	unsigned int nEntries = chain.GetEntries();
	cout << "Number of entries: " << nEntries << endl;
	int n = 0;
	for(unsigned int i = 0; i < nEntries; i++){
		if(!((i+1)%100000)) cout << "Processing entry: " << i+1 << endl;
		chain.GetEntry(i);

		if(n > nmaxpi) break;
		if(trigger < 1) continue;
 		
		if(runNumber > 10864 && runNumber < 10874) continue;
		if(runNumber > 10939 && runNumber < 10945) continue; 	
		if(runNumber > 11089) continue;  
		if(tdcS1->at(1) > 2500 || tdcS1->at(2) > 2500 || tdcS1->at(3) > 2500) continue;

 		vector<int> active;
		vector<double> tof1;
		vector<double> tof2;
		
		//****************************************************************************************************************
		// Cheching for active scintillators
		//double fact[2] = {-1, 1};

		for(unsigned int j = 0; j < nScint; j++){
 			
 			chaint0.GetEntry(j);
			
			
			int both[2] = {0,0};
			double tof[2] = {0, 0};
 			for(unsigned int k = 0; k < 2; k++){
 			 
 			 	if(maskPMT[j][k]==0){
 			 		both[k] = 1;
 			 		continue;
 			 	}
 			 	
 				if(tdc->at(pmtId[j][k]) < 1) continue;
 				if(tdc->at(pmtId[j][k]) >= tdcChan+93) continue;
 				
			
 				tof[k] = tdc->at(pmtId[j][k])*channelSize - t0[k] + 27;
 				both[k] = 1;
 							
 			}
 			
 			if(both[0]&&both[1]){
				
 				active.push_back(j);
 				tof1.push_back(tof[0]);
 				tof2.push_back(tof[1]);
 			}			
		}	
 		
 		if(active.size() == 0) continue;
 		if(xStart->size() == 0) continue;
		
		//****************************************************************************************************************

		n++;
 		ts12.push_back(tdcS1->at(1));
 		ts13.push_back(tdcS1->at(2));
 		ts14.push_back(tdcS1->at(3));

		
	}
	
	

	double r[3] = {20,10,20};
	
	double rtemp1[3] = {0, 0, 0};
	double rtemp2[3] = {0, 0, 0};
	double Jtemp1[6] = {0,0,0,0,0,0};

	double mt = 0;
	
	for(int i = 0; i < 10; i++){
		
		for(int j = 0; j < 6; j++){
			if(i <3){
				rtemp1[j] = 0;
				rtemp2[j] = 0;
			}
			Jtemp1[j]=0;

		}
		mt=0;
		for(int j = 0; j < ts12.size(); j++){
			double T = (r[0]*(ts12.at(j)-s120) + r[2]*(ts14.at(j)-s140) - r[1]*(ts13.at(j)-s130));
			mt += T;
			rtemp1[0] += T*(ts12.at(j)-s120);
			rtemp1[1] += T*(ts13.at(j)-s130);
			rtemp1[2] += T*(ts14.at(j)-s140);
			

			Jtemp1[0] += (ts12.at(j)-s120)*(ts12.at(j)-s120);
			Jtemp1[1] += (ts12.at(j)-s120)*(ts13.at(j)-s130);
			Jtemp1[2] += (ts12.at(j)-s120)*(ts14.at(j)-s140);

			Jtemp1[3] += (ts13.at(j)-s130)*(ts13.at(j)-s130);
			Jtemp1[4] += (ts13.at(j)-s130)*(ts14.at(j)-s140);

			Jtemp1[5] += (ts14.at(j)-s140)*(ts14.at(j)-s140);
			Jtemp1[6] += (ts14.at(j)-s140);


		}
		mt = mt/nmaxpi;
		for(int j = 0; j < 6; j++){
			if(i <3){
				rtemp1[j] = rtemp1[j]/nmaxpi;
				//rtemp2[j] = rtemp2[j]/nmaxpi;
			}
			Jtemp1[j]=Jtemp1[j]/nmaxpi;

		}
			
			
		TMatrixD col(3, 1);
		TMatrixD Jac(3,3);

		col[0][0] = 2*rtemp1[0];
		col[1][0] = -2*rtemp1[1];
		col[2][0] = 2*rtemp1[2];
		
		
		Jac[0][0] = 2*Jtemp1[0] ;
		Jac[0][1] = -2*Jtemp1[1];
		Jac[0][2] = 2*Jtemp1[2];
	
		
		Jac[1][0] = Jac[0][1];
		Jac[1][1] = 2*Jtemp1[3];
		Jac[1][2] = -2*Jtemp1[4];

		
		Jac[2][0] = Jac[2][0];
		Jac[2][1] = Jac[2][1];
		Jac[2][2] = 2*Jtemp1[5];



				
		TMatrixD JacInv = Jac.Invert();
		TMatrixD dx = JacInv*col;
		
		
		
		r[0] = r[0] - dx[0][0];
		r[1] = r[1] - dx[1][0];
		r[2] = r[2] - dx[2][0];
		cout << r[0] << " " << r[1] << " " << r[2] <<  endl;
		cout << "*******************************************" << endl;
	}
	TFile out("s1Res.root", "RECREATE");
	out.cd();

	out.Close();
}
