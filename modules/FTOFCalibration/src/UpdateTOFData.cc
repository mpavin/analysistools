#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <cstdlib>
#include <stdexcept>
#include <TTree.h>
#include <TFile.h>
#include <cstdlib>
#include <evt/Event.h>
#include <evt/EventHeader.h>
#include <io/EventFileChain.h>
#include <det/TriggerConst.h>
#include <det/BPDConst.h>
#include <evt/RawEvent.h>
#include <evt/SimEvent.h>
#include "StrTools.h"
#include <boost/format.hpp>
#include "Ana.h"
#include "TrackExtrapolation.h"
#include "evt/RawEvent.h"
#include "evt/raw/TOF.h"
#include <TMath.h>
#include <evt/IndexedObjectLinker.h>
#include <io/ShineEventFile.h>
using namespace std;
using namespace io;
using namespace evt;
using namespace evt::rec;
using namespace evt::sim;
using namespace evt::raw;
using namespace det;

using namespace StrTools;


void Ana::UpdateTOFData()
{
int k = 0;
	//EventFileChain eventFileChain(fFTOFConfig.GetJob());
	Event event1;
	int nTOF = 0;
	string name = fFTOFConfig.GetOutputDir() + "/shoe.root";
	ShineEventFile inFile;	
	inFile.Open(fFTOFConfig.GetJob().at(0), io::eRead);


	ShineEventFile outFile;	
	outFile.Open(name, io::eNew);	
	outFile.WriteConfig(inFile.GetConfig());
	//outFile.WriteConfig(eventFileChain.GetCurrentConfiguration());

	//TrackExtrapolation trackExtrapolation(true, 0.1, 30400.);
	TrackExtrapolation trackExtrapolationStop(true, 0.1, 30400.);
	TrackExtrapolation trackExtrapolationStart(true, 0.1, 30400.);

	//trackExtrapolation.SetTarget(fFTOFConfig.GetTarget());
	unsigned int nEvent = 0;	


	//while (eventFileChain.Read(event) == eSuccess && nEvent < Ana::fMaxEv) {
	while (inFile.Read(event1) == eSuccess && nEvent < Ana::fMaxEv) {
		nEvent++;
		
		Event event(event1);

    	if (!(nEvent%50))
      		cout << " --->Processing event # " << nEvent << endl;

		//outFile.Write(event);	
		//continue;
		//cout << "radi-3" << endl;
		RecEvent& recEvent = event.GetRecEvent();
		//**********************************************************************************
		//**********************************Clear TOF Hits**********************************
		//**********************************************************************************	
		
		
			//--------------------------------------------------------------------
			//-------------------Unlink VertexTrack and TOFMass-------------------
			//--------------------------------------------------------------------
			
		for (std::list<rec::VertexTrack>::const_iterator iter = recEvent.Begin<rec::VertexTrack>();
           iter != recEvent.End<rec::VertexTrack>(); ++iter)
      	{
			
			const rec::VertexTrack& vtxTr = *iter;
			rec::VertexTrack& vtxTrack  = recEvent.Get(vtxTr.GetIndex());
			
			vector<evt::Index<TOFMass> > removeTOF;
			for (rec::TOFMassIndexIterator tofIter = vtxTrack.TOFMassBegin();
				   		tofIter != vtxTrack.TOFMassEnd(); ++tofIter)
			 {
			 	//cerr << "radi-31" << endl;
				rec::TOFMass& tof = recEvent.Get(*tofIter);
				if(tof.GetTOFId() != TOFConst::eTOFF) continue;
				//cerr << "radi-32" << endl;	
				removeTOF.push_back(tof.GetIndex());
				
				//
				//cerr << "radi-33" << endl;
			}
			
			for(int i = 0; i < removeTOF.size(); i++){
				
				rec::TOFMass& tof  = recEvent.Get(removeTOF.at(i));
				tof.Detach(event);
				//evt::IndexedObjectLinker::UnLinkDaughterFromParent(tof, vtxTrack);
				recEvent.Erase(tof.GetIndex());
			}		
		}		
		//cerr << "radi-2" << endl;
			//---------------------------------------------------------------------
			//----------------------------Erase TOFMass----------------------------
			//---------------------------------------------------------------------
		//std::list<rec::TOFMass>::const_iterator iter = recEvent.Begin<rec::TOFMass>();

		/*while(recEvent.Begin<rec::TOFMass>() != recEvent.End<rec::TOFMass>())
		{
				
				const rec::TOFMass& tofM = *iter;
				if(tofM.GetTOFId() != TOFConst::eTOFF) continue;
				iter = recEvent.Erase(tofM.GetIndex());
		}*/


		//***********************************************************************************
		//******************************Calculate S1 correction******************************
		//***********************************************************************************
		//cerr << "radi-1" << endl;
		
		double s1Corr = 0;
		double nDiv = 1.;
		
		const evt::raw::Trigger& trigg = event.GetRawEvent().GetBeam().GetTrigger();
		

		if (trigg.HasTDC(TriggerConst::eS1_2)) {
			if(trigg.GetTDC(TriggerConst::eS1_2) > 0 && trigg.GetTDC(TriggerConst::eS1_2) < 2000){
				s1Corr += fFTOFConfig.GetS1Res(1)*(trigg.GetTDC(TriggerConst::eS1_2) - fFTOFConfig.GetS1T0(1));
				nDiv++;
			}
		}
		if (trigg.HasTDC(TriggerConst::eS1_3)) {
			if(trigg.GetTDC(TriggerConst::eS1_3) > 0 && trigg.GetTDC(TriggerConst::eS1_3) < 2000){
				s1Corr += fFTOFConfig.GetS1Res(2)*(trigg.GetTDC(TriggerConst::eS1_3) - fFTOFConfig.GetS1T0(2));
				nDiv++;
			}
		}
		if (trigg.HasTDC(TriggerConst::eS1_4)) {
			if(trigg.GetTDC(TriggerConst::eS1_4) > 0 && trigg.GetTDC(TriggerConst::eS1_4) < 2000){
				s1Corr += fFTOFConfig.GetS1Res(3)*(trigg.GetTDC(TriggerConst::eS1_4) - fFTOFConfig.GetS1T0(3));
				nDiv++;
			}
		}
		
		s1Corr = s1Corr/nDiv - 11.;
		//s1Corr = 0;
		
		
		//*************************************************************************************
		//******************************Find active scintillators******************************
		//*************************************************************************************		
		const evt::RawEvent::TOFList& tofR = event.GetRawEvent().GetTOFs();
		
		vector<int> activeId;
		vector<double> tof1;
		vector<double> tof2;
		//cout << "radi0" << endl;
		bool badTOF = false;
		for(int i = 0; i < tofR.size(); i++){
			if(tofR.at(i).GetId() != det::TOFConst::eTOFF)
				continue;
			
			int nx = tofR.at(i).GetNx();
			int ny = tofR.at(i).GetNy();
			
			if(nx != 80){
				cerr << __FUNCTION__ << ": Wrong number of scintillators." << endl;
				badTOF = true;
				break;
			}
			if(ny != 2){
				cerr << __FUNCTION__ << ": Wrong number of channels per scintillator." << endl;
				badTOF = true;
				break;
			}
			for(int j = 0; j < nx; j++){
				bool goodTDC1 = false;
				bool goodTDC2 = false;
				
				if(fFTOFConfig.IsActive(j+1, 1)){
					if( tofR.at(i).GetTDCData(j+1,1) > 0 && tofR.at(i).GetTDCData(j+1,1) < 4000){
						goodTDC1 = true;
					}
				}
				else{
					goodTDC1 = true;
				}
				
				
				if(fFTOFConfig.IsActive(j+1, 2)){
					if( tofR.at(i).GetTDCData(j+1,2) > 0 && tofR.at(i).GetTDCData(j+1,2) < 4000){
						goodTDC2 = true;
					}
				}
				else{
					goodTDC2 = true;
				}

				if(goodTDC1 && goodTDC2){
					//if(j+1 == 47) cout << "radiB" << endl;
					activeId.push_back(j+1);
					if(fFTOFConfig.IsActive(j+1, 1)) tof1.push_back(tofR.at(i).GetTDCData(j+1,1)*fFTOFConfig.GetTDCRes() - fFTOFConfig.GetTopPMTT0(j+1) - s1Corr);
					else tof1.push_back(0);
					
					if(fFTOFConfig.IsActive(j+1, 2)) tof2.push_back(tofR.at(i).GetTDCData(j+1,2)*fFTOFConfig.GetTDCRes() - fFTOFConfig.GetBottomPMTT0(j+1) - s1Corr);
					else tof2.push_back(0);					
				}
			}
		}	
		
	
		if(badTOF) continue;	

		//*************************************************************************************
		//********************************Match Tracks and Hits********************************
		//*************************************************************************************	
		//cout << "radi1" << endl;
		vector<int> trackId;
		vector<int> scintId1;
		vector<int> scintId2;
		vector<double> tofMass1;
		vector<double> tofMass2;
		vector<double> tofVal1;
		vector<double> tofVal2;
		vector<double> lenVal1;
		vector<double> lenVal2;
		vector<double> xTOF1;
		vector<double> yTOF1;
		vector<double> zTOF1;
		vector<double> xTOF2;
		vector<double> yTOF2;
		vector<double> zTOF2;
		int nTracks = 0;		
		for (std::list<rec::Track>::const_iterator iterTr = recEvent.Begin<rec::Track>();
           iterTr != recEvent.End<rec::Track>(); ++iterTr)
      	{
			
			const Track& tr = *iterTr;	
			const Track& track = recEvent.Get(tr.GetIndex());	
			nTracks++;
			if(track.GetStatus() != 0) continue;
			if(track.GetNumberOfClusters(TrackConst::eVTPC1) + track.GetNumberOfClusters(TrackConst::eVTPC2) +track.GetNumberOfClusters(TrackConst::eGTPC) == 0) continue;
			if(track.GetNumberOfClusters(TrackConst::eVTPC1) + track.GetNumberOfClusters(TrackConst::eVTPC2) == 0){
				if(track.GetNumberOfClusters(TrackConst::eMTPC) == 0) continue;
			}
			double p = track.GetMomentum().GetMag();
			if(p < 0.2) continue;
			double length = 0;
			
			trackExtrapolationStop.SetTrackParams(track);
			trackExtrapolationStart.SetTrackParams(track);	
			
			
			trackExtrapolationStart.Extrapolate(-612.4);
			
			TrackPar &trackPar2 = trackExtrapolationStop.GetStopTrackParam();
			
			vector<TPCCluster> fClusters;
			//cerr << "step1" << endl;
			ClusterIndexIterator it;
			double zp = track.GetMomentumPoint().GetZ();
			for(it = track.ClustersBegin(); it!= track.ClustersEnd(); it++){
				 
				const Cluster clust= recEvent.Get(*it);
				const Point& pos = clust.GetPosition();

				if(zp > pos.GetZ()-1) continue;

				TPCCluster temp(pos.GetX(), pos.GetY(), pos.GetZ(), clust.GetPositionUncertainty( evt::rec::ClusterConst::eX), clust.GetPositionUncertainty( evt::rec::ClusterConst::eY), clust.GetTPCId());
				fClusters.push_back(temp);
			}
			

			for(int i = 0; i < fClusters.size(); i++){
				trackExtrapolationStop.DoKalmanStep(fClusters.at(i));

			}

			trackExtrapolationStop.Extrapolate(fFTOFConfig.GetFTOFZ()-1.5);
			
			double pos1[3] = {trackPar2.GetPar(0), trackPar2.GetPar(1), trackPar2.GetZ()};
			length = trackExtrapolationStart.GetStopTrackParam().GetLength()+trackPar2.GetLength();

			trackExtrapolationStop.Extrapolate(fFTOFConfig.GetFTOFZ()+1.5);
			double pos2[3] = {trackPar2.GetPar(0), trackPar2.GetPar(1), trackPar2.GetZ()};

			
			double mTOF2[2] = {-99, -99};
			int scints[2] = {-99, -99};
			double tofTemp[2] = {0, 0};
			double lenTemp[2] = {-99, -99};
			for(int i = 0; i < activeId.size(); i++){
				
				if(activeId.at(i)%2){
					if(TMath::Abs(fFTOFConfig.GetScintillatorXPosition(activeId.at(i)) - pos2[0]) < 6){
						double tof[2] = {0,0};
						
						double nh = 0;
						if(fFTOFConfig.IsActive(activeId.at(i), 1)){
							tof[0] = tof1.at(i)-fFTOFConfig.GetCInv()*(60-pos2[1]);
						}
						else if(fFTOFConfig.IsActive(activeId.at(i), 2)){
							tof[0] = tof2.at(i)-fFTOFConfig.GetCInv()*(60+pos2[1]);
						}		
						if(fFTOFConfig.IsActive(activeId.at(i), 2)){
							tof[1] = tof2.at(i)-fFTOFConfig.GetCInv()*(60+pos2[1]);
						}
						else if(fFTOFConfig.IsActive(activeId.at(i), 1)){
							tof[1] = tof1.at(i)-fFTOFConfig.GetCInv()*(60-pos2[1]);
						}
						//k++;
						//cout << tof[0] << " " << tof[1] << endl;						
						if(TMath::Abs(tof[0]-tof[1]) < fFTOFConfig.GetTOFCut()){
							tofTemp[0] = (tof[0]+tof[1])/2;
							lenTemp[0] = length + sqrt((pos1[0]-pos2[0])*(pos1[0]-pos2[0])+(pos1[1]-pos2[1])*(pos1[1]-pos2[1])+(pos1[2]-pos2[2])*(pos1[2]-pos2[2]));
							mTOF2[0] = p*p*(0.0299792458*0.0299792458*tofTemp[0]*tofTemp[0]/(lenTemp[0]*lenTemp[0]) -1);
							
							scints[0] = activeId.at(i);
							
						}
						
						if(i == activeId.size() -1) continue;
						if(TMath::Abs(fFTOFConfig.GetScintillatorXPosition(activeId.at(i+1)) - pos1[0]) < 6){
							nh = 0;
							tof[0] = 0;
							tof[1] = 0;
							if(fFTOFConfig.IsActive(activeId.at(i+1), 1)){
								tof[0] = tof1.at(i+1)-fFTOFConfig.GetCInv()*(60-pos1[1]);
							}
							else if(fFTOFConfig.IsActive(activeId.at(i+1), 2)){
								tof[0] = tof2.at(i+1)-fFTOFConfig.GetCInv()*(60+pos1[1]);
							}
							if(fFTOFConfig.IsActive(activeId.at(i+1), 2)){
								tof[1] = tof2.at(i+1)-fFTOFConfig.GetCInv()*(60+pos1[1]);
							}
							else if(fFTOFConfig.IsActive(activeId.at(i+1), 1)){
								tof[1] = tof1.at(i+1)-fFTOFConfig.GetCInv()*(60-pos1[1]);
							}
							//k++;
							if(TMath::Abs(tof[0]-tof[1]) < fFTOFConfig.GetTOFCut()){
								lenTemp[1] = length;
								tofTemp[1] = (tof[0]+tof[1])/2;
								mTOF2[1] = p*p*(0.0299792458*0.0299792458*tofTemp[1]*tofTemp[1]/(length*length) -1);
								
								scints[1] = activeId.at(i+1);
								
							}
						}
						break;
					}
				
				}
				else{
					if(TMath::Abs(fFTOFConfig.GetScintillatorXPosition(activeId.at(i)) - pos1[0]) < 6){
						double tof[2] = {0,0};
						double nh = 0;
						if(fFTOFConfig.IsActive(activeId.at(i), 1)){
							tof[0] = tof1.at(i)-fFTOFConfig.GetCInv()*(60-pos1[1]);
						}
						else{
							tof[0] = tof2.at(i)-fFTOFConfig.GetCInv()*(60+pos1[1]);
						}
						if(fFTOFConfig.IsActive(activeId.at(i), 2)){
							tof[1] = tof2.at(i)-fFTOFConfig.GetCInv()*(60+pos1[1]);
						}
						else{
							tof[1] = tof1.at(i)-fFTOFConfig.GetCInv()*(60-pos1[1]);
						}	
						//k++;				
						if(TMath::Abs(tof[0]-tof[1]) < fFTOFConfig.GetTOFCut()){
						
							tofTemp[0] = (tof[0]+tof[1])/2;
							mTOF2[0] = p*p*(0.0299792458*0.0299792458*tofTemp[0]*tofTemp[0]/(length*length) -1);
							lenTemp[0] = length;
							
							scints[0] = activeId.at(i);
							
						}
						
						if(i == activeId.size() -1) continue;
						if(TMath::Abs(fFTOFConfig.GetScintillatorXPosition(activeId.at(i+1)) - pos2[0]) < 6){
							nh = 0;
							tof[0] = 0;
							tof[1] = 0;
							if(fFTOFConfig.IsActive(activeId.at(i+1), 1)){
								tof[0] = tof1.at(i+1)-fFTOFConfig.GetCInv()*(60-pos2[1]);
								//nh++;
							}
							else{
								tof[0] = tof2.at(i+1)-fFTOFConfig.GetCInv()*(60+pos2[1]);
							}

							if(fFTOFConfig.IsActive(activeId.at(i+1), 2)){
								tof[1] = tof2.at(i+1)-fFTOFConfig.GetCInv()*(60+pos2[1]);
							}
							else{
								tof[1] = tof1.at(i+1)-fFTOFConfig.GetCInv()*(60-pos2[1]);
							}
	
							//k++;
							if(TMath::Abs(tof[0]-tof[1]) < fFTOFConfig.GetTOFCut()){
								tofTemp[1] = (tof[0]+tof[1])/2;	
								//cout << tof[0] << endl;
								lenTemp[1] = length + sqrt((pos1[0]-pos2[0])*(pos1[0]-pos2[0])+(pos1[1]-pos2[1])*(pos1[1]-pos2[1])+(pos1[2]-pos2[2])*(pos1[2]-pos2[2]));
								mTOF2[1] = p*p*(0.0299792458*0.0299792458*tofTemp[1]*tofTemp[1]/(lenTemp[1]*lenTemp[1])-1);						
								scints[1] = activeId.at(i+1);
								
								
							}
						}
						break;
					}				
				}
			}
			
			trackId.push_back(nTracks);
			scintId1.push_back(scints[0]);
			scintId2.push_back(scints[1]);
			tofMass1.push_back(mTOF2[0]);
			tofMass2.push_back(mTOF2[1]);
			tofVal1.push_back(tofTemp[0]);
			tofVal2.push_back(tofTemp[1]);
			lenVal1.push_back(lenTemp[0]);
			lenVal2.push_back(lenTemp[1]);
			if(scints[0] % 2){
				xTOF1.push_back(pos2[0]);
				yTOF1.push_back(pos2[1]);
				zTOF1.push_back(pos2[2]);	
				xTOF2.push_back(pos1[0]);
				yTOF2.push_back(pos1[1]);
				zTOF2.push_back(pos1[2]);				
			}
			else{
				xTOF1.push_back(pos1[0]);
				yTOF1.push_back(pos1[1]);
				zTOF1.push_back(pos1[2]);	
				xTOF2.push_back(pos2[0]);
				yTOF2.push_back(pos2[1]);
				zTOF2.push_back(pos2[2]);					
			}
		
		}
		
		//cout << "radi2" << endl;
		//************************************************************************************
		//*********************************Remove Double Hits*********************************
		//************************************************************************************	
		
		for(int i = 0; i < trackId.size(); i++){
			
			double s[2] = {scintId1.at(i), scintId2.at(i)};
			for(int j = i+1; j < trackId.size(); j++){
				if(scintId1.at(j) == s[0]){
					scintId1.at(i) = -99;
					scintId1.at(j) = -99;
				}
				else if(scintId1.at(j) == s[1]){
					scintId2.at(i) = -99;
					scintId1.at(j) = -99;				
				}
				
				if(scintId2.at(j) == s[0]){
					scintId1.at(i) = -99;
					scintId2.at(j) = -99;
				}
				else if(scintId1.at(j) == s[1]){
					scintId2.at(i) = -99;
					scintId2.at(j) = -99;				
				}
			}
		}
		
		//*************************************************************************************
		//*********************************Fill TOF Structures*********************************
		//*************************************************************************************	

		int trIndex = 0;
		//cout << "radi3" << endl;
		nTracks = 0;
		for (std::list<rec::Track>::const_iterator iterTr = recEvent.Begin<rec::Track>();
           iterTr != recEvent.End<rec::Track>(); ++iterTr)
      	{
      		//cout << "radi31" << endl;
      		nTracks++;
      		if(trIndex >= trackId.size()) break;
      		
      		if(trackId.at(trIndex) != nTracks) continue;
      		
      		trIndex++;
      		const Track& track = *iterTr;	
      		
      		if(track.GetNumberOfVertexTracks() == 0) continue; 
      		
      		//cout << "radi32a" << endl;
      		//cout << "radi1" << endl;
      		if(scintId1.at(trIndex-1) < 0 && scintId2.at(trIndex-1) < 0) continue;
			
      		//cout << "radi32" << endl;
      		//int nv = 0;

      		
      		if(scintId1.at(trIndex-1) > 0){
      			rec::TOFMass& tofTemp1 = recEvent.Make<rec::TOFMass>();
	 				
		  		//if(scintId1.at(trIndex-1) == 47) cout << "radi" << endl;
				for(evt::rec::VertexTrackIndexIterator vtxIter = track.VertexTracksBegin(); vtxIter != track.VertexTracksEnd(); vtxIter++){
					evt::rec::VertexTrack &vtxRecTr = recEvent.Get(*vtxIter);
					//if(vtxRecTr.GetStatus() != 0) continue;
					//cout << "BEFORE " <<  vtxRecTr.GetNumberOfTOFMasses() << endl;
		  			evt::IndexedObjectLinker::LinkDaughterToParent(tofTemp1, vtxRecTr);

		  			nTOF++;
		  			//cout << "AFTER "  << vtxRecTr.GetNumberOfTOFMasses() << endl;
					break;
				} 
		  		tofTemp1.SetTOFId(TOFConst::eTOFF);
		  		utl::Point point1(xTOF1.at(trIndex-1), yTOF1.at(trIndex-1), zTOF1.at(trIndex-1));
		  		tofTemp1.SetPosition(point1);
		  		tofTemp1.SetPathLength(lenVal1.at(trIndex-1));
		  		tofTemp1.SetTimeOfFlight(tofVal1.at(trIndex-1));
		  		tofTemp1.SetScintillatorId(scintId1.at(trIndex-1));
		  		tofTemp1.SetStatus(0);
		  		tofTemp1.SetSquaredMass(tofMass1.at(trIndex-1), 0);     	       		
      		}	
      		
      		if(scintId2.at(trIndex-1) > 0){
      			//if(scintId2.at(trIndex-1) == 47) cout << "radi" << endl;
				rec::TOFMass& tofTemp2 = recEvent.Make<rec::TOFMass>();
				for(evt::rec::VertexTrackIndexIterator vtxIter = track.VertexTracksBegin(); vtxIter != track.VertexTracksEnd(); vtxIter++){
					evt::rec::VertexTrack &vtxRecTr = recEvent.Get(*vtxIter);
					//if(vtxRecTr.GetStatus() != 0) continue;
					//cout << "BEFORE " <<  vtxRecTr.GetNumberOfTOFMasses() << endl;
		  			evt::IndexedObjectLinker::LinkDaughterToParent(tofTemp2, vtxRecTr);

		  			nTOF++;
		  			//cout << "AFTER "  << vtxRecTr.GetNumberOfTOFMasses() << endl;
					break;
				}
				
		  		tofTemp2.SetTOFId(TOFConst::eTOFF);
		  		utl::Point point2(xTOF2.at(trIndex-1), yTOF2.at(trIndex-1), zTOF2.at(trIndex-1));
		  		tofTemp2.SetPosition(point2);
		  		tofTemp2.SetPathLength(lenVal2.at(trIndex-1));
		  		tofTemp2.SetTimeOfFlight(tofVal2.at(trIndex-1));
		  		tofTemp2.SetScintillatorId(scintId2.at(trIndex-1));
		  		tofTemp2.SetStatus(0);
		  		tofTemp2.SetSquaredMass(tofMass2.at(trIndex-1), 0);      		
      		         		
      		}	
      		//cout << " *******************************" << endl;  
      		   		      		
    	
      		//cout << nv << endl;
      	}	
		//outFile.Cd();
		
		for (std::list<rec::TOFMass>::const_iterator iterT = recEvent.Begin<rec::TOFMass>();
           iterT != recEvent.End<rec::TOFMass>(); ++iterT)
      	{	
      		
      		rec::TOFMass tm = *iterT;
      		if(tm.GetTOFId() == TOFConst::eTOFF) k++;
      			//cout << k << " " << tm.GetPosition().GetZ() << endl;
      	}
      	outFile.Cd();
		outFile.Write(event);	
	}
	cout << k << endl;
	inFile.Close();
	outFile.Close();
	cout << nTOF << endl;
}
