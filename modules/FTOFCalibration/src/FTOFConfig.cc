#include <stdlib.h>

#include "FTOFConfig.h"
#include <fstream>
#include <fwk/CentralConfig.h>
#include <utl/Branch.h>
#include <utl/BranchIterator.h>
#include "CConfig.h"

using namespace std;
using namespace fwk;
using namespace utl; 

void FTOFConfig::Read(){


	/*if(cConfig == NULL){
		cout << __FUNCTION__ << ": Central config not initialized! Exiting..." << endl;
		exit (EXIT_FAILURE);
	}*/
	
	for(int i = 0; i < 80; i++){
		fXPos.push_back(0);
		fT0Top.push_back(0);
		fT0Bottom.push_back(0);
		fActive1.push_back(0);
		fActive2.push_back(0);
	}
	Branch FTOFConf = cConfig.GetTopBranch("FTOFCalib");

	if(FTOFConf.GetName() != "FTOFCalib"){
		cout << __FUNCTION__ << ": Wrong FTOF config file! Top branch name doesn't match! Please check your configuration files! Exiting..." << endl;
		exit (EXIT_FAILURE);
	}

	for(BranchIterator it = FTOFConf.ChildrenBegin(); it != FTOFConf.ChildrenEnd(); ++it){
		Branch child = *it;
		if(child.GetName() == "data"){
			child.GetData(fInputData);
			if(fInputData.at(0)=='$'){
				string run = fInputData.substr(1, fInputData.size()-1);
				//cout << run << endl;
				fInputData = getenv(run.c_str());
			}
			

		}

		else if(child.GetName() == "outputDir"){
			child.GetData(fOutputDir);
			if(fOutputDir.at(0)=='$'){
				string run = fOutputDir.substr(1, fOutputDir.size()-1);
				//cout << run << endl;
				fOutputDir = getenv(run.c_str());
			}
		}
		else if(child.GetName() == "runNumbers"){
			child.GetData(fRunNumbers);

		}
		else if(child.GetName() == "TOFRes"){
			child.GetData(fTOFRes);
		}
		else if(child.GetName() == "zFTOF"){
			child.GetData(fFTOFZ);

		}	
		else if(child.GetName() == "target"){
			vector<double> position;
			position.push_back(0);
			position.push_back(0);
			position.push_back(-657.40);
			
			double tiltX = 0;
			double tiltY = 0;

			ETargetType type = eLong;
			int iType = 0;
			string name = "long";
			double length = 90;
			double radius = 1.3;

			for(BranchIterator t = child.ChildrenBegin(); t != child.ChildrenEnd(); ++t){
				Branch targetChild = *t;

				if(targetChild.GetName() == "name"){
					targetChild.GetData(name);
				}
				else if(targetChild.GetName() == "type"){

					targetChild.GetData(iType);
				}
				else if(targetChild.GetName() == "position"){
					position.clear();
					targetChild.GetData(position);
				}
				else if(targetChild.GetName() == "radius"){
					targetChild.GetData(radius);
				}
				else if(targetChild.GetName() == "tiltX"){
					targetChild.GetData(tiltX);
				}
				else if(targetChild.GetName() == "tiltY"){
					targetChild.GetData(tiltY);
				}
			}

			if(iType == 1) type = eThin;
			if(iType == 2) type = eNominalBeamline;
			SetTarget(name, type, length, radius, position.at(0), position.at(1), position.at(2), tiltX, tiltY);
			
		}
		
		else if(child.GetName() == "scintillator"){
			double xPos = 0;
			double t01 = 0;
			double t02 = 0;
			int id = -999;
			bool act1 = false;
			bool act2 = false;
			for(BranchIterator t = child.ChildrenBegin(); t != child.ChildrenEnd(); ++t){
				Branch targetChild = *t;
				if(targetChild.GetName() == "id"){
					targetChild.GetData(id);
				}
				if(targetChild.GetName() == "activeTop"){
					targetChild.GetData(act1);
				}
				if(targetChild.GetName() == "activeBottom"){
					targetChild.GetData(act2);
				}
				else if(targetChild.GetName() == "xPos"){
					targetChild.GetData(xPos);
				}
				else if(targetChild.GetName() == "t0Top"){
					targetChild.GetData(t01);
				}
				else if(targetChild.GetName() == "t0Bottom"){
					targetChild.GetData(t02);
				}
			}

			if(id < 1 || id > 80){
				cerr << __FUNCTION__ << ": Wrong scintillator id. Exiting..." << endl;
				exit(EXIT_FAILURE);
			}
			fXPos.at(id-1) = xPos;
			fT0Top.at(id-1) = t01;
			fT0Bottom.at(id-1) = t02;
			fActive1.at(id-1) = act1;
			fActive2.at(id-1) = act2;
			
		}
		else if(child.GetName() == "S1"){

			for(BranchIterator t = child.ChildrenBegin(); t != child.ChildrenEnd(); ++t){
				Branch targetChild = *t;
				if(targetChild.GetName() == "S1T0"){
					targetChild.GetData(fS1T0);
				}
				else if(targetChild.GetName() == "S1Res"){
					targetChild.GetData(fS1Res);
				}
			}
			
		}
		else if(child.GetName() == "cInv"){
			child.GetData(fCInv);
		}	
		else if(child.GetName() == "TDCRes"){
			child.GetData(fTDCRes);
		}	
		else if(child.GetName() == "TOFCut"){
			child.GetData(fTOFCut);
		}	
	}
	
}

//*********************************************************************************************
void FTOFConfig::SetTarget(string name, ETargetType type, double length, double radius, double positionX, double positionY, double positionZ, double tilt1, double tilt2){

	FTOFConfig::fTarget.SetName(name);
	FTOFConfig::fTarget.SetType(type);
	FTOFConfig::fTarget.SetLength(length);
	FTOFConfig::fTarget.SetRadius(radius);
	FTOFConfig::fTarget.SetUpstreamPosition(positionX, positionY, positionZ);
	FTOFConfig::fTarget.SetTiltXZ(tilt1);
	FTOFConfig::fTarget.SetTiltYZ(tilt2);

}


//*********************************************************************************************
void FTOFConfig::ReadInputList(string inputData){
	fInputData = inputData;
	ReadInputList();
}

//*********************************************************************************************
void FTOFConfig::ReadInputList(){



	if (FTOFConfig::fInputData != " " ){
		ifstream ifs;
		string line;
		ifs.open(FTOFConfig::fInputData.c_str());
		
		if (!ifs.is_open()){
			cerr << "[ReadInputListFile] Could not open infile. Exiting..." << endl;			
			exit (101);
		}
    
		while (getline(ifs, line)){
			if(line.at(0)=='#')
				continue;
			FTOFConfig::fJob.push_back(line);
		}
		ifs.close();	
	
	}
	else cout << "WARNING: Empty file list! Jobs not created!" << endl;


}


//*********************************************************************************************
vector<string>& FTOFConfig::GetJob(){

	if(FTOFConfig::fJob.size() == 0)
		FTOFConfig::ReadInputList();

	return FTOFConfig::fJob;

}

//*********************************************************************************************

bool FTOFConfig::SkipRun(int runNumber){

	div_t res = div(fRunNumbers.size(),2);
	bool good = false;
	for(unsigned int i = 0; i < res.quot; i++){
		if(runNumber >= fRunNumbers.at(2*i) && runNumber < fRunNumbers.at(2*i+1)){
			good = true;
			break;
		}
	}
	return good;
}



double FTOFConfig::GetScintillatorXPosition(int scintId){
	if(scintId < 1 || scintId > 80){
		cerr << __FUNCTION__ << ": Wrong scintillator id. Exiting..." << endl;
		exit(EXIT_FAILURE);
	}
	
	return fXPos.at(scintId-1);
}

double FTOFConfig::GetTopPMTT0(int scintId){
	if(scintId < 1 || scintId > 80){
		cerr << __FUNCTION__ << ": Wrong scintillator id. Exiting..." << endl;
		exit(EXIT_FAILURE);
	}
	
	return fT0Top.at(scintId-1);
}


double FTOFConfig::GetBottomPMTT0(int scintId){
	if(scintId < 1 || scintId > 80){
		cerr << __FUNCTION__ << ": Wrong scintillator id. Exiting..." << endl;
		exit(EXIT_FAILURE);
	}
	
	return fT0Bottom.at(scintId-1);
}


double FTOFConfig::GetS1T0(int pmtId){
	if(pmtId < 1 || pmtId > 3){
		cerr << __FUNCTION__ << ": Wrong S1 PMT id. Exiting..." << endl;
		exit(EXIT_FAILURE);
	}
	
	return fS1T0.at(pmtId-1);
}


double FTOFConfig::GetS1Res(int pmtId){
	if(pmtId < 1 || pmtId > 3){
		cerr << __FUNCTION__ << "Wrong S1 PMT id. Exiting..." << endl;
		exit(EXIT_FAILURE);
	}
	
	return fS1Res.at(pmtId-1);
}

bool FTOFConfig::IsActive(int scintId, int pmtId){

	if(scintId < 1 || scintId > 80){
		cerr << __FUNCTION__ << ": Wrong scintillator id. Exiting..." << endl;
		exit(EXIT_FAILURE);
	}
	
	
	if(pmtId < 1 || pmtId > 2){
		cerr << __FUNCTION__ << "Wrong S1 PMT id. Exiting..." << endl;
		exit(EXIT_FAILURE);
	}
	
	
	if(pmtId == 1){
		return fActive1.at(scintId-1);
	}
	else{
		return fActive2.at(scintId-1);
	}
	
}
