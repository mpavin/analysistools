#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <cstdlib>
#include <stdexcept>
#include <TTree.h>
#include <TFile.h>
#include <cstdlib>
#include <evt/Event.h>
#include <evt/EventHeader.h>
#include <io/EventFileChain.h>
#include <det/TriggerConst.h>
#include <det/BPDConst.h>
#include <evt/RawEvent.h>
#include <evt/SimEvent.h>
#include "StrTools.h"
#include <boost/format.hpp>
#include "Ana.h"
#include "TrackExtrapolation.h"
#include "evt/RawEvent.h"
#include "evt/raw/TOF.h"
#include <TMath.h>
#include <evt/IndexedObjectLinker.h>
#include <io/ShineEventFile.h>
#include "TOFResolution.h"
#include <TRandom3.h>

using namespace std;
using namespace io;
using namespace evt;
using namespace evt::rec;
using namespace evt::sim;
using namespace evt::raw;
using namespace det;

using namespace StrTools;


void Ana::UpdateTOFMC()
{

	//EventFileChain eventFileChain(fFTOFConfig.GetJob());
	
	Event event1;
	int nTOF = 0;
	string name = fFTOFConfig.GetOutputDir() + "/shoe.root";
	ShineEventFile inFile;	
	inFile.Open(fFTOFConfig.GetJob().at(0), io::eRead);

	TH1D pires("pires", "", 200, -0.15, 0.15);
	TH1D elres("elres", "", 200, -0.15, 0.15);
	
	ShineEventFile outFile;	
	outFile.Open(name, io::eNew);	
	outFile.WriteConfig(inFile.GetConfig());

	vector<double> ranges;
	for(int i = 0; i < 8; i++){
		ranges.push_back(0);
	}
	

	TOFResolution tofRes(fFTOFConfig.GetTOFRes(), ranges);

	unsigned int nEvent = 0;	
	
	TRandom3 ran(0);
	
	while (inFile.Read(event1) == eSuccess && nEvent < Ana::fMaxEv) {
		nEvent++;
		
		Event event(event1);

    	if (!(nEvent%1000))
      		cout << " --->Processing event # " << nEvent << endl;


		SimEvent& simEvent = event.GetSimEvent();
		RecEvent& recEvent = event.GetRecEvent();


		for (std::list<rec::Track>::const_iterator iter = recEvent.Begin<rec::Track>();
           iter != recEvent.End<rec::Track>(); ++iter)
      	{

			
			const rec::Track& track = *iter;
			
			
			if(track.GetNumberOfSimVertexTracks() == 0) continue;
			if(track.GetFirstPointOnTrack().GetZ()>50) continue;
			vector<rec::TOFMassIndexIterator> tofIds;
			
			for (rec::VertexTrackIndexIterator recvtx = track.VertexTracksBegin();
				recvtx != track.VertexTracksEnd(); ++recvtx)
			{	
				rec::VertexTrack& recvtxTrack = recEvent.Get(*recvtx);
				
				
				
				for (rec::TOFMassIndexIterator tofiter = recvtxTrack.TOFMassBegin();
						   		tofiter != recvtxTrack.TOFMassEnd(); ++tofiter)
				{
					const rec::TOFMass& tof = recEvent.Get(*tofiter);
					if(tof.GetTOFId() != TOFConst::eTOFF) continue;
					tofIds.push_back(tofiter);
				
				}
					
				  	
				

			}
			if(tofIds.size() == 0) continue;
			int pdg = 0;
			
			/*if(track.GetNumberOfSimVertexTracks() >1) {
				cout << "********************************" << endl;
				cout << track.GetMomentumPoint().GetZ() << " " << track.GetMomentum().GetMag()<< endl;
			}*/
			for (sim::VertexTrackIndexIterator simvtx = track.SimVertexTracksBegin();
						  simvtx != track.SimVertexTracksEnd(); ++simvtx)
			{
				
				sim::VertexTrack& simvtxTrack = simEvent.Get(*simvtx);
				if(simvtxTrack.GetCharge()==0) continue;
				pdg = abs(simvtxTrack.GetParticleId());
				
				/*if(track.GetNumberOfSimVertexTracks() >1 && simvtxTrack.HasStartVertex()){ 
					sim::Vertex ver = simEvent.Get(simvtxTrack.GetStartVertexIndex());
					cout << ver.GetPosition().GetZ() << " " << simvtxTrack.GetParticleId() << " ";
				
				}*/
				break;
			}
			//if(track.GetNumberOfSimVertexTracks() >1) cout << endl;
			
			if(pdg == 0) continue;
			
			double mass2 = -9999;
			if(pdg == 11){
				mass2 = 0.000511*0.000511 + ran.Gaus(0,tofRes.GetElectronWidth(track.GetMomentum().GetMag()));
			}
			else if(pdg == 13){
				mass2 = 0.105658*0.105658 + ran.Gaus(0,tofRes.GetPionWidth(track.GetMomentum().GetMag()));
			}
			else if(pdg == 211){
				mass2 = 0.139570*0.139570 + ran.Gaus(0,tofRes.GetPionWidth(track.GetMomentum().GetMag()));
			}
			else if(pdg == 321){
				mass2 = 0.493667*0.493667 + ran.Gaus(0,tofRes.GetKaonWidth(track.GetMomentum().GetMag()));
			}
			else if(pdg == 2212){
				mass2 = 0.938272*0.938272 + ran.Gaus(0,tofRes.GetProtonWidth(track.GetMomentum().GetMag()));
			}	
			
			if(mass2 < -9000) continue;
			for(int i = 0; i<tofIds.size(); i++){
				rec::TOFMass& tof = recEvent.Get(*(tofIds.at(i)));
				
				//if(track.GetNumberOfSimVertexTracks() >1) cout << tof.GetSquaredMass() << endl;
				tof.SetSquaredMass(mass2, 0);
			}		
			
		}

      	outFile.Cd();
		outFile.Write(event);
	}

	inFile.Close();
	outFile.Close();
	
	TFile f("Results/check.root", "RECREATE");
	f.cd();
	pires.Write();
	elres.Write();

}
