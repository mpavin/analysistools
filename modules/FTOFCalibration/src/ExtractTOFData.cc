/*!
* \file
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <cstdlib>
#include <stdexcept>
#include <TTree.h>
#include <TFile.h>
#include <cstdlib>
#include <evt/Event.h>
#include <evt/EventHeader.h>
#include <io/EventFileChain.h>
#include <det/TriggerConst.h>
#include <det/BPDConst.h>
#include <evt/RawEvent.h>
#include <evt/SimEvent.h>
#include "StrTools.h"
#include <boost/format.hpp>
#include "Ana.h"
#include "TrackExtrapolation.h"
#include "evt/RawEvent.h"
#include "evt/raw/TOF.h"
#include <TMath.h>
using namespace std;
using namespace io;
using namespace evt;
using namespace evt::rec;
using namespace evt::sim;
using namespace evt::raw;
using namespace det;
using namespace boost;
using namespace StrTools;
/*!
 *	\fn 
 *	Function Run reads SHOE files. For this class EventFileChain is used. It needs vector containing paths to the SHOE files.
 *	This vector is obtained from the DataAnaConfig class.
*/
void Ana::ExtractTOFData(){

	//string spectrumFileName = fDataAnaConfig.GetOutputDir() + "/spectrum_" + GetTimeString() + ".root";
	//string cutFile = fDataAnaConfig.GetOutputDir() + "/cuts_" + GetTimeString() + ".root";
	string ftofFileName = fFTOFConfig.GetOutputDir() + "/ftof.root";
	string cutFile = fFTOFConfig.GetOutputDir() + "/cuts.root";
	
	EventFileChain eventFileChain(fFTOFConfig.GetJob());
	Event event;

	

	EventSelection evSel;
	TrackSelection trSel;

	TrackExtrapolation trackExtrapolation(true, 0.1, 30400.);
	TrackExtrapolation trackExtrapolationStop(true, 0.1, 30400.);
	TrackExtrapolation trackExtrapolationLen(true, 0.1, 30400.);
	TargetAna target;
	trackExtrapolation.SetTarget(fFTOFConfig.GetTarget());
	unsigned int nEvent = 0;
	
	unsigned int runNumber = 0;
	unsigned int eventId = 0;
	vector<int> trackId;
	vector<int> vtxTrackId;
	unsigned int trigger = 0;
	
	BeamA beamD;
	vector<int> tdcS1;
	vector<int> qdcS1;
	vector<int> tdc;
	vector<int> qdc;
	
	vector<double> xStart;
	vector<double> yStart;
	vector<double> zStart;
	vector<double> xEStart;
	vector<double> yEStart;
	vector<double> distToTarg;
	
	vector<double> xTOF1;
	vector<double> yTOF1;
	vector<double> zTOF1;
	vector<double> xETOF1;
	vector<double> yETOF1;
	
	vector<double> xTOF2;
	vector<double> yTOF2;
	vector<double> zTOF2;
	vector<double> xETOF2;
	vector<double> yETOF2;
	
	vector<double> zTPCFirst;
	vector<double> zTPCLast;
	vector<double> zVert;
	vector<int> qD;
	vector<int> topology;
	vector<int> RST;
	
	vector<double> p;
	vector<double> pE;
	vector<double> length;
	
	vector<int> nVTPC;
	vector<int> nGTPC;
	vector<int> nMTPC;
	
	vector<double> dEdx;
	vector<double> m2TOF;
	vector<int> nTOF;
	vector<int> scintId;
	
	vector<int> vType;
	vector<double> theta;
	vector<double> phi;
	
	TFile *ftofFile = new TFile(ftofFileName.c_str(), "RECREATE");
	ftofFile->cd();
	
	fFTOF = new TTree("FTOF", "FTOF");
	fFTOF->SetAutoSave(-300000000);

	fFTOF->Branch("runNumber", &runNumber, "runNumber/I");
	fFTOF->Branch("eventId", &eventId, "eventId/I");
	fFTOF->Branch("trackId", "vector<int>", &trackId);
	fFTOF->Branch("vtxTrackId", "vector<int>", &vtxTrackId);
	fFTOF->Branch("eventId", &eventId, "eventId/I");
	fFTOF->Branch("trigger", &trigger, "trigger/I");
	
	fFTOF->Branch("Beam", &(beamD.aX), "aX/D:bX:aY:bY");
	
	fFTOF->Branch("tdcS1", "vector<int>", &tdcS1);
	fFTOF->Branch("qdcS1", "vector<int>", &qdcS1);
	fFTOF->Branch("tdc", "vector<int>", &tdc);
	fFTOF->Branch("qdc", "vector<int>", &qdc);
	

	fFTOF->Branch("xStart", "vector<double>", &xStart);
	fFTOF->Branch("yStart", "vector<double>", &yStart);
	fFTOF->Branch("zStart", "vector<double>", &zStart);
	fFTOF->Branch("xEStart", "vector<double>", &xEStart);
	fFTOF->Branch("yEStart", "vector<double>", &yEStart);
	fFTOF->Branch("distToTarg", "vector<double>", &distToTarg);
	
	fFTOF->Branch("xTOF1", "vector<double>", &xTOF1);
	fFTOF->Branch("yTOF1", "vector<double>", &yTOF1);
	fFTOF->Branch("zTOF1", "vector<double>", &zTOF1);
	fFTOF->Branch("xETOF1", "vector<double>", &xETOF1);
	fFTOF->Branch("yETOF1", "vector<double>", &yETOF1);
	fFTOF->Branch("xTOF2", "vector<double>", &xTOF2);
	fFTOF->Branch("yTOF2", "vector<double>", &yTOF2);
	fFTOF->Branch("zTOF2", "vector<double>", &zTOF2);
	fFTOF->Branch("xETOF2", "vector<double>", &xETOF2);
	fFTOF->Branch("yETOF2", "vector<double>", &yETOF2);
	fFTOF->Branch("theta", "vector<double>", &theta);
	fFTOF->Branch("phi", "vector<double>", &phi);
			
	fFTOF->Branch("zTPCFirst", "vector<double>", &zTPCFirst);
	fFTOF->Branch("zTPCLast", "vector<double>", &zTPCLast);
	fFTOF->Branch("nVTPC", "vector<int>", &nVTPC);
	fFTOF->Branch("nGTPC", "vector<int>", &nGTPC);
	fFTOF->Branch("nMTPC", "vector<int>", &nMTPC);
	fFTOF->Branch("zVert", "vector<double>", &zVert);
	fFTOF->Branch("topology", "vector<int>", &topology);
	fFTOF->Branch("RST", "vector<int>", &RST);
	fFTOF->Branch("p", "vector<double>", &p);
	fFTOF->Branch("pE", "vector<double>", &pE);
	fFTOF->Branch("length", "vector<double>", &length);
	fFTOF->Branch("q", "vector<int>", &qD);
	fFTOF->Branch("dEdx", "vector<double>", &dEdx);
	
	fFTOF->Branch("m2TOF", "vector<double>", &m2TOF);
	fFTOF->Branch("nTOF", "vector<int>", &nTOF);
	fFTOF->Branch("scintId", "vector<int>", &scintId);
	fFTOF->Branch("vType", "vector<int>", &vType);
	
	double zUpstream = fFTOFConfig.GetTarget().GetUpstreamPosition().GetZ()-45;
	target.SetRadius(0);
	target.SetLength(200.);

			double out[300][10];
			double chi1[300];
			double chi2[300];	
	while (eventFileChain.Read(event) == eSuccess && nEvent < Ana::fMaxEv) {
	//while (eventFileChain.Read(event) == eSuccess && nEvent < 2) {

	  //	  RawEvent& rawEvent = event.GetRawEvent();
	  //const evt::RawEvent::TOFList& tof = rawEvent.GetTOFs();
	  //cout << tof.size() << endl;
	  
	  //cout << tof.GetNy() << endl;
	  	tdcS1.clear();
	  	qdcS1.clear();
	  	tdc.clear();
	  	qdc.clear();
		zTPCFirst.clear();
		zTPCLast.clear();
		zVert.clear();	
		topology.clear();
		RST.clear();
		p.clear();
		pE.clear();
		qD.clear();
		length.clear();
		nVTPC.clear();
		nGTPC.clear();
		nMTPC.clear();
		dEdx.clear();
		nTOF.clear();
		m2TOF.clear();
		
		xStart.clear();
		yStart.clear();
		zStart.clear();
		xEStart.clear();
		yEStart.clear();
		distToTarg.clear();
		xTOF1.clear();
		yTOF1.clear();
		zTOF1.clear();
		xETOF1.clear();
		yETOF1.clear();
		
		xTOF2.clear();
		yTOF2.clear();
		zTOF2.clear();
		xETOF2.clear();
		yETOF2.clear();
		scintId.clear();
		vType.clear();
		theta.clear();
		phi.clear();
		trackId.clear();
		vtxTrackId.clear();
		
		bool skipRun = false;
		EventHeader &evHeader = event.GetEventHeader();
		eventId = evHeader.GetId();
		if(evHeader.GetRunNumber() != runNumber){
			runNumber = evHeader.GetRunNumber();
			skipRun = fFTOFConfig.SkipRun(runNumber);
		}

		if(skipRun) continue;

   		++nEvent;
    
    	if (!(nEvent%1000))
      		cout << " --->Processing event # " << nEvent << endl;


		if(evSel.IsEventGood(event) == false)
			continue;	
		
		trigger = 0;	
		const evt::raw::Trigger& trigg = event.GetRawEvent().GetBeam().GetTrigger();
		if(trigg.IsTrigger(TriggerConst::eT2, TriggerConst::eAll)) trigger+=1;
		if(trigg.IsTrigger(TriggerConst::eT3, TriggerConst::eAll)) trigger+=2;	
		
		//cout <<  trigg.GetTDC(TriggerConst::eS1_1) << " " << trigg.GetTDC(TriggerConst::eS1_2) << " " << trigg.GetTDC(TriggerConst::eS1_3) << " " << trigg.GetTDC(TriggerConst::eS1_4)<< endl;
		if (trigg.HasTDC(TriggerConst::eS1_1)) tdcS1.push_back(trigg.GetTDC(TriggerConst::eS1_1));
		else tdcS1.push_back(-99);
		if (trigg.HasTDC(TriggerConst::eS1_2)) tdcS1.push_back(trigg.GetTDC(TriggerConst::eS1_2));
		else tdcS1.push_back(-99);
		if (trigg.HasTDC(TriggerConst::eS1_3)) tdcS1.push_back(trigg.GetTDC(TriggerConst::eS1_3));
		else tdcS1.push_back(-99);
		if (trigg.HasTDC(TriggerConst::eS1_4)) tdcS1.push_back(trigg.GetTDC(TriggerConst::eS1_4));
		else tdcS1.push_back(-99);
		
		if (trigg.HasADC(TriggerConst::eS1)) qdcS1.push_back(trigg.GetADC(TriggerConst::eS1));
		else qdcS1.push_back(-99);
		if (trigg.HasADC(TriggerConst::eS1_1)) qdcS1.push_back(trigg.GetADC(TriggerConst::eS1_1));
		else qdcS1.push_back(-99);
		if (trigg.HasADC(TriggerConst::eS1_2)) qdcS1.push_back(trigg.GetADC(TriggerConst::eS1_2));
		else qdcS1.push_back(-99);
		if (trigg.HasADC(TriggerConst::eS1_3)) qdcS1.push_back(trigg.GetADC(TriggerConst::eS1_3));
		else qdcS1.push_back(-99);
		if (trigg.HasADC(TriggerConst::eS1_4)) qdcS1.push_back(trigg.GetADC(TriggerConst::eS1_4));
		else qdcS1.push_back(-99);
		//cout << "***********************" << endl;
		RawEvent& rawEvent = event.GetRawEvent();
		const evt::RawEvent::TOFList& tofR = rawEvent.GetTOFs();
		

		for(int i = 0; i < tofR.size(); i++){
			if(tofR.at(i).GetId() != det::TOFConst::eTOFF)
				continue;
			
			int nx = tofR.at(i).GetNx();
			int ny = tofR.at(i).GetNy();
			//cout << nx << " " << ny << endl;
			for(int j = 0; j < nx; j++){
				for(int k = 0; k < ny; k++){
					tdc.push_back(tofR.at(i).GetTDCData(j+1,k+1));
					qdc.push_back(tofR.at(i).GetQDCData(j+1,k+1));
				}			
			}
		}

		const RecEvent& recEvent = event.GetRecEvent();
		//cout << recEvent.Ge

		const Fitted2DLine lineX = recEvent.GetBeam().Get(BPDConst::eX);
    	const Fitted2DLine lineY = recEvent.GetBeam().Get(BPDConst::eY);
    		
		beamD.aX = lineX.GetSlope();
		beamD.bX = lineX.GetIntercept();
		beamD.aY = lineY.GetSlope();
		beamD.bY = lineY.GetIntercept();   		


		
		int ntracks = 0;	
		for (std::list<rec::Track>::const_iterator iter = recEvent.Begin<rec::Track>();
           iter != recEvent.End<rec::Track>(); ++iter)
      	{
			
			ntracks++;
			int nToF = 0;
			const Track& track = *iter;
			double tofMass = 0;
			bool hasToF = false;
			vector<int> tofSId;
			//double vtxLen = 0;
			AnaTrack anaTrack(trackExtrapolation);
			double vtxStart = 9999.;
			int vtxType = -999;
			
			if(track.GetNumberOfVertexTracks()< 1) 
				continue;
			
			anaTrack.SetTrack(track);
			//int kount = 0;
			
			int nvtxtracks = -99;
			int vtxtemp = 0;
			for(evt::rec::VertexTrackIndexIterator vtxIter = track.VertexTracksBegin(); vtxIter != track.VertexTracksEnd(); vtxIter++){		
			//evt::rec::VertexTrackIndexIterator vtxIter = track.VertexTracksBegin();
				const evt::rec::VertexTrack vtxRecTr = recEvent.Get(*vtxIter);
				
				if(vtxRecTr.GetTrackIndex() != track.GetIndex()) continue;
				vtxtemp++;	
				rec::Vertex startVtx = recEvent.Get(vtxRecTr.GetStartVertexIndex());
				if(vtxStart > startVtx.GetPosition().GetZ()){
					vtxStart = startVtx.GetPosition().GetZ();	
					vtxType = startVtx.GetType();
				}
				
				
				for (rec::TOFMassIndexIterator tofIter = vtxRecTr.TOFMassBegin();
				   		tofIter != vtxRecTr.TOFMassEnd(); ++tofIter)
			  	{
					const rec::TOFMass& tof = recEvent.Get(*tofIter);
					if(tof.GetTOFId() != TOFConst::eTOFF)
						continue;
				
					nvtxtracks = vtxtemp;
					nToF++;
					hasToF = true;	

					
					tofSId.push_back(tof.GetScintillatorId());
					tofMass += tof.GetSquaredMass();
				}
				tofMass = tofMass/nToF;
				std::sort (tofSId.begin(), tofSId.end());
			
				if(hasToF){
					anaTrack.SetToFMassSquared(tofMass);
					anaTrack.SetToFScintillatorId(tofSId.at(0));
				}
			
			}

			if(!trSel.IsTrackGood(anaTrack))
				continue;
			
			
			trackId.push_back(ntracks);
			vtxTrackId.push_back(nvtxtracks);
			//if(track.GetFirstPointOnTrack().GetX()*track.GetLastPointOnTrack().GetX() < 0) continue;
			
			//cout << vtxStart << endl;
			target.SetLength(track.GetFirstPointOnTrack().GetZ()-10- zUpstream);
			anaTrack.DoExtrapolation();
			
			
			nTOF.push_back(nToF);
			m2TOF.push_back(tofMass);
			scintId.push_back(anaTrack.GetToFScintillatorId());
			dEdx.push_back(track.GetEnergyDeposit(eAll));
			p.push_back(anaTrack.GetMomentum().GetMag());
			pE.push_back(trackExtrapolationStop.GetStartTrackParam().GetStd(4)/(trackExtrapolationStop.GetStartTrackParam().GetPar(4)*
			trackExtrapolationStop.GetStartTrackParam().GetPar(4)));
			topology.push_back(anaTrack.GetTopology());
			qD.push_back(track.GetCharge());
			zTPCFirst.push_back(track.GetFirstPointOnTrack().GetZ());
			zTPCLast.push_back(track.GetLastPointOnTrack().GetZ());		
			nVTPC.push_back(anaTrack.GetNumberOfClusters().VTPC1+anaTrack.GetNumberOfClusters().VTPC2);
			nGTPC.push_back(anaTrack.GetNumberOfClusters().GTPC);
			nMTPC.push_back(anaTrack.GetNumberOfClusters().MTPC);	
			//trackExtrapolation.Extrapolate(vtxStart);
			zVert.push_back(vtxStart);
			vType.push_back(vtxType);
			//trackExtrapolation.Extrapolate(-567.4);
			TrackPar &trackPar1 = trackExtrapolation.GetStopTrackParam();
			theta.push_back(atan(sqrt(trackPar1.GetPar(2)*trackPar1.GetPar(2)+trackPar1.GetPar(3)*trackPar1.GetPar(3))));
			
			double phiVal = acos(trackPar1.GetPar(2)/sqrt(trackPar1.GetPar(2)*trackPar1.GetPar(2)+trackPar1.GetPar(3)*trackPar1.GetPar(3)))*180/TMath::Pi();
			if(trackPar1.GetPar(2)>0 && trackPar1.GetPar(3) < 0){
				phiVal = -1*phiVal;
			}
			else if(trackPar1.GetPar(2)<0 && trackPar1.GetPar(3) < 0){
				phiVal = 360 - phiVal;
			}
			phi.push_back(phiVal);
			
			
			xStart.push_back(trackPar1.GetPar(0));
			yStart.push_back(trackPar1.GetPar(1));
			zStart.push_back(trackPar1.GetZ());
			xEStart.push_back(trackPar1.GetStd(0));
			yEStart.push_back(trackPar1.GetStd(1));
			distToTarg.push_back(anaTrack.GetDistanceFromTarget());
			//cout << trackPar1.GetPar(2) << " " << track.GetCharge()<< endl;
			if(trackPar1.GetPar(2)*track.GetCharge() > 0){
				RST.push_back(1);
			}
			else{
				RST.push_back(0);
			}
			
			
			trackExtrapolationStop.SetTrackParams(track);
			trackExtrapolationLen.SetTrackParams(track);
			trackExtrapolationLen.Extrapolate(-612.4);
			
			vector<TPCCluster> fClusters;
			//cerr << "step1" << endl;
			ClusterIndexIterator it;
			double zp = track.GetMomentumPoint().GetZ();
			for(it = track.ClustersBegin(); it!= track.ClustersEnd(); it++){
				 
				const Cluster clust= recEvent.Get(*it);
				const Point& pos = clust.GetPosition();

				if(zp > pos.GetZ()-1) continue;

				TPCCluster temp(pos.GetX(), pos.GetY(), pos.GetZ(), clust.GetPositionUncertainty( evt::rec::ClusterConst::eX), clust.GetPositionUncertainty( evt::rec::ClusterConst::eY), clust.GetTPCId());
				fClusters.push_back(temp);
			}
			
			//trackExtrapolationStop.SetClusters(track, recEvent);
			//cerr << "step2" << endl;
			TrackPar &trackPar2 = trackExtrapolationStop.GetStopTrackParam();
			for(int i = 0; i < fClusters.size(); i++){
				trackExtrapolationStop.DoKalmanStep(fClusters.at(i));

			}


			trackExtrapolationStop.Extrapolate(fFTOFConfig.GetFTOFZ()-1.5);
			
			xTOF1.push_back(trackPar2.GetPar(0));
			yTOF1.push_back(trackPar2.GetPar(1));
			zTOF1.push_back(trackPar2.GetZ());
			xETOF1.push_back(trackPar2.GetStd(0));
			yETOF1.push_back(trackPar2.GetStd(1));
			length.push_back(trackExtrapolationLen.GetStopTrackParam().GetLength()+trackPar2.GetLength());
			//length.push_back(trackPar1.GetLength()+trackPar2.GetLength());
			
			trackExtrapolationStop.Extrapolate(fFTOFConfig.GetFTOFZ()+1.5);
			xTOF2.push_back(trackPar2.GetPar(0));
			yTOF2.push_back(trackPar2.GetPar(1));
			zTOF2.push_back(trackPar2.GetZ());
			xETOF2.push_back(trackPar2.GetStd(0));
			yETOF2.push_back(trackPar2.GetStd(1));
			
		}

		if(xStart.size() != 0){
			ftofFile->cd();
			fFTOF->Fill();		
		}


	}
	
	ftofFile->cd();
	fFTOF->Write();
	ftofFile->Close();

	evSel.WriteToRoot("event_cuts", cutFile);
	trSel.WriteToRoot("track_cuts", cutFile);
}


