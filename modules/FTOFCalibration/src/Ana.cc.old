/*!
* \file
*/
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <cstdlib>
#include <stdexcept>
#include <TTree.h>
#include <TFile.h>
#include <cstdlib>
#include <evt/Event.h>
#include <evt/EventHeader.h>
#include <io/EventFileChain.h>
#include <det/TriggerConst.h>
#include <det/BPDConst.h>
#include <evt/RawEvent.h>

#include <boost/format.hpp>
#include "Ana.h"
#include "TrackExtrapolation.h"

using namespace std;
using namespace io;
using namespace evt;
using namespace evt::rec;
using namespace det;
using namespace boost;

/*!
 *	\fn 
 *	Function Run reads SHOE files. For this class EventFileChain is used. It needs vector containing paths to the SHOE files.
 *	This vector is obtained from the DataAnaConfig class.
*/
void Ana::Run(){

	string spectrumFileName = fDataAnaConfig.GetOutputDir() + "spectrum_" + fDataAnaConfig.GetTimeString() + ".root";
	string cutFile = fDataAnaConfig.GetOutputDir() + "cuts_" + fDataAnaConfig.GetTimeString() + ".root";

	
	EventFileChain eventFileChain(fDataAnaConfig.GetJob());
	Event event;

	unsigned int nEvents = 0;

	EventSelection evSel;
	TrackSelection trSel;

	TrackExtrapolation trackExtrapolation(fDataAnaConfig.GetTarget());

	TFile *spectrumFile = new TFile(spectrumFileName.c_str(), "RECREATE");
	spectrumFile->cd();
	fSpectrum = new TTree("Spectrum", "Spectrum");
	fSpectrum->SetAutoSave(-300000000);


	fSpectrum->Branch("fSpectrumStruct", &(fSpectrumStruct.x), "x/D:xE:y:yE:z:px:py:pz:dEdx:m2ToF:dEdxE:m2ToFE:scintillatorId/I:topology:q:runNumber");
	unsigned int runNumber = 0;
	
	int ntracks = 0;
	while (eventFileChain.Read(event) == eSuccess && nEvents < Ana::fMaxEv) {
	//while (eventFileChain.Read(event) == eSuccess && nEvents < 10000) {

		/*if(cfg.EventPlotsOn())
			evPlots.Fill(event, cfg.GetTarget());*/

	
		bool skipRun = false;
		EventHeader &evHeader = event.GetEventHeader();

		if(evHeader.GetRunNumber() != runNumber){
			runNumber = evHeader.GetRunNumber();
			skipRun = fDataAnaConfig.SkipRun(runNumber);
		}

		if(skipRun) continue;

   		++nEvents;
    
    	if (!(nEvents%1000))
      		cout << " --->Processing event # " << nEvents << endl;


		if(evSel.IsEventGood(event) == false)
			continue;	

		const RecEvent& recEvent = event.GetRecEvent();

		for (std::list<rec::VertexTrack>::const_iterator iter = recEvent.Begin<rec::VertexTrack>();
           iter != recEvent.End<rec::VertexTrack>(); ++iter)
      	{
		/*for (std::list<rec::Track>::const_iterator iter = recEvent.Begin<rec::Track>();
           iter != recEvent.End<rec::Track>(); ++iter)
      	{*/

			vector<double> tofMass;
			vector<double> tofMassE;
			bool hasToF = false;
			int tofSId = 0;
			const VertexTrack& vtxTrack = *iter;
			if(!vtxTrack.HasTrack()) continue;
			if (vtxTrack.GetNumberOfTOFMasses() > 2) continue;
			if (vtxTrack.GetNumberOfTOFMasses() > 0) hasToF = true;
			
			
			
			for(TOFMassIndexIterator tofIt = vtxTrack.TOFMassBegin(); tofIt != vtxTrack.TOFMassEnd(); tofIt++){
				const rec::TOFMass &tof = recEvent.Get(*tofIt);
				if(tof.GetTOFId() != TOFConst::eTOFF) continue;
				tofMass.push_back(tof.GetSquaredMass());
				tofMassE.push_back(tof.GetSquaredMassError());			
				tofSId = tof.GetScintillatorId();	
			}
			
			
			const Track& track = recEvent.Get(vtxTrack.GetTrackIndex());
			
			if(track.GetNumberOfVertexTracks() != 1) continue;
			ntracks++;

			
			AnaTrack anaTrack(track, trackExtrapolation);

			
			/*for (std::list<rec::TOFMass>::const_iterator tofIter = recEvent.Begin<rec::TOFMass>();
           	tofIter != recEvent.End<rec::TOFMass>(); ++tofIter)
      		{


				if(hasToF) break;

				const rec::TOFMass& tof = *tofIter;
				if(!tof.HasVertexTrack()) continue;
			
				const rec::VertexTrack& vtx = recEvent.Get(tof.GetVertexTrackId());

				if(!vtx.HasTrack()) continue;
			
				if(vtx.GetTrackIndex() != track.GetIndex()) continue;
				nToF++;
				if(vtx.GetNumberOfTOFMasses()<3){ 
					if(tof.GetTOFId() == TOFConst::eTOFF){
						tofMass = tof.GetSquaredMass();
			Jump t			tofE = tof.GetSquaredMassError();
						tofSId = tof.GetScintillatorId();
						hasToF = true;
					}
				}
			}*/
			
			double tofMean[2];
			for(int i = 0; i< tofMass.size(); i++){
				tofMean[0] = tofMass.at(i)/tofMass.size();
				tofMean[1] = tofMassE.at(i)/tofMassE.size();
			}
			if(hasToF){
				anaTrack.SetToFMassSquared(tofMean[0]);
				anaTrack.SetToFScintillatorId(tofSId);
			}

			if(!trSel.IsTrackGood(anaTrack))
				continue;


			fSpectrumStruct.z = anaTrack.GetExtrapolatedPosition().GetZ(); 
			fSpectrumStruct.x = anaTrack.GetExtrapolatedPosition().GetX();
			fSpectrumStruct.xE = anaTrack.GetPositionErrors().at(0);
			fSpectrumStruct.y = anaTrack.GetExtrapolatedPosition().GetY();
			fSpectrumStruct.yE = anaTrack.GetPositionErrors().at(1);
			fSpectrumStruct.px = anaTrack.GetMomentum().GetX();
			fSpectrumStruct.py = anaTrack.GetMomentum().GetY();
			fSpectrumStruct.pz = anaTrack.GetMomentum().GetZ();

						
			fSpectrumStruct.q = track.GetCharge();
			fSpectrumStruct.dEdx = track.GetEnergyDeposit(TrackConst::eAll);
			fSpectrumStruct.dEdxE = track.GetEnergyDepositVariance(TrackConst::eAll);
			fSpectrumStruct.m2ToF = anaTrack.GetToFMassSquared();
			fSpectrumStruct.m2ToFE = tofMean[1];
			fSpectrumStruct.scintillatorId = anaTrack.GetToFScintillatorId();

			fSpectrumStruct.topology = anaTrack.GetTopology();

			fSpectrumStruct.runNumber = runNumber;

			spectrumFile->cd();
			fSpectrum->Fill();

		}
	}
	
	cout << ntracks << endl;
	spectrumFile->cd();
	fSpectrum->Write();
	spectrumFile->Close();

	evSel.WriteToRoot("event_cuts", cutFile);
	trSel.WriteToRoot("track_cuts", cutFile);
}



