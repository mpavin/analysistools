#!/bin/sh
 
#set -e


MYDIR="/afs/cern.ch/user/m/mpavin/Work/analysistools/modules/FTOFCalibration"

#DATADIR="/eos/na61/data/prod/p_C_31_10/030_14d_v0r8p0_pp_slc6_phys/shoe.root"
#DATADIR="/eos/na61/data/test/p_C_31_10/030_14d_v0r8p0_pp_slc6_phys_FTOF_test"
DATADIR="/eos/experiment/na61/data/test/p_C_31_10/030_14d_v0r8p0_pp_slc6_phys_FTOF_test/mctest3"
#OUTDIR="/eos/na61/data/test/p_C_31_10/030_14d_v0r8p0_pp_slc6_phys_FTOF_test/updated"
OUTDIR="/eos/experiment/na61/data/test/p_C_31_10/030_14d_v0r8p0_pp_slc6_phys_FTOF_test/mctest4"
#OUTDIR="/afs/cern.ch/work/m/mpavin/Results/TESTTOF"
#DATADIR="/eos/na61/data/test/p_C_31_10/030_14d_v0r8p0_pp_slc6_phys_FTOF_test/2009"
#**********************************************************************************

UDIR=${DATADIR}
FILEADD="data"

#**********************************************************************************

#**********************************************************************************
# ARE DATA(MC) FILES ON EOS?
if [[ $UDIR == *"eos"* ]]; then
	EOSIN="1"
else
	EOSIN="0"
fi

if [[ $OUTDIR == *"eos"* ]]; then
	EOSOUT="1"
else
	EOSOUT="0"
fi
#**********************************************************************************

#**********************************************************************************
#	SHINE ENV
readonly shine_version='v1r4p0'
readonly shine_root="/afs/cern.ch/na61/Releases/SHINE/${shine_version}"
. "${shine_root}/scripts/env/lxplus_32bit_slc6.sh"
eval $("${shine_root}/bin/shine-offline-config" --env-sh)
#**********************************************************************************

#**********************************************************************************
#	EOS
eos="/afs/cern.ch/project/eos/installation/0.3.84-aquamarine/bin/eos.select"
eosumount="/afs/cern.ch/project/eos/installation/0.3.84-aquamarine/bin/eos.select -b fuse umount"                                                                               
eosmount="/afs/cern.ch/project/eos/installation/0.3.84-aquamarine/bin/eos.select -b fuse mount"    
#**********************************************************************************

#**********************************************************************************
#	TMP DIR
a=${PWD}
a=${a:0:5}
echo ${a}
#if [ -n "${LSB_BATCH_JID}" ]; then
if [ "$a" == "/pool" ]; then
        tmpdir="${PWD}"                                                                                                                          
else
        tmpdir="${TMPDIR:=/tmp/${USER}}"                                                                                                                        
fi
#if [ -n "${LSB_BATCH_JID}" ]; then
#        tmpdir="/pool/lsf/${USER}/${LSB_BATCH_JID}"                                                                                                                          
#else
#        tmpdir="${TMPDIR:=/tmp/${USER}}"                                                                                                                        
#fi
#**********************************************************************************

#**********************************************************************************
#	DIR FOR RESULTS
export RES="${tmpdir}/Results"
cd $tmpdir
mkdir Results
#**********************************************************************************


if [[ $EOSIN == "1" ]]; then
	"$eos" cp "$UDIR/$1" "./"
else
	cp "$UDIR/$1" "./"
fi			


for i in *.root; 
do 
	echo "${tmpdir}/${i}" >> list.txt
done;

cd ${MYDIR}
export INPUTFILE="${tmpdir}/list.txt"
./ftofCalib -u -b bootstrap.xml #>log.txt 2>log.err 

if [[ $EOSOUT == "1" ]]; then
	cp "$RES/shoe.root" "$OUTDIR/$1"
else
	cp "$RES/shoe.root" "$OUTDIR/$1"
fi		
rm "${tmpdir}/${1}"
rm "${tmpdir}/Results/shoe.root"
