#!/bin/sh                                                                                                                                                                

N=0
START=0
while read line
do

    N=$((N+1))
    if [ "$N" -lt "$START" ]; then                                           
	continue                                                                                                                                                                 
    fi  
    bsub -q 8nh -J "TOFCALIB_${N}" -e /afs/cern.ch/work/m/mpavin/logs/log.err -o /afs/cern.ch/work/m/mpavin/logs/log.txt runU.sh ${line};
    #./runU.sh ${line}
    #sleep 0.5
done <$1

