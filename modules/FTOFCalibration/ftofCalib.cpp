/**
 * \file
 * dataAna is module for analyzing data and creating output root file.
 * Root file contains information about track's extrapolated position and momentum with included dE/dx and \f$m^{2}_{TOF}\f$ information.
*/
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <evt/Event.h>
#include <evt/EventHeader.h>
#include <io/EventFileChain.h>

#include <boost/format.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/program_options.hpp>

#include "CConfig.h"
#include "Ana.h"

using namespace std;
using namespace io;
using namespace evt;
using namespace boost;

namespace po = boost::program_options;

CentralConfig& cConfig = fwk::CentralConfig::GetInstance("bootstrap.xml", false, false);
int main(int argc, char* argv[])
{

	
  string cfgFile = " ";	//Config file
  int nMaxEvents = 20000000; //Maximum number of evetns

//**************************************OPTIONS*************************************  
  po::options_description desc("Usage");

  desc.add_options()
    ("help,h",
     "Output this help.")
    ("bootstrap,b",
     po::value<string>(&cfgFile), "Bootstrap file.")  
    ("nev,n",
     po::value<int>(&nMaxEvents), "Max number of events.")  
    ("update,u",
     po::bool_switch()->default_value(false), "Clear and update TOF hits!")  
    ("mc,m",
     po::bool_switch()->default_value(false), "Clear MC TOF resolution!")  
    ;
  
  po::variables_map vm;
  try {
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);
  } catch (po::error& err) {
    cerr << "Command line error : " << err.what() << '\n'
	 << desc << endl;
    exit(EXIT_FAILURE);
  }
//*********************************************************************************

	//CentralConfig& cConfig = fwk::CentralConfig::GetInstance(cfgFile.c_str(), false, false);
	cConfig.GetInstance(cfgFile, false, false);


	Ana ana(nMaxEvents);
	
	if(vm["update"].as<bool>()){
		ana.UpdateTOFData();
	}
	else if(vm["mc"].as<bool>()){
		ana.UpdateTOFMC();
	}
	else{
		ana.ExtractTOFData();
	}
	

	
}
