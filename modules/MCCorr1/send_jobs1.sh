#!/bin/sh                                                                                                                                                                

MC=1
START=1001
STEP=10
for (( i = 0 ; i < 50 ; i++ )); do
    #bsub -q 1nh -J "SIMANA_${i}" -e /afs/cern.ch/work/m/mpavin/logs/simana.err -o /afs/cern.ch/work/m/mpavin/logs/simana.log run.sh $((MC)) $((START)) $((STEP));
    START=$((START+STEP))
	./run.sh $((MC)) $((START)) $((STEP));
	rm -r /tmp/mpavin/*
    #sleep 0.1
done;

