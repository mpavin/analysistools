#include <vector>
#include <string>
#include <iostream>
#include "Functions.h"
#include <TH1D.h>
#include <TH2D.h>
#include <TFile.h>
#include <TChain.h>
#include <TMath.h>


using namespace std;


int main(){

	
	string inputFile = "list.txt";

	
	TChain mcChain("SimData");
	
	string outName = "mcPred.root";
	TFile outFile(outName.c_str(), "RECREATE");
	ifstream ifs;
	string line;
	ifs.open(inputFile.c_str());
		
	if (!ifs.is_open()){
		cerr << "[ReadInputListFile] Could not open infile. Exiting..." << endl;			
		exit (101);
	}
    
	while (getline(ifs, line)){
		if(line.at(0)=='#')
			continue;
		mcChain.Add(line.c_str());
	}
	ifs.close();		

	
	BeamA beam;
	vector<double> *z = NULL;
	vector<double> *theta = NULL;
	vector<double> *pSim = NULL;
	vector<double> *pxSim = NULL;
	vector<int> *q = NULL;
	vector<int> *pdgId = NULL;
	vector<int> *binId = NULL;
	vector<int> *process = NULL;
	
	TBranch *b_aData = mcChain.GetBranch("Beam");
 	b_aData->SetAddress(&beam);
	mcChain.SetBranchAddress("z", &z);
	mcChain.SetBranchAddress("theta" , &theta);
	mcChain.SetBranchAddress("p", &pSim);
	mcChain.SetBranchAddress("px", &pxSim);
	mcChain.SetBranchAddress("q", &q);
	mcChain.SetBranchAddress("pdgId", &pdgId);
	//mcChain.SetBranchAddress("binId", &binId);
	mcChain.SetBranchAddress("process", &process);

	
	int nEntries = mcChain.GetEntries();
	
	
	double zBorders[7] ={-0.1, 18, 36, 54, 72, 89.99, 90.03};
	//double thetaBorders[12] ={0, 20, 40, 60, 80, 100, 140, 180, 220, 260, 300, 340};
	double thetaBorders[6][20] ={{0, 20, 40, 60, 80, 100, 140, 160, 180, 200, 220, 240, 260, 280, 300, 320},
								{0, 20, 40, 60, 80, 100, 140, 160, 180, 200, 220, 240, 260, 280, 300, 320},
								{0, 20, 40, 60, 80, 100, 140, 160, 180, 200, 220, 240, 260, 280, 300, 320},
								{0, 20, 40, 60, 80, 100, 140, 160, 180, 200, 220, 240, 260, 280, 300, 320},
								{0, 20, 40, 60, 80, 100, 140, 160, 180, 200, 220, 240, 260, 280, 300, 320},
								{0, 20, 40, 60, 80, 100, 140, 160, 180, 200, 220, 240, 260, 280, 300, 320}};
	//double thetaBorders[20] ={0, 10, 20, 30, 40, 50, 60, 70, 90, 110, 150, 190, 230, 270, 310, 320, 330, 350, 370, 390};
	double max[19] = {29.3, 29.625, 20, 20, 20, 20, 15, 15, 15, 15, 10, 10, 10, 10, 10, 10, 10, 10, 10};
	
	double nbins[19] = {40, 40, 50, 60, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100};
	vector<vector<TH1D> > predHist;
	for(int i = 0; i < 6; i++){
		vector<TH1D> temp;
		for(int j = 0; j < 19; j++){
			string name = "mcpred" + ConvertToString(i+1., 0) + "_" + ConvertToString(j+1.,0);
			string title = ConvertToString(zBorders[i],0) + " #leq z < " + ConvertToString(zBorders[i+1],0) + " cm, " + 
				ConvertToString(thetaBorders[i][j],0) + " #leq #theta < " + ConvertToString(thetaBorders[i][j+1],0) + " mrad";
			double nbinsd = max[j]/0.2;
			//int nbins = nbinsd;
			
			//nbins = 100;

			TH1D hist(name.c_str(), title.c_str(), nbins[j], 0, max[j]);
			temp.push_back(hist); 
		}
		predHist.push_back(temp);
	}
	TH1D eventId("evId", "", 200, 0, 30000000);
	TH2D pcor("pcor", "", 200, 0, 30000000, 200, 0, 31);
	//nEntries = 25000000;
	double scale = nEntries;
	cout << nEntries << endl;
	for(int i = 0; i < nEntries; i++){
	
		if(!((i+1)%100000)) cout << "Entry: " << i+1 << endl;
		mcChain.GetEntry(i);
		

		for(int j = 0; j < z->size(); j++){
			
			pcor.Fill(i, pSim->at(j));
			if(pdgId->at(j) != 3122) continue;
			//cout << process->at(j) << endl;
			if(process->at(j) != 13) continue;
			int indexZ = -99;
			int indexTh = -99;
			
			for(int k = 0; k < 6; k++){
				if(z->at(j)+657.41 >= zBorders[k] && z->at(j)+657.41 < zBorders[k+1]){
					indexZ = k;
				}
			}
			if(indexZ < 0) continue;
			for(int k = 0; k < 19; k++){
				if(theta->at(j) >= thetaBorders[indexZ][k] && theta->at(j) < thetaBorders[indexZ][k+1]){
					indexTh = k;
				}
			}
			
			if(indexTh < 0) continue;

			if(indexZ == 0 && indexTh == 0){
				if(pSim->at(j) > 1 && pSim->at(j) < 3) eventId.Fill(i);
				//if(pSim->at(j) > 6 && pSim->at(j) < 8) eventId.Fill(i);
				//if(pSim->at(j) > 20 && pSim->at(j) < 21) eventId.Fill(i);
			} 
			predHist.at(indexZ).at(indexTh).Fill(pSim->at(j), 1/(scale*predHist.at(indexZ).at(indexTh).GetBinWidth(1)));
			//predHist.at(indexZ).at(indexTh).Fill(pSim->at(j), 1);
		}
	}
	
	outFile.cd();
	for(int i = 0; i < 6; i++){

		for(int j = 0; j < 19; j++){
			predHist.at(i).at(j).SetLineWidth(3);
			predHist.at(i).at(j).SetLineColor(3);
			predHist.at(i).at(j).Write();
		}

	}	
	eventId.Write();
	pcor.Write();
	outFile.Close();
		
}





