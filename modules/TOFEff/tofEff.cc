#include <vector>
#include <string>
#include <iostream>
#include "Functions.h"
#include <TH1D.h>
#include <TH2D.h>
#include <TFile.h>
#include <TTree.h>
#include <TMath.h>
#include <math.h>

using namespace std;


int main(int argc, char* argv[]){

	string tofeff = "tofEff.root";

	if(argc < 2){
		cerr << "Number of parameters is different than 1! Exiting..." << endl;
		exit(100);
	}
	
	string data;
	
	stringstream sconv0(argv[1]);
	sconv0 >> data;
			
	cout << data << endl;
	TFile tofFile(tofeff.c_str(), "READ");
	TH1D *effHist = (TH1D*) tofFile.Get("xeff");
	
	TFile dfile(data.c_str(), "READ");
	dfile.cd();
	TTree *tree = (TTree*) dfile.Get("dEdx_m2tof");
	double zMin;
	double zMax;
	
	double thetaMin;
	double thetaMax;
	
	double pMin;
	double pMax;
	
	int indexZ;
	int indexTh;
	int indexP;
	
	vector<double> *dEdxBin = NULL;
	vector<double> *m2tofBin = NULL;
	vector<int> *charge = NULL;
	vector<int> *scintId = NULL;
		

	tree->SetBranchAddress("zMin", &zMin);
	tree->SetBranchAddress("zMax", &zMax);
	tree->SetBranchAddress("thetaMin", &thetaMin);
	tree->SetBranchAddress("thetaMax", &thetaMax);
	tree->SetBranchAddress("pMin", &pMin);
	tree->SetBranchAddress("pMax", &pMax);
	tree->SetBranchAddress("indexZ", &indexZ);
	tree->SetBranchAddress("indexTh", &indexTh);
	tree->SetBranchAddress("indexP", &indexP);
	tree->SetBranchAddress("dEdx", &dEdxBin);
	tree->SetBranchAddress("m2ToF", &m2tofBin);
	tree->SetBranchAddress("charge", &charge);
	tree->SetBranchAddress("scintId", &scintId);

	
	int nEntries = tree->GetEntries();
	
	
	for(int i = 0; i < nEntries; ++i){
		
		tree->GetEntry(i);
		
		if(!((i+1)%100000))
			cout << "Processing event # "  << i+1 << endl;
		
		double nsc[80];
		for(int j = 0; j < 80; ++j){
			nsc[j] = 0;
		}	
		
		double norm = scintId->size();
		double eff = 0;
		double effE = 0;
		if(norm == 0) continue;
		for(int j = 0; j < scintId->size(); ++j){
			nsc[scintId->at(j)-1] += 1.;
		}

		for(int j = 0; j < 80; ++j){
			double effi = effHist->GetBinContent(j+1);
			if(effi < 0.01) effi = effHist->GetBinContent(j);
			//cout << j << " " << effHist->GetBinContent(j+1) << endl;
			eff += nsc[j]/norm/effi;
		}	
		cout << thetaMin << " " << thetaMax << " " << eff << endl;
	}

		
}





