/*!
* \file
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <cstdlib>
#include <stdexcept>
#include <TTree.h>
#include <TFile.h>
#include <cstdlib>
#include <evt/Event.h>
#include <evt/EventHeader.h>
#include <io/EventFileChain.h>
#include <det/TriggerConst.h>
#include <det/BPDConst.h>
#include <evt/RawEvent.h>
#include <evt/SimEvent.h>
#include "StrTools.h"
#include <boost/format.hpp>
#include "Ana.h"
#include "TrackExtrapolation.h"
#include "evt/RawEvent.h"
#include "evt/raw/TOF.h"
#include <TMath.h>

using namespace std;
using namespace io;
using namespace evt;
using namespace evt::rec;
using namespace evt::sim;
using namespace evt::raw;
using namespace det;
using namespace boost;
using namespace StrTools;
/*!
 *	\fn 
 *	Function Run reads SHOE files. For this class EventFileChain is used. It needs vector containing paths to the SHOE files.
 *	This vector is obtained from the DataAnaConfig class.
*/
void Ana::Run(){
	int k = 0;

	string spectrumFileName = fGeoAlignConfig.GetOutputDir() + "/tpcalignment.root";
	string cutFile = fGeoAlignConfig.GetOutputDir() + "/cuts.root";
	
	EventFileChain eventFileChain(fGeoAlignConfig.GetJob());
	Event event;

	unsigned int nEvents = 0;
	
	EventSelection evSel;
	TrackSelection trSel;

	TrackExtrapolation trackExtrapolation(true, 0.04, 30400.);
	TrackExtrapolation trackExtrapolationVT1(true, 0.04, 30400.);
	TrackExtrapolation trackExtrapolationVT2a(true, 0.04, 30400.);
	TrackExtrapolation trackExtrapolationVT2b(true, 0.04, 30400.);
	TrackExtrapolation trackExtrapolationMTL(true, 0.04, 30400.);
	TrackExtrapolation trackExtrapolationMTR(true, 0.04, 30400.);
	TrackExtrapolation trackExtrapolationGT(true, 0.04, 30400.);

	trackExtrapolation.SetTarget(fGeoAlignConfig.GetTarget());
	


	double x;
	double y;
	double z;
	double xE;
	double yE;
	double distTarg;
	double px;
	double py;
	double pz;
	int q;
	int topology;
	
	vector<double> dx;
	vector<double> dy;
	vector<double> xv;
	vector<double> yv;
	vector<double> dtx;
	vector<double> dty;
	vector<int> diffId;
	int runNumber = 0;
	TFile *spectrumFile = new TFile(spectrumFileName.c_str(), "RECREATE");
	spectrumFile->cd();
	
	fSpectrum = new TTree("AlignData", "AlignData");
	fSpectrum->SetAutoSave(-300000000);

	fSpectrum->Branch("x", &x, "x/D");
	fSpectrum->Branch("y", &y, "y/D");
	fSpectrum->Branch("z", &z, "z/D");
	fSpectrum->Branch("xE", &xE, "xE/D");
	fSpectrum->Branch("yE", &yE, "yE/D");
	fSpectrum->Branch("distTarg", &distTarg, "yE/D");
	fSpectrum->Branch("px", &px, "px/D");
	fSpectrum->Branch("py", &py, "py/D");
	fSpectrum->Branch("pz", &pz, "pz/D");
	fSpectrum->Branch("q", &q, "q/I");
	fSpectrum->Branch("runNumber", &runNumber, "runNumber/I");
	fSpectrum->Branch("topology", &topology, "topology/I");
	fSpectrum->Branch("dx", "vector<double>", &dx);
	fSpectrum->Branch("dy", "vector<double>", &dy);
	fSpectrum->Branch("xv", "vector<double>", &xv);
	fSpectrum->Branch("yv", "vector<double>", &yv);
	fSpectrum->Branch("dtx", "vector<double>", &dtx);
	fSpectrum->Branch("dty", "vector<double>", &dty);
	fSpectrum->Branch("diffId", "vector<int>", &diffId);

	//fSpectrum->Branch("fSpectrumStruct", &(fSpectrumStruct.x), "x/D:xE:y:yE:z:px:py:pz:dEdx:m2ToF:dEdxE:m2ToFE:scintillatorId/I:topology:q:runNumber");
	
	
	
	bool mc = false;
	if(eventFileChain.Read(event) == eSuccess){
		const SimEvent& simEvent = event.GetSimEvent();
		mc = simEvent.HasMainVertex();
	}
	else{
		cerr << __FUNCTION__ << ": Error in input files! Exiting..." << endl;
		exit(EXIT_FAILURE);
	}
	
	int ntracks = 0;
	
	
	
	double startCov[15] = {25, 0, 0, 0, 0, 25, 0, 0, 0, 25, 0, 0, 25, 0, 25};
	while (eventFileChain.Read(event) == eSuccess && nEvents < Ana::fMaxEv) {
		
		bool skipRun = false;
		EventHeader &evHeader = event.GetEventHeader();

		if(evHeader.GetRunNumber() != runNumber){
			runNumber = evHeader.GetRunNumber();
			skipRun = fGeoAlignConfig.SkipRun(runNumber);
		}

		if(skipRun) continue;

   		++nEvents;
    
    	if (!(nEvents%100))
      		cerr << " --->Processing event # " << nEvents << endl;

		const RecEvent& recEvent = event.GetRecEvent();

		if(evSel.IsEventGood(event) == false)
			continue;	

		for (std::list<rec::Track>::const_iterator iter = recEvent.Begin<rec::Track>();
           iter != recEvent.End<rec::Track>(); ++iter)
      	{
			
			dx.clear();
			dy.clear();
			xv.clear();
			yv.clear();
			dtx.clear();
			dty.clear();
			diffId.clear();
			
			const Track& track = *iter;

			ntracks++;

			AnaTrack anaTrack(trackExtrapolation);
			anaTrack.SetTrack(track);
			
			if(!trSel.IsTrackGood(anaTrack))
				continue;
			
            x = anaTrack.GetExtrapolatedPosition().GetX();
            y = anaTrack.GetExtrapolatedPosition().GetY();
            z = anaTrack.GetExtrapolatedPosition().GetZ();

            xE = anaTrack.GetPositionErrors().at(0);
            yE = anaTrack.GetPositionErrors().at(1);
			q = track.GetCharge();
            distTarg = anaTrack.GetDistanceFromTarget();
            px = anaTrack.GetMomentum().GetX();
            py = anaTrack.GetMomentum().GetY();
            pz = anaTrack.GetMomentum().GetZ();
			
			topology = anaTrack.GetTopology();
			vector<TPCCluster> clustersVT1;
			vector<TPCCluster> clustersVT2;
			vector<TPCCluster> clustersMTL;
			vector<TPCCluster> clustersMTR;
			vector<TPCCluster> clustersGT;

			
			ClusterIndexIterator it;
			for(it = track.ClustersBegin(); it!= track.ClustersEnd(); it++){
				 
				const Cluster clust= recEvent.Get(*it);
				const Point& pos = clust.GetPosition();

				

				TPCCluster temp(pos.GetX(), pos.GetY(), pos.GetZ(), clust.GetPositionUncertainty( evt::rec::ClusterConst::eX), 
					clust.GetPositionUncertainty( evt::rec::ClusterConst::eY), clust.GetTPCId());
					//cout << clust.GetStatus() << endl;
				if(clust.GetTPCId() == det::TPCConst::eVTPC1){
					clustersVT1.push_back(temp);
				}
				else if(clust.GetTPCId() == det::TPCConst::eVTPC2){
					clustersVT2.push_back(temp);
				}
				else if(clust.GetTPCId() == det::TPCConst::eMTPCL){
					clustersMTL.push_back(temp);
				}
				else if(clust.GetTPCId() == det::TPCConst::eMTPCR){
					clustersMTR.push_back(temp);
				}
				else if(clust.GetTPCId() == det::TPCConst::eGTPC){
					clustersGT.push_back(temp);
				}
			}			
			
			bool mtl = false;
			if(clustersMTL.size() > 0){
				if(clustersMTR.size() == 0){
					mtl = true;
				}
				else{
					if(clustersMTL.at(0).GetPosition(eZ) < clustersMTR.at(0).GetPosition(eZ)){
						mtl = true;
					}
				}
			}

			startCov[14] = 1/track.GetMomentum().GetMag()/track.GetMomentum().GetMag();
			//startCov[14] = 0.04;
			if((topology & 1) && clustersVT1.size()>10){

				TrackPar &trackParVT1 = trackExtrapolationVT1.GetStopTrackParam();
				trackParVT1.SetType(eKisel);
				trackParVT1.SetPar(0, clustersVT1.at(0).GetPosition(eX));
				trackParVT1.SetPar(1, clustersVT1.at(0).GetPosition(eY));
				trackParVT1.SetZ(clustersVT1.at(0).GetPosition(eZ));

				double dz = clustersVT1.at(1).GetPosition(eZ)-clustersVT1.at(0).GetPosition(eZ);

				trackParVT1.SetPar(2, (clustersVT1.at(1).GetPosition(eX)-clustersVT1.at(0).GetPosition(eX))/dz);
				trackParVT1.SetPar(3, (clustersVT1.at(1).GetPosition(eY)-clustersVT1.at(0).GetPosition(eY))/dz);
				startCov[9] = TMath::Power((clustersVT1.at(1).GetPosition(eX)-clustersVT1.at(0).GetPosition(eX))/dz,2);
				startCov[12] = TMath::Power((clustersVT1.at(1).GetPosition(eY)-clustersVT1.at(0).GetPosition(eY))/dz,2);
				
				trackParVT1.SetPar(4, track.GetCharge()/track.GetMomentum().GetMag());
				//trackParVT1.SetPar(4, track.GetCharge()/5.);
				trackParVT1.SetCov(startCov);
				trackParVT1.SetCharge(track.GetCharge());

				for(int i = 1; i < clustersVT1.size(); i++){
					trackExtrapolationVT1.DoKalmanStep(clustersVT1.at(i));

				}	
				/*for(int i = 1; i < clustersVT1.size(); i++){
					trackExtrapolationVT1.DoKalmanStep(clustersVT1.at(clustersVT1.size()-1-i));
					/*if(abs(clustersVT1.at(i).GetPosition(eX) - trackParVT1.GetPar(0)) > 3*trackParVT1.GetStd(0)){
						clustersVT1.erase(clustersVT1.begin()+i);
					} 
					else if(abs(clustersVT1.at(i).GetPosition(eY) - trackParVT1.GetPar(1)) > 3*trackParVT1.GetStd(1)){
						clustersVT1.erase(clustersVT1.begin()+i);
					}

				}	*/
	
				//cout  << (track.GetCharge()*1./trackParVT1.GetPar(4) - track.GetMomentum().GetMag())/(trackParVT1.GetStd(4)/trackParVT1.GetPar(4)/trackParVT1.GetPar(4)) << endl;
				//exit(100);			
				if(((topology & 2) && clustersVT2.size()>10) || ((topology & 4)&& clustersGT.size()>10)){
					trackExtrapolationVT1.Extrapolate(-185);
				}
				else if((topology & 8) && (clustersMTL.size() > 1 || clustersMTR.size() >1)){
					trackExtrapolationVT1.Extrapolate(350);
				}
				
			}
			
			if((topology & 2) && clustersVT2.size() > 10){
				TrackPar &trackParVT2a = trackExtrapolationVT2a.GetStopTrackParam();
				trackParVT2a.SetType(eKisel);
				trackParVT2a.SetPar(0, clustersVT2.at(0).GetPosition(eX));
				trackParVT2a.SetPar(1, clustersVT2.at(0).GetPosition(eY));
				trackParVT2a.SetZ(clustersVT2.at(0).GetPosition(eZ));
				trackParVT2a.SetPar(2, (clustersVT2.at(1).GetPosition(eX)-clustersVT2.at(0).GetPosition(eX))/
					(clustersVT2.at(1).GetPosition(eZ)-clustersVT2.at(0).GetPosition(eZ)));
				trackParVT2a.SetPar(3, (clustersVT2.at(1).GetPosition(eY)-clustersVT2.at(0).GetPosition(eY))/
					(clustersVT2.at(1).GetPosition(eZ)-clustersVT2.at(0).GetPosition(eZ)));
				trackParVT2a.SetPar(4, track.GetCharge()/track.GetMomentum().GetMag());
				trackParVT2a.SetCov(startCov);
				trackParVT2a.SetCharge(track.GetCharge());
				
				for(int i = 1; i < clustersVT2.size(); i++){
					trackExtrapolationVT2a.DoKalmanStep(clustersVT2.at(i));
				}	
				
					
				if((topology & 1) && clustersVT1.size()>10){
					TrackPar &trackParVT2b = trackExtrapolationVT2b.GetStopTrackParam();
					trackParVT2b = trackParVT2a;
					for(int i = 1; i < clustersVT2.size(); i++){
						trackExtrapolationVT2b.DoKalmanStep(clustersVT2.at(clustersVT2.size()-1-i));
					}
					trackExtrapolationVT2b.Extrapolate(-185);				
				}
				if((topology & 8) && (clustersMTL.size() > 10 || clustersMTR.size() >10)){	
					trackExtrapolationVT2a.Extrapolate(350);
				}	
			}
			if((topology & 4) && clustersGT.size() > 4){
				TrackPar &trackParGT = trackExtrapolationGT.GetStopTrackParam();
				trackParGT.SetType(eKisel);
				trackParGT.SetPar(0, clustersGT.at(0).GetPosition(eX));
				trackParGT.SetPar(1, clustersGT.at(0).GetPosition(eY));
				trackParGT.SetZ(clustersGT.at(0).GetPosition(eZ));
				trackParGT.SetPar(2, (clustersGT.at(1).GetPosition(eX)-clustersGT.at(0).GetPosition(eX))/
					(clustersGT.at(1).GetPosition(eZ)-clustersGT.at(0).GetPosition(eZ)));
				trackParGT.SetPar(3, (clustersGT.at(1).GetPosition(eY)-clustersGT.at(0).GetPosition(eY))/
					(clustersGT.at(1).GetPosition(eZ)-clustersGT.at(0).GetPosition(eZ)));
				trackParGT.SetPar(4, track.GetCharge()/track.GetMomentum().GetMag());
				trackParGT.SetCov(startCov);
				trackParGT.SetCharge(track.GetCharge());
				
				for(int i = 1; i < clustersGT.size(); i++){
					trackExtrapolationGT.DoKalmanStep(clustersGT.at(i));
				}
				for(int i = 1; i < clustersGT.size(); i++){
					trackExtrapolationGT.DoKalmanStep(clustersGT.at(clustersGT.size()-1-i));
				}	
				trackExtrapolationGT.Extrapolate(-185);				
			}
			if((topology & 8) && (clustersMTL.size() > 10 || clustersMTR.size() >10)){
				
				if(mtl){
					TrackPar &trackParMTL = trackExtrapolationMTL.GetStopTrackParam();
					trackParMTL.SetType(eKisel);
					trackParMTL.SetPar(0, clustersMTL.at(0).GetPosition(eX));
					trackParMTL.SetPar(1, clustersMTL.at(0).GetPosition(eY));
					trackParMTL.SetZ(clustersMTL.at(0).GetPosition(eZ));
					trackParMTL.SetPar(2, (clustersMTL.at(1).GetPosition(eX)-clustersMTL.at(0).GetPosition(eX))/
						(clustersMTL.at(1).GetPosition(eZ)-clustersMTL.at(0).GetPosition(eZ)));
					trackParMTL.SetPar(3, (clustersMTL.at(1).GetPosition(eY)-clustersMTL.at(0).GetPosition(eY))/
						(clustersMTL.at(1).GetPosition(eZ)-clustersMTL.at(0).GetPosition(eZ)));
					trackParMTL.SetPar(4, track.GetCharge()/track.GetMomentum().GetMag());
					trackParMTL.SetCov(startCov);
					trackParMTL.SetCharge(track.GetCharge());
					
					for(int i = 1; i < clustersMTL.size(); i++){
						trackExtrapolationMTL.DoKalmanStep(clustersMTL.at(i));
					}					
					for(int i = 1; i < clustersMTL.size(); i++){
						trackExtrapolationMTL.DoKalmanStep(clustersMTL.at(clustersMTL.size()-1-i));
					}	
					trackExtrapolationMTL.Extrapolate(350);
				}	
				else {
					TrackPar &trackParMTR = trackExtrapolationMTR.GetStopTrackParam();
					trackParMTR.SetType(eKisel);
					trackParMTR.SetPar(0, clustersMTR.at(0).GetPosition(eX));
					trackParMTR.SetPar(1, clustersMTR.at(0).GetPosition(eY));
					trackParMTR.SetZ(clustersMTR.at(0).GetPosition(eZ));
					trackParMTR.SetPar(2, (clustersMTR.at(1).GetPosition(eX)-clustersMTR.at(0).GetPosition(eX))/
						(clustersMTR.at(1).GetPosition(eZ)-clustersMTR.at(0).GetPosition(eZ)));
					trackParMTR.SetPar(3, (clustersMTR.at(1).GetPosition(eY)-clustersMTR.at(0).GetPosition(eY))/
						(clustersMTR.at(1).GetPosition(eZ)-clustersMTR.at(0).GetPosition(eZ)));
					trackParMTR.SetPar(4, track.GetCharge()/track.GetMomentum().GetMag());
					trackParMTR.SetCov(startCov);
					trackParMTR.SetCharge(track.GetCharge());
					for(int i = 1; i < clustersMTR.size(); i++){
						trackExtrapolationMTR.DoKalmanStep(clustersMTR.at(i));
					}				
					for(int i = 1; i < clustersMTR.size(); i++){
						trackExtrapolationMTR.DoKalmanStep(clustersMTR.at(clustersMTR.size()-1-i));
					}				
					trackExtrapolationMTR.Extrapolate(350);
				}			
			}			
			

			if((topology & 1) && clustersVT1.size() > 10){
				if((topology & 4) && clustersGT.size() > 4){
					xv.push_back(trackExtrapolationVT1.GetStopTrackParam().GetPar(0));
					yv.push_back(trackExtrapolationVT1.GetStopTrackParam().GetPar(1));
					dx.push_back(trackExtrapolationVT1.GetStopTrackParam().GetPar(0)-trackExtrapolationGT.GetStopTrackParam().GetPar(0));
					dy.push_back(trackExtrapolationVT1.GetStopTrackParam().GetPar(1)-trackExtrapolationGT.GetStopTrackParam().GetPar(1));
					dtx.push_back(trackExtrapolationVT1.GetStopTrackParam().GetPar(2)-trackExtrapolationGT.GetStopTrackParam().GetPar(2));
					dty.push_back(trackExtrapolationVT1.GetStopTrackParam().GetPar(3)-trackExtrapolationGT.GetStopTrackParam().GetPar(3));
					diffId.push_back(2);				
				}
				else if((topology & 2) && clustersVT2.size() > 10){
					xv.push_back(trackExtrapolationVT1.GetStopTrackParam().GetPar(0));
					yv.push_back(trackExtrapolationVT1.GetStopTrackParam().GetPar(1));
					dx.push_back(trackExtrapolationVT1.GetStopTrackParam().GetPar(0)-trackExtrapolationVT2b.GetStopTrackParam().GetPar(0));
					dy.push_back(trackExtrapolationVT1.GetStopTrackParam().GetPar(1)-trackExtrapolationVT2b.GetStopTrackParam().GetPar(1));
					dtx.push_back(trackExtrapolationVT1.GetStopTrackParam().GetPar(2)-trackExtrapolationVT2b.GetStopTrackParam().GetPar(2));
					dty.push_back(trackExtrapolationVT1.GetStopTrackParam().GetPar(3)-trackExtrapolationVT2b.GetStopTrackParam().GetPar(3));
					diffId.push_back(1);
				}
				else if((topology & 8) && (clustersMTL.size() > 10 || clustersMTR.size() >10)){
					xv.push_back(trackExtrapolationVT1.GetStopTrackParam().GetPar(0));
					yv.push_back(trackExtrapolationVT1.GetStopTrackParam().GetPar(1));
					if(mtl){
						dx.push_back(trackExtrapolationVT1.GetStopTrackParam().GetPar(0)-trackExtrapolationMTL.GetStopTrackParam().GetPar(0));
						dy.push_back(trackExtrapolationVT1.GetStopTrackParam().GetPar(1)-trackExtrapolationMTL.GetStopTrackParam().GetPar(1));
						//dx.push_back(trackExtrapolationVT1.GetStopTrackParam().GetPar(0)-clustersMTL.at(0).GetPosition(eX));
						//dy.push_back(trackExtrapolationVT1.GetStopTrackParam().GetPar(1)-clustersMTL.at(0).GetPosition(eY));
						dtx.push_back(trackExtrapolationVT1.GetStopTrackParam().GetPar(2)-trackExtrapolationMTL.GetStopTrackParam().GetPar(2));
						dty.push_back(trackExtrapolationVT1.GetStopTrackParam().GetPar(3)-trackExtrapolationMTL.GetStopTrackParam().GetPar(3));
						diffId.push_back(3);					
					}
					else{
						dx.push_back(trackExtrapolationVT1.GetStopTrackParam().GetPar(0)-trackExtrapolationMTR.GetStopTrackParam().GetPar(0));
						dy.push_back(trackExtrapolationVT1.GetStopTrackParam().GetPar(1)-trackExtrapolationMTR.GetStopTrackParam().GetPar(1));
						//dx.push_back(trackExtrapolationVT1.GetStopTrackParam().GetPar(0)-clustersMTR.at(0).GetPosition(eX));
						//dy.push_back(trackExtrapolationVT1.GetStopTrackParam().GetPar(1)-clustersMTR.at(0).GetPosition(eY));
						dtx.push_back(trackExtrapolationVT1.GetStopTrackParam().GetPar(2)-trackExtrapolationMTR.GetStopTrackParam().GetPar(2));
						dty.push_back(trackExtrapolationVT1.GetStopTrackParam().GetPar(3)-trackExtrapolationMTR.GetStopTrackParam().GetPar(3));
						diffId.push_back(4);						
					}
				}

			
			}

			if((topology & 2) && clustersVT2.size() > 10){
				if(topology & 4){
					xv.push_back(trackExtrapolationVT2b.GetStopTrackParam().GetPar(0));
					yv.push_back(trackExtrapolationVT2b.GetStopTrackParam().GetPar(1));
					dx.push_back(trackExtrapolationVT2b.GetStopTrackParam().GetPar(0)-trackExtrapolationGT.GetStopTrackParam().GetPar(0));
					dy.push_back(trackExtrapolationVT2b.GetStopTrackParam().GetPar(1)-trackExtrapolationGT.GetStopTrackParam().GetPar(1));
					dtx.push_back(trackExtrapolationVT2b.GetStopTrackParam().GetPar(2)-trackExtrapolationGT.GetStopTrackParam().GetPar(2));
					dty.push_back(trackExtrapolationVT2b.GetStopTrackParam().GetPar(3)-trackExtrapolationGT.GetStopTrackParam().GetPar(3));
					diffId.push_back(5);				
				}
				if((topology & 8) && (clustersMTL.size() > 10 || clustersMTR.size() >10)){
					xv.push_back(trackExtrapolationVT2a.GetStopTrackParam().GetPar(0));
					yv.push_back(trackExtrapolationVT2a.GetStopTrackParam().GetPar(1));
					if(mtl){
						dx.push_back(trackExtrapolationVT2a.GetStopTrackParam().GetPar(0)-trackExtrapolationMTL.GetStopTrackParam().GetPar(0));
						dy.push_back(trackExtrapolationVT2a.GetStopTrackParam().GetPar(1)-trackExtrapolationMTL.GetStopTrackParam().GetPar(1));
						dtx.push_back(trackExtrapolationVT2a.GetStopTrackParam().GetPar(2)-trackExtrapolationMTL.GetStopTrackParam().GetPar(2));
						dty.push_back(trackExtrapolationVT2a.GetStopTrackParam().GetPar(3)-trackExtrapolationMTL.GetStopTrackParam().GetPar(3));
						diffId.push_back(6);					
					}
					else{
						dx.push_back(trackExtrapolationVT2a.GetStopTrackParam().GetPar(0)-trackExtrapolationMTR.GetStopTrackParam().GetPar(0));
						dy.push_back(trackExtrapolationVT2a.GetStopTrackParam().GetPar(1)-trackExtrapolationMTR.GetStopTrackParam().GetPar(1));
						dtx.push_back(trackExtrapolationVT2a.GetStopTrackParam().GetPar(2)-trackExtrapolationMTR.GetStopTrackParam().GetPar(2));
						dty.push_back(trackExtrapolationVT2a.GetStopTrackParam().GetPar(3)-trackExtrapolationMTR.GetStopTrackParam().GetPar(3));
						diffId.push_back(7);						
					}				
				}			
			
			}
			fSpectrum->Fill();
		}

	}
	
	cout << ntracks << endl;
	cout << nEvents << endl;
	spectrumFile->cd();
	fSpectrum->Write();
	spectrumFile->Close();


}


