#ifndef GEOALIGNCONFIG_H
#define GEOALIGNCONFIG_H
#include <iostream>
#include <vector>
#include <string>

#include "TargetAna.h"



using namespace std;

class GeoAlignConfig{

	public:
		GeoAlignConfig()  {Read();}  //Constructor

		//*************************Get functions***********************************
		TargetAna& GetTarget(){return fTarget;} //Returns target.

		string GetOutputDir(){return fOutputDir;}
		string GetInputListFile(){return fInputData;}

		vector<int>& GetRunNumbers(){return fRunNumbers;}

		//*************************Set functions***********************************
		void Read(); //Reads main config file.

		void SetTarget(string name, ETargetType type, double length, double radius, double positionX, double positionY, double positionZ, double tilt1, double tilt2); //Sets target's parameters.

		void SetTarget(TargetAna target){fTarget = target;} //Sets target's parameters.


		void SetInputDataFile(string inputData){fInputData = inputData;}
		void SetOutputDir(string outputDir){fOutputDir = outputDir;}

		void SetRunNumbers(vector<int> fRunNumbers);

		vector<string>& GetJob();
		void ReadInputList();
		void ReadInputList(string inputData);
	

		bool SkipRun(int runNumber);
	private:


		TargetAna fTarget; //Target.

		/*double fVTPC12z;
		double fGTPCVTPC2z;
		double fVTPC2MTPCRz;
		double fVTPC2MTPCLz;*/
		string fInputData;
		string fOutputDir;

		vector<string> fJob;

		vector<int> fRunNumbers;

};

#endif
