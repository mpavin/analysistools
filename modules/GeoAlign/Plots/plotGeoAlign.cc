#include <vector>
#include <string>
#include <iostream>
#include "Functions.h"
#include <TH1D.h>
#include <TH2D.h>
#include <TFile.h>
#include <TChain.h>
#include <TTree.h>
#include <TCanvas.h>
#include <TMath.h>
#include <math.h>
#include <TColor.h>
#include <TStyle.h>
using namespace std;


int main(){

	int data = 1;
	double scale;
	string inputFile;
	string add;
	
	if(data==1){
		inputFile = "listMC.txt";
		add = "data_";	
	}
	else{
		//scale = 0.13870344;
		inputFile = "listMC.txt";
		add = "mc_";			
	}
	TChain chain("AlignData");
	
	//string outName = add + "alignmentplots.root";
	//TFile outFile(outName.c_str(), "RECREATE");
	ifstream ifs;
	string line;
	ifs.open(inputFile.c_str());
		
	if (!ifs.is_open()){
		cerr << "[ReadInputListFile] Could not open infile. Exiting..." << endl;			
		exit (101);
	}
    
	while (getline(ifs, line)){
		if(line.at(0)=='#')
			continue;
		chain.Add(line.c_str());
	}
	ifs.close();		



	double x;
	double y;
	double z;
	double xE;
	double yE;
	double distTarg;
	double px;
	double py;
	double pz;
	int q;
	int topology;
	
	vector<double> *dx = NULL;
	vector<double> *dy = NULL;
	vector<double> *xv = NULL;
	vector<double> *yv = NULL;
	vector<double> *dtx = NULL;
	vector<double> *dty = NULL;
	vector<int> *diffId = NULL;
	

	chain.SetBranchAddress("x", &x);
	chain.SetBranchAddress("xE", &xE);
	chain.SetBranchAddress("y", &y);
	chain.SetBranchAddress("yE", &yE);
	chain.SetBranchAddress("z", &z);
	chain.SetBranchAddress("px", &px);
	chain.SetBranchAddress("py", &py);
	chain.SetBranchAddress("pz", &pz);
	chain.SetBranchAddress("distTarg", &distTarg);
	chain.SetBranchAddress("q", &q);
	chain.SetBranchAddress("dx", &dx);
	chain.SetBranchAddress("dy", &dy);
	chain.SetBranchAddress("xv", &xv);
	chain.SetBranchAddress("yv", &yv);
	chain.SetBranchAddress("dtx", &dtx);
	chain.SetBranchAddress("dty", &dty);
	chain.SetBranchAddress("diffId", &diffId);

	
	int nEntries = chain.GetEntries();
	cout << nEntries << endl;
	
	TH2D dxVT12x("dxVT12x", "", 200, -80, 80, 100, -5, 5);
	TH2D dxVT1GTx("dxVT1GTx", "", 100, -25, 25, 100, -5, 5);
	TH2D dxVT1MTLRx("dxVT1MTLRx", "", 200, -380, 380, 100, -20, 20);
	TH2D dxVT2GTx("dxVT2GTx", "", 100, -25, 25, 100, -5, 5);
	TH2D dxVT2MTLRx("dxVT2MTLRx", "", 200, -250, 250, 100, -5, 5);
	
	TH2D dyVT12x("dyVT12x", "", 200, -80, 80, 100, -5, 5);
	TH2D dyVT1GTx("dyVT1GTx", "", 100, -25, 25, 100, -5, 5);
	TH2D dyVT1MTLRx("dyVT1MTLx", "", 200, -380, 380, 100, -10, 10);
	TH2D dyVT2GTx("dyVT2GTx", "", 100, -25, 25, 100, -5, 5);
	TH2D dyVT2MTLRx("dyVT2MTLx", "", 200, -250, 250, 100, -5, 5);
	
	TH2D dxVT12y("dxVT12y", "", 140, -40, 30, 100, -5, 5);
	TH2D dxVT1GTy("dxVT1GTy", "", 140, -40, 30, 100, -5, 5);
	TH2D dxVT1MTLRy("dxVT1MTLRy", "", 240, -60, 60, 100, -20, 20);
	TH2D dxVT2GTy("dxVT2GTy", "", 140, -40, 30, 100, -5, 5);
	TH2D dxVT2MTLRy("dxVT2MTLRy", "", 240, -60, 60, 100, -5, 5);
	
	TH2D dyVT12y("dyVT12y", "", 140, -40, 30, 100, -5, 5);
	TH2D dyVT1GTy("dyVT1GTy", "", 140, -40, 30, 100, -5, 5);
	TH2D dyVT1MTLRy("dyVT1MTLy", "", 240, -60, 60, 100, -10, 10);
	TH2D dyVT2GTy("dyVT2GTy", "", 140, -40, 30, 100, -5, 5);
	TH2D dyVT2MTLRy("dyVT2MTLy", "", 240, -60, 60, 100, -5, 5);
	
	
	TH2D dtxVT12x("dtxVT12x", "", 200, -80, 80, 100, -0.05, 0.05);
	TH2D dtxVT1GTx("dtxVT1GTx", "", 100, -25, 25, 100, -0.05, 0.05);
	TH2D dtxVT1MTLRx("dtxVT1MTLRx", "", 200, -380, 380, 100, -0.05, 0.05);
	TH2D dtxVT2GTx("dtxVT2GTx", "", 100, -25, 25, 100, -0.15, 0.15);
	TH2D dtxVT2MTLRx("dtxVT2MTLRx", "", 200, -250, 250, 100, -0.05, 0.05);
	
	TH2D dtyVT12x("dtyVT12x", "", 200, -80, 80, 100, -0.05, 0.05);
	TH2D dtyVT1GTx("dtyVT1GTx", "", 100, -25, 25, 100, -0.05, 0.05);
	TH2D dtyVT1MTLRx("dtyVT1MTLx", "", 200, -380, 380, 100, -0.05, 0.05);
	TH2D dtyVT2GTx("dtyVT2GTx", "", 100, -25, 25, 100, -0.15, 0.15);
	TH2D dtyVT2MTLRx("dtyVT2MTLx", "", 200, -250, 250, 100, -0.05, 0.05);
	
	TH2D dtxVT12y("dtxVT12y", "", 140, -40, 30, 100, -0.05, 0.05);
	TH2D dtxVT1GTy("dtxVT1GTy", "", 140, -40, 30, 100, -0.05, 0.05);
	TH2D dtxVT1MTLRy("dtxVT1MTLRy", "", 240, -60, 60, 100, -0.05, 0.05);
	TH2D dtxVT2GTy("dtxVT2GTy", "", 140, -40, 30, 100, -0.15, 0.15);
	TH2D dtxVT2MTLRy("dtxVT2MTLRy", "", 240, -60, 60, 100, -0.05, 0.05);
	
	TH2D dtyVT12y("dtyVT12y", "", 140, -40, 30, 100, -0.05, 0.05);
	TH2D dtyVT1GTy("dtyVT1GTy", "", 140, -40, 30, 100, -0.05, 0.05);
	TH2D dtyVT1MTLRy("dtyVT1MTLy", "", 240, -60, 60, 100, -0.05, 0.05);
	TH2D dtyVT2GTy("dtyVT2GTy", "", 140, -40, 30, 100, -0.15, 0.15);
	TH2D dtyVT2MTLRy("dtyVT2MTLy", "", 240, -60, 60, 100, -0.05, 0.05);
	
	for(int i = 0; i < nEntries; ++i){
		
		chain.GetEntry(i,1);
		
		if(!((i+1)%100000))
			cout << "Processing event # "  << i+1 << endl;
		
		//if(q < 0) continue;
		//if(q*px > 0) continue;
		for(int j = 0; j < dx->size(); j++){
			switch(diffId->at(j)){
				case 1:
					dxVT12x.Fill(xv->at(j), dx->at(j));
					dyVT12x.Fill(xv->at(j), dy->at(j));
					dxVT12y.Fill(yv->at(j), dx->at(j));
					dyVT12y.Fill(yv->at(j), dy->at(j));
					
					dtxVT12x.Fill(xv->at(j), dtx->at(j));
					dtyVT12x.Fill(xv->at(j), dty->at(j));
					dtxVT12y.Fill(yv->at(j), dtx->at(j));
					dtyVT12y.Fill(yv->at(j), dty->at(j));
					break;
				case 2:
					dxVT1GTx.Fill(xv->at(j), dx->at(j));
					dyVT1GTx.Fill(xv->at(j), dy->at(j));
					dxVT1GTy.Fill(yv->at(j), dx->at(j));
					dyVT1GTy.Fill(yv->at(j), dy->at(j));
					
					dtxVT1GTx.Fill(xv->at(j), dtx->at(j));
					dtyVT1GTx.Fill(xv->at(j), dty->at(j));
					dtxVT1GTy.Fill(yv->at(j), dtx->at(j));
					dtyVT1GTy.Fill(yv->at(j), dty->at(j));
					break;
				case 3:
					dxVT1MTLRx.Fill(xv->at(j), dx->at(j));
					dyVT1MTLRx.Fill(xv->at(j), dy->at(j));
					dxVT1MTLRy.Fill(yv->at(j), dx->at(j));
					dyVT1MTLRy.Fill(yv->at(j), dy->at(j));
					
					dtxVT1MTLRx.Fill(xv->at(j), dtx->at(j));
					dtyVT1MTLRx.Fill(xv->at(j), dty->at(j));
					dtxVT1MTLRy.Fill(yv->at(j), dtx->at(j));
					dtyVT1MTLRy.Fill(yv->at(j), dty->at(j));
					break;
				case 4:
					dxVT1MTLRx.Fill(xv->at(j), dx->at(j));
					dyVT1MTLRx.Fill(xv->at(j), dy->at(j));
					dxVT1MTLRy.Fill(yv->at(j), dx->at(j));
					dyVT1MTLRy.Fill(yv->at(j), dy->at(j));
					
					dtxVT1MTLRx.Fill(xv->at(j), dtx->at(j));
					dtyVT1MTLRx.Fill(xv->at(j), dty->at(j));
					dtxVT1MTLRy.Fill(yv->at(j), dtx->at(j));
					dtyVT1MTLRy.Fill(yv->at(j), dy->at(j));
					break;
				case 5:
					dxVT2GTx.Fill(xv->at(j), dx->at(j));
					dyVT2GTx.Fill(xv->at(j), dy->at(j));
					dxVT2GTy.Fill(yv->at(j), dx->at(j));
					dyVT2GTy.Fill(yv->at(j), dy->at(j));	
					
					dtxVT2GTx.Fill(xv->at(j), dtx->at(j));
					dtyVT2GTx.Fill(xv->at(j), dty->at(j));
					dtxVT2GTy.Fill(yv->at(j), dtx->at(j));
					dtyVT2GTy.Fill(yv->at(j), dty->at(j));	
					break;
				case 6:
					dxVT2MTLRx.Fill(xv->at(j), dx->at(j));
					dyVT2MTLRx.Fill(xv->at(j), dy->at(j));
					dxVT2MTLRy.Fill(yv->at(j), dx->at(j));
					dyVT2MTLRy.Fill(yv->at(j), dy->at(j));
					
					dtxVT2MTLRx.Fill(xv->at(j), dtx->at(j));
					dtyVT2MTLRx.Fill(xv->at(j), dty->at(j));
					dtxVT2MTLRy.Fill(yv->at(j), dtx->at(j));
					dtyVT2MTLRy.Fill(yv->at(j), dty->at(j));
					break;
				case 7:
					dxVT2MTLRx.Fill(xv->at(j), dx->at(j));
					dyVT2MTLRx.Fill(xv->at(j), dy->at(j));
					dxVT2MTLRy.Fill(yv->at(j), dx->at(j));
					dyVT2MTLRy.Fill(yv->at(j), dy->at(j));
					
					dtxVT2MTLRx.Fill(xv->at(j), dtx->at(j));
					dtyVT2MTLRx.Fill(xv->at(j), dty->at(j));
					dtxVT2MTLRy.Fill(yv->at(j), dtx->at(j));
					dtyVT2MTLRy.Fill(yv->at(j), dty->at(j));
					break;				
			}
		}
		

	}
	//cout << nCount << " " << nEntries << endl;
	//outFile.cd();
	const UInt_t Number = 4;
	Double_t Red[Number]    = { 0.95, 0.35, 0.83, 0.00};
	Double_t Green[Number]  = { 0.95, 0.33, 0.35, 0.00};
	Double_t Blue[Number]   = { 0.95, 0.85, 0.33, 0.00};
	Double_t Length[Number] = { 0.0, 0.33, 0.66, 1.00 };
	Int_t nb=50;
	gStyle->SetNumberContours(nb);
	TColor::CreateGradientColorTable(Number,Length,Red,Green,Blue,nb);
	
	TCanvas cxx("cxx", "", 1600, 1000);
	cxx.Divide(3,2);
	cxx.cd(1)->SetLogz(kTRUE);
	cxx.cd(1)->SetMargin(0.15, 0.13, 0.15, 0.12);
	dxVT12x.SetStats(kFALSE);
	dxVT12x.SetTitle("VTPC1 - VTPC2");
	dxVT12x.GetXaxis()->SetTitle("x [cm]");
	dxVT12x.GetXaxis()->SetTitleSize(0.07);
	dxVT12x.GetXaxis()->SetLabelSize(0.05);
	dxVT12x.GetYaxis()->SetTitle("dx [cm]");
	dxVT12x.GetYaxis()->SetTitleSize(0.07);
	dxVT12x.GetYaxis()->SetLabelSize(0.05);
	dxVT12x.GetZaxis()->SetLabelSize(0.05);
	dxVT12x.Draw("colz");
	
	cxx.cd(2)->SetLogz(kTRUE);
	cxx.cd(2)->SetMargin(0.15, 0.13, 0.15, 0.12);
	dxVT1GTx.SetStats(kFALSE);
	dxVT1GTx.SetTitle("VTPC1 - GTPC");
	dxVT1GTx.GetXaxis()->SetTitle("x [cm]");
	dxVT1GTx.GetXaxis()->SetTitleSize(0.07);
	dxVT1GTx.GetXaxis()->SetLabelSize(0.05);
	dxVT1GTx.GetYaxis()->SetTitle("dx [cm]");
	dxVT1GTx.GetYaxis()->SetTitleSize(0.07);
	dxVT1GTx.GetYaxis()->SetLabelSize(0.05);
	dxVT1GTx.GetZaxis()->SetLabelSize(0.05);
	dxVT1GTx.Draw("colz");
	
	cxx.cd(3)->SetLogz(kTRUE);
	cxx.cd(3)->SetMargin(0.15, 0.13, 0.15, 0.12);
	dxVT1MTLRx.SetStats(kFALSE);
	dxVT1MTLRx.SetTitle("VTPC1 - MTPCL/R");
	dxVT1MTLRx.GetXaxis()->SetTitle("x [cm]");
	dxVT1MTLRx.GetXaxis()->SetTitleSize(0.07);
	dxVT1MTLRx.GetXaxis()->SetLabelSize(0.05);
	dxVT1MTLRx.GetYaxis()->SetTitle("dx [cm]");
	dxVT1MTLRx.GetYaxis()->SetTitleSize(0.07);
	dxVT1MTLRx.GetYaxis()->SetLabelSize(0.05);
	dxVT1MTLRx.GetZaxis()->SetLabelSize(0.05);
	dxVT1MTLRx.Draw("colz");
	
	cxx.cd(4)->SetLogz(kTRUE);
	cxx.cd(4)->SetMargin(0.15, 0.13, 0.15, 0.12);
	dxVT2GTx.SetStats(kFALSE);
	dxVT2GTx.SetTitle("VTPC2 - GTPC");
	dxVT2GTx.GetXaxis()->SetTitle("x [cm]");
	dxVT2GTx.GetXaxis()->SetTitleSize(0.07);
	dxVT2GTx.GetXaxis()->SetLabelSize(0.05);
	dxVT2GTx.GetYaxis()->SetTitle("dx [cm]");
	dxVT2GTx.GetYaxis()->SetTitleSize(0.07);
	dxVT2GTx.GetYaxis()->SetLabelSize(0.05);
	dxVT2GTx.GetZaxis()->SetLabelSize(0.05);
	dxVT2GTx.Draw("colz");
	
	cxx.cd(5)->SetLogz(kTRUE);
	cxx.cd(5)->SetMargin(0.15, 0.13, 0.15, 0.12);
	dxVT2MTLRx.SetStats(kFALSE);
	dxVT2MTLRx.SetTitle("VTPC2 - MTPCL/R");
	dxVT2MTLRx.GetXaxis()->SetTitle("x [cm]");
	dxVT2MTLRx.GetXaxis()->SetTitleSize(0.07);
	dxVT2MTLRx.GetXaxis()->SetLabelSize(0.05);
	dxVT2MTLRx.GetYaxis()->SetTitle("dx [cm]");
	dxVT2MTLRx.GetYaxis()->SetTitleSize(0.07);
	dxVT2MTLRx.GetYaxis()->SetLabelSize(0.05);
	dxVT2MTLRx.GetZaxis()->SetLabelSize(0.05);
	dxVT2MTLRx.Draw("colz");
	
	
	string cxxname = add + "xxalign.pdf";
	cxx.Print(cxxname.c_str());
	
	TCanvas cyx("cyx", "", 1600, 1000);
	cyx.Divide(3,2);
	cyx.cd(1)->SetLogz(kTRUE);
	cyx.cd(1)->SetMargin(0.15, 0.13, 0.15, 0.12);
	dyVT12x.SetStats(kFALSE);
	dyVT12x.SetTitle("VTPC1 - VTPC2");
	dyVT12x.GetXaxis()->SetTitle("x [cm]");
	dyVT12x.GetXaxis()->SetTitleSize(0.07);
	dyVT12x.GetXaxis()->SetLabelSize(0.05);
	dyVT12x.GetYaxis()->SetTitle("dy [cm]");
	dyVT12x.GetYaxis()->SetTitleSize(0.07);
	dyVT12x.GetYaxis()->SetLabelSize(0.05);
	dyVT12x.GetZaxis()->SetLabelSize(0.05);
	dyVT12x.Draw("colz");
	
	cyx.cd(2)->SetLogz(kTRUE);
	cyx.cd(2)->SetMargin(0.15, 0.13, 0.15, 0.12);
	dyVT1GTx.SetStats(kFALSE);
	dyVT1GTx.SetTitle("VTPC1 - GTPC");
	dyVT1GTx.GetXaxis()->SetTitle("x [cm]");
	dyVT1GTx.GetXaxis()->SetTitleSize(0.07);
	dyVT1GTx.GetXaxis()->SetLabelSize(0.05);
	dyVT1GTx.GetYaxis()->SetTitle("dy [cm]");
	dyVT1GTx.GetYaxis()->SetTitleSize(0.07);
	dyVT1GTx.GetYaxis()->SetLabelSize(0.05);
	dyVT1GTx.GetZaxis()->SetLabelSize(0.05);
	dyVT1GTx.Draw("colz");
	
	cyx.cd(3)->SetLogz(kTRUE);
	cyx.cd(3)->SetMargin(0.15, 0.13, 0.15, 0.12);
	dyVT1MTLRx.SetStats(kFALSE);
	dyVT1MTLRx.SetTitle("VTPC1 - MTPCL/R");
	dyVT1MTLRx.GetXaxis()->SetTitle("x [cm]");
	dyVT1MTLRx.GetXaxis()->SetTitleSize(0.07);
	dyVT1MTLRx.GetXaxis()->SetLabelSize(0.05);
	dyVT1MTLRx.GetYaxis()->SetTitle("dy [cm]");
	dyVT1MTLRx.GetYaxis()->SetTitleSize(0.07);
	dyVT1MTLRx.GetYaxis()->SetLabelSize(0.05);
	dyVT1MTLRx.GetZaxis()->SetLabelSize(0.05);
	dyVT1MTLRx.Draw("colz");
	
	cyx.cd(4)->SetLogz(kTRUE);
	cyx.cd(4)->SetMargin(0.15, 0.13, 0.15, 0.12);
	dyVT2GTx.SetStats(kFALSE);
	dyVT2GTx.SetTitle("VTPC2 - GTPC");
	dyVT2GTx.GetXaxis()->SetTitle("x [cm]");
	dyVT2GTx.GetXaxis()->SetTitleSize(0.07);
	dyVT2GTx.GetXaxis()->SetLabelSize(0.05);
	dyVT2GTx.GetYaxis()->SetTitle("dy [cm]");
	dyVT2GTx.GetYaxis()->SetTitleSize(0.07);
	dyVT2GTx.GetYaxis()->SetLabelSize(0.05);
	dyVT2GTx.GetZaxis()->SetLabelSize(0.05);
	dyVT2GTx.Draw("colz");
	
	cyx.cd(5)->SetLogz(kTRUE);
	cyx.cd(5)->SetMargin(0.15, 0.13, 0.15, 0.12);
	dyVT2MTLRx.SetStats(kFALSE);
	dyVT2MTLRx.SetTitle("VTPC2 - MTPCL/R");
	dyVT2MTLRx.GetXaxis()->SetTitle("x [cm]");
	dyVT2MTLRx.GetXaxis()->SetTitleSize(0.07);
	dyVT2MTLRx.GetXaxis()->SetLabelSize(0.05);
	dyVT2MTLRx.GetYaxis()->SetTitle("dy [cm]");
	dyVT2MTLRx.GetYaxis()->SetTitleSize(0.07);
	dyVT2MTLRx.GetYaxis()->SetLabelSize(0.05);
	dyVT2MTLRx.GetZaxis()->SetLabelSize(0.05);
	dyVT2MTLRx.Draw("colz");
	
	string cyxname = add + "yxalign.pdf";
	cyx.Print(cyxname.c_str());
	
	TCanvas cxy("cxy", "", 1600, 1000);
	cxy.Divide(3,2);
	cxy.cd(1)->SetLogz(kTRUE);
	cxy.cd(1)->SetMargin(0.15, 0.13, 0.15, 0.12);
	dxVT12y.SetStats(kFALSE);
	dxVT12y.SetTitle("VTPC1 - VTPC2");
	dxVT12y.GetXaxis()->SetTitle("y [cm]");
	dxVT12y.GetXaxis()->SetTitleSize(0.07);
	dxVT12y.GetXaxis()->SetLabelSize(0.05);
	dxVT12y.GetYaxis()->SetTitle("dx [cm]");
	dxVT12y.GetYaxis()->SetTitleSize(0.07);
	dxVT12y.GetYaxis()->SetLabelSize(0.05);
	dxVT12y.GetZaxis()->SetLabelSize(0.05);
	dxVT12y.Draw("colz");
	
	cxy.cd(2)->SetLogz(kTRUE);
	cxy.cd(2)->SetMargin(0.15, 0.13, 0.15, 0.12);
	dxVT1GTy.SetStats(kFALSE);
	dxVT1GTy.SetTitle("VTPC1 - GTPC");
	dxVT1GTy.GetXaxis()->SetTitle("y [cm]");
	dxVT1GTy.GetXaxis()->SetTitleSize(0.07);
	dxVT1GTy.GetXaxis()->SetLabelSize(0.05);
	dxVT1GTy.GetYaxis()->SetTitle("dx [cm]");
	dxVT1GTy.GetYaxis()->SetTitleSize(0.07);
	dxVT1GTy.GetYaxis()->SetLabelSize(0.05);
	dxVT1GTy.GetZaxis()->SetLabelSize(0.05);
	dxVT1GTy.Draw("colz");
	
	cxy.cd(3)->SetLogz(kTRUE);
	cxy.cd(3)->SetMargin(0.15, 0.13, 0.15, 0.12);
	dxVT1MTLRy.SetStats(kFALSE);
	dxVT1MTLRy.SetTitle("VTPC1 - MTPCL/R");
	dxVT1MTLRy.GetXaxis()->SetTitle("y [cm]");
	dxVT1MTLRy.GetXaxis()->SetTitleSize(0.07);
	dxVT1MTLRy.GetXaxis()->SetLabelSize(0.05);
	dxVT1MTLRy.GetYaxis()->SetTitle("dx [cm]");
	dxVT1MTLRy.GetYaxis()->SetTitleSize(0.07);
	dxVT1MTLRy.GetYaxis()->SetLabelSize(0.05);
	dxVT1MTLRy.GetZaxis()->SetLabelSize(0.05);
	dxVT1MTLRy.Draw("colz");
	
	cxy.cd(4)->SetLogz(kTRUE);
	cxy.cd(4)->SetMargin(0.15, 0.13, 0.15, 0.12);
	dxVT2GTy.SetStats(kFALSE);
	dxVT2GTy.SetTitle("VTPC2 - GTPC");
	dxVT2GTy.GetXaxis()->SetTitle("y [cm]");
	dxVT2GTy.GetXaxis()->SetTitleSize(0.07);
	dxVT2GTy.GetXaxis()->SetLabelSize(0.05);
	dxVT2GTy.GetYaxis()->SetTitle("dx [cm]");
	dxVT2GTy.GetYaxis()->SetTitleSize(0.07);
	dxVT2GTy.GetYaxis()->SetLabelSize(0.05);
	dxVT2GTy.GetZaxis()->SetLabelSize(0.05);
	dxVT2GTy.Draw("colz");
	
	cxy.cd(5)->SetLogz(kTRUE);
	cxy.cd(5)->SetMargin(0.15, 0.13, 0.15, 0.12);
	dxVT2MTLRy.SetStats(kFALSE);
	dxVT2MTLRy.SetTitle("VTPC2 - MTPCL/R");
	dxVT2MTLRy.GetXaxis()->SetTitle("y [cm]");
	dxVT2MTLRy.GetXaxis()->SetTitleSize(0.07);
	dxVT2MTLRy.GetXaxis()->SetLabelSize(0.05);
	dxVT2MTLRy.GetYaxis()->SetTitle("dx [cm]");
	dxVT2MTLRy.GetYaxis()->SetTitleSize(0.07);
	dxVT2MTLRy.GetYaxis()->SetLabelSize(0.05);
	dxVT2MTLRy.GetZaxis()->SetLabelSize(0.05);
	dxVT2MTLRy.Draw("colz");
	
	
	string cxyname = add + "xyalign.pdf";
	cxy.Print(cxyname.c_str());
	
	TCanvas cyy("cyy", "", 1600, 1000);
	cyy.Divide(3,2);
	cyy.cd(1)->SetLogz(kTRUE);
	cyy.cd(1)->SetMargin(0.15, 0.13, 0.15, 0.12);
	dyVT12y.SetStats(kFALSE);
	dyVT12y.SetTitle("VTPC1 - VTPC2");
	dyVT12y.GetXaxis()->SetTitle("y [cm]");
	dyVT12y.GetXaxis()->SetTitleSize(0.07);
	dyVT12y.GetXaxis()->SetLabelSize(0.05);
	dyVT12y.GetYaxis()->SetTitle("dy [cm]");
	dyVT12y.GetYaxis()->SetTitleSize(0.07);
	dyVT12y.GetYaxis()->SetLabelSize(0.05);
	dyVT12y.GetZaxis()->SetLabelSize(0.05);
	dyVT12y.Draw("colz");
	
	cyy.cd(2)->SetLogz(kTRUE);
	cyy.cd(2)->SetMargin(0.15, 0.13, 0.15, 0.12);
	dyVT1GTy.SetStats(kFALSE);
	dyVT1GTy.SetTitle("VTPC1 - GTPC");
	dyVT1GTy.GetXaxis()->SetTitle("y [cm]");
	dyVT1GTy.GetXaxis()->SetTitleSize(0.07);
	dyVT1GTy.GetXaxis()->SetLabelSize(0.05);
	dyVT1GTy.GetYaxis()->SetTitle("dy [cm]");
	dyVT1GTy.GetYaxis()->SetTitleSize(0.07);
	dyVT1GTy.GetYaxis()->SetLabelSize(0.05);
	dyVT1GTy.GetZaxis()->SetLabelSize(0.05);
	dyVT1GTy.Draw("colz");
	
	cyy.cd(3)->SetLogz(kTRUE);
	cyy.cd(3)->SetMargin(0.15, 0.13, 0.15, 0.12);
	dyVT1MTLRy.SetStats(kFALSE);
	dyVT1MTLRy.SetTitle("VTPC1 - MTPCL/R");
	dyVT1MTLRy.GetXaxis()->SetTitle("y [cm]");
	dyVT1MTLRy.GetXaxis()->SetTitleSize(0.07);
	dyVT1MTLRy.GetXaxis()->SetLabelSize(0.05);
	dyVT1MTLRy.GetYaxis()->SetTitle("dy [cm]");
	dyVT1MTLRy.GetYaxis()->SetTitleSize(0.07);
	dyVT1MTLRy.GetYaxis()->SetLabelSize(0.05);
	dyVT1MTLRy.GetZaxis()->SetLabelSize(0.05);
	dyVT1MTLRy.Draw("colz");
	
	cyy.cd(4)->SetLogz(kTRUE);
	cyy.cd(4)->SetMargin(0.15, 0.13, 0.15, 0.12);
	dyVT2GTy.SetStats(kFALSE);
	dyVT2GTy.SetTitle("VTPC2 - GTPC");
	dyVT2GTy.GetXaxis()->SetTitle("y [cm]");
	dyVT2GTy.GetXaxis()->SetTitleSize(0.07);
	dyVT2GTy.GetXaxis()->SetLabelSize(0.05);
	dyVT2GTy.GetYaxis()->SetTitle("dy [cm]");
	dyVT2GTy.GetYaxis()->SetTitleSize(0.07);
	dyVT2GTy.GetYaxis()->SetLabelSize(0.05);
	dyVT2GTy.GetZaxis()->SetLabelSize(0.05);
	dyVT2GTy.Draw("colz");
	
	cyy.cd(5)->SetLogz(kTRUE);
	cyy.cd(5)->SetMargin(0.15, 0.13, 0.15, 0.12);
	dyVT2MTLRy.SetStats(kFALSE);
	dyVT2MTLRy.SetTitle("VTPC2 - MTPCL/R");
	dyVT2MTLRy.GetXaxis()->SetTitle("y [cm]");
	dyVT2MTLRy.GetXaxis()->SetTitleSize(0.07);
	dyVT2MTLRy.GetXaxis()->SetLabelSize(0.05);
	dyVT2MTLRy.GetYaxis()->SetTitle("dy [cm]");
	dyVT2MTLRy.GetYaxis()->SetTitleSize(0.07);
	dyVT2MTLRy.GetYaxis()->SetLabelSize(0.05);
	dyVT2MTLRy.GetZaxis()->SetLabelSize(0.05);
	dyVT2MTLRy.Draw("colz");
	
	string cyyname = add + "yyalign.pdf";
	cyy.Print(cyyname.c_str());
	
	
	
	TCanvas ctxx("ctxx", "", 1600, 1000);
	ctxx.Divide(3,2);
	ctxx.cd(1)->SetLogz(kTRUE);
	ctxx.cd(1)->SetMargin(0.15, 0.13, 0.15, 0.12);
	dtxVT12x.SetStats(kFALSE);
	dtxVT12x.SetTitle("VTPC1 - VTPC2");
	dtxVT12x.GetXaxis()->SetTitle("x [cm]");
	dtxVT12x.GetXaxis()->SetTitleSize(0.07);
	dtxVT12x.GetXaxis()->SetLabelSize(0.05);
	dtxVT12x.GetYaxis()->SetTitle("dt_{x}");
	dtxVT12x.GetYaxis()->SetTitleSize(0.07);
	dtxVT12x.GetYaxis()->SetLabelSize(0.05);
	dtxVT12x.GetZaxis()->SetLabelSize(0.05);
	dtxVT12x.Draw("colz");
	
	ctxx.cd(2)->SetLogz(kTRUE);
	ctxx.cd(2)->SetMargin(0.15, 0.13, 0.15, 0.12);
	dtxVT1GTx.SetStats(kFALSE);
	dtxVT1GTx.SetTitle("VTPC1 - GTPC");
	dtxVT1GTx.GetXaxis()->SetTitle("x [cm]");
	dtxVT1GTx.GetXaxis()->SetTitleSize(0.07);
	dtxVT1GTx.GetXaxis()->SetLabelSize(0.05);
	dtxVT1GTx.GetYaxis()->SetTitle("dt_{x}");
	dtxVT1GTx.GetYaxis()->SetTitleSize(0.07);
	dtxVT1GTx.GetYaxis()->SetLabelSize(0.05);
	dtxVT1GTx.GetZaxis()->SetLabelSize(0.05);
	dtxVT1GTx.Draw("colz");
	
	ctxx.cd(3)->SetLogz(kTRUE);
	ctxx.cd(3)->SetMargin(0.15, 0.13, 0.15, 0.12);
	dtxVT1MTLRx.SetStats(kFALSE);
	dtxVT1MTLRx.SetTitle("VTPC1 - MTPCL/R");
	dtxVT1MTLRx.GetXaxis()->SetTitle("x [cm]");
	dtxVT1MTLRx.GetXaxis()->SetTitleSize(0.07);
	dtxVT1MTLRx.GetXaxis()->SetLabelSize(0.05);
	dtxVT1MTLRx.GetYaxis()->SetTitle("dt_{x}");
	dtxVT1MTLRx.GetYaxis()->SetTitleSize(0.07);
	dtxVT1MTLRx.GetYaxis()->SetLabelSize(0.05);
	dtxVT1MTLRx.GetZaxis()->SetLabelSize(0.05);
	dtxVT1MTLRx.Draw("colz");
	
	ctxx.cd(4)->SetLogz(kTRUE);
	ctxx.cd(4)->SetMargin(0.15, 0.13, 0.15, 0.12);
	dtxVT2GTx.SetStats(kFALSE);
	dtxVT2GTx.SetTitle("VTPC2 - GTPC");
	dtxVT2GTx.GetXaxis()->SetTitle("x [cm]");
	dtxVT2GTx.GetXaxis()->SetTitleSize(0.07);
	dtxVT2GTx.GetXaxis()->SetLabelSize(0.05);
	dtxVT2GTx.GetYaxis()->SetTitle("dt_{x}");
	dtxVT2GTx.GetYaxis()->SetTitleSize(0.07);
	dtxVT2GTx.GetYaxis()->SetLabelSize(0.05);
	dtxVT2GTx.GetZaxis()->SetLabelSize(0.05);
	dtxVT2GTx.Draw("colz");
	
	ctxx.cd(5)->SetLogz(kTRUE);
	ctxx.cd(5)->SetMargin(0.15, 0.13, 0.15, 0.12);
	dtxVT2MTLRx.SetStats(kFALSE);
	dtxVT2MTLRx.SetTitle("VTPC2 - MTPCL/R");
	dtxVT2MTLRx.GetXaxis()->SetTitle("x [cm]");
	dtxVT2MTLRx.GetXaxis()->SetTitleSize(0.07);
	dtxVT2MTLRx.GetXaxis()->SetLabelSize(0.05);
	dtxVT2MTLRx.GetYaxis()->SetTitle("dt_{x}");
	dtxVT2MTLRx.GetYaxis()->SetTitleSize(0.07);
	dtxVT2MTLRx.GetYaxis()->SetLabelSize(0.05);
	dtxVT2MTLRx.GetZaxis()->SetLabelSize(0.05);
	dtxVT2MTLRx.Draw("colz");
	
	
	string ctxxname = add + "txxalign.pdf";
	ctxx.Print(ctxxname.c_str());
	
	TCanvas ctyx("ctyx", "", 1600, 1000);
	ctyx.Divide(3,2);
	ctyx.cd(1)->SetLogz(kTRUE);
	ctyx.cd(1)->SetMargin(0.15, 0.13, 0.15, 0.12);
	dtyVT12x.SetStats(kFALSE);
	dtyVT12x.SetTitle("VTPC1 - VTPC2");
	dtyVT12x.GetXaxis()->SetTitle("x [cm]");
	dtyVT12x.GetXaxis()->SetTitleSize(0.07);
	dtyVT12x.GetXaxis()->SetLabelSize(0.05);
	dtyVT12x.GetYaxis()->SetTitle("dt_{y}");
	dtyVT12x.GetYaxis()->SetTitleSize(0.07);
	dtyVT12x.GetYaxis()->SetLabelSize(0.05);
	dtyVT12x.GetZaxis()->SetLabelSize(0.05);
	dtyVT12x.Draw("colz");
	
	ctyx.cd(2)->SetLogz(kTRUE);
	ctyx.cd(2)->SetMargin(0.15, 0.13, 0.15, 0.12);
	dtyVT1GTx.SetStats(kFALSE);
	dtyVT1GTx.SetTitle("VTPC1 - GTPC");
	dtyVT1GTx.GetXaxis()->SetTitle("x [cm]");
	dtyVT1GTx.GetXaxis()->SetTitleSize(0.07);
	dtyVT1GTx.GetXaxis()->SetLabelSize(0.05);
	dtyVT1GTx.GetYaxis()->SetTitle("dt_{y}");
	dtyVT1GTx.GetYaxis()->SetTitleSize(0.07);
	dtyVT1GTx.GetYaxis()->SetLabelSize(0.05);
	dtyVT1GTx.GetZaxis()->SetLabelSize(0.05);
	dtyVT1GTx.Draw("colz");
	
	ctyx.cd(3)->SetLogz(kTRUE);
	ctyx.cd(3)->SetMargin(0.15, 0.13, 0.15, 0.12);
	dtyVT1MTLRx.SetStats(kFALSE);
	dtyVT1MTLRx.SetTitle("VTPC1 - MTPCL/R");
	dtyVT1MTLRx.GetXaxis()->SetTitle("x [cm]");
	dtyVT1MTLRx.GetXaxis()->SetTitleSize(0.07);
	dtyVT1MTLRx.GetXaxis()->SetLabelSize(0.05);
	dtyVT1MTLRx.GetYaxis()->SetTitle("dt_{y}");
	dtyVT1MTLRx.GetYaxis()->SetTitleSize(0.07);
	dtyVT1MTLRx.GetYaxis()->SetLabelSize(0.05);
	dtyVT1MTLRx.GetZaxis()->SetLabelSize(0.05);
	dtyVT1MTLRx.Draw("colz");
	
	ctyx.cd(4)->SetLogz(kTRUE);
	ctyx.cd(4)->SetMargin(0.15, 0.13, 0.15, 0.12);
	dtyVT2GTx.SetStats(kFALSE);
	dtyVT2GTx.SetTitle("VTPC2 - GTPC");
	dtyVT2GTx.GetXaxis()->SetTitle("x [cm]");
	dtyVT2GTx.GetXaxis()->SetTitleSize(0.07);
	dtyVT2GTx.GetXaxis()->SetLabelSize(0.05);
	dtyVT2GTx.GetYaxis()->SetTitle("dt_{y}");
	dtyVT2GTx.GetYaxis()->SetTitleSize(0.07);
	dtyVT2GTx.GetYaxis()->SetLabelSize(0.05);
	dtyVT2GTx.GetZaxis()->SetLabelSize(0.05);
	dtyVT2GTx.Draw("colz");
	
	ctyx.cd(5)->SetLogz(kTRUE);
	ctyx.cd(5)->SetMargin(0.15, 0.13, 0.15, 0.12);
	dtyVT2MTLRx.SetStats(kFALSE);
	dtyVT2MTLRx.SetTitle("VTPC2 - MTPCL/R");
	dtyVT2MTLRx.GetXaxis()->SetTitle("x [cm]");
	dtyVT2MTLRx.GetXaxis()->SetTitleSize(0.07);
	dtyVT2MTLRx.GetXaxis()->SetLabelSize(0.05);
	dtyVT2MTLRx.GetYaxis()->SetTitle("dt_{y}");
	dtyVT2MTLRx.GetYaxis()->SetTitleSize(0.07);
	dtyVT2MTLRx.GetYaxis()->SetLabelSize(0.05);
	dtyVT2MTLRx.GetZaxis()->SetLabelSize(0.05);
	dtyVT2MTLRx.Draw("colz");
	
	string ctyxname = add + "tyxalign.pdf";
	ctyx.Print(ctyxname.c_str());
	
	TCanvas ctxy("ctxy", "", 1600, 1000);
	ctxy.Divide(3,2);
	ctxy.cd(1)->SetLogz(kTRUE);
	ctxy.cd(1)->SetMargin(0.15, 0.13, 0.15, 0.12);
	dtxVT12y.SetStats(kFALSE);
	dtxVT12y.SetTitle("VTPC1 - VTPC2");
	dtxVT12y.GetXaxis()->SetTitle("y [cm]");
	dtxVT12y.GetXaxis()->SetTitleSize(0.07);
	dtxVT12y.GetXaxis()->SetLabelSize(0.05);
	dtxVT12y.GetYaxis()->SetTitle("dt_{x}");
	dtxVT12y.GetYaxis()->SetTitleSize(0.07);
	dtxVT12y.GetYaxis()->SetLabelSize(0.05);
	dtxVT12y.GetZaxis()->SetLabelSize(0.05);
	dtxVT12y.Draw("colz");
	
	ctxy.cd(2)->SetLogz(kTRUE);
	ctxy.cd(2)->SetMargin(0.15, 0.13, 0.15, 0.12);
	dtxVT1GTy.SetStats(kFALSE);
	dtxVT1GTy.SetTitle("VTPC1 - GTPC");
	dtxVT1GTy.GetXaxis()->SetTitle("y [cm]");
	dtxVT1GTy.GetXaxis()->SetTitleSize(0.07);
	dtxVT1GTy.GetXaxis()->SetLabelSize(0.05);
	dtxVT1GTy.GetYaxis()->SetTitle("dt_{x}");
	dtxVT1GTy.GetYaxis()->SetTitleSize(0.07);
	dtxVT1GTy.GetYaxis()->SetLabelSize(0.05);
	dtxVT1GTy.GetZaxis()->SetLabelSize(0.05);
	dtxVT1GTy.Draw("colz");
	
	ctxy.cd(3)->SetLogz(kTRUE);
	ctxy.cd(3)->SetMargin(0.15, 0.13, 0.15, 0.12);
	dtxVT1MTLRy.SetStats(kFALSE);
	dtxVT1MTLRy.SetTitle("VTPC1 - MTPCL/R");
	dtxVT1MTLRy.GetXaxis()->SetTitle("y [cm]");
	dtxVT1MTLRy.GetXaxis()->SetTitleSize(0.07);
	dtxVT1MTLRy.GetXaxis()->SetLabelSize(0.05);
	dtxVT1MTLRy.GetYaxis()->SetTitle("dt_{x}");
	dtxVT1MTLRy.GetYaxis()->SetTitleSize(0.07);
	dtxVT1MTLRy.GetYaxis()->SetLabelSize(0.05);
	dtxVT1MTLRy.GetZaxis()->SetLabelSize(0.05);
	dtxVT1MTLRy.Draw("colz");
	
	ctxy.cd(4)->SetLogz(kTRUE);
	ctxy.cd(4)->SetMargin(0.15, 0.13, 0.15, 0.12);
	dtxVT2GTy.SetStats(kFALSE);
	dtxVT2GTy.SetTitle("VTPC2 - GTPC");
	dtxVT2GTy.GetXaxis()->SetTitle("y [cm]");
	dtxVT2GTy.GetXaxis()->SetTitleSize(0.07);
	dtxVT2GTy.GetXaxis()->SetLabelSize(0.05);
	dtxVT2GTy.GetYaxis()->SetTitle("dt_{x}");
	dtxVT2GTy.GetYaxis()->SetTitleSize(0.07);
	dtxVT2GTy.GetYaxis()->SetLabelSize(0.05);
	dtxVT2GTy.GetZaxis()->SetLabelSize(0.05);
	dtxVT2GTy.Draw("colz");
	
	ctxy.cd(5)->SetLogz(kTRUE);
	ctxy.cd(5)->SetMargin(0.15, 0.13, 0.15, 0.12);
	dtxVT2MTLRy.SetStats(kFALSE);
	dtxVT2MTLRy.SetTitle("VTPC2 - MTPCL/R");
	dtxVT2MTLRy.GetXaxis()->SetTitle("y [cm]");
	dtxVT2MTLRy.GetXaxis()->SetTitleSize(0.07);
	dtxVT2MTLRy.GetXaxis()->SetLabelSize(0.05);
	dtxVT2MTLRy.GetYaxis()->SetTitle("dt_{x}");
	dtxVT2MTLRy.GetYaxis()->SetTitleSize(0.07);
	dtxVT2MTLRy.GetYaxis()->SetLabelSize(0.05);
	dtxVT2MTLRy.GetZaxis()->SetLabelSize(0.05);
	dtxVT2MTLRy.Draw("colz");
	
	
	string ctxyname = add + "txyalign.pdf";
	ctxy.Print(ctxyname.c_str());
	
	TCanvas ctyy("ctyy", "", 1600, 1000);
	ctyy.Divide(3,2);
	ctyy.cd(1)->SetLogz(kTRUE);
	ctyy.cd(1)->SetMargin(0.15, 0.13, 0.15, 0.12);
	dtyVT12y.SetStats(kFALSE);
	dtyVT12y.SetTitle("VTPC1 - VTPC2");
	dtyVT12y.GetXaxis()->SetTitle("y [cm]");
	dtyVT12y.GetXaxis()->SetTitleSize(0.07);
	dtyVT12y.GetXaxis()->SetLabelSize(0.05);
	dtyVT12y.GetYaxis()->SetTitle("dt_{y}");
	dtyVT12y.GetYaxis()->SetTitleSize(0.07);
	dtyVT12y.GetYaxis()->SetLabelSize(0.05);
	dtyVT12y.GetZaxis()->SetLabelSize(0.05);
	dtyVT12y.Draw("colz");
	
	ctyy.cd(2)->SetLogz(kTRUE);
	ctyy.cd(2)->SetMargin(0.15, 0.13, 0.15, 0.12);
	dtyVT1GTy.SetStats(kFALSE);
	dtyVT1GTy.SetTitle("VTPC1 - GTPC");
	dtyVT1GTy.GetXaxis()->SetTitle("y [cm]");
	dtyVT1GTy.GetXaxis()->SetTitleSize(0.07);
	dtyVT1GTy.GetXaxis()->SetLabelSize(0.05);
	dtyVT1GTy.GetYaxis()->SetTitle("dt_{y}");
	dtyVT1GTy.GetYaxis()->SetTitleSize(0.07);
	dtyVT1GTy.GetYaxis()->SetLabelSize(0.05);
	dtyVT1GTy.GetZaxis()->SetLabelSize(0.05);
	dtyVT1GTy.Draw("colz");
	
	ctyy.cd(3)->SetLogz(kTRUE);
	ctyy.cd(3)->SetMargin(0.15, 0.13, 0.15, 0.12);
	dtyVT1MTLRy.SetStats(kFALSE);
	dtyVT1MTLRy.SetTitle("VTPC1 - MTPCL/R");
	dtyVT1MTLRy.GetXaxis()->SetTitle("y [cm]");
	dtyVT1MTLRy.GetXaxis()->SetTitleSize(0.07);
	dtyVT1MTLRy.GetXaxis()->SetLabelSize(0.05);
	dtyVT1MTLRy.GetYaxis()->SetTitle("dt_{y}");
	dtyVT1MTLRy.GetYaxis()->SetTitleSize(0.07);
	dtyVT1MTLRy.GetYaxis()->SetLabelSize(0.05);
	dtyVT1MTLRy.GetZaxis()->SetLabelSize(0.05);
	dtyVT1MTLRy.Draw("colz");
	
	ctyy.cd(4)->SetLogz(kTRUE);
	ctyy.cd(4)->SetMargin(0.15, 0.13, 0.15, 0.12);
	dtyVT2GTy.SetStats(kFALSE);
	dtyVT2GTy.SetTitle("VTPC2 - GTPC");
	dtyVT2GTy.GetXaxis()->SetTitle("y [cm]");
	dtyVT2GTy.GetXaxis()->SetTitleSize(0.07);
	dtyVT2GTy.GetXaxis()->SetLabelSize(0.05);
	dtyVT2GTy.GetYaxis()->SetTitle("dt_{y}");
	dtyVT2GTy.GetYaxis()->SetTitleSize(0.07);
	dtyVT2GTy.GetYaxis()->SetLabelSize(0.05);
	dtyVT2GTy.GetZaxis()->SetLabelSize(0.05);
	dtyVT2GTy.Draw("colz");
	
	ctyy.cd(5)->SetLogz(kTRUE);
	ctyy.cd(5)->SetMargin(0.15, 0.13, 0.15, 0.12);
	dtyVT2MTLRy.SetStats(kFALSE);
	dtyVT2MTLRy.SetTitle("VTPC2 - MTPCL/R");
	dtyVT2MTLRy.GetXaxis()->SetTitle("y [cm]");
	dtyVT2MTLRy.GetXaxis()->SetTitleSize(0.07);
	dtyVT2MTLRy.GetXaxis()->SetLabelSize(0.05);
	dtyVT2MTLRy.GetYaxis()->SetTitle("dt_{y}");
	dtyVT2MTLRy.GetYaxis()->SetTitleSize(0.07);
	dtyVT2MTLRy.GetYaxis()->SetLabelSize(0.05);
	dtyVT2MTLRy.GetZaxis()->SetLabelSize(0.05);
	dtyVT2MTLRy.Draw("colz");
	
	string ctyyname = add + "tyyalign.pdf";
	ctyy.Print(ctyyname.c_str());
	//outFile.Close();
	string outName = add + "align.root";
	TFile outFile(outName.c_str(), "RECREATE");	
	outFile.cd();
	dxVT12x.Write();
	dxVT2GTx.Write();
	dxVT2MTLRx.Write();
	dyVT12y.Write();
	dyVT2GTy.Write();
	dyVT2MTLRy.Write();
	dxVT1MTLRx.Write();
	dyVT1MTLRy.Write();
}





