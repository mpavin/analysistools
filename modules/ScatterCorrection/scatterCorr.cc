#include <vector>
#include <string>
#include <iostream>
#include "Functions.h"
#include <TH1D.h>
#include <TH2D.h>
#include <TFile.h>
#include <TChain.h>
#include <TTree.h>
#include <TMath.h>
#include <math.h>
#include <TH2Poly.h>
using namespace std;


int main(){

	int data = 1;

	string inputFile;
	string add;
	
	if(data==1){
		inputFile = "listData.txt";
		add = "data_";	
	}
	else{

		inputFile = "listMC.txt";
		add = "mc_";			
	}
	TChain chain("Data");
	
	string outName = add + "corr3.root";
	TFile outFile(outName.c_str(), "RECREATE");
	ifstream ifs;
	string line;
	ifs.open(inputFile.c_str());
		
	if (!ifs.is_open()){
		cerr << "[ReadInputListFile] Could not open infile. Exiting..." << endl;			
		exit (101);
	}
    
	while (getline(ifs, line)){
		if(line.at(0)=='#')
			continue;
		chain.Add(line.c_str());
	}
	ifs.close();		



	BeamA beamD;
	vector<double> *x = NULL;
	vector<double> *xE = NULL;
	vector<double> *y = NULL;
	vector<double> *yE = NULL;
	vector<double> *z = NULL;
	vector<double> *px = NULL;
	vector<double> *py = NULL;
	vector<double> *pz = NULL;
	vector<double> *distTarg = NULL;
	vector<double> *zStart = NULL;
	vector<double> *dEdx = NULL;
	vector<int> *qD = NULL;
	vector<int> *nVTPC1 = NULL;
	vector<int> *nVTPC2 = NULL;
	vector<int> *nGTPC = NULL;
	vector<int> *nMTPC = NULL;
	vector<int> *topology = NULL;
	vector<int> *nTOF = NULL;
	vector<double> *m2TOF = NULL;	
	vector<int> *tofScId = NULL;
	vector<int> *pdgId = NULL;
	int runNumber;
	
	TBranch *b_aData = chain.GetBranch("Beam");
 	b_aData->SetAddress(&beamD);
	chain.SetBranchAddress("x", &x);
	chain.SetBranchAddress("xE", &xE);
	chain.SetBranchAddress("y", &y);
	chain.SetBranchAddress("yE", &yE);
	chain.SetBranchAddress("z", &z);
	chain.SetBranchAddress("px", &px);
	chain.SetBranchAddress("py", &py);
	chain.SetBranchAddress("pz", &pz);
	chain.SetBranchAddress("distTarg", &distTarg);
	chain.SetBranchAddress("zStart", &zStart);
	chain.SetBranchAddress("dEdx", &dEdx);
	chain.SetBranchAddress("q", &qD);
	chain.SetBranchAddress("nVTPC1", &nVTPC1);
	chain.SetBranchAddress("nVTPC2", &nVTPC2);
	chain.SetBranchAddress("nGTPC", &nGTPC);
	chain.SetBranchAddress("nMTPC", &nMTPC);
	chain.SetBranchAddress("topology", &topology);
	chain.SetBranchAddress("nTOF", &nTOF);
	chain.SetBranchAddress("m2TOF", &m2TOF);
	chain.SetBranchAddress("tofScId", &tofScId);
	chain.SetBranchAddress("runNumber", &runNumber);
	//chain.SetBranchAddress("pdgId", &pdgId);
	
	int nEntries = chain.GetEntries();
	
	double zbor[7] = {0, 18, 36, 54, 72, 89.99, 90.01};
	/*double thbor[4] = {260, 300, 340, 380};
	int np[3] = {6, 5, 5};
	double pbor[3][7] = {{0.3, 0.675, 0.975, 1.25, 1.625, 2.225, 4.725},
						 {0.2, 0.45, 0.7, 0.95, 1.275, 2.55, 0},
						 {0.2, 0.45, 0.7, 0.95, 1.275, 2.55, 0}};*/
				
	int nth = 2;		 
	double thbor[4] = {260, 300, 380};
	int np[2] = {5, 5};
	double pbor[2][6] = {{0.5, 0.975, 1.25, 1.625, 2.225, 4.725},
						 {0.5, 0.975, 1.25, 1.625, 2.225, 4.725}};
	vector<TH2Poly*> zph;
	vector<TH2Poly*> zphTOF;
	vector<TH2Poly*> frac;


		
	for(int i = 0; i < 6; i++){
	  	string nm = "zbin" + ConvertToString(i+1., 0);
	  	string nm1 = "zbinTOF" + ConvertToString(i+1., 0);
	  	string nm2 = "frac" + ConvertToString(i+1., 0);
		string title = ConvertToString(zbor[i],0) + " #leq z < " + ConvertToString(zbor[i+1],0) + " cm";
		TH2Poly *temp;
		TH2Poly *temp1;
		TH2Poly *temp2;
		zph.push_back(temp);
		zphTOF.push_back(temp1);
		frac.push_back(temp2);
		
		zph.at(i) = new TH2Poly(nm.c_str(), title.c_str(), 0, 5.2, 260, 400);
		zphTOF.at(i) = new TH2Poly(nm1.c_str(), title.c_str(), 0, 5.2, 260, 400);
		frac.at(i) = new TH2Poly(nm2.c_str(), title.c_str(), 0, 5.2, 260, 400);

		zph.at(i)->GetXaxis()->SetTitle("p [GeV/c]");
		zph.at(i)->GetXaxis()->SetTitleOffset(0.9);
		zph.at(i)->GetXaxis()->SetTitleSize(0.06);
		zph.at(i)->GetXaxis()->SetLabelSize(0.06);
		zph.at(i)->GetYaxis()->SetTitle("#theta [mrad]");
		zph.at(i)->GetYaxis()->SetTitleOffset(0.9);
		zph.at(i)->GetYaxis()->SetTitleSize(0.06);
		zph.at(i)->GetYaxis()->SetLabelSize(0.06);
		zph.at(i)->SetStats(kFALSE);
		  
		zphTOF.at(i)->GetXaxis()->SetTitle("p [GeV/c]");
		zphTOF.at(i)->GetXaxis()->SetTitleOffset(0.9);
		zphTOF.at(i)->GetXaxis()->SetTitleSize(0.06);
		zphTOF.at(i)->GetXaxis()->SetLabelSize(0.06);
		zphTOF.at(i)->GetYaxis()->SetTitle("#theta [mrad]");
		zphTOF.at(i)->GetYaxis()->SetTitleOffset(0.9);
		zphTOF.at(i)->GetYaxis()->SetTitleSize(0.06);
		zphTOF.at(i)->GetYaxis()->SetLabelSize(0.06);
		zphTOF.at(i)->SetStats(kFALSE);
		
		frac.at(i)->GetXaxis()->SetTitle("p [GeV/c]");
		frac.at(i)->GetXaxis()->SetTitleOffset(0.9);
		frac.at(i)->GetXaxis()->SetTitleSize(0.06);
		frac.at(i)->GetXaxis()->SetLabelSize(0.06);
		frac.at(i)->GetYaxis()->SetTitle("#theta [mrad]");
		frac.at(i)->GetYaxis()->SetTitleOffset(0.9);
		frac.at(i)->GetYaxis()->SetTitleSize(0.06);
		frac.at(i)->GetYaxis()->SetLabelSize(0.06);
		frac.at(i)->SetStats(kFALSE);
		
		for(int j = 0; j < nth; j++){
			for(int k = 0; k < np[j]; k++){
				zph.at(i)->AddBin(pbor[j][k], thbor[j], pbor[j][k+1], thbor[j+1]);
				zphTOF.at(i)->AddBin(pbor[j][k], thbor[j], pbor[j][k+1], thbor[j+1]);
				frac.at(i)->AddBin(pbor[j][k], thbor[j], pbor[j][k+1], thbor[j+1]);
			}		
		}
	  
	}

	
	cout << nEntries << endl;
	for(int i = 0; i < nEntries; ++i){
		
		chain.GetEntry(i,1);
		
		if(!((i+1)%100000))
			cout << "Processing event # "  << i+1 << endl;
		
		//if(nCount > nTrack) break;	
		for(int j = 0; j < px->size(); j++){

			if(dEdx->at(j) > 1.3) continue;
			double theta = 1000*acos(pz->at(j)/sqrt(px->at(j)*px->at(j)+py->at(j)*py->at(j)+pz->at(j)*pz->at(j)));
			double p = sqrt(px->at(j)*px->at(j) + py->at(j)*py->at(j) + pz->at(j)*pz->at(j));

			if(qD->at(j) > 0) continue;
			if(qD->at(j)*px->at(j) > 0) continue;
		
			
			if(p < 0.2) continue;

			if(theta < 260) continue;
			if(theta > 400) continue;
			
			double phi = acos(px->at(j)/sqrt(px->at(j)*px->at(j)+py->at(j)*py->at(j)))*180/TMath::Pi();
			if(px->at(j)>0 && py->at(j) < 0){
				phi = -1*phi;
			}
			else if(px->at(j)<0 && py->at(j) < 0){
				phi = 360 - phi;
			}
			
			double sigmaR = sqrt(TMath::Power(x->at(j)*xE->at(j),2) + TMath::Power(y->at(j)*yE->at(j),2))/sqrt(x->at(j)*x->at(j)+y->at(j)*y->at(j));
			if(distTarg->at(j)/sigmaR > 1) continue;
				
			double zb = z->at(j) + 657.51;
			//cout << "radi2" << endl;
			int zId = -99;
			for(int k = 0; k < 6; k++){
				if(zb >= zbor[k] && zb <= zbor[k+1]){
					zId = k;
					break;
				}
			}
			if(zId < 0) continue;
			
			int thId = -99;
			for(int k = 0; k < nth; k++){
				if(theta >= thbor[k] && theta <= thbor[k+1]){
					thId = k;
					break;
				}
			}
			if(thId < 0) continue;
			
			zph.at(zId)->Fill(p, theta);
			if(nTOF->at(j) > 0) zphTOF.at(zId)->Fill(p, theta);
			
		}

	}

	outFile.cd();
	
	for(int i = 0; i < 6; i++){
		int nbins = zph.at(i)->GetNumberOfBins();
		for(int j = 0; j < nbins; j++){
			TH2PolyBin *bin1 = (TH2PolyBin*) zph.at(i)->GetBins()->At(j);
			TH2PolyBin *bin2 = (TH2PolyBin*) zphTOF.at(i)->GetBins()->At(j);
			
			double midx = 0.5*(bin1->GetXMin() + bin1->GetXMax());
			double midy = 0.5*(bin1->GetYMin() + bin1->GetYMax());
			
			double weight = 0;
			if(bin1->GetContent() > 200&&bin2->GetContent()>20){
				weight = bin2->GetContent()/bin1->GetContent();
				frac.at(i)->Fill(midx,midy,weight);
			}
			
		}
		zph.at(i)->Write();
		zphTOF.at(i)->Write();
		frac.at(i)->Write();
		
	}	

	outFile.Close();
		
}





