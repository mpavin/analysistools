#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <cstdlib>
#include <stdexcept>
//#include <TPRegexp.h>

#include <TTree.h>
#include <TFile.h>
#include <TH2D.h>
#include <evt/Event.h>
#include <evt/EventHeader.h>
#include <io/EventFileChain.h>
#include <det/TriggerConst.h>
#include <det/BPDConst.h>
#include <evt/RawEvent.h>

#include <boost/format.hpp>
#include "PlotResolution.h"
#include "PlotResolutionConfig.h"
#include <utl/Vector.h>
#include "EventSelection.h"
#include "TrackSelection.h"
#include "TrackExtrapolation.h"
#include "ResolutionHisto.h"
#include "Functions.h"

using namespace std;
using namespace io;
using namespace evt;
using namespace evt::rec;
using namespace det;
using namespace utl;
using namespace boost;


void PlotResolution(string inputFile){

	PlotResolutionConfig cfg;


	TFile input(inputFile.c_str(), "READ");
	TTree *tree = (TTree*) input.Get("MomentumResolution");

		
	ResVal resolution;
	

	string name = cfg.GetOutputDir() + "/resolution.root";
	TFile output(name.c_str(), "RECREATE");

	

	ResolutionHisto all("rst", "wst", "all", "_all");
	
	vector<ResolutionHisto> histos;
	
	for(int i = 9; i < 16; ++i){
		string add = "_" + ConvertToString(i);
		ResolutionHisto temp("rst", "wst", "all", add);
		
		histos.push_back(temp);
	}
	tree->SetBranchAddress("Resolution", &resolution);
	int nEntries = tree->GetEntries();



	for(int i = 0; i < nEntries; ++i){
		tree->GetEntry(i);
		
		if(!(i%10000)) cout << "---> Processing entry # " << i << endl;
		
		for(int j = 9; j < 16; ++j){
			if(resolution.topology != j) continue;
			if(resolution.chi2Ndf > cfg.GetChi2NdfCut(j)) continue;
			if(resolution.VTPC1 + resolution.VTPC2 < cfg.GetClusterCut(j,1)) continue;
			if(resolution.GTPC < cfg.GetClusterCut(j,2)) continue;
			if(resolution.MTPCL + resolution.MTPCR  < cfg.GetClusterCut(j,3)) continue;
			//cout << j - 9 << endl;	
			if(resolution.RST){
				if(cfg.PlotTopologies())
					histos.at(j-9).Fill(1, resolution.p, resolution.resolution);
				if(cfg.PlotAll())
					all.Fill(1, resolution.p, resolution.resolution);
			}
			else{
				if(cfg.PlotTopologies())
					histos.at(j-9).Fill(2, resolution.p, resolution.resolution);
				if(cfg.PlotAll())
					all.Fill(2, resolution.p, resolution.resolution);				
			}			
		}

	}

	if(cfg.PlotAll()){
		all.GetRSTHisto()->Write();
		all.GetWSTHisto()->Write();
		all.GetAllHisto()->Write();
	
		if(all.GetRSTCanvas() != NULL){
			all.GetRSTCanvas()->Write();
		}
		if(all.GetWSTCanvas() != NULL){
			all.GetWSTCanvas()->Write();
		}
		if(all.GetAllCanvas() != NULL){
			all.GetAllCanvas()->Write();
		}
	}
	
	if(cfg.PlotTopologies()){
		for(int i = 0; i < 7; ++i){
			histos.at(i).GetRSTHisto()->Write();
			histos.at(i).GetWSTHisto()->Write();
			histos.at(i).GetAllHisto()->Write();
	
			if(histos.at(i).GetRSTCanvas() != NULL){
				histos.at(i).GetRSTCanvas()->Write();
			}
			if(histos.at(i).GetWSTCanvas() != NULL){
				histos.at(i).GetWSTCanvas()->Write();
			}
			if(histos.at(i).GetAllCanvas() != NULL){
				histos.at(i).GetAllCanvas()->Write();
			}	
	
		}
	}
	output.Close();
	input.Close();
	

}

