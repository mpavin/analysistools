#include <stdlib.h>
#include "PlotResolutionConfig.h"
#include <fstream>
#include <fwk/CentralConfig.h>
#include <utl/Branch.h>
#include <utl/BranchIterator.h>
#include "CConfig.h"

using namespace std;
using namespace fwk;
using namespace utl; 

void PlotResolutionConfig::Read(){


	/*if(cConfig == NULL){
		cout << __FUNCTION__ << ": Central config not initialized! Exiting..." << endl;
		exit (EXIT_FAILURE);
	}*/
	Branch dataAnaConf = cConfig.GetTopBranch("PlotMomentumResolution");

	if(dataAnaConf.GetName() != "PlotMomentumResolution"){
		cout << __FUNCTION__ << ": Wrong PlotMomentumResolution config file! Top branch name doesn't match! Please check your configuration files! Exiting..." << endl;
		exit (EXIT_FAILURE);
	}

	for(BranchIterator it = dataAnaConf.ChildrenBegin(); it != dataAnaConf.ChildrenEnd(); ++it){
		Branch child = *it;

		if(child.GetName() == "outputDir"){
			child.GetData(fOutputDir);

		}
		if(child.GetName() == "plotAll"){
			child.GetData(fPlotAll);
		}
		if(child.GetName() == "plotTopologies"){
			child.GetData(fPlotTopologies);
		}
		if(child.GetName() == "topology"){
			int val = 0;
			vector<int> clusters;
			double chi2 = 0;
			
			for(BranchIterator t = child.ChildrenBegin(); t != child.ChildrenEnd(); ++t){
				Branch targetChild = *t;

				if(targetChild.GetName() == "value"){
					targetChild.GetData(val);
				}
				else if(targetChild.GetName() == "clusters"){
					targetChild.GetData(clusters);
					if(clusters.size() != 3){
						cerr << "ERROR: " << __FUNCTION__ << ": 3 numbers are needed for cluster cut!" << endl;
						exit (EXIT_FAILURE);
					}
				}
				else if(targetChild.GetName() == "chi2Ndf"){
					targetChild.GetData(chi2);
				}
			}
			std::map<int,vector<int> >::iterator it;
			
			it = fClusters.find(val);
			
			if (it != fClusters.end()){
				it->second.clear();
				it->second = clusters;
				fChi2Ndf[val] = chi2;
			}

			else{
				fClusters.insert ( std::pair<int,vector<int> >(val,clusters) );
				fChi2Ndf.insert(std::pair<int, double> (val, chi2));
			}
		}

	}
	
}

//*********************************************************************************************
int PlotResolutionConfig::GetClusterCut(int topology, short int cut){

	if(cut < 1 && cut > 3)
	{
		cerr << "ERROR: " << __FUNCTION__ << ": cut must have value 1, 2 or 3!" << endl;
		exit(EXIT_FAILURE);  
	}
	std::map<int,vector<int> >::iterator it;
	
	it = fClusters.find(topology);
	if (it != fClusters.end())
    	return it->second.at(cut - 1);
    else{
    	cerr << "WARNING: " << __FUNCTION__ << ": Chi2 cut for topology " << topology << " not defined!" << endl;
     	return 0;
    }
}

//*********************************************************************************************
void PlotResolutionConfig::AddClusterCut(int topology, int VTPC, int GTPC, int MTPC){

	std::map<int,vector<int> >::iterator it;
	
	it = fClusters.find(topology);
	if (it != fClusters.end()){
		it->second.clear();
		it->second.push_back(VTPC);
		it->second.push_back(GTPC);
		it->second.push_back(MTPC);
	}

    else{
    	vector<int> cut;
    	cut.push_back(VTPC);
    	cut.push_back(GTPC);
    	cut.push_back(MTPC);
    	fClusters.insert ( std::pair<int,vector<int> >(topology,cut) );
    }
}
//*********************************************************************************************

double PlotResolutionConfig::GetChi2NdfCut(int topology){
	std::map<int,double>::iterator it;
	
	it = fChi2Ndf.find(topology);
	if (it != fChi2Ndf.end())
    	return it->second;
    else{
    	cerr << __FUNCTION__ << ": WARNING: Chi2 cut for topology " << topology << " not defined!" << endl;
     	return 100;
    }
}

//*********************************************************************************************
void PlotResolutionConfig::AddChi2Cut(int topology, double chi2ndf){

	std::map<int,double>::iterator it;
	
	it = fChi2Ndf.find(topology);
	if (it != fChi2Ndf.end()){
		it->second = chi2ndf;
	}

    else{
    	fChi2Ndf.insert ( std::pair<int,double >(topology,chi2ndf) );
    }
}
//*********************************************************************************************
string PlotResolutionConfig::GetTimeString(){
	time_t rawtime;
	struct tm* timeinfo;

	time(&rawtime);
	timeinfo = localtime(&rawtime);

	string res;
	stringstream s;

	s << timeinfo->tm_mon + 1;
	s << "_";
	s << timeinfo->tm_mday;
	s << "_";
	s << timeinfo->tm_year + 1900;
	s << "_";
	s << timeinfo->tm_hour;
	s << "_";
	s << timeinfo->tm_min;
	s << "_";
	s << timeinfo->tm_sec;

	s >> res;

	return res;
}


