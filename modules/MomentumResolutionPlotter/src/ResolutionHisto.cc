#include <stdlib.h>

#include "ResolutionHisto.h"

#include <fwk/CentralConfig.h>
#include <utl/Branch.h>
#include <utl/BranchIterator.h>
#include "CConfig.h"


using namespace fwk;
using namespace utl; 

void ResolutionHisto::Init(string cName, string nameAdd, int rst){

	Branch histosCfg = cConfig.GetTopBranch("ResolutionHistos");
	if(histosCfg.GetName() != "ResolutionHistos"){
		cerr <<"ERROR: "<< __FUNCTION__ << ": Wrong config file! Top branch name doesn't match! Please check your configuration files! Exiting..." << endl;
		exit (EXIT_FAILURE);
	}


	Branch child = histosCfg.GetChild(cName);

	
	vector<int> nBins;
	vector<double> low;
	vector<double> high;
	string name;
	string title;
	vector<string> axisTitle;
	vector<float> axisTitleSize;
	vector<float> axisLabelSize;
	vector<int> canvasSize;
	string canvasName;
	bool plot;
	for(BranchIterator it = child.ChildrenBegin(); it != child.ChildrenEnd(); ++it){
		Branch option = *it;
		
		if(option.GetName() == "nBins"){
			option.GetData(nBins);
		}
		
		else if(option.GetName() == "lowValues"){
			option.GetData(low);
		}
		
		else if(option.GetName() == "highValues"){
			option.GetData(high);
		}
		
		else if(option.GetName() == "name"){
			option.GetData(name);
		}
		
		else if(option.GetName() == "title"){
			option.GetData(title);
		}
		
		else if(option.GetName() == "axisTitle"){
			option.GetData(axisTitle);
		}
		
		else if(option.GetName() == "axisTitleSize"){
			option.GetData(axisTitleSize);
		}

		else if(option.GetName() == "axisLabelSize"){
			option.GetData(axisLabelSize);
		}

		else if(option.GetName() == "plot"){
			option.GetData(plot);
		}
					
		else if(option.GetName() == "canvasName"){
			option.GetData(canvasName);
		}
		
		else if(option.GetName() == "canvasSize"){
			option.GetData(canvasSize);
		}
		else{
			cout << __FUNCTION__ << ": Unknown branch! Skipping..." << endl;
		}
	}
	
	name = name + nameAdd;
	
	switch (rst){
		case 1:
			fRST = new TH2D(name.c_str(), title.c_str(), nBins.at(0), low.at(0), high.at(0), nBins.at(1), low.at(1), high.at(1));
			fRST->GetXaxis()->SetLabelSize(axisLabelSize.at(0));
			fRST->GetXaxis()->SetTitleSize(axisTitleSize.at(0));
			fRST->GetXaxis()->SetTitle(axisTitle.at(0).c_str());
			
			fRST->GetYaxis()->SetLabelSize(axisLabelSize.at(1));
			fRST->GetYaxis()->SetTitleSize(axisTitleSize.at(1));
			fRST->GetYaxis()->SetTitle(axisTitle.at(1).c_str());
			if(plot){
				canvasName = canvasName + nameAdd;
				fPlotRST = true;
				fCRST = new TCanvas(canvasName.c_str(), "", canvasSize.at(0), canvasSize.at(1));

			}
			else fPlotRST = false;			
			break;
	
		case 2:
			fWST = new TH2D(name.c_str(), title.c_str(), nBins.at(0), low.at(0), high.at(0), nBins.at(1), low.at(1), high.at(1));
			fWST->GetXaxis()->SetLabelSize(axisLabelSize.at(0));
			fWST->GetXaxis()->SetTitleSize(axisTitleSize.at(0));
			fWST->GetXaxis()->SetTitle(axisTitle.at(0).c_str());
			
			fWST->GetYaxis()->SetLabelSize(axisLabelSize.at(1));
			fWST->GetYaxis()->SetTitleSize(axisTitleSize.at(1));
			fWST->GetYaxis()->SetTitle(axisTitle.at(1).c_str());
			if(plot){
				canvasName = canvasName + nameAdd;
				fPlotWST = true;
				fCWST = new TCanvas(canvasName.c_str(), "", canvasSize.at(0), canvasSize.at(1));

			}
			else fPlotWST = false;			
			break;
			
		case 3:
			fAll = new TH2D(name.c_str(), title.c_str(), nBins.at(0), low.at(0), high.at(0), nBins.at(1), low.at(1), high.at(1));
			fAll->GetXaxis()->SetLabelSize(axisLabelSize.at(0));
			fAll->GetXaxis()->SetTitleSize(axisTitleSize.at(0));
			fAll->GetXaxis()->SetTitle(axisTitle.at(0).c_str());
			
			fAll->GetYaxis()->SetLabelSize(axisLabelSize.at(1));
			fAll->GetYaxis()->SetTitleSize(axisTitleSize.at(1));
			fAll->GetYaxis()->SetTitle(axisTitle.at(1).c_str());
			if(plot){
				canvasName = canvasName + nameAdd;
				fPlotRST = true;
				fCAll = new TCanvas(canvasName.c_str(), "", canvasSize.at(0), canvasSize.at(1));

			}
			else fPlotAll = false;			
			break;
		default:
			cerr << "ERROR: " << __FUNCTION__ << ": rst variable must have values 1,2 or 3!" << endl;
			exit (EXIT_FAILURE);
			break;
	}


}


TCanvas* ResolutionHisto::GetRSTCanvas(){
	if(!fPlotRST) return NULL;
	
	fCRST->cd();
	fRST->Draw("colz");
	
	return fCRST;
}

TCanvas* ResolutionHisto::GetWSTCanvas(){
	if(!fPlotWST) return NULL;
	
	fCWST->cd();
	fWST->Draw("colz");
	
	return fCWST;
}

TCanvas* ResolutionHisto::GetAllCanvas(){
	if(!fPlotAll) return NULL;
	
	fCAll->cd();
	fAll->Draw("colz");
	
	return fCAll;
}

void ResolutionHisto::Fill(int rst, double p, double res){
	switch (rst){
		case 1:
			fRST->Fill(p, res);
			break;
		case 2:
			fWST->Fill(p,res);
			break;
	}
	
	fAll->Fill(p,res);
}
