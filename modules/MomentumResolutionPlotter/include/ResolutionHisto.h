#ifndef RESOLUTIONHISTO_H
#define RESOLUTIONHISTO_H

#include <TH2D.h>
#include <TCanvas.h>

using namespace std;

class ResolutionHisto{

	public:

		ResolutionHisto(string cNameR, string cNameW, string cNameA, string nameAdd) {Init(cNameR, nameAdd, 1); Init(cNameW, nameAdd, 2); Init(cNameA, nameAdd, 3);}

		TH2D* GetRSTHisto(){return fRST;}
		TH2D* GetWSTHisto(){return fWST;}
		TH2D* GetAllHisto(){return fAll;}
				
		TCanvas* GetRSTCanvas();
		TCanvas* GetWSTCanvas();
		TCanvas* GetAllCanvas();
				
		void Fill(int rst, double p, double res);
	private:


		void Init(string cName, string nameAdd, int rst);
		bool fPlotRST;
		bool fPlotWST;
		bool fPlotAll;
		
		TH2D *fRST;
		TH2D *fWST;
		TH2D *fAll;
		
		TCanvas *fCRST;
		TCanvas *fCWST;
		TCanvas *fCAll;
};

#endif
