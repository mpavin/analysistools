#ifndef PLOTRESOLUTIONCONFIG_H
#define PLOTRESOLUTIONCONFIG_H
#include <iostream>
#include <vector>
#include <map>
#include <string>





using namespace std;

class PlotResolutionConfig{

	public:
		PlotResolutionConfig()  {Read();}  //Constructor

		//*************************Get functions***********************************

		string GetOutputDir(){return fOutputDir;}
		bool PlotAll(){return fPlotAll;}
		bool PlotTopologies(){return fPlotTopologies;}
		double GetChi2NdfCut(int topology);
		
		int GetClusterCut(int topology, short int cut);
		//*************************Set functions***********************************
		void Read(); //Reads main config file.


		void SetPlotAll(bool val){fPlotAll = val;}
		void SetPlotTopologies(bool val){fPlotTopologies = val;}
		void SetOutputDir(string outputDir){fOutputDir = outputDir;}
		void AddClusterCut(int topology, int VTPC, int GTPC, int MTPC);
		void AddChi2Cut(int topology, double chi2ndf);
		string GetTimeString();
		

	private:


		string fOutputDir;
		bool fPlotAll;
		bool fPlotTopologies;
		
		map<int,vector<int> > fClusters;
		map<int,double> fChi2Ndf;
};

#endif
