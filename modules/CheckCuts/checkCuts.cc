#include <vector>
#include <string>
#include <iostream>
#include "Functions.h"
#include <TH1D.h>
#include <TH2D.h>
#include <TFile.h>
#include <TChain.h>
#include <TTree.h>
#include <TMath.h>
#include <math.h>

using namespace std;


int main(int argc, char* argv[]){

	//cout << argc << " " << argv[0] << endl;

	if(argc < 3){
		cerr << "Number of parameters is different than 2! Exiting..." << endl;
		exit(100);
	}
	
	int data;
	string inputFile = argv[2];
	
	stringstream sconv0(argv[1]);
	sconv0 >> data;
	

	TChain chain("Data");
	

	ifstream ifs;
	string line;
	ifs.open(inputFile.c_str());
		
	if (!ifs.is_open()){
		cerr << "[ReadInputListFile] Could not open infile. Exiting..." << endl;			
		exit (101);
	}
    
	while (getline(ifs, line)){
		if(line.at(0)=='#')
			continue;
		chain.Add(line.c_str());
	}
	ifs.close();		

	string outName;
	if(data){
		outName = "binDataTree.root";
	}
	else{
		outName = "binMCTree.root";
	}
	
	
	 
	TFile outFile(outName.c_str(), "RECREATE");

	BeamA beamD;
	vector<double> *x = NULL;
	vector<double> *xE = NULL;
	vector<double> *y = NULL;
	vector<double> *yE = NULL;
	vector<double> *z = NULL;
	vector<double> *px = NULL;
	vector<double> *py = NULL;
	vector<double> *pz = NULL;
	vector<double> *zLast = NULL;
	vector<double> *xLast = NULL;
	vector<double> *distTarg = NULL;
	vector<double> *zStart = NULL;
	vector<double> *dEdx = NULL;
	vector<int> *qD = NULL;
	vector<int> *nVTPC1 = NULL;
	vector<int> *nVTPC2 = NULL;
	vector<int> *nGTPC = NULL;
	vector<int> *nMTPC = NULL;
	vector<int> *topology = NULL;
	vector<int> *nTOF = NULL;
	vector<double> *m2TOF = NULL;	
	vector<int> *tofScId = NULL;
	int runNumber;
	
	TBranch *b_aData = chain.GetBranch("Beam");
 	b_aData->SetAddress(&beamD);
	chain.SetBranchAddress("x", &x);
	chain.SetBranchAddress("xE", &xE);
	chain.SetBranchAddress("y", &y);
	chain.SetBranchAddress("yE", &yE);
	chain.SetBranchAddress("z", &z);
	chain.SetBranchAddress("px", &px);
	chain.SetBranchAddress("py", &py);
	chain.SetBranchAddress("pz", &pz);
	chain.SetBranchAddress("xLast", &xLast);
	chain.SetBranchAddress("zLast", &zLast);
	chain.SetBranchAddress("distTarg", &distTarg);
	chain.SetBranchAddress("zStart", &zStart);
	chain.SetBranchAddress("dEdx", &dEdx);
	chain.SetBranchAddress("q", &qD);
	chain.SetBranchAddress("nVTPC1", &nVTPC1);
	chain.SetBranchAddress("nVTPC2", &nVTPC2);
	chain.SetBranchAddress("nGTPC", &nGTPC);
	chain.SetBranchAddress("nMTPC", &nMTPC);
	chain.SetBranchAddress("topology", &topology);
	chain.SetBranchAddress("nTOF", &nTOF);
	chain.SetBranchAddress("m2TOF", &m2TOF);
	chain.SetBranchAddress("tofScId", &tofScId);
	chain.SetBranchAddress("runNumber", &runNumber);
	
	int nEntries = chain.GetEntries();
	



	//BeamA beamD1;
	vector<double> x1;
	vector<double> xE1;
	vector<double> y1;
	vector<double> yE1;
	vector<double> z1;
	vector<double> px1;
	vector<double> py1;
	vector<double> pz1;
	vector<double> distTarg1;
	vector<double> zStart1;
	vector<double> zLast1;
	vector<double> xLast1;
	vector<double> dEdx1;
	vector<int> qD1;
	vector<int> nVTPC11;
	vector<int> nVTPC21;
	vector<int> nGTPC1;
	vector<int> nMTPC1;
	vector<int> topology1;
	vector<int> nTOF1;
	vector<double> m2TOF1;	
	vector<int> tofScId1;
	
	//unsigned int runNumber = 0;

	
	outFile.cd();
	TTree *fSpectrum = new TTree("Data", "Data");
	fSpectrum->SetAutoSave(-300000000);

	fSpectrum->Branch("Beam", &(beamD.aX), "aX/D:aY:bX:bY");
	fSpectrum->Branch("x", "vector<double>", &x1);
	fSpectrum->Branch("xE", "vector<double>", &xE1);
	fSpectrum->Branch("y", "vector<double>", &y1);
	fSpectrum->Branch("yE", "vector<double>", &yE1);
	fSpectrum->Branch("z", "vector<double>", &z1);
	fSpectrum->Branch("px", "vector<double>", &px1);
	fSpectrum->Branch("py", "vector<double>", &py1);
	fSpectrum->Branch("pz", "vector<double>", &pz1);
	fSpectrum->Branch("distTarg", "vector<double>", &distTarg1);
	fSpectrum->Branch("q", "vector<int>", &qD1);
	fSpectrum->Branch("zStart", "vector<double>", &zStart1);
	fSpectrum->Branch("xLast", "vector<double>", &xLast1);
	fSpectrum->Branch("zLast", "vector<double>", &zLast1);
	fSpectrum->Branch("nVTPC1", "vector<int>", &nVTPC11);
	fSpectrum->Branch("nVTPC2", "vector<int>", &nVTPC21);
	fSpectrum->Branch("nGTPC", "vector<int>", &nGTPC1);
	fSpectrum->Branch("nMTPC", "vector<int>", &nMTPC1);
	fSpectrum->Branch("topology", "vector<int>", &topology1);
	fSpectrum->Branch("dEdx", "vector<double>", &dEdx1);
	fSpectrum->Branch("nTOF", "vector<int>", &nTOF1);
	fSpectrum->Branch("m2TOF", "vector<double>", &m2TOF1);
	fSpectrum->Branch("tofScId", "vector<int>", &tofScId1);
	fSpectrum->Branch("runNumber", &runNumber, "runNumber/I");
	for(int i = 0; i < nEntries; ++i){
		
		chain.GetEntry(i,1);
		
		if(!((i+1)%100000))
			cout << "Processing event # "  << i+1 << endl;
		
		if(x1.size() > 0){
			x1.clear();
			xE1.clear();
			y1.clear();
			yE1.clear();
			z1.clear();
			px1.clear();
			py1.clear();
			pz1.clear();
			distTarg1.clear();
			zStart1.clear();
			zLast1.clear();
			xLast1.clear();
			dEdx1.clear();
			qD1.clear();
			nVTPC11.clear();
			nVTPC21.clear();
			nGTPC1.clear();
			nMTPC1.clear();
			topology1.clear();
			nTOF1.clear();
			m2TOF1.clear();	
			tofScId1.clear();		
		}
		for(int j = 0; j < px->size(); j++){

			if(z->at(j) + 657.51 > 18) continue;
			if(z->at(j) + 657.51 < 0) continue;
			double p = sqrt(px->at(j)*px->at(j)+py->at(j)*py->at(j)+pz->at(j)*pz->at(j));
			
			//if(p < 1.5) continue;
			//if(p > 3.5) continue;
			double theta = 1000*acos(pz->at(j)/p);
			
			if(theta < 60) continue;
			if(theta > 80) continue;
			//cout << "radi" << endl;
			x1.push_back(x->at(j));
			xE1.push_back(xE->at(j));
			y1.push_back(y->at(j));
			yE1.push_back(yE->at(j));
			z1.push_back(z->at(j));
			px1.push_back(px->at(j));
			py1.push_back(py->at(j));
			pz1.push_back(pz->at(j));
			distTarg1.push_back(distTarg->at(j));
			qD1.push_back(qD->at(j));
			dEdx1.push_back(dEdx->at(j));
			zStart1.push_back(zStart->at(j));
			xLast1.push_back(xLast->at(j));
			zLast1.push_back(zLast->at(j));
			nVTPC11.push_back(nVTPC1->at(j));
			nVTPC21.push_back(nVTPC2->at(j));
			nGTPC1.push_back(nGTPC->at(j));
			nMTPC1.push_back(nMTPC->at(j));
			topology1.push_back(topology->at(j));
			nTOF1.push_back(nTOF->at(j));
			m2TOF1.push_back(m2TOF->at(j));
			tofScId1.push_back(tofScId->at(j));
			
		}
		if(x1.size() > 0) fSpectrum->Fill();

	}
	

	
	outFile.cd();
	fSpectrum->Write();

	
	outFile.Close();
		
}





