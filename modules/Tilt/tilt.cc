#include <vector>
#include <string>
#include <iostream>
#include "Functions.h"
#include <TH1D.h>
#include <TH2D.h>
#include <TFile.h>
#include <TChain.h>
#include <TTree.h>
#include <TMath.h>
#include <math.h>
#include <TF1.h>
using namespace std;


int main(){


	string inputFile = "list.txt";

	
	//TChain chain("Data");
	TChain chain("TargetAnalysis");
	
	string outName = "targettilt.root";
	TFile outFile(outName.c_str(), "RECREATE");
	ifstream ifs;
	string line;
	ifs.open(inputFile.c_str());
		
	if (!ifs.is_open()){
		cerr << "[ReadInputListFile] Could not open infile. Exiting..." << endl;			
		exit (101);
	}
    
	while (getline(ifs, line)){
		if(line.at(0)=='#')
			continue;
		chain.Add(line.c_str());
	}
	ifs.close();		

	vector<double> *x = NULL;
	vector<double> *xE = NULL;
	vector<double> *y = NULL;
	vector<double> *yE = NULL;
	vector<double> *z = NULL;
	vector<double> *zS = NULL;
	vector<double> *px = NULL;
	vector<double> *py = NULL;
	vector<double> *pz = NULL;
	vector<int> *q = NULL;
	vector<int> *topology = NULL;
	vector<int> *nVTPC = NULL;
	vector<int> *nGTPC = NULL;
	vector<int> *nMTPC = NULL;
	double aX;
	double bX;
	double aY;
	double bY;
	int runNumber;	
	

	
	chain.SetBranchAddress("x", &x);
	chain.SetBranchAddress("xE", &xE);
	chain.SetBranchAddress("y", &y);
	chain.SetBranchAddress("yE", &yE);
	chain.SetBranchAddress("z", &z);
	chain.SetBranchAddress("zS", &zS);
	chain.SetBranchAddress("px", &px);
	chain.SetBranchAddress("py", &py);
	chain.SetBranchAddress("pz", &pz);
	chain.SetBranchAddress("q", &q);
	chain.SetBranchAddress("nVTPC", &nVTPC);
	chain.SetBranchAddress("nGTPC", &nGTPC);
	chain.SetBranchAddress("nMTPC", &nMTPC);
	chain.SetBranchAddress("topology", &topology);
	chain.SetBranchAddress("aX", &aX);
	chain.SetBranchAddress("aY", &aY);
	chain.SetBranchAddress("bX", &bX);
	chain.SetBranchAddress("bY", &bY);
	chain.SetBranchAddress("runNumber", &runNumber);

	chain.SetBranchAddress("runNumber", &runNumber);

	/*BeamA beamD;
	vector<double> *x = NULL;
	vector<double> *xE = NULL;
	vector<double> *y = NULL;
	vector<double> *yE = NULL;
	vector<double> *z = NULL;
	vector<double> *px = NULL;
	vector<double> *py = NULL;
	vector<double> *pz = NULL;
	vector<double> *distTarg = NULL;
	vector<double> *zStart = NULL;
	vector<double> *dEdx = NULL;
	vector<int> *qD = NULL;
	vector<int> *nVTPC1 = NULL;
	vector<int> *nVTPC2 = NULL;
	vector<int> *nGTPC = NULL;
	vector<int> *nMTPC = NULL;
	vector<int> *topology = NULL;
	vector<int> *nTOF = NULL;
	vector<double> *m2TOF = NULL;	
	vector<int> *tofScId = NULL;
	//vector<int> *pdgId = NULL;
	int runNumber;
	
	//TBranch *b_aData = chain.GetBranch("Beam");
 	//b_aData->SetAddress(&beamD);
 	chain.SetBranchAddress("Beam", &beamD);
	chain.SetBranchAddress("x", &x);
	chain.SetBranchAddress("xE", &xE);
	chain.SetBranchAddress("y", &y);
	chain.SetBranchAddress("yE", &yE);
	chain.SetBranchAddress("z", &z);
	chain.SetBranchAddress("px", &px);
	chain.SetBranchAddress("py", &py);
	chain.SetBranchAddress("pz", &pz);
	chain.SetBranchAddress("distTarg", &distTarg);
	chain.SetBranchAddress("zStart", &zStart);
	chain.SetBranchAddress("dEdx", &dEdx);
	chain.SetBranchAddress("q", &qD);
	chain.SetBranchAddress("nVTPC1", &nVTPC1);
	chain.SetBranchAddress("nVTPC2", &nVTPC2);
	chain.SetBranchAddress("nGTPC", &nGTPC);
	chain.SetBranchAddress("nMTPC", &nMTPC);
	chain.SetBranchAddress("topology", &topology);
	chain.SetBranchAddress("nTOF", &nTOF);
	chain.SetBranchAddress("m2TOF", &m2TOF);
	chain.SetBranchAddress("tofScId", &tofScId);
	chain.SetBranchAddress("runNumber", &runNumber);
	chain.SetBranchAddress("pdgId", &pdgId);*/
	
	int nEntries = chain.GetEntries();
	

	vector<TH1D> xhistos;
	vector<TH1D> yhistos;
	vector<TH1D> xbhistos;
	vector<TH1D> ybhistos;
	TH1D meanx("meanx", "", 15, 0, 90);
	TH1D meany("meany", "", 15, 0, 90);
	TH1D zdist("zdist", "", 130, -677, -547);
	TH2D xy("xy", "", 200, -2, 2, 200, -2 ,2);
	
	TH1D ytemp1("y1", "", 800, -2, 2);
	for(int i = 0; i < 15; i++){
		string namex = "x" + ConvertToString(i+1.,0);
		string namey = "y" + ConvertToString(i+1.,0);
		string namexb = "xb" + ConvertToString(i+1.,0);
		string nameyb = "yb" + ConvertToString(i+1.,0);
		
		TH1D xtemp(namex.c_str(), "", 1000, -2, 2);
		TH1D ytemp(namey.c_str(), "", 1000, -2, 2);
		TH1D xbtemp(namexb.c_str(), "", 1000, -2, 2);
		TH1D ybtemp(nameyb.c_str(), "", 1000, -2, 2);
		
		xhistos.push_back(xtemp);
		yhistos.push_back(ytemp);
		xbhistos.push_back(xtemp);
		ybhistos.push_back(ytemp);
		/*if(i == 1){
			yhistos.push_back(ytemp1);
		}
		else{
			yhistos.push_back(ytemp);
		}*/
	}
	
	cout << nEntries << endl;
	for(int i = 0; i < nEntries; ++i){
		
		chain.GetEntry(i,1);
		
		if(!((i+1)%100000))
			cout << "Processing event # "  << i+1 << endl;
		//double xb = -657.41*beamD.aX + beamD.bX - 0.015;
		//double yb = -657.41*beamD.aY + beamD.bY - 0.285;
		double xb = -657.51*aX + bX - 0.015;
		double yb = -657.51*aY + bY - 0.285;
		for(int j = 0; j < 15; j++){
			xbhistos.at(j).Fill((-657.51+1.+2.*j)*aX + bX);
			ybhistos.at(j).Fill((-657.51+1.+2.*j)*aY + bY);
		}
		//cout << xb << " "<< yb<<endl;
		if(sqrt(xb*xb+yb*yb)>1.28) continue;
		//if(nCount > nTrack) break;	
		for(int j = 0; j < px->size(); j++){
			//cout << pdgId->at(j) << endl;
			
			//if(zStart->at(j) > -480) continue;
			if(nVTPC->at(j) < 20) continue;
			//if(nMTPC->at(j) < 54) continue;
			double p = sqrt(px->at(j)*px->at(j) + py->at(j)*py->at(j) + pz->at(j)*pz->at(j));
			//if(p>5) continue;
			double theta = 1000*acos(pz->at(j)/p);
			if(theta < 20) continue;
			//if(theta > 200) continue;
			//if(z->at(j) < -657.6) continue;
			//if(z->at(j) > -567.6) continue;
			zdist.Fill(z->at(j));
			xy.Fill(x->at(j), y->at(j));
			int n = (z->at(j)+657.51)/6.;
			//cout << n << endl;
			if(n > 14) continue;
			if(n < 0) continue;
			//cout << n << endl;
			xhistos.at(n).Fill(x->at(j));
			yhistos.at(n).Fill(y->at(j));
			//xhistos.at(n).Fill(z->at(j)*aX+bX);
			//yhistos.at(n).Fill(z->at(j)*aX+bX);
			//xhistos.at(n).Fill((-657.51+1.+2.*j)*aX+bX);
			//yhistos.at(n).Fill((-657.51+1.+2.*j)*aY+bY);
		}

	}
	
	/*TF1* zFit = new TF1("zFit", "0.5*[0]*[1]*[2]*TMath::Exp(0.5*[2]*(2*[3] + [2]*[4]*[4] - 2*x))*TMath::Erfc(([3] + [2]*[4]*[4] - x)/(sqrt(2)*[4]))", -675., -640);

				
		/// Setting parameter 0
	zFit->SetParName(0, "N");
	zFit->SetParameter(0, 25000);
	zFit->SetParLimits(0, 10000, 110000000);	


								
		/// Fixing parameter 1
	zFit->SetParName(1, "#Delta z");
	zFit->FixParameter(1, 1);

		
		/// Setting parameter 2
	zFit->SetParName(2, "#rho#sigma");
	zFit->SetParameter(2, 0.1);
	zFit->SetParLimits(2, 0.001, 1.5);

		
		/// Setting parameter 3		
	zFit->SetParName(3, "z_{0}");
	zFit->SetParameter(3, -650);
	zFit->SetParLimits(3, -670, -630);

		
		/// Setting parameter 4
	zFit->SetParName(4, "#sigma");
	zFit->SetParameter(4, 0.5);
	zFit->SetParLimits(4, 0.1, 20);*/

	TF1* zFit = new TF1("zFit", "[0]*TMath::TanH([1]*(x-[2]))+[0]", -672., -640);	
	zFit->SetParName(0, "N");
	zFit->SetParameter(0, 25000);
	zFit->SetParLimits(0, 10000, 110000000);	
	
	zFit->SetParName(1, "#mu_{1}");
	zFit->SetParameter(1, 2);
	zFit->SetParLimits(1, 0, 10);
	
	zFit->SetParName(2, "z_{0}");
	zFit->SetParameter(2, -657.0);
	zFit->SetParLimits(2, -670, -640);		

	/*TF1* zFit = new TF1("zFit", "([0]*TMath::Erf((x-[1])/[2])+[0])", -672., -640);
	zFit->SetParName(0, "N");
	zFit->SetParameter(0, 25000);
	zFit->SetParLimits(0, 1000, 110000000);	
	
	
	zFit->SetParName(1, "z_{0}");
	zFit->SetParameter(1, -657);
	zFit->SetParLimits(1, -665, -640);	
	
	zFit->SetParName(2, "#sigma");
	zFit->SetParameter(2, 2);
	zFit->SetParLimits(2, 1, 20);	*/


	/*TF1* zFit = new TF1("zFit", "(x<[3])*([0]*TMath::Erf((x-[1])/[2])+[0])+(x>[3])*([4]*TMath::Power(x-[3],4)+[5]*TMath::Power(x-[3],3)+[6]*TMath::Power(x-[3],2)+2*[0])+[7]", -665., -610);
	zFit->SetParName(0, "N");
	zFit->SetParameter(0, 25000);
	zFit->SetParLimits(0, 1000, 110000000);	
	
	
	zFit->SetParName(1, "z_{0}");
	zFit->SetParameter(1, -657);
	zFit->SetParLimits(1, -665, -640);	
	
	zFit->SetParName(2, "#sigma");
	zFit->SetParameter(2, 2);
	zFit->SetParLimits(2, 1, 20);	

	zFit->SetParName(3, "#mu");
	zFit->SetParameter(3, -655);
	zFit->SetParLimits(3, -655, -630);	
	
	zFit->SetParName(4, "a");
	zFit->SetParameter(4, 1);
	zFit->SetParLimits(4, -10, 10);	
	
	zFit->SetParName(5, "b");
	zFit->SetParameter(5, 1);
	zFit->SetParLimits(5, -20, 20);	
	
	zFit->SetParName(6, "c");
	zFit->SetParameter(6, 1);
	zFit->SetParLimits(6, -40, 20);	

	zFit->SetParName(7, "n");
	zFit->SetParameter(7, 30);
	zFit->SetParLimits(7, 0, 1000);	*/	
	zdist.Fit(zFit, "R");
	//cout << nCount << " " << nEntries << endl;
	outFile.cd();

	double val[3] = {0,0,0};
	FindMaximum(val, 0.5,0.5, 0.01, &xy);
	
	
	cout << val[0] << " " << val[1] << endl;
	for(int i = 0; i < xhistos.size(); i++){
	
		/*TF1 fit1("fit1", "[0]*TMath::Gaus(x,[1], [2])", -1, 1.3);
		
		fit1.SetParameter(0, xhistos.at(i).GetEntries());
		fit1.SetParLimits(0, 0, 1.5*xhistos.at(i).GetEntries());
		fit1.SetParameter(1, -0.2);
		fit1.SetParLimits(1, -0.3, 0.3);
		fit1.SetParameter(2, 0.6);
		fit1.SetParLimits(2, 0.2, 1.0);
		
		TF1 fit2("fit2", "[0]*TMath::Gaus(x,[1], [2])", -1, 1);
		
		fit2.SetParameter(0, yhistos.at(i).GetEntries());
		fit2.SetParLimits(0, 0, 1.5*yhistos.at(i).GetEntries());
		fit2.SetParameter(1, -0.2);
		fit2.SetParLimits(1, -0.3, 0.3);
		fit2.SetParameter(2, 0.6);
		fit2.SetParLimits(2, 0.2, 1.0);
		
		xhistos.at(i).Fit(&fit1, "R");
		yhistos.at(i).Fit(&fit2, "R");*/
		double x1;
		double y1;
		
		FindMaximum(x1, 1.3, xhistos.at(i));
		FindMaximum(y1, 1.3, yhistos.at(i));
		/*double mx = 0;
		double mxE = 0;
		bool startx = false;
		bool starty = false;
	
		double my = 0;
		double myE = 0;
		for(int j = 0; j < xhistos.at(i).GetNbinsX(); j++){


			if(xbhistos.at(i).GetBinContent(j+1) > 10){

				double xe = xhistos.at(i).GetBinContent(j+1);
				double xbe = xbhistos.at(i).GetBinContent(j+1);
				double eff = xe/xbe;
				double xee = sqrt(eff*(1-eff))/sqrt(xbe);
				//xhistos.at(i).SetBinContent(j+1, eff);
				//xhistos.at(i).SetBinError(j+1, xee);
				if(eff > 0.00001 && eff <= 1){
					if(!startx){
						mx = j;
						startx = true;
					}
					mxE = j;
				}
			}
			else{

				//xhistos.at(i).SetBinContent(j+1, 0);
				//xhistos.at(i).SetBinError(j+1, 0);			
			}
			
			if(ybhistos.at(i).GetBinContent(j+1) > 20){
				double ye = yhistos.at(i).GetBinContent(j+1);
				double ybe = ybhistos.at(i).GetBinContent(j+1);
				double eff = ye/ybe;
				double yee = sqrt(eff*(1-eff))/sqrt(ybe);
				//yhistos.at(i).SetBinContent(j+1, eff);
				//yhistos.at(i).SetBinError(j+1, yee);
				if(eff > 0.00001 && eff <= 0.95){
					if(!starty){
						my = j;
						starty = true;
					}
					myE = j;
				}	
			}
			else{

				//yhistos.at(i).SetBinContent(j+1, 0);
				//yhistos.at(i).SetBinError(j+1, 0);				
			}

			
		}
		cout << mx << " " << mxE << " " << my << " " << myE << endl;*/
		meanx.SetBinContent(i+1, x1);
		meanx.SetBinError(i+1, 0.004);
		meany.SetBinContent(i+1, y1);
		meany.SetBinError(i+1, 0.004);
		/*meanx.SetBinContent(i+1, xhistos.at(i).GetMean());
		meanx.SetBinError(i+1, xhistos.at(i).GetMeanError());
		meany.SetBinContent(i+1, yhistos.at(i).GetMean());
		meany.SetBinError(i+1, yhistos.at(i).GetMeanError());*/
		//int xbin = xhistos.at(i).GetMaximumBin();
		//int ybin = yhistos.at(i).GetMaximumBin();
		/*double xmean = 0;
		double ymean = 0;
		bool sx = false;
		bool sy = false;
		for(int j = 0; j < xhistos.at(i).GetNbinsX(); j++){
			if(!sx){
				if(xhistos.at(i).GetBinContent(j+1) != 0){
					xmean += xhistos.at(i).GetBinCenter(j+1);
					sx = true;
				}
			}
			else{
				if(xhistos.at(i).GetBinContent(j+1) == 0){
					xmean += xhistos.at(i).GetBinCenter(j+1);
					break;
				}			
			}
		}
		
		for(int j = 0; j < yhistos.at(i).GetNbinsX(); j++){
			if(!sx){
				if(yhistos.at(i).GetBinContent(j+1) != 0){
					ymean += yhistos.at(i).GetBinCenter(j+1);
					sy = true;
				}
			}
			else{
				if(yhistos.at(i).GetBinContent(j+1) == 0){
					ymean += yhistos.at(i).GetBinCenter(j+1);
					break;
				}			
			}
		}
		//meanx.Fill(i*2+1, xhistos.at(i).GetBinCenter(xbin));
		//meany.Fill(i*2+1, yhistos.at(i).GetBinCenter(ybin));
		meanx.Fill(i*2+1, xmean/2.);
		meany.Fill(i*2+1, ymean/2.);
		meanx.SetBinError(i+1,0.025);
		meany.SetBinError(i+1,0.025);*/
		xhistos.at(i).Write();
		yhistos.at(i).Write();
	}
	meanx.Write();
	meany.Write();
	zdist.Write();
	xy.Write();
	//thetaPhi.Write();
	outFile.Close();
		
}





