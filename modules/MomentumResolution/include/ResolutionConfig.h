#ifndef RESOLUTIONCONFIG_H
#define RESOLUTIONCONFIG_H
#include <iostream>
#include <vector>
#include <string>
#include "TargetAna.h"




using namespace std;

class ResolutionConfig{

	public:
		ResolutionConfig()  {Read();}  //Constructor

		//*************************Get functions***********************************

		string GetOutputDir(){return fOutputDir;}
		string GetInputListFile(){return fInputData;}

		vector<int>& GetRunNumbers(){return fRunNumbers;}
		TargetAna& GetTarget(){return fTarget;}
		//*************************Set functions***********************************
		void Read(); //Reads main config file.


		void SetInputDataFile(string inputData){fInputData = inputData;}
		void SetOutputDir(string outputDir){fOutputDir = outputDir;}

		void SetRunNumbers(vector<int> fRunNumbers);
		void SetTarget(string name, ETargetType type, double length, double radius, double positionX, double positionY, double positionZ, double tilt1, double tilt2);

		vector<string>& GetJob();
		void ReadInputList();
		void ReadInputList(string inputData);

		string GetTimeString();
		

		bool SkipRun(int runNumber);
	private:


		
		string fInputData;
		string fOutputDir;

		vector<string> fJob;

		vector<int> fRunNumbers;
		TargetAna fTarget;
};

#endif
