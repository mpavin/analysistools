#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <cstdlib>
#include <stdexcept>
//#include <TPRegexp.h>

#include <TTree.h>
#include <TFile.h>
#include <TH2D.h>
#include <evt/Event.h>
#include <evt/EventHeader.h>
#include <io/EventFileChain.h>
#include <det/TriggerConst.h>
#include <det/BPDConst.h>
#include <evt/RawEvent.h>
#include "TrackTools.h"
#include <boost/format.hpp>
#include "Resolution.h"
#include "ResolutionConfig.h"
#include "TargetAna.h"
#include "AnaTrack.h"
#include <utl/Vector.h>
#include "EventSelection.h"
#include "TrackSelection.h"
#include "TrackExtrapolation.h"

using namespace std;
using namespace io;
using namespace evt;
using namespace evt::rec;
using namespace det;
using namespace utl;
using namespace boost;
using namespace TrackTools;

void FindResolution(int n){

	ResolutionConfig cfg;
	//string fileName = cfg.GetOutputDir() + "resolution_" + cfg.GetTimeString() + ".root";
	string fileName = cfg.GetOutputDir() + "/resolution.root";
	
	EventSelection evSel;
	TrackSelection trSel;
	EventFileChain eventFileChain(cfg.GetJob());
	Event event;

   TrackExtrapolation trackExtrapolation(true, 0.05, 30400.);
   trackExtrapolation.SetTarget(cfg.GetTarget());

	unsigned int nEvents = 0;


	TFile *outputFile = new TFile(fileName.c_str(), "RECREATE");
	outputFile->cd();
	TTree* resTree = new TTree("MomentumResolution", "MomentumResolution");
	resTree->SetAutoSave(-300000000);

	ResVal res;
	resTree->Branch("Resolution", &(res.topology), "topology/I:nClusters:VTPC1:VTPC2:GTPC:MTPCL:MTPCR:RST:p/D:chi2Ndf:resolution:theta:phi");
	unsigned int runNumber = 0;
	
	TH2D histoRes("histoRes", "Momentum Resolution", 230, 20, 250, 100, 0, 0.05); 
	TH2D histoResP("histoResP", "Momentum Resolution", 180, 0, 30, 100, 0, 0.05);
	while (eventFileChain.Read(event) == eSuccess && nEvents < n) {


	
		bool skipRun = false;
		EventHeader &evHeader = event.GetEventHeader();

		if(evHeader.GetRunNumber() != runNumber){
			runNumber = evHeader.GetRunNumber();
			skipRun = cfg.SkipRun(runNumber);
		}

		if(skipRun) continue;

   		++nEvents;
    
    	if (!(nEvents%1000))
      		cout << " --->Processing event # " << nEvents << endl;


		if(evSel.IsEventGood(event) == false)
			continue;	

		const RecEvent& recEvent = event.GetRecEvent();


		for (std::list<rec::Track>::const_iterator iter = recEvent.Begin<rec::Track>();
           iter != recEvent.End<rec::Track>(); ++iter)
      	{

			const Track& track = *iter;

			if(track.GetStatus() != 0) continue;
			int nToF = 0;
			double tofMass;
			int scId;
			
			for (std::list<rec::TOFMass>::const_iterator tofIter = recEvent.Begin<rec::TOFMass>();
           	tofIter != recEvent.End<rec::TOFMass>(); ++tofIter)
      		{

				

				const rec::TOFMass& tof = *tofIter;
				if(!tof.HasVertexTrack()) continue;
			
				const rec::VertexTrack& vtx = recEvent.Get(tof.GetVertexTrackId());

				if(!vtx.HasTrack()) continue;
			
				if(vtx.GetTrackIndex() != track.GetIndex()) continue;

				if(tof.GetTOFId() == TOFConst::eTOFF){
					scId = tof.GetScintillatorId();
					tofMass = tof.GetSquaredMass();
					++nToF;
				}

			}


			TargetAna target;
			//TrackExtrapolation trkExtrap(cfg.GetTarget());
			AnaTrack anaTrack(trackExtrapolation);
			
			 anaTrack.SetTrack(track);
			if(nToF == 1){
				anaTrack.SetToFMassSquared(tofMass);
				anaTrack.SetToFScintillatorId(scId);
			}			

			if(trSel.IsTrackGood(anaTrack) == false)
				continue;				
			TrackPar &trkPar = trackExtrapolation.GetStopTrackParam();
			const Vector& p = track.GetMomentum();
			
			//double resolution = sqrt(pow(p.GetX(),2)*trkPar.Cov[0] + pow(p.GetY(),2)*trkPar.GetCov[5] + pow(p.GetZ(),2)*trkPar.GetCov[9])/p.GetMag()/p.GetMag();
			double resolution = sqrt(trkPar.GetCov(14)*p.GetMag()*p.GetMag());
			res.topology = anaTrack.GetTopology();
			res.nClusters = anaTrack.GetNumberOfClusters().all;
			res.VTPC1 = anaTrack.GetNumberOfClusters().VTPC1;
			res.VTPC2 = anaTrack.GetNumberOfClusters().VTPC2;
			res.GTPC = anaTrack.GetNumberOfClusters().GTPC;
			if(track.GetLastPointOnTrack().GetX() > 0) {
				res.MTPCL = anaTrack.GetNumberOfClusters().MTPC;
				res.MTPCR = 0;
			}
			else{ 
				res.MTPCR = anaTrack.GetNumberOfClusters().MTPC;
				res.MTPCL = 0;
			}
			if(p.GetX()*track.GetCharge() > 0) res.RST = 1;
			else res.RST = 0;
			res.p = p.GetMag();
			res.chi2Ndf = track.GetChi2()/track.GetNdf();
			res.resolution = resolution;
			res.theta = atan(sqrt(p.GetX()*p.GetX() + p.GetY()*p.GetY()));
			if(p.GetX() < 0) res.phi = atan(p.GetY()/p.GetX()) + ROOT::Math::Pi()/2.;
			else res.phi = atan(p.GetY()/p.GetX()) - ROOT::Math::Pi()/2.;
			histoRes.Fill(res.nClusters, resolution);
			histoResP.Fill(p.GetMag(),resolution);
			outputFile->cd();
			resTree->Fill();

		}
	}
	outputFile->cd();
	resTree->Write();

	histoRes.Write();
	histoResP.Write();
	outputFile->Close();

}

