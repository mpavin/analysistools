#include <vector>
#include <string>
#include <iostream>
#include "Functions.h"
#include <TH1D.h>
#include <TH2D.h>
#include <TFile.h>
#include <TChain.h>
#include <TMath.h>
#include <TCanvas.h>
#include <TGraphErrors.h>

using namespace std;


int main(){

	string inputFile = "list.txt"
	const int nZ = 6;

	double zBorders[7] ={-0.1, 18, 36, 54, 72, 89.99, 90.03};
	double thetaBorders[12] ={0, 20, 40, 60, 80, 100, 140, 180, 220, 260, 300, 340};
	

	vector<string> files;
	ifstream ifs;
	string line;
	ifs.open(inputFile.c_str());
		
	if (!ifs.is_open()){
		cerr << "[ReadInputListFile] Could not open infile. Exiting..." << endl;			
		exit (101);
	}
    
	while (getline(ifs, line)){
		if(line.at(0)=='#')
			continue;
		files.push_back(line.c_str());
	}
	ifs.close();		
		
	for(int i = 0; i < filse.size(); i++)
	{
	
		string name = "zbin" + ConvertToString(i+1., 0);
		TCanvas c(name.c_str(), "", 1600, 900);
		TFile file(files.at(i).c_str(), "READ");

		double zMin;
		double zMax;
		double thetaMin;
		double thetaMax;
		vector<double> *p = NULL;
		vector<double> *pWidth = NULL;
		vector<double> *nData = NULL;
		vector<double> *nDataE = NULL;
		vector<double> *nMC = NULL;
		vector<double> *nMCE = NULL;
		vector<double> *nSim = NULL;
		int indexZ;
		int indexTh;
		int indexP;
		
		TTree *tree = (TTree*) input.Get("Multiplicity");
		
		tree->SetBranchAddress("zMin", &zMin);
		tree->SetBranchAddress("zMax", &zMax);
		tree->SetBranchAddress("thetaMin", &thetaMin);
		tree->SetBranchAddress("thetaMax", &thetaMax);
		tree->SetBranchAddress("p", &p);
		tree->SetBranchAddress("pWidth", &pWidth);
		tree->SetBranchAddress("nData", &nData);
		tree->SetBranchAddress("nDataE", &nDataE);
		tree->SetBranchAddress("nMC", &nMC);
		tree->SetBranchAddress("nMCE", &nMCE);
		tree->SetBranchAddress("nSim", &nSim);
		tree->SetBranchAddress("indexZ", &indexZ);
		tree->SetBranchAddress("indexTh", &indexTh);
		tree->SetBranchAddress("indexP", &indexP);
		
		int nEntries = tree->GetEntries();
		
		for(int j = 0; j < nEntries; j++){
			tree->Getentry(j);
		}
		
	}
		
}





