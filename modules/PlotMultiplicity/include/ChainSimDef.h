#ifndef CHAINSIMDEF_H
#define CHAINSIMDEF_H

#include <vector>
#include <string>
#include <TChain.h>
#include <TBranch.h>
#include "Functions.h"

using namespace std;

BeamA beam;
vector<double> *z = NULL;
vector<double> *theta = NULL;
vector<double> *pSim = NULL;
vector<double> *pxSim = NULL;
vector<int> *q = NULL;
vector<int> *pdgId = NULL;
vector<int> *iSimZ = NULL;
vector<int> *iSimTh = NULL;
vector<int> *iSimP = NULL;


void ReadMC(TChain &chain, string fileName){
	chain.Add(fileName);
	
	TBranch *b_aData = mcChain.GetBranch("Beam");
 	b_aData->SetAddress(&beam);
	mcChain.SetBranchAddress("z", &z);
	mcChain.SetBranchAddress("theta" , &theta);
	mcChain.SetBranchAddress("p", &pSim);
	mcChain.SetBranchAddress("px", &pxSim);
	mcChain.SetBranchAddress("q", &q);
	mcChain.SetBranchAddress("pdgId", &pdgId);
	mcChain.SetBranchAddress("indexZ", &iSimZ);
	mcChain.SetBranchAddress("indexTh", &iSimTh);
	mcChain.SetBranchAddress("indexP", &iSimP);

}
#endif





