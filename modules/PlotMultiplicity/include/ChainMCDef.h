#ifndef CHAINMCDEF_H
#define CHAINMCDEF_H

#include <vector>
#include <string>
#include <TChain.h>
#include <TBranch.h>
#include "Functions.h"

using namespace std;


double binLowm[3];
double binHighm[3];
int indexZm, indexThm, indexPm;
double npim[2];
double npm[2];
double nKm[2];
double nem[2];


void ReadMC(TChain &chain, string fileName){
	chain.Add(fileName);
	
	chain.SetBranchAddress("zMin", &(binLowm[0]));
	chain.SetBranchAddress("zMax", &(binHighm[0]));
	chain.SetBranchAddress("thetaMin", &(binLowm[1]));
	chain.SetBranchAddress("thetaMax", &(binHighm[1]));
	chain.SetBranchAddress("pMin", &(binLowm[2]));
	chain.SetBranchAddress("pMax", &(binHighm[2]));
	chain.SetBranchAddress("indexZ", &indexZm);
	chain.SetBranchAddress("indexTh", &indexThm);
	chain.SetBranchAddress("indexP", &indexPm);
	chain.SetBranchAddress("npi", &(npim[0]));
	chain.SetBranchAddress("npiE", &(npim[1]));
	chain.SetBranchAddress("np", &(npm[0]));
	chain.SetBranchAddress("npE", &(npm[1]));
	chain.SetBranchAddress("nK", &(nKm[0]));
	chain.SetBranchAddress("nKE", &(nKm[1]));
	chain.SetBranchAddress("ne", &(nem[0]));
	chain.SetBranchAddress("neE", &(nem[1]));

}
#endif





