#ifndef CHAINDATADEF_H
#define CHAINDATADEF_H

#include <vector>
#include <string>
#include <TChain.h>
#include <TBranch.h>
#include "Functions.h"

using namespace std;

double binLowd[3];
double binHighd[3];
int indexZd, indexThd, indexPd;
double npid[2];
double npd[2];
double nKd[2];
double ned[2];


void ReadData(TChain &chain, string fileName){
	chain.Add(fileName);
	
	chain.SetBranchAddress("zMin", &(binLowd[0]));
	chain.SetBranchAddress("zMax", &(binHighd[0]));
	chain.SetBranchAddress("thetaMin", &(binLowd[1]));
	chain.SetBranchAddress("thetaMax", &(binHighd[1]));
	chain.SetBranchAddress("pMin", &(binLowd[2]));
	chain.SetBranchAddress("pMax", &(binHighd[2]));
	chain.SetBranchAddress("indexZ", &indexZd);
	chain.SetBranchAddress("indexTh", &indexThd);
	chain.SetBranchAddress("indexP", &indexPd);
	chain.SetBranchAddress("npi", &(npid[0]));
	chain.SetBranchAddress("npiE", &(npid[1]));
	chain.SetBranchAddress("np", &(npd[0]));
	chain.SetBranchAddress("npE", &(npd[1]));
	chain.SetBranchAddress("nK", &(nKd[0]));
	chain.SetBranchAddress("nKE", &(nKd[1]));
	chain.SetBranchAddress("ne", &(ned[0]));
	chain.SetBranchAddress("neE", &(ned[1]));

}
#endif





