#include "dEdxResolution.h"
#include <TF1.h>
#include <TH1D.h>
#include <TFile.h>
#include <stdlib.h>
#include <math.h>
#include <TMath.h>

dEdxResolution::dEdxResolution(string fileName, vector<double> &ranges){


	if(ranges.size() != 8){
		cerr << __FUNCTION__ << ": Size of the vector<double> ranges must be 8 with this order: r_piMean, r_piWidth, r_pMean, r_pWidth, r_KMean, r_KWidth, r_eMean, r_eWidth. " << endl;
		exit(EXIT_FAILURE);
	}
	
	fPionPars[0] = ranges.at(0);
	fPionPars[1] = ranges.at(1);
	fProtonPars[0] = ranges.at(2);
	fProtonPars[1] = ranges.at(3);
	fKaonPars[0] = ranges.at(4);
	fKaonPars[1] = ranges.at(5);
	fElectronPars[0] = ranges.at(6);
	fElectronPars[1] = ranges.at(7);	
	
	TFile input(fileName.c_str(), "READ");
	
	fdEdxMean = (TF1*) input.Get("dEdxFit");
	fdEdxWidth = (TF1*) input.Get("dEdxWidthFit");
	//TTree *tree = (TTree*) input.Get("dEdxMap");
	input.Close();
	
	cout << fdEdxMean->Eval(1.) << endl;
	/*double bgVal;
	double dEdxVal;
	TBranch *bg = tree->GetBranch("bg");
	TBranch *dEdx = tree->GetBranch("dEdx");
	
	bg->SetAddress(&bgVal);	
	dEdx->SetAddress(&dEdxVal);	
	int nEntries = tree->GetEntries();
	
	for(int i=0; i<nEntries; i++){
	

		tree->GetEntry(i);
		fBetaGamma.push_back(bgVal);
		fMean.push_back(dEdxVal);
		
		if(i==1){
			fStep = bgVal-fBetaGamma.at(0);
		}
	}*/	
}



double dEdxResolution::GetMean(double bg){
	
	/*if(fMean.size() < 2){
		cerr << __FUNCTION__ << ": dE/dx resolution is not properly set. More than 2 values needed." << endl;
		exit(EXIT_FAILURE);
	}
		
	double indexD = (bg - fBetaGamma.at(0))/fStep;
	int indexMin = floor(indexD);
	int indexMax = ceil(indexD);
	
	if(indexMin == indexMax) 
		return fMean.at(indexMin);
	
	//if(indexMax>fMean.size()-1) return fMean.at(fMean.size()-1);
	if(indexMax>fMean.size()-1) return 1.51;	
	double a = (fMean.at(indexMax) - fMean.at(indexMin))/(fBetaGamma.at(indexMax) - fBetaGamma.at(indexMin));
	double b = fMean.at(indexMin) - a*fBetaGamma.at(indexMin);
	return a*bg+b;*/
	
	return fdEdxMean->Eval(TMath::Log10(bg));
	
}

double dEdxResolution::GetWidth(double bg1, double bg2){
	double mean1 = GetMean(bg1);
	double mean2 = GetMean(bg2);
	return fdEdxWidth->Eval(TMath::Log10((bg1+bg2)/2.)) + fabs(mean2-mean1)/4.;
}

double dEdxResolution::GetWidth(double bg){
	//return fWidth;
	return fdEdxWidth->Eval(TMath::Log10(bg));
}

