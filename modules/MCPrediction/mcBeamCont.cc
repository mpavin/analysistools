#include <vector>
#include <string>
#include <iostream>
#include "Functions.h"
#include "Constants.h"
#include <TH1D.h>
#include <TH2D.h>
#include <TFile.h>
#include <TChain.h>
#include <TTree.h>
#include <TMath.h>
#include <math.h>

using namespace std;


int main(int argc, char* argv[]){

	string input;
	string output;
	double xt;
	double yt;	
	string opt = argv[1];
	if(opt == "0"){
		cout << "T2" << endl;
		input = "list.txt";
		output = "res.root";
		xt = 0.1153;
		yt = 0.1258;
	}
	else if(opt == "1"){
		cout << "T3" << endl;
		input = "list1.txt";
		output = "res1.root";
		xt = 0.1153;
		yt = 0.1258;
	}
	else if(opt == "2"){
		cout << "T2K" << endl;
		input = "list2.txt";
		output = "res2.root";
		xt = 0.0;
		yt = 0.0;
	}
	else if(opt == "3"){
		cout << "NA61scaled" << endl;
		input = "list3.txt";
		output = "res3.root";
		xt = 0.1153;
		yt = 0.1258;
	}
	else if(opt == "4"){
		cout << "T2Kshift" << endl;
		input = "list4.txt";
		output = "res4.root";
		xt = 0.00;
		yt = -0.04;
	}
	
	TChain chain("h1000");
	

	ifstream ifs;
	string line;
	ifs.open(input.c_str());
		
	if (!ifs.is_open()){
		cerr << "[ReadInputListFile] Could not open infile. Exiting..." << endl;			
		exit (101);
	}
    
	while (getline(ifs, line)){
		if(line.at(0)=='#')
			continue;
		chain.Add(line.c_str());
	}
	ifs.close();		

	Int_t           protnum;
  	Int_t           ipart;
  	Float_t         mom;
  	Float_t         pos[3];
  	Float_t         vec[3];

  // Declaration of leaf types for replica input
  	Int_t           pgen;
  	Int_t           ng;
  	Float_t         gpx[20];    //[ng]
  	Float_t         gpy[20];    //[ng]
  	Float_t         gpz[20];    //[ng]
  	Float_t         gvx[20];    //[ng]
  	Float_t         gvy[20];    //[ng]
  	Float_t         gvz[20];    //[ng]
  	Int_t           gpid[20];   //[ng]
  	Int_t           gmec[20];   //[ng]

	TBranch        *b_protnum;   //!
  	TBranch        *b_ipart;     //!
  	TBranch        *b_mom;       //!
  	TBranch        *b_pos;       //!
  	TBranch        *b_vec;       //!
  	TBranch        *b_pgen;      //!
  	TBranch        *b_ng;        //!
  	TBranch        *b_gpx;       //!
  	TBranch        *b_gpy;       //!
  	TBranch        *b_gpz;       //!
  	TBranch        *b_gvx;       //!
  	TBranch        *b_gvy;       //!
  	TBranch        *b_gvz;       //!
  	TBranch        *b_gpid;      //!
  	TBranch        *b_gmec;      //!
    
	chain.SetBranchAddress("protnum", &protnum, &b_protnum);
  	chain.SetBranchAddress("ipart", &ipart, &b_ipart);
  	chain.SetBranchAddress("mom", &mom, &b_mom);
  	chain.SetBranchAddress("pos", pos, &b_pos);
  	chain.SetBranchAddress("vec", vec, &b_vec);
  	chain.SetBranchAddress("pgen", &pgen, &b_pgen);
  	chain.SetBranchAddress("ng", &ng, &b_ng);
  	chain.SetBranchAddress("gpx", gpx, &b_gpx);
  	chain.SetBranchAddress("gpy", gpy, &b_gpy);
  	chain.SetBranchAddress("gpz", gpz, &b_gpz);
  	chain.SetBranchAddress("gvx", gvx, &b_gvx);
  	chain.SetBranchAddress("gvy", gvy, &b_gvy);
  	chain.SetBranchAddress("gvz", gvz, &b_gvz);
  	chain.SetBranchAddress("gpid", gpid, &b_gpid);
  	chain.SetBranchAddress("gmec", gmec, &b_gmec);
  	
  	
  	int nEntries = chain.GetEntries();

  	vector<TH2D> mcpredPiPos;
  	vector<TH2D> mcpredPiNeg;
  	for(int i = 0; i < 6; i++){
  	
  		string name1 = "thetaP_piPos_z" + ConvertToString(i+1.,0);
  		string name2 = "thetaP_piNeg_z" + ConvertToString(i+1.,0);
  		TH2D temp1(name1.c_str(), "", 25, 0, 25, 20, 0, 400);
  		TH2D temp2(name2.c_str(), "", 25, 0, 25, 20, 0, 400);
  		
  		mcpredPiPos.push_back(temp1);
  		mcpredPiNeg.push_back(temp2);
  	}

	/*TH2D cMat("c", "", nBbins, 0, nBbins, nPbins, 0, nPbins);
	TH2D beam("beam", "", 200, -2, 2, 200, -2, 2);
	TH1D beamspace("beamspace", "", nBbins, 0, nBbins);
	TH1D zPos("zPos", "", 110, -10, 100);*/
  	int eventId = -1;
  	int totev = 0;

	int bBinId = -99;

	//nEntries = 10000000;

	
	double zbord[7] = {0, 18, 36, 54, 72, 89.99, 90.01};
	double sc = 1;
  	for(int i = 0; i < nEntries; i++){
  		
  		chain.GetEntry(i);
		if(sqrt((gvx[0]-xt)*(gvx[0]-xt) + (gvy[0]-yt)*(gvy[0]-yt))>1.30) continue;
		

		if(protnum != eventId){

			if(sqrt((gvx[0]-xt)*(gvx[0]-xt) + (gvy[0]-yt)*(gvy[0]-yt))>0.7) sc = 0.6013;
			else sc = 1.1364;		
				
			eventId = protnum;
			totev++;
  			if(!(totev%100000))
  				cout << "Processing event # " << totev << endl;
	
		}
		
		
		if(ipart != 8 && ipart != 9) continue;
		
		if(sqrt((pos[0]-xt)*(pos[0]-xt) + (pos[1]-yt)*(pos[1]-yt))>1.33) continue;
		
		int zbin = -99;
		for(int j = 0; j < 6; j++){
			if(zbord[j] <= pos[2] && pos[2] < zbord[j+1]){
				zbin = j;
				break;
			} 
		}
		if(zbin < 0) continue;
		
		if(ipart == 8){
			mcpredPiPos.at(zbin).Fill(mom, 1000*acos(vec[2]), sc);
		}
		else if(ipart == 9){
			mcpredPiNeg.at(zbin).Fill(mom, 1000*acos(vec[2]), sc);
		}
		/*if(ipart != 14) continue;
		if(mom < 29.55) continue;
		
		zPos.Fill(pos[2]);
		if(pos[2] < 89.99) continue;
		bool skip = false;
		for(int j = 0; j < ng; j++){
			if(gmec[j] == 101){
				skip = true;
				break;
			}
		}
		if(skip) continue;
		totev++;*/
		/*if(protnum != eventId){
			
			double r = sqrt(TMath::Power(-gvz[0]*gpx[0]/gpz[0] + gvx[0] - targX,2) + TMath::Power(-gvz[0]*gpy[0]/gpz[0] + gvy[0]- targY,2));
			double r1 = sqrt(TMath::Power((90-gvz[0])*gpx[0]/gpz[0] + gvx[0]- targX ,2) + TMath::Power((90-gvz[0])*gpy[0]/gpz[0] + gvy[0]- targY ,2));
			
			//beam.Fill(-gvz[0]*gpx[0]/gpz[0] + gvx[0]- targX, -gvz[0]*gpy[0]/gpz[0] + gvy[0]- targY);
			
			int rbin = -99;
			for(int j = 0; j < nr; j++){
				if(rbord[j] <= r && r < rbord[j+1]){
					rbin = j;
					break;
				}
			}
			if(rbin >= 0){
				int r1bin = -99;
				for(int j = 0; j < nr1[rbin]; j++){
					if(rbord[rbin]+r1bord[rbin][j] <= r1 && r1 < rbord[rbin]+r1bord[rbin][j+1]){
						r1bin = j;
						break;
					}
				}
				if(r1bin >= 0){
					bBinId = r1bin+1;
					for(int j = 0; j < rbin; j++){
						bBinId += nr1[j];
					}
					beam.Fill(gvx[0], gvy[0]);
					beamspace.Fill(bBinId-0.5);
				}
				else{
					bBinId = -99;
				}
			}
			else{
				bBinId = -99;
			}
			eventId = protnum;
			totev++;
  			if(!(totev%100000))
  				cout << "Processing event # " << totev << endl;
  				
  			
		}
		if(bBinId < 0) continue;
		if(ipart!=8) continue;
		
		double z = pos[2];// + 657.51;
		double r = sqrt((pos[0]-targX)*(pos[0]-targX) + (pos[1]-targY)*(pos[1]-targY));
		if(r > 1.31) continue;
		if(z<0) continue;
		if(z>90.01) continue;

		int zbin = -99;
		for(int j = 0; j < nz; j++){
			if(zbord[j] <= z && z < zbord[j+1]){
				zbin = j;
				break;
			}
		}
		if(zbin < 0) continue;
		
		int thbin = -99;
		double th= 1000*acos(vec[2]);
		for(int j = 0; j < nth[zbin]; j++){
			if(thbord[j] <= th && th < thbord[j+1]){
				thbin = j;
				break;
			}			
		}
		if(thbin < 0) continue;
		
		int pbin = -99;

		for(int j = 0; j < np[zbin][thbin]; j++){
			if(pbord[thbin][j] <= mom && mom < pbord[thbin][j+1]){
				pbin = j;
				break;
			}			
		}	
		if(pbin < 0) continue;
		
		int bin = sumbins[zbin];
		for(int j = 0; j < thbin; j++){
			bin += np[zbin][j];
		}
		bin += pbin+1; 
		
		cMat.Fill(bBinId-0.5, bin-0.5);	*/
  	}
  	
  	cout << totev << endl;
  	TFile out(output.c_str(), "RECREATE");
  	out.cd();
  	
  	double tev = totev;
  	for(int i = 0; i < 6; i++){
  		mcpredPiPos.at(i).Scale(50000000./tev);
  		mcpredPiNeg.at(i).Scale(50000000./tev);
  		mcpredPiPos.at(i).Write();
  		mcpredPiNeg.at(i).Write();
  	}
  	/*beam.Write();
 	cMat.Write();
 	zPos.Write();
 	beamspace.Write();*/
  	out.Close();
}





