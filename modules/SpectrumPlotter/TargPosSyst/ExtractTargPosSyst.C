#include <vector>
#include <TH2Poly.h>

std::string ConvertToString(double x, int prec){
	std::stringstream ss;
	std::string val;

	ss << std::fixed << std::setprecision(prec);
	ss << x;
	ss >> val;

	return val;
}


void ExtractTargPosSyst(){

	int q = 1;
	string list[11] = { "../Results/dEdx_m2tof_6_16_17_pions.root",
						"../Results/dEdx_m2tof_6_16_17_pions_tgx+0.02.root",
						"../Results/dEdx_m2tof_6_16_17_pions_tgx-0.02.root",
						"../Results/dEdx_m2tof_6_16_17_pions_tgy+0.02.root",
						"../Results/dEdx_m2tof_6_16_17_pions_tgy-0.02.root",
						"../Results/dEdx_m2tof_6_16_17_pions_tgz+0.1.root",
						"../Results/dEdx_m2tof_6_17_17_pions_tgz-0.1.root",
						"../Results/dEdx_m2tof_6_17_17_pions_tgtx+0.0003.root",
						"../Results/dEdx_m2tof_6_17_17_pions_tgtx-0.0003.root",
						"../Results/dEdx_m2tof_6_17_17_pions_tgty+0.0003.root",
						"../Results/dEdx_m2tof_6_17_17_pions_tgty-0.0003.root"
					};

	string names[10] = {"errXplus", "errXminus", "errYplus", "errYminus", "errZplus", "errZminus", "errTXplus", "errTXminus", "errTYplus", "errTYminus"};
	vector<TH2Poly*> standard;
	vector<vector<TH2Poly*> > errPos;

	
	
	for(int i = 0; i < 6; i++){
		string name1 = "standard_z" + ConvertToString(i+1., 0);

		
		string title;
		if(i < 5) title = ConvertToString(i*18., 0) + " #leq z < " + ConvertToString(i*18. + 18., 0) + " cm";
		else title = "z = 90 cm";
		TH2Poly *temp1;

		standard.push_back(temp1);

		
		standard.at(i) = new TH2Poly(name1.c_str(), title.c_str(), 0, 30, 0, 400);

	}     
    
	for(int i = 0; i < 10; i++){
	
		vector<TH2Poly*> temp;
		for(int j = 0; j < 6; j++){	
			string name1 = names[i] + "_z" + ConvertToString(j+1., 0);
			
			string title;
			if(i < 5) title = ConvertToString(j*18., 0) + " #leq z < " + ConvertToString(j*18. + 18., 0) + " cm";
			else title = "z = 90 cm";
			TH2Poly *temp1;

			temp.push_back(temp1);
			//cerr << "radi" << endl;
			temp.at(j) = new TH2Poly(name1.c_str(), title.c_str(), 0, 30, 0, 400);	
			//cerr << "radi1" << endl;	
		}	
		//cerr << "r" << endl;
		errPos.push_back(temp);
	}
	
	vector<double> binLow;
	vector<double> binHigh;	
	
	for(int i = 0; i < 3; i++){
		binLow.push_back(0);
		binHigh.push_back(0);
	}
	int indexZ;
	int indexTh;
	int indexP;
	vector<double> *dEdxBin = NULL;
	vector<double> *m2tofBin = NULL;
	vector<double> *charge = NULL;
	
	TFile input(list[0].c_str(), "READ");
	
	TTree *tree = (TTree*) input.Get("dEdx_m2tof");
	tree->SetBranchAddress("zMin", &(binLow.at(0)));
	tree->SetBranchAddress("zMax", &(binHigh.at(0)));
	tree->SetBranchAddress("thetaMin", &(binLow.at(1)));
	tree->SetBranchAddress("thetaMax", &(binHigh.at(1)));
	tree->SetBranchAddress("pMin", &(binLow.at(2)));
	tree->SetBranchAddress("pMax", &(binHigh.at(2)));
	tree->SetBranchAddress("indexZ", &indexZ);
	tree->SetBranchAddress("indexTh", &indexTh);
	tree->SetBranchAddress("indexP", &indexP);
	tree->SetBranchAddress("dEdx", &dEdxBin);
	tree->SetBranchAddress("m2ToF", &m2tofBin);
	tree->SetBranchAddress("charge", &charge);
	
	int nEntries = tree->GetEntries();
	
	for(int i = 0; i < nEntries; i++){
		tree->GetEntry(i);

		double val = 0;
		for(int j = 0; j < charge->size(); j++){
			if(charge->at(j)*q < 0) continue;
			val+=1.;
		}
		standard.at(indexZ-1)->AddBin(binLow.at(2), binLow.at(1), binHigh.at(2), binHigh.at(1));

		TH2PolyBin *bin1 = (TH2PolyBin*) standard.at(indexZ-1)->GetBins()->At(standard.at(indexZ-1)->GetNumberOfBins()-1);
		bin1->SetContent(val);
		
	}
	input.Close();


	cerr << "radi" << endl;
	for(int i = 0; i < 10; i++){
		dEdxBin = NULL;
		m2tofBin = NULL;
		charge = NULL;
		TFile input1(list[i+1].c_str(), "READ");
		TTree *tree1 = (TTree*) input1.Get("dEdx_m2tof");
		
		nEntries = tree1->GetEntries();
		for(int j = 0; j < nEntries; j++){
			tree->GetEntry(j);
			double val = 0;
			for(int k = 0; k < charge->size(); k++){
				if(charge->at(k)*q < 0) continue;
				val+=1.;
			}	
			errPos.at(i).at(indexZ-1)->AddBin(binLow.at(2), binLow.at(1), binHigh.at(2), binHigh.at(1));

			TH2PolyBin *bin1 = (TH2PolyBin*) errPos.at(i).at(indexZ-1)->GetBins()->At(errPos.at(i).at(indexZ-1)->GetNumberOfBins()-1);
			
			TH2PolyBin *bin2 = (TH2PolyBin*) standard.at(indexZ-1)->GetBins()->At(errPos.at(i).at(indexZ-1)->GetNumberOfBins()-1);
			
			if(bin2->GetContent() < 1) bin1->SetContent(0);
			else bin1->SetContent((val - bin2->GetContent())/bin2->GetContent());	
							
		}
		input1.Close()
	}
	
	string add;
	if(q > 0) add = "Pos";
	else add = "Neg";
	string oname = "errMig" + add + ".root";
	TFile output(oname.c_str(), "RECREATE");
	output.cd();
	
	for(int i = 0; i < errPos.size(); i++){
		for(int j = 0; j < errPos.at(i).size(); j++){
			errPos.at(i).at(j)->Write();
		}
	}
	
	output.Close();
}
