#include "vector"
class TH2Poly;
#ifdef __CINT__ 
#pragma link C++ nestedclasses;
#pragma link C++ nestedtypedefs;
#pragma link C++ class vector<TH2Poly*>+;
#pragma link C++ class vector<TH2Poly*>::*;
#ifdef G__VECTOR_HAS_CLASS_ITERATOR
#pragma link C++ operators vector<TH2Poly*>::iterator;
#pragma link C++ operators vector<TH2Poly*>::const_iterator;
#pragma link C++ operators vector<TH2Poly*>::reverse_iterator;
#endif
#endif
