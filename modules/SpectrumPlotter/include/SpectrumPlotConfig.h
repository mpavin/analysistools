#ifndef SPECTRUMPLOTCONFIG_H
#define SPECTRUMPLOTCONFIG_H
#include <iostream>
#include <vector>
#include <string>

#include <vector>



using namespace std;

class SpectrumPlotConfig{

	public:
		SpectrumPlotConfig(){Read();}

		double GetTargetZ(){return fTargetZ;}
		string GetOutputDir(){return fOutput;}
		

		void SetOutputDir(string value){fOutput = value;}
		void SetTargetZ(double z){fTargetZ = z;}
		
		
	private:

		void Read();
		double fTargetZ;
		string fOutput;
		


};

#endif
