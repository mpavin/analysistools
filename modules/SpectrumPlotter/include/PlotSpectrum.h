#ifndef PLOTSPECTRUM_H
#define PLOTSPECTRUM_H


#include <iostream>
#include <vector>
#include <string>


using namespace std;

class PlotSpectrum{

	public:
		PlotSpectrum(string inputFile, bool isList): fInputFile(inputFile), fIsList(isList) {}
		void SetInputFile(string inputFile, bool isList){fInputFile = inputFile; fIsList = isList;}
		void Run();
		
		
		string GetInputFile(){return fInputFile;}
		bool IsListFile(){return fIsList;}
		
	private:

		string fInputFile;
		bool fIsList;

		
};
#endif
