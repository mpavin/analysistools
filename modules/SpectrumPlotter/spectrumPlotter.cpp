#include <iostream>
#include <fstream>
#include <cstdlib>
#include <evt/Event.h>
#include <evt/EventHeader.h>
#include <io/EventFileChain.h>

#include <boost/format.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/program_options.hpp>

#include "CConfig.h"
#include "PlotSpectrum.h"

using namespace std;
using namespace io;
using namespace evt;
using namespace boost;

namespace po = boost::program_options;

CentralConfig& cConfig = fwk::CentralConfig::GetInstance("bootstrap.xml", false, false);
int main(int argc, char* argv[])
{

	
  string cfgFile = "";	//Config file
  string input = "" ; //Input file
  string listfile = "";

//**************************************OPTIONS*************************************  
  po::options_description desc("Usage");

  desc.add_options()
    ("help,h",
     "Output this help.")
    ("bootstrap,b",
     po::value<string>(&cfgFile), "Bootstrap file.")  
     ("input,i",
     po::value<string>(&input), "Input root file.") 
     ("listfile,l",
     po::value<string>(&listfile), "Input root file.")     
    ;
  
  po::variables_map vm;
  try {
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);
  } catch (po::error& err) {
    cerr << "Command line error : " << err.what() << '\n'
	 << desc << endl;
    exit(EXIT_FAILURE);
  }
//*********************************************************************************


	cConfig.GetInstance(cfgFile, false, false);
	
	if(listfile.size() < 2){
		PlotSpectrum ana(input, false);
		ana.Run();
	}
	else if(input.size() < 2){
		PlotSpectrum ana(listfile, true);
		ana.Run();
	}
	else{
		cerr << "Input file or list not provided!" << endl;
		exit(EXIT_FAILURE);
	}
	
}
