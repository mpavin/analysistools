#include <stdlib.h>

#include "SpectrumPlotConfig.h"
#include <fstream>
#include <fwk/CentralConfig.h>
#include <utl/Branch.h>
#include <utl/BranchIterator.h>
#include "CConfig.h"
#include <math.h>  

using namespace std;
using namespace fwk;
using namespace utl; 

void SpectrumPlotConfig::Read(){
	Branch spectrumPlot = cConfig.GetTopBranch("SpectrumPlotter");

	if(spectrumPlot.GetName() != "SpectrumPlotter"){
		cout << __FUNCTION__ << ": Wrong SpectrumPlotter config file! Top branch name doesn't match! Please check your configuration files! Exiting..." << endl;
		exit (EXIT_FAILURE);
	}

	for(BranchIterator it = spectrumPlot.ChildrenBegin(); it != spectrumPlot.ChildrenEnd(); ++it){
		Branch child = *it;
		
		if(child.GetName() == "targetZ"){
			child.GetData(fTargetZ);
		} 
				
		else if(child.GetName() == "output"){
			child.GetData(fOutput);
		}

	}

}

