#include <stdlib.h>
#include "PhaseSpace.h"
#include "PlotSpectrum.h"
#include "SpectrumPlotConfig.h"
#include "TrackSelection.h"
#include "PlotHisto.h"
#include <sstream>
#include <fstream>
#include <TChain.h>
#include <TFile.h>
#include <TH2D.h>
#include <TTree.h>
#include <TBranch.h>
#include "StrTools.h"

using namespace utl; 
using namespace StrTools;
void PlotSpectrum::Run(){

	PhaseSpace phaseSpace;
	SpectrumPlotConfig cfg;
	TrackSelection trkSel;

	TChain chain("Data");
	
	if(fIsList){
		ifstream ifs;
		string line;
		ifs.open(fInputFile.c_str());
		
		if (!ifs.is_open()){
			cerr << "[ReadInputListFile] Could not open infile. Exiting..." << endl;			
			exit (101);
		}
    
		while (getline(ifs, line)){
			if(line.at(0)=='#')
				continue;
			chain.Add(line.c_str());
		}
		ifs.close();		
	}
	else {
		chain.Add(fInputFile.c_str());
	}


	BeamA beamD;
	vector<double> *x = NULL;
	vector<double> *xE = NULL;
	vector<double> *y = NULL;
	vector<double> *yE = NULL;
	vector<double> *z = NULL;
	vector<double> *px = NULL;
	vector<double> *py = NULL;
	vector<double> *pz = NULL;
	vector<double> *distTarg = NULL;
	vector<double> *zStart = NULL;
	vector<double> *zLast = NULL;
	vector<double> *dEdx = NULL;
	vector<int> *qD = NULL;
	vector<int> *nVTPC1 = NULL;
	vector<int> *nVTPC2 = NULL;
	vector<int> *nGTPC = NULL;
	vector<int> *nMTPC = NULL;
	vector<int> *topology = NULL;
	vector<int> *nTOF = NULL;
	vector<double> *m2TOF = NULL;	
	vector<int> *tofScId = NULL;
	
	/*vector<int> *xSim = NULL;
	vector<int> *ySim = NULL;
	vector<int> *zSim = NULL;
	vector<int> *pdgId = NULL;
	vector<int> *process = NULL;*/
	int runNumber;


		
	TBranch *b_aData = chain.GetBranch("Beam");
 	b_aData->SetAddress(&beamD);
	chain.SetBranchAddress("x", &x);
	chain.SetBranchAddress("xE", &xE);
	chain.SetBranchAddress("y", &y);
	chain.SetBranchAddress("yE", &yE);
	chain.SetBranchAddress("z", &z);
	chain.SetBranchAddress("px", &px);
	chain.SetBranchAddress("py", &py);
	chain.SetBranchAddress("pz", &pz);
	chain.SetBranchAddress("distTarg", &distTarg);
	chain.SetBranchAddress("zStart", &zStart);
	chain.SetBranchAddress("zLast", &zLast);
	chain.SetBranchAddress("dEdx", &dEdx);
	chain.SetBranchAddress("q", &qD);
	chain.SetBranchAddress("nVTPC1", &nVTPC1);
	chain.SetBranchAddress("nVTPC2", &nVTPC2);
	chain.SetBranchAddress("nGTPC", &nGTPC);
	chain.SetBranchAddress("nMTPC", &nMTPC);
	chain.SetBranchAddress("topology", &topology);
	chain.SetBranchAddress("nTOF", &nTOF);
	chain.SetBranchAddress("m2TOF", &m2TOF);
	chain.SetBranchAddress("tofScId", &tofScId);
	chain.SetBranchAddress("runNumber", &runNumber);
	
	/*chain.SetBranchAddress("zSim", &zSim);
	chain.SetBranchAddress("xSim", &xSim);
	chain.SetBranchAddress("ySim", &ySim);
	chain.SetBranchAddress("pdgId", &pdgId);
	chain.SetBranchAddress("process", &process);*/
	
	string name = cfg.GetOutputDir() + "/" + "dEdx_m2tof_" + GetTimeString() + ".root";
	TFile output(name.c_str(), "RECREATE");	

	PlotHisto momentumPlotsdEdx(2, "SpectrumHistos", "dEdx_pos", "dEdx_neg", "dEdx_all", "", NULL, NULL, NULL, NULL);
	PlotHisto momentumPlotsTOF(2, "SpectrumHistos", "m2tof_pos", "m2tof_neg", "m2tof_all", "");
	PlotHisto thetaP(2, "SpectrumHistos", "theta_p_pos", "theta_p_neg", "theta_p_all", "");

	
	int nEntries = chain.GetEntries();
	int nDim = phaseSpace.GetNumberOfDimensions();
	
	if(nDim != 3){
		cerr << __FUNCTION__ << ": dimension of the phase space must be 3! Exiting..." << endl;
		exit(EXIT_FAILURE);
	}
	

	vector<int> indexzTemp;
	vector<int> indexthTemp;
	vector<int> indexpTemp;
	vector<double> dEdxTemp;
	vector<double> m2tofTemp;
	vector<int> chargeTemp;
	vector<int> scintIdTemp;
	for(int i = 0; i < nEntries; ++i){
		
		chain.GetEntry(i,1);
		//cout << x->size() << " " << pdgId->size() << endl;
		if(!((i+1)%10000))
			cout << "Processing event # "  << i+1 << endl;
		for(int j = 0; j < x->size(); j++){
			
			//if(zSim->at(j) >= cfg.GetTargetZ()+90 ) continue;
			//
			//if(pdgId->at(j) != -321 ) continue;
			//if(process->at(j) != 9) continue;
			/*if(zSim->at(j) < -567.509){
				if(process->at(j) == 13) continue;
			}*/
			//cout << process->at(j) << endl;
			/*if(zSim->at(j) < -567.509){
				double dist = sqrt((xSim->at(j)-0.1350)*(xSim->at(j)-0.1350) + (ySim->at(j)-0.1258)*(ySim->at(j)-0.1258)) - 1.3;
				if(dist < 0.01) continue;
			}*/
			
			//cout << pdgId->at(j) << endl;
			//cout << "radi" << endl;
			NumberOfClusters nClusters;
			nClusters.VTPC1 = nVTPC1->at(j);
			nClusters.VTPC2 = nVTPC2->at(j);
			nClusters.GTPC = nGTPC->at(j);
			nClusters.MTPC = nMTPC->at(j);
			
			Point pos(x->at(j), y->at(j), z->at(j));
			Vector mom(px->at(j), py->at(j), pz->at(j));
			
			AnaTrack anaTrack;
			anaTrack.SetExtrapolatedPosition(pos);
			anaTrack.SetMomentum(mom);
			anaTrack.SetNumberOfClusters(nClusters);
			anaTrack.SetDistanceFromTarget(distTarg->at(j));
			if(nTOF->at(j) > 0) {
				anaTrack.SetToFMassSquared(m2TOF->at(j));
				anaTrack.SetToFScintillatorId(tofScId->at(j));
			}
			anaTrack.SetTopology(topology->at(j));
			anaTrack.SetCharge(qD->at(j));
			anaTrack.SetPositionErrors(xE->at(j), yE->at(j));
			anaTrack.SetLastZ(zLast->at(j));
			if(!trkSel.IsTrackGood(anaTrack))
				continue;	
			
			//cout << nTOF->at(j) << " " << anaTrack.HasToF() << endl;
			/*if(topology->at(j) == 12){
				if(px->at(j)*qD->at(j) < 0) continue;
			}*/
			/*if(topology->at(j) == 14){
				if(1000*acos(pz->at(j)/anaTrack.GetMomentum().GetMag()) > 40){
					if(px->at(j)*qD->at(j) < 0) continue;
				}
			}*/
			double zVal = z->at(j) - cfg.GetTargetZ();
			
			double pVal = sqrt(px->at(j)*px->at(j) + py->at(j)*py->at(j) + pz->at(j)*pz->at(j));
			double thetaVal = 1000*acos(pz->at(j)/pVal);
			
			/*if(qD->at(j) > 0){
				if(zVal < 18 && zVal >= 0){
					if(thetaVal >= 40 && thetaVal < 60){
						if(px->at(j) < 0){
							if(topology->at(j) == 6) continue;
							if(topology->at(j) == 7) continue;
							if(topology->at(j) == 11) continue;
							if(topology->at(j) == 14) continue;
							if(topology->at(j) == 15) continue;
						}
					}
				}
				else if(zVal >= 18 && zVal < 36){
					if(thetaVal >= 20 && thetaVal < 40){
						if(px->at(j) > 0){
							if(topology->at(j) == 15) continue;
						}
						else if(px->at(j) < 0){
							if(topology->at(j) == 6) continue;
							if(topology->at(j) == 14) continue;
						}
					}				
				}	
			}
			else{
				if(zVal < 18 && zVal >= 0){
					if(thetaVal >= 40 && thetaVal < 60){
						if(px->at(j) > 0){
							if(topology->at(j) == 7) continue;
						}
						else if(px->at(j) < 0){
							if(topology->at(j) == 11) continue;
						}
					}
				}
				else if(zVal >= 18 && zVal < 36){
					if(thetaVal >= 20 && thetaVal < 40){
						if(px->at(j) < 0){
							if(topology->at(j) == 11) continue;
							if(topology->at(j) == 14) continue;
						}
					}				
				}			
			}*/
			/*if(px->at(j)*qD->at(j) < 0){
				if(thetaVal < 30) continue;
				if(thetaVal >= 30 && thetaVal < 50){
					if(mom.GetMag() < 7) continue;
				}
				if(thetaVal >= 50 && thetaVal < 70){
					if(mom.GetMag() < 4.5) continue;
				}
				if(thetaVal >= 70 && thetaVal < 90){
					if(mom.GetMag() < 2.8) continue;
				}
			}*/
			if(qD->at(j) > 0){
				momentumPlotsdEdx.Fill(1, log10(pVal), dEdx->at(j));
				momentumPlotsTOF.Fill(1, pVal, m2TOF->at(j));
				thetaP.Fill(1, pVal, thetaVal);
			}
			else{
				momentumPlotsdEdx.Fill(2, log10(pVal), dEdx->at(j));
				momentumPlotsTOF.Fill(2, pVal, m2TOF->at(j));
				thetaP.Fill(2, pVal, thetaVal);
			}
			


			int nBinsz = phaseSpace.GetDimBins(1);	
			int iZ = -99;
			for(int k=0; k < nBinsz; k++){
				Bin& bin = phaseSpace.GetBin(k+1, 1, 1);
				if(bin.GetLowValue(1) > zVal || zVal >= bin.GetHighValue(1)) continue;
				iZ = k+1;		
			}
			if(iZ < 1) continue;
			
			int nBinsth = phaseSpace.GetDimBins(2, iZ);	
			int iTh = -99;
			for(int k=0; k < nBinsth; k++){
				Bin& bin = phaseSpace.GetBin(iZ, k+1, 1);
				if(bin.GetLowValue(2) > thetaVal || thetaVal >= bin.GetHighValue(2)) continue;
				iTh = k+1;					
			}	
			if(iTh < 1) continue;	
			
			int nBinsp = phaseSpace.GetDimBins(3, iZ, iTh);	
			int iP = -99;
			for(int k=0; k < nBinsp; k++){
				Bin& bin = phaseSpace.GetBin(iZ, iTh, k+1);
				if(bin.GetLowValue(3) > pVal || pVal >= bin.GetHighValue(3)) continue;
				iP = k+1;		
			}	
			if(iP < 1) continue;
			indexzTemp.push_back(iZ);
			indexthTemp.push_back(iTh);
			indexpTemp.push_back(iP);
									
			dEdxTemp.push_back(dEdx->at(j));
			m2tofTemp.push_back(m2TOF->at(j));
			chargeTemp.push_back(qD->at(j));
			scintIdTemp.push_back(tofScId->at(j));
																
		}
	}
	
	//input.Close();
	
	
	TTree *outTree = new TTree("dEdx_m2tof", "dEdx_m2tof");
	

	double zMin;
	double zMax;
	
	double thetaMin;
	double thetaMax;
	
	double pMin;
	double pMax;
	
	int indexZ;
	int indexTh;
	int indexP;
	
	vector<double> dEdxBin;
	vector<double> m2tofBin;
	vector<int> charge;
	vector<int> scintId;
	
	outTree->Branch("zMin", &zMin, "zMin/D");
	outTree->Branch("zMax", &zMax, "zMax/D");
	outTree->Branch("thetaMin", &thetaMin, "thetaMin/D");
	outTree->Branch("thetaMax", &thetaMax, "thetaMax/D");
	outTree->Branch("pMin", &pMin, "pMin/D");
	outTree->Branch("pMax", &pMax, "pMax/D");
	outTree->Branch("indexZ", &indexZ, "indexZ/I");
	outTree->Branch("indexTh", &indexTh, "indexTh/I");
	outTree->Branch("indexP", &indexP, "indexP/I");
	outTree->Branch("dEdx", "vector<double>", &dEdxBin);
	outTree->Branch("m2ToF", "vector<double>", &m2tofBin);
	outTree->Branch("charge", "vector<int>", &charge);
	outTree->Branch("scintId", "vector<int>", &scintId);


	int nBinsz = phaseSpace.GetDimBins(1);	
	for(int k=0; k < nBinsz; k++){

		int nBinsth = phaseSpace.GetDimBins(2, k+1);	
		for(int l = 0; l < nBinsth; l++){
			int nBinsp = phaseSpace.GetDimBins(3, k+1, l+1);
			for(int m = 0; m < nBinsp; m++){
			
				cout << "Processing bin: nz = " << k+1 << ", ntheta = " << l+1 << ", np = " << m+1 << endl;
				dEdxBin.clear();
				m2tofBin.clear();
				charge.clear();
				scintId.clear();
				Bin& bin = phaseSpace.GetBin(k+1, l+1, m+1);
				zMin	 = bin.GetLowValue(1);
				zMax	 = bin.GetHighValue(1);

				
				thetaMin = bin.GetLowValue(2);
				thetaMax = bin.GetHighValue(2);
				
				pMin = bin.GetLowValue(3);
				pMax = bin.GetHighValue(3);
				
				indexZ = k+1;
				indexTh = l+1;
				indexP = m+1;
				
				for(int n = 0; n < dEdxTemp.size(); n++){
					if (indexzTemp.at(n) != k+1)
						continue;
					if (indexthTemp.at(n) != l+1)
						continue;					
					if (indexpTemp.at(n) != m+1)
						continue;		
						
					dEdxBin.push_back(dEdxTemp.at(n));
					m2tofBin.push_back(m2tofTemp.at(n));			
					charge.push_back(chargeTemp.at(n));
					scintId.push_back(scintIdTemp.at(n));
				}
				output.cd();
				outTree->Fill();
				
			} 
		}
				
	}	
	
	outTree->Write();
	momentumPlotsdEdx.GetRSTHisto()->Write();
	momentumPlotsdEdx.GetWSTHisto()->Write();
	momentumPlotsTOF.GetRSTHisto()->Write();
	momentumPlotsTOF.GetWSTHisto()->Write();
	thetaP.GetRSTHisto()->Write();
	thetaP.GetWSTHisto()->Write();
		
	output.Close();
	
	
}



