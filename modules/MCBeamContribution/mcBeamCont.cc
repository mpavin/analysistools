#include <vector>
#include <string>
#include <iostream>
#include "Functions.h"
#include "Constants.h"
#include <TH1D.h>
#include <TH2D.h>
#include <TFile.h>
#include <TChain.h>
#include <TTree.h>
#include <TMath.h>
#include <math.h>

using namespace std;


int main(){

	string input = "list1.txt";
	TChain chain("h1000");
	

	ifstream ifs;
	string line;
	ifs.open(input.c_str());
		
	if (!ifs.is_open()){
		cerr << "[ReadInputListFile] Could not open infile. Exiting..." << endl;			
		exit (101);
	}
    
	while (getline(ifs, line)){
		if(line.at(0)=='#')
			continue;
		chain.Add(line.c_str());
	}
	ifs.close();		

	Int_t           protnum;
  	Int_t           ipart;
  	Float_t         mom;
  	Float_t         pos[3];
  	Float_t         vec[3];

  // Declaration of leaf types for replica input
  	Int_t           pgen;
  	Int_t           ng;
  	Float_t         gpx[20];    //[ng]
  	Float_t         gpy[20];    //[ng]
  	Float_t         gpz[20];    //[ng]
  	Float_t         gvx[20];    //[ng]
  	Float_t         gvy[20];    //[ng]
  	Float_t         gvz[20];    //[ng]
  	Int_t           gpid[20];   //[ng]
  	Int_t           gmec[20];   //[ng]

	TBranch        *b_protnum;   //!
  	TBranch        *b_ipart;     //!
  	TBranch        *b_mom;       //!
  	TBranch        *b_pos;       //!
  	TBranch        *b_vec;       //!
  	TBranch        *b_pgen;      //!
  	TBranch        *b_ng;        //!
  	TBranch        *b_gpx;       //!
  	TBranch        *b_gpy;       //!
  	TBranch        *b_gpz;       //!
  	TBranch        *b_gvx;       //!
  	TBranch        *b_gvy;       //!
  	TBranch        *b_gvz;       //!
  	TBranch        *b_gpid;      //!
  	TBranch        *b_gmec;      //!
    
	chain.SetBranchAddress("protnum", &protnum, &b_protnum);
  	chain.SetBranchAddress("ipart", &ipart, &b_ipart);
  	chain.SetBranchAddress("mom", &mom, &b_mom);
  	chain.SetBranchAddress("pos", pos, &b_pos);
  	chain.SetBranchAddress("vec", vec, &b_vec);
  	chain.SetBranchAddress("pgen", &pgen, &b_pgen);
  	chain.SetBranchAddress("ng", &ng, &b_ng);
  	chain.SetBranchAddress("gpx", gpx, &b_gpx);
  	chain.SetBranchAddress("gpy", gpy, &b_gpy);
  	chain.SetBranchAddress("gpz", gpz, &b_gpz);
  	chain.SetBranchAddress("gvx", gvx, &b_gvx);
  	chain.SetBranchAddress("gvy", gvy, &b_gvy);
  	chain.SetBranchAddress("gvz", gvz, &b_gvz);
  	chain.SetBranchAddress("gpid", gpid, &b_gpid);
  	chain.SetBranchAddress("gmec", gmec, &b_gmec);
  	
  	
  	int nEntries = chain.GetEntries();

  	

	TH2D cMat("c", "", nBbins, 0, nBbins, nPbins, 0, nPbins);
	TH2D beam("beam", "", 200, -2, 2, 200, -2, 2);
	TH1D beamspace("beamspace", "", nBbins, 0, nBbins);
  	int eventId = -1;
  	int totev = 0;

	int bBinId = -99;

	//nEntries = 10000000;
  	for(int i = 0; i < nEntries; i++){
  		
  		chain.GetEntry(i);

		if(protnum != eventId){
			
			double r = sqrt(TMath::Power(-gvz[0]*gpx[0]/gpz[0] + gvx[0] - targX,2) + TMath::Power(-gvz[0]*gpy[0]/gpz[0] + gvy[0]- targY,2));
			double r1 = sqrt(TMath::Power((90-gvz[0])*gpx[0]/gpz[0] + gvx[0]- targX ,2) + TMath::Power((90-gvz[0])*gpy[0]/gpz[0] + gvy[0]- targY ,2));
			
			//beam.Fill(-gvz[0]*gpx[0]/gpz[0] + gvx[0]- targX, -gvz[0]*gpy[0]/gpz[0] + gvy[0]- targY);
			
			int rbin = -99;
			for(int j = 0; j < nr; j++){
				if(rbord[j] <= r && r < rbord[j+1]){
					rbin = j;
					break;
				}
			}
			if(rbin >= 0){
				int r1bin = -99;
				for(int j = 0; j < nr1[rbin]; j++){
					if(rbord[rbin]+r1bord[rbin][j] <= r1 && r1 < rbord[rbin]+r1bord[rbin][j+1]){
						r1bin = j;
						break;
					}
				}
				if(r1bin >= 0){
					bBinId = r1bin+1;
					for(int j = 0; j < rbin; j++){
						bBinId += nr1[j];
					}
					beam.Fill(gvx[0], gvy[0]);
					beamspace.Fill(bBinId-0.5);
				}
				else{
					bBinId = -99;
				}
			}
			else{
				bBinId = -99;
			}
			eventId = protnum;
			totev++;
  			if(!(totev%100000))
  				cout << "Processing event # " << totev << endl;
  				
  			
		}
		if(bBinId < 0) continue;
		if(ipart!=8) continue;
		
		double z = pos[2];// + 657.51;
		double r = sqrt((pos[0]-targX)*(pos[0]-targX) + (pos[1]-targY)*(pos[1]-targY));
		if(r > 1.31) continue;
		if(z<0) continue;
		if(z>90.01) continue;

		int zbin = -99;
		for(int j = 0; j < nz; j++){
			if(zbord[j] <= z && z < zbord[j+1]){
				zbin = j;
				break;
			}
		}
		if(zbin < 0) continue;
		
		int thbin = -99;
		double th= 1000*acos(vec[2]);
		for(int j = 0; j < nth[zbin]; j++){
			if(thbord[j] <= th && th < thbord[j+1]){
				thbin = j;
				break;
			}			
		}
		if(thbin < 0) continue;
		
		int pbin = -99;

		for(int j = 0; j < np[zbin][thbin]; j++){
			if(pbord[thbin][j] <= mom && mom < pbord[thbin][j+1]){
				pbin = j;
				break;
			}			
		}	
		if(pbin < 0) continue;
		
		int bin = sumbins[zbin];
		for(int j = 0; j < thbin; j++){
			bin += np[zbin][j];
		}
		bin += pbin+1; 
		
		cMat.Fill(bBinId-0.5, bin-0.5);	
  	}
  	
  	TFile out("res.root", "RECREATE");
  	out.cd();
  	beam.Write();
 	cMat.Write();
 	beamspace.Write();
  	out.Close();
}





