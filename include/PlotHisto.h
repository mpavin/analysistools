//*******************************************************************************
/**
*	\class PlotHisto
*	\details Wraps Root classes TH1D or TH2D
*	One can choose 1D or 2D histograms.
*	There are 3 histograms, RST, WST and ALL.
*	It is useful beacuse often one wants to examine separately RST and WST.
*	Histogram can be plotted on canvases.
*	Parameters (labels, size, titles, bin number, range) are read from xml config file.
*
*	\author M. Pavin\n
*	E-mail: mpavin@cern.ch
*	\date 10. 10. 2015.
*/
//*******************************************************************************
#ifndef PLOTHISTO_H
#define PLOTHISTO_H

#include <TH2D.h>
#include <TCanvas.h>

using namespace std;

class PlotHisto{

	public:

		PlotHisto(int dim, string config, string cNameR, string cNameW, string cNameA, string nameAdd, double *range1=NULL, double *range2=NULL, int *nBins1=NULL, int *nBins2=NULL);
		
		//~PlotHisto();
		///< Class constructor.
		
		int GetDim(){return fDim;} ///< Returns dimension of the histograms.
		TH1* GetRSTHisto(); ///< Returns RST histogram.
		TH1* GetWSTHisto();	///< Returns WST histogram.
		TH1* GetAllHisto();	///< Returns All histogram.
				
		TCanvas* GetRSTCanvas(); ///< Returns RST canvas.
		TCanvas* GetWSTCanvas(); ///< Returns WST canvas.
		TCanvas* GetAllCanvas(); ///< Returns All canvas.
				
		void Fill(int rst, double p, double res = 0); 
		/**<
		*	Fills histograms.
		*	If rst = 1, fills RST and All histograms.
		*	If wst = 2, fills WST and All histograms. 
		*	If histrogram is 1D uses just p value.
		*/
	private:


		void Init(string config, string cName, string nameAdd, int dim, int rst, double *range=NULL, int *num=NULL);
		/**<
		*	Reads histogram options from xml file and
		*	branch called "cName".
		*/
		bool fPlotRST;
		bool fPlotWST;
		bool fPlotAll;
		
		TH1D *fRST1;
		TH1D *fWST1;
		TH1D *fAll1;
		
		TH2D *fRST2;
		TH2D *fWST2;
		TH2D *fAll2;
		
		TCanvas *fCRST;
		TCanvas *fCWST;
		TCanvas *fCAll;
		
		int fDim;
};

#endif
