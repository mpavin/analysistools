/**
*	\file
*	\details Defines central config from SHINE
*	It is used to get magnetic field from NA61/SHINE database
*	and any other configuration file.
*	It needs bootstrap.xml file in which all config files are defined. 
*
*	\author M. Pavin\n
*	E-mail: mpavin@cern.ch
*	\date 10. 10. 2015.
*/

#ifndef CCONFIG_H
#define CCONFIG_H
#include <fwk/CentralConfig.h>

using namespace fwk;

extern CentralConfig& cConfig;

#endif
