/*!
* 	\class AnaTrack
* 	\details Class used for applying cuts on track parameters. \n
* 	Information needed for LT analysis are stored in 2 SHINE classes: Track and VertexTrack
* 	This class contains information from VertecTrack which are easily accessible and has pointer to Track structure.
* 	It also contains pointer to TrackExtrapolation class which is initialized with correct track parameters.\n
*
*	This class will be improved.
*
*	\author M. Pavin \n
*	E-mail: mpavin@cern.ch
*	\date 10. 10. 2015.
*/
#ifndef ANATRACK_H
#define ANATRACK_H

#include <iostream>
#include <string>
#include <map>
#include "Constants.h"
#include <evt/rec/Track.h>
#include <evt/rec/RecEventConst.h>
#include "TrackExtrapolation.h"
#include "TargetAna.h"
#include <utl/Point.h>
#include <utl/Vector.h>

using namespace std;
using namespace utl;
using namespace evt;
using namespace evt::rec;     
     

struct NumberOfClusters{
	int all;
	int VTPC1;
	int VTPC2;
	int GTPC;
	int MTPC;
};
///< Struct containing information about number of clusters which is easily accessible. 

class AnaTrack{

	public:
		
		AnaTrack(const Track &track, TrackExtrapolation &trackExtrapolation){ 
			//fMomentum = track.GetMomentum();
			//fExtrapolatedPosition = Point(0,0,0); 
			
			fHasToF = false;
			fTrackExtrapolation = &trackExtrapolation; 
			Set(track);
		} ///< Class constructor with Track and TrackExtrapolation
		AnaTrack(TrackExtrapolation &trackExtrapolation){
			fZLast = 0;
			fHasToF = false;
			fTrack = NULL;
			fTrackExtrapolation = &trackExtrapolation;
		}
		///< Class constructor with TrackExtrapolation
		AnaTrack() {
			fZLast = 0;
			fHasToF = false;	
			fTrack = NULL;	
			
		} ///< Class constructor without parameters.
		
		//************************Set functions******************************************

		void SetMomentum(Vector &momentum){ fMomentum = &momentum;} ///< Sets track momentum. Usually used after extrapolation in MF.
		void SetCharge(int charge){fCharge = charge;} ///< Sets track charge.

		void SetExtrapolatedPosition(Point &point){fExtrapolatedPosition = point; fExtrapolatedFlag=true;} 
		///< Sets extrapolated position of the track. 
		
		void SetDistanceFromTarget(double distance){fDistanceFromTarget = distance; fExtrapolatedFlag=true;}
		///< Sets distance from the target.
		
		void SetNumberOfClusters(NumberOfClusters nClusters){fNumberOfClusters = nClusters;}
		///< Sets number of clusters (uses struct NumberOfClusters).
		
		void SetToFMassSquared(double massSqToF){fMassSqToF = massSqToF; fHasToF = true;}
		///< Sets tof mass squared

		void SetToFScintillatorId(int scintillator){fToFScintillator = scintillator; fHasToF = true;}
		///< Sets TOF-F scintillator id (goes from 1 to 80).
		
		void SetTopology(int topology){fTopology = topology;}
		///< Sets tracks topology (look at Constants.h)
		
		void SetLastZ(double zLast){fZLast = zLast;}
		
		void SetTrack(const Track& track){Set(track);}
		///< Initialize track extrapolation with track parameters. Sets Track pointer. 


		void SetPositionErrors(double xE, double yE){fPositionErrors.push_back(xE); fPositionErrors.push_back(yE);}
		//************************Get functions******************************************


		const Vector& GetMomentum(){return *fMomentum;}
		 ///< Returns reference to track momentum. Momentum is stored in Vector class from SHINE utl namespace.
		int GetCharge(){return fCharge;}

		Point& GetExtrapolatedPosition(){return fExtrapolatedPosition;}
		///< Returns extrapolated track position. Position is stored in Point class from SHINE utl namespace.
		
		double GetDistanceFromTarget(){return fDistanceFromTarget;}
		///< Returns extrapolated track distance from the target.
		
		NumberOfClusters& GetNumberOfClusters(){return fNumberOfClusters;}
		///< Returns reference to the struct containing number of clusters.
		
		double GetToFMassSquared(){return fMassSqToF;} ///< Returns TOF mass squared. 
		int GetToFScintillatorId(){return fToFScintillator;} ///< Returns TOF scinitllator id. 
		
		int GetTopology(){return fTopology;} ///< Returns track topology.
		double GetLastZ(){return fZLast;}
		vector<double>& GetPositionErrors(){return fPositionErrors;} ///< Returns extrapolated position error. 

		bool IsExtrapolated(){return fExtrapolatedFlag;} ///< Returns extrapolation status.
		bool HasToF(){return fHasToF;}	///< Checks if TOF hit is present.
		const Track& GetTrack(){return *fTrack;}; ///< Returns pointer to the Track
		void DoExtrapolation(); ///< Does extrapolation in MF towards the target or beam track.
		TrackExtrapolation* GetTrackExtrapolation(){return fTrackExtrapolation;} ///< Returns pointer to TrackExtrapolation.
	private:

		const Track* fTrack;
		void Set(const Track& track);
		int FindTopology();
		int fCharge;
		double fZLast;
		const Vector *fMomentum;
		NumberOfClusters fNumberOfClusters;
		Point fExtrapolatedPosition;
		vector<double> fPositionErrors;
		double fDistanceFromTarget;
		
		double fMassSqToF;
		int fToFScintillator;

		int fTopology;

		bool fExtrapolatedFlag;
		bool fHasToF;

		TrackExtrapolation *fTrackExtrapolation;
};

#endif
