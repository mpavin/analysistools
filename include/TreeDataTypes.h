//*******************************************************************************
/**
*	\file
*	TreeDataTypes
*	This file contains definitions of structs used for saving information in root trees
*
*	\author M. Pavin\n
*	Email: mpavin@cern.ch
*	\date 10. 10. 2015.
*/
//*******************************************************************************

#ifndef TREEDATATYPES_H
#define TREEDATATYPES_H

#include <iostream>

using namespace std;

struct SpectrumdEdxToF{

	double x;
	double xE;
	double y;
	double yE;
	double z;
	double distFTarg;
	double xS;
	double yS;
	double zS;
	
	double px;
	double py;
	double pz;

	double dEdx;
	double m2ToF;

	int scintillatorId;
	int topology;
	int q;
	
	int runNumber;
};
/**<
*	Struct SpectrumdEdxToF is used in DataAna module
*	It contains extrapolated position and momentum of the track, dE/dx, m^2_TOF, scintillator id, topology, charge 
*	and run number. 
*	It is also used by SpectrumPlotter when this inormation is read from the root file. 
*/

struct Position{
	double x;
	double y;
	double z;
	double xE;
	double yE;
};

struct Momentum{
	double px;
	double py;
	double pz;
	double pxE;
	double pyE;
	double pzE;
};

struct MomentumWE{
	double px;
	double py;
	double pz;
};
struct TPCA{
	int nVTPC1;
	int nVTPC2;
	int nGTPC;
	int nMTPC;
	int topology;
};

struct BeamA{
	double aX;
	double bX;
	double aY;
	double bY;
};

struct TOFA{
	int nTOF;
	double m2TOF;
	int firstScId;
};
struct MCData{

	BeamA beam;
	Position sim_vert;
	Position sim_rec_first;
	Position sim_rec_last;
	Position sim_rec_extrap;
	
	Momentum sim_p;
	Momentum sim_rec_p;
	Momentum extrap_p;
	
	TPCA clusters_sim;
	TPCA clusters_rec;
	int topology;
	double distTarg;
	int qSim;
	int qRec;
	double dEdx;
	TOFA tof;
	
	int pdgId;

};

struct CutVal{

	bool intFlag;
	double valI;
	double valD;
	bool selected;
};
/**<
*	Struct CutVal is used by class Selection to store selected and unselected parameter values.
*/
struct ResVal{
	int topology;
	int nClusters;
	int VTPC1;
	int VTPC2;
	int GTPC;
	int MTPCL;
	int MTPCR;
	int RST;
	double p;
	double chi2Ndf;
	double resolution;
	double theta;
	double phi;
};
/**<
*	Struct ResVal is used by module MomentumResolution and MomentumResolutionPlotter.
*	it contains information about number of clusters in the TPCs anf goodness of track fit.
*/
struct AlignmentData{
	double beamX;
	double beamY;
	double beamSlopeX;
	double beamInterceptX;
	double beamSlopeY;
	double beamInterceptY;	
	double px;
	double py;
	double pz;
	double x;
	double y;
	double z;
	/*vector<double> xS;
	vector<double> yS;
	vector<double> zS;*/
	double distToBeamTrack;
	int q;
};
/**<
*	Struct ResVal is used by module TargetAna and TargetPos.
*	it contains information needed for target alignment procedure.
*/

struct BeamData{
	
	double bpd1x;
	double bpd1y;
	double bpd1z;
	double bpd1Ex;
	double bpd1Ey;
	double bpd2x;
	double bpd2y;
	double bpd2z;
	double bpd2Ex;
	double bpd2Ey;
	double bpd3x;
	double bpd3y;
	double bpd3z;
	double bpd3Ex;
	double bpd3Ey;
	double slopex;
	double interceptx;
	double chi2Ndfx;
	double slopey;
	double intercepty;
	double chi2Ndfy;
	int trigger;
};
/**<
*	Struct BeamData is used by module Beam and BeamPlot.
*	It contains beam postions at BPD 1, 2, 3 and parameters of the beam fit.
*/
#endif


