//*******************************************************************************
/**
*	\class Selection
*	\details Contains vector of Cuts.
*	Reads cut parameters from xml config file.
*	
*	\author M. Pavin\n
*	E-mail: mpavin@cern.ch
*	\date 10. 10. 2015.
*/
//*******************************************************************************
#ifndef SELECTION_H
#define SELECTION_H

#include "Cut.h"
#include <TH1D.h>
#include <TH2D.h>
#include <TFile.h>
#include <map>
#include <vector>


class Selection {
	public:
		Selection(string cuts):  fNEvents(0), fInitHisto(false) {Read(cuts);}
 
  		void AddCut(Cut &c){fCut.push_back(c);}  
  		///< Adds new cut.
  		int GetNEvents(){return fNEvents;}
  		///< Returns number of events (tracks) which passed every cut.
  		int GetNEvents() const {return fNEvents;}
  		///< Returns const number of events (tracks) which passed every cut.
  		vector <Cut> &GetCuts(){return fCut;}
  		///< Returns reference to vector containing all cuts. 
  		const vector <Cut> &GetCuts() const {return fCut;}
  		///< Returns const reference to vector containing all cuts. 
  		Cut &GetCut(int index){return fCut.at(index);}
  		///< Returns reference to the cut at index.
  		const Cut &GetCut(int index) const {return fCut.at(index);}
   		///< Returns const reference to the cut at index. 

  
  		void Reset () {
    			fNEvents = 0;
    			fCut.clear();
  		} ///< Deletes all cuts and sets number of selected events (tracks) to 0.
  		
		void Print(){
			for (vector <Cut>::iterator it=fCut.begin(); it < fCut.end();++it)
			it -> Print();
		} ///< Prints information about all cuts.
  
		void StoreCutHisto(string histoName, string rootFileName){
    
			const int nCuts = fCut.size();    
			TH1D cuts(histoName.c_str(), histoName.c_str(), nCuts+1, -0.5, nCuts+0.5); 

			cuts.GetXaxis() -> SetBinLabel(1, "No cut");
			for (int i=0; i < nCuts; ++i) {
				string cutName = fCut.at(i).GetName();
				cuts.GetXaxis()-> SetBinLabel(i+2, cutName.c_str());
			}

    
			double weight = fNEvents;
			cuts.Fill(0., weight);    
			for (unsigned int i=0 ; i< fCut.size(); ++i){
				weight = fCut.at(i).GetNEvents();
				cuts.Fill(static_cast<double>(i+1), weight);      
			}
    
			TFile f(rootFileName.c_str(), "UPDATE");
			cuts.Write();
			f.Close();
		} ///< Stores information about number of selected tracks (events) per cut to one histogram and saves it in the root file.
  
  		
  		void WriteToRoot(string cuts, string file);
  		///< Stores information about number of selected tracks (events) per cut to the root tree and saves it in the root file.
	protected:
		int   fNEvents;
		vector <Cut> fCut; 

		void Read(string branchName);
		///< Reads cut parameters from the xml config file.
		bool fInitHisto;
};
#endif
