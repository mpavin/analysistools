#ifndef STRTOOLS_H
#define STRTOOLS_H

#include <string>

namespace StrTools{

	std::string ConvertToString(int x);
	std::string ConvertToString(double x, int prec);
	void RemoveLeadingTrailingSpace(std::string &str);
	std::string GetTimeString();
}

#endif
