/**
*	\class Bin
*	\details Class Bin represents one bin in phase space, it can be 1D, 2D and 3D.
* 	Also it contains bin borders and bin type. 
*	
*	\author Matej Pavin \n
*	E-mail: mpavin@cern.ch
*	\date 10. 10. 2015.
*/

#ifndef BIN_H
#define BIN_H

#include <iostream>
#include <vector>

using namespace std;

enum BinType{
	eUnknown = 0,
	eMomentum = 1 << 0,
	eTransverseMom = 1 << 1,
	eLongMom = 1 << 2,
	eRapidity = 1 << 3,
	eFeynmanX = 1 << 4,
	eTheta = 1 << 5,
	eZ = 1 << 6,
	eInvariantMass = 1 << 7,
	edEdx = 1 << 8,
	eMassToFSq = 1 << 9,

};
/**<
*	Bin type
*	There are several possible bin types: momentum, transverse momentum, longitudinal momentum, rapidity, 
*	Feynman x, theta angle (atan(pt/pz)), z position, invariant mass, dE/dx, m^2_TOF
*	Depending on the bin dimension, combinations of these are possible:
*	for example for LT analysis: bins are 3D: z, theta, momentum. 
*/


class Bin{

	public:

		Bin(): fType(0) {} ///< Class constructor without parameters.
		Bin(vector<double> &low, vector<double> &high, int type=0): fLow(low), fHigh(high), fType(type) {}

		///< Class constructor with bin parameters.
		
		void SetType(int type){fType = type;} ///< Sets bin type.
		void AddBorders(double low, double high); ///< Adds bin borders.


		int GetType(){return fType;} ///< Returns bin type.

		double GetLowValue(int dim=1); ///< Returns bin low border for the given dimension. 
		double GetHighValue(int dim=1); ///< Returns bin high border for the given dimension. 
		double GetMidValue(int dim=1); ///< Returns bin central value for the given dimension. 
		double GetDelta(int dim=1); ///< Returns bin size for the given dimension.
		int GetDimension(){return fLow.size();}

	private: 

		vector<double> fLow;
		vector<double> fHigh;
		int fType;

};

#endif
