#ifndef TRACKEXTRAPOLATION_H
#define TRACKEXTRAPOLATION_H

#include <iostream>
#include "TrackTools.h"
#include "TargetAna.h"
#include <det/Detector.h>
#include <utl/Point.h>
#include <utl/Vector.h>
#include <utl/CovarianceMatrix.h>
#include <evt/rec/Track.h>
#include <evt/RecEvent.h>
//#include "CConfig.h"
#include <fwk/CentralConfig.h>

using namespace std;
using namespace det;
using namespace utl;
using namespace evt::rec;
using namespace fwk;
using namespace TrackTools;

class TrackExtrapolation{

	public:
		TrackExtrapolation(bool cov, double step, double radLength=0);
		TrackExtrapolation(bool cov, const evt::rec::Track& track, double step, double radLength=0);
		
		//**********************************Get functions********************************
		double GetStep(){return fStep;}
		TrackPar& GetStartTrackParam(){return fStartPar;}
		TrackPar& GetStopTrackParam(){return fStopPar;}
		TargetAna* GetTarget(){return fTarget;}
		static double GetKappa(){return fKappa;}
		bool GetErrorEstimation(){return fErrorEstimation;}
		//**********************************Set functions********************************
		void SetTrackParams(const evt::rec::Track& track);
		//void SetClusters(const evt::rec::Track& track, const evt::RecEvent& recEvent);
		void SetStep(double step){fStep = step;}
		void SetTarget(TargetAna& target){fTarget = &target;}
		void SetErrorEstimation(bool cov){fErrorEstimation = cov;};
		//**********************************Other functions******************************
	
		void Extrapolate(double zStop, double step, double radLength);
		void Extrapolate(double zStop, double radLength);
		void Extrapolate(double zStop);
		double ExtrapolateToTarget(double minStep);
		void DoKalmanStep(TPCCluster &cluster);
		//void DoKalmanFit(vector<TPCCluster> &fClusters, double Out[300][10], double* chi2_1, double* chi2_2 );
	private:

		/*void SetPrediction( TrackPar &Hin, double zc,double xkkp1[5],double Cikkp1[15] );
		void DoKalmanFilter( vector<TPCCluster> &clusters,
		     double xkkp1_f[300][5],double xkp1_f[300][5],
		     double Cikkp1_f[300][15],double Ckp1_f[300][15],
		     TrackPar &Hout,double* chi2_2 );
		void DoChi2Cut(vector<TrackTools::TPCCluster> &clusters, double Out[300][10], double chi2_sm[300],double cut_chi2);*/
		void GetMagField(Point& point, double &Bx, double &By, double &Bz);
		void AddNoise(TrackPar &trackPar, double X);
		
		TargetAna* fTarget;
		
		TrackPar fStartPar;
		TrackPar fStopPar;
		
		bool fErrorEstimation;
		
		double fStep;
		double fLength;
		static Detector &fDetector;       
		static const MagneticField &fMagField;
		
		static double fKappa;
		double fRadLength;
		static CentralConfig& fCentralConfig;	
		//help variables;
		
};

#endif
