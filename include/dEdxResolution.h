#ifndef DEDXRESOLUTION_H
#define DEDXRESOLUTION_H

#include <iostream>
#include <vector>
#include <string>
#include <TF1.h>
#include <TCanvas.h>

using namespace std;


class dEdxResolution{
	public: 
		dEdxResolution(string fileName, vector<double> &ranges);
		
		double GetMean(double bg);
		double GetWidth(double bg);
		double GetWidth(double bg1, double bg2);
		double GetDelta(double bg1, double bg2);
		double GetPionMeanRange(){return fPionPars[0];}
		double GetPionWidthRange(){return fPionPars[1];}
		double GetProtonMeanRange(){return fProtonPars[0];}
		double GetProtonWidthRange(){return fProtonPars[1];}
		double GetKaonMeanRange(){return fKaonPars[0];}
		double GetKaonWidthRange(){return fKaonPars[1];}
		double GetElectronMeanRange(){return fElectronPars[0];}
		double GetElectronWidthRange(){return fElectronPars[1];}
	
		void SetPionMeanRange(double val){fPionPars[0] = val;}
		void SetPionWidthRange(double val){fPionPars[1] = val;}
		void SetProtonMeanRange(double val){fProtonPars[0] = val;}
		void SetProtonWidthRange(double val){fProtonPars[1] = val;}
		void SetPKaonMeanRange(double val){fKaonPars[0] = val;}
		void SetKaonWidthRange(double val){fKaonPars[1] = val;}
		void SetElectronMeanRange(double val){fElectronPars[0] = val;}
		void SetElectronWidthRange(double val){fElectronPars[1] = val;}		
	private:
		//vector<double> fMean;
		//double fWidth;
		//vector<double> fBetaGamma;
		//double fStep;
		TF1 *fdEdxMean;
		TF1 *fdEdxWidth;
		double fPionPars[2];
		double fProtonPars[2];
		double fKaonPars[2];
		double fElectronPars[2];
		
};

#endif
