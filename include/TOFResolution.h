#ifndef TOFRESOLUTION_H
#define TOFRESOLUTION_H

#include <iostream>
#include <vector>
#include <string>
#include <TH2D.h>
#include <TCanvas.h>
#include <TGraphErrors.h>

using namespace std;

class TOFResolution{
	public: 
		TOFResolution(string tofMap, vector<double> &ranges);
		
		double GetPionMean(double p){return fPionPars[0] + fPionPars[1]*p + fPionPars[2]*p*p;}
		double GetPionMeanRange(){return fPionPars[6];}
		double GetProtonMean(double p){return fProtonPars[0]+fProtonPars[1]*p+fProtonPars[2]*p*p;}
		double GetProtonMeanRange(){return fProtonPars[6];}
		double GetElectronMean(double p){return fElectronPars[0]+fElectronPars[1]*p+fElectronPars[2]*p*p;}
		double GetElectronMeanRange(){return fElectronPars[6];}
		double GetKaonMean(double p){return fKaonPars[0]+fKaonPars[1]*p+fKaonPars[2]*p*p;}
		double GetKaonMeanRange(){return fKaonPars[6];}
		double GetPionWidth(double p){return fPionPars[3] + fPionPars[4]*p + fPionPars[5]*p*p;}
		double GetPionWidthRange(){return fPionPars[7];}
		double GetProtonWidth(double p){return fProtonPars[3] + fProtonPars[4]*p + fProtonPars[5]*p*p;}
		double GetProtonWidthRange(){return fProtonPars[7];}
		double GetElectronWidth(double p){return TMath::Abs(fElectronPars[3] + fElectronPars[4]*p + fElectronPars[5]*p*p);}
		double GetElectronWidthRange(){return fElectronPars[7];}
		double GetKaonWidth(double p){return fKaonPars[3] + fKaonPars[4]*p + fKaonPars[5]*p*p;}
		double GetKaonWidthRange(){return fKaonPars[7];}
		string GetTOFMapFile(){return fTOFMap;}
		
		void SetTOFMapFile(string file){fTOFMap = file;}
		void SetPionMean(double p0, double p1, double p2){fPionPars[0] = p0; fPionPars[1] = p1; fPionPars[2] = p2;}
		void SetPionWidth(double p0, double p1, double p2){fPionPars[3] = p0; fPionPars[4] = p1; fPionPars[5] = p2;}
		void SetPionMeanRange(double val){fPionPars[6] = val;}
		void SetPionWidthRange(double val){fPionPars[7] = val;}
		void SetProtonMean(double p0, double p1, double p2){fProtonPars[0] = p0; fProtonPars[1] = p1; fProtonPars[2] = p2;}
		void SetProtonWidth(double p0, double p1, double p2){fProtonPars[3] = p0; fProtonPars[4] = p1; fProtonPars[5] = p2;}
		void SetProtonMeanRange(double val){fProtonPars[6] = val;}
		void SetProtonWidthRange(double val){fProtonPars[7] = val;}
		void SetKaonMean(double p0, double p1, double p2){fKaonPars[0] = p0; fKaonPars[1] = p1; fKaonPars[2] = p2;}
		void SetKaonWidth(double p0, double p1, double p2){fKaonPars[3] = p0; fKaonPars[4] = p1; fKaonPars[5] = p2;}
		void SetKaonMeanRange(double val){fKaonPars[6] = val;}
		void SetKaonWidthRange(double val){fKaonPars[7] = val;}
		void SetElectronMean(double p0, double p1, double p2){fElectronPars[0] = p0; fElectronPars[1]=p1; fElectronPars[2]=p2;}
		void SetElectronWidth(double p0, double p1, double p2){fElectronPars[3] = p0; fElectronPars[4] = p1; fElectronPars[5] = p2;}
		void SetElectronMeanRange(double val){fElectronPars[6] = val;}
		void SetElectronWidthRange(double val){fElectronPars[7] = val;}
		//void SetMaxMomentum(double pmax){fMaxMomentum = pmax;}
		
	private:
		string fTOFMap;
		double fPionPars[8];
		double fProtonPars[8];
		double fElectronPars[8];
		double fKaonPars[8];
};

#endif
