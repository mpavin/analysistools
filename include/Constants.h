#ifndef CONSTANTS_H
#define CONSTANTS_H

/**
*	\file
*	\details Contains constants used in analysis.
*	Some of the constants are not defined here, instead
*	they are defined in SHINE.
*
*	\author M. Pavin\n
*	Email: mpavin@cern.ch
*	\n 10. 10. 2015.
*/

enum ETopology{
	
	eTopVTPC1 = 1 << 0,
	eTopVTPC2 = 1 << 1,
	eTopGTPC = 1 << 2,
	eTopMTPC = 1 << 3,
};
/**<
*	ETopology contains basic track topologies.
*	In this case track topology is position of the track in the TPCs
*	 
*	eTopVTPC1 = 1 << 0 = 1
*	eTopVTPC2 = 1 << 1 = 2
*	eTopGTPC = 1 << 2 = 4
*	eTopMTPC = 1 << 3 = 8
*
*	Topologies are defined like int flags, any combination of them is allowed.
*	For example: track located in VTPC1 + VTPC2 + MTPC will have topology = 1 + 2 + 8 = 11
*/

enum ETopInMagFld{
	eRST,
	eWST
};
/**<
*	There are 2 track topologies when looking tracks in magnetic field:
*	eRST (right side tracks) - tracks which have x component of momentum in the direction of the bending in MF
*	eWST (wrong side tracks) - tracks which have x component of momentum in the opposite direction of the bending in MF
*/

enum EBPDCoordinate{
	eBPD1x,
	eBPD1y,
	eBPD2x,
	eBPD2y,
	eBPD3x,
	eBPD3y
};
/**<
*	BPD coordinates
*	Will be removed, it can be used from SHINE
*/

enum EBPDPos{
	eBPD1Pos,
	eBPD2Pos,
	eBPD3Pos

}; ///< Old relic from the first version of the tools. It will be removed. 

enum ETargetType{
	eLong,
	eThin,
	eNominalBeamline
};
/**<
*	Target types:
*	eLong - T2K replica target (90 cm long, 2.6 cm in diameter)
*	eThin - thin carbon target, 1.9% of interaction length
*	eNominalBeamline - empty target, infinite line passing through x = 0, y = 0. Useful for target alignment procedure.
*/

enum ETrigger{
	eTr1 = 1 << 0,
	eTr2 = 1 << 1,
	eTr3 = 1 << 2,
	eTr4 = 1 << 3
};
/**<
*	Trigger types
* 	Usually there are 4 triggers. 
*	Please look at other sources for the trigger selection (Beam and BeamPlot xml config files).
*	Any combination of triggers is possible (some of the combinations are redundant), but number of events may be different
*	due to downscaling factors. 
*/
#endif
