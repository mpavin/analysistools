/**
*	\file
* 	\details This file contains some functions which are needed for various tasks.
*
*	\author M. Pavin \n
*	Email: mpavin@cern.ch
*	\date 10. 10. 2015.
*/
#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <cstdlib>
#include <stdexcept>

#include <string>
#include <vector>
#include <TString.h>
#include <TPRegexp.h>
#include <time.h>
#include <TH1D.h>
#include <TH2D.h>
#include <evt/rec/RecEventConst.h>
#include <evt/rec/Track.h>
#include "Constants.h"

using namespace std;
using namespace evt;
using namespace evt::rec;


double DoStep(double x, double y, double r1, double r2, TH1 *hist); 
///< This functions counts number of entries inside a circle of radius r, centered around (x,y) in a histrogram.
bool FindMaximum(double *val, double r1, double r2, double step, TH1 *hist);
///< Finds point around which number of entries inside a circle of radius r is maximum. 

bool ChangedSign(double *x0, double *x1);
///< Checks if 2 numbers have opposite signs. 

#endif


