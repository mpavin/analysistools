//*****************************************************************************
// This is special style comment for Doxygen documentation
/**
*	\class Cut
*	\details Defines a cut on some track parameter.
*	Contains information about cut: allowed range for the parameter
*	and if tracks which are selected need to have opposite value. 
*	Also number of selected tracks or events is stored.
*
*	\author by M. Pavin\n
*	Email: mpavin@cern.ch \n
*	IMPORTANT: This class is based on recycled code written by T. Susa
*	\date 10. 10. 2015.
*/
//*****************************************************************************
#ifndef CUT_H
#define CUT_H

#include <string>
#include <vector>
#include <iostream>
#include <TreeDataTypes.h>
using namespace std;



class Cut {

	public:
		Cut () : fName(""), fIsAntiCut(false), fNEvents (0) {}
		///< Class constructor without parameters.
  
  		Cut (string name, vector<double> param, bool isAntiCut, int nEvents = 0) : 
  			fName(name), 
    		fParam (param),
    		fIsAntiCut(isAntiCut),
    		fNEvents(nEvents)
    		{}
		///< Class constructor with cut parameters

		// GET functions
  		string GetName(){return fName;} ///< Returns string containing cut name.
  		string GetName() const {return fName;} ///< Return constant string containing cut name.
  
  
  		vector<double> &GetParam(){return fParam;} ///< Returns vector containing cut parameters.
  		const vector<double> &GetParam() const {return fParam;} /// Returns constant vector containing cut parameters.
  
  		bool IsAntiCut(){return fIsAntiCut;} ///< Checks if cut is anti cut.
  		/**<
  		*	Definition of anticut:
  		*	For example, let's take cut on momentum. 
  		*	If cut parameters are 1 GeV/c and 10 GeV/c, then only tracks
		*	with momentum 1 < p < 10 GeV/c will be selected. 
		*	If fIsAntiCut is true, then only tracks with momentum p <= 1 GeV/c
		*	and p >= 10 will be selected.
		*	Anticut feature can sometimes be very useful. 
  		*/
  		bool IsAntiCut() const {return fIsAntiCut;} ///< Checks if cut is anti cut. Returns constant value.
  
  		int GetNEvents(){return fNEvents;} ///< Returns int number of selected events (tracks)
  		int GetNEvents() const {return fNEvents;} ///< Returns const int number of selected events (tracks)
  		
  		vector<CutVal>& GetValues(){return fVariableValue;} ///< Returns vector containing selected values of the event (track) parameters.
		const vector<CutVal>& GetValues() const {return fVariableValue;} ///< Returns const vector containing selected values of the event (track) parameters.
		
		// SET functions
		void SetName(string name){fName = name;} ///< Sets cut name.

		void AddParam(double param){fParam.push_back(param);} ///< Adds parameter value. 
		void SetParam(vector<double> param){fParam = param;} ///< Adds vector of parameter values.
  
		void SetAntiCut(bool isAntiCut){fIsAntiCut = isAntiCut;} ///< Sets if cut is anticut.

  
		void IncrementNEvents(){++fNEvents;} ///< Increases number of selected events (tracks).
		
		void AddValue(CutVal value){fVariableValue.push_back(value);} ///< Adds selected value.
		void Reset(){
			fName = "";
			fParam.clear();
    			fIsAntiCut = false;
    			fNEvents = 0;
  		} ///< Reset cut's name, parameters and number of selected track (events).

  		void Print(){
    			cout << "[Cut] " << "Nev = " << fNEvents << " "  << ", name = " << fName; 
    			for (unsigned int i=0; i < fParam.size(); ++i)
      				cout << ", p" << i << " = " << fParam[i];
    
    				cout << ", anti = " << fIsAntiCut << endl;
  		} ///< Prints information about cut. 

 	private:
  		string fName;
		string fVariableName;

		vector<CutVal> fVariableValue;
  		vector <double>  fParam;
  		bool fIsAntiCut;
		
  		int  fNEvents;

};

#endif
