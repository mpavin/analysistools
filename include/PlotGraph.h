/**
*	\class PlotGraph
*	\details Wraps ROOT class TGraphErrors
*	Reads various values (label or title size, titles, ...) 
*	Fits function to the data and can plot it on the canvas. 
*
*	\author M. Pavin\n
*	E-mail: mpavin@cern.ch
*	\date 10. 10. 2015.
*/
#ifndef PLOTGRAPH_H
#define PLOTGRAPH_H

#include <TGraphErrors.h>
#include <TCanvas.h>
#include <TF1.h>

using namespace std;

class PlotGraph{

	public:

		PlotGraph(int nPoints, double *x, double *y, double *xE, double *yE, string config, string graphName, string nameAdd);
		///< Class constructor, creates TGraphErrors with array of data and reads options from xml file.
		TGraphErrors* GetGraph(){return fGraph;} ///< Returns TGraphErrors.
		TCanvas* GetCanvas(); ///< Returns Canvas
		void AddFunction(TF1 *f, string option); ///< Adds function to be fitted.
		void FitFunction(TF1 *f, double min, double max); ///< Fits function to the data.
	private:

		bool fPlot;
		TGraphErrors *fGraph;
		TCanvas *fCanvas;
		string fPlotOptions;
		bool fDraw;
		vector<double> fText;
};

#endif
