//*******************************************************************************
/**
*	\cClass TrackSelection
*	\details Similar to the class EventSelection.
*	Contains list of the functions which apply cuts on the tracks.
*	Inherits class Selection.
*
*	\author M. Pavin\n
*	E-mail: mpavin@cern.ch
*	\date 10. 10. 2015.
*/
//*******************************************************************************
#ifndef TRACKSELECTION_H
#define TRACKSELECTION_H

#include "Cut.h"
#include "Selection.h"
#include <evt/Event.h>
#include <evt/RecEvent.h>
#include "Constants.h"
#include "AnaTrack.h"

using namespace std;
using namespace utl;
using namespace evt;
using namespace evt::rec;      
using namespace TrackConst;


class TrackSelection : public Selection  {
 public:
	TrackSelection(): Selection("TrackCuts") {} ///< Class constructor
  

  	bool IsTrackGood(AnaTrack &tr); ///< Checks if track passes all cuts.


 private:


  	typedef bool(*evtFuncPtr)(AnaTrack &, Cut &c);
  	static  map<string, evtFuncPtr> fEvtFunc; ///< Assigns cut names to the cut functions defined below.
  
	static bool IsStatusGood(AnaTrack &tr, Cut&c);
	///< Checks status of the tracks. Status 0 means that track is perfectly fitted and momentum is determined
	static bool Momentum(AnaTrack &tr, Cut&c);
	///< Checks if track has momentum in desired range.
	static bool SigmaR(AnaTrack &tr, Cut&c);
	///< Checks uncertainty in the track position.
	static bool SigmaP(AnaTrack &tr, Cut&c);
	///< Checks momentum resolution of a tracks (Sigma_p /p).
	static bool NumVTPC(AnaTrack &tr, Cut&c);
	///< Checks number of clusters in the VTPCs.
	static bool NumGTPC(AnaTrack &tr, Cut&c);
	///< Checks number of clusters in GTPC.
	static bool Charge(AnaTrack &tr, Cut&c);
	///< Checks charge of the track.
	static bool HasToF(AnaTrack &tr, Cut&c); 
	///< Checks if the track has tof hit in the TOF-F.
	static bool GoodTopology(AnaTrack &tr, Cut&c); 
	///< Checks if the track has desired topology.
	static bool GoodNumClustersTopology(AnaTrack &tr, Cut&c); 

	///< Checks number of clusters depending on the track topology.
	static bool GoodTopologyTFace(AnaTrack &tr, Cut&c); 
	///< Allows only certain topologies to be extrapolated to the target downstream face. This was used in 2007 LT analysis and it will probably be removed in next version of the tools.
	static bool TopologyInMagF(AnaTrack &tr, Cut&c); 
	///< Check topology in magnetic field. RST or WST may be selected. 
	//static bool TrackDistance(AnaTrack &tr, Cut&c); 
	static bool PyTopology(AnaTrack &tr, Cut&c); 
	
	static bool HasMTPC (AnaTrack &tr, Cut&c); 
	///< Checks if track has clusters in the MTPCs
	static bool Theta (AnaTrack &tr, Cut&c);
	///< Checks if track's polar angle is in desired range.
  	static bool DistanceOfApproach(AnaTrack &tr, Cut&c);
	///< Checks extrapolated distance from the target. 
	static bool DifferentialPhi(AnaTrack &tr, Cut&c);
	///< Checks first point on track
	static bool FirstZ(AnaTrack &tr, Cut&c);
	///< Checks last point on track
	static bool LastZ(AnaTrack &tr, Cut&c);
	
	static bool ExitMFMap(AnaTrack &tr, Cut&c);
};

#endif
