/**
*	\class PhaseSpace 
*	\details Contains vector of Bins which cover phase space in the analysis.
*	Every bin can be accessed with dimension names, titles and units.
*
*	\author M. Pavin\n
*	E-mail: mpavin@cern.ch
*	\date 10. 10. 2015.
*/
#ifndef PHASESPACE_H
#define PHASESPACE_H

#include <vector>
#include "Bin.h"
#include <TH2D.h>

using namespace std;

class PhaseSpace{

	public:

		PhaseSpace(): fNDimensions(0), fVarDimensions(1) {Read();} ///<Class constructor, calls function Read().

		void SetType(int dimension, int type); ///< Sets dimension's variable type.
		void SetName(int dimension, string name); ///< Sets dimension's name.
		void SetTitle(int dimension,string title); ///< Sets dimension's title.
		void SetNumberOfDimensions(int nDim); ///< Sets number of dimensions.

		int GetNumberOfDimensions(){return fNDimensions;} ///< Returns number of dimensions.
		string GetDimTitle(int dim); ///< Returns dimension title. 
		string GetDimUnit(int dim); ///< Returns dimension unit.
		int GetDimType(int dim); ///< Returns dimension type.
		
		int GetNumberOfBins(){return fBin.size();} ///< Returns number of bins.
		int GetDimBins(int dim, int n1=0, int n2=0);
		Bin& GetBin(int binNum); ///< Returns reference to the bin. It's useful for loops over bins. 
		Bin& GetBin(int binNum1, int binNum2, int binNum3);
		int GetBinIndex(int binNum1, int binNum2=0, int binNum3=0);
		double GetVarBinSize(int var, int bin);
		void Read(); ///< Reads xml file containing list of all bins. 

	private:

		//void Read1D(int dim, int binX = 0, int binY = 0);
		void CountDimBins();
		int fNDimensions;
		int fVarDimensions;
		vector<int> fBinType;
		vector<string> fBinName;
		vector<string> fBinUnit;
		vector<int> fVarType;
		vector<string> fVarName;
		vector<string> fVarUnit;

		vector<Bin> fBin;
		vector<double> fVarBinSize1;
		vector<double> fVarBinSize2;
		TH2D fSpectrumAll;
		
		int fDimBins1D;
		vector<int> fDimBins2D;
		vector<vector<int> > fDimBins3D;

};

#endif
