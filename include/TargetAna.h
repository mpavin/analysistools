//*******************************************************************************
/**
*	\class TargetAna
*	\details Contains information about target position and tilt.
*
*	\author M. Pavin\n
*	E-mail: mpavin@cern.ch
*	\date 10. 10. 2015.
*/
//*******************************************************************************
#ifndef TARGETANA_H
#define TARGETANA_H

#include <iostream>
#include <string>
#include "Constants.h"
#include <utl/Point.h>

using namespace std;
using namespace utl;

class TargetAna{

	public:
	
		TargetAna(string name, ETargetType type, double *pos, double *dim, double *tilt){Load(name, type, pos, dim, tilt);}
		///< Class constructor
		TargetAna(){} ///< Class constructor
		void Load(string name, ETargetType type, double *pos, double *dim, double *tilt); ///< Loads target data.
		void PrintTargetData(); ///< Prints target data;

		//*************************Get functions***********************************
        string GetName(){return fName;} ///< Returns target name.
		ETargetType GetType(){return fType;} ///< Returns target type (look ETargetType in Constants.h).
		const Point& GetUpstreamPosition(){return fPosition;}
		double GetLength(){return fDimensions[0];} ///< Returns length of the target.
		double GetRadius(){return fDimensions[1];} ///< Returns diameter of the target.
		double GetDistanceFromTargetCenter(double x, double y, double z);
		///< Calculates distance from the target center.
		double GetDistanceFromTargetSurface(double x, double y, double z);
		///< Calculates distance from the target surface.
		
		//*************************Set functions***********************************
		void SetName(string nm){fName = nm;} ///< Sets target name.
		void SetType(ETargetType type){fType = type;} ///< Sets target type (look ETargetType in Constants.h).
		void SetUpstreamPosition(double &x, double &y, double &z){fPosition = Point(x, y, z);} ///< Sets target upstream face position.
		void SetLength(double length){fDimensions[0] = length;} ///< Sets length of the target.
		void SetRadius(double radius){fDimensions[1] = radius;} ///< Sets radius of the target.
		void SetTiltXZ(double tilt){fTilt[0] = tilt;} ///< Sets xz tilt of the target (angle).
		void SetTiltYZ(double tilt){fTilt[1] = tilt;} ///< Sets xz tilt of the target (angle).

	private:

		//void RotatePoint2(double* x,double* y, double* z,double xc,double yc,double zc, double tXZ,double tYZ); ///< Transformes point into target coordinate system.

		ETargetType fType;
		string fName;
		Point fPosition;
		double fDimensions[2];
		double fTilt[2];	

};

#endif
