/**
*	\class EventSelection
*	\details Inherits Selection class.
*	Contains event cuts, and applies selecetion on events.
*
*	Some cuts will be modified.
*
*	\author M. Pavin\n
*	E-mail: mpavin@cern.ch
*	\date: 10. 10. 2015.
*/
#ifndef EVENTSELECTION_H
#define EVENSELECTION_H

#include "Cut.h"
#include "Selection.h"
#include <evt/Event.h>
#include <evt/RecEvent.h>


using namespace std;
using namespace utl;
using namespace evt;
using namespace evt::rec;      



class EventSelection : public Selection  {
 public:
 	EventSelection(): Selection("EventCuts") {} ///< Class constructor
  

	bool IsEventGood(const Event & event); ///< Checks if event passes selection.
	
  
 private:


  	typedef bool(*evtFuncPtr)(const Event &, Cut &c);
  	static  map<string, evtFuncPtr> fEvtFunc; ///< Assigns cut names to the cut functions defined below.
  
 	static bool T2(const Event &event, Cut&c); 
 	///< Checks if event is T2 event (presence of T2 trigger - interaction in the target).
 	static bool T3(const Event &event, Cut&c); 
 	///< Checks if event is T3 event (presence of T3 trigger - interaction in the target + beam more similar to the T2K beam).
 	static bool HasToF(const Event &event, Cut&c); ///< Checks if the event has TOF hits.
 	static bool WFA(const Event &event, Cut&c);  
 	///< Check Wave form analyzer. Checks for the beam particles in defined interval around trigger. This removes off-time tracks in the TPCs
  	static bool BPD(const Event &event, Cut&c);  ///< Checks if all 3 BPDs are used for beam track fit.
  	static bool BPD3(const Event &event, Cut&c); ///< Checks if clusters in BPD3 are good.
  	static bool Chi2(const Event &event,  Cut&c); ///< Checks chi2/Ndf for beam track fit. There is a problem with Chi2 distribution in the beam fit
	static bool R(const Event &event, Cut&c); ///< Selects beam particles inside a certain radius. 
	static bool Multiplicity(const Event &event, Cut&c); ///< Checks number of tracks in the event.
};

#endif
