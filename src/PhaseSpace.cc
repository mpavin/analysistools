#include <stdlib.h>
#include "PhaseSpace.h"
#include <sstream>

#include <fwk/CentralConfig.h>
#include <utl/Branch.h>
#include <utl/BranchIterator.h>
#include "CConfig.h"
#include <math.h>
using namespace fwk;
using namespace utl; 



//*********************************************************************************************
void PhaseSpace::SetNumberOfDimensions(int nDim){
	if(nDim < 0 || nDim > 3){
		cout << __FUNCTION__ << ": Wrong number of dimensions! Only 1, 2 or 3 accepted!" << endl;
		exit(EXIT_FAILURE);
	}
	fNDimensions = nDim;

}




//*********************************************************************************************
void PhaseSpace::Read(){

	string cName = "PhaseSpace";
	Branch phaseSpace = cConfig.GetTopBranch(cName);

	if(phaseSpace.GetName() != "PhaseSpace"){
		cout << __FUNCTION__ << ": Wrong DataAna config file! Top branch name doesn't match! Please check your configuration files! Exiting..." << endl;
		exit (EXIT_FAILURE);
	}

	Branch dimDef = phaseSpace.GetFirstChild();

	if(dimDef.GetName() != "header" ){
		cout << __FUNCTION__ << ": First child of the top branch is not header branch! Header branch provides dimension of the phase space and variable names. Exiting..." << endl;
		exit (EXIT_FAILURE);		
	}
	
	
	for(BranchIterator it = dimDef.ChildrenBegin(); it != dimDef.ChildrenEnd(); ++it){
		Branch child = *it;
		
		
		if(child.GetName() == "dim"){
			++fNDimensions;
			for(BranchIterator dimIt = child.ChildrenBegin(); dimIt != child.ChildrenEnd(); ++dimIt){
				Branch childDim = *dimIt;	
				if(childDim.GetName() == "type"){
					int data = 0;
					childDim.GetData(data);
					fBinType.push_back(data);
				}
				else if(childDim.GetName() == "name"){
					string data = "";
					childDim.GetData(data);
					fBinName.push_back(data);
				}
				else if(childDim.GetName() == "unit"){
					string data = "";
					childDim.GetData(data);
					fBinUnit.push_back(data);
				}
				
			}
			if(fBinName.size() < fNDimensions) 	fBinName.push_back("unknown");
			if(fBinUnit.size() < fNDimensions) 	fBinUnit.push_back("unknown");	
			if(fBinType.size() < fNDimensions) 	fBinType.push_back(0);	

		}
		else if(child.GetName() == "var"){
			++fVarDimensions;
			for(BranchIterator dimIt = dimDef.ChildrenBegin(); dimIt != dimDef.ChildrenEnd(); ++dimIt){
				Branch childDim = *dimIt;	
				if(childDim.GetName() == "type"){
					int data = 0;
					childDim.GetData(data);
					fVarType.push_back(data);
				}
				else if(childDim.GetName() == "name"){
					string data = "";
					childDim.GetData(data);
					fVarName.push_back(data);
				}
				else if(childDim.GetName() == "unit"){
					string data = "";
					childDim.GetData(data);
					fVarUnit.push_back(data);
				}
							
			
			}	

			if(fVarName.size() < fVarDimensions) 	fVarName.push_back("unknown");
			if(fVarUnit.size() < fVarDimensions) 	fVarUnit.push_back("unknown");	
			if(fVarType.size() < fVarDimensions) 	fVarType.push_back(0);			
		}
		else{
			cout << __FUNCTION__ << ": Unknown branch: " << child.GetName() << "! Skipping..." << endl;
		}
	}
	
	int type = 0;


	for(int i = 0; i < fNDimensions; ++i) type = type | fBinType.at(i);

	for(BranchIterator it = ++phaseSpace.ChildrenBegin(); it != phaseSpace.ChildrenEnd(); ++it){	
		Branch child = *it;
		if(child.GetName() == "bin"){
		
			switch(fNDimensions){
				case 1:
				{
					vector<double> low;
					vector<double> high;
					for(BranchIterator bIt = child.ChildrenBegin(); bIt !=  child.ChildrenEnd(); ++bIt){
						Branch binChild = *bIt;
						if(binChild.GetName() == "low1"){
							binChild.GetData(low);
						}
						else if(binChild.GetName() == "high1"){
							binChild.GetData(high);
						}
						else if(binChild.GetName() == "varBinSize1"){
							vector<double> binSize;
							binChild.GetData(binSize);
							for(int i = 0; i < binSize.size(); i++){
								fVarBinSize1.push_back(binSize.at(i));
							}
						}
						else if(binChild.GetName() == "varBinSize2"){
							vector<double> binSize;
							binChild.GetData(binSize);
							for(int i = 0; i < binSize.size(); i++){
								fVarBinSize2.push_back(binSize.at(i));
							}
						}
					}
					if(low.size() != high.size()){
						cout << __FUNCTION__ << ": Low and high borders don't have equal dimension. Check phase space config file. Exiting..." << endl;
						exit (EXIT_FAILURE);				
					}
			
					for(int i = 0; i < low.size(); ++i){
					
						vector<double> lowT;
						vector<double> highT;
						
						lowT.push_back(low.at(i));
						highT.push_back(high.at(i));
						Bin bin(lowT, highT, type);
						fBin.push_back(bin);						
					}

					break;
				}
				case 2:
				{
					double low1;
					double high1;
					vector<double> low2;
					vector<double> high2;
					for(BranchIterator bIt = child.ChildrenBegin(); bIt !=  child.ChildrenEnd(); ++bIt){
						Branch binChild = *bIt;
						if(binChild.GetName() == "low1"){
							binChild.GetData(low1);
						}
						else if(binChild.GetName() == "high1"){
							binChild.GetData(high1);
						}
						else if(binChild.GetName() == "low2"){
							binChild.GetData(low2);
						}
						else if(binChild.GetName() == "high2"){
							binChild.GetData(high2);
						}
						else if(binChild.GetName() == "varBinSize1"){
							vector<double> binSize;
							binChild.GetData(binSize);
							for(int i = 0; i < binSize.size(); i++){
								fVarBinSize1.push_back(binSize.at(i));
							}
						}
						else if(binChild.GetName() == "varBinSize2"){
							vector<double> binSize;
							binChild.GetData(binSize);
							for(int i = 0; i < binSize.size(); i++){
								fVarBinSize2.push_back(binSize.at(i));
							}
						}						
					}
					for(int i = 0; i < low2.size(); ++i){
						vector<double> low;
						vector<double> high;	
						
						low.push_back(low1);
						high.push_back(high1);	
						low.push_back(low2.at(i));
						high.push_back(high2.at(i));	
						Bin bin(low, high, type);
						fBin.push_back(bin);										
					}	
					break;
				}
				case 3:
				{
					double low1;
					double high1;
					double low2;
					double high2;	
					for(BranchIterator bIt = child.ChildrenBegin(); bIt !=  child.ChildrenEnd(); ++bIt){
						Branch binChild = *bIt;
						if(binChild.GetName() == "low1"){
							binChild.GetData(low1);
						}
						else if(binChild.GetName() == "high1"){
							binChild.GetData(high1);
						}
						else if(binChild.GetName() == "subbin"){
							vector<double> low3;
							vector<double> high3;								
							for(BranchIterator sbIt = binChild.ChildrenBegin(); sbIt !=  binChild.ChildrenEnd(); ++sbIt){
								Branch subBinChild = *sbIt;
								if(subBinChild.GetName() == "low2"){
									subBinChild.GetData(low2);
								}
								else if(subBinChild.GetName() == "high2"){
									subBinChild.GetData(high2);
								}
								else if(subBinChild.GetName() == "low3"){
									subBinChild.GetData(low3);
								}
								else if(subBinChild.GetName() == "high3"){
									subBinChild.GetData(high3);
								}
								else if(subBinChild.GetName() == "varBinSize1"){
									vector<double> binSize;
									subBinChild.GetData(binSize);
									for(int i = 0; i < binSize.size(); i++){
										fVarBinSize1.push_back(binSize.at(i));
									}
								}
								else if(subBinChild.GetName() == "varBinSize2"){
									vector<double> binSize;
									subBinChild.GetData(binSize);
									for(int i = 0; i < binSize.size(); i++){
										fVarBinSize2.push_back(binSize.at(i));
									}
								}
							}
							for(int i = 0; i < low3.size(); ++i){
								vector<double> low;
								vector<double> high;	
								//cout << low3.size() << " " << high3.size() << endl;
								low.push_back(low1);
								high.push_back(high1);							
								low.push_back(low2);
								high.push_back(high2);	
								
								low.push_back(low3.at(i));
								high.push_back(high3.at(i));	
								Bin bin(low, high, type);
								fBin.push_back(bin);										
							}	
						}
						
					}	
					break;
				}							
			}
		}
	
	}
	CountDimBins();

}

//*********************************************************************************************

Bin& PhaseSpace::GetBin(int binNum){
	if(binNum > fBin.size()){
		cout << __FUNCTION__ << ": Provided bin number is larger than maximum bin number. Exiting..." << endl;
		exit (EXIT_FAILURE);			
	}
	
	return fBin.at(binNum - 1);
}

string PhaseSpace::GetDimTitle(int dim){
	if(dim > fBinName.size() || dim < 1){
		cout << __FUNCTION__ << ": Provided bin number is larger than maximum bin number. Exiting..." << endl;
		exit (EXIT_FAILURE);			
	}
	
	
	return fBinName.at(dim-1);
}

string PhaseSpace::GetDimUnit(int dim){
	if(dim > fBinUnit.size() || dim < 1){
		cout << __FUNCTION__ << ": Provided bin number is larger than maximum bin number. Exiting..." << endl;
		exit (EXIT_FAILURE);			
	}
	
	
	return fBinUnit.at(dim-1);
}

int PhaseSpace::GetDimType(int dim){
	if(dim > fBinType.size() || dim < 1){
		cout << __FUNCTION__ << ": Provided bin number is larger than maximum bin number. Exiting..." << endl;
		exit (EXIT_FAILURE);			
	}
	
	
	return fBinType.at(dim-1);
}

void PhaseSpace::CountDimBins(){

	/*for(int i = 0; i < fBin.size(); i++){
		cout << fBin.at(i).GetLowValue(1) << "-" <<fBin.at(i).GetHighValue(1) << ", " << fBin.at(i).GetMidValue() << ", " << fBin.at(i).GetLowValue(2) << "-" <<fBin.at(i).GetHighValue(2) << ", " << fBin.at(i).GetLowValue(3) << "-"<< fBin.at(i).GetHighValue(3) << endl;
	}*/
	switch(fNDimensions){
		case 1:
		{
			fDimBins1D = fBin.size();
			break;
		}
		case 2:
		{
			int n1D = 1;
			int n2D = 0;
			double value = fBin.at(0).GetMidValue();
			for(int i = 0; i < fBin.size(); i++){
				if((fBin.at(i).GetMidValue() - value) < 0.0001){
					n2D++;
				}
				else{
					value = fBin.at(i).GetMidValue();
					n1D++;
					fDimBins2D.push_back(n2D);
					n2D = 1;
				}
			}
			fDimBins1D = n1D;
			fDimBins2D.push_back(n2D);
			break;
		}
		case 3:
		{
			int n1D = 1;
			int n2D = 1;
			int n3D = 0;

			double value1D = fBin.at(0).GetMidValue();
			double value2D = fBin.at(0).GetMidValue(2);
			vector<int> dimBins3D;
			for(int i = 0; i < fBin.size(); i++){
				//cout   <<fBin.at(i).GetMidValue(1) << " " << fBin.at(i).GetMidValue(2) << " " << fBin.at(i).GetMidValue(3) << endl;
				
				if(fabs(fBin.at(i).GetMidValue() - value1D) < 0.0001){
					if(fabs(fBin.at(i).GetMidValue(2) - value2D) < 0.0001){
						n3D++;
					}
					else{
						value2D = fBin.at(i).GetMidValue(2);
						n2D++;
						dimBins3D.push_back(n3D);
						n3D = 1;
					}
				}
				else{

					value1D = fBin.at(i).GetMidValue();
					value2D = fBin.at(i).GetMidValue(2);

					n1D++;
					fDimBins2D.push_back(n2D);
					n2D = 1;
					dimBins3D.push_back(n3D);
					fDimBins3D.push_back(dimBins3D);
					n3D = 1;
					dimBins3D.clear();
				}				
			}
			fDimBins1D = n1D;
			fDimBins2D.push_back(n2D);
			dimBins3D.push_back(n3D);
			fDimBins3D.push_back(dimBins3D);
			break;
		}
	
	}
}

int PhaseSpace::GetBinIndex(int binNum1, int binNum2, int binNum3){

	switch(fNDimensions)
	{
		case 1:
		{
			if(binNum1 < 1 || binNum1 > fDimBins1D){
				cerr << "[ERROR] " << __FUNCTION__ << ": bad 1. dim bin id! Exiting..." << endl;
				exit(EXIT_FAILURE);
			}
			
			return binNum1;			
			break;
		}

		case 2:
		{

			if(binNum1 < 1 || binNum1 > fDimBins1D){
				cerr << "[ERROR] " << __FUNCTION__ << ": bad 1. dim bin id! Exiting..." << endl;
				exit(EXIT_FAILURE);
			}
			
			if(binNum2 < 1 || binNum2 > fDimBins2D.at(binNum1-1)){
				cerr << "[ERROR] " << __FUNCTION__ << ": bad 2. dim bin id! Exiting..." << endl;
				exit(EXIT_FAILURE);
			}
			int index = 0;
			for(int i = 0; i < binNum1-1; i++){
				index += fDimBins2D.at(i);
			}
			index += binNum2;

			return index;
			break;
		}
		
		case 3:
		{
			if(binNum1 < 1 || binNum1 > fDimBins1D){
				cerr << "[ERROR] " << __FUNCTION__ << ": bad 1. dim bin id! Exiting..." << endl;
				exit(EXIT_FAILURE);
			}
			
			if(binNum2 < 1 || binNum2 > fDimBins2D.at(binNum1-1)){
				cerr << "[ERROR] " << __FUNCTION__ << ": bad 2. dim bin id! Exiting..." << endl;
				exit(EXIT_FAILURE);
			}
			if(binNum3 < 1 || binNum3 > fDimBins3D.at(binNum1-1).at(binNum2-1)){
				cerr << "[ERROR] " << __FUNCTION__ << ": bad 2. dim bin id! Exiting..." << endl;
				exit(EXIT_FAILURE);
			}		
			
			int index = 0;
			
			for(int i = 0; i < binNum1-1; i++){
				for(int j = 0; j < fDimBins2D.at(i); j++){
					index += fDimBins3D.at(i).at(j);
				}
			}
			
			for(int j = 0; j < binNum2-1; j++){
				index += fDimBins3D.at(binNum1-1).at(j);
			}
			index += binNum3;
			//cout << index << endl;
			return index;			
			break;
		}	
	}

}

Bin& PhaseSpace::GetBin(int binNum1, int binNum2, int binNum3){

	switch(fNDimensions)
	{
		case 1:
		{
			if(binNum1 < 1 || binNum1 > fDimBins1D){
				cerr << "[ERROR] " << __FUNCTION__ << ": bad 1. dim bin id! Exiting..." << endl;
				exit(EXIT_FAILURE);
			}
			
			return fBin.at(binNum1 - 1);			
			break;
		}

		case 2:
		{

			if(binNum1 < 1 || binNum1 > fDimBins1D){
				cerr << "[ERROR] " << __FUNCTION__ << ": bad 1. dim bin id! Exiting..." << endl;
				exit(EXIT_FAILURE);
			}
			
			if(binNum2 < 1 || binNum2 > fDimBins2D.at(binNum1-1)){
				cerr << "[ERROR] " << __FUNCTION__ << ": bad 2. dim bin id! Exiting..." << endl;
				exit(EXIT_FAILURE);
			}
			int index = 0;
			for(int i = 0; i < binNum1-1; i++){
				index += fDimBins2D.at(i);
			}
			index += binNum2;

			return fBin.at(index-1);
			break;
		}
		
		case 3:
		{
			if(binNum1 < 1 || binNum1 > fDimBins1D){
				cerr << "[ERROR] " << __FUNCTION__ << ": bad 1. dim bin id! Exiting..." << endl;
				exit(EXIT_FAILURE);
			}
			
			if(binNum2 < 1 || binNum2 > fDimBins2D.at(binNum1-1)){
				cerr << "[ERROR] " << __FUNCTION__ << ": bad 2. dim bin id! Exiting..." << endl;
				exit(EXIT_FAILURE);
			}
			if(binNum3 < 1 || binNum3 > fDimBins3D.at(binNum1-1).at(binNum2-1)){
				cerr << "[ERROR] " << __FUNCTION__ << ": bad 2. dim bin id! Exiting..." << endl;
				exit(EXIT_FAILURE);
			}		
			
			int index = 0;
			
			for(int i = 0; i < binNum1-1; i++){
				for(int j = 0; j < fDimBins2D.at(i); j++){
					index += fDimBins3D.at(i).at(j);
				}
			}
			
			for(int j = 0; j < binNum2-1; j++){
				index += fDimBins3D.at(binNum1-1).at(j);
			}
			index += binNum3;
			//cout << index << endl;
			return fBin.at(index-1);			
			break;
		}	
	}

}


int PhaseSpace::GetDimBins(int dim, int n1, int n2){
	if(dim < 1 || dim > fNDimensions){
		cerr << __FUNCTION__ << ": Wrong dimension number! Please, check number of dimensions! Exiting..." << endl;
		exit(EXIT_FAILURE);
	}
	
	switch(dim){
		case 1:
		{
			return fDimBins1D;
			break;
		}
		case 2:
		{
			if (n1 < 1 || n1 > fDimBins1D){
				cerr << __FUNCTION__ << ": Wrong number of first dim. bins! Exiting..." << endl;
				exit(EXIT_FAILURE);			
			}
			
			return fDimBins2D.at(n1-1);
			break;
		}
		case 3:
		{
			if (n1 < 1 || n1 > fDimBins1D){
				cerr << __FUNCTION__ << ": Wrong number of first dim. bins! Exiting..." << endl;
				exit(EXIT_FAILURE);			
			}
			if (n2 < 1 || n2 > fDimBins2D.at(n1-1)){
				cerr << __FUNCTION__ << ": Wrong number of second dim. bins! Exiting..." << endl;
				exit(EXIT_FAILURE);			
			}
			
			return fDimBins3D.at(n1-1).at(n2-1);
			break;
		}
	}
}


double PhaseSpace::GetVarBinSize(int var, int bin){
	if(bin < 1 || bin > fBin.size()){
		cerr << __FUNCTION__ << ": Wrong bin id. Exiting..." << endl;
		exit(EXIT_FAILURE);
	}
	
	if(var == 1){
		if( fVarBinSize1.at(bin-1) < 0.0001)
			return 0.001;
		else
			return fVarBinSize1.at(bin-1);
	}
	else if(var == 2){
		if( fVarBinSize1.at(bin-1) < 0.0001)
			return 0.001;
		else
			return fVarBinSize1.at(bin-1);
	}
}
