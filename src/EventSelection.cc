#include "EventSelection.h"
#include <det/TriggerConst.h>
#include <det/BPDConst.h>
#include <evt/RawEvent.h>
#include <boost/assign/std/map.hpp>
#include <boost/assign/list_of.hpp> 
#include <map>
#include <math.h>
#include <evt/rec/BPDPlane.h>
#include <TreeDataTypes.h>


using namespace boost::assign;
using namespace std;
using namespace evt;
using namespace evt::rec;
using namespace evt::raw;
using namespace det;




map<string, EventSelection::evtFuncPtr> EventSelection::fEvtFunc = 
  	boost::assign::map_list_of
  	("T2", T2)  
	("T3", T3)
	("HasToF", HasToF)
	("WFA", WFA)
	("BPD", BPD)
  	("BPD3", BPD3) 
  	("Chi2", Chi2)
	("R", R)
	("Multiplicity",Multiplicity);


bool EventSelection::IsEventGood(const Event & event){
  
  	++fNEvents;
	//cout << fCut.size() << endl;
  	for (vector<Cut>::iterator cIt = fCut.begin(); cIt < fCut.end(); ++cIt){        

			//cout << "radi 1 " << endl;
	    	string cutName = cIt -> GetName();
			//cout << "radi 2 " << endl;
	    	if (fEvtFunc.count(cutName) == 1){
	      		bool retValue = fEvtFunc[cutName]( event, *cIt);
	      
	      		if (retValue == true){
					cIt -> IncrementNEvents();    
				}
	      		else 
					return false;
	    	}
	    	else { 
	      		cout << "Event Cut " << cutName  << " is not implemented!!! Exiting..." << endl;
	      		exit (100);
	    	}
  	}
  	return true;
}

//*************************************************************************************************
bool EventSelection::T2(const Event &event, Cut&c){
  

        if(event.GetSimEvent().HasMainVertex()) return true;
	const evt::raw::Trigger& trigger = event.GetRawEvent().GetBeam().GetTrigger();
	bool isT2 = trigger.IsTrigger(TriggerConst::eT2, TriggerConst::eAll);
	

	return (c.IsAntiCut() ? !isT2 != 0 : isT2);
}

//*************************************************************************************************
bool EventSelection::T3(const Event &event, Cut&c){
  


        if(event.GetSimEvent().HasMainVertex()) return true;
	const evt::raw::Trigger& trigger = event.GetRawEvent().GetBeam().GetTrigger();
	bool isT3 = trigger.IsTrigger(TriggerConst::eT3, TriggerConst::eAll);


	return (c.IsAntiCut() ? !isT3 != 0 : isT3);
}

//*************************************************************************************************
bool EventSelection::HasToF(const Event &event,Cut&c){
  

	bool hasToF = false;

	int count = 0;
	const RecEvent& recEvent = event.GetRecEvent();
	for (std::list<rec::TOFMass>::const_iterator tofIter = recEvent.Begin<rec::TOFMass>();
           	tofIter != recEvent.End<rec::TOFMass>(); ++tofIter)
    {
		++count;
		if(count >0) continue;

	}

	
	if(count > 0)
		hasToF = true;

	return (c.IsAntiCut() ? !hasToF : hasToF);
}

//*************************************************************************************************
bool EventSelection::WFA(const Event &event, Cut&c){



	const vector<double> &cutParam = c.GetParam();
	if (cutParam.size() != 1) {
		cout << "[EventSelection::WFA] "
		 << "Missing or wrong cut parameters. Exiting..." << endl;
	    	exit(100);
	}

	const evt::raw::Trigger& trigger = event.GetRawEvent().GetBeam().GetTrigger();
	unsigned int count=0;
	bool wfa = false;
	//cout << trigger.GetADC(TriggerConst::eS3) << endl;
	CutVal ct;
 	ct.intFlag = true;
	if (trigger.HasTimeStructure(TimeStructureConst::eWFA, TriggerConst::eS1_1)==true){
		vector<double> BeamTimes = trigger.GetTimeStructure(TimeStructureConst::eWFA, TriggerConst::eS1_1);
		//cout << BeamTimes.size() << endl;

		
		for (unsigned int i=0; i<BeamTimes.size(); i++) {

			if (abs(BeamTimes[i]) < cutParam.at(0)) {
				++count;

			}
			//fWFA.at(0).Fill(BeamTimes[i]/1000.);

		}

		ct.valI = count;

		if(count == 1){
			wfa=true;
		}
	}
	else{
		ct.valI = 0;
	}

	ct.selected = (c.IsAntiCut() ? !wfa : wfa);  
	c.AddValue(ct);
	return (c.IsAntiCut() ? !wfa : wfa);  
}

//*************************************************************************************************
bool EventSelection::BPD(const Event &event, Cut&c){
	const vector<double> &cutParam = c.GetParam();
	if (cutParam.size() != 1) {
		cout << __FUNCTION__ 
		 << ": Missing or wrong cut parameters. Exiting..." << endl;
	    	exit(100);
	}
	

	if(event.GetSimEvent().HasMainVertex()) return true;
	const int beamStatus =  event.GetRecEvent().GetBeam().GetStatus();
	bool isBPDGood = true;

	if(beamStatus != cutParam.at(0))
		isBPDGood = false;

  	return (c.IsAntiCut() ? !isBPDGood : isBPDGood);
}


bool EventSelection::BPD3(const Event &event, Cut&c){




	if(event.GetRecEvent().GetBeam().GetNBPDPlanes() < 1)
		return (c.IsAntiCut() ? true : false);
  	unsigned int statusX = event.GetRecEvent().GetBeam().GetBPDPlane(BPDConst::eBPD3x).GetStatus();
	unsigned int statusY = event.GetRecEvent().GetBeam().GetBPDPlane(BPDConst::eBPD3y).GetStatus();
	
	bool hasBPD3 = (statusX == 0) && (statusY == 0);

	return (c.IsAntiCut() ? !hasBPD3 : hasBPD3);  
}


//*************************************************************************************************
bool EventSelection::Chi2(const Event &event, Cut&c){
  

	const vector<double> &cutParam = c.GetParam();
	if (cutParam.size() != 1) {
		cout << "[EventSelection::Chi2] "
		 << "Missing or wrong cut parameters. Exiting..." << endl;
	    	exit(100);
	}
	const Fitted2DLine lineX = event.GetRecEvent().GetBeam().Get(BPDConst::eX);
	const Fitted2DLine lineY = event.GetRecEvent().GetBeam().Get(BPDConst::eY);

	const evt::rec::BPDPlane bpd1X = event.GetRecEvent().GetBeam().GetBPDPlane(BPDConst::eBPD1, BPDConst::eX);
	const evt::rec::BPDPlane bpd1Y = event.GetRecEvent().GetBeam().GetBPDPlane(BPDConst::eBPD1, BPDConst::eY);
	double z1 = bpd1X.GetZ();

	const evt::rec::BPDPlane bpd2X = event.GetRecEvent().GetBeam().GetBPDPlane(BPDConst::eBPD2, BPDConst::eX);
	const evt::rec::BPDPlane bpd2Y = event.GetRecEvent().GetBeam().GetBPDPlane(BPDConst::eBPD2, BPDConst::eY);
	double z2 = bpd2X.GetZ();
	
	const evt::rec::BPDPlane bpd3X = event.GetRecEvent().GetBeam().GetBPDPlane(BPDConst::eBPD3, BPDConst::eX);
	const evt::rec::BPDPlane bpd3Y = event.GetRecEvent().GetBeam().GetBPDPlane(BPDConst::eBPD3, BPDConst::eY);
	double z3 = bpd3X.GetZ();

	double valX = 0;
	double valY = 0;

	valX += pow((bpd1X.GetPosition()-lineX.GetValueAtZ(z1))/bpd1X.GetPositionError(),2);
	valY += pow((bpd1Y.GetPosition()-lineY.GetValueAtZ(z1))/bpd1Y.GetPositionError(),2);

	valX += pow((bpd2X.GetPosition()-lineX.GetValueAtZ(z2))/bpd2X.GetPositionError(),2);
	valY += pow((bpd2Y.GetPosition()-lineY.GetValueAtZ(z2))/bpd2Y.GetPositionError(),2);

	valX += pow((bpd3X.GetPosition()-lineX.GetValueAtZ(z3))/bpd3X.GetPositionError(),2);
	valY += pow((bpd3Y.GetPosition()-lineY.GetValueAtZ(z3))/bpd3Y.GetPositionError(),2);
	bool goodChi2X = valX < cutParam[0];
	bool goodChi2Y = valY < cutParam[0];



	bool isGoodChi2 = goodChi2X && goodChi2Y;



  	return (c.IsAntiCut() ? !isGoodChi2 : isGoodChi2);  
  
}


//*************************************************************************************************
bool EventSelection::R(const Event &event, Cut&c){


	const vector<double> &cutParam = c.GetParam();
	if (cutParam.size() != 4) {
		cout << "[EventSelection::R] "
		 << "Missing or wrong cut parameters. Exiting..." << endl;
	    	exit(100);
	}

        double x = 0;
	double y = 0;

	if(event.GetSimEvent().HasMainVertex()){
	  const SimEvent& simEvent = event.GetSimEvent();
	  const evt::sim::Beam& beam = simEvent.GetBeam();

	  x = (cutParam.at(0)-beam.GetOrigin().GetZ())*beam.GetMomentum().GetX()/beam.GetMomentum().GetZ() + beam.GetOrigin().GetX();
	  y = (cutParam.at(0)-beam.GetOrigin().GetZ())*beam.GetMomentum().GetY()/beam.GetMomentum().GetZ() + beam.GetOrigin().GetY();
	}
    else{
	  const Fitted2DLine lineX = event.GetRecEvent().GetBeam().Get(BPDConst::eX);
	  const Fitted2DLine lineY = event.GetRecEvent().GetBeam().Get(BPDConst::eY);

	  x = lineX.GetValueAtZ(cutParam.at(0));
	  y = lineY.GetValueAtZ(cutParam.at(0));
     }
     bool goodR = false;

     CutVal ct;
     ct.intFlag = false;

     ct.valD = sqrt(pow(x-cutParam.at(1),2)+pow(y-cutParam.at(2),2));
     if(ct.valD  < cutParam.at(3))
	 	goodR = true;

     ct.selected = (c.IsAntiCut() ? !goodR : goodR);
     return (c.IsAntiCut() ? !goodR : goodR);

}

//*************************************************************************************************
bool EventSelection::Multiplicity(const Event &event, Cut&c){
	const vector<double> &cutParam = c.GetParam();
	if (cutParam.size() != 1) {
		cout << __FUNCTION__ 
		 << ": Missing or wrong cut parameters. Exiting..." << endl;
	    	exit(100);
	}
	

	const RecEvent& recEvent = event.GetRecEvent();
	int count = 0;
	bool isMultGood = false;
	if(recEvent.HasMainVertex()){
		if (recEvent.GetMainVertex().GetNumberOfDaughterTracks() > cutParam.at(0)) isMultGood = true;
		//cout << recEvent.GetMainVertex().GetNumberOfDaughterTracks() << endl;
	}
	/*for (std::list<rec::Track>::const_iterator iter = recEvent.Begin<rec::Track>();
           iter != recEvent.End<rec::Track>(); ++iter)
    {
    	++count;
    	if(count > cutParam.at(0)){
    	    isMultGood = true;
    		break;

    	}
    }*/

  	return (c.IsAntiCut() ? !isMultGood : isMultGood);
}
