#include "TrackExtrapolation.h"
#include "evt/rec/Cluster.h"
//#include "CConfig.h"

//CentralConfig& cConfig = fwk::CentralConfig::GetInstance("bootstrap.xml", false, false);
CentralConfig &TrackExtrapolation::fCentralConfig = fwk::CentralConfig::GetInstance("bootstrap.xml", false, false);
Detector &TrackExtrapolation::fDetector = Detector::GetInstance();
const MagneticField &TrackExtrapolation::fMagField = TrackExtrapolation::fDetector.GetMagneticField();

double TrackExtrapolation::fKappa = 0.000299792458L;
TrackExtrapolation::TrackExtrapolation(bool cov, double step, double radLength){
	fStep = step;
	fRadLength = radLength;
	fTarget = NULL;
	TrackPar temp(eNA61);
	fStartPar = temp;
	fStopPar = temp;
	fErrorEstimation = cov;
}
TrackExtrapolation::TrackExtrapolation(bool cov, const Track &track, double step, double radLength){
	fTarget = NULL;
	SetTrackParams(track);
	fStep = step;
	fRadLength = radLength;
	fErrorEstimation = cov;
}

void TrackExtrapolation::SetTrackParams(const Track& track){

	const CovarianceMatrix &cov = track.GetCovarianceMatrix();
	double pxz = sqrt(track.GetMomentum().GetX()*track.GetMomentum().GetX()+track.GetMomentum().GetZ()*track.GetMomentum().GetZ());
	double par[5] = {track.GetCharge()/pxz, track.GetMomentum().GetY()/pxz, atan(track.GetMomentum().GetZ()/track.GetMomentum().GetX()), 
		//track.GetFirstPointOnTrack().GetX(), track.GetFirstPointOnTrack().GetY()};
		track.GetMomentumPoint().GetX(), track.GetMomentumPoint().GetY()};
	double covM[15];
	

	int ind = 0;
	for(int i = 0; i < cov.GetExtent(); ++i){
		for(int j = i; j < cov.GetExtent(); ++j){
			covM[ind] = cov[i][j];
			ind++;
		}	
	}
	
	TrackPar temp(eNA61, track.GetMomentumPoint().GetZ(), par, covM);
	temp.SetCharge(track.GetCharge());
	//temp.Print();
	ConvertTrackPar(temp, eKisel);
	//temp.Print();
	fStartPar = temp;
	fStopPar = temp;
	
}


/*void TrackExtrapolation::SetClusters(const evt::rec::Track& track, const evt::RecEvent& recEvent){
		fClusters.clear();
		ClusterIndexIterator it;
		for(it = track.ClustersBegin(); it!= track.ClustersEnd(); it++){
			const Cluster clust= recEvent.Get(*it);
			const Point& pos = clust.GetPosition();
			TPCCluster temp(pos.GetX(), pos.GetY(), pos.GetZ(), clust.GetPositionUncertainty( evt::rec::ClusterConst::eX), clust.GetPositionUncertainty( evt::rec::ClusterConst::eY), clust.GetTPCId());
			fClusters.push_back(temp);
		}
}*/
void TrackExtrapolation::GetMagField(Point& point, double &Bx, double &By, double &Bz){
	//double zmin = -592;
	double zmin = -567.4;
  	float dist = 0;
 	float supr = 0.03;
	double norm = 100000;
	double kN = 1;
	if(point.GetZ() > zmin){
		const Vector magVec = fMagField.GetField(point);
		Bx = norm*magVec.GetX();
		By = norm*magVec.GetY();
		Bz = norm*magVec.GetZ();
	}
	else{

		const Point pField(point.GetX(), point.GetY(), zmin);
		const Vector magVec = fMagField.GetField(pField);
		double dist = zmin - point.GetZ();
		Bx = kN*norm*magVec.GetX()*exp(-dist*supr);
		By = kN*norm*magVec.GetY()*exp(-dist*supr);
		Bz = kN*norm*magVec.GetZ()*exp(-dist*supr);
		
	}
}
