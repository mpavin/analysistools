#include "AnaTrack.h"
#include "Constants.h"
#include <math.h>

void AnaTrack::Set(const Track& track){
	
	fExtrapolatedFlag = false;
	fMomentum = &track.GetMomentum();
	fTrack = &track;

	fHasToF = false;
	
	fNumberOfClusters.VTPC1 = track.GetNumberOfClusters(TrackConst::eVTPC1);
	fNumberOfClusters.VTPC2 = track.GetNumberOfClusters(TrackConst::eVTPC2);
	fNumberOfClusters.GTPC = track.GetNumberOfClusters(TrackConst::eGTPC);
	fNumberOfClusters.MTPC = track.GetNumberOfClusters(TrackConst::eMTPC);
	fNumberOfClusters.all = fNumberOfClusters.VTPC1 + fNumberOfClusters.VTPC2 + fNumberOfClusters.GTPC + fNumberOfClusters.MTPC;
	/*fdEdxVal = track.GetEnergyDeposit(TrackConst::eAll);
	fdEdxVar = track.GetEnergyDepositVariance(TrackConst::eAll);*/
	fCharge = track.GetCharge();
	fTopology = FindTopology();
	//fTopInMagFld = eWST;

	/*if(fMomentum->GetX()*fCharge > 0)
		fTopInMagFld = eRST;*/

	fZLast = track.GetLastPointOnTrack().GetZ();
	fDistanceFromTarget = 0;
	fExtrapolatedPosition = Point(0,0,0);

	//if(fStatus == 0){
		//const CovarianceMatrix &cov = track.GetCovarianceMatrix();
		//fSigmaR = sqrt(cov.Std(4a)*cov.Std(4) + cov.Std(5)*cov.Std(5));
		//fSigmaR = 0;
		/*double pxz = sqrt(fMomentum->GetX()*fMomentum->GetX()+fMomentum->GetZ()*fMomentum->GetZ());
		double py = fMomentum->GetY();
		double pMag = fMomentum->GetMag();

		double sigmaPxz = cov.Std(1)*pxz*pxz;
		double sigmaPy = sqrt(pow(cov.Std(2)*pxz,2)+pow(cov.Std(1)*pxz*pxz*pxz/py,2));
		fSigmaP = sqrt(pow(py*sigmaPy/pMag,2) + pow(pxz*sigmaPxz/pMag,2));*/
		//fSigmaP = 0;
	/*}
	else{
		fSigmaR = 999;
		fSigmaP = 999;
	}*/
	fTrackExtrapolation->SetTrackParams(track);
	
}

//*****************************************************************************************
int AnaTrack::FindTopology(){
	
	int n = fNumberOfClusters.all - fNumberOfClusters.MTPC;
	int top = 0;
	if(fNumberOfClusters.VTPC1 > 0){
			top = top | eTopVTPC1;
	}
	
	if(fNumberOfClusters.VTPC2 > 0){
			top = top | eTopVTPC2;
	}
	if(fNumberOfClusters.GTPC > 0){

			top = top | eTopGTPC;
	}
	if(fNumberOfClusters.MTPC){
			top = top | eTopMTPC;
	}

	return top;
}


void AnaTrack::DoExtrapolation(){
	fPositionErrors.clear();
	
	//cout << "****************************************************" << endl;
	fExtrapolatedFlag = true;
	fDistanceFromTarget = fTrackExtrapolation->ExtrapolateToTarget(0.01);
	
	TrackPar &trackPar = fTrackExtrapolation->GetStopTrackParam();
	
			//cout << __FUNCTION__ << " "  << trackPar.GetPar(0) << " " << trackPar.GetPar(1) <<  " " << sqrt(trackPar.GetPar(0)*trackPar.GetPar(0) + trackPar.GetPar(1)*trackPar.GetPar(1)) << endl;
			
	fExtrapolatedPosition = Point(trackPar.GetPar(0), trackPar.GetPar(1), trackPar.GetZ());
		//cout << fExtrapolatedPosition.GetX() << " " << fExtrapolatedPosition.GetY() << " " << fExtrapolatedPosition.GetZ() << endl;
	//cout << trackPar.GetZ() << endl;
	double denom = sqrt(1 + trackPar.GetPar(2)*trackPar.GetPar(2) + trackPar.GetPar(3)*trackPar.GetPar(3));
	double pz = fabs(1/trackPar.GetPar(4))/denom;
	double px = pz*trackPar.GetPar(2);
	double py = pz*trackPar.GetPar(3);
	fMomentum = new Vector(px, py, pz);

	fPositionErrors.push_back(trackPar.GetStd(0));
	fPositionErrors.push_back(trackPar.GetStd(1));
}
