#include "TargetAna.h"


void TargetAna::Load(string name, ETargetType type, double *pos, double *dim, double *tilt){
	fName = name;
	fType = type;

	
	fPosition = Point(pos[0], pos[1], pos[2]);

	fDimensions[0] = dim[0]; fDimensions[1] = dim[1];

	fTilt[0] = tilt[0]; fTilt[1] = tilt[0];
}

void TargetAna::PrintTargetData(){
	
	string targType;

	switch(fType){
		case eLong:
			targType = "Long target";
			break;
		case eThin:
			targType = "Thin target";
			break;
		case eNominalBeamline:
			targType = "Nominal beamline!";
			break;
	}

	printf("*********************TARGET DATA*********************\n");
	printf("Target name: %s.\n", fName.c_str());
	printf("Target type: %s.\n", targType.c_str());

	if(fType != eNominalBeamline){

		printf("Target upstream position: x = %.2f cm, y = %.2f cm, z = %.2f cm.\n", fPosition.GetX(), fPosition.GetY(), fPosition.GetZ());
		printf("Target dimensions: l = %.2f cm, phi = %.2f cm.\n", fDimensions[0], fDimensions[1]);
	}
	else printf("Nominal beamline is only used for determining target alignment!\n");
	printf("*****************************************************\n");
}


/*void TargetAna::RotatePoint2(double* x,double* y,double* z,
		  double xc,double yc,double zc, double tXZ,double tYZ)
{
  	float xp = *x-xc;
  	float yp = *y-yc;
  	float zp = *z-zc;
  
  	float R[3][3]={{          cos(tXZ),       0,          sin(tXZ)},
		 { sin(tYZ)*sin(tXZ),cos(tYZ),-sin(tYZ)*cos(tXZ)},
		 {-cos(tYZ)*sin(tXZ),sin(tYZ), cos(tYZ)*cos(tXZ)}};

  
  	*x = R[0][0]*xp + R[0][1]*yp + R[0][2]*zp;
  	*y = R[1][0]*xp + R[1][1]*yp + R[1][2]*zp;
  	*z = R[2][0]*xp + R[2][1]*yp + R[2][2]*zp;

}*/



double TargetAna::GetDistanceFromTargetCenter(double x, double y, double z){
	

	//return sqrt(X*X + Y*Y);
	//return abs(sqrt(X*X+Y*Y) - fDimensions[1]);
	double zp = z - fPosition.GetZ();
	double dist = sqrt((x-fTilt[0]*zp - fPosition.GetX())*(x-fTilt[0]*zp - fPosition.GetX()) + (y-fTilt[1]*zp - fPosition.GetY())*(y-fTilt[1]*zp - fPosition.GetY())
	 + (fTilt[1]*(x-fPosition.GetX()) - fTilt[0]*(y-fPosition.GetY()))*(fTilt[1]*(x-fPosition.GetX()) - fTilt[0]*(y-fPosition.GetY())))/
	 sqrt(1 + fTilt[0]*fTilt[0] + fTilt[1]*fTilt[1]);
	//double dist = fabs(y-fTilt[1]*zp-fPosition.GetY())/sqrt(1+fTilt[1]*fTilt[1]);
	
	/*double zT = (z - fTilt[0]*(fPosition.GetX() - x) + fTilt[1]*(fPosition.GetY() - y))/(1 + fTilt[0]*fTilt[0] + fTilt[1]*fTilt[1]);
	double x0 = fTilt[0]*zT + fPosition.GetX();
	double y0 = fTilt[1]*zT + fPosition.GetY();
	return sqrt((x-x0)*(x-x0) + (y-y0)*(y-y0) + (z-zT)*(z-zT));*/
	/*RotatePoint2(&X, &Y, &Z, fPosition.GetX(), fPosition.GetY(), fPosition.GetZ(), fTilt[0], fTilt[1]);

	double upstreamZ = 0;
	double downstreamZ = fDimensions[0];

	if(Z < upstreamZ){
		dist = sqrt(X*X + Y*Y + Z*Z);
	}

	if(Z > downstreamZ){
		dist = sqrt(X*X + Y*Y + (Z - downstreamZ)*(Z - downstreamZ));

	}

	if(Z > upstreamZ && Z < downstreamZ){
		dist = sqrt(X*X + Y*Y);
	}*/


	return dist;

}



double TargetAna::GetDistanceFromTargetSurface(double x, double y, double z){

	//return abs(sqrt(X*X+Y*Y) - 1.3);
	double distC = GetDistanceFromTargetCenter(x, y, z);
	//cout << dist << endl;
	double dist = distC - fDimensions[1];
	
	if(dist < 0) dist = 0; 
	return dist;
	
	
	/*RotatePoint2(&X, &Y, &Z, fPosition.GetX(), fPosition.GetY(), fPosition.GetZ(), fTilt[0], fTilt[1]);

	double upstreamZ = 0;
	double downstreamZ = fDimensions[0];

	if(Z > upstreamZ && Z < downstreamZ){
		dist = sqrt(X*X + Y*Y) - fDimensions[1];
		if (dist < 0) dist = 0;
	}
	if(Z < upstreamZ){
		dist = sqrt(X*X + Y*Y);
		if (dist < fDimensions[1]) dist = upstreamZ - Z;
		else dist = sqrt((dist - fDimensions[1])*(dist - fDimensions[1]) + (upstreamZ - Z)*(upstreamZ - Z));
	}

	if(Z > downstreamZ){
		dist = sqrt(X*X + Y*Y);
		if (dist < fDimensions[1]) dist = Z - downstreamZ;
		else dist = sqrt((dist - fDimensions[1])*(dist - fDimensions[1]) + (Z - downstreamZ)*(Z - downstreamZ));
	}

	return dist;*/
	
}
