#include "TrackTools.h"
#include <math.h>

using namespace TrackTools;

TrackPar::TrackPar(){
	fType = eUnknown;
	fCharge = 0;
	fZ = 0;
	fLength = 0;
	for (int i = 0; i < 15; i++){
		if(i < 5){
			fPar[i] = 0;
		}
		fCov[i] = 0;
	}
}
//**************************************************************************************************
TrackPar::TrackPar(TrackingType type){
	fType = type;
	fCharge = 0;
	fZ = 0;
	fLength = 0;
	for (int i = 0; i < 15; i++){
		if(i < 5){
			fPar[i] = 0;
		}
		fCov[i] = 0;
	}
}
//**************************************************************************************************
TrackPar::TrackPar(TrackingType type, double z, double *par){
	fType = type;
	fZ = z;
	fLength = 0;
	//ParameterCheck(par);
	ChargeCheck(type, par);
	
	for(int i = 0; i < 15; i++){
		if(i < 5){
			fPar[i] = par[i];
		}
		fCov[i] = 0;
	}

}
//**************************************************************************************************
TrackPar::TrackPar(TrackingType type, double z, double *par, double *cov){
	fType = type;
	fZ = z;
	fLength = 0;
	//ParameterCheck(par);
	//CovarianceCheck(cov);
	ChargeCheck(type, par);
	
	for(int i = 0; i < 15; i++){
		if(i < 5){
			fPar[i] = par[i];
		}
		fCov[i] = cov[i];
	}	
}
//**************************************************************************************************

double TrackPar::GetPar(int i){
	//CheckIndex(i, fPar);
	return fPar[i];
}

//**************************************************************************************************
double TrackPar::GetCov(int i){
	//CheckIndex(i, fCov);
	return fCov[i];	
}

//**************************************************************************************************
double TrackPar::GetStd(int i){
	//CheckIndex(i, fPar);
	
	int index = 7*(i+1)-6-(i+1)*(i+2)/2;
	return sqrt(fCov[index]);
}
//**************************************************************************************************
void TrackPar::SetPar(int i, double val){
	//CheckIndex(i, fPar);
	fPar[i] = val;
}
//**************************************************************************************************
void TrackPar::SetPar(double *par){
	//ParameterCheck(par);
	
	for(int i = 0; i < 5; i++){
		fPar[i] = par[i];	
	}		
}
//**************************************************************************************************
void TrackPar::SetCov(int i, double val){
	//CheckIndex(i, fCov);
	fCov[i] = val;
}
//**************************************************************************************************
void TrackPar::SetCov(double *cov){
	//ParameterCheck(cov);
	
	for(int i = 0; i < 15; i++){
		fCov[i] = cov[i];	
	}		
}

TrackPar& TrackPar::operator= (TrackPar &parIn){
	if(this != &parIn){
		fType = parIn.GetType();
		fCharge = parIn.GetCharge();
		fZ = parIn.GetZ();
		fLength = parIn.GetLength();
		
		for(int i = 0; i < 15; i++){
			if(i < 5){
				fPar[i] = parIn.GetPar(i);
			}
			fCov[i] = parIn.GetCov(i);
		}
	}
	return *this;
}
//**************************************************************************************************

void TrackPar::Print(){

	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
	cout << "Type = " << fType << endl;
	cout << "z = " << fZ << endl;
	cout << "q = " << fCharge << endl;
	cout << "*****************************TRACK PARAMETERS*****************************" << endl;
	for(int i = 0; i < 5; i++){
		cout << "p" << i+1 << " = " << fPar[i] << endl;
	}
	cout << "*****************************COVARIANCE MATRIX*****************************" << endl;
	int ind1 = 0;
	//int ind2 = 0;
	for(int i = 0; i < 5; i++){
		for(int j = 0; j < 5; j++){
			if(j >= i){
				cout << fCov[ind1] << "	";
				ind1++;
			}
			else {
				cout << "		";
				
			}
		}
		cout << endl;
	}	
	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
}

