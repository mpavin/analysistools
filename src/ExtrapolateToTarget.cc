#include "TrackExtrapolation.h"

double TrackExtrapolation::ExtrapolateToTarget(double minStep){

	if(fTarget == NULL){
		cerr << "ERROR: " << __FUNCTION__ << ": target is not set! Exiting!!!" << endl;
		exit(EXIT_FAILURE);
	}
		
	
	
	Extrapolate(fTarget->GetUpstreamPosition().GetZ()+fTarget->GetLength());
	
	//bool rst = fStopPar.GetPar(0)*fStopPar.GetCharge() > 0;
	double dist = fTarget->GetDistanceFromTargetCenter(fStopPar.GetPar(0), fStopPar.GetPar(1), fStopPar.GetZ());


		/*double sigmaR = sqrt(fStopPar.GetPar(0)*fStopPar.GetPar(0)*fStopPar.GetStd(0)*fStopPar.GetStd(0)
		 + fStopPar.GetPar(1)*fStopPar.GetPar(1)*fStopPar.GetStd(1)*fStopPar.GetStd(1))
		 /sqrt(fStopPar.GetPar(0)*fStopPar.GetPar(0) + fStopPar.GetPar(1)*fStopPar.GetPar(1));*/
	//cout << fTarget->GetRadius() << " " << fTarget->GetLength()<<  endl;
	if(dist <= (fTarget->GetRadius())){
		return 0.;
	}	

	
	dist = fTarget->GetDistanceFromTargetSurface(fStopPar.GetPar(0), fStopPar.GetPar(1), fStopPar.GetZ());
	double oldEStep = fStep;
	
	fStep = 0.005;
	
	double step = minStep;
	if(minStep < fStep){
		step = 2*fStep;
		cerr << "WARNING: " << __FUNCTION__ << ": distance minimization step is smaller than extrapolation step. Minimization step is set to " << step << "." << endl;	
	}
	
	double distTemp = 0;
	
	//cout << "radi1 " << fStopPar.GetZ()  << endl;
	bool insideS = false;
	double thD = 0;
	double thDTemp = 0;
	//int count = 0;
	//cout << fStep << endl;
	while(fabs(fStopPar.GetZ() - fTarget->GetUpstreamPosition().GetZ()) > fabs(step)){
		fStartPar=fStopPar;

		Extrapolate(fStopPar.GetZ() - 2*step);
		distTemp = fTarget->GetDistanceFromTargetSurface(fStopPar.GetPar(0), fStopPar.GetPar(1), fStopPar.GetZ());
		
		/*double sigmaR = sqrt(fStopPar.GetPar(0)*fStopPar.GetPar(0)*fStopPar.GetStd(0)*fStopPar.GetStd(0)
		 + fStopPar.GetPar(1)*fStopPar.GetPar(1)*fStopPar.GetStd(1)*fStopPar.GetStd(1))
		 /sqrt(fStopPar.GetPar(0)*fStopPar.GetPar(0) + fStopPar.GetPar(1)*fStopPar.GetPar(1));
		if(!insideS){
		  if(distTemp < sigmaR){
		    thD = sigmaR/sqrt(fStopPar.GetPar(2)*fStopPar.GetPar(2) + fStopPar.GetPar(3)*fStopPar.GetPar(3))/3;
		     insideS = true;
		  }
		}
		else{
		  thDTemp += step;
		  if(thDTemp > thD && thDTemp < 2){
		    fStopPar = fStartPar;
		    dist = distTemp;
		    break;
		  }
		}*/
		/*if(distTemp < sigmaR)
			count++;
		
		if(count > 20){
			dist = distTemp;
			fStopPar = fStartPar;
			break;
			}*/

		if(distTemp < dist){
			if(fStopPar.GetZ() <  fTarget->GetUpstreamPosition().GetZ() + 1.5*step){
				dist = 99999;
				fStopPar = fStartPar;
				break;
			}
			else{
				dist = distTemp;
			}
				//break;
				//fStartPar = fStopPar;
		}
		else{
			fStopPar = fStartPar;
			break;
		}
	
	}
	//cout << fStopPar.GetZ() << endl;

	fStep = oldEStep;
	return dist;
	
		
}
