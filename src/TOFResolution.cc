#include "TOFResolution.h"
#include <TF1.h>
#include <TH1D.h>
#include <TTree.h>
#include <TFile.h>
#include <stdlib.h>
#include "Functions.h"
TOFResolution::TOFResolution(string tofMap, vector<double> &ranges){

	if(ranges.size() != 8){
		cerr << __FUNCTION__ << ": Size of the vector<double> ranges must be 8 with this order: r_piMean, r_piWidth, r_pMean, r_pWidth, r_KMean, r_KWidth, r_eMean, r_eWidth. " << endl;
		exit(EXIT_FAILURE);
	}
	
	fPionPars[6] = ranges.at(0);
	fPionPars[7] = ranges.at(1);
	fProtonPars[6] = ranges.at(2);
	fProtonPars[7] = ranges.at(3);
	fKaonPars[6] = ranges.at(4);
	fKaonPars[7] = ranges.at(5);
	fElectronPars[6] = ranges.at(6);
	fElectronPars[7] = ranges.at(7);	
	fTOFMap = tofMap;

		
	TFile input(tofMap.c_str(), "READ");

	TTree *tree = (TTree*) input.Get("tofPars");

	string *particle = NULL;
	int n;
	double pars[6];
	
	TBranch *bParticle = tree->GetBranch("particle");
	TBranch *bN = tree->GetBranch("n");
	TBranch *bPars = tree->GetBranch("pars");

	bParticle->SetAddress(&particle);

	bN->SetAddress(&n);

	bPars->SetAddress(pars);
		
	int nEntries = tree->GetEntries();
	for(int i=0; i<nEntries; i++){
	
		tree->GetEntry(i);
		
		if(*particle == "pion"){
		
			for(int j = 0; j < n; ++j){
				//cout << "*1" << endl;
				cout << pars[j] << endl;
				fPionPars[j] = pars[j];
				//cout << "*2" << endl;

			}
			
		}
		else if(*particle == "proton"){

			for(int j = 0; j < n; ++j){
				fProtonPars[j] = pars[j];

			}

		}
		else if(*particle == "electron"){

			for(int j = 0; j < n; ++j){
				fElectronPars[j] = pars[j];

			}

		}
		else if(*particle == "kaon"){

			for(int j = 0; j < n; ++j){
				fKaonPars[j] = pars[j];

			}

		}		
	}
	
	//*++++++++++++++++++++++++++++++++++++++++
	//fElectronPars[0] = 0.002;
	//fElectronPars[1] = 0.0;
	//fProtonPars[0] = 0.92;
	//fKaonPars[0] = 0.25;
}




