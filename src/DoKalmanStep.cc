#include <iostream> 
#include <math.h> 
#include <algorithm> 

#include "TrackExtrapolation.h"
#include "TrackTools.h"

#define DEBUG 0
/*************************************************************************************************************************

	
**************************************************************************************************************************/




void TrackExtrapolation::DoKalmanStep(TPCCluster &cluster){

	if(DEBUG){
		cout << "********************************************************" << endl;
		cout << "CLUSTER" << endl;
		cout << "(x, y, z) = " << cluster.GetPosition(eX) << ", " << cluster.GetPosition(eY) << ", " << cluster.GetPosition(eZ) << ") [cm]" << endl;
		cout << "(dx, dy) = " << cluster.GetError(errX) << ", " << cluster.GetError(errY) << ") [cm]" << endl;
		cout << "********************************************************" << endl;
		cout << "Track parameters (BEFORE EXTRAPOLATION)" << endl;
		fStopPar.Print();
	}

	double xStart[3] = {fStopPar.GetPar(0),fStopPar.GetPar(1),fStopPar.GetZ()};
	Extrapolate(cluster.GetPosition(eZ));


	
	double resid[2] = {cluster.GetPosition(eX)-fStopPar.GetPar(0), cluster.GetPosition(eY)-fStopPar.GetPar(1)};
	if(DEBUG){
		cout << "********************************************************" << endl;
		cout << "Track parameters (AFTER EXTRAPOLATION)" << endl;
		fStopPar.Print();
		cout << "********************************************************" << endl;
		cout << "RESIDUALS" << endl;
		cout << "(resX, resY) = " << resid[0] << ", " << resid[1] << ") [cm]" << endl;	
	}	
	double *cov = fStopPar.GetCov();
	double det = (cov[0]+cluster.GetError(errX)*cluster.GetError(errX))*(cov[5]+cluster.GetError(errY)*cluster.GetError(errY)) - cov[1]*cov[1];
	//cout << resid[0] << " " << resid[1] << " " << det << endl;
	double Sinv[3] = {(cov[5]+cluster.GetError(errY)*cluster.GetError(errY))/det, -cov[1]/det, (cov[0]+cluster.GetError(errX)*cluster.GetError(errX))/det};
	
	double K[5][2] = {	{cov[0]*Sinv[0] + cov[1]*Sinv[1], cov[0]*Sinv[1] + cov[1]*Sinv[2]}, 
						{cov[1]*Sinv[0] + cov[5]*Sinv[1], cov[1]*Sinv[1] + cov[5]*Sinv[2]},
						{cov[2]*Sinv[0] + cov[6]*Sinv[1], cov[2]*Sinv[1] + cov[6]*Sinv[2]},
						{cov[3]*Sinv[0] + cov[7]*Sinv[1], cov[3]*Sinv[1] + cov[7]*Sinv[2]},
						{cov[4]*Sinv[0] + cov[8]*Sinv[1], cov[4]*Sinv[1] + cov[8]*Sinv[2]}};
	
	if(DEBUG){
		cout << "********************************************************" << endl;
		cout << "KALMAN GAIN" << endl;
		for(int i = 0; i < 5; i++){
			cout << K[i][0] << "	" << K[i][1] << endl;
		}	
	}	

	fStopPar.AddLength(sqrt(pow(fStopPar.GetPar(0)+K[0][0]*resid[0]+K[0][1]*resid[1]-xStart[0], 2)+pow(fStopPar.GetPar(1)+K[1][0]*resid[0]+K[1][1]*resid[1]-xStart[1], 2)+pow(fStopPar.GetZ()-xStart[2], 2))-
	sqrt(pow(fStopPar.GetPar(0)-xStart[0], 2)+pow(fStopPar.GetPar(1)-xStart[1], 2)+pow(fStopPar.GetZ()-xStart[2], 2)));
	for(int i = 0; i < 5; i++){
		//cout << fStopPar.GetPar(i) << " " << K[i][0]*resid[0]+K[i][1]*resid[1] << endl;
		fStopPar.SetPar(i, fStopPar.GetPar(i) + K[i][0]*resid[0]+K[i][1]*resid[1]);
	}	
	
						
	double KHC[15] = { K[0][0]*cov[0]+K[0][1]*cov[1], K[0][0]*cov[1]+K[0][1]*cov[5], K[0][0]*cov[2]+K[0][1]*cov[6], K[0][0]*cov[3]+K[0][1]*cov[7], K[0][0]*cov[4]+K[0][1]*cov[8], 														  K[1][0]*cov[1]+K[1][1]*cov[5], K[1][0]*cov[2]+K[1][1]*cov[6], K[1][0]*cov[3]+K[1][1]*cov[7], K[1][0]*cov[4]+K[1][1]*cov[8],
																					 K[2][0]*cov[2]+K[2][1]*cov[6], K[2][0]*cov[3]+K[2][1]*cov[7], K[2][0]*cov[4]+K[2][1]*cov[8],
																													K[3][0]*cov[3]+K[3][1]*cov[7], K[3][0]*cov[4]+K[3][1]*cov[8],
																																				   K[4][0]*cov[4]+K[4][1]*cov[8]};
	for(int i = 0; i < 15; i++){
		cov[i] = cov[i] - KHC[i];
	}					
	if(DEBUG){
		cout << "********************************************************" << endl;
		cout << "Track parameters (AFTER KALMAN STEP)" << endl;
		fStopPar.Print();
		cout << "********************************************************" << endl;
		cout << "********************************************************" << endl;

	}	
	
}


