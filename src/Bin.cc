#include <stdlib.h>
#include "Bin.h"

using namespace std;




//*********************************************************************************************

double Bin::GetLowValue(int dim){
	if(dim > fLow.size()){
		cerr << "[ERROR]: " << __FUNCTION__ << " - Bin dimension is less than " << dim << "! Exiting..." << endl;
		exit(EXIT_FAILURE);	
	}
	
	return fLow.at(dim-1);

}

//*********************************************************************************************
double Bin::GetHighValue(int dim){
	if(dim > fLow.size()){
		cerr << "[ERROR]: " << __FUNCTION__ << " - Bin dimension is less than " << dim << "! Exiting..." << endl;
		exit(EXIT_FAILURE);	
	}
	
	return fHigh.at(dim-1);
}

//*********************************************************************************************
double Bin::GetDelta(int dim){
	if(dim > fLow.size()){
		cerr << "[ERROR]: " << __FUNCTION__ << " - Bin dimension is less than " << dim << "! Exiting..." << endl;
		exit(EXIT_FAILURE);	
	}
	
	
	return fHigh.at(dim - 1) - fLow.at(dim - 1);

}

//*********************************************************************************************
double Bin::GetMidValue(int dim){
	if(dim > fLow.size()){
		cerr << "[ERROR]: " << __FUNCTION__ << " - Bin dimension is less than " << dim << "! Exiting..." << endl;
		exit(EXIT_FAILURE);	
	}
	
	return fLow.at(dim - 1) + (fHigh.at(dim - 1) - fLow.at(dim - 1))/2.;

}

