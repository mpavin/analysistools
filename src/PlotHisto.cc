#include <stdlib.h>

#include "PlotHisto.h"

#include <fwk/CentralConfig.h>
#include <utl/Branch.h>
#include <utl/BranchIterator.h>
#include "CConfig.h"
#include <cmath>

using namespace fwk;
using namespace utl; 
using namespace std;

PlotHisto::PlotHisto(int dim, string config, string cNameR, string cNameW, string cNameA, string nameAdd, double *range1, double *range2, int *nBins1, int *nBins2){
	if(dim < 1 && dim > 2){
		cerr << "ERROR: " << __FUNCTION__ << ": Wrong dimension! Only 1 or 2 accepted!" << endl;
		exit (EXIT_FAILURE);
	}
	fDim = dim;
	switch(dim){
		case 1:
			Init(config, cNameR, nameAdd, 1, 1, range1, nBins1);
			Init(config, cNameW, nameAdd, 1, 2, range2, nBins2); 
			Init(config, cNameA, nameAdd, 1, 3, range1, nBins1);
			break;
		
		case 2:
			Init(config, cNameR, nameAdd, 2, 1, range1, nBins1);
			Init(config, cNameW, nameAdd, 2, 2, range2, nBins2); 
			Init(config, cNameA, nameAdd, 2, 3, range1, nBins1);
			break;		
	}
}

/*PlotHisto::~PlotHisto(){
cout << "radi1" << endl;
	if(fRST1 != NULL){
		delete fRST1;
		fRST1 = NULL;
	}
cout << "radi2" << endl;
	if(fWST1 != NULL){
		delete fWST1;
		fWST1 = NULL;
	}
cout << "radi3" << endl;
	if(fAll1 != NULL){
		delete fAll1;
		fAll1 = NULL;
	}
cout << "radi4" << endl;
	if(fRST2 != NULL){
		delete fRST2;
		fRST2 = NULL;
	}
cout << "radi5" << endl;
	if(fWST2 != NULL){
		delete fWST2;
		fWST2 = NULL;
	}
cout << "radi6" << endl;
	if(fAll2 != NULL){
		delete fAll2;
		fAll2 = NULL;
	}
cout << "radi7" << endl;
	if(fCRST != NULL){
		delete fCRST;
		fCRST = NULL;
	}
cout << "radi8" << endl;
	if(fCWST != NULL){
		delete fCWST;
		fCWST = NULL;
	}
cout << "radi9" << endl;
	if(fCAll != NULL){
		delete fCAll;
		fCAll = NULL;
	}

}*/

void PlotHisto::Init(string config, string cName, string nameAdd, int dim, int rst, double *range, int *num){

	Branch histosCfg = cConfig.GetTopBranch(config);
	if(histosCfg.GetName() != config){
		cerr <<"ERROR: "<< __FUNCTION__ << ": Wrong config file! Top branch name doesn't match! Please check your configuration files! Exiting..." << endl;
		exit (EXIT_FAILURE);
	}


	Branch child = histosCfg.GetChild(cName);

	
	vector<int> nBins;
	vector<double> low;
	vector<double> high;
	string name;
	string title;
	vector<string> axisTitle;
	vector<float> axisTitleSize;
	vector<float> axisLabelSize;
	vector<int> canvasSize;
	string canvasName;
	bool plot;
	vector<double> axisOffset;
	vector<double> resizeY;
	bool stats = true;
	for(BranchIterator it = child.ChildrenBegin(); it != child.ChildrenEnd(); ++it){
		Branch option = *it;
		
		if(option.GetName() == "nBins"){
			option.GetData(nBins);
		}
		
		else if(option.GetName() == "lowValues"){
			option.GetData(low);
		}
		
		else if(option.GetName() == "highValues"){
			option.GetData(high);
		}
		
		else if(option.GetName() == "name"){
			option.GetData(name);
		}
		
		else if(option.GetName() == "title"){
			option.GetData(title);
		}
		
		else if(option.GetName() == "axisTitle"){
			option.GetData(axisTitle);
		}
		
		else if(option.GetName() == "axisTitleSize"){
			option.GetData(axisTitleSize);
		}

		else if(option.GetName() == "axisLabelSize"){
			option.GetData(axisLabelSize);
		}
		
		else if(option.GetName() == "axisOffset"){
			option.GetData(axisOffset);
		}

		else if(option.GetName() == "showStats"){
			option.GetData(stats);
		}
				
		else if(option.GetName() == "plot"){
			option.GetData(plot);
		}
					
		else if(option.GetName() == "canvasName"){
			option.GetData(canvasName);
		}
		
		else if(option.GetName() == "canvasSize"){
			option.GetData(canvasSize);
		}
		else{
			cerr << __FUNCTION__ << ": Unknown branch! Skipping..." << endl;
		}
	}
	
	
	if(range != NULL){
		low.clear();
		high.clear();
		if(dim == 1){
			low.push_back(range[0]);
			high.push_back(range[1]);
		}
		else if(dim == 2){
			low.push_back(range[0]);
			high.push_back(range[1]);
			low.push_back(range[2]);
			high.push_back(range[3]);						
		}
	}
	if(num != NULL){
		nBins.clear();
		if(dim == 1){
			nBins.push_back(num[0]);
		}
		if(dim == 2){
			nBins.push_back(num[0]);
			nBins.push_back(num[1]);
		}
	}
	name = name + nameAdd;
	
	switch(dim){
		case 1:
			switch (rst){
			
				case 1:
					fRST1 = new TH1D(name.c_str(), title.c_str(), nBins.at(0), low.at(0), high.at(0));
					fRST1->GetXaxis()->SetLabelSize(axisLabelSize.at(0));
					fRST1->GetXaxis()->SetTitleSize(axisTitleSize.at(0));
					fRST1->GetXaxis()->SetTitleOffset(axisOffset.at(0));
					fRST1->GetXaxis()->SetTitle(axisTitle.at(0).c_str());
			
					fRST1->GetYaxis()->SetLabelSize(axisLabelSize.at(1));
					fRST1->GetYaxis()->SetTitleSize(axisTitleSize.at(1));
					fRST1->GetYaxis()->SetTitleOffset(axisOffset.at(1));
					fRST1->GetYaxis()->SetTitle(axisTitle.at(1).c_str());
					
					fRST1->SetStats(stats);
					fRST1->Sumw2();
					if(plot){
						canvasName = canvasName + nameAdd;
						fPlotRST = true;
						fCRST = new TCanvas(canvasName.c_str(), "", canvasSize.at(0), canvasSize.at(1));

					}
					else{
						fPlotRST = false;
						fCRST = NULL;
					}			
					break;
	
				case 2:
					fWST1 = new TH1D(name.c_str(), title.c_str(), nBins.at(0), low.at(0), high.at(0));
					fWST1->GetXaxis()->SetLabelSize(axisLabelSize.at(0));
					fWST1->GetXaxis()->SetTitleSize(axisTitleSize.at(0));
					fWST1->GetXaxis()->SetTitleOffset(axisOffset.at(0));
					fWST1->GetXaxis()->SetTitle(axisTitle.at(0).c_str());
			
					fWST1->GetYaxis()->SetLabelSize(axisLabelSize.at(1));
					fWST1->GetYaxis()->SetTitleSize(axisTitleSize.at(1));
					fWST1->GetXaxis()->SetTitleOffset(axisOffset.at(1));
					fWST1->GetYaxis()->SetTitle(axisTitle.at(1).c_str());
					fWST1->SetStats(stats);
					fWST1->Sumw2();
					if(plot){
						canvasName = canvasName + nameAdd;
						fPlotWST = true;
						fCWST = new TCanvas(canvasName.c_str(), "", canvasSize.at(0), canvasSize.at(1));

					}
					else{
						fPlotWST = false;
						fCWST = NULL;
					}			
					break;
			
				case 3:
					fAll1 = new TH1D(name.c_str(), title.c_str(), nBins.at(0), low.at(0), high.at(0));
					fAll1->GetXaxis()->SetLabelSize(axisLabelSize.at(0));
					fAll1->GetXaxis()->SetTitleSize(axisTitleSize.at(0));
					fAll1->GetXaxis()->SetTitleOffset(axisOffset.at(0));
					fAll1->GetXaxis()->SetTitle(axisTitle.at(0).c_str());
			
					fAll1->GetYaxis()->SetLabelSize(axisLabelSize.at(1));
					fAll1->GetYaxis()->SetTitleSize(axisTitleSize.at(1));
					fAll1->GetXaxis()->SetTitleOffset(axisOffset.at(1));
					fAll1->GetYaxis()->SetTitle(axisTitle.at(1).c_str());
					fAll1->SetStats(stats);
					fAll1->Sumw2();
					if(plot){
						canvasName = canvasName + nameAdd;
						fPlotRST = true;
						fCAll = new TCanvas(canvasName.c_str(), "", canvasSize.at(0), canvasSize.at(1));

					}
					else{
						fPlotAll = false;
						fCAll = NULL;
					}			
					break;
				default:
					cerr << "ERROR: " << __FUNCTION__ << ": rst variable must have values 1,2 or 3!" << endl;
					exit (EXIT_FAILURE);
					break;
			}
			fRST2 = NULL;
			fWST2 = NULL;
			fAll2 = NULL;
			break;	

		case 2:
			
			switch (rst){
			
				case 1:

					fRST2 = new TH2D(name.c_str(), title.c_str(), nBins.at(0), low.at(0), high.at(0), nBins.at(1), low.at(1), high.at(1));
					fRST2->GetXaxis()->SetLabelSize(axisLabelSize.at(0));
					fRST2->GetXaxis()->SetTitleSize(axisTitleSize.at(0));
					fRST2->GetXaxis()->SetTitleOffset(axisOffset.at(0));
					fRST2->GetXaxis()->SetTitle(axisTitle.at(0).c_str());
			
					fRST2->GetYaxis()->SetLabelSize(axisLabelSize.at(1));
					fRST2->GetYaxis()->SetTitleSize(axisTitleSize.at(1));
					fRST2->GetXaxis()->SetTitleOffset(axisOffset.at(1));
					fRST2->GetYaxis()->SetTitle(axisTitle.at(1).c_str());
					fRST2->SetStats(stats);
					fRST2->Sumw2();
					if(plot){
						canvasName = canvasName + nameAdd;
						fPlotRST = true;
						fCRST = new TCanvas(canvasName.c_str(), "", canvasSize.at(0), canvasSize.at(1));

					}
					else{
						fPlotRST = false;
						fCRST = NULL;
					}			
					break;
	
				case 2:
					fWST2 = new TH2D(name.c_str(), title.c_str(), nBins.at(0), low.at(0), high.at(0), nBins.at(1), low.at(1), high.at(1));
					fWST2->GetXaxis()->SetLabelSize(axisLabelSize.at(0));
					fWST2->GetXaxis()->SetTitleSize(axisTitleSize.at(0));
					fWST2->GetXaxis()->SetTitleOffset(axisOffset.at(0));
					fWST2->GetXaxis()->SetTitle(axisTitle.at(0).c_str());
			
					fWST2->GetYaxis()->SetLabelSize(axisLabelSize.at(1));
					fWST2->GetYaxis()->SetTitleSize(axisTitleSize.at(1));
					fWST2->GetXaxis()->SetTitleOffset(axisOffset.at(1));
					fWST2->GetYaxis()->SetTitle(axisTitle.at(1).c_str());
					fWST2->SetStats(stats);
					fWST2->Sumw2();
					if(plot){
						canvasName = canvasName + nameAdd;
						fPlotWST = true;
						fCWST = new TCanvas(canvasName.c_str(), "", canvasSize.at(0), canvasSize.at(1));

					}
					else{
						fPlotWST = false;
						fCWST = NULL;
					}		
					break;
			
				case 3:
					fAll2 = new TH2D(name.c_str(), title.c_str(), nBins.at(0), low.at(0), high.at(0), nBins.at(1), low.at(1), high.at(1));
					fAll2->GetXaxis()->SetLabelSize(axisLabelSize.at(0));
					fAll2->GetXaxis()->SetTitleSize(axisTitleSize.at(0));
					fAll2->GetXaxis()->SetTitleOffset(axisOffset.at(0));
					fAll2->GetXaxis()->SetTitle(axisTitle.at(0).c_str());
			
					fAll2->GetYaxis()->SetLabelSize(axisLabelSize.at(1));
					fAll2->GetYaxis()->SetTitleSize(axisTitleSize.at(1));
					fAll2->GetXaxis()->SetTitleOffset(axisOffset.at(1));
					fAll2->GetYaxis()->SetTitle(axisTitle.at(1).c_str());
					fAll2->SetStats(stats);
					//fAll2->Sumw2();
					if(plot){
						canvasName = canvasName + nameAdd;
						fPlotRST = true;
						fCAll = new TCanvas(canvasName.c_str(), "", canvasSize.at(0), canvasSize.at(1));

					}
					else{
						fPlotAll = false;
						fCAll = NULL;
					}		
					break;
				default:
					cerr << "ERROR: " << __FUNCTION__ << ": rst variable must have values 1,2 or 3!" << endl;
					exit (EXIT_FAILURE);
					break;
			}
			fRST1 = NULL;
			fWST1 = NULL;
			fAll1 = NULL;
			break;
	}

}

TH1* PlotHisto::GetRSTHisto(){
	switch(fDim){
		case 1:

			return fRST1;
			break;	
		case 2:

			return fRST2;
			break;
	}
}

TH1* PlotHisto::GetWSTHisto(){
	switch(fDim){
		case 1:
			return fWST1;
			break;
		
		case 2:
			return fWST2;
			break;
	}
}

TH1* PlotHisto::GetAllHisto(){
	switch(fDim){
		case 1:
			return fAll1;
			break;
		
		case 2:
			return fAll2;
			break;
	}
}

TCanvas* PlotHisto::GetRSTCanvas(){
	if(!fPlotRST) return NULL;
	
	fCRST->cd();
	if(fDim == 1){
		fRST1->Draw();
	}
	else fRST2->Draw("colz");
	return fCRST;
}

TCanvas* PlotHisto::GetWSTCanvas(){
	if(!fPlotWST) return NULL;
	
	fCWST->cd();
	if(fDim == 1){
		fWST1->Draw();
	}
	else fWST2->Draw("colz");
	return fCWST;
}

TCanvas* PlotHisto::GetAllCanvas(){
	if(!fPlotAll) return NULL;
	
	fCAll->cd();
	
	if(fDim == 1){
		fAll1->Draw();
	}
	else fAll2->Draw("colz");
	
	return fCAll;
}

void PlotHisto::Fill(int rst, double p, double res){
	switch (rst){
		case 1:

			if(fDim == 1){
				fRST1->Fill(p);
			}
			else{
				fRST2->Fill(p, res);
				
			}
			break;
		case 2:

			if(fDim == 1){
				fWST1->Fill(p);
			}
			else fWST2->Fill(p, res);
			break;
	}

	if(fDim == 1){
		fAll1->Fill(p);
	}
	else fAll2->Fill(p, res);
}
