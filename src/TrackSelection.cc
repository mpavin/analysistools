#include "TrackSelection.h"
#include "Functions.h"
#include <evt/rec/RecEventConst.h>
#include <det/BPDConst.h>
#include <boost/assign/std/map.hpp>
#include <boost/assign/list_of.hpp> 
#include <map>
#include <TreeDataTypes.h>
#include <TMath.h>

using namespace boost::assign;
using namespace std;
using namespace evt;
using namespace evt::rec;
using namespace det;


map<string, TrackSelection::evtFuncPtr> TrackSelection::fEvtFunc = 
  	boost::assign::map_list_of
	("status", IsStatusGood)
	("hasTOF", HasToF)
	("momentum", Momentum)
	("sigmaP", SigmaP)
	("sigmaR", SigmaR)
	("hasMTPC", HasMTPC)
	("clustersVTPC", NumVTPC)
	("clustersGTPC", NumGTPC)
	("charge", Charge)
	("topology", GoodTopology)
	("topologyTargFace", GoodTopologyTFace)	
	("numberOfClusters", GoodNumClustersTopology)
	("topologyInMF", TopologyInMagF)
	("pyTopology", PyTopology)
	("theta", Theta)
	("distanceOfApproach", DistanceOfApproach)
	("differentialPhi", DifferentialPhi)
	("firstZ", FirstZ)
	("lastZ", LastZ)
	("exitMFMap", ExitMFMap);



bool TrackSelection::IsTrackGood(AnaTrack &anaTrack){
  
  	++fNEvents;
	//cout << fCut.size() << endl;
  	for (vector<Cut>::iterator cIt = fCut.begin(); cIt < fCut.end(); ++cIt){        

	    	string cutName = cIt -> GetName();
	    	if (fEvtFunc.count(cutName) == 1){
	      		bool retValue = fEvtFunc[cutName]( anaTrack, *cIt);
	      
	      		if (retValue == true){
					cIt -> IncrementNEvents();    
				}
	      		else 
					return false;
	    	}
	    	else { 
	      		cout << "Event Cut " << cutName  << " is not implemented!!! Exiting..." << endl;
	      		exit (100);
	    	}
  	}
  	return true;
}

//********************************************************************************************
bool TrackSelection::IsStatusGood(AnaTrack &tr, Cut&c){
	const vector<double> &cutParam = c.GetParam();
	if (cutParam.size() != 1) {
		cout << __FUNCTION__ 
		 << ": Missing or wrong cut parameters. Exiting..." << endl;
	    	exit(100);
	}

	bool isStatusGood = true;
	if(tr.GetTrack().GetStatus() != (int) cutParam.at(0))
		isStatusGood = false;
 
 	//if(isStatusGood) cout << tr.GetTrack().GetStatus() << endl;
  	return (c.IsAntiCut() ? !isStatusGood : isStatusGood);
}

//********************************************************************************************
bool TrackSelection::Momentum(AnaTrack &tr, Cut&c){



	const vector<double> &cutParam = c.GetParam();
	if (cutParam.size() != 2) {
		cout << "[EventSelection::Momentum] "
		 << "Missing or wrong cut parameters. Exiting..." << endl;
	    	exit(100);
	}

	bool isMomentumGood = false;

 	CutVal ct;
 	ct.intFlag = false;

	ct.valD = tr.GetMomentum().GetMag();
	if(cutParam.at(0) < ct.valD  && ct.valD  < cutParam.at(1))
		isMomentumGood = true;
 
 
	ct.selected = (c.IsAntiCut() ? !isMomentumGood : isMomentumGood);
	//c.AddValue(ct);
  	return (c.IsAntiCut() ? !isMomentumGood : isMomentumGood);
}


//********************************************************************************************
bool TrackSelection::SigmaR(AnaTrack &tr, Cut&c){


	const vector<double> &cutParam = c.GetParam();
	if (cutParam.size() != 1) {
		cout << "[EventSelection::SigmaR] "
		 << "Missing or wrong cut parameters. Exiting..." << endl;
	    	exit(100);
	}

	if(tr.IsExtrapolated() == false) tr.DoExtrapolation();
	
	bool isSigmaGood = false;
	
	CutVal ct;
 	ct.intFlag = false;
 	
	const CovarianceMatrix &cov = tr.GetTrack().GetCovarianceMatrix();
	double x = tr.GetExtrapolatedPosition().GetX();
	double y = tr.GetExtrapolatedPosition().GetY();
	double sigmaX = tr.GetPositionErrors().at(0);
	double sigmaY = tr.GetPositionErrors().at(1);
	double r = sqrt(x*x +y*y);
	
	ct.valD = sqrt(x*x*sigmaX*sigmaX/(r*r) + y*y*sigmaY*sigmaY/(r*r));
	//cout << x << " " << y << " " << sigmaX << " "  << sigmaY << " " << ct.valD << endl;
	if(ct.valD < cutParam.at(0))
		isSigmaGood = true;
 
 	ct.selected = (c.IsAntiCut() ? !isSigmaGood : isSigmaGood);
	//c.AddValue(ct);
  	return (c.IsAntiCut() ? !isSigmaGood : isSigmaGood);
}

//********************************************************************************************
bool TrackSelection::SigmaP(AnaTrack &tr, Cut&c){


	const vector<double> &cutParam = c.GetParam();
	if (cutParam.size() != 1) {
		cout << "[EventSelection::SigmaP] "
		 << "Missing or wrong cut parameters. Exiting..." << endl;
	    	exit(100);
	}
	const CovarianceMatrix &cov = tr.GetTrack().GetCovarianceMatrix();
	double pxz = sqrt(tr.GetMomentum().GetX()*tr.GetMomentum().GetX()+tr.GetMomentum().GetZ()*tr.GetMomentum().GetZ());
	double py = tr.GetMomentum().GetY();
	double pMag = tr.GetMomentum().GetMag();

	double sigmaPxz = cov.Std(1)*pxz*pxz;
	double sigmaPy = sqrt(pow(cov.Std(2)*pxz,2)+pow(cov.Std(1)*pxz*pxz*pxz/py,2));
	bool isSigmaGood = false;
	
	CutVal ct;
 	ct.intFlag = false;
 	
	ct.valD = sqrt(pow(py*sigmaPy/pMag,2) + pow(pxz*sigmaPxz/pMag,2))/pMag;
	
	if(ct.valD < cutParam.at(0))
		isSigmaGood = true;
 
 	ct.selected = (c.IsAntiCut() ? !isSigmaGood : isSigmaGood);
	//c.AddValue(ct);
  	return (c.IsAntiCut() ? !isSigmaGood : isSigmaGood);
}

//********************************************************************************************
bool TrackSelection::GoodTopology(AnaTrack &tr, Cut&c){

	const vector<double> &cutParam = c.GetParam();
	if (cutParam.size() < 1) {
		cout << "[EventSelection::GoodNumClustersTopology] "
		 << "Missing or wrong cut parameters. Exiting..." << endl;
	    	exit(100);
	}

	bool goodTopology = false;


	CutVal ct;
 	ct.intFlag = true;
	for(unsigned int i = 0; i < cutParam.size(); i++){
		if(tr.GetTopology() == (int) cutParam.at(i)){
			goodTopology = true;
			ct.valI = i+1;

			break;
		}
	}

	ct.selected = (c.IsAntiCut() ? !goodTopology : goodTopology);	
	//c.AddValue(ct);	
  	return (c.IsAntiCut() ? !goodTopology : goodTopology);	
	
} 
//********************************************************************************************

bool TrackSelection::GoodNumClustersTopology(AnaTrack &tr, Cut&c){


	const vector<double> &cutParam = c.GetParam();
	if (cutParam.size() != 4) {
		cout << "[EventSelection::GoodNumClustersTopology] "
		 << "Missing or wrong cut parameters. Exiting..." << endl;
	    	exit(100);
	}

	CutVal ct;
 	ct.intFlag = true;
	ct.valI = tr.GetTopology();
	
	if(ct.valI != (int) cutParam.at(0)) 
		return true;
		
	bool goodTopology = true;
	if(tr.GetNumberOfClusters().VTPC1 + tr.GetNumberOfClusters().VTPC2 < cutParam.at(1) ){
		goodTopology = false;
	}
	if(tr.GetNumberOfClusters().GTPC < cutParam.at(2) ){
		goodTopology = false;
	}
	if(tr.GetNumberOfClusters().MTPC  < cutParam.at(3) ){
		goodTopology = false;
	}
	

	ct.selected = (c.IsAntiCut() ? !goodTopology : goodTopology);	
	//c.AddValue(ct);	
  	return (c.IsAntiCut() ? !goodTopology : goodTopology);	
	
} 

//********************************************************************************************
bool TrackSelection::TopologyInMagF(AnaTrack &tr, Cut&c){


	const vector<double> &cutParam = c.GetParam();
	if (cutParam.size() != 1) {
		cout << "[EventSelection::TopologyInMagF] "
		 << "Missing or wrong cut parameters. Exiting..." << endl;
	    	exit(100);
	}

	bool goodTopology = false;
	CutVal ct;
 	ct.intFlag = true;
 	if(tr.GetMomentum().GetX()*tr.GetCharge() > 0){
 		ct.valI = 1;	
 	}
 	else{
 		ct.valI = 0;
 	}
	
 	
	if(tr.GetMomentum().GetX()*tr.GetCharge() > 0 && cutParam.at(0) > 0.5)
		goodTopology = true;
		
	if(tr.GetMomentum().GetX()*tr.GetCharge() < 0 && cutParam.at(0) < 0.5)
		goodTopology = true;
	
	//cout << goodTopology << endl;
	ct.selected = (c.IsAntiCut() ? !goodTopology : goodTopology);
  	return ct.selected;	
	
} 


bool TrackSelection::PyTopology(AnaTrack &tr, Cut&c){


	const vector<double> &cutParam = c.GetParam();
	if (cutParam.size() != 1) {
		cout << "[EventSelection::PyTopology] "
		 << "Missing or wrong cut parameters. Exiting..." << endl;
	    	exit(100);
	}

	bool goodTopology = false;
	CutVal ct;
 	ct.intFlag = true;
 	if(tr.GetMomentum().GetY() > 0){
 		ct.valI = 1;	
 	}
 	else{
 		ct.valI = 0;
 	}
	
 	
	if(tr.GetMomentum().GetY() > 0 && cutParam.at(0) > 0.5)
		goodTopology = true;
		
	if(tr.GetMomentum().GetY() < 0 && cutParam.at(0) < 0.5)
		goodTopology = true;
	
	//cout << goodTopology << endl;
	ct.selected = (c.IsAntiCut() ? !goodTopology : goodTopology);
  	return ct.selected;	
	
} 

//********************************************************************************************
bool TrackSelection::GoodTopologyTFace(AnaTrack &tr, Cut&c){

	

	if(!tr.IsExtrapolated()){
		tr.DoExtrapolation();
	}

	const vector<double> &cutParam = c.GetParam();
	if (cutParam.size() != 1) {
		cout << "[EventSelection::GoodTopologyTFace] "
		 << "Missing or wrong cut parameters. Exiting..." << endl;
	    	exit(100);
	}

	bool goodTopology = true;
	
	CutVal ct;
 	ct.intFlag = true;
	if(!tr.IsExtrapolated()){
		ct.valI = 0;
		ct.selected = false;
		//c.AddValue(ct);			
		return false;
	}
	ct.valI = tr.GetTopology();
	if(tr.GetExtrapolatedPosition().GetZ() < cutParam.at(0)){
		if(tr.GetTopology() == 4 || tr.GetTopology() == 6)
			goodTopology = false;
	}

	ct.selected = (c.IsAntiCut() ? !goodTopology : goodTopology);	
	//c.AddValue(ct);	
	return (c.IsAntiCut() ? !goodTopology : goodTopology);	
	
} 

//********************************************************************************************
bool TrackSelection::NumVTPC(AnaTrack &tr, Cut&c){


	const vector<double> &cutParam = c.GetParam();
	if (cutParam.size() != 1) {
		cout << "[EventSelection::NumVTPC] "
		 << "Missing or wrong cut parameters. Exiting..." << endl;
	    	exit(100);
	}

	CutVal ct;
 	ct.intFlag = true;
 	bool isNumGood = true;
 	if(tr.GetNumberOfClusters().GTPC == 0 || (tr.GetNumberOfClusters().VTPC1 > 0 && tr.GetNumberOfClusters().VTPC2 > 0) || tr.GetNumberOfClusters().VTPC1>0){
		ct.valI = tr.GetNumberOfClusters().VTPC1 + tr.GetNumberOfClusters().VTPC2;

		
		if(ct.valI < cutParam.at(0))
			isNumGood = false;

	}
	ct.selected = (c.IsAntiCut() ? !isNumGood : isNumGood);
	//c.AddValue(ct);
  	return (c.IsAntiCut() ? !isNumGood : isNumGood);
}


//********************************************************************************************
bool TrackSelection::NumGTPC(AnaTrack &tr, Cut&c){


	const vector<double> &cutParam = c.GetParam();
	if (cutParam.size() != 1) {
		cout << "[EventSelection::NumVTPC] "
		 << "Missing or wrong cut parameters. Exiting..." << endl;
	    	exit(100);
	}

	CutVal ct;
 	ct.intFlag = true;
 	bool isNumGood = true;
 	if(tr.GetNumberOfClusters().GTPC > 0 && tr.GetNumberOfClusters().VTPC1 == 0){
		ct.valI = tr.GetNumberOfClusters().GTPC;

		
		if(ct.valI < cutParam.at(0))
			isNumGood = false;
	}
	ct.selected = (c.IsAntiCut() ? !isNumGood : isNumGood);
	//c.AddValue(ct);
  	return (c.IsAntiCut() ? !isNumGood : isNumGood);
}
//********************************************************************************************
bool TrackSelection::Charge(AnaTrack &tr, Cut&c){



	const vector<double> &cutParam = c.GetParam();
	if (cutParam.size() != 1) {
		cout << "[EventSelection::Charge] "
		 << "Missing or wrong cut parameters. Exiting..." << endl;
	    	exit(100);
	}
	bool isChargeGood = true;
	CutVal ct;
 	ct.intFlag = true;
	ct.valI = tr.GetCharge();

	if(ct.valI != cutParam.at(0)){
		isChargeGood = false;
	}

	ct.selected = (c.IsAntiCut() ? !isChargeGood : isChargeGood);
	//c.AddValue(ct);
	
  	return (c.IsAntiCut() ? !isChargeGood : isChargeGood);
}

//********************************************************************************************
bool TrackSelection::DistanceOfApproach(AnaTrack &tr, Cut&c){



	if(!tr.IsExtrapolated()){
		tr.DoExtrapolation();
	}

	const vector<double> &cutParam = c.GetParam();
	if (cutParam.size() != 1) {
		cout << "[EventSelection::DistanceOfApproach] "
		 << "Missing or wrong cut parameters. Exiting..." << endl;
	    	exit(100);
	}
	bool isDoAGood = true;

	CutVal ct;
 	ct.intFlag = false;
	if(!tr.IsExtrapolated()){
		ct.valD = 0;
		ct.selected = false;
		return false;
	}
	double x = tr.GetExtrapolatedPosition().GetX();
	double y = tr.GetExtrapolatedPosition().GetY();
	//cout << __FUNCTION__ << " " << x << " " << y << endl;
	double sigmaX = tr.GetPositionErrors().at(0);
	double sigmaY = tr.GetPositionErrors().at(1);
	//cout << sigmaX << " " << sigmaY << endl;
	double r = sqrt(x*x + y*y);
	ct.valD = tr.GetDistanceFromTarget();
	double posError = sqrt(pow(x*sigmaX,2)+pow(y*sigmaY,2))/r;
	//cout << ct.valD << endl;
	//double selVal = 0.6;
	double selVal = cutParam.at(0)*posError;
	//cout << selVal << " " << posError << " "  << ct.valD << endl;
	//if(cutParam.at(0)*posError < selVal) selVal = cutParam.at(0)*posError;
	if(ct.valD > selVal){
		isDoAGood = false;
	}
	//cout << cutParam.at(0)*posError << endl;
	ct.selected = (c.IsAntiCut() ? !isDoAGood : isDoAGood);
	//c.AddValue(ct);
  	return (c.IsAntiCut() ? !isDoAGood : isDoAGood);
}


//********************************************************************************************
bool TrackSelection::HasToF(AnaTrack &tr, Cut&c){


	//cout << tr.HasToF() << endl;
  	return (c.IsAntiCut() ? !tr.HasToF() : tr.HasToF());
}
/*

bool TrackSelection::HasdEdx(const Track &tr, const Cut&c){

	if(fHasToF.size()==0){
		TH1D h11("HasdEdxOrig", "HasdEdx",6,-0.3,1.1);
		TH1D h12("HasdEdxSel", "HasdEdx",6,-0.3,1.1);

		fHasdEdx.push_back(h11);
		fHasdEdx.push_back(h12);
	}

	bool hasdEdx = true;
	if(tr.GetNumdEdx() < 1)
		hasdEdx = false;
 

	if(hasdEdx == true){

		fHasdEdx.at(0).Fill(1);
		if(c.IsAntiCut() == false){	
			fHasdEdx.at(1).Fill(1);
		}

	}  

	else{
		fHasdEdx.at(0).Fill(0);
		if(c.IsAntiCut() == true){
			fHasdEdx.at(1).Fill(0);
		}
	}
  	return (c.IsAntiCut() ? !hasdEdx : hasdEdx);
}
*/

//********************************************************************************************
bool TrackSelection::HasMTPC(AnaTrack &tr,Cut&c){



	bool hasMTPC = false;

	if(tr.GetNumberOfClusters().MTPC > 0) hasMTPC = true;

  	return (c.IsAntiCut() ? !hasMTPC : hasMTPC);
}



//********************************************************************************************
bool TrackSelection::Theta(AnaTrack &tr, Cut&c){



	const vector<double> &cutParam = c.GetParam();
	if (cutParam.size() != 2) {
		cerr << "ERROR: " << __FUNCTION__ 
		 << ": Missing or wrong cut parameters. Exiting..." << endl;
	    	exit(EXIT_FAILURE);
	}
	bool isThetaGood = true;
	CutVal ct;
 	ct.intFlag = false;
	ct.valD = 1000*acos(tr.GetMomentum().GetZ()/tr.GetMomentum().GetMag());

	if(ct.valD < cutParam.at(0) || ct.valD > cutParam.at(1)){
		isThetaGood = false;
	}

	ct.selected = (c.IsAntiCut() ? !isThetaGood : isThetaGood);
	//c.AddValue(ct);
	
  	return (c.IsAntiCut() ? !isThetaGood : isThetaGood);
}

//********************************************************************************************

bool TrackSelection::DifferentialPhi(AnaTrack &tr, Cut&c){

	if(!tr.IsExtrapolated()){
		tr.DoExtrapolation();
	}
	const vector<double> &cutParam = c.GetParam();
	if (cutParam.size() != 7) {
		cout << "[TrackSelection::DifferentialPhi] "
		 << "Missing or wrong cut parameters. Exiting..." << endl;
	    	exit(100);
	}
	bool isPhiGood = false;

	
	CutVal ct;
 	ct.intFlag = false;
	//cout << "radi1" << endl;
	double px = tr.GetMomentum().GetX();
	double py = tr.GetMomentum().GetY();
	double pz = tr.GetMomentum().GetZ();
	
	if(cutParam.at(0) == 1 && px*tr.GetCharge() < 0){
		ct.selected = true;
		return true;
	} 
	else if(cutParam.at(0) == 0 && px*tr.GetCharge() > 0) {
		ct.selected = true;
		return true;	
	}
	
	//cout << "radi2" << endl;
	double theta = 1000*acos(pz/sqrt(px*px+py*py+pz*pz));
	ct.valD = -180;
	
	if(theta >= cutParam.at(1) && theta < cutParam.at(2)){
		double phi = acos(px/sqrt(px*px+py*py))*180/TMath::Pi();
		if(px>0 && py < 0){
			phi = -1*phi;
		}
		else if(px<0 && py < 0){
			phi = 360 - phi;
		}
		ct.valD = phi;
		bool good1 = false;
		bool good2 = false;
		
		if(phi > cutParam.at(3) && phi < cutParam.at(4)) good1 = true;
		if(phi > cutParam.at(5) && phi < cutParam.at(6)) good2 = true;
		
		isPhiGood = good1 || good2;
		//cout << isPhiGood << " " << phi << " " << cutParam.at(3) << " " << cutParam.at(4) << " " << cutParam.at(5) << " " <<cutParam.at(6)  << endl;
	}
	else isPhiGood=true;

	

	//cout << cutParam.at(0)*posError << endl;
	ct.selected = (c.IsAntiCut() ? !isPhiGood : isPhiGood);
	//c.AddValue(ct);
  	return (c.IsAntiCut() ? !isPhiGood : isPhiGood);
}
  
bool TrackSelection::FirstZ(AnaTrack &tr, Cut&c){


	const vector<double> &cutParam = c.GetParam();
	if (cutParam.size() != 1) {
		cout << "[TrackSelection::StartZ] "
		 << "Missing or wrong cut parameters. Exiting..." << endl;
	    	exit(100);
	}
	bool isZGood = false;

	CutVal ct;
 	ct.intFlag = false;


	if(tr.GetTrack().GetFirstPointOnTrack().GetZ() <= cutParam.at(0)){
		isZGood = true;
	}

	ct.selected = (c.IsAntiCut() ? !isZGood : isZGood);
	//c.AddValue(ct);
  	return (c.IsAntiCut() ? !isZGood : isZGood);
}


bool TrackSelection::LastZ(AnaTrack &tr, Cut&c){


	const vector<double> &cutParam = c.GetParam();
	if (cutParam.size() != 1) {
		cout << __FUNCTION__ 
		 << ": Missing or wrong cut parameters. Exiting..." << endl;
	    	exit(100);
	}
	bool isZGood = false;

	CutVal ct;
 	ct.intFlag = false;

	if(tr.GetLastZ() >= cutParam.at(0)){
		isZGood = true;
	}
	/*if(tr.GetTrack().GetLastPointOnTrack().GetZ() >= cutParam.at(0)){
		isZGood = true;
	}*/

	ct.selected = (c.IsAntiCut() ? !isZGood : isZGood);
	//c.AddValue(ct);
  	return (c.IsAntiCut() ? !isZGood : isZGood);
}


bool TrackSelection::ExitMFMap(AnaTrack &tr, Cut&c){

	if(!tr.IsExtrapolated()){
		tr.DoExtrapolation();
	}
	
	const vector<double> &cutParam = c.GetParam();
	if (cutParam.size() != 3) {
		cerr << "ERROR: " << __FUNCTION__ 
		 << ": Missing or wrong cut parameters. Exiting..." << endl;
	    	exit(EXIT_FAILURE);
	}
	bool isInside = true;
	
	CutVal ct;
 	ct.intFlag = false;
 		
	if(1000*acos(tr.GetMomentum().GetZ()/tr.GetMomentum().GetMag()) < cutParam.at(0)){
		ct.selected = (c.IsAntiCut() ? !isInside : isInside);
		ct.valD = 0;
		return (c.IsAntiCut() ? !isInside : isInside);
	}

	TrackExtrapolation trackExtrapolation(false, 0.05, 0);
	trackExtrapolation.SetTrackParams(tr.GetTrack());
	
	

	TrackPar &trackPar = trackExtrapolation.GetStopTrackParam(); 
	while(trackPar.GetZ() < -50){
		trackExtrapolation.Extrapolate(trackPar.GetZ()+1.);
		if(TMath::Abs(trackPar.GetPar(0)) > cutParam.at(1) || TMath::Abs(trackPar.GetPar(1)) > cutParam.at(2)){
			isInside = false;
			ct.valD = trackPar.GetPar(0);
			break;
		}
	}
	
	//cout << isInside << " " << 1000*acos(tr.GetMomentum().GetZ()/tr.GetMomentum().GetMag()) << " " << trackPar.GetPar(0) << " " << trackPar.GetPar(1) << " " <<tr.GetExtrapolatedPosition().GetZ() << endl;
  	return (c.IsAntiCut() ? !isInside : isInside);
}
