#include "StrTools.h"
#include <sstream>
#include <time.h>
#include <iostream>     
#include <iomanip>  
	
std::string StrTools::ConvertToString(int x){
	std::stringstream ss;
	std::string val;

	ss << x;
	ss >> val;

	return val;
}

std::string StrTools::ConvertToString(double x, int prec){
	std::stringstream ss;
	std::string val;

	ss << std::fixed << std::setprecision(prec);
	ss << x;
	ss >> val;

	return val;
}
	
void StrTools::RemoveLeadingTrailingSpace(std::string &str){
	while(str.at(0) == ' '){
		str.erase(0,1);
	}
	
	while(str.at(str.size()-1) == ' '){
		str.erase(str.size()-1,1);
	}
}
	
std::string StrTools::GetTimeString(){
	time_t rawtime;
	struct tm* timeinfo;

	time(&rawtime);
	timeinfo = localtime(&rawtime);

	std::string res;
	std::stringstream s;

	s << timeinfo->tm_mon + 1;
	s << "_";
	s << timeinfo->tm_mday;
	s << "_";
	s << timeinfo->tm_year + 1900;
	s << "_";
	s << timeinfo->tm_hour;
	s << "_";
	s << timeinfo->tm_min;
	s << "_";
	s << timeinfo->tm_sec;

	s >> res;

	return res;
}
