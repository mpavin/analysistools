#include <stdlib.h>

#include "PlotGraph.h"
#include <TAxis.h>
#include <TPaveText.h>
#include <fwk/CentralConfig.h>
#include <utl/Branch.h>
#include <utl/BranchIterator.h>
#include "CConfig.h"
#include "Functions.h"
#include <vector>
#include "StrTools.h"

using namespace fwk;
using namespace utl; 
using namespace StrTools;

PlotGraph::PlotGraph(int nPoints, double *x, double *y, double *xE, double *yE, string config, string graphName, string nameAdd){

	Branch graphsCfg = cConfig.GetTopBranch(config);
	if(graphsCfg.GetName() != config){
		cerr <<"ERROR: "<< __FUNCTION__ << ": Wrong graph config file! Top branch name doesn't match! Please check your configuration files! Exiting..." << endl;
		exit (EXIT_FAILURE);
	}


	Branch child = graphsCfg.GetChild(graphName);
	
	fPlot = false;
	fDraw = false;
	fPlotOptions = "P";
	string name = "graph";
	string title = "graph";
	string canvasName = "cGraph";
	double minimum = 0;
	double maximum = 1;
	bool min = false;
	bool max = false;
	int markerType = 4;
	double markerSize = 1;
	int markerColor = 4;
	
	int lineSize = 1;
	int lineColor = 4;
	int lineType = 1;
	vector<string> axisTitle;
	vector<double> axisTitleSize;
	vector<double> axisLabelSize;
	vector<double> axisOffset(2,1);
	vector<int> canvasSize;
	for(BranchIterator it = child.ChildrenBegin(); it != child.ChildrenEnd(); ++it){
		Branch option = *it;
		if(option.GetName() == "name"){
			option.GetData(name);
		}

		else if(option.GetName() == "title"){
			option.GetData(title);
		}
		
		else if(option.GetName() == "axisTitle"){
			axisTitle.clear();
			option.GetData(axisTitle);
		}
		
		else if(option.GetName() == "axisTitleSize"){
			axisTitleSize.clear();
			option.GetData(axisTitleSize);
		}	
		
		else if(option.GetName() == "axisLabelSize"){
			axisLabelSize.clear();
			option.GetData(axisLabelSize);
		}
		
		else if(option.GetName() == "axisOffset"){
			axisOffset.clear();
			option.GetData(axisOffset);
		}

		else if(option.GetName() == "minimum"){
			option.GetData(minimum);
			min = true;
		}
		
		else if(option.GetName() == "maximum"){
			option.GetData(maximum);
			max = true;
		}	
		
		else if(option.GetName() == "markerType"){
			option.GetData(markerType);
		}	
		
		else if(option.GetName() == "markerSize"){
			option.GetData(markerSize);
		}	
		
		else if(option.GetName() == "markerColor"){
			option.GetData(markerColor);
		}
		
		else if(option.GetName() == "lineType"){
			option.GetData(lineType);
		}	
		
		else if(option.GetName() == "lineSize"){
			option.GetData(lineSize);
		}	
		
		else if(option.GetName() == "lineColor"){
			option.GetData(lineColor);
		}
		
		else if(option.GetName() == "plot"){
			option.GetData(fPlot);
		}	
		
		else if(option.GetName() == "canvasName"){
			option.GetData(canvasName);
		}	
		
		else if(option.GetName() == "canvasSize"){
			canvasSize.clear();
			option.GetData(canvasSize);
		}
		
		else if(option.GetName() == "plotOptions"){
			option.GetData(fPlotOptions);
		}		
		else if(option.GetName() == "paveText"){
			option.GetData(fText);
		}			
	}
	
	fGraph = new TGraphErrors(nPoints, x, y, xE, yE);	
	fGraph->SetName(name.c_str());
	fGraph->SetTitle(title.c_str());
	fGraph->GetXaxis()->SetLabelSize(axisLabelSize.at(0));
	fGraph->GetXaxis()->SetTitleSize(axisTitleSize.at(0));
	fGraph->GetXaxis()->SetTitleOffset(axisOffset.at(0));
	fGraph->GetXaxis()->SetTitle(axisTitle.at(0).c_str());
	
	fGraph->GetYaxis()->SetLabelSize(axisLabelSize.at(1));
	fGraph->GetYaxis()->SetTitleSize(axisTitleSize.at(1));
	fGraph->GetYaxis()->SetTitleOffset(axisOffset.at(1));
	fGraph->GetYaxis()->SetTitle(axisTitle.at(1).c_str());
	
	if(min)
		fGraph->SetMinimum(minimum);
	if(max)
		fGraph->SetMaximum(maximum);
	
	fGraph->SetMarkerStyle(markerType);
	fGraph->SetMarkerSize(markerSize);
	fGraph->SetMarkerColor(markerColor);
	
	fGraph->SetLineStyle(lineType);
	fGraph->SetLineWidth(lineSize);
	fGraph->SetLineColor(lineColor);
	
	if(fPlot){
		canvasName = canvasName + nameAdd;
		fCanvas = new TCanvas(canvasName.c_str(), "", canvasSize.at(0), canvasSize.at(1));
	}
	
}


TCanvas* PlotGraph::GetCanvas(){
	if(!fPlot) return NULL;
	if(!fDraw){
		fCanvas->cd();
		fGraph->Draw(fPlotOptions.c_str());
		fDraw = true;
	}
	return fCanvas;
}

void PlotGraph::AddFunction(TF1 *f, string option){
	if(!fPlot) return;
	fCanvas->cd();
	if(!fDraw){
		
		fGraph->Draw(fPlotOptions.c_str());
		fDraw = true;
	}

	f->Draw(option.c_str());

}

void PlotGraph::FitFunction(TF1 *f, double min, double max){
	fGraph->Fit(f, "Q", "Csame", min, max );
	
	if(fPlot){
		int n = f->GetNpar();
		
		TPaveText *pt = new TPaveText(fText.at(0), fText.at(1), fText.at(2), fText.at(3), "brNDC");
		
		fCanvas->cd();
		if(!fDraw){
		
			fGraph->Draw(fPlotOptions.c_str());
			fDraw = true;
		}
		for(int i = 0; i < n; ++i){
			string text = "p_{" + StrTools::ConvertToString(i) + "} = " + StrTools::ConvertToString(f->GetParameter(i),5) + " #pm " + StrTools::ConvertToString(f->GetParError(i),5);
			pt->AddText(text.c_str());
		}
		string text = "#chi^{2}/NDF = " + StrTools::ConvertToString(f->GetChisquare()/f->GetNDF());
		pt->AddText(text.c_str());
		//f->Draw("Lsame");
		pt->Draw();		
		
	}
}
