#include "Selection.h"
#include <TTree.h>
#include <TFile.h>
#include <stdlib.h>
#include <fwk/CentralConfig.h>
#include <utl/Branch.h>
#include <utl/BranchIterator.h>
#include "CConfig.h"
#include <TreeDataTypes.h>

using namespace fwk;
using namespace utl; 

void Selection::Read(string branchName){
	Branch cuts = cConfig.GetTopBranch(branchName);
	if(cuts.GetName() != branchName){
		cout << __FUNCTION__ << ": Wrong cut config file! Top branch name doesn't match! Please check your configuration files! Exiting..." << endl;
		exit (EXIT_FAILURE);
	}
	for(BranchIterator it = cuts.ChildrenBegin(); it != cuts.ChildrenEnd(); ++it){
		Branch child = *it;
		AttributeMap atts = child.GetAttributes();

		if(atts["on"] == "0") continue;

		vector<double> pars;
		bool isAntiCut = false;
		for(BranchIterator cutIt = child.ChildrenBegin(); cutIt != child.ChildrenEnd(); ++cutIt){
			Branch cut = *cutIt;

			if(cut.GetName() == "anticut"){
				cut.GetData(isAntiCut);
			}
			if(cut.GetName() == "val"){
				cut.GetData(pars);

			}
		}
		Cut c;
		c.SetName(child.GetName());
		c.SetAntiCut(isAntiCut);
		c.SetParam(pars);
		
		fCut.push_back(c);
	}
}


void Selection::WriteToRoot(string cuts, string file){


	TFile f(file.c_str(),"UPDATE");

	f.mkdir(cuts.c_str());
	f.cd(cuts.c_str());
	for(int i = 0; i < fCut.size(); i++){
		TTree* tree = new TTree(fCut.at(i).GetName().c_str(), fCut.at(i).GetName().c_str());

		int nEvent = fCut.at(i).GetNEvents();
		vector<CutVal> vals = fCut.at(i).GetValues();

		if(vals.size() == 0){
			tree->Branch("NPassed", &nEvent, "NPassed/I");
			tree->Fill();
		}
		else{

			bool sel = 0;
			tree->Branch("selected", &sel, "selected/O");
			
			if(vals.at(0).intFlag == false){
				double val = 0;
				tree->Branch("value", &val, "value/D");
	
				for(int j = 0; j < vals.size(); j++){
					val = vals.at(j).valD;
					sel = vals.at(j).selected;

					tree->Fill();
				}
			}
			else{
				int val;
				tree->Branch("value", &val, "value/I");
	
				for(int j = 0; j < vals.size(); j++){

					val = vals.at(j).valI;
					sel = vals.at(j).selected;

					tree->Fill();
				}
			}
		}

		tree->Write();
	}

	f.Close();
	
}
