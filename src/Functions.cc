#include <stdlib.h>
#include <math.h>
#include "Functions.h"

using namespace std;


double DoStep(double x, double y, double r1, double r2, TH1 *hist){
	unsigned int startX = hist->GetXaxis()->FindBin(x-r1);
	unsigned int stopX = hist->GetXaxis()->FindBin(x+r1);

	unsigned int startY = hist->GetYaxis()->FindBin(y-r2);
	unsigned int stopY = hist->GetYaxis()->FindBin(y+r2);

	double sum = 0;
	for(unsigned int k = startX; k <= stopX; ++k){
		for(unsigned int m = startY; m <= stopY; ++m){
			double x1 = hist->GetXaxis()->GetBinCenter(k);
			double y1 = hist->GetYaxis()->GetBinCenter(m);
			unsigned int nBin = hist->GetBin(k, m);

			//double dist = sqrt((x-x1)*(x-x1) + (y-y1)*(y-y1));
			double dist = (x-x1)*(x-x1)/r1/r1 + (y-y1)*(y-y1)/r2/r2;
			if(dist < 1){
				sum += hist->GetBinContent(nBin);

			}

		}
				
	}
	//if(sum > 0) cout << "SUM: " << sum << endl;
	return sum;

}

bool FindMaximum(double *val, double r1, double r2, double step, TH1 *hist){

	//double r = 1.3; //radius = 1.3 cm

	unsigned int nBinsX = hist->GetNbinsX();
	unsigned int nBinsY = hist->GetNbinsY();
	//double step = 0.1;
	
	double startX = hist->GetXaxis()->GetBinLowEdge(1)+r1;
	double stopX = hist->GetXaxis()->GetBinUpEdge(nBinsX)-r1;

	double startY = hist->GetYaxis()->GetBinLowEdge(1)+r2;
	double stopY = hist->GetYaxis()->GetBinUpEdge(nBinsY)-r2;
	
	//double startY = -0.3;
	//double stopY = 0.3;
	unsigned int nStepsX = (stopX - startX)/step;
	unsigned int nStepsY = (stopY - startY)/step;

	double valXY[3] = {0, 0, 0};
	double sum = 0;
	for(unsigned int i = 0; i < nStepsX; ++i){

	  double x = startX + i*step;
	  for(unsigned int j = 0; j < nStepsY; ++j){
	    double y = startY + j*step;
	    sum = DoStep(x,y,r1, r2,hist);
	    
	    if(sum > valXY[2]){
	      valXY[2] = sum;
	      valXY[0] = x;
	      valXY[1] = y;
	    }
	    if(sum == valXY[2]){
	      if((fabs(x-valXY[0]) == step)||(fabs(y-valXY[1]) == step)){
		valXY[0] += (x-valXY[0])/2;
		valXY[1] += (y-valXY[1])/2;
		}
		}
	  }
	}
	
	//cout << "F:" << valXY[0] << " " << valXY[1] << endl;
	val[0] = valXY[0];
	val[1] = valXY[1];
	val[2] = valXY[2];
	return true;
	
}

bool ChangedSign(double *x0, double *x1){
	
	if((*x0 < 0)^(*x1 > 0)){
		return true;
	}
	else{
		return false;
	}
	
}


/*ETopology GetTopology(const Track &tr){
	ETopology top;
	unsigned int nClusters[3];
	
	nClusters[0] = tr.GetNumberOfClusters(TrackConst::eVTPC1);
	nClusters[1] = tr.GetNumberOfClusters(TrackConst::eVTPC2);
	nClusters[2] = tr.GetNumberOfClusters(TrackConst::eGTPC);

	unsigned int n = nClusters[0] + nClusters[1] + nClusters[2];

	//if(nClusters[2] > 0) cout << "# of GTPC clusters: " << nClusters[2] << endl;
	if(n == nClusters[0]) return eTopVTPC1;
	if(n == nClusters[1]) return eTopVTPC2;
	if(n == nClusters[0]+nClusters[1]) return eTopVTPC1_VTPC2;
	if(n == nClusters[0]+nClusters[2]) return eTopVTPC1_GTPC;
	if(n == nClusters[1]+nClusters[2]){
		//cout << n << endl;
		return eTopVTPC2_GTPC;
	}
	if(n == nClusters[2]) return eTopGTPC;

	return eTopUnknown;
	
}*/

void SetHistoStyle(TH1 *hist, string xLabel, string yLabel, string title){

	if(title != ""){
		hist->SetTitle(title.c_str());
		hist->SetTitleSize(0.7);
	}
	hist->GetXaxis()->SetTitle(xLabel.c_str());
	hist->GetXaxis()->SetTitleSize(0.5);
	hist->GetYaxis()->SetTitle(yLabel.c_str());
	hist->GetYaxis()->SetTitleSize(0.5);
}

void SetHistoStyle(TH2 *hist, string xLabel, string yLabel, string title){

	if(title != ""){
		hist->SetTitle(title.c_str());
		hist->SetTitleSize(0.7);
	}
	hist->GetXaxis()->SetTitle(xLabel.c_str());
	hist->GetXaxis()->SetTitleSize(0.5);
	hist->GetYaxis()->SetTitle(yLabel.c_str());
	hist->GetYaxis()->SetTitleSize(0.5);
}

